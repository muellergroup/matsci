# matsci

The matsci library provides core classes that can be used to build Java applications for atomic-scale computational materials research.

The matsci library makes use of the EJML and Colt matrix libraries.  EJML is licensed under an Apache v2.0 license, and Colt is used under the following license terms:

Copyright (c) 1999 CERN - European Organization for Nuclear Research.

Permission to use, copy, modify, distribute and sell this software and its documentation for any purpose is hereby granted without fee, provided that the above copyright notice appear in all copies and that both that copyright notice and this permission notice appear in supporting documentation. CERN makes no representations about the suitability of this software for any purpose. It is provided "as is" without expressed or implied warranty. 