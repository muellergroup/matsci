/*
 * Created on May 23, 2014
 *
 */
package matsci.io.vasp;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;

import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.reciprocal.KPointLattice;
import matsci.util.MSMath;
import matsci.util.StringUtils;
import matsci.util.arrays.ArrayUtils;

public class KPOINTS {
  
  private enum FILE_TYPE {MANUAL, AUTOMATIC, BANDSTRUCTURE};
  
  private BravaisLattice m_Lattice;
  private String m_Comment = "VASP KPOINTS file";
  private Coordinates[] m_Coordinates;
  private AbstractLinearBasis m_Basis = CartesianBasis.getInstance();
  private double[] m_Weights;
  
  private int[][] m_Tetrahedra;
  private int[] m_TetrahedraWeights;
  private double m_TetrahedraVolumeFraction;
  
  private FILE_TYPE m_FileType;
  
  private double m_FormatPrecision = 10E-15;

  public KPOINTS(Coordinates[] coords, double[] weights) {
    m_FileType = FILE_TYPE.MANUAL;
    m_Coordinates = (Coordinates[]) ArrayUtils.copyArray(coords);
    m_Weights = ArrayUtils.copyArray(weights);
  }
  
  public KPOINTS(Coordinates[] coords, double[] weights, LinearBasis basis) {
    m_FileType = FILE_TYPE.MANUAL;
    m_Coordinates = (Coordinates[]) ArrayUtils.copyArray(coords);
    m_Weights = ArrayUtils.copyArray(weights);
    // Shift to a Cartesian Origin because that's what VASP uses
    m_Basis = new LinearBasis(CartesianBasis.getInstance().getOrigin(), basis.getBasisToCartesian());
    
  }
  
  public KPOINTS(KPointLattice lattice, LinearBasis basis) {
    m_FileType = FILE_TYPE.MANUAL;
    m_Coordinates = lattice.getDistinctKPointCoords();
    m_Weights = lattice.getDistinctKPointWeights();
    m_Basis = basis;
    m_Lattice = lattice.getKPointLattice();
    /*m_Tetrahedra = lattice.getDistinctTetrahedaIndices();
    m_TetrahedraWeights = lattice.getDistinctTetrahedraWeights();*/
    m_Tetrahedra = lattice.getReducedTetraheda();
    m_TetrahedraWeights = lattice.getReducedTetrahedraWeights();
    m_TetrahedraVolumeFraction = 1.0 / (lattice.numTotalKPoints() * lattice.numTetPerPrimCell());
  }
  
  public KPOINTS(KPointLattice lattice) {
    this(lattice, lattice.getReciprocalSpaceLattice().getLatticeBasis());
  }

  public void useAutomaticOutput() {
    m_FileType = FILE_TYPE.AUTOMATIC;
  }
  
  public void setDescription(String comment) {
    m_Comment = comment;
  }
  
  public String setComment() {
    return m_Comment;
  }
  
  public void writeFile(String fileName) {
    
    try {
      FileWriter fileWriter = new FileWriter(fileName);
      BufferedWriter writer = new BufferedWriter(fileWriter);
      this.write(writer);
      writer.flush();
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException("Failed to write file " + fileName, e);
    } 
  }
  
  public void write(Writer writer) {
    try {
      switch (m_FileType) {
        case MANUAL : writeManual(writer); break;
        case AUTOMATIC : writeAutomatic(writer); break;
        case BANDSTRUCTURE : writeBandStructure(writer); break;
      }
    } catch (IOException e) {
      throw new RuntimeException("Failed to write kpoints.", e);
    } 

  }
  
  private void writeManual(Writer writer) throws IOException {
    writer.write(m_Comment);
    this.newLine(writer);
    
    writer.write(m_Coordinates.length + "");
    this.newLine(writer);
    
    boolean isCartesian = (m_Basis == CartesianBasis.getInstance());
    String basisType = isCartesian ? "Cartesian" : "Fractional";
    writer.write(basisType);
    this.newLine(writer);
    
    String formatString = "0.00000000000000";
    DecimalFormat formatter = new DecimalFormat(formatString);
    
    for (int pointNum = 0; pointNum < m_Coordinates.length; pointNum++) {
      double[] coordArray = m_Coordinates[pointNum].getCoordArray(m_Basis);
      if (!isCartesian) {
        for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
          // Grid rid of some numerical issues output 1.0000000 as coordinate.
          if (Math.abs(coordArray[dimNum] - Math.round(coordArray[dimNum])) < m_FormatPrecision) {
              coordArray[dimNum] = MSMath.roundWithPrecision(coordArray[dimNum], m_FormatPrecision);
          }
          coordArray[dimNum] -= Math.floor(coordArray[dimNum]);
        }
      }
      writer.write(formatter.format(coordArray[0]) + " ");
      writer.write(formatter.format(coordArray[1]) + " ");
      writer.write(formatter.format(coordArray[2]) + " ");
      writer.write(m_Weights[pointNum] + " ! " + (pointNum + 1));
      this.newLine(writer);
    }
    this.writeTetrahedra(writer);
    
  }
  
  private void writeTetrahedra(Writer writer) throws IOException {
    if (m_Tetrahedra == null) {return;}
    writer.write("Tetrahedra");
    this.newLine(writer);
    writer.write(m_Tetrahedra.length + "    " + m_TetrahedraVolumeFraction);
    this.newLine(writer);
    for (int tetNum = 0; tetNum < m_Tetrahedra.length; tetNum++) {
      writer.write(m_TetrahedraWeights[tetNum] + "    ");
      int[] tet = m_Tetrahedra[tetNum];
      for (int pointNum = 0; pointNum < tet.length; pointNum++) {
        writer.write((tet[pointNum]+1) + "   ");
      }
      this.newLine(writer);
    }
  }
  
  private void writeAutomatic(Writer writer) throws IOException {
    
    if (m_Lattice == null) {
      throw new RuntimeException("Cannot write KPOINTS file in automatic mode if k-point lattice vectors are not provided.");
    }
    
    writer.write(m_Comment);
    this.newLine(writer);
    
    writer.write("0");
    this.newLine(writer);
    
    boolean isCartesian = (m_Basis == CartesianBasis.getInstance());
    String basisType = isCartesian ? "Cartesian" : "Fractional";
    writer.write(basisType);
    this.newLine(writer);
    
    String formatString = "0.0000000000000000";
    if (m_FormatPrecision < 1) {
      formatString = formatString.substring(0, 2-((int) Math.round(Math.log10(m_FormatPrecision))));
    } else {
      formatString = "0";
    }
    DecimalFormat formatter = new DecimalFormat(formatString);

    BravaisLattice compactLattice = m_Lattice.getCompactLattice();
    
    double[][] outputArray = new double[compactLattice.numTotalVectors()][];
    for (int vecNum = 0; vecNum < outputArray.length; vecNum++) {
      outputArray[vecNum] = compactLattice.getCellVector(vecNum).getDirectionArray(m_Basis);
    }
    double determinant = MSMath.determinant(outputArray); // VASP seems to struggle with negative determinants.
    if (determinant < 0) {
      outputArray[outputArray.length - 1] = MSMath.arrayMultiply(outputArray[outputArray.length - 1], -1);
    }
    
    for (int vecNum = 0; vecNum < outputArray.length; vecNum++) {
      
      double[] coordArray = outputArray[vecNum];
      
      for (int coordNum = 0; coordNum < coordArray.length; coordNum++) {
        double value = MSMath.roundWithPrecision(coordArray[coordNum], m_FormatPrecision); // Gets rid of some ugly numerical errors
        String output = formatter.format(value);
        output = StringUtils.padLeft(output, formatString.length() + 3);
        writer.write(output);
      }
      this.newLine(writer);
    }
    Vector shiftVector = new Vector(CartesianBasis.getInstance().getOrigin(), m_Lattice.getOrigin());
    double[] shiftArray = shiftVector.getDirectionArray(compactLattice.getLatticeBasis());
    writer.write(StringUtils.padLeft(formatter.format(shiftArray[0]), formatString.length() + 3));
    writer.write(StringUtils.padLeft(formatter.format(shiftArray[1]), formatString.length() + 3));
    writer.write(StringUtils.padLeft(formatter.format(shiftArray[2]), formatString.length() + 3));
    this.newLine(writer);
  }

  private void writeBandStructure(Writer writer) throws IOException {
    //TODO
  }
  
  private void newLine(Writer writer) throws IOException {
    writer.write("\n");
  }


}
