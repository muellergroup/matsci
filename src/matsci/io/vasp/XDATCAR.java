/*
 * Created on Nov 17, 2004
 *
 */
package matsci.io.vasp;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;

import matsci.location.basis.AbstractBasis;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class XDATCAR {
  
  private String m_FileName;
  private AbstractBasis m_Basis;
  
  // The inidices are first the ion, then the iteration, then the coordinate
  private double[][][] m_Coordinates = new double[0][0][0];
  private int m_NumIterations = 0;
  private int m_NumIons =0;
  
  public XDATCAR(String fileName, AbstractBasis basis) {
    
    m_FileName = fileName;
    m_Basis = basis;
    this.readFromFile();
  }
  
  public void readFromFile() {
    
    try {      
	    sizeArrays();
	    FileReader fileReader = new FileReader(m_FileName);
	    LineNumberReader lineReader = new LineNumberReader(fileReader);
	    readHeader(lineReader);
	    readCoordinates(lineReader);
    } catch (IOException e){
      throw new RuntimeException("Error reading file " + m_FileName, e);
    }
    
  }
  
  protected void readHeader(LineNumberReader lineReader) throws IOException {
    //TODO Figure out what all this header stuff means
    
    StringTokenizer st = new StringTokenizer(lineReader.readLine());
    m_NumIons = Integer.parseInt(st.nextToken());
    
    for (int i = 0; i < 4; i++) {
      lineReader.readLine();
    }
  }
  
  protected void sizeArrays() throws IOException {
    
    FileReader fileReader = new FileReader(m_FileName);
    LineNumberReader lineReader = new LineNumberReader(fileReader);
    readHeader(lineReader);
    String line = lineReader.readLine();
    
    int numLines = 0;
    while (line != null) {
      if (line.trim().length() != 0){
        numLines++;
      }
      line = lineReader.readLine();
    }
    m_NumIterations =numLines / m_NumIons;
    m_Coordinates = new double[m_NumIons][m_NumIterations][3];
  }
  
  protected void readCoordinates(LineNumberReader lineReader) throws IOException {
    
    String line = lineReader.readLine();
    
    // line == null means end of file
    // line.trim().length == 0 means whitespace and next iteration
    for (int iterationNumber = 0; iterationNumber < m_NumIterations; iterationNumber++) {
      for (int ionNumber = 0; ionNumber < m_NumIons; ionNumber++) {
        while (line.trim().length() == 0) {
          line = lineReader.readLine();
        }
        StringTokenizer st = new StringTokenizer(line);
        for (int coordNum = 0; coordNum < 3; coordNum++) {
          m_Coordinates[ionNumber][iterationNumber][coordNum] = Double.parseDouble(st.nextToken());
        }
        line = lineReader.readLine();
      }
    }
  }
  
  public String getFileName() {
    return m_FileName;
  }
  
  public AbstractBasis getBasis() {
    return m_Basis;
  }
  
  public int numIons() {
    return m_Coordinates.length;
  }
  
  public int numIterations() {
    return m_NumIterations;
  }
  
  public double[][] getCoordsForIon(int ionNumber) {
    return ArrayUtils.copyArray(m_Coordinates[ionNumber]);
  }

}
