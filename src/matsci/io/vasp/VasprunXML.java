/*
 * Created on Oct 6, 2005
 *
 */
package matsci.io.vasp;

import java.io.*;
import java.util.StringTokenizer;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import matsci.Species;
import matsci.location.*;
import matsci.location.basis.*;
import matsci.structure.*;

public class VasprunXML {

  private Document m_Document;
  private NodeList m_IonicIterations;
  private AbstractBasis[] m_DirectBases; // Listed by iteration number
  private Species[] m_Species; // Listed by site number
  
  public VasprunXML(String fileName) {
    
    File newFile = new File(fileName);
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    
    try {
      DocumentBuilder parser = factory.newDocumentBuilder();
      m_Document = parser.parse(newFile);
    } catch (Exception e) {
      throw new RuntimeException("Error reading XML document: ", e);
    }
    
    m_IonicIterations = m_Document.getElementsByTagName("calculation");
    m_DirectBases = new AbstractBasis[m_IonicIterations.getLength()];
    
    Element atomInfo = (Element) m_Document.getElementsByTagName("atominfo").item(0);
    NodeList arrays = atomInfo.getElementsByTagName("array");
    for (int arrayNum = 0; arrayNum < arrays.getLength(); arrayNum++) {
      Element array = (Element) arrays.item(arrayNum);
      if (array.hasAttribute("name") && array.getAttribute("name").equals("atoms")) {
        String[][] arrayInfo = this.readArray(array);
        m_Species = new Species[arrayInfo.length];
        for (int atomNum = 0; atomNum < m_Species.length; atomNum++) {
          m_Species[atomNum] = Species.get(arrayInfo[atomNum][0].trim());
        }
      }
    }
  }
  
  protected String[][] readArray(Element array) {
    NodeList rows = array.getElementsByTagName("rc");
    String[][] returnArray = new String[rows.getLength()][];
    for (int rowNum = 0; rowNum < rows.getLength(); rowNum++) {
      Element row = (Element) rows.item(rowNum);
      NodeList entries = row.getElementsByTagName("c");
      returnArray[rowNum] = new String[entries.getLength()];
      for (int colNum = 0; colNum < entries.getLength(); colNum++) {
        Node entry = entries.item(colNum);
        returnArray[rowNum][colNum] = this.getNodeValue(entry);
      }                           
    }
    return returnArray;
  }
  
  protected String[][] readVArray(Element vArray) {
    NodeList rows = vArray.getElementsByTagName("v");
    String[][] returnArray = new String[rows.getLength()][];
    for (int rowNum = 0; rowNum < rows.getLength(); rowNum++) {
      Node row = rows.item(rowNum);
      StringTokenizer st = new StringTokenizer(this.getNodeValue(row));
      returnArray[rowNum] = new String[st.countTokens()];
      for (int colNum = 0; colNum < returnArray[rowNum].length; colNum++) {
        returnArray[rowNum][colNum] = st.nextToken();
      }
    }
    return returnArray;
  }
  
  protected Coordinates[] readSiteCoordinates(Element structure, AbstractBasis directBasis) {
    NodeList varrays = structure.getElementsByTagName("varray");
    for (int arrayNum = 0; arrayNum < varrays.getLength(); arrayNum++) {
      Element positions = (Element) varrays.item(arrayNum);
      if (positions.hasAttribute("name") && positions.getAttribute("name").equals("positions")) {
        String[][] entries = this.readVArray(positions);
        Coordinates[] returnArray = new Coordinates[entries.length];
        for (int rowNum= 0; rowNum < entries.length; rowNum++) {
          double[] coordArray = new double[entries[rowNum].length];
          for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
            coordArray[dimNum] = Double.parseDouble(entries[rowNum][dimNum]);
          }
          returnArray[rowNum] = new Coordinates(coordArray, directBasis);
        }
        return returnArray;
      }
    }
    return null;
  }
  
  protected StructureBuilder readStructure(Element structure) {
    StructureBuilder returnBuilder = new StructureBuilder();
    returnBuilder.setDescription(getDescription());
    Vector[] cellVectors = this.readCellVectors(structure);
    returnBuilder.setCellVectors(cellVectors);
    LinearBasis basis = new LinearBasis(cellVectors);
    Coordinates[] siteCoords = this.readSiteCoordinates(structure, basis);
    for (int siteNum = 0; siteNum < siteCoords.length; siteNum++) {
      returnBuilder.addSite(siteCoords[siteNum], this.getSiteSpecies(siteNum));
    }
    return returnBuilder;
  }
  
  protected Vector[] readCellVectors(Element structure) {
    NodeList crystals = structure.getElementsByTagName("crystal");
    Element crystal = (Element) crystals.item(0);
    NodeList varrays = crystal.getElementsByTagName("varray");
    for (int arrayNum = 0; arrayNum < varrays.getLength(); arrayNum++) {
      Element basis = (Element) varrays.item(arrayNum);
      if (basis.hasAttribute("name") && basis.getAttribute("name").equals("basis")) {
        NodeList vEntries = basis.getElementsByTagName("v");
        Vector[] returnArray = new Vector[vEntries.getLength()];
        for (int entryNum = 0; entryNum < returnArray.length; entryNum++) {
          Element entry = (Element) vEntries.item(entryNum);
          StringTokenizer st = new StringTokenizer(this.getNodeValue(entry));
          double[] vecArray = new double[st.countTokens()];
          for (int dimNum = 0; dimNum < vecArray.length; dimNum++) {
            vecArray[dimNum] = Double.parseDouble(st.nextToken());
          }
          returnArray[entryNum] = new Vector(vecArray, CartesianBasis.getInstance());
        }
        return returnArray;
      }
    }
    return null;
  }
  
  public int numIonicSteps() {
    return m_IonicIterations.getLength();
  }
  
  public int numDefiningSites() {
    return m_Species.length;
  }
  
  protected String getNodeValue(Node node) {
    return node.getChildNodes().item(0).getNodeValue();
  }
  
  public StructureBuilder getStructure(int iterationNum) {

    StructureBuilder builder = new StructureBuilder();
    builder.setDescription(this.getDescription());
    builder.setCellVectors(this.getCellVectors(iterationNum));
    int numDefiningSites = this.numDefiningSites();
    Coordinates[] siteCoordinates =this.getSiteCoordinates(iterationNum);
    for (int siteNum = 0; siteNum < numDefiningSites; siteNum++) {
      builder.addSite(siteCoordinates[siteNum], m_Species[siteNum]);
    }
    
    return builder;
  }
  
  public double getEnergy(int iterationNum) {
    Element iteration = (Element) m_IonicIterations.item(iterationNum);
    NodeList energies = iteration.getElementsByTagName("energy");
    Element finalEnergy = (Element) energies.item(energies.getLength() - 1);
    NodeList entries = finalEnergy.getElementsByTagName("i");
    for (int i = entries.getLength() -1; i >= 0; i--) { 
      Element entry = (Element) entries.item(i);
      if (entry.hasAttribute("name") && entry.getAttribute("name").equals("e_wo_entrp")) {
        String value = this.getNodeValue(entry);
        return Double.parseDouble(value.trim());
      }
    }
    throw new RuntimeException("Couldn't find energy for iteration " + iterationNum);
  }
  
  public Coordinates[] getSiteCoordinates(int iterationNum) {
    AbstractBasis basis = this.getDirectBasis(iterationNum);
    Element structureNode = (Element) m_IonicIterations.item(iterationNum);
    return this.readSiteCoordinates(structureNode, basis);
  }
  
  public Vector[] getForces(int iterationNum) {
    Coordinates[] siteCoords = this.getSiteCoordinates(iterationNum);
    Element structureNode = (Element) m_IonicIterations.item(iterationNum);
    NodeList varrays = structureNode.getElementsByTagName("varray");
    for (int arrayNum = 0; arrayNum < varrays.getLength(); arrayNum++) {
      Element positions = (Element) varrays.item(arrayNum);
      if (positions.hasAttribute("name") && positions.getAttribute("name").equals("forces")) {
        String[][] entries = this.readVArray(positions);
        Vector[] returnArray = new Vector[entries.length];
        for (int rowNum = 0; rowNum < entries.length; rowNum++) {
          double[] forceArray = new double[entries[rowNum].length];
          for (int colNum = 0; colNum < forceArray.length; colNum++) {
            forceArray[colNum] =  Double.parseDouble(entries[rowNum][colNum]);
          }
          returnArray[rowNum] = new Vector(siteCoords[rowNum], forceArray, CartesianBasis.getInstance());
        }
        return returnArray;
      }
    }
    return null;
  }
  
  public AbstractBasis getDirectBasis(int iterationNum) {
    if (m_DirectBases[iterationNum] == null) {
      Vector[] latticeVectors = this.getCellVectors(iterationNum);
      m_DirectBases[iterationNum] = new LinearBasis(latticeVectors);
    }
    return m_DirectBases[iterationNum];
  }
  
  public Species getSiteSpecies(int siteNum) {
    return m_Species[siteNum];
  }
  
  public String getDescription() {
    NodeList separators = m_Document.getElementsByTagName("separator");
    for (int i = 0; i < separators.getLength(); i++) {
      Element separator = (Element) separators.item(i);
      if (separator.hasAttribute("name") && separator.getAttribute("name").equals("general")) {
        NodeList children = separator.getChildNodes();
        for (int childNum = 0; childNum < children.getLength(); childNum++) {
          Node child = children.item(childNum);
          if (!child.hasAttributes()) {continue;}
          NamedNodeMap attributes = child.getAttributes();
          Node name = attributes.getNamedItem("name");
          if (name == null) {continue;}
          if (name.getNodeValue().equals("SYSTEM")) {
            return this.getNodeValue(child);
          }
        }
      }
    }
    return "Unknown system";
  }
  
  public Vector[] getCellVectors(int iterationNum) {
    Element structureNode = (Element) m_IonicIterations.item(iterationNum);
    return this.readCellVectors(structureNode);
  }
  
  public double getInitialEnergy() {
    return this.getEnergy(0);
  }
  
  public double getFinalEnergy() {
    return this.getEnergy(numIonicSteps() - 1);
  }
  
  public StructureBuilder getInitialStructure() {
    NodeList structures = m_Document.getElementsByTagName("structure");
    for (int structNum = 0; structNum < structures.getLength(); structNum++) {
      Element structure = (Element) structures.item(structNum);
      if (structure.hasAttribute("name") && structure.getAttribute("name").equals("initialpos")) {
        return this.readStructure(structure);
      }
    }
    
    throw new RuntimeException("Couldn't find initial structure in vasprun!");
  }
  
  public StructureBuilder getFinalStructure() {
    NodeList structures = m_Document.getElementsByTagName("structure");
    for (int structNum = 0; structNum < structures.getLength(); structNum++) {
      Element structure = (Element) structures.item(structNum);
      if (structure.hasAttribute("name") && structure.getAttribute("name").equals("finalpos")) {
        return this.readStructure(structure);
      }
    }
    
    throw new RuntimeException("Couldn't find final structure in vasprun!");
  }

}
