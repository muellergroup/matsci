package matsci.io.vasp;


import java.io.Reader;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;

import matsci.Species;
import matsci.io.structure.IStructureFile;
import matsci.structure.IStructureData;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class POSCAR extends VASPCell implements IStructureFile {

  public POSCAR(IStructureData data, Species[] speciesInOrder) {
    super(data, speciesInOrder);
  }
  
  public POSCAR(IStructureData data) {
    super(data);
  }
  
  public POSCAR(IStructureData data, boolean alphabetizeSpecies) {
    super(data, alphabetizeSpecies);
  }
  
  public POSCAR(String fileName, Species[] speciesInOrder) {

    super(fileName, speciesInOrder);

    try {
      readFile(fileName);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public POSCAR(Reader reader) {
    this(reader, false);
  }
  
  public POSCAR(String fileName) {
    this(fileName, false);
  }
  
  public POSCAR(Reader reader, boolean allowUnknownSpecies) {
    super(allowUnknownSpecies);
    try {
      readFile(reader);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading from reader " + reader, e);
    }
  }
  
  public POSCAR(Reader reader, boolean allowUnknownSpecies, boolean readSpeciesFromHeader) {
    super(allowUnknownSpecies, readSpeciesFromHeader);
    try {
      readFile(reader);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading from reader " + reader, e);
    }
  }
  
  public POSCAR(String fileName, boolean allowUnknownSpecies) {
    super(fileName, allowUnknownSpecies);
    try {
      readFile(fileName);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public POSCAR(String fileName, boolean allowUnknownSpecies, boolean readSpeciesFromHeader) {
    super(fileName, allowUnknownSpecies, readSpeciesFromHeader);
    try {
      readFile(fileName);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public void writeFile(String fileName) {
    
    try {
      FileWriter fileWriter = new FileWriter(fileName);
      BufferedWriter writer = new BufferedWriter(fileWriter);
      this.write(writer);
      writer.flush();
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException("Failed to write file " + fileName, e);
    } 
    
  }
  
  /*public void writeFile(String fileName) {
    
    try {
      FileWriter fileWriter = new FileWriter(fileName);
      BufferedWriter writer = new BufferedWriter(fileWriter);
      this.writeComment(writer);
      this.writeScaleFactor(writer);
      this.writeLatticeVectors(writer);
      this.writeSitesPerSpecies(writer);
      this.writeSelectiveDynamics(writer);
      this.writeBasis(writer);
      this.writeSites(writer);
      writer.flush();
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException("Failed to write POSCAR to file " + fileName, e);
    } 
    
  }*/
  
  public void write(Writer writer) {
    
    try {
      this.writeComment(writer);
      this.writeScaleFactor(writer);
      this.writeLatticeVectors(writer);
      this.writeSitesPerSpecies(writer);
      this.writeSelectiveDynamics(writer);
      this.writeBasis(writer);
      this.writeSites(writer);
      writer.flush();
    } catch (IOException e) {
      throw new RuntimeException("Failed to write POSCAR! ", e);
    }
    
  }


  protected void readFile(String fileName) throws IOException {

    FileReader fileReader = new FileReader(fileName);
    this.readFile(fileReader);
    fileReader.close();
  }
  
  protected void readFile(Reader reader) throws IOException {

    LineNumberReader lineReader = new LineNumberReader(reader);

    readComment(lineReader);
    readScaleFactor(lineReader);
    readLatticeVectors(lineReader);
    readSpeciesInOrder(lineReader);
    readSitesPerSpecies(lineReader);
    readSelectiveDynamics(lineReader);
    readBasis(lineReader);
    
    readSites(lineReader);
  }

  public IStructureFile create(IStructureData structureData) {
    return new POSCAR(structureData);
  }

}
