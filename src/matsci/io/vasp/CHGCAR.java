package matsci.io.vasp;


import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;

import matsci.Species;
import matsci.location.*;
import matsci.location.basis.*;
import matsci.structure.*;
import matsci.util.arrays.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class CHGCAR extends VASPCell {

  protected int[] m_GridDimensions;
  protected double[][][] m_DensityTot;
  protected double[][][] m_DensityDiff;
  protected double[][] m_AugmentationOccupancies;
  protected boolean m_SpinPolarized = true;
  
  public CHGCAR(IStructureData structure, int[] gridDimensions, double[] densityTot) {
    super(structure);

    ArrayIndexer indexer = new ArrayIndexer(gridDimensions);
    if (indexer.numAllowedStates() != densityTot.length) {
      throw new RuntimeException("ScalarGrid dimensions not compatabile with number of data points for CHGCAR!");
    }
    m_GridDimensions = ArrayUtils.copyArray(gridDimensions);
    m_DensityTot = new double[gridDimensions[0]][gridDimensions[1]][gridDimensions[2]];
    
    // Check to see if the z direction was reversed for the fixed triple product
    Vector origZVec = structure.getCellVectors()[2];
    Vector newZVec = this.getCellVectors()[2];
    boolean reverseZDirection = (origZVec.add(newZVec).length() < CartesianBasis.getPrecision());
    
    int[] gridPoint = new int[3];
    for (int xDim = 0; xDim < m_DensityTot.length; xDim++) {
      gridPoint[0] = xDim;
      double[][] plane = m_DensityTot[xDim];
      for (int yDim = 0; yDim < plane.length; yDim++) {
        gridPoint[1] = yDim;
        double[] row = plane[yDim];
        for (int zDim = 0; zDim < row.length; zDim++) {
          gridPoint[2] = reverseZDirection? ((row.length - zDim) % row.length) : zDim;
          row[zDim] = densityTot[indexer.joinValuesBounded(gridPoint)];
        }
      }
    }
  }
  
  public CHGCAR(IStructureData structure, double[][][] densityTot) {
    super(structure);
    m_DensityTot = ArrayUtils.copyArray(densityTot);
    int xDim = densityTot.length;
    int yDim = densityTot[0].length;
    int zDim = densityTot[0][0].length;
    m_GridDimensions = new int[] {xDim, yDim, zDim};
  }
  
  public CHGCAR(IStructureData data, double[][][] densityTot, boolean alphabetizeSpecies) {
    super(data, alphabetizeSpecies);
    m_DensityTot = ArrayUtils.copyArray(densityTot);
    int xDim = densityTot.length;
    int yDim = densityTot[0].length;
    int zDim = densityTot[0][0].length;
    m_GridDimensions = new int[] {xDim, yDim, zDim};
  }

  public CHGCAR(String fileName, Species[] speciesInOrder) {

    super(fileName, speciesInOrder);
    
    try {
      readFile(fileName);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading file " + fileName);
    }
  }
  
  public CHGCAR(String fileName, boolean allowUnknownSpecies) {
    super(fileName, allowUnknownSpecies);
    try {
      readFile(fileName);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading file " + fileName);
    }
  }

  protected void readFile(String fileName) throws IOException {

    FileReader fileReader = new FileReader(fileName);
    LineNumberReader lineReader = new LineNumberReader(fileReader);

    readComment(lineReader);
    readScaleFactor(lineReader);
    readLatticeVectors(lineReader);
    readSitesPerSpecies(lineReader);
    readBasis(lineReader);
    readSites(lineReader);

    readGridDimensions(lineReader);
    readDensityTot(lineReader);

    // Check to see if this was a spin polarized calculation.  If so, we have spin up - spin down charge too.
    try {
      readAugmentationOccupancies(lineReader);
      readGridDimensions(lineReader);
    } catch (Exception e) {
      m_SpinPolarized = false;
    }

    if (m_SpinPolarized) {
      readDensityDiff(lineReader);
    }
    fileReader.close();
  }
  
  protected boolean readAugmentationOccupancies(LineNumberReader reader) throws IOException {
    
    reader.mark(40000);
    StringTokenizer st = new StringTokenizer(reader.readLine());
    while (st.countTokens() == 0) {
      st = new StringTokenizer(reader.readLine());
    }
    
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      String firstToken = st.nextToken();
      if (!firstToken.equals("augmentation")) {
        reader.reset();
        return false;
      }
      String occupancies = st.nextToken();
      String siteNumString = st.nextToken();
      int numValues = Integer.parseInt(st.nextToken());
      m_AugmentationOccupancies[siteNum] = new double[numValues];
      for (int valNum = 0; valNum < numValues; valNum++) {
        if (!st.hasMoreElements()) {
          st = new StringTokenizer(reader.readLine());
        }
        m_AugmentationOccupancies[siteNum][valNum] = Double.parseDouble(st.nextToken());
      }
      st = new StringTokenizer(reader.readLine());
    }
    return true;
  }

  protected void readGridDimensions(LineNumberReader reader) throws IOException {

    StringTokenizer st = new StringTokenizer(reader.readLine());
    while (st.countTokens() != 3) {
      st = new StringTokenizer(reader.readLine());
    }
    
    m_GridDimensions = new int[3];
    m_GridDimensions[0] = Integer.parseInt(st.nextToken());
    m_GridDimensions[1] = Integer.parseInt(st.nextToken());
    m_GridDimensions[2] = Integer.parseInt(st.nextToken());
  }

  protected void readDensityTot(LineNumberReader reader) throws IOException {

    m_DensityTot = new double[m_GridDimensions[0]][m_GridDimensions[1]][m_GridDimensions[2]];
    StringTokenizer st = new StringTokenizer(reader.readLine());
    
    for (int z = 0; z < m_GridDimensions[2]; z++) {
      for (int y = 0; y < m_GridDimensions[1]; y++) {
        for (int x = 0; x < m_GridDimensions[0]; x++) {
          while (!st.hasMoreElements()) {
            st = new StringTokenizer(reader.readLine());
          }
          m_DensityTot[x][y][z] = Double.parseDouble(st.nextToken());
        }
      }
    }

  }
  
  protected void readDensityDiff(LineNumberReader reader) throws IOException {

    m_DensityDiff = new double[m_GridDimensions[0]][m_GridDimensions[1]][m_GridDimensions[2]];
    StringTokenizer st = new StringTokenizer(reader.readLine());
    
    for (int z = 0; z < m_GridDimensions[2]; z++) {
      for (int y = 0; y < m_GridDimensions[1]; y++) {
        for (int x = 0; x < m_GridDimensions[0]; x++) {
          while (!st.hasMoreElements()) {
            st = new StringTokenizer(reader.readLine());
          }
          m_DensityDiff[x][y][z] = Double.parseDouble(st.nextToken());
        }
      }
    }

  }
  
  public void writeTotalCHGCAR(String fileName) {

    try {
      FileWriter fileWriter = new FileWriter(fileName);
      BufferedWriter writer = new BufferedWriter(fileWriter);
      writeCHGCARHeader(writer);
      writeCHG(writer, m_DensityTot);
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Failed to write chgcar", e);
    }

  }

  protected void writeCHGCARHeader(BufferedWriter writer) throws IOException {

    this.writeComment(writer);
    this.writeScaleFactor(writer);
    this.writeLatticeVectors(writer);
    this.writeSitesPerSpecies(writer);
    this.writeSelectiveDynamics(writer);
    this.writeBasis(writer);
    this.writeSites(writer);
    writer.flush();

  }

  protected void writeCHG(BufferedWriter writer, double[][][] values) throws IOException {

    writer.newLine();
    for (int dim =0; dim < m_GridDimensions.length; dim++) {
      writer.write("  ");
      writer.write(new Integer(m_GridDimensions[dim]).toString());
    }
    writer.newLine();
    for (int z = 0; z < m_GridDimensions[2]; z++) {
      for (int y = 0; y < m_GridDimensions[1]; y++) {
        for (int x = 0; x < m_GridDimensions[0]; x++) {
          writer.write(" ");
          writer.write(new Double(values[x][y][z]).toString());
        }
      }
    }
    writer.newLine();
    writer.flush();
  }

  public int[] getGridDimensions() {
    return ArrayUtils.copyArray(m_GridDimensions);
  }
  
  public int numGridPoints() {
    int returnValue = 1;
    for (int dim = 0; dim < m_GridDimensions.length; dim++) {
      returnValue *= m_GridDimensions[dim];
    }
    return returnValue;
  }

  public double getDensity(int x, int y, int z) {
    return m_DensityTot[x][y][z];
  }

  public double getDensityDiff(int x, int y, int z) {
    return m_DensityDiff[x][y][z];
  }

  public boolean isSpinPolarized() {
    return m_SpinPolarized;
  }
  
  public void setGridDimensions(int x, int y, int z) {
    m_GridDimensions = new int[] {x,y,z};
    clearDensity();
    clearDensityDiff();
  }
  
  public void clearDensity() {
    m_DensityTot = new double[m_GridDimensions[0]][m_GridDimensions[1]][m_GridDimensions[2]];
  }
  
  public void clearDensityDiff() {
    m_DensityDiff = new double[m_GridDimensions[0]][m_GridDimensions[1]][m_GridDimensions[2]];
  }
  
  public void setDensity(int x, int y, int z, double value) {
    m_DensityTot[x][y][z] = value;
  }
  
  public void incrementDensity(int x, int y, int z, double value) {
    m_DensityTot[x][y][z] += value;
  }
  
  public void setDensityDiff(int x, int y, int z, double value) {
    m_DensityDiff[x][y][z] = value;
  }
  
  public void incrementDensityDiff(int x, int y, int z, double value) {
    m_DensityDiff[x][y][z] += value;
  }
  
  public Coordinates getCoordinates(int x, int y, int z) {
    
    double[] directArray = new double[3];
    directArray[0] = ((double) x) / m_GridDimensions[0];
    directArray[1] = ((double) y) / m_GridDimensions[1];
    directArray[2] = ((double) z) / m_GridDimensions[2];
    
    return new Coordinates(directArray, this.getDirectBasis());
  }
  
  public int[] getNearbyGridPoint(Coordinates coordinates) {
    
    double[] directArray = coordinates.getCoordArray(m_DirectBasis);
    int[] returnArray = new int[directArray.length];
    for (int dimNum = 0; dimNum < directArray.length; dimNum++) {
      double value = directArray[dimNum] * m_GridDimensions[dimNum];
      returnArray[dimNum] = ((int) Math.round(value)) % m_GridDimensions[dimNum];
    }
    
    return returnArray;
  }
}
