package matsci.io.vasp;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

import matsci.Element;
import matsci.Species;
import matsci.io.structure.IStructureFile;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.IStructureData;
import matsci.structure.Structure;
import matsci.util.MSMath;
import matsci.util.StringUtils;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public abstract class VASPCell implements IStructureData {

  protected String m_FileName;
  protected String m_Comment;
  protected double m_ScaleFactor = 1; // A safe default
  protected Vector[] m_CellVectors; // These vectors define the lattice.
  protected boolean[] m_IsVectorPeriodic; // This is for consistency with structure formats that allow for non-periodic dimensions
  protected Coordinates[] m_SiteCoords;
  protected int[] m_SitesPerSpecies;
  protected boolean[][] m_SelectiveDynamics;
  protected boolean m_UseSelectiveDynamics;
  protected LinearBasis m_DirectBasis;
  protected boolean m_FormatWithDirect = true; // Default to direct b/c VENUS chokes on Cartesian
  protected Species[] m_Species = new Species[0];
  
  private double m_FormatPrecision = 10E-15;
  private boolean m_AllowUnknownSpecies = false;
  private boolean m_WriteSpeciesWithDescription = true;
  private boolean m_WriteElementSymbolsOnly = true;
  private boolean m_FixTripleProduct = true; 
  private boolean m_UseVASP5Format = true; // Changed to default of true on July 24, 2014
  private boolean m_ReadSpeciesFromHeader = true; // Added on April 12, 2017
  
  public VASPCell(IStructureData data, Species[] speciesInOrder) {

    m_Species = (Species[]) ArrayUtils.copyArray(speciesInOrder);
    this.mergeData(data, false);
    
  }
  
  public VASPCell(IStructureData data, boolean alphabetizeSpecies) {

    this.mergeData(data, alphabetizeSpecies);
    
  }

  public VASPCell(IStructureData data) {

    this.mergeData(data, false);
    
  }

  public void mergeData(IStructureData data, boolean alphabetize) {
    
    this.setDescription(data.getDescription());
    this.readComment(data);
    this.readLatticeVectors(data);
    this.readSitesPerSpecies(data, alphabetize);
    this.readSites(data);
    
  }
  
  public VASPCell(String fileName, Species[] speciesInOrder) {
    m_Species = (Species[]) ArrayUtils.copyArray(speciesInOrder);
    m_FileName = fileName;
  }
  
  public VASPCell(String fileName, boolean allowUnknownSpecies, boolean readSpeciesFromHeader) {
    this(fileName, allowUnknownSpecies);
    m_ReadSpeciesFromHeader = readSpeciesFromHeader;
  }
  
  public VASPCell(String fileName, boolean allowUnknownSpecies) {
    m_AllowUnknownSpecies = allowUnknownSpecies;
    m_FileName = fileName;
  }
  
  public VASPCell(boolean allowUnknownSpecies) {
    m_AllowUnknownSpecies = allowUnknownSpecies;
  }
  
  public VASPCell(boolean allowUnknownSpecies, boolean readSpeciesFromHeader) {
    m_AllowUnknownSpecies = allowUnknownSpecies;
    m_ReadSpeciesFromHeader = readSpeciesFromHeader;
  }
  
  public void setSpecies(int specNum, Species species) {
    if (specNum < 0) {return;}
    if (specNum >= m_Species.length) {return;}
    m_Species[specNum] = species;
  }
  
  public void labelUnknownSpecies() {
    Species[] speciesSet = this.getSpeciesSet();
    for (int specNum = 0; specNum < speciesSet.length; specNum++) {
      if (speciesSet[specNum] == null) {
        //this.setSpecies(specNum, Species.get("POSCAR_UNKNOWN_SPECIES_" + specNum, -1, 0));  This causes problems re-reading the file -- it things specNum is an oxidation state
        this.setSpecies(specNum, Species.get("POSCAR_SPECIES_" + specNum + "_UNKNOWN", -1, 0)); 
      }
    }
  }
  
  public void writeVICSOutFile(String fileName) {
    
    try {
      FileWriter fileWriter = new FileWriter(fileName);
      BufferedWriter writer = new BufferedWriter(fileWriter);
      for (int specNum = 0; specNum < m_Species.length; specNum++) {
        
        Species spec = m_Species[specNum];
        if (spec == Species.vacancy) {
          continue;
        }
        if (m_WriteElementSymbolsOnly && (specNum > 0) && m_Species[specNum - 1].getElement() == spec.getElement()) {
          continue;
        }
        String specSymbol = this.getSpecSymbol(spec);
        writer.write(" POTCAR:   PAW_PBE " + specSymbol);
        this.newLine(writer);
      }
      writer.flush();
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException("Failed to write VICS out to file " + fileName, e);
    }
    
  }
  
  protected String getSpecSymbol(Species spec) {
    return m_WriteElementSymbolsOnly ? spec.getElementSymbol() : spec.toString();
  }
  
  

  protected void readComment(LineNumberReader lineReader) throws IOException {
    String comment = readLineTrimmed(lineReader);
    
    // Try to read the species from the comment.  This overrides whatever species the user has already set
    if (m_ReadSpeciesFromHeader) {
      int startIndex = comment.indexOf("<s>");
      if (startIndex >= 0) {
        int endIndex = comment.indexOf("</s>", startIndex);
        if (endIndex >= 0) {
          String specString = comment.substring(startIndex + 3, endIndex);
          StringTokenizer st = new StringTokenizer(specString);
          m_Species = new Species[st.countTokens()];
          for (int specNum = 0; specNum < m_Species.length; specNum++) {
            m_Species[specNum] = Species.get(st.nextToken());
          }
          comment = comment.substring(0, startIndex) + comment.substring(endIndex + 4, comment.length());
        }
      }
    }
    m_Comment = comment;
  }
  
  protected void readComment(IStructureData data) {
    m_Comment = data.getDescription();
  }

  protected void readScaleFactor(LineNumberReader lineReader) throws IOException {
    String line = readLineTrimmed(lineReader);
    m_ScaleFactor = Double.parseDouble(line);
  }

  protected void readLatticeVectors(LineNumberReader lineReader) throws IOException {
    String thisLine;

    m_CellVectors = new Vector[3];
    m_IsVectorPeriodic = new boolean[3];

    //Read the lattice vectors and build the matrix for converting from direct to cartesian coordinates
    double[][] cartCoords = new double[3][];
    for (int vecNum = 0; vecNum < m_CellVectors.length; vecNum++) {
      double[] coordArray = new double[3];
      thisLine = readLineTrimmed(lineReader);
      StringTokenizer st = new StringTokenizer(thisLine);
      for (int col = 0; col < 3; col++) {
        //coordArray[col] = Double.parseDouble(st.nextToken()) * m_ScaleFactor;
        coordArray[col] = Double.parseDouble(st.nextToken());
      }
      cartCoords[vecNum] = coordArray;
      m_CellVectors[vecNum] = new Vector(coordArray, CartesianBasis.getInstance());
      m_IsVectorPeriodic[vecNum] = true;
    }
    
    // A negative scale factor is the target volume
    double scaleFactor = m_ScaleFactor;
    if (scaleFactor < 0) {
      double volume = Math.abs(MSMath.determinant(cartCoords));
      scaleFactor = Math.cbrt(-scaleFactor / volume);
    }
    for (int vecNum = 0; vecNum < m_CellVectors.length; vecNum++) {
      m_CellVectors[vecNum] = m_CellVectors[vecNum].multiply(scaleFactor);
    }
    
    // Create the appropriate direct basis for these vectors
    m_DirectBasis = new LinearBasis(m_CellVectors);
  }
  
  protected void readLatticeVectors(IStructureData data ) {
    
    /*Vector[] latticeVectors = data.getCellVectors();
    Vector[] nonPeriodicVectors = data.getNonPeriodicVectors();
    Vector[] givenVectors = new Vector[latticeVectors.length + nonPeriodicVectors.length];
    m_IsVectorPeriodic = new boolean[3];
    for (int vecNum = 0; vecNum < givenVectors.length; vecNum++) {
      if (vecNum < latticeVectors.length) {
        givenVectors[vecNum] = latticeVectors[vecNum];
        m_IsVectorPeriodic[vecNum] = true;
      } else {
        givenVectors[vecNum] = nonPeriodicVectors[vecNum  - latticeVectors.length];
      }
    }
    
    m_CellVectors = LinearBasis.fill3DBasis(givenVectors);*/
    m_CellVectors = data.getCellVectors();
    m_IsVectorPeriodic = data.getVectorPeriodicity();
    if (m_FixTripleProduct) {
      fixTripleProduct(m_CellVectors);
    }
    m_DirectBasis = new LinearBasis(m_CellVectors);
  }
  
  /**
   * For some reason, VASP insists that the triple product of the lattice vectors is positive.
   * This ensures that it is.
   *
   */
  protected static void fixTripleProduct(Vector[] cellVectors) {
    
    double tripleProduct = cellVectors[0].tripleProductCartesian(cellVectors[1], cellVectors[2]);
    if (tripleProduct < 0) {
      /*Vector tempVector = m_CellVectors[0];
      boolean tempBool = m_IsVectorPeriodic[0];
      m_CellVectors[0] = m_CellVectors[2];
      m_IsVectorPeriodic[0] = m_IsVectorPeriodic[2];
      m_CellVectors[2] = tempVector;
      m_IsVectorPeriodic[2] = tempBool;*/
      cellVectors[2] = cellVectors[2].multiply(-1);
    }
    
  }
  
  /**
   * For VASP 5 formatting
   * 
   * @param lineReader
   * @throws IOException
   */
  protected void readSpeciesInOrder(LineNumberReader lineReader) throws IOException {
    
    lineReader.mark(500);
    
    String line = readLineTrimmed(lineReader);
    if (Character.isLetter(line.charAt(0))) {
      StringTokenizer st = new StringTokenizer(line);
      m_Species = new Species[st.countTokens()];
      for (int specNum = 0; specNum < m_Species.length; specNum++) {
        m_Species[specNum] = Species.get(st.nextToken());
      }
      return;
    } else {
      lineReader.reset();
    }
    
  }

  protected void readSitesPerSpecies(LineNumberReader lineReader) throws IOException {
    
    // Get the number of sites and species and use this to intialize the appropriate arrays
    String thisLine = readLineTrimmed(lineReader);
    StringTokenizer st = new StringTokenizer(thisLine);
    int numSpecies = st.countTokens();
    if (!m_AllowUnknownSpecies && (m_Species.length != numSpecies)) {
      if (m_Species.length == 0) {
        throw new RuntimeException("Species not specified in VASP file.  Consider allowing unknown species.");
      }
      throw new RuntimeException("Wrong number of species specified for VASP file.");
    }
    m_SitesPerSpecies = new int[numSpecies];
    for (int speciesNum = 0; speciesNum < numSpecies; speciesNum++) {
      m_SitesPerSpecies[speciesNum] = Integer.parseInt(st.nextToken());
    }

    // Pad the species array if it's too short
    if (m_Species.length < numSpecies) {
      m_Species = (Species[]) ArrayUtils.growArray(m_Species, numSpecies - m_Species.length);
    }
    
    // Truncate the species array if it's too long
    if (m_Species.length > numSpecies) {
      m_Species = (Species[]) ArrayUtils.truncateArray(m_Species,  numSpecies);
    }
  }
  
  protected void readSitesPerSpecies(IStructureData data, boolean alphabetize) {
    
    // We reset the species counts, but not the known species
    //int[] sitesPerSpecies = new int[m_Species.length];
    m_SitesPerSpecies = new int[m_Species.length];
    //Specie[] species = m_Species; // This ensures we keep known species in the same order
    
    // First record the species as they appear in order
    for (int siteNum = 0; siteNum < data.numDefiningSites(); siteNum++) {
      Species currSpecies = data.getSiteSpecies(siteNum);
      int specNum = ArrayUtils.findIndex(m_Species, currSpecies);
      if (specNum < 0) {
        // Try to group species of the same element 
        Element currElement = currSpecies.getElement();
        for (specNum = 0; specNum < m_Species.length; specNum++) {
          if (m_Species[specNum].getElement() == currElement) {
            break;
          }
        }
        m_Species = (Species[]) ArrayUtils.insertElement(m_Species, specNum, currSpecies);
        m_SitesPerSpecies = ArrayUtils.insertElement(m_SitesPerSpecies, specNum, 0);
        
      }
      m_SitesPerSpecies[specNum]++;
    }
    
    if (alphabetize) {alphabetizeSpecieOrder();}
    
  }
  
  protected void alphabetizeSpecieOrder() {
    
    String[] elementSymbols = new String[m_Species.length];
    for (int specNum = 0; specNum < elementSymbols.length; specNum++) {
      elementSymbols[specNum] = m_Species[specNum].getElementSymbol();
    }
    
    int[] map = ArrayUtils.getSortPermutation(elementSymbols);
    
    Species[] sortedSpecies = new Species[m_Species.length];
    int[] sortedSpecCounts = new int[m_SitesPerSpecies.length];
    
    for (int specNum= 0; specNum < map.length; specNum++) {
      sortedSpecies[specNum] = m_Species[map[specNum]];
      sortedSpecCounts[specNum] = m_SitesPerSpecies[map[specNum]];
    }
    
    m_Species = sortedSpecies;
    m_SitesPerSpecies = sortedSpecCounts;
    
  }

  protected void readSelectiveDynamics(LineNumberReader lineReader) throws IOException {
    lineReader.mark(100);
    // Check to see if selective dynamics is set
    if (readLineTrimmed(lineReader).toLowerCase().startsWith("s")) {
      m_UseSelectiveDynamics = true;
      return;
    }
    lineReader.reset();
    m_UseSelectiveDynamics = false;
  }

  protected void readBasis(LineNumberReader lineReader) throws IOException {
    //Are the lattice sites in direct or Cartesian Coordinates?
    String thisLine = readLineTrimmed(lineReader);
    StringTokenizer st1 = new StringTokenizer(thisLine);
    String firstToken = st1.nextToken().toLowerCase();
    
    if (firstToken.startsWith("c") || firstToken.startsWith("k")) {
      m_FormatWithDirect = false;
    } else {
      m_FormatWithDirect = true;
    }
  }
  
  protected static String readLineTrimmed(LineNumberReader lineReader) throws IOException {
    String line = lineReader.readLine();
    if (line == null) {
      return line;
    }
    
    int index = line.indexOf("#");
    if (index >= 0) {
      line = line.substring(0, index);
    }
    
    int index2 = line.indexOf("!");
    if (index2 >= 0) {
      line = line.substring(0, index2);
    }
    
    return line.trim();
  }

  protected void readSites(LineNumberReader lineReader) throws IOException {

    int numSpecies = m_SitesPerSpecies.length;
    
    int totalSites = 0;
    for (int speciesNum = 0; speciesNum < numSpecies; speciesNum++) {
      totalSites += m_SitesPerSpecies[speciesNum];
    }

    m_SiteCoords = new Coordinates[totalSites];
    m_SelectiveDynamics = new boolean[totalSites][];
    for (int currIndex = 0; currIndex < totalSites; currIndex++) {
      double[] coordArray = new double[3];
      StringTokenizer st = new StringTokenizer(readLineTrimmed(lineReader));
      for (int coord = 0; coord < 3; coord++) {
        coordArray[coord] = Double.parseDouble(st.nextToken());
      }
      m_SiteCoords[currIndex] = new Coordinates(coordArray, this.getFormatBasis());
      
      m_SelectiveDynamics[currIndex] = new boolean[st.countTokens()];
      for (int dimNum = 0; dimNum < m_SelectiveDynamics[currIndex].length; dimNum++) {
        m_SelectiveDynamics[currIndex][dimNum] = st.nextToken().toUpperCase().equals("T");
      }
    }

  }
  
  protected void readSites(IStructureData data) {
    
    m_SiteCoords = new Coordinates[data.numDefiningSites()];
    
    int siteCoordIndex = 0;
    for (int speciesType = 0; speciesType < m_SitesPerSpecies.length; speciesType++) {
      Species currSpecies = m_Species[speciesType];
      for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
        if (data.getSiteSpecies(siteNum) == currSpecies) {
          m_SiteCoords[siteCoordIndex++] = data.getSiteCoords(siteNum);
        }
      }
    }

    int dim = data.getCellVectors().length;
    m_SelectiveDynamics = new boolean[m_SiteCoords.length][dim];
  }
  
  protected void writeComment(Writer writer) throws IOException {
    
    String comment = m_Comment;
    if (m_WriteSpeciesWithDescription) {
      comment = comment + " <s>";
      boolean containsNull = false;
      for (int specNum = 0; specNum < m_Species.length; specNum++) {
        if (specNum >= m_SitesPerSpecies.length) {continue;}
        if (m_Species[specNum] == null) {
          containsNull = true;
          break;
        }
      }
      if (!containsNull) {
        for (int specNum = 0; specNum < m_Species.length; specNum++) {
          if (specNum >= m_SitesPerSpecies.length) {continue;}
          if (m_SitesPerSpecies[specNum] == 0) {continue;}
          if (m_Species[specNum] == Species.getVacancy()) {continue;}
          Species spec = m_Species[specNum];
          if (m_WriteElementSymbolsOnly && (specNum > 0) && (m_Species[specNum - 1].getElement() == spec.getElement())) {
            continue;
          }
          String specSymbol = this.getSpecSymbol(spec);
          comment = comment + " " + specSymbol;
        }
      }
      comment = comment + " </s>";
    }
    
    writer.write(comment);
    newLine(writer);
  }
  
  protected void writeScaleFactor(Writer writer) throws IOException {
    DecimalFormat formatter = new DecimalFormat("0.00000000000000");
    String output = formatter.format(this.getScaleFactor());
    output = "   " + output;
    output = output.substring(output.length() - 18);
    writer.write(output);
    newLine(writer);
  }
  
  protected void newLine(Writer writer) throws IOException {
    writer.write("\n");
    
    //Use this for writing multi-line Java strings 
    //writer.write("\\n\" + \n\"");
  }
  
  protected void writeLatticeVectors(Writer writer) throws IOException {
    
    double[][] directToCartesian = m_DirectBasis.getBasisToCartesian();
    
    String formatString = "0.0000000000000000";
    if (m_FormatPrecision < 1) {
      formatString = formatString.substring(0, 2-((int) Math.round(Math.log10(m_FormatPrecision))));
    } else {
      formatString = "0";
    }
    DecimalFormat formatter = new DecimalFormat(formatString);
    for (int vectNum = 0; vectNum < directToCartesian.length; vectNum++) {
      for (int coordNum = 0; coordNum < 3; coordNum++) {
        double value = directToCartesian[vectNum][coordNum] / this.getScaleFactor();
        value = MSMath.roundWithPrecision(value, m_FormatPrecision); // Gets rid of some ugly numerical errors
        String output = formatter.format(value);
        output = StringUtils.padLeft(output, formatString.length() + 5);
        /*output = "      " + output;
        output = output.substring(output.length() - (formatString.length() + 5));*/
        writer.write(output);
      }
      newLine(writer);
    }
    
  }
  

  protected void writeSitesPerSpecies(Writer writer) throws IOException {
     this.writeSitesPerSpecies(writer, false);
  }
  
  protected void writeSitesPerSpecies(Writer writer, boolean writeVacancies) throws IOException {
    
    int[] sitesPerSpecies = this.getSitesPerSpecies();
    
    if (m_UseVASP5Format) {
    
      writer.write("  ");
      for (int specNum = 0; specNum < sitesPerSpecies.length; specNum++) {
        if (!writeVacancies && m_Species[specNum] == Species.getVacancy()) {continue;}
        if (sitesPerSpecies[specNum] == 0) {continue;}
        if (m_WriteElementSymbolsOnly && (specNum < sitesPerSpecies.length - 1) && (m_Species[specNum + 1].getElement() == m_Species[specNum].getElement())) {
          continue;
        }
        /*writer.write("  ");
        writer.write(m_Species[specNum].getElementSymbol());*/
        String elementString = m_WriteElementSymbolsOnly ? m_Species[specNum].getElementSymbol() : m_Species[specNum].toString();
        //writer.write(StringUtils.padRight(elementString, 6));
        int fieldLength = Math.max(8, elementString.length() + 1);
        writer.write(StringUtils.padRight(elementString, fieldLength) + " "); // Made this bigger to accommodate vacancies
      }      
      newLine(writer);
    }
    
    int totalForElement = 0;
    writer.write("  ");
    for (int specNum = 0; specNum < sitesPerSpecies.length; specNum++) {
      if (!writeVacancies && (m_Species[specNum] == Species.getVacancy())) {continue;}
      if (sitesPerSpecies[specNum] == 0) {continue;}
      totalForElement += sitesPerSpecies[specNum];
      if (m_WriteElementSymbolsOnly && (specNum < sitesPerSpecies.length - 1) && (m_Species[specNum + 1].getElement() == m_Species[specNum].getElement())) {
        continue;
      }
      /*writer.write("  ");
      writer.write(new Integer(totalForElement).toString());*/
      //writer.write(StringUtils.padRight(new Integer(totalForElement).toString(), 6));
      
      writer.write(StringUtils.padRight(new Integer(totalForElement).toString(), 8) + " ");  // Made this bigger to accommodate vacancies
      totalForElement = 0;
    }
    newLine(writer);
    
  }
  
  protected void writeSelectiveDynamics(Writer writer) throws IOException {
  
    if (m_UseSelectiveDynamics) {
      writer.write("Selective dynamics");
      newLine(writer);
    }
    
  }
  
  protected void writeBasis(Writer writer) throws IOException {
    AbstractBasis basis = this.getFormatBasis();
    if (basis == CartesianBasis.getInstance()) {
      writer.write("Cartesian");
    } else {
      writer.write("Direct");
    }
  
    newLine(writer);
  }
  
  protected void writeSites(Writer writer) throws IOException {
    
    String formatString = "0.0000000000000000";
    if (m_FormatPrecision < 1) {
      formatString = formatString.substring(0, 2-((int) Math.round(Math.log10(m_FormatPrecision))));
    } else {
      formatString = "0";
    }
    DecimalFormat formatter = new DecimalFormat(formatString);
    
    int siteNum = 0;
    for (int specNum = 0; specNum < this.m_SitesPerSpecies.length; specNum++) {
      int numSpecies = m_SitesPerSpecies[specNum];
      Species species = m_Species[specNum];
      for (int specSiteNum = 0; specSiteNum < numSpecies; specSiteNum++) {
        if (species != Species.getVacancy()) { // Don't write out vacancies
          Coordinates siteCoords = this.getSiteCoords(siteNum);
          double[] formatCoordArray = siteCoords.getCoordArray(this.getFormatBasis());
          writer.write("  ");
          for (int i = 0; i < siteCoords.numCoords(); i++) {
            double value = formatCoordArray[i];
            if (this.getFormatBasis() == CartesianBasis.getInstance()) {
              value = value / this.getScaleFactor();
            }
            value = MSMath.roundWithPrecision(value, m_FormatPrecision); // Gets rid of some ugly numerical errors
            String output = formatter.format(value);
            output = StringUtils.padLeft(output, formatString.length() + 3);
            output = output + "   ";
            //output = output.substring(output.length() - (formatString.length() + 3));*/
            writer.write(output);
          }
          
          if (m_UseSelectiveDynamics) {
            for (int dimNum = 0; dimNum < m_SelectiveDynamics[siteNum].length; dimNum++) {
              writer.write(m_SelectiveDynamics[siteNum][dimNum] ? " T" : " F");
            }
          }
          newLine(writer);
        }
        siteNum++;
      }
    }
    
    /*
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {

      Coordinates siteCoords = this.getSiteCoords(siteNum);
      double[] formatCoordArray = siteCoords.getCoordArray(this.getFormatBasis());
      for (int i = 0; i < siteCoords.numCoords(); i++) {
        double value = formatCoordArray[i];
        if (this.getFormatBasis() == CartesianBasis.getInstance()) {
          value = value / this.getScaleFactor();
        }
        value = MSMath.roundWithPrecision(value, m_FormatPrecision); // Gets rid of some ugly numerical errors
        String output = formatter.format(value);
        output = "    " + output;
        output = output.substring(output.length() - 21);
        writer.write(output);
      }
      
      if (m_UseSelectiveDynamics) {
        for (int dimNum = 0; dimNum < m_SelectiveDynamics[siteNum].length; dimNum++) {
          writer.write(m_SelectiveDynamics[siteNum][dimNum] ? "  T" : "  F");
        }
      }
      writer.newLine();
    }*/
  }
  
  public boolean getSelectiveDynamics(int siteNum, int dimNum) {
    return m_SelectiveDynamics[siteNum][dimNum];
  }
  
  public void setSelectiveDynamics(int siteNum, int dimNum, boolean value) {
    m_SelectiveDynamics[siteNum][dimNum] = value;
  }
  
  public void setSelectiveDynamics(int siteNum, boolean value) {
    boolean[] array = m_SelectiveDynamics[siteNum];
    for (int dimNum = 0; dimNum < array.length; dimNum++) {
      array[dimNum] = value;
    }
  }
  
  public void setFormatPrecision(double value) {
    m_FormatPrecision = value;
  }
  
  public double getFormatPrecision() {
    return m_FormatPrecision;
  }
  
  public void setDescription(String description) {
    m_Comment = description;
  }
  
  public int numSpecies() {
    int numSpecies = 0;
    for (int specNum = 0; specNum < m_SitesPerSpecies.length; specNum++) {
      if (m_SitesPerSpecies[specNum] > 0) {numSpecies++;}
    }
    return numSpecies;
  }
  
  public void writeSpeciesWithDescription(boolean value) {
    m_WriteSpeciesWithDescription = value;
  }
  
  public void writeElementSymbolsOnly(boolean value) {
    m_WriteElementSymbolsOnly = value;
  }
  
  public boolean writesElementSymbolsOnly() {
	return m_WriteElementSymbolsOnly;
  }

  public String getDescription() {
    return m_Comment;
  }

  public double getScaleFactor() {
    return m_ScaleFactor;
  }
  
  public void setScaleFactor(double value) {
    m_ScaleFactor = value;
  }

  public Vector[] getCellVectors() {
    
    /*Vector[] returnArray = new Vector[0];
    for (int vecNum = 0; vecNum < m_IsVectorPeriodic.length; vecNum++) {
      if (m_IsVectorPeriodic[vecNum]) {
        returnArray = (Vector[]) ArrayUtils.appendElement(returnArray, m_CellVectors[vecNum]);
      }
    }
    return returnArray;*/
    return (Vector[]) ArrayUtils.copyArray(m_CellVectors);
  }
  
  /*
  public Vector[] getNonPeriodicVectors() {
    
    Vector[] returnArray = new Vector[0];
    for (int vecNum = 0; vecNum < m_IsVectorPeriodic.length; vecNum++) {
      if (!m_IsVectorPeriodic[vecNum]) {
        returnArray = (Vector[]) ArrayUtils.appendElement(returnArray, m_CellVectors[vecNum]);
      }
    }
    return returnArray;
  }*/
  
  public void setVectorPeriodicity(int vecNum, boolean value) {
    m_IsVectorPeriodic[vecNum] = value;
  }
  
  public boolean[] getVectorPeriodicity() {
    return ArrayUtils.copyArray(m_IsVectorPeriodic);
  }

  public int[] getSitesPerSpecies() {
    return ArrayUtils.copyArray(m_SitesPerSpecies);
  }
  
  public Species[] getSpeciesSet() {
    return (Species[]) ArrayUtils.copyArray(m_Species);
  }
  
  public boolean allowsUnknownSpecies() {
    return m_AllowUnknownSpecies;
  }

  public boolean getSelectiveDynamics() {
    return m_UseSelectiveDynamics;
  }
  
  public boolean usesSelectiveDynamics() {
    return m_UseSelectiveDynamics;
  }
  
  public void useSelectiveDynamics(boolean value) {
    m_UseSelectiveDynamics = value;
  }

  public AbstractBasis getFormatBasis() {
    if (m_FormatWithDirect) {
      return m_DirectBasis;
    } 
    return CartesianBasis.getInstance();
  }
  
  public boolean usesVASP5Format() {
    return m_UseVASP5Format;
  }
  
  public void useVASP5Format(boolean value) {
    m_UseVASP5Format = value;
  }
  
  public void useDirectBasis(boolean value) {
    m_FormatWithDirect = value;
  }
  
  public AbstractLinearBasis getDirectBasis() {
    return m_DirectBasis;
  }

  public int numDefiningSites() {
    return m_SiteCoords.length;
  }

  public Coordinates getSiteCoords(int index) {
    return m_SiteCoords[index];
  }

  public Species getSiteSpecies(int index) {
    for (int specNum = 0; specNum < m_SitesPerSpecies.length; specNum++) {
      index -= m_SitesPerSpecies[specNum];
      if (index < 0) {
        return m_Species[specNum];
      }
    }
    return null;
  }

  public String getFileName() {
    return m_FileName;
  }
  
}
