/*
 * Created on Jul 5, 2010
 *
 */
package matsci.io.vasp;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;

import matsci.electronicstructure.IProjectedDiscreteBandStructure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class PROCAR implements IProjectedDiscreteBandStructure {
  
  private int m_NumKPoints;
  private int m_NumBands;
  private int m_NumIons;
  private boolean m_SpinPolarized;
  
  // All now first by spin
  private double[][][] m_KPoints = new double[2][][];
  private double[][] m_KPointWeights = new double[2][];
  private double[][][] m_BandEnergies = new double[2][][];
  private double[][][] m_BandOccupancies = new double[2][][];
  private double[][][][][] m_Projections = new double[2][][][][]; // First by k-point, then by band, then by ion, then by orbital
  private double[][][][] m_ProjectionTotals= new double[2][][][]; // First by k-point, then by band, then by orbital
  
  private String[] m_OrbitalLabels;
  
  public PROCAR(String fileName) {
    
    try {
      readFile(fileName);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
    System.currentTimeMillis();
    
  }
  
  protected void readFile(String fileName) throws IOException {
    
    LineNumberReader reader = new LineNumberReader(new FileReader(fileName));
    
    String line = this.nextMeaningfulLine(reader); 
    
    if (line.contains("phase")) {
      throw new RuntimeException("Cannot read PROCAR with phase factors yet.");
    }
    
    m_OrbitalLabels = line.contains("lm") ? new String[] {"s", "py", "pz", "px", "dxy", "dyz", "dz2", "dxz", "dx2", "tot"} :
        new String[] {"s", "p", "d", "tot"};
    
    this.readSpin(reader, 0);
    this.readSpin(reader, 1);

  }
  
  protected void readSpin(LineNumberReader reader, int spinNum) throws IOException {
    
    String kPointMarker = "# of k-points:";
    String bandMarker = "# of bands:"; 
    String ionMarker = "# of ions:";
    
    String line = this.nextMeaningfulLine(reader);
    if (line == null) {
      if (spinNum == 1) {
        m_KPoints[1] = m_KPoints[0];
        m_KPointWeights[1] = m_KPointWeights[0];
        m_BandEnergies[1] = m_BandEnergies[0];
        m_BandOccupancies[1] = m_BandOccupancies[0];
        for (int kPointNum = 0; kPointNum < this.numKPoints(); kPointNum++) {
          m_BandOccupancies[1][kPointNum] = MSMath.arrayDivide(m_BandOccupancies[1][kPointNum], 2.0);
        }
        m_Projections[1] = m_Projections[0]; // First by k-point, then by band, then by ion, then by orbital
        m_ProjectionTotals[1] = m_ProjectionTotals[0];
        m_SpinPolarized = false;
      }
      return;
    }
    
    if (spinNum == 1) {
      m_SpinPolarized = true;
    }
    
    int kPointStart = line.indexOf(kPointMarker) + kPointMarker.length();
    int kPointEnd = line.indexOf(bandMarker);
    m_NumKPoints = Integer.parseInt(line.substring(kPointStart, kPointEnd).trim());
    
    int bandStart  = kPointEnd + bandMarker.length();
    int bandEnd = line.indexOf(ionMarker);
    m_NumBands = Integer.parseInt(line.substring(bandStart, bandEnd).trim());
    
    int ionStart = bandEnd + ionMarker.length();
    m_NumIons = Integer.parseInt(line.substring(ionStart).trim());
    
    m_KPoints[spinNum] = new double[m_NumKPoints][3];
    m_KPointWeights[spinNum] = new double[m_NumKPoints];
    m_BandEnergies[spinNum] = new double[m_NumKPoints][m_NumBands];
    m_BandOccupancies[spinNum] = new double[m_NumKPoints][m_NumBands];
    m_Projections[spinNum] = new double[m_NumKPoints][m_NumBands][m_NumIons][m_OrbitalLabels.length];
    m_ProjectionTotals[spinNum] = new double[m_NumKPoints][m_NumBands][m_OrbitalLabels.length];
    
    for (int kpointNum = 0; kpointNum < m_NumKPoints; kpointNum++) {
      line = this.nextMeaningfulLine(reader);
      
      StringTokenizer st = new StringTokenizer(line);

      st.nextToken(); // k-point
      st.nextToken(); // k-point number
      st.nextToken(); // colon
      m_KPoints[spinNum][kpointNum][0] = Double.parseDouble(st.nextToken());
      m_KPoints[spinNum][kpointNum][1] = Double.parseDouble(st.nextToken());
      m_KPoints[spinNum][kpointNum][2] = Double.parseDouble(st.nextToken());

      st.nextToken(); // weight
      st.nextElement(); // =
      m_KPointWeights[spinNum][kpointNum] = Double.parseDouble(st.nextToken());
      for (int bandNum = 0; bandNum < m_NumBands; bandNum++) {
        line = this.nextMeaningfulLine(reader);
        st = new StringTokenizer(line);
        st.nextToken(); // band
        st.nextToken(); // band number
        st.nextToken(); // hash
        st.nextToken(); // energy
        m_BandEnergies[spinNum][kpointNum][bandNum] = Double.parseDouble(st.nextToken());
        st.nextToken(); // hash
        st.nextToken(); // occ.
        m_BandOccupancies[spinNum][kpointNum][bandNum] = Double.parseDouble(st.nextToken());
        
        line = this.nextMeaningfulLine(reader); // Header line
        for (int ionNum = 0; ionNum < m_NumIons; ionNum++) {
          line = this.nextMeaningfulLine(reader);
          st = new StringTokenizer(line);
          String ionNumString = st.nextToken();
          for (int orbitNum = 0; orbitNum < m_OrbitalLabels.length; orbitNum++) {
            m_Projections[spinNum][kpointNum][bandNum][ionNum][orbitNum] = Double.parseDouble(st.nextToken());
          }
        }
        if (m_NumIons > 1) {
          line = this.nextMeaningfulLine(reader); // Totals
          st = new StringTokenizer(line);
          String ionNumString = st.nextToken();
          for (int orbitNum = 0; orbitNum < m_OrbitalLabels.length; orbitNum++) {
            m_ProjectionTotals[spinNum][kpointNum][bandNum][orbitNum] = Double.parseDouble(st.nextToken());
          }
        } else { // VASP doesn't print the totals row if there's only one ion.
          m_ProjectionTotals[spinNum][kpointNum][bandNum] = ArrayUtils.copyArray(m_Projections[spinNum][kpointNum][bandNum][0]);
        }
      }
    }
    
  }
  
  protected String nextMeaningfulLine(LineNumberReader reader) throws IOException {
    
    String line = reader.readLine();
    while ((line != null) && line.trim().length() == 0) {
      line = reader.readLine();
    }
    
    return line;
    
  }
  
  public boolean isSpinPolarized() {
    return m_SpinPolarized;
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IDiscreteBandStructure#numKPoints()
   */
  public int numKPoints() {
    return m_NumKPoints;
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IDiscreteBandStructure#numBands()
   */
  public int numBands() {
    return m_NumBands;
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#numIons()
   */
  public int numIons() {
    return m_NumIons;
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#numOrbitals()
   */
  public int numOrbitals() {
    return m_OrbitalLabels.length - 1;
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#getOrbitalLabel(int)
   */
  public String getOrbitalLabel(int orbitalNum) {
    return m_OrbitalLabels[orbitalNum];
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#getOrbitalLabelNum(java.lang.String)
   */
  public int getOrbitalLabelNum(String orbitalLabel) {
    return ArrayUtils.findIndex(m_OrbitalLabels, orbitalLabel);
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IDiscreteBandStructure#getKPointCoords(int)
   */
  public double[] getKPointCoords(int kpointNum) {
    return ArrayUtils.copyArray(m_KPoints[0][kpointNum]);
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IDiscreteBandStructure#getWeight(int)
   */
  public double getWeight(int kpointNum) {
    return m_KPointWeights[0][kpointNum];
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IDiscreteBandStructure#getBandEnergy(int, int)
   */
  /*public double getBandEnergy(int kpointNum, int bandNum) {
    return m_BandEnergies[0][kpointNum][bandNum];
  }*/
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IDiscreteBandStructure#getBandEnergy(int, int)
   */
  public double getBandEnergy(int spinNum, int kpointNum, int bandNum) {
    return m_BandEnergies[spinNum][kpointNum][bandNum];
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IDiscreteBandStructure#getBandOccupancy(int, int)
   */
  /*public double getBandOccupancy(int kpointNum, int bandNum) {
    return m_BandOccupancies[0][kpointNum][bandNum];
  }*/
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IDiscreteBandStructure#getBandOccupancy(int, int)
   */
  public double getBandOccupancy(int spinNum, int kpointNum, int bandNum) {
    return m_BandOccupancies[spinNum][kpointNum][bandNum];
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#getDensity(int, int)
   */
  /*public double getProjection(int kPointNum, int bandNum) {
    return m_ProjectionTotals[0][kPointNum][bandNum][m_OrbitalLabels.length - 1];
  }*/
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#getDensity(int, int)
   */
  public double getProjection(int spinNum, int kPointNum, int bandNum) {
    return m_ProjectionTotals[spinNum][kPointNum][bandNum][m_OrbitalLabels.length - 1];
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#getProjectedDensity(int, int, java.lang.String)
   */
  public double getProjection(int spinNum, int kPointNum, int bandNum, String orbitalLabel) {
    double returnValue = 0;
    for (int labelNum = 0; labelNum < m_OrbitalLabels.length; labelNum++) {
      if (m_OrbitalLabels[labelNum].startsWith(orbitalLabel)) {
        returnValue += m_ProjectionTotals[spinNum][kPointNum][bandNum][labelNum];
      }
    }
    return returnValue;
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#getProjectedDensity(int, int, int)
   */
  public double getProjection(int spinNum, int kPointNum, int bandNum, int ionNum) {
    return m_Projections[spinNum][kPointNum][bandNum][ionNum][m_OrbitalLabels.length - 1];
  }
  
  /* (non-Javadoc)
   * @see matsci.electronicstructure.IProjectedDiscreteBandStructure#getProjectedDensity(int, int, int, java.lang.String)
   */
  public double getProjection(int spinNum, int kPointNum, int bandNum, int ionNum, String orbitalLabel) {
    double returnValue = 0;
    for (int labelNum = 0; labelNum < m_OrbitalLabels.length; labelNum++) {
      if (m_OrbitalLabels[labelNum].startsWith(orbitalLabel)) {
        returnValue += m_Projections[spinNum][kPointNum][bandNum][ionNum][labelNum];
      }
    }
    return returnValue;
  }

}
