/*
 * Created on Mar 9, 2006
 *
 */
package matsci.io.clusterexpansion;

import java.io.*;
import java.math.*;
import java.text.DecimalFormat;
import java.util.*;

import matsci.*;
import matsci.structure.superstructure.SuperLatticeGenerator;
import matsci.structure.function.ce.structures.*;
import matsci.util.*;
import matsci.util.arrays.*;

public class ConFile {

  private int[] m_ConsideredSizes;
  private int[] m_ConsideredLatticeIndices;
  private int[][][] m_ConsideredCells;
  private BigInteger[][] m_DecorationIDs = new BigInteger[0][]; // By size, by lattice index, then by ID index
  private double[][][] m_Concentrations = new double[0][][]; // By size, by lattice index, then by ID index
  private Species[] m_SigmaSpecies;
  
  public ConFile(int[][][] consideredCells, Species[] sigmaSpecies) {
    this.setConsideredSuperCells(consideredCells);
    m_SigmaSpecies = (Species[]) ArrayUtils.copyArray(sigmaSpecies);
  }
  
  public ConFile(String fileName) {
    
    try {
      readFile(fileName);
    }
    catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
    
  }
  
  protected void readFile(String fileName) throws IOException {
    
    FileReader fileReader = new FileReader(fileName);
    LineNumberReader lineReader = new LineNumberReader(fileReader);
    String firstLine = lineReader.readLine(); // Ignore this
    String nextLine = lineReader.readLine().trim();
    while (nextLine.length() == 0) {
      nextLine = lineReader.readLine().trim();
    }
    StringTokenizer st = new StringTokenizer(nextLine);
    int numSuperToDirects = Integer.parseInt(st.nextToken());
    m_ConsideredCells = new int[numSuperToDirects][][];
    m_ConsideredSizes = new int[numSuperToDirects];
    m_ConsideredLatticeIndices = new int[numSuperToDirects];
    m_DecorationIDs = new BigInteger[numSuperToDirects][0];
    m_Concentrations = new double[numSuperToDirects][0][];
    
    for (int cellNum = 0; cellNum < m_ConsideredCells.length; cellNum++) {
      this.readStructuresForCell(lineReader, cellNum);
    }
  }
  
  protected void readStructuresForCell(LineNumberReader reader, int cellNum) throws IOException {
    String cellVectorString = reader.readLine().trim();
    while (cellVectorString.length() == 0) {
      cellVectorString = reader.readLine().trim();
    }
    
    StringTokenizer st = new StringTokenizer(cellVectorString);
    int dimension = (int) Math.sqrt(st.countTokens());
    if (dimension * dimension != st.countTokens()) {
      throw new IOException("Can't determine number of periodic dimensions.  Matrix must be square: " + cellVectorString);
    }
    
    int[][] superToDirect = new int[dimension][dimension];
    for (int rowNum = 0; rowNum < dimension; rowNum++) {
      int[] row = superToDirect[rowNum];
      for (int colNum = 0; colNum < dimension; colNum++) {
        row[colNum] = Integer.parseInt(st.nextToken());
      }
    }
    m_ConsideredCells[cellNum] = superToDirect;
    
    String firstText = reader.readLine().trim();
    while (firstText.length() == 0) {
      firstText = reader.readLine().trim();
    }
    
    String secondText = reader.readLine();
    String numStructString = reader.readLine().trim();
    int numStructs = Integer.parseInt(numStructString);
    m_DecorationIDs[cellNum] = new BigInteger[numStructs];
    m_Concentrations[cellNum] = new double[numStructs][];
    for (int structNum= 0; structNum < numStructs; structNum++) {
      String structLine = reader.readLine().trim();
      st = new StringTokenizer(structLine);
      m_DecorationIDs[cellNum][structNum] = new BigInteger(st.nextToken());
      String concentrationString = st.nextToken();
      double[] concentrations = new double[st.countTokens()];
      for (int specNum = 0; specNum < concentrations.length; specNum++) {
        concentrations[specNum] = Double.parseDouble(st.nextToken());
      }
      m_Concentrations[cellNum][structNum] = concentrations;
    }
    
  }

  protected void setConsideredSuperCells(int[][][] superCells) {

    if (superCells.length == 0) {return;}
    int numDimensions = superCells[0].length;
    m_ConsideredCells = ArrayUtils.copyArray(superCells);
    m_DecorationIDs = new BigInteger[superCells.length][0];
    m_Concentrations = new double[superCells.length][0][];
    
    m_ConsideredSizes = new int[superCells.length];
    m_ConsideredLatticeIndices = new int[superCells.length];
    SuperLatticeGenerator[] generators = new SuperLatticeGenerator[0];
    for (int cellNum = 0; cellNum < superCells.length; cellNum++) {
      int[][] superToDirect = superCells[cellNum];
      int size =SuperLatticeGenerator.getSize(superToDirect);
      if (size >= generators.length) {
        generators = (SuperLatticeGenerator[]) ArrayUtils.growArray(generators, (size - generators.length) + 1);
        generators[size] = new SuperLatticeGenerator(size, numDimensions);
      }
      SuperLatticeGenerator generator = generators[size];
      m_ConsideredLatticeIndices[cellNum] = generator.getLatticeIndex(superToDirect);
    }
  }
  
  public void addCorrelations(int consideredCellNum, BigInteger decorationID, double[] concentrations) {
    
    if (consideredCellNum >= m_ConsideredCells.length) {
      throw new RuntimeException("Tried to add correlations for a supercell not known as a considered supercell.");
    }
    
    m_DecorationIDs[consideredCellNum] = (BigInteger[]) ArrayUtils.appendElement(m_DecorationIDs[consideredCellNum], decorationID);
    m_Concentrations[consideredCellNum] = ArrayUtils.appendElement(m_Concentrations[consideredCellNum], concentrations);
  }
  
  public void addCorrelations(int size, int latticeIndex, BigInteger decorationID, double[] concentrations) {
    
    if (size >= m_ConsideredCells.length) {
      throw new RuntimeException("Tried to add correlations for a supercell of size greater than known considered supercells.");
    }
    
    int cellNum = 0;
    for (cellNum = 0; cellNum < m_ConsideredCells.length; cellNum++) {
      if ((m_ConsideredSizes[cellNum] == size) && (m_ConsideredLatticeIndices[cellNum] == latticeIndex)) {
        break;
      }
    }
    
    this.addCorrelations(cellNum, decorationID, concentrations);
 }
  
  public void addCorrelations(ClustStructureData structData) {
    this.addCorrelations(structData.getSize(), structData.getLatticeNumber(), structData.getDecorationID(), structData.getConcentrations());
  }
  
  public void addCorrelations(int[][] superToDirect, BigInteger decorationID, double[] concentrations) {
    int size = SuperLatticeGenerator.getSize(superToDirect);
    SuperLatticeGenerator generator = new SuperLatticeGenerator(size, superToDirect.length);
    int latticeIndex = generator.getLatticeIndex(superToDirect);
    this.addCorrelations(size, latticeIndex, decorationID, concentrations);
  }
  
  public int numSuperCellsConsidered() {
    return m_ConsideredCells.length;
  }
  
  public int[][] getConsideredCell(int cellNum) {
    return ArrayUtils.copyArray(m_ConsideredCells[cellNum]);
  }
  
  public int numStructuresForCell(int cellNum) {
    return m_DecorationIDs[cellNum].length;
  }
  
  public BigInteger getDecorationID(int cellNum, int structNum) {
    return m_DecorationIDs[cellNum][structNum];
  }
  
  public double[] getConcentrations(int cellNum, int structNum) {
    return ArrayUtils.copyArray(m_Concentrations[cellNum][structNum]);
  }
  
  public int getConsideredCellSize(int cellNum) {
    return m_ConsideredSizes[cellNum];
  }
  
  public int getConsideredLatticeIndex(int cellNum) {
    return m_ConsideredLatticeIndices[cellNum];
  }
  
  public void writeFile(String fileName) throws IOException {
    FileWriter fileWriter = new FileWriter(fileName);
    BufferedWriter writer = new BufferedWriter(fileWriter);
    writer.write(" Structures generated within supercells.  Sigma Specie: ");
    for (int specNum = 0; specNum < m_SigmaSpecies.length; specNum++) {
      writer.write(m_SigmaSpecies[specNum].getSymbol());
      if (specNum != m_SigmaSpecies.length -1) {
        writer.write(", ");
      }
    }
    DecimalFormat formatter = new DecimalFormat("0.000000");
    this.newLine(writer);
    this.newLine(writer);
    writer.write(StringUtils.rightAlign(m_ConsideredCells.length, 12) + "  supercells considered");
    this.newLine(writer);
    for (int cellNum = 0; cellNum < m_ConsideredCells.length; cellNum++) {
      int[][] superCell = m_ConsideredCells[cellNum];
      if (superCell == null) {continue;}
      writer.write(this.getSuperCellString(superCell));
      this.newLine(writer);
      this.newLine(writer);
      writer.write(" multer of distinct configurations");
      this.newLine(writer);
      writer.write(" in this supercell are");
      this.newLine(writer);
      BigInteger[] decorationIDs = m_DecorationIDs[cellNum];
      double[][] concentrations = m_Concentrations[cellNum];
      writer.write(StringUtils.rightAlign(decorationIDs.length, 12));
      this.newLine(writer);
      for (int decNum = 0; decNum < decorationIDs.length; decNum++) {
        String output = StringUtils.rightAlign(decorationIDs[decNum].toString(), 10);
        output = output + "    concentrat";
        for (int specNum = 0; specNum < concentrations[decNum].length; specNum++) {
          output = output + StringUtils.rightAlign(formatter.format(concentrations[decNum][specNum]), 11);
        }
        writer.write(output);
        this.newLine(writer);
      }
      this.newLine(writer);
      this.newLine(writer);
    }
      
    writer.flush();
    writer.close();
  }
  
  private String getSuperCellString(int[][] superCell) {
    String returnString = "";
    for (int row = 0; row < superCell.length; row++) {
      for (int col = 0; col < superCell.length; col++) {
        returnString = returnString + StringUtils.rightAlign(superCell[row][col], 4);
      }
    }
    return returnString;
  }

  private void newLine(BufferedWriter writer) throws IOException {
    writer.write("\n");
  }

}
