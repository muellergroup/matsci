package matsci.io.clusterexpansion;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;
import java.util.Arrays;

import matsci.util.*;
import matsci.util.arrays.ArrayUtils;
import matsci.structure.decorate.function.ce.*;
import matsci.structure.decorate.function.ce.clusters.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class ECIIn {

  private int[] m_IncludeECI;
  private int[] m_Multiplicity;
  private int[] m_ClusterSize;
  private int[][] m_SubClusters;

  //private int m_NumGroups;
  private int[] m_GroupNumbers;
  
  public static int FORCE_ECI() {return 2;}
  public static int INCLUDE_ECI() {return 1;}
  public static int CANDIDATE_ECI() {return 0;}
  public static int IGNORE_ECI() {return -1;}
  
  public ECIIn(ClusterExpansion ce, int defaultInclusion) {
    m_GroupNumbers = new int[ce.numGroups()];
    int arraySize = ce.getMaxGroupNumber() + 1;
    m_IncludeECI = new int[arraySize];
    m_Multiplicity = new int[arraySize];
    m_ClusterSize = new int[arraySize];
    m_SubClusters = new int[arraySize][];
    int totalGroupIndex = 0;
    for (int groupNumber= 0; groupNumber <= ce.getMaxGroupNumber(); groupNumber++) {
      ClusterGroup group = ce.getClusterGroup(groupNumber); 
      if (group == null) {continue;}
      m_GroupNumbers[totalGroupIndex++] = groupNumber;
      m_IncludeECI[groupNumber] = defaultInclusion;
      m_Multiplicity[groupNumber] = group.getMultiplicity();
      m_ClusterSize[groupNumber] = group.numSitesPerCluster();
      int[] subClusters = new int[group.numSubGroups()];
      for (int subGroupIndex = 0; subGroupIndex < subClusters.length; subGroupIndex++) {
        subClusters[subGroupIndex] = group.getSubGroup(subGroupIndex).getGroupNumber();
      }
      m_SubClusters[groupNumber] = subClusters;
    }
  }
  
  public ECIIn(ClusterExpansion ce) {
    this(ce, CANDIDATE_ECI());
  }

  public ECIIn(String fileName) {

    try {
      LineNumberReader lineReader = new LineNumberReader(new FileReader(fileName));
      this.sizeArrays(lineReader);
      lineReader.close();
      lineReader = new LineNumberReader(new FileReader(fileName));
      this.populateArrays(lineReader);
      lineReader.close();
    } catch (IOException e) {
      throw new RuntimeException("Failed to read file: " + fileName, e);
    }
  }
  
  public void writeFile(String fileName) throws IOException {
    FileWriter fileWriter = new FileWriter(fileName);
    BufferedWriter writer = new BufferedWriter(fileWriter);
    
    writer.write("ECIs    fit   mult   clust size    subclusters");
    this.newLine(writer);
    for (int groupIndex = 0; groupIndex < m_GroupNumbers.length; groupIndex++) {
      int groupNumber = m_GroupNumbers[groupIndex];
      writer.write(StringUtils.rightAlign(groupNumber, 4));
      writer.write(StringUtils.rightAlign(m_IncludeECI[groupNumber], 7));
      writer.write(StringUtils.rightAlign(m_Multiplicity[groupNumber], 7));
      writer.write(StringUtils.rightAlign(m_ClusterSize[groupNumber], 7));
      writer.write(StringUtils.rightAlign(m_SubClusters[groupNumber].length, 11));
      for (int subGroupIndex = 0; subGroupIndex < m_SubClusters[groupNumber].length; subGroupIndex++) {
        writer.write(StringUtils.rightAlign(m_SubClusters[groupNumber][subGroupIndex], 5));
      }
      this.newLine(writer);
    }
    writer.flush();
    writer.close();
  }

  private void newLine(BufferedWriter writer) throws IOException {
    writer.write("\n");
  }

  private void sizeArrays(LineNumberReader lineReader) throws IOException {


    // Toss out the first line
    lineReader.readLine();

    String line = lineReader.readLine();
    int maxGroupNumber = 0;
    int numGroups = 0;
    while (line != null) {
      numGroups++;
      StringTokenizer st = new StringTokenizer(line);
      int groupNumber = Integer.parseInt(st.nextToken());
      maxGroupNumber = Math.max(groupNumber, maxGroupNumber);
      line = lineReader.readLine();
    }

    m_GroupNumbers = new int[numGroups];

    int arrayLength = maxGroupNumber + 1;
    m_IncludeECI = new int[arrayLength];
    m_Multiplicity = new int[arrayLength];
    m_ClusterSize = new int[arrayLength];
    m_SubClusters = new int[arrayLength][0];
  }

  private void populateArrays(LineNumberReader lineReader) throws IOException {

    // Set the default inclusion values
    Arrays.fill(m_IncludeECI, IGNORE_ECI());
    
    // Toss out the first line
    lineReader.readLine();

    String line = lineReader.readLine();
    int lineNumber = 0;
    while (line != null) {
      StringTokenizer st = new StringTokenizer(line);
      int groupNumber = Integer.parseInt(st.nextToken());
      m_GroupNumbers[lineNumber++] = groupNumber;
      m_IncludeECI[groupNumber] = Integer.parseInt(st.nextToken());
      m_Multiplicity[groupNumber] = Integer.parseInt(st.nextToken());
      m_ClusterSize[groupNumber] = Integer.parseInt(st.nextToken());
      int[] subClusters = new int[Integer.parseInt(st.nextToken())];
      for (int subClustNum = 0; subClustNum < subClusters.length; subClustNum++) {
        subClusters[subClustNum] = Integer.parseInt(st.nextToken());
      }
      m_SubClusters[groupNumber] = subClusters;
      line = lineReader.readLine();
    }
  }

  public int numGroups() {
    return m_GroupNumbers.length;
  }

  public int getMaxGroupNumber() {
    return m_IncludeECI.length - 1;
  }

  public int[] getGroupNumbers() {
    return ArrayUtils.copyArray(m_GroupNumbers);
  }

  public int[] getSubGroups(int groupNumber) {
    return ArrayUtils.copyArray(m_SubClusters[groupNumber]);
  }
  
  public void setGroupInclusion(int groupNumber, int inclusionNumber) {
    m_IncludeECI[groupNumber] = inclusionNumber;
  }
  
  public int numCandidateGroups() {
    int returnValue = 0;
    for (int groupIndex = 0; groupIndex < m_GroupNumbers.length; groupIndex++) {
      int groupNumber = m_GroupNumbers[groupIndex];
      if (this.isGroupCandidate(groupNumber)) {returnValue++;}
    }
    return returnValue;
  }
  
  public boolean isGroupCandidate(int groupNumber) {
    int candidateECI = CANDIDATE_ECI();
    return (m_IncludeECI[groupNumber] == candidateECI || this.isGroupIncluded(groupNumber));
  }
  
  public boolean isGroupIncluded(int groupNumber) {
    int includeECI = INCLUDE_ECI();
    return (m_IncludeECI[groupNumber] == includeECI || this.isGroupForced(groupNumber));
  }
  
  public boolean isGroupForced(int groupNumber) {
    int forceECI = FORCE_ECI();
    return (m_IncludeECI[groupNumber] == forceECI);
  }
  
  public int numForcedGroups() {
    int returnValue = 0;
    for (int groupIndex = 0; groupIndex < m_GroupNumbers.length; groupIndex++) {
      int groupNumber = m_GroupNumbers[groupIndex];
      if (this.isGroupForced(groupNumber)) {returnValue++;}
    }
    return returnValue;
  }
  
  public int numIncludedGroups() {
    int returnValue = 0;
    for (int groupIndex = 0; groupIndex < m_GroupNumbers.length; groupIndex++) {
      int groupNumber = m_GroupNumbers[groupIndex];
      if (this.isGroupIncluded(groupNumber)) {returnValue++;}
    }
    return returnValue;
  }
  
  public void setCandidateGroup(int groupNumber) {
    m_IncludeECI[groupNumber] = CANDIDATE_ECI();
  }
  
  public void setIncludeGroup(int groupNumber) {
    m_IncludeECI[groupNumber] = INCLUDE_ECI();
  }
  
  public void setIgnoreGroup(int groupNumber) {
    m_IncludeECI[groupNumber] = IGNORE_ECI();
  }

}