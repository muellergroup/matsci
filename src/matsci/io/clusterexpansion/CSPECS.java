/*
 * Created on Feb 25, 2005
 *
 */
package matsci.io.clusterexpansion;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;

import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class CSPECS {

  private String m_Comment = "";
  private int[] m_SitesPerCluster = new int[0];
  private double[] m_ClusterRadii = new double[0];
  
  public CSPECS() {
  }
  
  public CSPECS(String fileName) {
    try {
      readFile(fileName);
    }
    catch (IOException e) {
      throw new RuntimeException("Error reading CSPECS file " + fileName, e);
    }
  }
  
  private void readFile(String fileName) throws IOException {
    
    FileReader fileReader = new FileReader(fileName);
    LineNumberReader lineReader = new LineNumberReader(fileReader);
    
    this.readComment(lineReader);
    this.readHeaders(lineReader);
    this.readSpecs(lineReader);
  }
  
  private void readComment(LineNumberReader reader) throws IOException {
    m_Comment = reader.readLine();
  }
  
  private void readHeaders(LineNumberReader reader) throws IOException {
    String header = reader.readLine();
  }
  
  private void readSpecs(LineNumberReader reader) throws IOException {
    
    String line = reader.readLine();
    int entryNum = 0;
    while (line != null && line.trim().length() != 0) {
      StringTokenizer st = new StringTokenizer(line);
      m_SitesPerCluster = ArrayUtils.appendElement(m_SitesPerCluster, Integer.parseInt(st.nextToken()));
      m_ClusterRadii = ArrayUtils.appendElement(m_ClusterRadii, Double.parseDouble(st.nextToken()));
      entryNum++;
      line = reader.readLine();
    }
  }
  
  public int numEntries() {
    return m_SitesPerCluster.length;
  }
  
  public int getSitesPerCluster(int entryNum) {
    return m_SitesPerCluster[entryNum];
  }
  
  public double getClusterRadius(int entryNum) {
    return m_ClusterRadii[entryNum];
  }
  
  public void setEntries(int[] sitesPerCluster, double[] clusterRadii) {
    m_SitesPerCluster = ArrayUtils.copyArray(sitesPerCluster);
    m_ClusterRadii = ArrayUtils.copyArray(clusterRadii);
  }

}
