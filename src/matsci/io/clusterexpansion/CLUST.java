/*
 * Created on Dec 2, 2005
 *
 */
package matsci.io.clusterexpansion;

import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

import matsci.location.Coordinates;
import matsci.location.basis.AbstractBasis;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;
import matsci.util.*;

public class CLUST {

//This is an array of arrays of clusterdata.  Each entry corresponds to a group.  We initialize the empty group
  private Coordinates[][] m_ClusterCoordsByGroup = new Coordinates[][] {new Coordinates[0]};
  private int[] m_Multiplicities = new int[] {1};
  private boolean[] m_ActiveGroups = new boolean[] {true};
  
  private AbstractBasis m_Basis;

  public CLUST(String fileName, AbstractBasis basis) {

    m_Basis = basis;

    try {
      readFile(fileName);
    }
    catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public CLUST(ClusterGroup[] groups) {
    
    int maxGroupNumber = 0;
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      maxGroupNumber = Math.max(groups[groupIndex].getGroupNumber(), maxGroupNumber);
    }
    m_ClusterCoordsByGroup = new Coordinates[maxGroupNumber + 1][];
    m_ClusterCoordsByGroup[0] = new Coordinates[0];
    m_Multiplicities = new int[maxGroupNumber + 1];
    m_Multiplicities[0] = 1;
    m_ActiveGroups = new boolean[maxGroupNumber + 1];
    m_ActiveGroups[0] = true;
    
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      ClusterGroup group = groups[groupIndex];
      int groupNum = group.getGroupNumber();
      m_ClusterCoordsByGroup[groupNum] = group.getSampleCoords();
      m_Multiplicities[groupNum]  = group.getMultiplicity();
      m_ActiveGroups[groupNum] = true;
      m_Basis = group.getClusterExpansion().getBaseStructure().getDirectBasis();
    }
    
  }
  
  public CLUST(ClusterExpansion ce) {
    m_ClusterCoordsByGroup = new Coordinates[ce.getMaxGroupNumber() + 1][];
    m_ClusterCoordsByGroup[0] = new Coordinates[0];
    m_Multiplicities = new int[ce.getMaxGroupNumber() + 1];
    m_Multiplicities[0] = 1;
    m_ActiveGroups = new boolean[ce.getMaxGroupNumber() + 1];
    m_ActiveGroups[0] = true;
    for (int groupNum = 1; groupNum < m_ClusterCoordsByGroup.length; groupNum++) {
      ClusterGroup group = ce.getClusterGroup(groupNum);
      if (group == null) {continue;}
      m_ClusterCoordsByGroup[groupNum] = group.getSampleCoords();
      m_Multiplicities[groupNum]  = group.getMultiplicity();
      m_ActiveGroups[groupNum] = true;
    }
    
    m_Basis = ce.getBaseStructure().getDirectBasis();
  }
  
  public void writeFile(String fileName) throws IOException {
    this.writeFile(fileName, m_Basis);
  }
  
  public void writeFile(String fileName, AbstractBasis basis) throws IOException {
    FileWriter fileWriter = new FileWriter(fileName);
    BufferedWriter writer = new BufferedWriter(fileWriter);
    
    int numGroups = 0;
    for (int groupNum = 1; groupNum < m_ClusterCoordsByGroup.length; groupNum++) {
      if (m_ClusterCoordsByGroup[groupNum] != null) {numGroups++;}
    }
    
    writer.write("          " + numGroups);
    newLine(writer);
    for (int groupNum = 1; groupNum < m_ClusterCoordsByGroup.length; groupNum++) {
      this.writeClusterGroup(writer, basis, groupNum);
    }
    writer.flush();
    writer.close();
  }
  
  private void writeClusterGroup(BufferedWriter writer, AbstractBasis basis, int groupNum) throws IOException {
    
    DecimalFormat formatter = new DecimalFormat("0.0000000");
    
    Coordinates[] sampleCoords = m_ClusterCoordsByGroup[groupNum];
    if (sampleCoords == null) {return;}
    
    double distance = 0;
    for (int siteNum = 0; siteNum < sampleCoords.length; siteNum++) {
      for (int prevSiteNum = 0; prevSiteNum < siteNum; prevSiteNum++) {
        distance = Math.max(distance, sampleCoords[siteNum].distanceFrom(sampleCoords[prevSiteNum]));
      }
    }
    
    String groupNumString = StringUtils.rightAlign(groupNum, 5);
    String sizeString = StringUtils.rightAlign(sampleCoords.length, 5);
    String multiplicityString = StringUtils.rightAlign(m_Multiplicities[groupNum], 5);
    int groupActivity = m_ActiveGroups[groupNum] ? 1 : 0;
    writer.write(groupNumString + sizeString + multiplicityString + "  " + groupActivity + "                         distance=   " + formatter.format(distance));
    newLine(writer);
    for (int coordNum = 0; coordNum < sampleCoords.length; coordNum++) {
      double[] coordArray = sampleCoords[coordNum].getCoordArray(basis);
      for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
        String output = formatter.format(coordArray[dimNum]);
        output = "     " + output;
        output = output.substring(output.length() - 11);
        writer.write(output + " ");
      }
      newLine(writer);
    }
    
  }
  
  private void newLine(BufferedWriter writer) throws IOException {
    writer.write("\n");
  }

  private void readFile(String fileName) throws IOException {

    FileReader fileReader = new FileReader(fileName);
    LineNumberReader lineReader = new LineNumberReader(fileReader);
    boolean keepgoing = true;
    
    try {
      int numClusters = Integer.parseInt(lineReader.readLine().trim());
      do {
        keepgoing = readClusterGroup(lineReader);
      } while (keepgoing);
    } catch (EOFException e) {}

    fileReader.close();
  }

  private boolean readClusterGroup(LineNumberReader lineReader) throws IOException {
    
    // Read the cluster information
    String groupSummaryString = lineReader.readLine();
    if ((groupSummaryString == null) || (groupSummaryString.trim().length() == 0)) {
      return false;
    }
    StringTokenizer st = new StringTokenizer(groupSummaryString);
    int groupNumber = Integer.parseInt(st.nextToken());
    int clusterSize = Integer.parseInt(st.nextToken());
    int numClusters = Integer.parseInt(st.nextToken());
    int activityNumber = Integer.parseInt(st.nextToken());
    boolean activity = (activityNumber == 1);
    String label = st.nextToken();
    Coordinates[] sampleClusterCoords = new Coordinates[clusterSize];

    // Read the various sites coordinates
    for (int siteNum = 0; siteNum < clusterSize; siteNum++) {
      double[] coordArray = new double[3];
      String clusterCoordLine = lineReader.readLine();
      st = new StringTokenizer(clusterCoordLine);
      coordArray[0] = Double.parseDouble(st.nextToken());
      coordArray[1] = Double.parseDouble(st.nextToken());
      coordArray[2] = Double.parseDouble(st.nextToken());
      sampleClusterCoords[siteNum] = new Coordinates(coordArray, m_Basis);
    }

    // The index of each group is the groupNumber.  We resize the GroupArray if necessary.
    this.setClustersForGroup(groupNumber, numClusters, activity, sampleClusterCoords);
    return true;
  }
  

  public Coordinates[][] getAllClusterCoords(boolean includeEmpty) {
    if (includeEmpty) {
      return (Coordinates[][]) ArrayUtils.copyArray(m_ClusterCoordsByGroup);
    }
    
    return (Coordinates[][]) ArrayUtils.removeElement(m_ClusterCoordsByGroup, 0);
  }

  public int numGroups() {
    int returnValue = 0;
    for (int groupNum = 0; groupNum < m_ClusterCoordsByGroup.length; groupNum++) {
      if (this.containsGroup(groupNum)) {returnValue++;}
    }
    return returnValue;
  }

  public int sitesPerClusterInGroup(int groupNumber) {
    if (! this.containsGroup(groupNumber)) {return 0;}
    Coordinates[] clusterCoords = m_ClusterCoordsByGroup[groupNumber];
    if (clusterCoords == null) {return 0;}
    return clusterCoords.length;
  }

  public Coordinates[] getClusterCoordsForGroup(int groupNumber) {
    return m_ClusterCoordsByGroup[groupNumber];
  }

  public int getMaxGroupNumber() {
    return m_ClusterCoordsByGroup.length - 1;
  }

  public boolean containsGroup(int groupNumber) {
    return (m_ClusterCoordsByGroup[groupNumber] != null);
  }
  
  public void addClusters(Coordinates[] clusters) {
    m_ClusterCoordsByGroup = (Coordinates[][]) ArrayUtils.appendArray(m_ClusterCoordsByGroup, clusters);
  }

  public void setClustersForGroup(int groupNumber, int multiplicity, boolean activity, Coordinates[] clusters) {
    //groupNumber = m_ClusterCoordsByGroup.length;
    if (groupNumber >= m_ClusterCoordsByGroup.length) {
      int increment = groupNumber - m_ClusterCoordsByGroup.length + 1;
      m_ClusterCoordsByGroup = (Coordinates[][]) ArrayUtils.growArray(m_ClusterCoordsByGroup, increment);
      m_Multiplicities = ArrayUtils.growArray(m_Multiplicities, increment);
      m_ActiveGroups = ArrayUtils.growArray(m_ActiveGroups, increment);
    }
    m_ClusterCoordsByGroup[groupNumber]= clusters;
    m_Multiplicities[groupNumber] = multiplicity;
    m_ActiveGroups[groupNumber] = activity;
  }
  
  public boolean isGroupActive(int groupNum) {
    if (groupNum >= m_ActiveGroups.length) {return false;}
    return m_ActiveGroups[groupNum];
  }
}
