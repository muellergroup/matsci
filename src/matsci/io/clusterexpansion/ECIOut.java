package matsci.io.clusterexpansion;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

import matsci.structure.decorate.function.ce.clusters.*;
import matsci.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class ECIOut {

  private int[] m_ClusterOrbitNumbers = new int[0];
  private int[] m_FunctionOrbitNumbers = new int[0];
  private double[] m_CorrelationCoefficients = new double[0];
  private double[] m_ECIs = new double[0];

  public ECIOut(String fileName) {
    try {
      sizeArrays(fileName);
      readFile(fileName);
    }
    catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public ECIOut(ClusterGroup[] groups) {
    
    int numEntries = 0; 
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      numEntries += groups[groupIndex].numFunctionGroups();
    }
    m_ClusterOrbitNumbers = new int[numEntries];
    m_FunctionOrbitNumbers = new int[numEntries];
    m_CorrelationCoefficients = new double[numEntries];
    m_ECIs = new double[numEntries];
    int entryNum = 0;
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      ClusterGroup group = groups[groupIndex];
      m_ClusterOrbitNumbers[entryNum] = group.getGroupNumber();
      for(int functNum = 0; functNum < group.numFunctionGroups(); functNum++) {
        m_FunctionOrbitNumbers[entryNum] = functNum;
        m_CorrelationCoefficients[entryNum] = group.getCorrelationCoefficient(functNum);
        m_ECIs[entryNum] = group.getECI(functNum);
        entryNum++;
      }
    }
  }
  
  private void sizeArrays(String fileName) throws IOException {
    LineNumberReader lineReader = new LineNumberReader(new FileReader(fileName));
    int numEntries = 0;
    String line = lineReader.readLine();
    while (line != null) {
      StringTokenizer st = new StringTokenizer(line);
      if (st.countTokens() >= 3) {numEntries++;}
      line = lineReader.readLine();
    }
    m_ClusterOrbitNumbers = new int[numEntries];
    m_FunctionOrbitNumbers = new int[numEntries];
    m_CorrelationCoefficients = new double[numEntries];
    m_ECIs = new double[numEntries];
    lineReader.close();
  }

  private void readFile(String fileName) throws IOException {

    LineNumberReader lineReader = new LineNumberReader(new FileReader(fileName));
    String line = lineReader.readLine();
    int entryNum = 0;
    
    while (line != null && line.trim().length() != 0) {
      int clusterNumber = 0;
      int functionNumber = 0;
      StringTokenizer st = new StringTokenizer(line);
      double correlationCoefficient = Double.parseDouble(st.nextToken());
      double eci = Double.parseDouble(st.nextToken());
      String clusterName = st.nextToken();
      if (!clusterName.equals("empty")) {
        clusterNumber = Integer.parseInt(st.nextToken()); 
        if (st.countTokens() >= 2) {
          String functionName = st.nextToken();
          functionNumber = Integer.parseInt(st.nextToken());
        }
      }
      m_ClusterOrbitNumbers[entryNum] = clusterNumber;
      m_FunctionOrbitNumbers[entryNum] = functionNumber;
      m_CorrelationCoefficients[entryNum] = correlationCoefficient;
      m_ECIs[entryNum] = eci;
      line = lineReader.readLine();
      entryNum++;
    }
    lineReader.close();
  }
  
  public void writeFile(String fileName) {
    
      try {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        DecimalFormat formatter = new DecimalFormat("0.#####E0");
        for (int entryNum = 0; entryNum < m_ClusterOrbitNumbers.length; entryNum++) {
          String correlations = formatter.format(m_CorrelationCoefficients[entryNum]);
          correlations = StringUtils.rightAlign(correlations, 13);
          writer.write(correlations);
          String eci = formatter.format(m_ECIs[entryNum]);
          eci = StringUtils.rightAlign(eci, 15);
          writer.write(eci);
          int orbitNumber = m_ClusterOrbitNumbers[entryNum];
          if (orbitNumber == 0) {
            writer.write("  empty       ");
            this.newLine(writer);
            continue;
          }
          writer.write("  cluster#");
          writer.write(StringUtils.rightAlign(orbitNumber, 4));
          writer.write("  function#");
          writer.write(StringUtils.rightAlign(m_FunctionOrbitNumbers[entryNum], 4));
          this.newLine(writer);
        }
        writer.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
  }
  
  protected void newLine(BufferedWriter writer) throws IOException {
    writer.write("\n");
  }

  public int numEntries() {
    return m_CorrelationCoefficients.length;
  }

  public double getCorrelationCoefficient(int entryNumber) {
    return m_CorrelationCoefficients[entryNumber];
  }

  public double getECI(int entryNumber) {
    return m_ECIs[entryNumber];
  }
  
  public int getClusterOrbit(int entryNumber) {
    return m_ClusterOrbitNumbers[entryNumber];
  }
  
  public int getFunctionOrbit(int entryNumber) {
    return m_FunctionOrbitNumbers[entryNumber];
  }
  
}
