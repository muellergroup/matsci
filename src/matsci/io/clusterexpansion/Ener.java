/*
 * Created on Apr 5, 2006
 *
 */
package matsci.io.clusterexpansion;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import matsci.util.*;
import matsci.util.arrays.*;

public class Ener {
  
  private double[][] m_Concentrations = new double[0][];
  private double[] m_Energies = new double[0];
  private int[] m_StructureNumbers = new int[0];

  public Ener(double[][] concentrations, double[] energies, boolean[] activeStructures) {
    m_Concentrations = ArrayUtils.copyArray(concentrations);
    m_Energies = ArrayUtils.copyArray(energies);
    m_StructureNumbers = ArrayUtils.getSetBits(activeStructures);
  }
  
  public void writeFile(String fileName) throws IOException {
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
    DecimalFormat formatter = new DecimalFormat("0.000");
    for (int structNum = 0; structNum < m_Concentrations.length; structNum++) {
      double[] concentrations = m_Concentrations[structNum];
      String concentrationZero = formatter.format(concentrations[0]);
      writer.write(StringUtils.rightAlign(concentrationZero, 13));
      String energy = formatter.format(m_Energies[structNum]);
      writer.write(StringUtils.rightAlign(energy, 15));
      writer.write("  structure");
      writer.write(StringUtils.rightAlign(m_StructureNumbers[structNum], 5));
      for (int specNum = 1; specNum < concentrations.length; specNum++) {
        String concentration = formatter.format(concentrations[specNum]);
        writer.write(StringUtils.rightAlign(concentration, 15));
      }
      this.newLine(writer);
    }
    writer.close();
  }
  
  protected void newLine(BufferedWriter writer) throws IOException {
    writer.write("\n");
  }
}
