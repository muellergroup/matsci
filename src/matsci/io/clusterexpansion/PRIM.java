package matsci.io.clusterexpansion;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

import matsci.Species;
import matsci.io.vasp.VASPCell;
import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.structure.IStructureData;
import matsci.structure.decorate.DecorationTemplate;
import matsci.util.MSMath;
import matsci.util.StringUtils;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class PRIM extends VASPCell {

  private Species[][] m_AllowedSpeciesPerSite;
  private boolean m_WriteElementsOnly = true;
  private boolean m_WriteVacancies = false;

  public PRIM(IStructureData data, Species[] speciesInOrder) {
    super(data, speciesInOrder);
    m_AllowedSpeciesPerSite = new Species[this.numDefiningSites()][];
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      m_AllowedSpeciesPerSite[siteNum] = new Species[] {this.getSiteSpecies(siteNum)};
    }
  }
  
  public PRIM(IStructureData data) {
    super(data);
    m_AllowedSpeciesPerSite = new Species[data.numDefiningSites()][];
    int siteCoordIndex = 0;
    for (int speciesType = 0; speciesType < m_SitesPerSpecies.length; speciesType++) {
      Species currSpecies = m_Species[speciesType];
      for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
        if (data.getSiteSpecies(siteNum) == currSpecies) {
          m_AllowedSpeciesPerSite[siteCoordIndex++] = new Species[] {currSpecies};
        }
      }
    }
  }
  
  public PRIM(DecorationTemplate template) {
	    super(template.getBaseStructure());
	    m_AllowedSpeciesPerSite = template.getAllowedSpecies();
  }
  
  public PRIM(IStructureData data, boolean alphabetizeSpecies) {
    super(data, alphabetizeSpecies);
    m_AllowedSpeciesPerSite = new Species[this.numDefiningSites()][];
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      m_AllowedSpeciesPerSite[siteNum] = new Species[] {this.getSiteSpecies(siteNum)};
    }
  }
  
  public PRIM(String fileName) {
    this(fileName, true); // Changed default to "true" for PRIM files, because species might be read from allowed species.
  }
  
  public PRIM(String fileName, Species[] speciesInOrder) {

    super (fileName, speciesInOrder);

    try {
      readFile(fileName);
    }
    catch (IOException e) {
      throw new RuntimeException("Error reading PRIM file " + fileName, e);
    }
  }
  
  public PRIM(String fileName, boolean allowUnknownSpecies) {
    super(fileName, allowUnknownSpecies);
    try {
      readFile(fileName);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public PRIM(StringReader reader) {
    super(true);
    try {
      readFile(reader);
    }
    catch (Exception e) {
      throw new RuntimeException("Error reading PRIM from string: " + reader.toString(), e);
    }
  }
  
  protected void readFile(String fileName) throws IOException {

    FileReader fileReader = new FileReader(fileName);
    this.readFile(fileReader);
    
    fileReader.close();
    
  }

  protected void readFile(Reader reader) throws IOException {

    LineNumberReader lineReader = new LineNumberReader(reader);

    readComment(lineReader);
    readScaleFactor(lineReader);
    readLatticeVectors(lineReader);
    readSpeciesInOrder(lineReader);
    readSitesPerSpecies(lineReader);
    readSelectiveDynamics(lineReader);
    readBasis(lineReader);
    readSites(lineReader);

  }

  // TODO move this into VASPCell  
  public void writeVacancies(boolean value) {
	  m_WriteVacancies = value;
  }

  // TODO move this into VASPCell
  public boolean writesVacancies() {
	  return m_WriteVacancies;
  }
  
  public void writeAllowedElementsOnly(boolean value) {
	  m_WriteElementsOnly = value;
  }
  
  public boolean writesAllowedElementsOnly() {
	  return m_WriteElementsOnly;
  }
  
  public void writeFile(String fileName) {
    
    try {
      FileWriter fileWriter = new FileWriter(fileName);
      BufferedWriter writer = new BufferedWriter(fileWriter);
      this.write(writer);
      writer.flush();
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException("Failed to write PRIM to file " + fileName, e);
    }
    
  }
  
  public void write(Writer writer) {
    try {
      this.writeComment(writer);
      this.writeScaleFactor(writer);
      this.writeLatticeVectors(writer);
      this.writeSitesPerSpecies(writer, m_WriteVacancies);
      this.writeSelectiveDynamics(writer);
      this.writeBasis(writer);
      this.writeSites(writer);
    } catch (IOException e) {
      throw new RuntimeException("Failed to write PRIM.", e);
    }
  }

  protected void readSites(LineNumberReader lineReader) throws IOException {

    int numSpecies = m_SitesPerSpecies.length;
    
    int totalSites = 0;
    for (int speciesNum = 0; speciesNum < numSpecies; speciesNum++) {
      totalSites += m_SitesPerSpecies[speciesNum];
    }

    m_SiteCoords = new Coordinates[totalSites];
    m_AllowedSpeciesPerSite = new Species[totalSites][];

    for (int currIndex = 0; currIndex < totalSites; currIndex++) {
      StringTokenizer st = new StringTokenizer(lineReader.readLine());

      // Read the coordiantes first
      double[] coordArray = new double[3];
      for (int coord = 0; coord < 3; coord++) {
        coordArray[coord] = Double.parseDouble(st.nextToken());
      }
      m_SiteCoords[currIndex] = new Coordinates(coordArray, this.getFormatBasis());

      // Read in the allowed species
      Species[] allowedSpecies = new Species[st.countTokens()];
      for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {
        allowedSpecies[specNum] = Species.get(st.nextToken());
      }
      m_AllowedSpeciesPerSite[currIndex] = allowedSpecies;
    }
  }
  
  protected void readSites(IStructureData data) {
    super.readSites(data);
    m_AllowedSpeciesPerSite = new Species[data.numDefiningSites()][];
  }

  public Species[] getAllowedSpecies(int index) {
    return m_AllowedSpeciesPerSite[index];
  }
  
  public Species[][] getAllowedSpecies() {
    return (Species[][]) ArrayUtils.copyArray(m_AllowedSpeciesPerSite);
  }
  
  public void setAllowedSpecies(int index, Species[] allowedSpecies) {
    m_AllowedSpeciesPerSite[index] = allowedSpecies;
  }
  
  /*protected void writeSites(BufferedWriter writer) throws IOException {
    DecimalFormat formatter = new DecimalFormat("0.0000000000000000");
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      Coordinates siteCoords = this.getSiteCoords(siteNum);
      double[] formatCoordArray = siteCoords.getCoordArray(this.getFormatBasis());
      for (int i = 0; i < siteCoords.numCoords(); i++) {
        writer.write("  ");
        double value = formatCoordArray[i];
        if (this.getFormatBasis() == CartesianBasis.getInstance()) {
          value = value / this.getScaleFactor();
        }
        value = MSMath.roundWithPrecision(value, this.getFormatPrecision()); // Gets rid of some ugly numerical errors
        writer.write(formatter.format(value));
      }
      
      Specie[] allowedSpecies = m_AllowedSpeciesPerSite[siteNum];
      for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {
        writer.write("  " + allowedSpecies[specNum].getElement());
      }
      writer.newLine();
    }
  }*/
  
  protected void writeSites(Writer writer) throws IOException {
    
    String formatString = "0.0000000000000000";
    double formatPrecision = this.getFormatPrecision();
    if (formatPrecision < 1) {
      formatString = formatString.substring(0, 2-((int) Math.round(Math.log10(formatPrecision))));
    } else {
      formatString = "0";
    }
    DecimalFormat formatter = new DecimalFormat(formatString);
    
    int siteNum = 0;
    for (int specNum = 0; specNum < this.m_SitesPerSpecies.length; specNum++) {
      int numSpecies = m_SitesPerSpecies[specNum];
      Species species = m_Species[specNum];
      for (int specSiteNum = 0; specSiteNum < numSpecies; specSiteNum++) {
        if (m_WriteVacancies || (species != Species.getVacancy())) { // Don't write out vacancies
          Coordinates siteCoords = this.getSiteCoords(siteNum);
          double[] formatCoordArray = siteCoords.getCoordArray(this.getFormatBasis());
          writer.write("  ");
          for (int i = 0; i < siteCoords.numCoords(); i++) {
            double value = formatCoordArray[i];
            if (this.getFormatBasis() == CartesianBasis.getInstance()) {
              value = value / this.getScaleFactor();
            }
            value = MSMath.roundWithPrecision(value, formatPrecision); // Gets rid of some ugly numerical errors
            String output = formatter.format(value);
            output = StringUtils.padLeft(output, formatString.length() + 3);
            output = output + "   ";
            //output = output.substring(output.length() - (formatString.length() + 3));*/
            writer.write(output);
          }
          
          Species[] allowedSpecies = m_AllowedSpeciesPerSite[siteNum];
          for (int allowedSpecNum = 0; allowedSpecNum < allowedSpecies.length; allowedSpecNum++) {
        	if (this.writesElementSymbolsOnly()) {
        		boolean seenBefore = false;
        		for (int prevSpecNum = 0; prevSpecNum < allowedSpecNum; prevSpecNum++) {
        			seenBefore |= (allowedSpecies[allowedSpecNum].getElement() == allowedSpecies[prevSpecNum].getElement());
        		}
        		if (seenBefore) {continue;}
        		writer.write(allowedSpecies[allowedSpecNum].getElement() + "  ");
        	} else {
        		writer.write(allowedSpecies[allowedSpecNum] + "  ");
        	}
          }
          newLine(writer);
        }
        siteNum++;
      }
    }

  }
  
  public Species getSiteSpecies(int siteNum) {
    Species returnSpecies = super.getSiteSpecies(siteNum);
    if ((returnSpecies == null) && (m_AllowedSpeciesPerSite[siteNum].length == 1)) {
      returnSpecies = m_AllowedSpeciesPerSite[siteNum][0];
    }
    return returnSpecies;
  }

}

