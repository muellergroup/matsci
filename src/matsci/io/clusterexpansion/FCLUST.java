package matsci.io.clusterexpansion;

import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

import matsci.location.Coordinates;
import matsci.location.basis.AbstractBasis;
import matsci.util.StringUtils;
import matsci.util.arrays.ArrayUtils;
import matsci.structure.decorate.function.ce.*;
import matsci.structure.decorate.function.ce.clusters.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class FCLUST {

//This is an array of arrays of clusterdata.  Each entry corresponds to a group.  We initialize the empty group
  private Coordinates[][][] m_ClusterCoordsByGroup = new Coordinates[][][] {new Coordinates[1][0]};
  
  private AbstractBasis m_Basis;
  private boolean[] m_ActiveGroups = new boolean[] {true};

  public FCLUST(String fileName, AbstractBasis basis) {

    m_Basis = basis;

    try {
      readFile(fileName);
    }
    catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public FCLUST(ClusterExpansion ce) {
    m_ClusterCoordsByGroup = new Coordinates[ce.getMaxGroupNumber() + 1][][];
    m_ClusterCoordsByGroup[0] = new Coordinates[1][0];
    m_ActiveGroups = new boolean[ce.getMaxGroupNumber() + 1];
    for (int groupNum = 1; groupNum < m_ClusterCoordsByGroup.length; groupNum++) {
      ClusterGroup group = ce.getClusterGroup(groupNum);
      if (group == null) {continue;}
      m_ClusterCoordsByGroup[groupNum] = new Coordinates[group.numPrimClusters()][];
      for (int primClusterNum = 0; primClusterNum < group.numPrimClusters(); primClusterNum++) {
        m_ClusterCoordsByGroup[groupNum][primClusterNum] = group.getPrimClusterCoords(primClusterNum);
      }
      m_ActiveGroups[groupNum] = true;
    }
    
    m_Basis = ce.getBaseStructure().getDirectBasis();
  }
  
  public void writeFile(String fileName) throws IOException {
    this.writeFile(fileName, m_Basis);
  }
  
  public void writeFile(String fileName, AbstractBasis basis) throws IOException {
    FileWriter fileWriter = new FileWriter(fileName);
    BufferedWriter writer = new BufferedWriter(fileWriter);
    
    for (int groupNum = 1; groupNum < m_ClusterCoordsByGroup.length; groupNum++) {
      this.writeClusterGroup(writer, basis, groupNum);
    }
    writer.close();
  }
  
  private void writeClusterGroup(BufferedWriter writer, AbstractBasis basis, int groupNum) throws IOException {
    
    DecimalFormat formatter = new DecimalFormat("0.0000000");
    
    Coordinates[][] allClusterCoords = m_ClusterCoordsByGroup[groupNum];
    if (allClusterCoords == null) {return;}
    writer.write("           " + groupNum);
    newLine(writer);
    int numSitesPerCluster = allClusterCoords.length == 0 ? 0 : allClusterCoords[0].length;
    String groupNumString = StringUtils.rightAlign(groupNum, 3);
    String sizeString = StringUtils.rightAlign(numSitesPerCluster, 5);
    String multiplicityString = StringUtils.rightAlign(allClusterCoords.length, 5);
    writer.write(groupNumString + sizeString + multiplicityString + "  1");
    newLine(writer);
    for (int primClusterNum = 0; primClusterNum < allClusterCoords.length; primClusterNum++) {
      writer.write("cluster   " + primClusterNum);
      newLine(writer);
      Coordinates[] clusterCoords = allClusterCoords[primClusterNum];
      for (int coordNum = 0; coordNum < clusterCoords.length; coordNum++) {
        double[] coordArray = clusterCoords[coordNum].getCoordArray(basis);
        for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
          String output = formatter.format(coordArray[dimNum]);
          output = "     " + output;
          output = output.substring(output.length() - 11);
          writer.write(output + "   ");
        }
        newLine(writer);
      }
    }
    
  }
  
  private void newLine(BufferedWriter writer) throws IOException {
    writer.write("\n");
  }

  private void readFile(String fileName) throws IOException {

    FileReader fileReader = new FileReader(fileName);
    LineNumberReader lineReader = new LineNumberReader(fileReader);

    boolean keepgoing = true;

    try {

      do {
        keepgoing = readClusterGroup(lineReader);
      } while (keepgoing);
    } catch (EOFException e) {}

    fileReader.close();
  }

  private boolean readClusterGroup(LineNumberReader lineReader) throws IOException {
    
    // Read the cluster information
    String groupNumberString = lineReader.readLine();
    if ((groupNumberString == null) || (groupNumberString.trim().length() == 0)) {
      return false;
    }
    String groupSummaryString = lineReader.readLine();
    StringTokenizer st = new StringTokenizer(groupSummaryString);
    int groupNumber = Integer.parseInt(st.nextToken());
    int clusterSize = Integer.parseInt(st.nextToken());
    int numClusters = Integer.parseInt(st.nextToken());
    int activityNumber = Integer.parseInt(st.nextToken()); 
    boolean activity = (activityNumber == 1);
    Coordinates[][] groupClusterCoords = new Coordinates[numClusters][];

    for (int clusterNum = 0; clusterNum < numClusters; clusterNum++) {

      Coordinates[] clusterCoords = new Coordinates[clusterSize];
      lineReader.readLine(); // Reads the "Cluster ##" line

      // Read the various sites coordinates
      for (int siteNum = 0; siteNum < clusterSize; siteNum++) {
        double[] coordArray = new double[3];
        String clusterCoordLine = lineReader.readLine();
        st = new StringTokenizer(clusterCoordLine);
        coordArray[0] = Double.parseDouble(st.nextToken());
        coordArray[1] = Double.parseDouble(st.nextToken());
        coordArray[2] = Double.parseDouble(st.nextToken());
        clusterCoords[siteNum] = new Coordinates(coordArray, m_Basis);
      }
      groupClusterCoords[clusterNum] = clusterCoords;
    }

    // The index of each group is the groupNumber.  We resize the GroupArray if necessary.
    this.setClustersForGroup(groupNumber, activity, groupClusterCoords);
    return true;
  }
  

  public Coordinates[][][] getAllClusterCoords(boolean includeEmpty) {
    if (includeEmpty) {
      return (Coordinates[][][]) ArrayUtils.copyArray(m_ClusterCoordsByGroup);
    }
    
    Coordinates[][][] returnArray = new Coordinates[m_ClusterCoordsByGroup.length - 1][][];
    System.arraycopy(m_ClusterCoordsByGroup, 1, returnArray, 0, returnArray.length);
    return returnArray;
  }

  public int numGroups() {
    int returnValue = 0;
    for (int groupNum = 0; groupNum < m_ClusterCoordsByGroup.length; groupNum++) {
      if (this.containsGroup(groupNum)) {returnValue++;}
    }
    return returnValue;
  }

  public int numClusters() {
    int returnValue = 0;
    for (int groupNum = 0; groupNum < m_ClusterCoordsByGroup.length; groupNum++) {
      returnValue += this.numClustersInGroup(groupNum);
    }
    return returnValue;
  }

  public int sitesPerClusterInGroup(int groupNumber) {
    if (! this.containsGroup(groupNumber)) {return 0;}
    Coordinates[][] clusterCoordArray = m_ClusterCoordsByGroup[groupNumber];
    if (clusterCoordArray.length == 0) {return 0;}
    return clusterCoordArray[0].length;
  }

  public int numClustersInGroup (int groupNumber) {
    if (! this.containsGroup(groupNumber)) {return 0;}
    return m_ClusterCoordsByGroup[groupNumber].length;
  }

  public Coordinates[][] getClusterCoordsForGroup(int groupNumber) {
    return m_ClusterCoordsByGroup[groupNumber];
  }

  public int getMaxGroupNumber() {
    return m_ClusterCoordsByGroup.length - 1;
  }

  public boolean containsGroup(int groupNumber) {
    return (m_ClusterCoordsByGroup[groupNumber] != null);
  }
  
  public void addClusters(Coordinates[][] clusters) {
    m_ClusterCoordsByGroup = (Coordinates[][][]) ArrayUtils.appendArray(m_ClusterCoordsByGroup, clusters);
  }

  public void setClustersForGroup(int groupNumber, boolean activity, Coordinates[][] clusters) {
    if (groupNumber >= m_ClusterCoordsByGroup.length) {
      int increment = groupNumber - m_ClusterCoordsByGroup.length + 1;
      m_ClusterCoordsByGroup = (Coordinates[][][]) ArrayUtils.growArray(m_ClusterCoordsByGroup, increment);
      m_ActiveGroups = ArrayUtils.growArray(m_ActiveGroups, increment);
    }
    m_ClusterCoordsByGroup[groupNumber]= clusters;
    m_ActiveGroups[groupNumber] = activity;
  }
  
  public boolean isGroupActive(int groupNum) {
    if (groupNum >= m_ActiveGroups.length) {return false;}
    return m_ActiveGroups[groupNum];
  }
}
