package matsci.io.clusterexpansion;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

import matsci.util.arrays.ArrayUtils;
import matsci.util.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class CorrelationsList {

  private double m_Tolerance = 1E-3;
  
  int m_NumECI;
  private double[][] m_Correlations;

  public CorrelationsList(String fileName) {

    try {
      this.sizeCorrelations(fileName);
      this.readCorrelations(fileName);
    } catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public CorrelationsList(double[][] correlations) {
    
    m_Correlations = ArrayUtils.copyArray(correlations);
    m_NumECI = (correlations.length == 0) ? 0 : correlations[0].length;
    
  }
  
  public CorrelationsList(int numECI) {
    m_NumECI = numECI;
    m_Correlations = new double[0][];
  }
  
  public CorrelationsList(double[][][] correlations) {
    m_Correlations = new double[correlations.length][];
    m_NumECI = 0;
    if (correlations.length == 0) {return;}
    double[][] firstCorrelations = correlations[0];
    for (int groupNum = 0; groupNum < firstCorrelations.length; groupNum++) {
      if (firstCorrelations[groupNum] == null) {continue;}
      m_NumECI += firstCorrelations[groupNum].length;
    }
    for (int entryNum = 0; entryNum < correlations.length; entryNum++) {
      m_Correlations[entryNum] = new double[m_NumECI];
      int eciNum = 0;
      double[][] entryCorrelations = correlations[entryNum];
      for (int groupNum = 0; groupNum < entryCorrelations.length; groupNum++) {
        if (entryCorrelations[groupNum] == null) {continue;}
        for (int functNum = 0; functNum < entryCorrelations[groupNum].length; functNum++) {
          m_Correlations[entryNum][eciNum++] = entryCorrelations[groupNum][functNum];
        }
      }
    }
  }
  
  public void writeCorrelations(String fileName) throws IOException {
    
    DecimalFormat formatter = new DecimalFormat("0.000000000000");
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
    for (int structNum = 0; structNum < m_Correlations.length; structNum++) {
      double[] correlations = m_Correlations[structNum];
      for (int clustNum = 0; clustNum < correlations.length; clustNum++) {
        String correlationsString = formatter.format(correlations[clustNum]);
        writer.write(StringUtils.rightAlign(correlationsString, 18));
      }
      writer.write("\n");
    }
    writer.flush();
    writer.close();
  }
  
  private void sizeCorrelations(String fileName) throws IOException {

    LineNumberReader lineReader = new LineNumberReader(new FileReader(fileName));

    // Count the number of ECI, using the last line
    String lastLine = null;
    String nextLine = lineReader.readLine();
    while (nextLine != null && nextLine.trim().length() != 0) {
      lastLine = nextLine;
      nextLine = lineReader.readLine();
    }
    if (lastLine == null) {
      m_NumECI= 0;
      m_Correlations = new double[0][];
      return;
    }
    StringTokenizer st = new StringTokenizer(lastLine);
    m_NumECI = 0;
    while (st.hasMoreTokens()) {
      try {
        Double.parseDouble(st.nextToken());
      } catch (NumberFormatException e) {
        break;
      }
      m_NumECI++;
    }
    lineReader.close();
    
    lineReader = new LineNumberReader(new FileReader(fileName));
    // Count the number of structures
    int numStructures = 0;
    String line = lineReader.readLine();
    while (line != null && line.trim().length() != 0) {
      st = new StringTokenizer(line);
      if (st.countTokens() >= m_NumECI) {numStructures++;}
      line = lineReader.readLine();
    }
    lineReader.close();
    m_Correlations = new double[numStructures][m_NumECI];
  }

  private void readCorrelations(String fileName) throws IOException {
    LineNumberReader lineReader = new LineNumberReader(new FileReader(fileName));

    int structNum = 0;
    String line = lineReader.readLine();
    while (line != null && line.trim().length() != 0) {
      StringTokenizer st = new StringTokenizer(line);
      if (st.countTokens() < m_NumECI) {
        System.out.println(st.countTokens());
        line = lineReader.readLine();
        continue;
      }
      for (int eciNum = 0; eciNum < m_NumECI; eciNum++) {
        double value = Double.parseDouble(st.nextToken());
        m_Correlations[structNum][eciNum] = value;
      }
      line = lineReader.readLine();
      structNum++;
    }
    lineReader.close();
  }

  public int numStructures() {
    return m_Correlations.length;
  }

  public double[][] getCorrelations() {

    double[][] returnArray = new double[m_Correlations.length][];
    for (int structureNum = 0; structureNum < returnArray.length; structureNum++) {
      returnArray[structureNum] = this.getCorrelations(structureNum);
    }
    return returnArray;
  }

  public double[] getCorrelations(int structureIndex) {
    return ArrayUtils.copyArray(m_Correlations[structureIndex]);
  }
  
  public double getTolerance() {
    return m_Tolerance;
  }
  
  public void setTolerance(double tolerance) {
    m_Tolerance =tolerance;
  }
  
  public boolean addCorrelations(double[] correlations, boolean allowDuplicates) {
    if (correlations.length != m_NumECI) {
      throw new RuntimeException("Size of correlations array differs from size of known correlations");
    }
    
    if (!allowDuplicates && this.containsCorrelaions(correlations)) {return false;}
    m_Correlations = ArrayUtils.appendElement(m_Correlations, correlations);
    return true;
  }
  
  public boolean addCorrelations(double[][] correlations) {
    if (correlations.length == 0) {return false;}
    
    if (correlations[0].length != m_NumECI) {
      throw new RuntimeException("Size of correlations array differs from size of known correlations");
    }
    
    m_Correlations = ArrayUtils.appendArray(m_Correlations, correlations);
    return true;
  }
  
  public boolean containsCorrelaions(double[] correlations) {

    if (correlations.length != m_NumECI) {return false;}
    
    for (int rowNum = 0; rowNum < m_Correlations.length; rowNum++) {
      double[] prevCorrelations = m_Correlations[rowNum];
      for (int colNum = 0; colNum < correlations.length; colNum++) {
        double delta = Math.abs(prevCorrelations[colNum] - correlations[colNum]);
        if (delta > m_Tolerance) {return true;}
      }
    }
    return false;
  }
  
  public void removeCorrelations(int structureIndex) {
    m_Correlations = ArrayUtils.removeElement(m_Correlations, structureIndex);
  }

}