/*
 * Created on Mar 23, 2006
 *
 */
package matsci.io.clusterexpansion;

import java.io.*;
import java.util.*;

public class FSPECS {

  private double m_SlopeChangeWeight;
  private int[] m_StructIndices = new int[0];
  private double[] m_HullDistanceWeights = new double[0];
  private String[] m_StructLabels = new String[0];
  
  public FSPECS(String fileName) {
    try {
      this.sizeArrays(fileName);
      this.readFile(fileName);
    } catch (IOException e) {
      throw new RuntimeException("Failed to read FSPECS file " + fileName + ".", e);
    }
  }
  
  private void sizeArrays(String  fileName) throws IOException {
    LineNumberReader reader = new LineNumberReader(new FileReader(fileName));
    int numEntries = 0;
    reader.readLine(); // Comment
    reader.readLine(); // slope change weight
    reader.readLine(); // comment
    String line = reader.readLine(); 
    while (line != null && line.trim().length() != 0) {
      numEntries++;
      line = reader.readLine();
    }
    m_StructIndices = new int[numEntries];
    m_HullDistanceWeights = new double[numEntries];
    m_StructLabels = new String[numEntries];
    reader.close();
  }
  
  private void readFile(String fileName) throws IOException {
    LineNumberReader reader = new LineNumberReader(new FileReader(fileName));
    reader.readLine(); // Comment
    m_SlopeChangeWeight =Double.parseDouble(reader.readLine().trim()); // slope change weight
    reader.readLine(); // comment
    String line = reader.readLine();
    int entryNum = 0;
    while (line != null && line.trim().length() != 0) {
      StringTokenizer st = new StringTokenizer(line);
      m_StructIndices[entryNum] = Integer.parseInt(st.nextToken());
      m_HullDistanceWeights[entryNum] = Double.parseDouble(st.nextToken());
      m_StructLabels[entryNum] = st.nextToken();
      line = reader.readLine();
      entryNum++;
    }
    reader.close();
  }
  
  public int numEntries() {
    return m_StructIndices.length;
  }
  
  public double getSlopeChangeWeight() {
    return m_SlopeChangeWeight;
  }
  
  public int getStructIndex(int entryNum) {
    return m_StructIndices[entryNum];
  }
  
  public double getHullDistanceWeight(int entryNum) {
    return m_HullDistanceWeights[entryNum];
  }
  
  public String getLabel(int entryNum) {
    return m_StructLabels[entryNum];
  }

}
