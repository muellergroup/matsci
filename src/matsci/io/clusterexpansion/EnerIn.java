package matsci.io.clusterexpansion;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.StringTokenizer;
import java.util.Arrays;

import matsci.util.arrays.ArrayUtils;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class EnerIn {

  private String m_Comment = "No Comment";
  private String m_FileName;
  private int[] m_EntryNumbers;
  private double[] m_Energies;
  private double[] m_Weights;
  private String[] m_Labels;
  private int[] m_GroupNumbers;

  public EnerIn(String fileName) {
    m_FileName = fileName;

    try {
      int numStructures = this.countStructures();
      m_EntryNumbers = new int[numStructures];
      m_Energies = new double[numStructures];
      Arrays.fill(m_Energies, Double.NaN);
      m_Weights = new double[numStructures];
      m_Labels = new String[numStructures];
      m_GroupNumbers = new int[numStructures];
      Arrays.fill(m_Energies, Double.NaN);
      Arrays.fill(m_Weights, 1);
      this.readEnergies();
    } catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
  }
  
  public EnerIn(int numStructures) {
    m_EntryNumbers = new int[numStructures];
    m_Energies = new double[numStructures];
    m_Weights = new double[numStructures];
    m_GroupNumbers = new int[numStructures];
    m_Labels = new String[numStructures];
  }
  
  public void setEntryNumber(int structureIndex, int structureNum) {
    m_EntryNumbers[structureIndex] = structureNum;
  }
  
  public void setEnergy(int structureIndex, double energy) {
    m_Energies[structureIndex] = energy;
  }
  
  public void setWeight(int structureIndex, double weight) {
    m_Weights[structureIndex] = weight;
  }
  
  public void setGroupNumber(int structureIndex, int groupNumber) {
    m_GroupNumbers[structureIndex] = groupNumber;
  }
  
  public void setLabel(int structureIndex, String label) {
    m_Labels[structureIndex] = label;
  }
  
  public void setComment(String comment) {
    m_Comment = comment;
  }

  public void writeFile(String fileName) throws IOException {
    
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
    writer.write(m_Comment + "\n");
    for (int entryIndex = 0; entryIndex < m_EntryNumbers.length; entryIndex++) {
      writer.write(m_EntryNumbers[entryIndex] + "\t");
      writer.write(m_Energies[entryIndex] + "\t");
      writer.write(m_Weights[entryIndex] + "\t");
      writer.write(m_Labels[entryIndex] + "\t");
      writer.write(m_GroupNumbers[entryIndex] + "\t");
      writer.write("\n");
    }
    writer.flush();
    writer.close();
  }

  private int countStructures() throws IOException {
    LineNumberReader lineReader = new LineNumberReader(new FileReader(m_FileName));

    /** Toss out the first line */
    String firstLine = lineReader.readLine();

    int numEnergies = 0;
    String line = lineReader.readLine();
    while (line != null) {
      if (line.trim().length() != 0) {numEnergies++;}
      line = lineReader.readLine();
    }

    lineReader.close();
    return numEnergies;
  }

  private void readEnergies() throws IOException {
    LineNumberReader lineReader = new LineNumberReader(new FileReader(m_FileName));

    m_Comment = lineReader.readLine();

    String line = lineReader.readLine();
    int structNum = 0;
    while (line != null && line.trim().length() != 0) {
      StringTokenizer st = new StringTokenizer(line);
      //if (st.countTokens() < 3) {continue;}
      m_EntryNumbers[structNum] = Integer.parseInt(st.nextToken());
      //m_StructureFileNames[structNum] = st.nextToken();
      if (st.hasMoreTokens()) {m_Energies[structNum] = Double.parseDouble(st.nextToken());}
      if (st.hasMoreTokens()) {m_Weights[structNum] = Math.max(Double.parseDouble(st.nextToken()), 0);}
      if (st.hasMoreTokens()) {m_Labels[structNum] = st.nextToken();}
      if (st.hasMoreTokens()) {m_GroupNumbers[structNum] = Integer.parseInt(st.nextToken());}
      structNum++;
      /*m_Energies[structNum] = Double.parseDouble(st.nextToken());
      m_Weights[structNum] = Math.max(Double.parseDouble(st.nextToken()), 0);
      if (st.hasMoreTokens()) {
        m_Labels[structNum] = st.nextToken();
      }
      if (st.hasMoreTokens()) {
        m_GroupNumbers[structNum] = Integer.parseInt(st.nextToken());
      }
      structNum++;*/
      line = lineReader.readLine();
    }
    lineReader.close();
  }
  
  public String getComment() {
    return m_Comment;
  }
  
  public String getLabel(int entryIndex) {
    return m_Labels[entryIndex];
  }

  public double[] getEnergies() {
    return ArrayUtils.copyArray(m_Energies);
  }
  
  public int getGroupNumber(int entryIndex) {
    return m_GroupNumbers[entryIndex];
  }
  
  public int getEntryNumber(int entryIndex) {
    return m_EntryNumbers[entryIndex];
  }
  
  public int[] getEntryNumbers() {
    return ArrayUtils.copyArray(m_EntryNumbers);
  }

  public int numEntries() {
    return m_Energies.length;
  }

  public double getEnergy(int structIndex) {
    return m_Energies[structIndex];
  }
  
  public void removeEnergy(int structureIndex) {
    m_Energies = ArrayUtils.removeElement(m_Energies, structureIndex);
  }
  
  public double getWeight(int structIndex) {
    return m_Weights[structIndex];
  }
  
  public double[] getWeights() {
    return ArrayUtils.copyArray(m_Weights);
  }
  
  public int[] getGroupNumbers() {
    return ArrayUtils.copyArray(m_GroupNumbers);
  }
}