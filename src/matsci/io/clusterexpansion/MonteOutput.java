/*
 * Created on Apr 10, 2006
 *
 */
package matsci.io.clusterexpansion;

import java.io.*;
import java.text.DecimalFormat;

import matsci.*;
import matsci.util.arrays.*;
import matsci.util.*;
import matsci.engine.monte.*;

public class MonteOutput {

  private double[] m_Temperatures = new double[0]; // in K
  private double[] m_TriggerRatios = new double[0];
  private double[] m_AverageValues = new double[0];
  private double[] m_AverageSqValues = new double[0];
  private double[] m_TrackedValues = new double[0];
  private double[] m_RecalculatedValues = new double[0];
  private Species[] m_SigmaSpecies = new Species[0];
  private double[][] m_ChemicalPotentials = new double[0][];
  
  private double m_TempToValue = 1;
  private int m_SuperCellSize = 1;
  
  public MonteOutput(Species[] sigmaSpecies, double tempConversion, int superCellSize) {
    m_SigmaSpecies = (Species[]) ArrayUtils.copyArray(sigmaSpecies);
    m_TempToValue = tempConversion;
    m_SuperCellSize = superCellSize;
  }
  
  public void addMonteOuput(double temp, BasicRecorder recorder, double trackedValue, double recalculatedValue, double[] chemPots) {
    this.addMonteOutput(temp, recorder.getTriggerRatio(), recorder.getAverageValue(), recorder.getAverageSquaredValue(), trackedValue, recalculatedValue, chemPots);
  }
  
  public void addMonteOutput(double temp, double triggerRatio, double averageValue, double averageSqValue, double trackedValue, double recalculatedValue, double[] chemPots) {
    m_Temperatures = ArrayUtils.appendElement(m_Temperatures, temp);
    m_TriggerRatios = ArrayUtils.appendElement(m_TriggerRatios, triggerRatio);
    m_AverageValues = ArrayUtils.appendElement(m_AverageValues, averageValue);
    m_AverageSqValues = ArrayUtils.appendElement(m_AverageSqValues, averageSqValue);
    m_TrackedValues = ArrayUtils.appendElement(m_TrackedValues, trackedValue);
    m_RecalculatedValues = ArrayUtils.appendElement(m_RecalculatedValues, recalculatedValue);
    m_ChemicalPotentials = ArrayUtils.appendElement(m_ChemicalPotentials, ArrayUtils.copyArray(chemPots));
  }
  
  public void writeFile(String fileName) throws IOException {
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
    writer.write("Temperature(Temp Units)     Temperature(Value Units)      Event Trigger Ratio     Average Value     Average Per Unit Value     Average Sq. Value     Tracked Final Value      Recalculated Final Value");
    for (int specNum = 0; specNum < m_SigmaSpecies.length; specNum++) {
      String specHeading = "    Chem. Pot. for " + m_SigmaSpecies[specNum];
      writer.write(StringUtils.rightAlign(specHeading, 24));
    }
    this.newLine(writer);
    DecimalFormat formatter = new DecimalFormat("0.0000000");
    for (int entryNum = 0; entryNum < m_Temperatures.length; entryNum++) {
      String tempString = formatter.format(m_Temperatures[entryNum]);
      tempString = StringUtils.rightAlign(tempString, 22);
      String evTempString = formatter.format(m_Temperatures[entryNum] * m_TempToValue);
      evTempString = StringUtils.rightAlign(evTempString, 30);
      String triggerString = formatter.format(m_TriggerRatios[entryNum]);
      triggerString = StringUtils.rightAlign(triggerString, 25);
      String averageString = formatter.format(m_AverageValues[entryNum]);
      averageString = StringUtils.rightAlign(averageString, 18);
      String averagePerUnitString = formatter.format(m_AverageValues[entryNum] / m_SuperCellSize);
      averagePerUnitString = StringUtils.rightAlign(averagePerUnitString, 27);
      String averageSqString = formatter.format(m_AverageSqValues[entryNum]);
      averageSqString = StringUtils.rightAlign(averageSqString, 22);
      String trackedString = formatter.format(m_TrackedValues[entryNum]);
      trackedString = StringUtils.rightAlign(trackedString, 24);
      String recalculatedString = formatter.format(m_RecalculatedValues[entryNum]);
      recalculatedString = StringUtils.rightAlign(recalculatedString, 30);
      writer.write(tempString + evTempString + triggerString + averageString + averagePerUnitString + averageSqString + trackedString + recalculatedString);
      for (int specNum = 0; specNum < m_SigmaSpecies.length; specNum++) {
        String chemPotString = formatter.format(m_ChemicalPotentials[entryNum][specNum]);
        chemPotString = StringUtils.rightAlign(chemPotString, 24);
        writer.write(chemPotString);
      }
      this.newLine(writer);
    }
    writer.close();
  }
  
  private void newLine(BufferedWriter writer) throws IOException {
    writer.write("\n");
  }

}
