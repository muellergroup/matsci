/*
 * Created on Mar 7, 2006
 *
 */
package matsci.io.app.param;

import java.util.HashMap;
import java.util.Iterator;

import matsci.io.app.log.Status;
import matsci.util.arrays.*;

/**
 * This is a base class that provides a bridge between the Parameters class, which contains raw text values, and 
 * applications, which require that these text values be parsed.  Each class that makes use of user-assigned 
 * parameters should create a subclass of this class, in which default values are stored and text values 
 * may be parsed.
 * 
 * @author Tim Mueller
 *
 */
public abstract class ParameterParser {
  
  private Parameters m_Parameters;

  public ParameterParser() {
    this(new Parameters(new String[0], null));
  }
 
  /**
   * 
   * @param file The parameters object from which this parser should be initialized.  If "null" is provided, it 
   * is assumed that no user-assigned parameter values have been provided.
   */
  public ParameterParser(Parameters file) {
    if (file == null) {
      file = new Parameters(new String[0], null);
    }
    m_Parameters = file;
    this.setDefaults(this.getDefaultValues());
  }
  
  /**
   * 
   * @param file The parameters object from which this parser should be initialized.  If "null" is provided, it 
   * is assumed that no user-assigned parameter values have been provided.
   * @param label A label for the set of parameters being loaded.
   */
  public ParameterParser(Parameters file, String label) {
    Status.flow("Loading " + label + " parameters.");
    if (file == null) {
      file = new Parameters(new String[0], null);
    }
    m_Parameters = file;
    this.setDefaults(this.getDefaultValues());
  }
  
  /**
   * This method should be called by subclasses to assign default values.
   * 
   * @param defaults The i<i>th</i> default value corresponds to the i<i>th</i> key.
   */
  protected void setDefaults(HashMap<String, String[]> defaults) {
    //Status.flow("Setting unspecified parameters to default values: ");
    //Status.basic("***************** Default Parameters ******************");
    Status.basic("----------------- Default Parameters -----------------");
    Iterator<String> iterator = defaults.keySet().iterator();
    while (iterator.hasNext()) {
      String key = iterator.next();
      String[] value = defaults.get(key);
      if (!m_Parameters.containsValueFor(key)) {
        m_Parameters.storeParameter(key, value);
      }
    }
    //Status.basic("********* End Default Parameters ***********");

    Status.basic("--------------- End Default Parameters ---------------");
  }
  
  /**
   * This method should be called by subclasses to assign default values.
   * 
   * @param keys
   * @param defaultValues The i<i>th</i> default value corresponds to the i<i>th</i> key.
   */
  protected void setDefaults(String[] keys, String[][] defaultValues) {
    //Status.flow("Setting unspecified parameters to default values: ");
    //Status.basic("***************** Default Parameters ******************");
    Status.basic("----------------- Default Parameters -----------------");
    for (int keyNum = 0; keyNum < keys.length; keyNum++) {
      String key = keys[keyNum];
      if (!m_Parameters.containsValueFor(key)) {
        m_Parameters.storeParameter(key, defaultValues[keyNum]);
      }
    }
    //Status.basic("********* End Default Parameters ***********");

    Status.basic("--------------- End Default Parameters ---------------");
  }
  
  /**
   * 
   * @return The parameters class on which this parser is based.
   */
  public Parameters getParameters() {
    return m_Parameters;
  }
  
  /**
   * This utility method can be used if it is known that the value of a parameter is not an array.
   * 
   * @param key The key for the requested parameter.
   * @return The non-array value of the requested parameter.
   */
  public String getSingleLineParameter(String key) {
    String[] params = m_Parameters.getParameter(key);
    if (params == null) {return null;}
    return params[0];
  }
  
  /**
   * 
   * @param key The key for the requested parameter.
   * @return The array of string values for the parameter.
   */
  public String[] getParameter(String key) {
    return ArrayUtils.copyArray(m_Parameters.getParameter(key));
  }
  
  /**
   * This utility method can be used if it is known that the value of a parameter is not an array.
   * 
   * @param key The key for the requested parameter.
   * @return "true" if and only if the parameter value starts with the letter "t" (not case-sensitive).
   */
  public boolean isTrue(String key) {
    return this.getSingleLineParameter(key).toLowerCase().startsWith("t");
  }
  
  /**
   * This utility method can be used if it is known that the value of a parameter is not an array.
   * 
   * @param key The key for the requested parameter.
   * @return The value of the parameter converted to an int.
   */
  public int intValue(String key) {
    return Integer.parseInt(this.getSingleLineParameter(key));
  }
  
  /**
   * This utility method can be used if it is known that the value of a parameter is not an array.
   * 
   * @param key The key for the requested parameter.
   * @return The value of the parameter converted to an long.
   */
  public long longValue(String key) {
    return Long.parseLong(this.getSingleLineParameter(key));
  }
  
  /**
   * 
   * This utility method can be used if the value of a parameter may be an array.
   * 
   * @param key The key for the requested parameter.
   * @return An array in which each parameter value has been converted to an int.
   */
  public int[] intArrayValue(String key) {
    String[] params = this.getParameter(key);
    int[] returnArray = new int[params.length];
    for (int paramNum = 0; paramNum < params.length; paramNum++) {
      returnArray[paramNum] = Integer.parseInt(params[paramNum]);
    }
    return returnArray;
  }
  
  /**
   * This utility method can be used if it is known that the value of a parameter is not an array.
   * 
   * @param key The key for the requested parameter.
   * @return The value of the parameter converted to a double.
   */
  public double doubleValue(String key) {
    return Double.parseDouble(this.getSingleLineParameter(key));
  }
  
  /**
   * 
   * This utility method can be used if the value of a parameter may be an array.
   * 
   * @param key The key for the requested parameter.
   * @return An array in which each parameter value has been converted to a double.
   */
  public double[] doubleArrayValue(String key) {
    String[] params = this.getParameter(key);
    double[] returnArray = new double[params.length];
    for (int paramNum = 0; paramNum < params.length; paramNum++) {
      returnArray[paramNum] = Double.parseDouble(params[paramNum]);
    }
    return returnArray;
  }
  
  protected abstract HashMap<String, String[]> getDefaultValues();
  
}
