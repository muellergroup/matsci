/*
 * Created on Feb 24, 2006
 *
 */
package matsci.io.app.param;

import matsci.io.app.log.*;
import matsci.util.arrays.*;
import java.io.*;
import java.util.*;

/**
 * This is a class for reading application parameters from either the command line or a configuration file.  
 * 
 * @author Tim Mueller
 *
 */
public class Parameters {

  private HashMap m_Parameters = new HashMap();
  
  /**
   * 
   * @param args A set of arguments, such as those that may have come from the command line.  Arguments that are 
   * keys should start with "-", and the following arguments are the values associated with the key.  A special
   * key is "PARAMETERS_LOCATION", the value for which provides the location of a file from which additional
   * parameters should be read.  
   */
  public Parameters(String[] args) {
    String key = null;
    String[] values = new String[0];
    
    Status.basic("---------------- Specified Parameters ----------------");
    for (int argNum = 0; argNum < args.length; argNum++) {
      String arg = args[argNum];
      if (arg.startsWith("-")) {
        if (key != null) {
          this.storeParameter(key, values);
          //m_Parameters.put(key.toUpperCase(), values);
        }
        values = new String[0];
        key = arg.substring(1, arg.length());
      } else {
        if (key == null) {
          throw new RuntimeException("First command line parameter is not a switch: '" + arg + "'.");
        }
        values = (String[]) ArrayUtils.appendElement(values, arg);
      }
    }
    if (key != null) {
      this.storeParameter(key, values);
      //m_Parameters.put(key.toUpperCase(), values);
    }
    Status.basic("-------------- End Specified Parameters --------------");

  }
  
  /**
   * 
   * @param args A set of arguments, such as those that may have come from the command line.  Arguments that are 
   * keys should start with "-", and the following arguments are the values associated with the key.  A special
   * key is "PARAMETERS_LOCATION", the value for which provides the location of a file from which additional
   * parameters should be read.  If this argument is not provided, the application will look for a file named 
   * "parameters.txt."  If this file is not found, a warning message is written.
   * @param defaultFileLocation The path to a default location for the parameters file, if it is not given in 
   * args.
   */
  public Parameters(String[] args, String defaultFileLocation) {
    
    this(args);
    
    String paramLocationKey = "PARAMETERS_LOCATION";
    if ((m_Parameters.get(paramLocationKey) == null) && (defaultFileLocation != null)) {
      this.storeParameter(paramLocationKey, new String[] {defaultFileLocation});
      //m_Parameters.put(paramLocationKey, new String[] {defaultFileLocation});
    }
    
    String[] paramLocation = (String[]) m_Parameters.get(paramLocationKey);
    if (paramLocation != null) {
      try {
        this.readParameters(paramLocation[0]);
      } catch (FileNotFoundException f) {
        Status.warning("Could not find parameters file '" + paramLocation[0] + "'. Using default values for unassigned parameters.");
      } catch (IOException e) {
        Status.warning("Error loading parameters file '" + paramLocation[0] + "'. Using default values for unassigned parameters.");
      }
    }
  }
  
  /**
   * This method reads parameters from a file.  In the file, blank lines are ignored and comments start with "#".<br>
   * <br>
   * A sample format of the file is:<br>
   * <pre><br>
   * KEY1 =<br>
   * value11<br>
   * value12<br>
   * ...<br>
   * <br>
   * KEY2 =<br>
   * value21<br>
   * <br>
   * KEY3 = value31<br>
   * ...<br>
   * <br></pre>
   * where the KEY indicate the parameters to be set, and each line of text after the "=" after the key is a 
   * parameter value.  Multiple lines may be used for arrays of values.
   * 
   * @param fileName A filename in which parameters may be stored.
   * @throws IOException If there is a problem reading the file.
   */
  public Parameters(String fileName) throws IOException {
    this.readParameters(fileName);
  }
  
  protected void readParameters(String fileName) throws IOException {
    Status.flow("Reading parameters from " + fileName + "...");
    //Status.basic("************* Parameters ***************");
    FileReader fileReader = new FileReader(fileName);
    LineNumberReader lineReader = new LineNumberReader(fileReader);
    String line =lineReader.readLine();
    String lastKey = null;
    String[] parameter = new String[0];
    Status.basic("--------------------- Parameters from file ---------------------");
    Status.basic("");
    while (line != null) {
      String currLine = line.trim();
      line = lineReader.readLine();
      int commentStart = currLine.indexOf("#");
      String unCommentedLine = commentStart < 0 ? currLine : currLine.substring(0, commentStart).trim();
      if (unCommentedLine.length() == 0) {continue;}
      int keyEnd = unCommentedLine.indexOf("=");
      if (keyEnd < 0) {
        parameter = (String[]) ArrayUtils.appendElement(parameter, unCommentedLine);
        continue;
      }
      if (lastKey == null && parameter.length != 0) {
        throw new IOException("Found uncommented text in parameter file without an associated label");
      }
      if (lastKey != null) {
        if (m_Parameters.containsKey(lastKey)) {
          Status.warning("Label '" + lastKey + "' has more than one value.  Overwriting previous value.");
        }
        this.storeParameter(lastKey, parameter);
      }
      if (unCommentedLine.length() != keyEnd + 1) {
        parameter = new String[] {unCommentedLine.substring(keyEnd+1).trim()};
      } else {
        parameter = new String[0];
      }
      lastKey = unCommentedLine.substring(0, keyEnd).trim();
    }
    if (lastKey != null) {
      this.storeParameter(lastKey, parameter);
    }
    //Status.basic("************ End Parameters ************");
    Status.basic("");
    Status.basic("------------------------ End Parameters ------------------------");
    Status.basic("");
    lineReader.close();
  }
  
  protected void storeParameter(String key, String[] parameter) {
    Status.basic(key + " =");
    for (int paramLine = 0; paramLine < parameter.length; paramLine++) {
      Status.basic("   " + parameter[paramLine]);
    }
    m_Parameters.put(key, parameter);
  }
  
  /**
   * 
   * @param paramName The key for the requested parameter.
   * @return The values for the requested parameter.
   */
  public String[] getParameter(String paramName) {
    return (String[]) m_Parameters.get(paramName);
  }
  
  /**
   * 
   * @param paramName The key for the requested parameter.
   * @param defaults The values to return if no values have been set for the given parameter.
   * @return The values for the requested parameter.
   */
  public String[] getParameter(String paramName, String[] defaults) {
    if (!m_Parameters.containsKey(paramName)) {
      return defaults;
    }
    return (String[]) m_Parameters.get(paramName);
  }
  
  /**
   * Returns the value of an integer-type parameter
   * 
   * @param paramName The key for the requested parameter.
   * @param defaultValue The value to return if no value has been set for the given parameter.
   * @return The value for the requested parameter.
   */
  public int getParameter(String paramName, int defaultValue) {
    if (!m_Parameters.containsKey(paramName)) {
      return defaultValue;
    }
    return Integer.parseInt(this.getParameter(paramName)[0]);
  }
  
  /**
   * Returns the value of an boolean-type parameter
   * 
   * @param paramName The key for the requested parameter.
   * @param defaultValue The value to return if no value has been set for the given parameter.
   * @return The value for the requested parameter.
   */
  public boolean getParameter(String paramName, boolean defaultValue) {
    if (!m_Parameters.containsKey(paramName)) {
      return defaultValue;
    }
    return Boolean.parseBoolean(this.getParameter(paramName)[0]);
  }
  
  /**
   * Returns the value of an double-type parameter
   * 
   * @param paramName The key for the requested parameter.
   * @param defaultValue The value to return if no value has been set for the given parameter.
   * @return The value for the requested parameter.
   */
  public double getParameter(String paramName, double defaultValue) {
    if (!m_Parameters.containsKey(paramName)) {
      return defaultValue;
    }
    return Double.parseDouble(this.getParameter(paramName)[0]);
  }
  
  /**
   * Returns the value of an String-type parameter
   * 
   * @param paramName The key for the requested parameter.
   * @param defaultValue The value to return if no value has been set for the given parameter.
   * @return The value for the requested parameter.
   */
  public String getParameter(String paramName, String defaultValue) {
    if (!m_Parameters.containsKey(paramName)) {
      return defaultValue;
    }
    return this.getParameter(paramName)[0];
  }
  
  /**
   * 
   * @param paramName A parameter key.
   * @return True if and only if a value has been assigned to the parameter.
   */
  public boolean containsValueFor(String paramName) {
    String[] value = (String[]) m_Parameters.get(paramName);
    return (value != null);
  }
  
  /**
   * 
   * @param fileName A file from which parameters should be read
   * @return A Parameters object containing values read from the file, if possible.  If there was a problem 
   * reading the file, the application terminates. 
   */
  public static Parameters loadFileOrDie(String fileName) {
    
    try {
      Parameters returnFile = new Parameters(fileName);
      return returnFile;
    } catch (IOException e) {
      Status.error(e.toString());
      System.exit(-1);
    }
    return null;
  }
  
  /**
   * 
   * @return A string describing how this class parses command line arguments.
   */
  public static String getHelpString() {
    return "Input parameters are given on the command line in the format: \n" +
            "   -SWITCH_1 {args1} -SWITCH_2 {args2} ...\n" +
            "The allowed values for the switches and the arguments for the \n" +
            "switches are given in the sample Parameters file.  Please see this \n" +
            "file for more details.\n" +
            "\n" +
            "The location of the parameters file can be specified by using the \n" +
            "-PARAMETERS_LOCATION switch.  E.g.: \n" +
            "   -PARAMETERS_LOCATION parameters.txt.\n";
  }

}
