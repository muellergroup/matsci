/*
 * Created on Feb 24, 2006
 *
 */
package matsci.io.app.log;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This a generic logging class to be used statically, much like System.out.  Some features include:<br>
 * <br>
 * -- The ability to control the amount of logging output by setting a logging level.<br>
 * -- The ability to provide the output and error PrintStreams.<br>
 * -- The option of providing custom time stamps on all output.<br>
 * <br>
 * The default log levels are provided as static constants.  In addition to the default levels any 
 * double value may be used, allowing for custom levels of logging.<br>
 * 
 * @author Tim Mueller
 *
 */
public class Status {
  
  /**
   * Generally useful for debugging.  Integer value of 8.
   */
  public final static int LEVEL_DETAIL = 8;
  
  /**
   * The default log level, providing limited, but useful, information.  Integer value of 6.
   */
  public final static int LEVEL_BASIC = 6;
  
  /**
   *  A level at which the application records the current state in processing.  Integer value of 4.
   */
  public final static int LEVEL_FLOW = 4;
  
  /**
   *  Messages that are important, but not problematic.  Integer value of 3.
   */
  public final static int LEVEL_ALERT = 3;
  
  /**
   *  Messages that probably demand your attention because of a problem, but are not showstoppers.  Integer value of 2.
   */
  public final static int LEVEL_WARNING = 2;
  
  /**
   * Errors are written.  Messages at this level and below are written to the error PrintStream.  Integer value of 1.
   */
  public final static int LEVEL_ERROR = 1;
  
  /**
   *  Nothing is written.  Integer value of 0.
   */
  public final static int LEVEL_QUIET = 0;
  
  private static PrintStream OUT = System.out;
  private static PrintStream ERROR = System.err;
  private static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("MMM d, yyyy. HH:mm:ss.SSS |  ");
  private static double LOG_LEVEL = LEVEL_BASIC;
  private static boolean INCLUDE_TIME;
  private static boolean HELP_MODE;
  
  private Status() {
    super();
  }
  
  /**
   * For a description of meaningful arguments, pass in the arg -h or just call Status.getHelpString().
   * 
   * @param args Arguments, such as those taken from the command line, that set the logging behavior. 
   */
  public static void initFromArgs(String[] args) {
    
    for (int argNum = 0; argNum < args.length; argNum++) {
      String arg = args[argNum].toLowerCase();
      if (arg.startsWith("-h") || arg.startsWith("?") || arg.startsWith("-?") || arg.equals("help")) {
        HELP_MODE = true;
        return;
      }
    }
    
    for (int argNum = 0; argNum < args.length - 1; argNum++) {
      String arg = args[argNum].toLowerCase();
      if (arg.equals("-o")) {
        String outFile = args[argNum+1].toLowerCase();
        try {
          setOutfile(outFile);
        } catch (FileNotFoundException e) {
          throw new RuntimeException("Failed to open file " + args[argNum+1] + " given by switch '-o'.", e);
        }
      }
    }
    
    for (int argNum = 0; argNum < args.length - 1; argNum++) {
      String arg = args[argNum].toLowerCase();
      if (arg.equals("-e")) {
        String outFile = args[argNum+1].toLowerCase();
        try {
          setErrfile(outFile);
        } catch (FileNotFoundException e) {
          throw new RuntimeException("Failed to open file " + args[argNum+1] + " given by switch '-e'.", e);
        }
      }
    }
    
    for (int argNum = 0; argNum < args.length - 1; argNum++) {
      String arg = args[argNum].toLowerCase();
      if (arg.equals("-t")) {
        String value = args[argNum+1].toLowerCase();
        if (value.equals("true")) {includeTime(true);}
        else if (value.equals("false")) {includeTime(false);}
        else {
          throw new RuntimeException("Unknown argument for command line switch '-t': " + args[argNum + 1]);
        }
      }
    }
    
    for (int argNum = 0; argNum < args.length - 1; argNum++) {
      String arg = args[argNum].toLowerCase();
      if (arg.equals("-v")) {
        String verbosity = args[argNum+1].toLowerCase();
        if (verbosity.equals("quiet")) {setLogLevel(LEVEL_QUIET);}
        else if (verbosity.equals("error")) {setLogLevel(LEVEL_ERROR);}
        else if (verbosity.equals("warning")) {setLogLevel(LEVEL_WARNING);}
        else if (verbosity.equals("alert")) {setLogLevel(LEVEL_ALERT);}
        else if (verbosity.equals("flow")) {setLogLevel(LEVEL_FLOW);}
        else if (verbosity.equals("basic")) {setLogLevel(LEVEL_BASIC);}
        else if (verbosity.equals("detail")) {setLogLevel(LEVEL_DETAIL);}
        else {
          throw new RuntimeException("Unknown argument for command line switch '-v': " + args[argNum + 1]);
        }
      }
    }
  }
  
  /**
   * 
   * @return True if the arguments passed to this class indicate that the user wants help.  At the beginning 
   * of your application, you should check help mode and if it is true your application should display
   * Status.getHelpString().
   */
  public static boolean helpMode() {
    return HELP_MODE;
  }
  
  /**
   * 
   * @param outFileName The name of a file to which output should be written.
   * @throws FileNotFoundException If there is a problem opening the output file.
   */
  public static void setOutfile(String outFileName) throws FileNotFoundException {
    FileOutputStream outputStream = new FileOutputStream(outFileName);
    OUT = new PrintStream(outputStream);
  }
  
  /**
   * 
   * @param errFileName The name of a file to which error messages should be written.
   * @throws FileNotFoundException If there is a problem opening the output file.
   */
  public static void setErrfile(String errFileName) throws FileNotFoundException {
    FileOutputStream outputStream = new FileOutputStream(errFileName);
    ERROR = new PrintStream(outputStream);
  }
  
  /**
   * Set the error output stream to the main output stream.
   *
   */
  public static void useOutForErr() {
    ERROR = OUT;
  }
  
  /**
   * 
   * @return The current log level.
   */
  public static double getLogLevel() {
    return LOG_LEVEL;
  }
  
  /**
   * All messages assigned the provided log level or lower will be written.  
   * 
   * @param logLevel The maximum level of messages to be written.  
   */
  public static void setLogLevel(double logLevel) {
    LOG_LEVEL = logLevel;
  }

  /**
   * Write nothing to the logs.
   *
   */
  public static void setLogLevelQuiet() {
    setLogLevel(LEVEL_QUIET);
  }
  
  /**
   * Write only messages at LEVEL_ERROR or lower.
   *
   */
  public static void setLogLevelError() {
    setLogLevel(LEVEL_ERROR);
  }
  
  /**
   * Write only messages at LEVEL_WARNING or lower.
   *
   */
  public static void setLogLevelWarning() {
    setLogLevel(LEVEL_WARNING);
  }
  
  /**
   * Write only messages at LEVEL_ALERT or lower.
   *
   */
  public static void setLogLevelAlert() {
    setLogLevel(LEVEL_ALERT);
  }

  /**
   * Write only messages at LEVEL_WARNING or lower.
   *
   */
  public static void setLogLevelFlow() {
    setLogLevel(LEVEL_FLOW);
  }
  
  /**
   * Write only messages at LEVEL_BASIC or lower.
   *
   */
  public static void setLogLevelBasic() {
    setLogLevel(LEVEL_BASIC);
  }
  
  /**
   * Write only messages at LEVEL_DETAIL or lower.
   *
   */
  public static void setLogLevelDetail() {
    setLogLevel(LEVEL_DETAIL);
  }
  
  /**
   * 
   * @param value True if and only if a time stamp should be written at the start of each message.
   */
  public static void includeTime(boolean value) {
    INCLUDE_TIME = value;
  }
  
  /**
   * 
   * @return Whether or not time stamps are currently being included in the log. 
   */
  public static boolean includesTime() {
    return INCLUDE_TIME;
  }
  
  /**
   * 
   * @param status A message to be written to the log
   * @param priority The priority of the message.  The message will be written if and only if its priority is lower
   * than the current log level.
   */
  public static void writeStatus(String status, double priority) {
    if (priority > LOG_LEVEL) {return;}
    if (INCLUDE_TIME ) {OUT.print(DATE_FORMATTER.format(new Date()));}
    if (priority <= LEVEL_ERROR) {
      ERROR.println(status);
    } else {
      OUT.println(status);
    }
  }
  
  /**
   * 
   * @param formatString A format string that is recognized by java.text.SimpleDateFormat.  The string will be used
   * to format any time stamps.
   */
  public static void setDateFormat(String formatString) {
    DATE_FORMATTER.applyPattern(formatString);
  }
  
  /**
   * Writes a message as long as log_level is at LEVEL_ERROR or below.
   * 
   * @param status The message to be written to the log.
   */
  public static void error(String status) {
    writeStatus("### ERROR! " + status + " ###", LEVEL_ERROR);
  }
  
  /**
   * Writes a message as long as log_level is at LEVEL_WARNING or below.
   * 
   * @param status The message to be written to the log.
   */
  public static void warning(String status) {
    writeStatus("### WARNING! " + status + " ###", LEVEL_WARNING);
  }
  
  /**
   * Writes a message as long as log_level is at LEVEL_ALERT or below.
   * 
   * @param status The message to be written to the log.
   */
  public static void alert(String status) {
    writeStatus(status, LEVEL_ALERT);
  }
  
  /**
   * Writes a message as long as log_level is at LEVEL_FLOW or below.
   * 
   * @param status The message to be written to the log.
   */
  public static void flow(String status) {
    writeStatus(status, LEVEL_FLOW);
  }
  
  /**
   * Writes a message as long as log_level is at LEVEL_BASIC or below.
   * 
   * @param status The message to be written to the log.
   */
  public static void basic(String status) {
    writeStatus(status, LEVEL_BASIC);
  }
  
  /**
   * Writes a message as long as log_level is at LEVEL_DETAIL or below.
   * 
   * @param status The message to be written to the log.
   */
  public static void detail(String status) {
    writeStatus(status, LEVEL_DETAIL);
  }
  
  /**
   * 
   * @return A string that describes (in English) the command line arguments recognized by this class.
   */
  public static String getHelpString() {
    
    return "Command line switches for controlling output are: \n" +
            "   -v {quiet, error, warning, flow, basic, detail} \n" +
            "       quiet:  No status output \n" +
            "       error:  Only error messages \n" +
            "       warning:  Only errors and warnings \n" +
            "       flow: Errors, warnings, and basis flow udpates. \n" +
            "       basic:  The default setting \n" +
            "       detail:  Extra detail for debugging purposes \n" +
            "   -o OUTPUT_FILE \n" +
            "       OUTPUT_FILE:  Where status updates should be written (default:  stdOut) \n" +
            "   -e ERROR_FILE \n" +
            "       ERROR_FILE:  Where error messages should be written (default:  stdErr) \n" +
            "   -t {true, false} \n" +
            "       true:  Include timestamps on messages \n" +
            "       false:  Don't include timestamps (default) \n";
  }

}
