/*
 * Created on Nov 22, 2011
 *
 */
package matsci.io.structure;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;

import matsci.Element;
import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianScaleBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.IStructureData;
import matsci.util.arrays.ArrayUtils;

public class QEStructure implements IStructureData {

  private String m_Description;
  private Species[] m_SiteSpecies;
  private Coordinates[] m_SiteCoordinates;
  private Vector[] m_LatticeVectors;

  private Element[] m_PseudoPotentialElements;
  private String[] m_PseudoPotentials;
  
  public QEStructure(IStructureData structure) {
    
    this(structure, null, null);
    
  }
  
  public QEStructure(IStructureData structure, Element[] pseudoPotentialElements, String[] pseudoPotentials) {

    m_Description = structure.getDescription();
    m_LatticeVectors = structure.getCellVectors();
    m_SiteSpecies = new Species[structure.numDefiningSites()];
    m_SiteCoordinates = new Coordinates[structure.numDefiningSites()];
    
    for (int siteNum = 0; siteNum < m_SiteSpecies.length; siteNum++) {
      m_SiteSpecies[siteNum] = structure.getSiteSpecies(siteNum);
      m_SiteCoordinates[siteNum] = structure.getSiteCoords(siteNum);
    }
    
    if (pseudoPotentials == null) {
      m_PseudoPotentialElements = new Element[0];
      m_PseudoPotentials = new String[0];
      for (int siteNum = 0; siteNum < m_SiteSpecies.length; siteNum++) {
        Species species = m_SiteSpecies[siteNum];
        Element element = species.getElement();
        if (ArrayUtils.arrayContains(m_PseudoPotentialElements, element)) {
          continue;
        }
        m_PseudoPotentialElements = (Element[]) ArrayUtils.appendElement(m_PseudoPotentialElements, element);
        m_PseudoPotentials = (String[]) ArrayUtils.appendElement(m_PseudoPotentials, "PP_PLACEHOLDER_" + element.getSymbol());
      }
    } else {
      m_PseudoPotentials = ArrayUtils.copyArray(pseudoPotentials);
      m_PseudoPotentialElements = (Element[]) ArrayUtils.copyArray(pseudoPotentialElements);
    }
  }
  
  public void writeFile(String fileName) {
    
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
      this.writeFile(writer);
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
  }
  
  public void writeFile(Writer writer) {
    
    int ntyp = 0;
    for (int typeNum = 0; typeNum < m_PseudoPotentials.length; typeNum++) {
      if (m_PseudoPotentialElements[typeNum] == Element.vacancy) {continue;}
      ntyp++;
    }
    
    int nat = 0;
    for (int typeNum = 0; typeNum < m_SiteSpecies.length; typeNum++) {
      if (m_SiteSpecies[typeNum] == Species.vacancy) {continue;}
      nat++;
    }
    
    try {
      this.writeLine(writer, "SYSTEM_HEADER_PLACEHOLDER");
      this.writeLine(writer, " ibrav = 0,");
      this.writeLine(writer, " ntyp = " + ntyp + ", ");
      this.writeLine(writer, " nat = " + nat + ", ");
      this.writeLine(writer, "/");
      this.newLine(writer);
      this.writeLine(writer, "&ELECTRONS /");
      this.writeCellVectors(writer);
      this.writeAtomicSpeciesCard(writer);
      this.writeAtomicPositionsCard(writer);
      writer.flush();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
  }
  
  protected void writeAtomicSpeciesCard(Writer writer) throws IOException {
    
    this.newLine(writer);
    writer.write("ATOMIC_SPECIES");
    this.newLine(writer);
    for (int specNum = 0; specNum < m_PseudoPotentialElements.length; specNum++) {
      Element element = m_PseudoPotentialElements[specNum];
      if (element == element.vacancy) {continue;}
      DecimalFormat formatter = new DecimalFormat("0.0000");
      writer.write(" " + element.getSymbol() + " " + formatter.format(element.getAtomicWeight()) + " " + m_PseudoPotentials[specNum]);
      this.newLine(writer);
    }
    
  }

  protected void writeAtomicPositionsCard(Writer writer) throws IOException {
    
    this.newLine(writer);
    writer.write("ATOMIC_POSITIONS crystal");
    this.newLine(writer);
    LinearBasis directBasis = new LinearBasis(m_LatticeVectors);
    DecimalFormat formatter = new DecimalFormat("0.00000000000000");
    for (int siteNum = 0; siteNum < m_SiteCoordinates.length; siteNum++) {
      if (m_SiteSpecies[siteNum] == Species.vacancy) {continue;}
      double[] coordArray = m_SiteCoordinates[siteNum].getCoordArray(directBasis);
      String symbol = m_SiteSpecies[siteNum].getElementSymbol();
      writer.write(" " + symbol 
          + " " + formatter.format(coordArray[0] - Math.floor(coordArray[0]))
          + " " + formatter.format(coordArray[1] - Math.floor(coordArray[1]))
          + " " + formatter.format(coordArray[2] - Math.floor(coordArray[2])));
      this.newLine(writer);
    }
    
  }
  
  protected void writeCellVectors(Writer writer) throws IOException {
    
    this.newLine(writer);
    writer.write("CELL_PARAMETERS cubic"); // TODO implement hexagonal someday.
    this.newLine(writer);
    CartesianScaleBasis basis = new CartesianScaleBasis(0.52917720859);
    DecimalFormat formatter = new DecimalFormat("0.00000000000000");
    for (int vecNum = 0; vecNum < m_LatticeVectors.length; vecNum++) {
      double[] coordArray = m_LatticeVectors[vecNum].getDirectionArray(basis);
      writer.write(" " + formatter.format(coordArray[0]) + 
          " " + formatter.format(coordArray[1]) + 
          " " + formatter.format(coordArray[2]));
      this.newLine(writer);
    }
    
  }
  
  public String getDescription() {
    return m_Description;
  }

  public Vector[] getCellVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_LatticeVectors);
  }

  public boolean[] getVectorPeriodicity() {
    return new boolean[] {true, true, true};
  }

  public int numDefiningSites() {
    return m_SiteCoordinates.length;
  }

  public Coordinates getSiteCoords(int index) {
    return m_SiteCoordinates[index];
  }

  public Species getSiteSpecies(int index) {
    return m_SiteSpecies[index];
  }
  
  protected void newLine(Writer writer) throws IOException {
    writer.write("\n");
  }
  
  protected void writeLine(Writer writer, String string) throws IOException {
    writer.write(string + "\n");
  }

}
