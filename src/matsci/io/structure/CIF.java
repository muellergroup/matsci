/*
 * Created on Feb 4, 2005
 *
 */
package matsci.io.structure;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import matsci.Element;
import matsci.Species;
import matsci.io.app.log.Status;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.BravaisLattice;
import matsci.structure.IDisorderedStructureData;
import matsci.structure.IStructureData;
import matsci.structure.PartiallyOccupiedStructure;
import matsci.structure.DisorderedStructureBuilder;
import matsci.structure.Structure;
import matsci.structure.Structure.Site;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 * This obeys the CIF 1.1 semantics.  This is especially important for the treatment of split lines.
 * http://www.iucr.org/iucr-top/cif/spec/version1.1/cifsemantics.html#W95
 *
 */
public class CIF implements IDisorderedStructureData {
  
  // The objects in this hashmap are hashmaps that contain the data blocks.
  // The keys are the names of the data blocks.
  private HashMap<String, HashMap<String, Object>> m_Data = new HashMap<String, HashMap<String, Object>>();
  
  private HashMap<String, String[][]> m_LabelSets = new HashMap<String, String[][]>(); // String[][] keyed by the data set name
  
  // This information is lazy-loaded
  private Coordinates[] m_SiteCoords;
  private Species[][] m_SiteSpecies;
  private double[][] m_SiteOccupancies;
  private LinearBasis m_PrimitiveBasis;
  private LinearBasis m_ConventionalBasis;
  private boolean[] m_IsVectorPeriodic = new boolean[] {true, true, true};
  
  private double m_RoundPrecision = 0;
  private double m_FormatPrecision = 1E-10;
  
  public CIF(IStructureData structureData) {
	  this(structureData, true);
  }
  
  public CIF(IDisorderedStructureData structureData) {
	  this(structureData, true);  
  }

  public CIF(IDisorderedStructureData structureData, boolean removeVacancies) {
	  this(structureData, removeVacancies, new HashMap<String, String[][]>());
  }
  
  public CIF(IDisorderedStructureData structureData, boolean removeVacancies, Map<String, String[][]> siteAttributes) {
	  
	  PartiallyOccupiedStructure structure = new PartiallyOccupiedStructure(structureData);
	  String formula_short = structure.getCompositionString("", true);
	  String dataBlockName = formula_short;
	  
	  HashMap<String, Object> dataBlock = new HashMap<String, Object>();
	  
	  m_Data.put(dataBlockName, dataBlock);
	  
	  // We define these separately so that users can add their own attributes
	  String[] siteLabels = new String[] {
		  "_atom_site_type_symbol", 
		  "_atom_site_label", 
		  "_atom_site_fract_x", 
		  "_atom_site_fract_y",
  		  "_atom_site_fract_z",
  		  "_atom_site_occupancy",
	  };
	  
	  // For user-defined labels
	  for (String labelName : siteAttributes.keySet()) {
		  boolean seenBefore = false;
		  for (String knownLabel : siteLabels) {
			  if (labelName.equals(knownLabel)) {
				  Status.warning("User not allowed to set standard site attribute: " + knownLabel);
				  seenBefore = true;
				  break;
			  }
		  }
		  if (seenBefore) {continue;}
		  siteLabels = (String[]) ArrayUtils.appendElement(siteLabels, labelName);
	  }
	  
	  String[][] labelSets = new String[][] {
		  new String[] {"_description"},
		  new String[] {"_chemical_formula_sum"},
		  new String[] {"_chemical_formula_weight"},
		  new String[] {"_cell_formula_units_Z"},
		  new String[] {"_cell_length_a"},
		  new String[] {"_cell_length_b"},
		  new String[] {"_cell_length_c"},
		  new String[] {"_cell_angle_alpha"},
		  new String[] {"_cell_angle_beta"},
		  new String[] {"_cell_angle_gamma"},
		  new String[] {"_cell_volume"},
		  new String[] {"_symmetry_space_group_name_H-M"},
		  new String[] {"_symmetry_equiv_pos_as_xyz"},
		  siteLabels
	  };
	  m_LabelSets.put(dataBlockName, labelSets);
	  
	  // Store the structureal data in the cif file
	  dataBlock.put("_chemical_formula_sum", structure.getCompositionString(" ", false));
	  dataBlock.put("_chemical_formula_weight", "" + structure.getAtomicWeightPerUnitCell());
		  
	  Element[] elements = structure.getDistinctElements();
	  double[] elementCounts = new double[elements.length];
	  int[] intElementCounts = new int[elements.length];
	  boolean allInts = true;
	  for (int elementNum = 0; elementNum < elements.length; elementNum++) {
		  elementCounts[elementNum] = structure.getTotalOccupancy(elements[elementNum]);
		  intElementCounts[elementNum] = (int) Math.round(elementCounts[elementNum]);
		  allInts &= (elementCounts[elementNum] == intElementCounts[elementNum]);
	  }
	  
	  int numFormulaUnits = allInts ? MSMath.GCF(intElementCounts) : 1;
	  dataBlock.put("_cell_formula_units_Z", "" + numFormulaUnits);
		  
	  Vector[] cellVectors = structureData.getCellVectors();

	  dataBlock.put("_cell_length_a", "" + cellVectors[0].length());
	  dataBlock.put("_cell_length_b", "" + cellVectors[1].length());
	  dataBlock.put("_cell_length_c", "" + cellVectors[2].length());
	  double alpha = cellVectors[1].angleDegrees(cellVectors[2]);
	  double beta = cellVectors[0].angleDegrees(cellVectors[2]);
	  double gamma = cellVectors[0].angleDegrees(cellVectors[1]);
	  dataBlock.put("_cell_angle_alpha", "" + MSMath.roundWithPrecision(alpha, m_FormatPrecision));
	  dataBlock.put("_cell_angle_beta", "" + MSMath.roundWithPrecision(beta, m_FormatPrecision));
	  dataBlock.put("_cell_angle_gamma", "" + MSMath.roundWithPrecision(gamma, m_FormatPrecision));
	  dataBlock.put("_cell_volume", "" + structure.getBaseStructure().getDefiningVolume());
	  
	  dataBlock.put("_symmetry_space_group_name_H-M", "P 1");
	  dataBlock.put("_symmetry_equiv_pos_as_xyz", new String[] {"x,y,z"});
	  
	  // Now process the sites in the structure
	  ArrayList<String> symbols = new ArrayList<String>();
	  ArrayList<String> labels = new ArrayList<String>();
	  ArrayList<String> xCoords  = new ArrayList<String>();
	  ArrayList<String> yCoords = new ArrayList<String>();
	  ArrayList<String> zCoords = new ArrayList<String>();
	  ArrayList<String> occupancies = new ArrayList<String>();
	  
	  // This puts all coords between 0 and 1.
	  Structure coordStructure = structure.getBaseStructure().useSitesInCellAsDefining(false);

	  // Initialize the user-defined attributes
	  for (String attributeName : siteAttributes.keySet()) {
		  dataBlock.put(attributeName, new String[0]);
	  }
	  
	  int[] counts = new int[elements.length];
	  for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {

		  double[] coordArray =  coordStructure.getSiteCoords(siteNum).getCoordArray(coordStructure.getDirectBasis());
		  int numAllowedSpecies = structure.numAllowedSpecies(siteNum);
		  for (int specNum = 0; specNum < numAllowedSpecies; specNum++) {

			  double occupancy = structure.getSiteOccupancy(siteNum, specNum);
			  if (occupancy == 0) {continue;}
			  
			  Species species = structure.getAllowedSpecies(siteNum, specNum);
			  if (removeVacancies && species == Species.vacancy) {continue;}
				  
			  occupancies.add("" + occupancy);		
		  
			  xCoords.add("" + MSMath.roundWithPrecision(coordArray[0], m_FormatPrecision));
			  yCoords.add("" + MSMath.roundWithPrecision(coordArray[1], m_FormatPrecision));
			  zCoords.add("" + MSMath.roundWithPrecision(coordArray[2], m_FormatPrecision));
			  
			  String symbol = species.getSymbol();
			  Element element = species.getElement();
			  int index = ArrayUtils.findIndex(elements, element);
			  counts[index]++;
			  String label = element.getSymbol() + counts[index];
			  symbols.add(symbol);
			  labels.add(label);
			  
			  // Now add the user-defined attributes
			  for (String attributeName : siteAttributes.keySet()) {
				  String[][] sourceValues = siteAttributes.get(attributeName);
				  String[] destValues = (String[]) dataBlock.get(attributeName);
				  String value = sourceValues[siteNum][specNum];
				  destValues = (String[]) ArrayUtils.appendElement(destValues, value);
				  dataBlock.put(attributeName, destValues);
			  }
		  }
	  }
	  
	  dataBlock.put("_atom_site_type_symbol", symbols.toArray(new String[0]));
	  dataBlock.put("_atom_site_label", labels.toArray(new String[0]));
	  dataBlock.put("_atom_site_fract_x", xCoords.toArray(new String[0]));
	  dataBlock.put("_atom_site_fract_y", yCoords.toArray(new String[0]));
	  dataBlock.put("_atom_site_fract_z", zCoords.toArray(new String[0]));
	  dataBlock.put("_atom_site_occupancy", occupancies.toArray(new String[0]));
	  
	  this.setDescription(structureData.getDescription());
  }
  
  public CIF(IStructureData structureData, boolean removeVacancies) {
	this(new DisorderedStructureBuilder(structureData), removeVacancies);  
  }
  
  public CIF(String fileName, double roundPrecision) {
    
    m_RoundPrecision = roundPrecision;
    try {
      FileReader reader = new FileReader(fileName);
      this.readFile(reader);
    } catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
    
  }
  
  public CIF(String fileName) {
    
    try {
      FileReader reader = new FileReader(fileName);
      this.readFile(reader);
    } catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName, e);
    }
    
  }
  
  public CIF(Reader reader) {
    
    try {
      this.readFile(reader);
    } catch (IOException e) {
      throw new RuntimeException("Error reading from reader: " + reader, e);
    }
    
  }
  
  
  public void writeFile(String fileName) {
    
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
      this.write(writer);
      writer.flush();
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
  
  public void write(Writer writer) throws IOException {
    
    Iterator<String> dataKeyIterator = m_Data.keySet().iterator();
    while (dataKeyIterator.hasNext()) {
      String key = (String) dataKeyIterator.next();
      this.writeDataBlock(key, writer);
      this.newLine(writer);
    }
    
  }
  
  protected void newLine(Writer writer) throws IOException {
    writer.write("\n");
  }
  
  public void readFile(Reader reader) throws IOException{
    
    LineNumberReader lineReader = new LineNumberReader(reader);
    
    String line = this.nextMeaningfulLine(lineReader);
    while (line != null) {
      if (line.startsWith("data_")) {
        line = readDataBlock(line, lineReader);
        continue;
      }
      line = this.nextMeaningfulLine(lineReader); // It's something else that isn't data
    }
    
    lineReader.close();
  }
  
  // TODO move the Newlines around so we don't end up with extras at the end of the file.
  protected void writeDataBlock(String dataLabel, Writer writer) throws IOException {
    
    HashMap<String, Object> data = m_Data.get(dataLabel);
    writer.write("data_" + dataLabel);
    this.newLine(writer);
    
    String[][] labelSets = m_LabelSets.get(dataLabel);
    for (int setNum = 0; setNum < labelSets.length; setNum++){
      String[] labels = labelSets[setNum];
      Object datum = data.get(labels[0]);
      if (datum instanceof String[]) {
        String[] entries = (String[]) datum;
        writer.write("loop_");
        this.newLine(writer);
        for (int labelNum = 0; labelNum < labels.length; labelNum++) {
          writer.write(labels[labelNum]);
          this.newLine(writer);
        }
        for (int entryNum = 0; entryNum < entries.length; entryNum++) {
          for (int labelNum = 0; labelNum < labels.length; labelNum++) {
            entries = (String[]) data.get(labels[labelNum]);
            String entry = entries[entryNum];
            StringTokenizer st = new StringTokenizer(entry);
            if (st.countTokens() > 1) {
              entry = "'" + entry + "'";
            }
            writer.write(entry + " ");
          }
          this.newLine(writer);
        }
        this.newLine(writer);
      } else {
        String entry = (String) datum;
        StringTokenizer st = new StringTokenizer(entry);
        if (st.countTokens() > 1) {
          entry = "'" + entry + "'";
        }
        writer.write(labels[0] + " " + entry);
        this.newLine(writer);
      }
    }
    this.newLine(writer);
    
  }
  
  protected String readDataBlock(String firstLine, LineNumberReader reader) throws IOException {
    
    String dataLabel = firstLine.substring(5);  // Trims the leading "data_"
    HashMap<String, Object> data = new HashMap<String, Object>();
    m_Data.put(dataLabel, data);
    m_LabelSets.put(dataLabel, new String[0][]);
    
    String line = this.nextMeaningfulLine(reader);
    while (line != null) {
      if (line.startsWith("data_")) {return line;} // It's the next data block
      if (line.startsWith("loop_")) {
        line = readLoop(reader, dataLabel);
        continue;
      }
      if (line.startsWith("_")) {
        StringTokenizer st = new StringTokenizer(line);
        String label = this.nextToken(st);
        String datum = st.hasMoreElements() ? this.nextToken(st) : "";
        line = this.nextMeaningfulLine(reader);
        while (!this.isKeyWord(line)) {          
          datum = datum + line;
          line = this.nextMeaningfulLine(reader);          
        }
        datum = datum.trim();
        if (datum.startsWith(";") && datum.endsWith(";")) {
        	datum = datum.substring(1, datum.length() - 1); // Remove semicolons
        }
        /*if (st.hasMoreElements()) {
	         datum = this.nextToken(st);
        } else if (!this.isKeyWord(line)) { // The data is on the next line
	         st = new StringTokenizer(line);
	         datum = this.nextToken(st);
	         line = this.nextMeaningfulLine(reader);
        }*/
        data.put(label, datum);
        String[][] labelSets = m_LabelSets.get(dataLabel);
        labelSets = (String[][]) ArrayUtils.appendElement(labelSets, new String[] {label});
        m_LabelSets.put(dataLabel, labelSets);
        continue;
      }
      //line = this.nextMeaningfulLine(reader);
    }
    return null;
  }
  
  protected boolean isKeyWord(String line) {
    
    return (line.startsWith("loop_") || line.startsWith("data_") || line.startsWith("_"));
    
  }
  
  protected String readLoop(LineNumberReader reader, String dataLabel) throws IOException {
    
    String[] dataLabels = new String[0];
    HashMap<String, Object> data = m_Data.get(dataLabel);
    
    String line = this.nextMeaningfulLine(reader);
    while (line != null) {      
      if (line.startsWith("_")) { // It tells us the type of data we're looking at
        dataLabels = (String[]) ArrayUtils.appendElement(dataLabels, line);
        line = this.nextMeaningfulLine(reader);
        continue;
      }
      break; // It's not a data label
    }
    
    String[][] labelSets = (String[][]) m_LabelSets.get(dataLabel);
    labelSets = (String[][]) ArrayUtils.appendElement(labelSets, dataLabels);
    m_LabelSets.put(dataLabel, labelSets);
    
    String[][] dataArray = new String[dataLabels.length][0];
    
    int passNum = 0;
    while (line != null) {
      
      if (this.isKeyWord(line)) {break;}
      
      // Grow all the data arrays
      for (int labelNum = 0; labelNum < dataLabels.length; labelNum++) {
        dataArray[labelNum] = (String[]) ArrayUtils.growArray(dataArray[labelNum], 1);
      }
      if (dataLabels.length == 1) {
        dataArray[0][passNum] = this.nextToken(new StringTokenizer(line));
      } else {
	      StringTokenizer st = new StringTokenizer(line);
	      int labelNum = 0;
	      while (st.hasMoreTokens()) {
	        dataArray[labelNum][passNum] = this.nextToken(st);
	        labelNum++;
	      }
      }
      line = this.nextMeaningfulLine(reader);
      passNum++;
    }
    
    for (int labelNum = 0; labelNum < dataLabels.length; labelNum++) {
      data.put(dataLabels[labelNum], dataArray[labelNum]);
    }
    
    return line;
    
  }
  
  /**
   * This handles singly and doubly quoted strings
   *
   * @return
   */
  protected String nextToken(StringTokenizer st) { 

    String nextToken = st.nextToken(" \t\n\r\f");
    if (nextToken.startsWith("\"")) {
      nextToken = nextToken.substring(1, nextToken.length());
      if (!nextToken.endsWith("\"")) {
        nextToken += st.nextToken("\"");
        //st.nextToken(" \t\n\r\f");
      }
    }
    
    if (nextToken.startsWith(";")) {
      nextToken = nextToken.substring(1, nextToken.length());
      if (!nextToken.endsWith(";")) {
        nextToken += st.nextToken(";");
        //st.nextToken(" \t\n\r\f");
      }
    }
   
    
    if (nextToken.startsWith("'")) {
      nextToken = nextToken.substring(1, nextToken.length());
      if (nextToken.endsWith("'")) {
    	nextToken = nextToken.substring(0, nextToken.length() - 1);
      } else {
        nextToken += st.nextToken("'");
        st.nextToken(" \t\n\r\f"); // This was commented before, but appears to be necessary to properly advance the stringtokenizer.
      }
    }
    
    
    return nextToken;
  }

  protected String nextMeaningfulLine(LineNumberReader reader) throws IOException {
    
    String line = reader.readLine();
      
    // Check to see if it's a blank line, comment, or single semicolon
    // Single semicolons seem to be a relic from 1.0.
    while (line != null && (line.trim().length() == 0 || line.startsWith("#"))) {
      line= reader.readLine();
    }
    
    if (line == null) {return line;}
    // Handle multi-line text
    if (line.trim().equals(";\\")) {
      line = "\\";
	  while (line.endsWith("\\")) {
	    line = line.substring(0, line.length()-1);
	    line = line + this.nextMeaningfulLine(reader);
	  }
	}
    
    // I think this is actually the preferred way to handle multi-line text
    if (line.startsWith(";")) {
      String nextLine = null;
      do {
        nextLine = reader.readLine();
        line = line + " " + nextLine;
      } while (!nextLine.startsWith(";"));
    }
    
    return line.trim();
    
  }
  
  protected double parseNumber(String number) {
    int numberEnd = number.indexOf("(");
    if (numberEnd >= 0) {
      number = number.substring(0, numberEnd);
    }
    return Double.parseDouble(number);
  }
  
  protected double parseFraction(String number) {
    int numberEnd = number.indexOf("(");
    if (numberEnd >= 0) {
      number = number.substring(0, numberEnd);
    }
    double returnValue = Double.parseDouble(number);
    if (m_RoundPrecision > 0) {
      returnValue = MSMath.roundWithPrecision(returnValue, m_RoundPrecision);
    }
    return returnValue;
  }
  
    
  /**
   *   This might be a smarter way to parse doubles.  
   *   Still could use improvement because it changes non-fractional values.
  **/
  /*protected double parseFraction(String number) {
    
    int numberEnd = number.indexOf("(");
    if (numberEnd >= 0) {
      number = number.substring(0, numberEnd);
    }
    double returnValue = Double.parseDouble(number);
    int decimal = number.indexOf(".");
    if (decimal >= 0) {
      int power = number.length() - decimal - 1;
      if (power >= 1) {
        double precision = 72 * Math.pow(10, power - 2);
        returnValue = MSMath.roundWithPrecision(returnValue, 1.0 / precision);
      }
    }
    return returnValue;
    
  }*/
  
  protected HashMap<String, Object> getFirstDataBlock() {
    return m_Data.values().iterator().next();
  }
  
  /**
   * Populates the m_SiteCoords and m_SiteSpecies arrays
   *
   */
  protected void findAllSites() {
    
    SymmetryOperation[] operations = this.getSpaceOperations();
    HashMap<String, Object> data = this.getFirstDataBlock();
    
    // The symbols can be mapped to different oxidation states
    HashMap<String, Species> symbolMap = new HashMap<String, Species>();
    String[] symbols = (String[]) data.get("_atom_type_symbol");
	String[] oxidationStates = (String[]) data.get("_atom_type_oxidation_number");
    if ((symbols != null) && (oxidationStates != null)) {
    	for (int symbolNum = 0; symbolNum < symbols.length; symbolNum++) {
    		String symbol = symbols[symbolNum];
    		double oxidationState = Double.parseDouble(oxidationStates[symbolNum]);
    		Species species = Species.get(symbol);
    		species = species.setOxidationState(oxidationState);
    		symbolMap.put(symbol, species);
    	}
    }
    
    //String[] siteLabels = (String[]) data.get("_atom_site_label");
    String[] speciesSymbols = (String[]) data.get("_atom_site_type_symbol");
    if (speciesSymbols == null) {  // CIF files are complicated.
    	speciesSymbols = (String[]) data.get("_atom_site_label");
        if (speciesSymbols == null) {  
        	speciesSymbols = (String[]) data.get("_atom_site_label_component_0");
        } else {
        	for (int symbolNum = 0; symbolNum < speciesSymbols.length; symbolNum++) { // Remove the site number
        		StringTokenizer st = new StringTokenizer(speciesSymbols[symbolNum], "_");
        		speciesSymbols[symbolNum] = st.nextToken().replaceAll("[0-9]*$", ""); // From https://stackoverflow.com/questions/1375466/java-regex-to-remove-all-trailing-numbers        		
        	}
        }
    }
    String[] xCoords = (String[]) data.get("_atom_site_fract_x");
    String[] yCoords = (String[]) data.get("_atom_site_fract_y");
    String[] zCoords = (String[]) data.get("_atom_site_fract_z");
    String[] occupancies = (String[]) data.get("_atom_site_occupancy");
    BravaisLattice lattice = new BravaisLattice(this.getCellVectors(), this.getVectorPeriodicity());
    
    m_SiteCoords = new Coordinates[0];
    m_SiteSpecies = new Species[0][0];
    m_SiteOccupancies = new double[0][0];
    
    for (int siteType = 0; siteType < speciesSymbols.length; siteType++) {
      String xString = xCoords[siteType];
      String yString = yCoords[siteType];
      String zString = zCoords[siteType];
      double occupancy = ((occupancies == null) || (occupancies[siteType] == null)) ? 1 : this.parseNumber(occupancies[siteType]);
      if (xString.equals(".") || yString.equals(".") || zString.equals(".")) {continue;}
      double xVal = parseFraction(xString);
      double yVal = parseFraction(yString);
      double zVal = parseFraction(zString);
      //String speciesName = new StringTokenizer(siteLabels[siteType], "+-0123456789").nextToken();  // TODO Check to see if removing this breaks anything.  Not sure why it's there.
      String speciesName = speciesSymbols[siteType];
      //Species species = Species.get(speciesName);
      
      Species species = symbolMap.get(speciesName);
      if (species == null) {
    	  species = Species.get(speciesName);
      }
      
      Coordinates baseCoords = new Coordinates(new double[] {xVal, yVal, zVal}, this.getConventionalBasis());
      for (int opNum =0; opNum < operations.length; opNum++) {
        //Coordinates symCoords =  lattice.softRemainder(operations[opNum].operate(baseCoords));
        Coordinates oppedCoords = operations[opNum].operate(baseCoords);
        int prevSiteNum = 0;
        for (prevSiteNum = 0; prevSiteNum < m_SiteCoords.length; prevSiteNum++) {
          if (lattice.areTranslationallyEquivalent(m_SiteCoords[prevSiteNum], oppedCoords)) {
          //if (symCoords.isCloseEnoughTo(m_SiteCoords[prevSiteNum])) {
            //newSite = false;
            break;
          }
        }
        if (prevSiteNum == m_SiteCoords.length) {
          //m_SiteCoords[siteNum] = symCoords;
          m_SiteCoords = (Coordinates[]) ArrayUtils.appendElement(m_SiteCoords, lattice.hardRemainder(oppedCoords));
          m_SiteSpecies = (Species[][]) ArrayUtils.appendElement(m_SiteSpecies, new Species[0]);
          m_SiteOccupancies = ArrayUtils.appendElement(m_SiteOccupancies, new double[0]);
        }
        int specIndex = ArrayUtils.findIndex(m_SiteSpecies[prevSiteNum], species);
        if (specIndex < 0) {
          m_SiteSpecies[prevSiteNum] = (Species[]) ArrayUtils.appendElement(m_SiteSpecies[prevSiteNum], species);
          m_SiteOccupancies[prevSiteNum] = ArrayUtils.appendElement(m_SiteOccupancies[prevSiteNum], occupancy);
        }
      }
    }
  }
  
  public LinearBasis getConventionalBasis() {
    if (m_ConventionalBasis == null) {
      this.getConventionalVectors();
    }
    return m_ConventionalBasis;
  }
  
  protected SymmetryOperation[] getSpaceOperations() {
    
    HashMap<String, Object> data = this.getFirstDataBlock();
    String[] equivPos = (String[]) data.get("_symmetry_equiv_pos_as_xyz");
    if (equivPos == null) {
    	equivPos = (String[]) data.get("_space_group_symop_operation_xyz");
    }
    if (equivPos == null) {
      equivPos = new String[] {"x, y, z"}; // Assume that the CIF file is not symmetry-reduced
    }
    SymmetryOperation[] returnArray = new SymmetryOperation[equivPos.length];
    for (int opNum = 0; opNum < equivPos.length; opNum++) {
      returnArray[opNum] = this.parseSpaceOp(equivPos[opNum]);
    }
    return returnArray;
    
  }
  
  protected SymmetryOperation parseSpaceOp(String position) {
    
    double[][] pointOp = new double[3][3];
    double[] translation = new double[3];
    
    int dimNum = 0;
    StringTokenizer st = new StringTokenizer(position);
    while (st.hasMoreElements()) {
      String subString = st.nextToken(",").toLowerCase();
      StringTokenizer subTokenizer = new StringTokenizer(subString, "+-", true);
      double multiplier = 1;
      while (subTokenizer.hasMoreElements()) {
        String term = subTokenizer.nextToken().trim();
        if (term.length() == 0) {continue;}
        if (term.equals("+")) {
          continue;
        } else if (term.equals("-")) {
          multiplier = -1;
          continue;
        }
        if (term.endsWith("x") || term.endsWith("y") || term.endsWith("z")) {
          String coefficient = term.substring(0, term.length()-1);
          if (coefficient.length() != 0) {
            multiplier *= Double.parseDouble(coefficient);
          }
          //  We create column vectors here b/c these multipliers are the multipliers of the coefficients
          if (term.endsWith("x")) {pointOp[0][dimNum] = multiplier;}
          if (term.endsWith("y")) {pointOp[1][dimNum] = multiplier;}
          if (term.endsWith("z")) {pointOp[2][dimNum] = multiplier;}
          multiplier = 1;
        } else {
          StringTokenizer termTokenizer = new StringTokenizer(term, "/");
          double numerator = Double.parseDouble(termTokenizer.nextToken());
          double denominator = termTokenizer.hasMoreElements() ? Double.parseDouble(termTokenizer.nextToken()) : 1;
          translation[dimNum] = multiplier * numerator / denominator;
          multiplier = 1;
        }
      }
      dimNum++;
    }
    
    return new SymmetryOperation(pointOp, translation, this.getConventionalBasis());
    
  }

  public LinearBasis getBasis() {
    if (m_PrimitiveBasis == null) {
      this.getCellVectors();
    }
    return m_PrimitiveBasis;
  }

  /**
   * TODO Decide if we should return conventional or primitive vectors
   */
  public Vector[] getCellVectors() {
    return this.getConventionalVectors();
  }
  
  public Vector[] getPrimitiveVectors() {
    
    Vector[] conventionalVectors = this.getConventionalVectors();
    Vector[] returnVectors = conventionalVectors;
    HashMap<String, Object> data = this.getFirstDataBlock();
    String spaceGroupName = (String) data.get("_symmetry_space_group_name_H-M");
    if (spaceGroupName == null) {spaceGroupName = "P1";}
    String cellType = new StringTokenizer(spaceGroupName).nextToken().toUpperCase();
    
    m_PrimitiveBasis = m_ConventionalBasis;
    
    if (cellType.equals("F")) {
      Vector halfX = conventionalVectors[0].multiply(0.5);
      Vector halfY = conventionalVectors[1].multiply(0.5);
      Vector halfZ = conventionalVectors[2].multiply(0.5);
      returnVectors[0] = halfX.add(halfY);
      returnVectors[1] = halfX.add(halfZ);
      returnVectors[2] = halfY.add(halfZ);
      m_PrimitiveBasis = new LinearBasis(returnVectors);
    }
    
    if (cellType.equals("I")) {
      Vector newVector = conventionalVectors[0].multiply(0.5);
      newVector = newVector.add(conventionalVectors[1].multiply(0.5));
      newVector = newVector.add(conventionalVectors[2].multiply(0.5));
      returnVectors[2] = newVector;
      m_PrimitiveBasis = new LinearBasis(returnVectors);
    }
    
    if (cellType.equals("A")) {
      Vector newVector = conventionalVectors[1].multiply(0.5);
      newVector = newVector.add(conventionalVectors[2].multiply(0.5));
      returnVectors[2] = newVector;
      m_PrimitiveBasis = new LinearBasis(returnVectors);
    }
    
    if (cellType.equals("B")) {
      Vector newVector = conventionalVectors[0].multiply(0.5);
      newVector = newVector.add(conventionalVectors[2].multiply(0.5));
      returnVectors[0] = newVector;
      m_PrimitiveBasis = new LinearBasis(returnVectors);
    }
    
    if (cellType.equals("C")) {
      Vector newVector = conventionalVectors[0].multiply(0.5);
      newVector = newVector.add(conventionalVectors[1].multiply(0.5));
      returnVectors[1] = newVector;
      m_PrimitiveBasis = new LinearBasis(returnVectors);
    }
    
    return returnVectors;
  }
  
  public int primPerConventional() {

    HashMap<String, Object> data = this.getFirstDataBlock();
    String spaceGroupName = (String) data.get("_symmetry_space_group_name_H-M");
    String cellType = new StringTokenizer(spaceGroupName).nextToken().toUpperCase();
    
    if (cellType.equals("I")) {return 2;}
    if (cellType.equals("A")) {return 2;}
    if (cellType.equals("B")) {return 2;}
    if (cellType.equals("C")) {return 2;}
    if (cellType.equals("F")) {return 4;}
    return 1;
  }

  public Vector[] getConventionalVectors() {
    
    Vector[] returnArray = new Vector[3];
    
    HashMap<String, Object> data = this.getFirstDataBlock();
    double lengthA = this.parseNumber((String)data.get("_cell_length_a"));
    double lengthB = this.parseNumber((String)data.get("_cell_length_b"));
    double lengthC = this.parseNumber((String)data.get("_cell_length_c"));
    
    double alpha = this.parseNumber((String)data.get("_cell_angle_alpha"));
    double beta = this.parseNumber((String)data.get("_cell_angle_beta"));
    double gamma = this.parseNumber((String)data.get("_cell_angle_gamma"));
    returnArray[0] = new Vector(new double[] {lengthA,0,0}, CartesianBasis.getInstance());

    double y1 = lengthB * Math.cos(Math.toRadians(gamma));
    double y2 = Math.sqrt((lengthB * lengthB - y1 * y1));
    returnArray[1] = new Vector(new double[] {y1, y2, 0}, CartesianBasis.getInstance());
    
    double z1 = lengthC * Math.cos(Math.toRadians(beta));
    double z2 = (lengthB * lengthC * Math.cos(Math.toRadians(alpha)) - y1 * z1) / y2;
    double z3 = Math.sqrt(lengthC * lengthC -  z2 * z2 - z1 * z1);
    returnArray[2] = new Vector(new double[] {z1, z2, z3}, CartesianBasis.getInstance());
    
    m_ConventionalBasis = new LinearBasis(returnArray);
    return returnArray;
  }
  
  /* (non-Javadoc)
   * @see matsci.io.IStructureData#numSites()
   */
  public int numDefiningSites() {
    
    if (m_SiteCoords == null) {
      this.findAllSites();
    }
    return m_SiteCoords.length;
    
    /*HashMap data = this.getFirstDataBlock();
    String[] multiplicities = (String[]) data.get("_atom_site_symmetry_multiplicity");
    String[] occupancies = (String[]) data.get("_atom_site_occupancy");
    String[] xCoords = (String[]) data.get("_atom_site_fract_x");
    
    int numSites = 0;
    for (int siteType = 0; siteType < xCoords.length; siteType++) {
      if (xCoords[siteType].equals(".")) {continue;}
      int multiplicity = (multiplicities == null) ? 1 : Integer.parseInt(multiplicities[siteType]);
      double occupancy = parseNumber(occupancies[siteType]);
      numSites += Math.round(multiplicity * occupancy);
    }
    
    return numSites / this.primPerConventional();*/
    
  }

  /* (non-Javadoc)
   * @see matsci.io.IStructureData#getSiteCoords(int)
   */
  public Coordinates getSiteCoords(int index) {
    
    if (m_SiteCoords == null) {
      this.findAllSites();
    }
    return m_SiteCoords[index];
    
  }

  /* (non-Javadoc)
   * @see matsci.io.ISigmaStructureData#getSiteSpecies(int,int)
   */
  public Species getSiteSpecies(int index, int allowedSpecIndex) {
    if (m_SiteSpecies == null) {
      this.findAllSites();
    }
    return m_SiteSpecies[index][allowedSpecIndex];
  }

  /* (non-Javadoc)
   * @see matsci.io.ISigmaStructureData#getSiteSpecies(int, int)
   */
  public double getSiteOccupancy(int siteNum, int allowedSpecIndex) {
    if (m_SiteOccupancies == null) {
      this.findAllSites();
    }
    return m_SiteOccupancies[siteNum][allowedSpecIndex];
  }
  
  public boolean isOrdered(double occupancyTolerance) {
	  for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
		  if(this.numAllowedSpecies(siteNum) > 1) {return false;}
		  double occupancy = this.getSiteOccupancy(siteNum, 0);
		  double error = Math.abs(occupancy - Math.round(occupancy));
		  if (error > occupancyTolerance) {
			  return false;
		  }
	  }
	  return true;  
  }
  
  /* (non-Javadoc)
   * @see matsci.io.IStructureData#getDescription()
   */
  public String getDescription() {

    HashMap<String, Object> data = this.getFirstDataBlock();
    String returnString = (String) data.get("_description"); // A special tag used by this code
    if (returnString != null) {return returnString;}
    
    returnString = (String) data.get("_chemical_name_systematic");
    if (returnString != null) {return returnString;}
    
    returnString = (String) data.get("_chemical_formula_structural");
    if (returnString != null) {return returnString;}
    
    returnString = (String) data.get("_chemical_formula_sum");
    if (returnString != null) {return returnString;}
    
    return "No Description";
    
  }
  
  /**
   * Sets a new tag "_description", to the given value.
   * 
   * @param description The description of this file
   */
  public void setDescription(String description) {
    this.getFirstDataBlock().put("_description", description);
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getNonPeriodicVectors()
   */
  /*
  public Vector[] getNonPeriodicVectors() {
    return new Vector[0];
  }*/
  
  public boolean[] getVectorPeriodicity() {
    return ArrayUtils.copyArray(m_IsVectorPeriodic);
  }

  /* (non-Javadoc)
   * @see matsci.io.ISigmaStructureData#numAllowedSpecies(int)
   */
  public int numAllowedSpecies(int index) {
    if (m_SiteSpecies == null) {
      this.findAllSites();
    }
    return m_SiteSpecies[index].length;
  }

}
