/*
 * Created on Jan 8, 2011
 *
 */
package matsci.io.structure;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Writer;
import java.util.StringTokenizer;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.IStructureData;
import matsci.util.arrays.ArrayUtils;

public class XYZFile implements IStructureData {
  
  private String m_Description;
  private Species[] m_SiteSpecies;
  private Coordinates[] m_SiteCoordinates;
  private Vector[] m_CellVectors = LinearBasis.fill3DBasis(new Vector[0]);
  private boolean[] m_IsVectorPeriodic = new boolean[m_CellVectors.length];
  
  public XYZFile(IStructureData structureData) {
    m_Description = structureData.getDescription();
    m_CellVectors = structureData.getCellVectors();
    m_SiteSpecies = new Species[structureData.numDefiningSites()];
    m_SiteCoordinates= new Coordinates[structureData.numDefiningSites()];
    for (int siteNum = 0; siteNum < m_SiteSpecies.length; siteNum++) {
      m_SiteSpecies[siteNum] = structureData.getSiteSpecies(siteNum);
      m_SiteCoordinates[siteNum] = structureData.getSiteCoords(siteNum);
    }
  }
  
  public XYZFile(String fileName) {
    
    try {
      LineNumberReader lineReader = new LineNumberReader(new FileReader(fileName));
      this.readFile(lineReader);
      lineReader.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
  }
  
  public XYZFile(LineNumberReader reader) {
    
    try {
      this.readFile(reader);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
  }
  
  protected void readFile(LineNumberReader reader) throws IOException {
    
    int numAtoms = Integer.parseInt(reader.readLine().trim());
    m_Description = reader.readLine();
    
    m_SiteCoordinates = new Coordinates[numAtoms];
    m_SiteSpecies = new Species[numAtoms];
    int siteNum = 0;
    String line = reader.readLine();
    while ((line != null) && (line.trim().length() != 0)) {
      StringTokenizer st = new StringTokenizer(line);
      String specSymbol = st.nextToken();
      // I'm not sure if box size is standard, but Eric Johlin used it in the 
      // files he sent me for a-Si:H
      if (specSymbol.equals("box")) {
        specSymbol = st.nextToken();
        if (!specSymbol.equals("size")) {
          throw new RuntimeException("Unknown symbol in XYZ file: " + specSymbol);
        }
        double length = Double.parseDouble(st.nextToken());
        m_CellVectors = LinearBasis.fill3DBasis(new Vector[0]);
        for (int vecNum = 0; vecNum < m_CellVectors.length; vecNum++) {
          m_CellVectors[vecNum] = m_CellVectors[vecNum].resize(length);
          m_IsVectorPeriodic[vecNum] = true;
        }
        line = reader.readLine();
        continue;
      }
      double[] coordArray = new double[] {
          Double.parseDouble(st.nextToken()),
          Double.parseDouble(st.nextToken()),
          Double.parseDouble(st.nextToken())
      };
      
      // I'm not sure if this Tx Ty Tz stuff is standard, but Eric Johlin used it in the 
      // files he sent me for a-Si:H
      if (specSymbol.equals("Tx")) {
        m_CellVectors[0] = new Vector(coordArray, CartesianBasis.getInstance());
        m_IsVectorPeriodic[0] = true;
      } else if (specSymbol.equals("Ty")) {
        m_CellVectors[1] = new Vector(coordArray, CartesianBasis.getInstance());
        m_IsVectorPeriodic[1] = true;
      } else if (specSymbol.equals("Tz")) {
        m_CellVectors[2] = new Vector(coordArray, CartesianBasis.getInstance());
        m_IsVectorPeriodic[2] = true;
      } else {
        m_SiteSpecies[siteNum] = Species.get(specSymbol);
        m_SiteCoordinates[siteNum] = new Coordinates(coordArray, CartesianBasis.getInstance());
        siteNum++;
      }
      
      line = reader.readLine();
    }
    
    
  }

  public String getDescription() {
    return m_Description;
  }

  public Vector[] getCellVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_CellVectors);
  }

  public boolean[] getVectorPeriodicity() {
    return ArrayUtils.copyArray(m_IsVectorPeriodic);
  }

  public int numDefiningSites() {
    return m_SiteSpecies.length;
  }

  public Coordinates getSiteCoords(int index) {
    return m_SiteCoordinates[index];
  }
  
  public void setSiteCoords(int index, Coordinates coords) {
    m_SiteCoordinates[index] = coords;
  }

  public Species getSiteSpecies(int index) {
    return m_SiteSpecies[index];
  }
  
  public void writeFile(String fileName) {
    
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
      this.writeFile(writer);
      writer.flush();
      writer.close();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    
  }
  
  public void writeFile(Writer writer) { 
    
    try {
      writer.write("" + m_SiteSpecies.length);
      this.newLine(writer);
      writer.write(m_Description);
      this.newLine(writer);
      for (int siteNum = 0; siteNum < m_SiteSpecies.length; siteNum++) {
        writer.write(m_SiteSpecies[siteNum].getElementSymbol() + " ");
        double[] coordArray = m_SiteCoordinates[siteNum].getCartesianArray();
        writer.write(coordArray[0] + " " + coordArray[1] + " " + coordArray[2]);
        this.newLine(writer);
      }
      writer.flush();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    
  }
  
  protected void newLine(Writer writer) throws IOException {
    writer.write("\n");
  }
  
}
