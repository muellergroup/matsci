/*
 * Created on Apr 24, 2006
 *
 */
package matsci.io.structure;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

import matsci.Species;
import matsci.location.basis.*;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.structure.*;
import matsci.structure.IStructureData;
import matsci.util.arrays.*;

public class GULP {

  protected IStructureData[] m_StructureData = new IStructureData[0];
  
  protected boolean m_FormatWithDirect = true; 
  protected LinearBasis[] m_FractionalBases;
  
  protected boolean m_ABCCellFormat = false;
  
  public GULP(IStructureData structure) {
    m_StructureData = new IStructureData[] {new StructureBuilder(structure)};
    m_FractionalBases = new LinearBasis[] {new LinearBasis(structure.getCellVectors())};
  }
  
  public GULP(IStructureData[] structures) {
    m_StructureData = (IStructureData[]) ArrayUtils.copyArray(structures);
    m_FractionalBases = new LinearBasis[m_StructureData.length];
    for (int structNum = 0; structNum < m_FractionalBases.length; structNum++) {
      m_FractionalBases[structNum] = new LinearBasis(m_StructureData[structNum].getCellVectors());
    }
  }
  
  public GULP(String fileName) {
    
    try {
      this.readFile(fileName);
    } catch (IOException e) {
      throw new RuntimeException("Error reading file " + fileName + ".", e);
    }
  }
 
  protected void readFile(String fileName) throws IOException {
    
    this.sizeArrays(fileName);
    this.readStructures(fileName);
  }
  
  protected void sizeArrays(String fileName) throws IOException {
    
    LineNumberReader reader = new LineNumberReader(new FileReader(fileName));
    String line = this.nextUncommentedLine(reader);
    int numStructures = 0;
    while (line != null) {
      line = line.toLowerCase();
      if ((line.indexOf("cell") >= 0) || (line.indexOf("vect") >= 0)) {
        numStructures++;
      }
      line = this.nextUncommentedLine(reader);
    }
    
    m_StructureData = new IStructureData[numStructures];
    m_FractionalBases = new LinearBasis[numStructures];
    reader.close();
  }
  
  protected String nextUncommentedLine(LineNumberReader reader) throws IOException {
    String line = reader.readLine();
    while(line != null) {
      line = line.trim();
      if (line.length() != 0 && !line.startsWith("#")) {
        StringTokenizer commentTokenizer = new StringTokenizer(line, "#");
        if (commentTokenizer.hasMoreTokens()) {
          line = commentTokenizer.nextToken().trim();
          if (line.length() != 0) {return line;}
        }
      }
      line = reader.readLine();
    }
    return null;
  }
  
  protected void readStructures(String fileName) throws IOException {
    LineNumberReader reader = new LineNumberReader(new FileReader(fileName));
    boolean useDirectBasis = false;  // Default TODO check GULP behavior
    String line = this.nextUncommentedLine(reader);

    for (int structNum = 0; structNum < m_StructureData.length; structNum++) {
      String description = "No Description";
      Vector[] cellVectors = null;
      LinearBasis directBasis = null;
      Species[] siteSpecies = new Species[0];
      Coordinates[] siteCoords = new Coordinates[0];

      while (line != null) {
        String lowerLine = line.toLowerCase();
        if (lowerLine.indexOf("cell") >= 0) {
          if (cellVectors != null) {break;}
          cellVectors = this.readFromCell(reader, lowerLine);
          directBasis = new LinearBasis(cellVectors);
        } else if (lowerLine.indexOf("vect") >= 0) {
          if (cellVectors != null) {break;}
          cellVectors = this.readCellVectors(reader, lowerLine);
          directBasis = new LinearBasis(cellVectors);
        } else if (lowerLine.indexOf("name") >= 0) {
          if (cellVectors != null) {break;}
          description = line.substring(lowerLine.indexOf("name") + 5, line.length());
        } else if (lowerLine.indexOf("frac") >= 0) {
          useDirectBasis = true;
        } else if (lowerLine.indexOf("cart") >= 0) {
          useDirectBasis = false;
        } else if (lowerLine.indexOf("core") >= 0) {
          if (siteSpecies.length == 0) {
            AbstractBasis formatBasis = useDirectBasis ? (AbstractBasis) directBasis : CartesianBasis.getInstance();
            while (lowerLine.indexOf("core") >= 0) {
              siteSpecies = (Species[]) ArrayUtils.appendElement(siteSpecies, this.parseSiteSpecies(line));
              siteCoords = (Coordinates[]) ArrayUtils.appendElement(siteCoords, this.parseSiteCoordinates(lowerLine, formatBasis));
              line = this.nextUncommentedLine(reader);
              if (line == null) {break;}
              lowerLine = line.toLowerCase();
            }
            continue;
          }
        }
        line = this.nextUncommentedLine(reader);
      }
      if (cellVectors != null) {
        this.addStructure(structNum, description, cellVectors, siteCoords, siteSpecies, directBasis);
      }
    }
    reader.close();
  }
  
  protected void addStructure(int structNum, String description, Vector[] cellVectors, Coordinates[] siteCoords, Species[] siteSpecies, LinearBasis directBasis) {
    StructureBuilder newBuilder = new StructureBuilder();
    newBuilder.setDescription(description);
    newBuilder.setCellVectors(cellVectors);
    newBuilder.setSiteCoordinates(siteCoords);
    newBuilder.setSiteSpecies(siteSpecies);
    m_StructureData[structNum] = newBuilder;
    m_FractionalBases[structNum] = directBasis;
  }
  
  protected String readFirstValue(LineNumberReader reader, String currentLine, String paramName) throws IOException {
    String paramString = currentLine.substring(currentLine.indexOf(paramName));
    StringTokenizer st = new StringTokenizer(paramString);
    paramString = paramString.substring(st.nextToken().length()).trim();
    if (paramString.length() == 0) { 
      return this.nextUncommentedLine(reader);
    }
    return paramString;
  }
  
  protected Species parseSiteSpecies(String line) {
    StringTokenizer st = new StringTokenizer(line);
    String specToken = st.nextToken();
    for (int charNum = 0; charNum < specToken.length(); charNum++) {
      if (!Character.isLetter(specToken.charAt(charNum))) {
        return Species.get(specToken.substring(0, charNum));
      }
    }
    return Species.get(specToken);
  }
  
  protected Coordinates parseSiteCoordinates(String line, AbstractBasis formatBasis) {
    StringTokenizer st = new StringTokenizer(line);
    st.nextToken();
    st.nextToken();
    double[] returnArray = new double[3];
    for (int i = 0; i < returnArray.length; i++) {
      returnArray[i] = Double.parseDouble(st.nextToken());
    }
    return new Coordinates(returnArray, formatBasis);
  }
  
  protected Vector[] readCellVectors(LineNumberReader reader, String currentLine) throws IOException {
    String paramString = this.readFirstValue(reader, currentLine, "vect");
    Vector[] returnArray = new Vector[3];
    for (int vecNum = 0; vecNum < 3; vecNum++) {
      StringTokenizer st = new StringTokenizer(paramString);
      double[] direction = new double[3];
      for (int dimNum= 0; dimNum < 3; dimNum++) {
        direction[dimNum] = Double.parseDouble(st.nextToken());
      }
      returnArray[vecNum] = new Vector(direction, CartesianBasis.getInstance());
      if (vecNum < 2) {paramString = this.nextUncommentedLine(reader);}
    }
    return returnArray;
  }
  
  protected Vector[] readFromCell(LineNumberReader reader, String currentLine) throws IOException {

    String paramString = this.readFirstValue(reader, currentLine, "cell");
    StringTokenizer st = new StringTokenizer(paramString);
    double[] lengths = new double[3];
    double[] angles = new double[3];
    for (int vecNum = 0; vecNum < 3; vecNum++) {
      lengths[vecNum] = Double.parseDouble(st.nextToken());
    }
    for (int angleNum = 0; angleNum < 3; angleNum++) {
      angles[angleNum] = Math.toRadians(Double.parseDouble(st.nextToken()));
    }
    Vector[] returnArray = new Vector[3];
    
    double[] vec1Direction = new double[] {lengths[0], 0, 0};
    returnArray[0] = new Vector(vec1Direction, CartesianBasis.getInstance());
    
    double[] vec2Direction = new double[3];
    vec2Direction[0] = Math.cos(angles[2]);
    vec2Direction[1] = Math.sin(angles[2]);
    returnArray[1] = new Vector(vec2Direction, CartesianBasis.getInstance()).resize(lengths[1]);
    
    double[] vec3Direction = new double[3];
    vec3Direction[0] = Math.cos(angles[1]);
    vec3Direction[1] = (Math.cos(angles[0]) - vec2Direction[0] * vec3Direction[0]) / vec2Direction[1];
    vec3Direction[2] = Math.sqrt(1 - (vec3Direction[0] * vec3Direction[0]) - (vec3Direction[1] * vec3Direction[1]));
    returnArray[2] = new Vector(vec3Direction, CartesianBasis.getInstance()).resize(lengths[2]);
    
    return returnArray;
  }
  
  public int numStructures() {
    return m_StructureData.length;
  }
  
  public IStructureData getStructureData(int structNum) {
    return new StructureBuilder(m_StructureData[structNum]);
  }
  
  public void writeFile(String fileName) {
    this.writeFile(0, fileName);
  }
  
  public void writeFile(int structNum, String fileName) {
    try {
      BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false));
      this.writeStructure(structNum, writer);
      writer.flush();
      writer.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
  
  public void writeStructure(int structNum, String fileName, boolean append) throws IOException {
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, append));
    this.writeStructure(structNum, writer);
    writer.flush();
    writer.close();
  }
  
  public void writeStructure(int structNum, Writer writer) throws IOException {
    this.writeName(writer, structNum);
    this.writeCell(writer, structNum);
    this.writeSites(writer, structNum);
  }
  
  public void writeStructures(String fileName, boolean append) throws IOException {
    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, append));
    for (int structNum = 0; structNum < m_StructureData.length; structNum++) {
      this.writeStructure(structNum, writer);
    }
    writer.close();
  }
  
  protected void writeName(Writer writer, int structNum) throws IOException {
    String description = m_StructureData[structNum].getDescription();
    writer.write("name " + description);
    this.newLine(writer);
  }
  
  protected void writeCell(Writer writer, int structNum) throws IOException {

    Vector[] cellVectors =m_StructureData[structNum].getCellVectors();
    
    DecimalFormat formatter = new DecimalFormat("0.000000");
    if (m_ABCCellFormat) {
      writer.write("cell");
      this.newLine(writer);
      for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
        Vector vector = cellVectors[vecNum];
        writer.write(formatter.format(vector.length()) + " ");
      }
      for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
        Vector vector1 = cellVectors[(vecNum + 1) % cellVectors.length];
        Vector vector2 = cellVectors[(vecNum + 2) % cellVectors.length];
        double cosTheta = vector1.innerProductCartesian(vector2) / (vector1.length() * vector2.length());
        double theta = StrictMath.toDegrees(Math.acos(cosTheta));
        writer.write(formatter.format(theta) + " ");
      }
      this.newLine(writer);
    } else {
      writer.write("vectors");
      this.newLine(writer);
      for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
        double[] cartArray = cellVectors[vecNum].getCartesianDirection();
        for (int dimNum = 0; dimNum < cartArray.length; dimNum++) {
          writer.write(formatter.format(cartArray[dimNum]) + "  ");
        }
        this.newLine(writer);
      }
    }

  }
  
  protected void writeSites(Writer writer, int structNum) throws IOException {
    
    IStructureData structureData = m_StructureData[structNum];
    
    DecimalFormat formatter = new DecimalFormat("0.000000");
    String formatString = m_FormatWithDirect ? "fractional" : "cartesian";
    writer.write(formatString);
    this.newLine(writer);
    AbstractBasis formatBasis = m_FormatWithDirect ? (AbstractBasis) m_FractionalBases[structNum] : CartesianBasis.getInstance();
    for (int siteNum = 0; siteNum < structureData.numDefiningSites(); siteNum++) {
      double[] coordArray = structureData.getSiteCoords(siteNum).getCoordArray(formatBasis);
      Species species = structureData.getSiteSpecies(siteNum);
      if (species == Species.vacancy) {continue;}
      writer.write(species.getElementSymbol() + " core ");
      for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
        writer.write(formatter.format(coordArray[dimNum]) + " ");
      }
      this.newLine(writer);
    }
  }
  
  protected void newLine(Writer writer) throws IOException {
    writer.write("\n");
  }
  
  public void useABCCellFormat(boolean value) {
    m_ABCCellFormat = value;
  }
  
  /*
   * I think this gets tricky if you use cell notation and Cartesian coordinates
  public void useFractionalCoordiantes(boolean value) {
    m_FormatWithDirect = value;
  }*/

}
