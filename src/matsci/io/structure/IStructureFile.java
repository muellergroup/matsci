/*
 * Created on Jun 17, 2012
 *
 */
package matsci.io.structure;

import java.io.Writer;

import matsci.structure.IStructureData;

public interface IStructureFile extends IStructureData {

  public void write(Writer writer);
  public IStructureFile create(IStructureData structureData);
  
}
