/*
 * Created on May 15, 2007
 *
 */
package matsci.engine.minimizer;

import java.util.ArrayList;
import java.util.Arrays;

import matsci.engine.IContinuousFunctionState;
import matsci.io.app.log.Status;
import matsci.util.MSMath;

public class CGMinimizer implements IContinuousMinimizer {

  private double m_MaxIterations = Double.POSITIVE_INFINITY;
  private double m_MinAllowedGrad = 1E-4;
  private double m_MinAllowedStep = 1E-4;
  private double m_MaxAllowedStep = Double.POSITIVE_INFINITY;
  private double m_InitialStepSize = 1;
  
  private ILinearMinimizer m_LinearMinimizer;
  private IContinuousFunctionState m_MinimumState;
  
  private boolean m_RecordSteps;
  private ArrayList m_Steps;
  
  public CGMinimizer() {
	  m_LinearMinimizer = new GRLineMinimizer();
	  m_LinearMinimizer.setMinAllowedStep(m_MinAllowedStep);
  }
  
  public CGMinimizer(ILinearMinimizer linMinimizer) {
    m_LinearMinimizer = linMinimizer;
  }
  
  public ILinearMinimizer getLinearMinimizer() {
    return m_LinearMinimizer;
  }
  
  public boolean minimize(IContinuousFunctionState state) {
    
    int numParameters = state.numParameters();
    double[] gradient = new double[numParameters];
    double[] hVector = new double[numParameters];
    double[] nextDirection = new double[numParameters];
    double[] lastGradient = new double[numParameters];
    double minGradSq = m_MinAllowedGrad * m_MinAllowedGrad;
    
    double stepSize = m_InitialStepSize;
    m_MinimumState = null;
    boolean notStuck = true;
    if (m_RecordSteps) {
      m_Steps.clear();
    }
    
    double lastMinValue = Double.POSITIVE_INFINITY;
    int numiterations = 0;
    
    // I have no idea where the below line (and m_MaxIterations) came from.  It appears to have been added
    // before the repo was created -- maybe by Adarsh or Phil?
    //while (notStuck && numiterations < m_MaxIterations) {//TODO is 1 a reasonable value?
    while (notStuck && numiterations < m_MaxIterations) {
//    	System.out.println("! "+numiterations);
    	numiterations++;
      if (m_RecordSteps) {
        m_Steps.add(state);
      }

      double newMinValue = state.getValue();
      /*if (newMinValue > lastMinValue) {
        break;
      }*/ // We're stuck, I think.  TODO verify that this is right
      /*double[] params = state.getUnboundedParameters(null);
      for (int paramNum = 0; paramNum < params.length; paramNum++) {
        System.out.print(Math.exp(params[paramNum]) + ", ");
      }
      System.out.println(newMinValue);*/
      
      lastMinValue = newMinValue;
      if (Double.isInfinite(lastMinValue) || Double.isNaN(lastMinValue)) {
        Status.detail("Invalid value: " + lastMinValue);
        return false;
      }
      
      gradient = state.getGradient(gradient);
      /*for (int binNum = 0; binNum < gradient.length; binNum++) {
    	  System.out.println(gradient[binNum]);
      }*/
      
      double gradMagSq = MSMath.dotProduct(gradient, gradient);
      if (Double.isInfinite(gradMagSq) || Double.isNaN(gradMagSq)) {
        Status.detail("Invalid gradient magnitude squared: " + gradMagSq);
        return false;
      }
      if (gradMagSq < minGradSq) {
        Status.detail("Gradient magnitude of " + Math.sqrt(gradMagSq) + " is below the minimum of " + Math.sqrt(minGradSq) + ".  Stopping.");
        break;
      }
      
      double numerator = 0;
      for (int varNum = 0; varNum < gradient.length; varNum++) {
        numerator += (gradient[varNum] - lastGradient[varNum]) * gradient[varNum];
      }
      
      double gamma = numerator / MSMath.dotProduct(lastGradient, lastGradient);
      if (Double.isInfinite(gamma)) {gamma = 0;} // TODO check this
      double hMagSq = 0;
      for (int varNum = 0; varNum < hVector.length; varNum++) {
        hVector[varNum] = gradient[varNum] + gamma*hVector[varNum];
        hMagSq += hVector[varNum] * hVector[varNum];
      }
      double hMag = Math.sqrt(hMagSq);
      
      for (int varNum = 0; varNum < nextDirection.length; varNum++) {
        nextDirection[varNum] = -hVector[varNum] / hMag;
      }
      
      //stepSize = hMag;
      stepSize = Math.min(stepSize, hMag);
      
      m_LinearMinimizer.setInitialStepSize(stepSize);
      notStuck = m_LinearMinimizer.minimize(state, nextDirection);
      state = m_LinearMinimizer.getMinimumState();
      stepSize = m_LinearMinimizer.getMinStepSize();
      if (stepSize >= m_MaxAllowedStep) { // We didn't actually hit minimum curvature, so reset.
        Status.detail("Step size of " + stepSize + " has reached maximum allowed step size of " + m_MaxAllowedStep + ".  Resetting gradient.");
        Arrays.fill(lastGradient, 0);
      } else if (stepSize < m_MinAllowedStep) {
        Status.detail("Step size of " + stepSize + " is below minimum allowed step size of " + m_MinAllowedStep + ".  Stopping search.");
        notStuck = false;
      } else {
        System.arraycopy(gradient, 0, lastGradient, 0, gradient.length);
      }
    }
    //m_LinearMinimizer.setInitialStepSize(stepSize); // TODO see how this works, maybe make a flag to turn it on.
    m_MinimumState = state;
    
    if (m_RecordSteps) {
      m_Steps.add(state);
    }
    return true;
    
  }
  
  public void setGradMagConvergence(double value) {
    m_MinAllowedGrad = value;
    m_LinearMinimizer.setGradMagConvergence(value);
  }

  public double getMinAllowedGrad() {
    return m_MinAllowedGrad;
  }
  
  public void setInitialStep(double value) {
    m_InitialStepSize = value;
    m_LinearMinimizer.setInitialStepSize(value);
  }
  
  public double getInitialStep() {
    return m_InitialStepSize;
  }

  public void setMinAllowedStep(double value) {
    m_MinAllowedStep = value;
    m_LinearMinimizer.setMinAllowedStep(value);
  }
 
  public double getMinAllowedStep() {
    return m_MinAllowedStep;
  }

  public void setMaxAllowedStep(double value) {
    m_MaxAllowedStep = value;
    m_LinearMinimizer.setMaxAllowedStep(value);
  }

  public double getMaxAllowedStep() {
    return m_MaxAllowedStep;
  }
  
  public void setMaxAllowedIterations(int value) {
	  m_MaxIterations = value;
  }
  
  public double getMaxAllowedIterations() {
	  return m_MaxIterations;
  }
  
  public IContinuousFunctionState getMinimumState() {
    return m_MinimumState;
  }
  
  public void recordSteps(boolean value) {
    m_RecordSteps = value;
    if (value && (m_Steps == null)) {
      m_Steps = new ArrayList();
    }
  }
  
  public boolean areStepsRecorded() {
    return m_RecordSteps;
  }
  
  public IContinuousFunctionState[] getSteps() {
    if (m_Steps == null) {return null;}
    return (IContinuousFunctionState[]) m_Steps.toArray(new IContinuousFunctionState[0]);
  }

}

