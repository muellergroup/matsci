package matsci.engine.minimizer;

import matsci.engine.IContinuousFunctionState;
import matsci.io.app.log.Status;
import matsci.util.MSMath;

public class GRLineMinimizer extends AbstractLineMinimizer {

	@Override
	public boolean minimize(IContinuousFunctionState functionState, double[] direction) {
		
		double mag = MSMath.magnitude(direction);
		if (mag == 0) {
	      Status.detail("Cannot minimize when zero vector is given as direction.");
	      return failure(new State(functionState, 0));
	    }
		    
	    if (Double.isNaN(mag)) {
	      Status.detail("Cannot minimize when direction is undefined.");
	      return failure(new State(functionState, 0));
	    }
	    
	    double grSmall = (3.0 - Math.sqrt(5)) / 2;
	    double grLarge = 1 - grSmall;
		
		double stepSize = this.getInitialStepSize();
		
		State leftState = new State(functionState, 0);
		State centerState = leftState.shiftState(direction, stepSize);
		State rightState = null;
		
		while ((centerState.getValue() > leftState.getValue()) && (stepSize > this.getMinAllowedStep())) {
			stepSize *= grSmall;
			rightState = centerState;
			centerState = leftState.shiftState(direction, stepSize);
		}
		
		if (stepSize < this.getMinAllowedStep()) {
			if (centerState.getValue() < leftState.getValue()) {
				return success(centerState);
			}
			return failure(leftState);
		}
		
		if (rightState == null) {
			rightState = centerState.shiftState(direction, stepSize / grLarge);
		}
		
		while (rightState.getValue() < centerState.getValue()) {
			stepSize /= grSmall;
			if (stepSize > this.getMaxAllowedStep()) {
				Status.detail("Surpassed maximum allowed step size of " + this.getMaxAllowedStep() + ".  Terminating search.");
				return success(rightState);
			}
			centerState = rightState;
			rightState = centerState.shiftState(direction, stepSize / grLarge);
		}
		
		State bigState = rightState;
		State smallState = leftState;
		
		// Now find the minimum in the established range
		while (stepSize > this.getMinAllowedStep() && this.getAbsoluteGradient(smallState, centerState, bigState) > this.getMinAllowedGrad()) {
			
			stepSize *= grLarge;
			
			double nextStep = bigState.getStep() * grLarge + smallState.getStep() * grSmall;
			State nextState = centerState.shiftState(direction, nextStep - centerState.getStep());
			
			if (nextState.getValue() < centerState.getValue()) {
				smallState = centerState;
				centerState = nextState;
			} else {
				bigState = smallState;
				smallState = nextState;
			}
			
		}
		
		return success(centerState);
		
	}
	
	private double getAbsoluteGradient(State smallState, State centerState, State bigState) {

      double smallGrad = (smallState.getValue() - centerState.getValue()) / (smallState.getStep() - centerState.getStep());
      double bigGrad = (bigState.getValue() - centerState.getValue()) / (bigState.getStep() - centerState.getStep());
      return Math.min(Math.abs(smallGrad), Math.abs(bigGrad));
      
	}
}
