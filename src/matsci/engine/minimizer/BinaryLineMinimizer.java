package matsci.engine.minimizer;

import matsci.engine.IContinuousFunctionState;
import matsci.io.app.log.Status;
import matsci.util.MSMath;

public class BinaryLineMinimizer extends AbstractLineMinimizer {

	@Override
	public boolean minimize(IContinuousFunctionState functionState, double[] direction) {
		
		double mag = MSMath.magnitude(direction);
		if (mag == 0) {
	      Status.detail("Cannot minimize when zero vector is given as direction.");
	      return failure(new State(functionState, 0));
	    }
		    
	    if (Double.isNaN(mag)) {
	      Status.detail("Cannot minimize when direction is undefined.");
	      return failure(new State(functionState, 0));
	    }
		
		double stepSize = this.getInitialStepSize();
		
		State leftState = new State(functionState, 0);
		State centerState = leftState.shiftState(direction, stepSize);
		State rightState = null;
		
		while ((centerState.getValue() > leftState.getValue()) && (stepSize > this.getMinAllowedStep())) {
			stepSize /= 2;
			rightState = centerState;
			centerState = leftState.shiftState(direction, stepSize);
		}
		
		if (stepSize < this.getMinAllowedStep()) {
			if (centerState.getValue() < leftState.getValue()) {
				return success(centerState);
			}
			return failure(leftState);
		}
		
		if (rightState == null) {
			rightState = centerState.shiftState(direction, stepSize);
		}
		
		while (rightState.getValue() < centerState.getValue()) {
			stepSize *= 2;
			if (stepSize > this.getMaxAllowedStep()) {
				Status.detail("Failed to find minimum with step size less than " + this.getMaxAllowedStep() + ".  Terminating search.");
				return failure(rightState);
			}
			centerState = rightState;
			rightState = centerState.shiftState(direction, stepSize);
		}		
		
		// Now find the minimum in the established range
		while (stepSize > this.getMinAllowedStep()) {
			
			stepSize /= 2;
			State newStateLeft = centerState.shiftState(direction, -stepSize);
			
			if (newStateLeft.getValue() < centerState.getValue()) {
				rightState = centerState;
				centerState = newStateLeft;
				continue;
			} 
			
			State newStateRight = centerState.shiftState(direction, stepSize);
			if (newStateRight.getValue() < centerState.getValue()) {
				leftState = centerState;
				centerState = newStateRight;
			} else {
				leftState = newStateLeft;
				rightState = newStateRight;
			}
		}
		
		return success(centerState);
		
	}
}
