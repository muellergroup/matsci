package matsci.engine.minimizer;

import matsci.engine.IContinuousFunctionState;
import matsci.engine.minimizer.AbstractLineMinimizer.State;
import matsci.io.app.log.Status;
import matsci.util.MSMath;

public class QuadraticLineMinimizer extends AbstractLineMinimizer {

	@Override
	public boolean minimize(IContinuousFunctionState functionState, double[] direction) {
		
		double mag = MSMath.magnitude(direction);
		if (mag == 0) {
	      Status.detail("Cannot minimize when zero vector is given as direction.");
	      return failure(new State(functionState, 0));
	    }
		    
	    if (Double.isNaN(mag)) {
	      Status.detail("Cannot minimize when direction is undefined.");
	      return failure(new State(functionState, 0));
	    }
		
		double stepSize = this.getInitialStepSize();
		
		State leftState = new State(functionState, 0);
		State centerState = leftState.shiftState(direction, stepSize);
		State rightState = null;
		
		while ((centerState.getValue() > leftState.getValue()) && (stepSize > this.getMinAllowedStep())) {
			stepSize /= 2;
			rightState = centerState;
			centerState = leftState.shiftState(direction, stepSize);
		}
		
		if (stepSize < this.getMinAllowedStep()) {
			if (centerState.getValue() < leftState.getValue()) {
				return success(centerState);
			}
			return failure(leftState);
		}
		
		if (rightState == null) {
			rightState = centerState.shiftState(direction, stepSize);
		}
		
		while (rightState.getValue() < centerState.getValue()) {
			stepSize *= 2;
			if (stepSize > this.getMaxAllowedStep()) {
				Status.detail("Failed to find minimum with step size less than " + this.getMaxAllowedStep() + ".  Terminating search.");
				return failure(rightState);
			}
			centerState = rightState;
			rightState = centerState.shiftState(direction, stepSize);
		}		
		
		double[][] matrix = new double[3][3];
		matrix[0][0] = 1;
		matrix[1][0] = 1;
		matrix[2][0] = 1;
		double[] values = new double[3];
		while (stepSize > this.getMinAllowedStep()) {
			
			matrix[0][1] = leftState.getStep();
			matrix[0][2] = leftState.getStep() * leftState.getStep();
			matrix[1][1] = centerState.getStep();
			matrix[1][2] = centerState.getStep() * centerState.getStep();
			matrix[2][1] = rightState.getStep();
			matrix[2][2] = rightState.getStep() * rightState.getStep();
			values[0] = leftState.getValue();
			values[1] = centerState.getValue();
			values[2] = rightState.getValue();

			double[][] inverse = MSMath.simpleInverse(matrix);
			double[] coefficients = MSMath.matrixTimesVector(inverse, values);
			double nextX = -1 * coefficients[1] / (2 * coefficients[2]);
			
			if (nextX < leftState.getStep() || nextX > rightState.getStep()) {
				// This can sometimes happen when the matrix is poorly conditioned, which usually means points are close to each other
				// TODO use an alternative equation solving approach to fix this -- Gaussian elimination?
				
				Status.warning("QuadraticLinearMinimizer found state outside expected bounds.  Using best state found so far.");
				return success(centerState);
			}
			
			State nextState = centerState.shiftState(direction, nextX - centerState.getStep());
			if (nextState.getStep() < centerState.getStep()) {
				if ((nextState.getValue() < centerState.getValue())) {
					rightState = centerState;
					centerState = nextState;
				} else {
					leftState = nextState;			
				}
			} else {
				if ((nextState.getValue() < centerState.getValue())) {
					leftState = centerState;
					centerState = nextState;
				} else {
					rightState = nextState;				
				}
			}

			stepSize = Math.min(centerState.getStep() - leftState.getStep(), rightState.getStep() - centerState.getStep());
			
		}
		
		return success(centerState);
		
	}
}
