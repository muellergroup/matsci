package matsci.engine.minimizer;

import matsci.engine.IContinuousFunctionState;
import matsci.io.app.log.Status;
import matsci.util.MSMath;

public abstract class AbstractLineMinimizer implements ILinearMinimizer {
	
	  private double m_MinAllowedGrad = 0;
	  private double m_MinAllowedStep = 1E-4;
	  private double m_MaxAllowedStep = Double.POSITIVE_INFINITY;
	  private double m_InitialStepSize = 1;
	  
	  private State m_MinState;

	@Override
	public void setGradMagConvergence(double value) {
		m_MinAllowedGrad = value;
	}

	@Override
	public double getMinAllowedGrad() {
		return m_MinAllowedGrad;
	}

	@Override
	public void setMinAllowedStep(double value) {
		m_MinAllowedStep = value;
	}

	@Override
	public double getMinAllowedStep() {
		return m_MinAllowedStep;
	}

	@Override
	public void setMaxAllowedStep(double value) {
		m_MaxAllowedStep = value;
	}

	@Override
	public double getMaxAllowedStep() {
		return m_MaxAllowedStep;
	}

	@Override
	public IContinuousFunctionState getMinimumState() {
		return m_MinState.getFunctionState();
	}

	@Override
	public boolean minimize(IContinuousFunctionState functionState) {
		double[] direction = functionState.getGradient(null);

	    double ratio = -1 / MSMath.magnitude(direction);
		for (int paramNum = 0; paramNum < direction.length; paramNum++) {
			direction[paramNum] *= ratio;
		}
		return this.minimize(functionState, direction);
	}

	@Override
	public void setInitialStepSize(double value) {
		m_InitialStepSize = value;
	}

	@Override
	public double getInitialStepSize() {
		return m_InitialStepSize;
	}

	@Override
	public double getMinStepSize() {
		return m_MinState.getStep();
	}
	
	/*
	protected void setMinState(State state) {
		m_MinState = state;
	}*/
	
	protected boolean success(State minState) {
	    Status.detail("Reached linear minimum after step size of " + minState.getStep() + ".  Value is " + minState.getValue() + ".");
	    m_MinState = minState;
		return true;
	}
	
	protected boolean failure(State minState) {
		Status.detail("Failed to find minimum in linear search.  Value = " + minState.getValue() + ".");
		m_MinState = minState;
		return false;
	}
	
	protected class State {
		
		private double m_Step;
		private double m_Value;
		private IContinuousFunctionState m_FunctionState;
		
		protected State(IContinuousFunctionState functionState, double step) {
			m_FunctionState = functionState;
			m_Step = step;
			m_Value = functionState.getValue();
		}
		
		public State shiftState(double[] direction, double stepSize) {
			
			double[] params = m_FunctionState.getUnboundedParameters(null);
			
			for (int paramNum = 0; paramNum < params.length; paramNum++) {
				params[paramNum] += direction[paramNum] * stepSize; // TODO fix the sign of directions throughout the minimizers
			}
			
			return new State(m_FunctionState.setUnboundedParameters(params), m_Step + stepSize);
			
		}
		
		public double getValue() {
			return m_Value;
		}
		
		public IContinuousFunctionState getFunctionState() {
			return m_FunctionState;
		}
		
		public double getStep() {
			return m_Step;
		}
	}
	

}
