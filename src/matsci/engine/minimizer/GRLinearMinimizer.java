/*
 * Created on May 16, 2007
 *
 */
package matsci.engine.minimizer;

import matsci.engine.IContinuousFunctionState;
import matsci.io.app.log.Status;
import matsci.util.MSMath;
import matsci.util.arrays.*;

public class GRLinearMinimizer implements ILinearMinimizer {
  
  private double m_MinAllowedGrad = 1E-4;
  private double m_MinAllowedStep = 1E-4;
  private double m_MaxAllowedStep = Double.POSITIVE_INFINITY;
  private double m_MinStepSize;
  private int m_MaxNumSteps = Integer.MAX_VALUE;
  private IContinuousFunctionState m_MinimumFunction;
  
  private double m_InitialStepSize = 1;
  
  public GRLinearMinimizer() {
    super();
  }
  
  public void setInitialStepSize(double value) {
    m_InitialStepSize =value;
  }
  
  public double getInitialStepSize() {
    return m_InitialStepSize;
  }
  
  public void setMaxNumSteps(int value) {
    m_MaxNumSteps = value;
  }
  
  public boolean minimize(IContinuousFunctionState state) {
    
    double[] direction = state.getGradient(null);
    double mag = 0;
    for (int dimNum = 0; dimNum < direction.length; dimNum++) {
      double value = direction[dimNum];
      mag += value * value;
    }
    
    if (mag == 0) {
      Status.detail("Cannot minimize when zero vector is given as direction");
      m_MinStepSize = 0;
      m_MinimumFunction = state;
      return false;
    }
    
    if (Double.isNaN(mag)) {
      Status.detail("Cannot minimize when gradient is not defined");
      m_MinStepSize = 0;
      m_MinimumFunction = state;
      return false;
    }
    
    double ratio = -1 / Math.sqrt(mag);
    for (int dimNum = 0; dimNum < direction.length; dimNum++) {
    	direction[dimNum] *= ratio;
    }
    
    return this.minimize(state, direction);
    
  }
  
  /**
   * Can rely on one underlying state that just changes a lot
   */
  public boolean minimize(IContinuousFunctionState state, double[] direction) {
    
    double mag = MSMath.magnitude(direction);
    if (mag == 0) {
      Status.detail("Cannot minimize when zero vector is given as direction");
      m_MinStepSize = 0;
      m_MinimumFunction = state;
      return false;
    }
    
    if (Double.isNaN(mag)) {
      Status.detail("Cannot minimize when direction is undefined.");
      m_MinStepSize = 0;
      m_MinimumFunction = state;
      return false;
    }
    
    int numParameters = state.numParameters();
    double[] currentParameters = state.getUnboundedParameters(null);
    double lastMinValue = state.getValue();
    if (Double.isNaN(lastMinValue)) {
      Status.detail("Cannot minimize when value is undefined.");
      m_MinStepSize = 0;
      m_MinimumFunction = state;
      return false;
    }

    double[] bigParameters = ArrayUtils.copyArray(currentParameters);
    double[] centerParameters = new double[bigParameters.length];
    double[] smallParameters = new double[bigParameters.length];
    double[] nextParameters = new double[smallParameters.length];
    double[] tempParameters = new double[nextParameters.length];
    
    IContinuousFunctionState origState = state;
    IContinuousFunctionState bigState = null;
    IContinuousFunctionState centerState = state; // In case we pass right through
    IContinuousFunctionState smallState = null;
    IContinuousFunctionState nextState = null;
    IContinuousFunctionState tempState = null;
    
    m_MinStepSize = 0; // In case we pass right through
    
    double grSmall = (3.0 - Math.sqrt(5)) / 2;
    double grLarge = 1 - grSmall;
    
    double bigX = 0;
    double bigVal = lastMinValue;
    double smallX = m_InitialStepSize * 2;
    /*double smallVal = bigVal + 1;
    while (smallVal > bigVal) {
      smallX /= 2;
      if (smallX < m_MinAllowedStep) {
        m_MinimumFunction = state.setUnboundedParameters(currentParameters);
        m_MinStepSize = smallX;
        Status.detail("Step of " + smallX + " is less than the minimum allowed step size of " + m_MinStepSize + ".  Stopping linear minimization.");
        return false; // Stuck
      }
      for (int varNum = 0; varNum < numParameters; varNum++) {
        smallParameters[varNum] = currentParameters[varNum] - direction[varNum] * smallX;
      }
      smallState = state.setUnboundedParameters(smallParameters);
      smallVal = smallState.getValue();
      if (Double.isInfinite(smallVal) || Double.isNaN(smallVal)) {smallVal = bigVal + 1;}
    }*/
    double smallVal = 0;
    int stepNum = 0;
    do {
      smallX /= 2;
      if (smallX < m_MinAllowedStep) {
        m_MinimumFunction = state.setUnboundedParameters(currentParameters);
        m_MinStepSize = 0;  // Used to be smallX, but I think 0 is better
        Status.detail("Step of " + smallX + " is less than the minimum allowed step size of " + m_MinAllowedStep + ".  Stopping linear minimization.");
        return false; // Stuck
      }
      for (int varNum = 0; varNum < numParameters; varNum++) {
        smallParameters[varNum] = currentParameters[varNum] + direction[varNum] * smallX;
      }
      smallState = state.setUnboundedParameters(smallParameters);
      smallVal = smallState.getValue();
      //stepNum++;
    } while ((stepNum < m_MaxNumSteps) && (Double.isInfinite(smallVal) || Double.isNaN(smallVal) || (smallVal > bigVal)));

    /*double centerVal = smallVal + 1;
    double centerX = smallX;
    while ((smallVal < centerVal)) {
      tempParameters = centerParameters;
      tempState = centerState;
      centerParameters = smallParameters;
      centerVal = smallVal;
      centerX = smallX;
      centerState = smallState;
      smallX *= 1 / grLarge;
      smallParameters = tempParameters;
      if (smallX > m_MaxAllowedStep) {
        break;
      }
      smallState = tempState;
      for (int varNum = 0; varNum < smallParameters.length; varNum++) {
        smallParameters[varNum] = currentParameters[varNum] - direction[varNum] * smallX;
      }
      smallState = state.setUnboundedParameters(smallParameters);
      smallVal = smallState.getValue();
      if (Double.isInfinite(smallVal) || Double.isNaN(smallVal)) {smallVal = centerVal - 1;}
    }*/
    // TODO better address situation in which smallX / grLarge is NaN.  See findMinPolakRibiere in Reg Selector
    double centerVal = smallVal;
    double centerX = smallX;
    do {
      tempParameters = centerParameters;
      tempState = centerState;
      centerParameters = smallParameters;
      centerVal = smallVal;
      centerX = smallX;
      centerState = smallState;
      smallX *= 1 / grLarge;
      smallParameters = tempParameters;
      if (smallX > m_MaxAllowedStep || Double.isInfinite(smallX)) {
        break;
      }
      smallState = tempState;
      for (int varNum = 0; varNum < smallParameters.length; varNum++) {
        smallParameters[varNum] = currentParameters[varNum] + direction[varNum] * smallX;
      }
      smallState = state.setUnboundedParameters(smallParameters);
      smallVal = smallState.getValue();
      //stepNum++;
      //if (Double.isInfinite(smallVal) || Double.isNaN(smallVal)) {smallVal = centerVal - 1;}
      if (Double.isInfinite(smallVal) || Double.isNaN(smallVal)) {smallVal = centerVal + 1;} // TODO test this.
    } while ((stepNum < m_MaxNumSteps) && (Double.isInfinite(smallVal) || Double.isNaN(smallVal) || (smallVal < centerVal)));
    
    if (smallX > m_MaxAllowedStep) {
      smallX = m_MaxAllowedStep;
      centerX = m_MaxAllowedStep * grLarge;
      for (int varNum = 0; varNum < smallParameters.length; varNum++) {
        smallParameters[varNum] = currentParameters[varNum] + direction[varNum] * smallX;
        centerParameters[varNum] = currentParameters[varNum] + direction[varNum] * centerX;
      }      
      centerState = state.setUnboundedParameters(centerParameters);
      centerVal = centerState.getValue();
      smallState = state.setUnboundedParameters(smallParameters);
      smallVal = smallState.getValue();
      while ((smallVal < centerVal) && (stepNum < m_MaxNumSteps)) {
        bigX = centerX;
        centerX = grSmall * centerX + grLarge * smallX;
        //if (Math.abs(centerX - bigX) < m_MinAllowedStep) { // TODO why did I put this?
        if (Math.abs(centerX - bigX) > m_MaxAllowedStep) {
          m_MinimumFunction = smallState;
          m_MinStepSize = m_MaxAllowedStep;
          Status.detail("Reached the maximum allowed step size of " + m_MaxAllowedStep + ".  Stopping linear minimization.");
          return true; // We're not really stuck if we hit the max step size
        }
        bigVal = centerVal;
        bigState = centerState;
        for (int varNum = 0; varNum < smallParameters.length; varNum++) {
          bigParameters[varNum] = centerParameters[varNum];
          centerParameters[varNum] = currentParameters[varNum] + direction[varNum] * centerX;
        }
        centerState = state.setUnboundedParameters(centerParameters);
        centerVal = centerState.getValue();
        stepNum++;
      }
    }
    
    double linGrad = Double.POSITIVE_INFINITY;
    while (Math.abs(linGrad) > m_MinAllowedGrad && (Math.abs(centerX - bigX) > m_MinAllowedStep) && (stepNum < m_MaxNumSteps)) {
      /*if (Double.isInfinite(centerVal)) {
        System.out.println("Why?");
      }*/
      double nextX = bigX + smallX - centerX;
      for (int varNum = 0; varNum < numParameters; varNum++) {
        nextParameters[varNum] = currentParameters[varNum] + direction[varNum] * nextX;
      }
      nextState = state.setUnboundedParameters(nextParameters);
      double nextVal = nextState.getValue();
      stepNum++;
      /*if (Double.isInfinite(nextVal) || Double.isNaN(nextVal)) {nextVal = centerVal + 1;}
      if (nextVal < centerVal) {
        linGrad = (centerVal - nextVal) / (centerX - nextX);
        tempParameters = smallParameters;
        tempState = smallState;
        smallVal = centerVal;
        smallParameters = centerParameters;
        smallX = centerX;
        smallState = centerState;
        centerVal = nextVal;
        centerParameters = nextParameters;
        centerX = nextX;
        centerState = nextState;
        nextParameters = tempParameters;
        nextState = tempState;
      } else {
        tempParameters = bigParameters;
        tempState = bigState;
        bigVal = smallVal;
        bigParameters = smallParameters;
        bigX = smallX;
        bigState = smallState;
        smallVal = nextVal;
        smallParameters = nextParameters;
        smallX = nextX;
        smallState = nextState;
        nextParameters = tempParameters;
        nextState = tempState;
        double smallGrad = Math.abs((smallVal - centerVal) / (smallX - centerX));
        double bigGrad = Math.abs((bigVal - centerVal) / (bigX - centerX));
        linGrad = Math.min(smallGrad, bigGrad);
      }*/
      
      if (Double.isInfinite(nextVal) || Double.isNaN(nextVal) || (nextVal > centerVal)) {
        tempParameters = bigParameters;
        tempState = bigState;
        bigVal = smallVal;
        bigParameters = smallParameters;
        bigX = smallX;
        bigState = smallState;
        smallVal = nextVal;
        smallParameters = nextParameters;
        smallX = nextX;
        smallState = nextState;
        nextParameters = tempParameters;
        nextState = tempState;
        double smallGrad = Math.abs((smallVal - centerVal) / (smallX - centerX));
        double bigGrad = Math.abs((bigVal - centerVal) / (bigX - centerX));
        linGrad = Math.min(smallGrad, bigGrad);
      } else {
        linGrad = (centerVal - nextVal) / (centerX - nextX);
        tempParameters = smallParameters;
        tempState = smallState;
        smallVal = centerVal;
        smallParameters = centerParameters;
        smallX = centerX;
        smallState = centerState;
        centerVal = nextVal;
        centerParameters = nextParameters;
        centerX = nextX;
        centerState = nextState;
        nextParameters = tempParameters;
        nextState = tempState;
      } 
    }
    
    m_MinStepSize = centerX;
    //m_MinimumFunction = centerFunction.setParameters(centerParameters);
    //m_MinimumFunction = origState.setParameters(centerParameters);
    m_MinimumFunction = centerState;
    Status.detail("Reached linear minimum after step size of " + centerX + ".  Value is " + centerVal + ".");
    return true;
  }
  
  public double getMinStepSize() {
    return m_MinStepSize;
  }

  public void setGradMagConvergence(double value) {
    m_MinAllowedGrad = value;
  }

  public double getMinAllowedGrad() {
    return m_MinAllowedGrad;
  }

  public void setMinAllowedStep(double value) {
    m_MinAllowedStep = value;
  }

  public double getMinAllowedStep() {
    return m_MinAllowedStep;
  }
  
  public void setMaxAllowedStep(double value) {
    m_MaxAllowedStep = value;
  }

  public double getMaxAllowedStep() {
    return m_MaxAllowedStep;
  }

  public IContinuousFunctionState getMinimumState() {
    return m_MinimumFunction;
  }

}
