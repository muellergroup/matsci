/*
 * Created on May 16, 2007
 *
 */
package matsci.engine.minimizer;

import matsci.engine.IContinuousFunctionState;

public interface ILinearMinimizer extends IContinuousMinimizer {

  public void setInitialStepSize(double value);
  public double getInitialStepSize();
  
  public double getMinStepSize();
  public boolean minimize(IContinuousFunctionState function, double[] direction);
  
}
