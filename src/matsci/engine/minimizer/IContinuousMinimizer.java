/*
 * Created on May 16, 2007
 *
 */
package matsci.engine.minimizer;

import matsci.engine.*;

public interface IContinuousMinimizer {

  public void setGradMagConvergence(double value);
  public double getMinAllowedGrad();
  public void setMinAllowedStep(double value);
  public double getMinAllowedStep();
  public void setMaxAllowedStep(double value);
  public double getMaxAllowedStep();
  
  public IContinuousFunctionState getMinimumState();
  public boolean minimize(IContinuousFunctionState functionState);
  
}
