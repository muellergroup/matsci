/*
 * Created on May 15, 2007
 *
 */
package matsci.engine;

public interface IContinuousFunctionState {

  public int numParameters();
  public double[] getUnboundedParameters(double[] template);
  public double[] getGradient(double[] template);
  public IContinuousFunctionState setUnboundedParameters(double[] parameters);
  public double getValue();
}
