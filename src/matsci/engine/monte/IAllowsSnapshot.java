package matsci.engine.monte;

/**
 * <p>Title: </p>
 * <p>Description:  Classes that implement this interface are capable of exporting a single object that 
 * represents the current state of the instance.  The type of object used for the snapshot will vary 
 * from class to class.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public interface IAllowsSnapshot {

  /**
   * 
   * @param snapshot  An object representing a state to which this instance should be set.
   */
  public void setState(Object snapshot);
  
  /**
   * Returns an object representing the state of this instance.
   * 
   * @param template  Optional.  If a template object is provided, the state is copied to the template object
   * and returned.  If null is provided, a new snapshot object is created and populated with the state of the system.
   * Using a template may be faster becasue it does not require the creation of a new object.
   * @return  An object representing the state of this instance.
   */
  public Object getSnapshot(Object template);
}