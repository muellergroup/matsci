/*
 * Created on Mar 28, 2005
 *
 */
package matsci.engine.monte;


/**
 * A ground state recorder tracks, in addition to the information tracked by the BasicRecorder, information
 * about the state of the system that has the lowest value during the Metropolis run.
 * 
 * @author Tim Mueller
 *
 */
public class GroundStateRecorder extends BasicRecorder {

  protected IAllowsSnapshot m_System;
  protected double m_MinValue = Double.POSITIVE_INFINITY;
  protected Object m_GroundState;
  
  /**
   * 
   * @param system The state of this system will be recorded as the groundstate when the value reaches a new 
   * minimum.
   */
  public GroundStateRecorder(IAllowsSnapshot system) {
    super();
    m_System = system;
  }
  
  public void recordEndState(double value, double weight) {
    super.recordEndState(value, weight);
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IRecorder#recordChange(double)
   */
  public void recordNewState(double value) {
    super.recordNewState(value);
    if (value < m_MinValue) {
      m_MinValue = value;
      m_GroundState = m_System.getSnapshot(m_GroundState);
    }
  }
  
  /**
   * 
   * @return The system used in the constructor of this method
   */
  public IAllowsSnapshot getSystem() {
    return m_System;
  }
  
  /**
   * 
   * @return The state of the system when the Monte Carlo engine reached the minimum value.
   */
  public Object getGroundState() {
    return m_GroundState;
  }
  
  /**
   * 
   * @return The minimum value found during the Monte Carlo algorithm.  This is also the value of the groundstate.
   */
  public double getMinValue() {
    return m_MinValue;
  }

}
