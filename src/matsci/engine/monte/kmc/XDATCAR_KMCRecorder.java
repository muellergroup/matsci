/*
 * Created on Oct 13, 2014
 *
 */
package matsci.engine.monte.kmc;

import matsci.io.vasp.XDATCAR;
import matsci.structure.decorate.function.AppliedDecorationFunction;

public class XDATCAR_KMCRecorder extends BasicKMCRecorder {
  
  private XDATCAR m_XDATCAR;

  public XDATCAR_KMCRecorder(AppliedDecorationFunction function) {
    
  }
  

  public void recordNewState() {
    
  }

  public void recordEndState(IKMCEvent event, double timeDelta) {
    time += timeDelta;
  }

}
