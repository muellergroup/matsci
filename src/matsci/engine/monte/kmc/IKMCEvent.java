/*
 * Created on Oct 11, 2014
 *
 */
package matsci.engine.monte.kmc;

public interface IKMCEvent {
  
  public void trigger();
  public double getRate();

}
