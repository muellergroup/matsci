/*
 * Created on Oct 11, 2014
 *
 */
package matsci.engine.monte.kmc;

public interface IKMCRecorder {
  
  public void recordNewState();
  public void recordEndState(IKMCEvent event, double timeDelta);

}
