/*
 * Created on Oct 12, 2014
 *
 */
package matsci.engine.monte.kmc;

public class BasicKMCRecorder implements IKMCRecorder {

  double time = 0;
  
  public BasicKMCRecorder() {
  }

  public void recordNewState() {
    
  }

  public void recordEndState(IKMCEvent event, double timeDelta) {
    time += timeDelta;
  }
  
  public double getTime() {
    return time;
  }

}
