/*
 * Created on Oct 11, 2014
 *
 */
package matsci.engine.monte.kmc;

import java.util.Random;

public class KineticMonteCarlo {

  private static final Random m_Generator = new Random();
  
  private int m_NumIterations;

  public KineticMonteCarlo() {
    
  }
  
  public void setNumIterations(int value) {
    m_NumIterations = value;
  }
  
  public int getNumIterations() {
    return m_NumIterations;
  }
  
  public IKMCRecorder run(IAllowsKMC system) {
    return this.runBodyAndRecord(system, new BasicKMCRecorder());
  }
  
  public IKMCRecorder run(IAllowsKMC system, IKMCRecorder recorder) {
    return this.runBodyAndRecord(system, recorder);
  }
  
  protected IKMCRecorder runBodyAndRecord(IAllowsKMC system, IKMCRecorder recorder) {
    
    double timeDelta = 0;
    for (int iter = 0; iter < m_NumIterations; iter++) {

      recorder.recordNewState();
      
      IKMCEvent event = system.getRandomEvent();
      
      double random = 1 - m_Generator.nextDouble(); // Because nextDouble includes 0 but excludes 1
      timeDelta = -Math.log(random) / system.getTotalRate();

      recorder.recordEndState(event, timeDelta);
      event.trigger();
      
    }
 
    return recorder;
  }
  

}
