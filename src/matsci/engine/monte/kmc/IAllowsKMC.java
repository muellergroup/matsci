/*
 * Created on Oct 11, 2014
 *
 */
package matsci.engine.monte.kmc;

import matsci.engine.monte.IAllowsSnapshot;

public interface IAllowsKMC extends IAllowsSnapshot {
  
  public IKMCEvent getRandomEvent();
  public double getTotalRate();

}
