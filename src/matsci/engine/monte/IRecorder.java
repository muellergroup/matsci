/*
 * Created on Mar 27, 2005
 *
 */
package matsci.engine.monte;

/**
 * Classes that implement this interface can be passed into a Monte Carlo engine to record data during 
 * the execution of the Monte Carlo algorithm.
 * 
 * @author Tim Mueller
 *
 */
public interface IRecorder {
  
  /**
   * This method is called during the execution of the Monte Carlo algorithm every time the state of the
   * underlying system is changed, and once at the end of the algorithm.  
   * 
   * @param value  The last value of the system (before the change).
   * @param weight  The weight that should be given to this value for averaging
   */
  public abstract void recordEndState(double value, double weight);
  
  /**
   * This method is called every time the state of the underlying system is changed.  It differs from 
   * recordState() in that it is given the new value of the system.  It is called after every change,
   * and once at the beginning of the Monte Carlo run.
   * 
   * Use this instead of recordEndState when you need to access the state to which this value corresponds.
   * Fore recordEndState, the value may refer to the previous state.
   * 
   * @param value  The next value of the system after the change.
   */
  public abstract void recordNewState(double value);
  
}
