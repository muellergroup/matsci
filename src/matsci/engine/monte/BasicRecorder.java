/*
 * Created on Mar 28, 2005
 *
 */
package matsci.engine.monte;


/**
 * The basic recorder records the average value tracked during a Monte Carlo run and the number of states the 
 * system goes through.
 * 
 * @author Tim Mueller
 *
 */
public class BasicRecorder implements IRecorder {

  protected double m_TotalWeight = 0;
  protected double m_AverageValue = 0;
  protected double m_AverageSqValue = 0;
  protected int m_NumChanges = -1; // Subtract one for the initial state
  
  public BasicRecorder() {}

  /* (non-Javadoc)
   * @see matsci.engine.monte.IRecorder#recordState(double, double)
   */
  public void recordEndState(double value, double weight) {
    double newTotalWeight = m_TotalWeight + weight;
    double correctionRatio = m_TotalWeight / newTotalWeight;
    double incrementRatio = (1 - correctionRatio);
    m_AverageValue = (correctionRatio * m_AverageValue) + (incrementRatio * value);
    m_AverageSqValue = (correctionRatio * m_AverageSqValue) + (incrementRatio * value *value);
    m_TotalWeight = newTotalWeight;
    m_NumChanges++;
  }
  
  /* (non-Javadoc)
   * @see matsci.engine.monte.IRecorder#recordChange(double)
   */
  public void recordNewState(double value) {}
  
  /**
   * 
   * @return  The average value recorded over the Monte Carlo run.
   */
  public double getAverageValue() {
    return m_AverageValue;
  }
  
  /**
   * 
   * @return  The average squared value recorded over the Monte Carlo run.
   */
  public double getAverageSquaredValue() {
    return m_AverageSqValue;
  }
  
  /**
   * 
   * @return  The number of different states the underlying system has been in.
   */
  public int numStates() {
    return m_NumChanges;
  }
  
  /**
   * 
   * @return  The ratio between the number of states the underlying system has been in and the number
   * of attempted changes to the underlying system.
   */
  public double getTriggerRatio() {
    return m_NumChanges / m_TotalWeight;
  }
}
