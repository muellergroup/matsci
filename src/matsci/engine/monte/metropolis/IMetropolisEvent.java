package matsci.engine.monte.metropolis;

/**
 * <p>Title: </p>
 * <p>Description: Classes implementing this interface can be used by a Metropolis engine to modify 
 * an underlying system and determine the impact the modifications have on the value of that system.
 * The value should be consistent with the value returned by IAllowedMetropolis.getValue()</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public interface IMetropolisEvent {

  /**
   * Triggers the event to cause the change to the underlying system
   *
   */
  public void trigger();
  
  /**
   * Determines the change in the value without actually triggering the event
   * 
   * @return The potential change in the value of the underlying system
   */
  public double getDelta();
  
  /**
   * Triggers the event to change the underlying system and returns the change in the value of the system
   * 
   * @return The change in the value of the underlying system
   */
  public double triggerGetDelta();
  
  /**
   * Reverses the changes caused by triggering the event.  This method should only be called 
   * after the event has been triggered.  
   *
   */
  public void reverse();

}