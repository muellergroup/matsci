package matsci.engine.monte.metropolis;

import matsci.engine.monte.IAllowsSnapshot;

/**
 * <p>Title: </p>
 * <p>Description: Classes that implement this interface can be passed to the Metropolis engine
 * for analysis using the Metropolis Monte Carlo algorithm</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public interface IAllowsMetropolis extends IAllowsSnapshot {

  /**
   * 
   * @return The value that determines the probablility of an event being triggered
   */
  public double getValue();
  
  /**
   * 
   * @return An event that, when triggered, changes the underlying system.  The Metropolis engine
   * works by asking for random events, determining how these events change the value, and deciding
   * which ones to trigger based upon the magnitude of the change.
   */
  public IMetropolisEvent getRandomEvent();

}
