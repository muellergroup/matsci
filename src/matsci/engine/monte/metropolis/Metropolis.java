package matsci.engine.monte.metropolis;

import java.util.Random;

import matsci.engine.monte.BasicRecorder;
import matsci.engine.monte.DummyRecorder;
import matsci.engine.monte.GroundStateRecorder;
import matsci.engine.monte.IRecorder;

/**
 * <p>Title: </p>
 * <p>Description: This is an engine for a Metropolis Monte Carlo algorithm.  The general usage is as follows:
 * 1)  The user creates an object that implements the IAllowsMetropolis interface to manage the system on which 
 * the metropolis algorithm should be run.  
 * 2)  The user creates a new Metropolis engine and sets the parameters for the Metropolis algorithm using the 
 * getter and setter methods.  
 * 3)  The user optionally creates a recorder that will record data during the execution of the Metropolis algorithm.
 * 4)  The user feeds the IAllowsMetropolis system and (optionally) the recorder to the engine and receives the 
 * recorded data as a result.
 * </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class Metropolis {

  private static final Random m_Generator = new Random();

  //private boolean m_RunOptimistic = false; // Because we're often interested in ground states
  private boolean m_RunOptimistic = true; // Because many IAllowsMetropolis implementations are faster this way

  private int m_NumIterations;
  private int m_MaxTriggeredEvents = -1;
  private double m_kT;

  private double m_Value;

  public Metropolis() {
  }

  /**
   * 
   * @param system  The system on which the metropolis algorithm should be run
   * @return A BasicRecorder that has recorded information during the execution of the algorithm.
   */
  public BasicRecorder runBasic(IAllowsMetropolis system) {
    return (BasicRecorder) this.runBodyAndRecord(system, new BasicRecorder());
  }
  
  /**
   * 
   * @param system  The system on which the metropolis algorithm should be run
   * @return A BasicRecorder that has recorded information during the execution of the algorithm.
   * @param systemValue  The current value of the system.  Make sure this is correct!
   */
  public BasicRecorder runBasic(IAllowsMetropolis system, double systemValue) {
    m_Value = systemValue;
    return (BasicRecorder) this.runBodyAndRecordKnownValue(system, new BasicRecorder());
  }
  
  /**
   * 
   * @param system  The system on which the metropolis algorithm should be run
   * @return A DummyRecorder that has recorded nothing.
   */
  public DummyRecorder runEquilibration(IAllowsMetropolis system) {
    m_Value = system.getValue(); // TODO consider whether this is necessary.  It might not be, but it can be confusing to leave it out.
    return (DummyRecorder) this.runBodyAndRecordKnownValue(system, new DummyRecorder());
  }
  
  /**
   * 
   * @param system  The system on which the metropolis algorithm should be run
   * @return A DummyRecorder that has recorded nothing.
   * @param systemValue  The current value of the system.  Make sure this is correct!
   */
  public DummyRecorder runEquilibration(IAllowsMetropolis system, double systemValue) {
    m_Value = systemValue;
    return (DummyRecorder) this.runBodyAndRecordKnownValue(system, new DummyRecorder());
  }
  
  /**
   * 
   * @param system  The system on which the metropolis algorithm should be run
   * @return A GroundStateRecorder that has recorded information during the execution of the algorithm
   */
  public GroundStateRecorder runGroundState(IAllowsMetropolis system) {
    return (GroundStateRecorder) this.runBodyAndRecord(system, new GroundStateRecorder(system));
  }
  
  /**
   * 
   * @param system  The system on which the metropolis algorithm should be run
   * @param recorder  A user-created recorder that will be used to record information during the execution of the 
   * algorithm
   * @return  The recorder passed in to the algorithm
   */
  public IRecorder run(IAllowsMetropolis system, IRecorder recorder) {
    return this.runBodyAndRecord(system, recorder);
  }
  
  /**
   * 
   * @param system  The system on which the metropolis algorithm should be run
   * @param recorder  A user-created recorder that will be used to record information during the execution of the 
   * algorithm
   * @param systemValue  The current value of the system.  Make sure this is correct!
   * @return  The recorder passed in to the algorithm
   */
  public IRecorder run(IAllowsMetropolis system, IRecorder recorder, double systemValue) {
    m_Value = systemValue;
    return this.runBodyAndRecord(system, recorder);
  }


  /*
  private void runBody(IAllowsMetropolis system) {
    double multiplier = -1 / m_kT;
    double lastFlipIter = 0;
    for (int iter = 0; iter < m_NumIterations; iter++) {
      
      IMetropolisEvent event = system.getRandomEvent();
      double delta = m_RunOptimistic ? event.triggerGetDelta() : event.getDelta();
      if (Double.isNaN(delta)) {
        if (m_RunOptimistic) {event.reverse();}
        continue;
      }
      if (delta > 0) {
        if (Math.exp(multiplier * delta) < (1 - GENERATOR.nextDouble())) {
        //if ((multiplier * delta) < Math.log(1 - GENERATOR.nextDouble())) {
          if (m_RunOptimistic) {event.reverse();}
          continue;
        }
      }
      
      int nextIter = iter + 1;
      double correctionRatio = lastFlipIter / nextIter;
      double incrementRatio = (1 - correctionRatio);
      m_AverageValue = (m_AverageValue * correctionRatio) + (m_Value * incrementRatio);
      m_AverageValueSQ = (m_AverageValueSQ * correctionRatio) + (m_Value * m_Value * incrementRatio);
      m_NumTriggers++;
      lastFlipIter = nextIter;
      
      if (!m_RunOptimistic) {event.trigger();}
      m_Value += delta;
      
      if (m_Value < m_MinValue) {
        m_MinValue = m_Value;
        if (m_RecordGroundState) {m_GroundState = system.getSnapshot(m_GroundState);}
      }
    }
    
    double correctionRatio = lastFlipIter / m_NumIterations;
    double incrementRatio = (1 - correctionRatio);
    m_AverageValue = (m_AverageValue * correctionRatio) + (m_Value * incrementRatio);
    m_AverageValueSQ = (m_AverageValueSQ * correctionRatio) + (m_Value * m_Value * incrementRatio);
  }
  */
  
  protected IRecorder runBodyAndRecordKnownValue(IAllowsMetropolis system, IRecorder recorder) {
    
    //return m_RunOptimistic ? this.runOptimisticAndRecord(system, recorder) : this.runPessimisticAndRecord(system, recorder);
    
    double multiplier = -1 / m_kT;
    int lastFlipIter = -1;

    recorder.recordNewState(m_Value);
    double delta;
    int numTriggeredEvents = 0;
    for (int iter = 0; iter < m_NumIterations; iter++) {
      
      //System.out.println(iter);
      IMetropolisEvent event = system.getRandomEvent();
      try {
        delta = m_RunOptimistic ? event.triggerGetDelta() : event.getDelta();
      } catch (NullPointerException e) {
        throw new RuntimeException("Bad event created in Metropolis run: " + event, e);
      }
      if (Double.isNaN(delta)) {
        if (m_RunOptimistic) {event.reverse();}
        continue;
      }
      /*System.out.print("System tracked value: " + system.getValue() + ", ");
      System.out.print("Metropolis value: " + m_Value + ", ");
      System.out.print("Next metropolis tracked Value: " + (m_Value + delta) + ", ");
      //System.out.print("System tracked value: " + system.getValue() + ", ");
      system.setState(system.getSnapshot(null));
      System.out.print("System recalculated value: " + system.getValue() + ", ");
      System.out.println("Delta: " + delta);
      if (Math.abs(m_Value + delta - system.getValue()) > 0.0001) {
        System.exit(-1);
      }*/

      if (delta > 0) {
        if (Math.exp(multiplier * delta) < (1 - m_Generator.nextDouble())) {
        //if ((multiplier * delta) < Math.log(1 - GENERATOR.nextDouble())) {
          if (m_RunOptimistic) {event.reverse();}
          continue;
        }
      }
      if (!m_RunOptimistic) {event.trigger();}
      
      /*System.out.print("System tracked value: " + system.getValue() + ", ");
      System.out.print("Last Metropolis value: " + m_Value + ", ");
      System.out.print("Metropolis tracked Value: " + (m_Value + delta) + ", ");
      //System.out.print("System tracked value: " + system.getValue() + ", ");
      system.setState(system.getSnapshot(null));
      System.out.print("System recalculated value: " + system.getValue() + ", ");
      System.out.println("Delta: " + delta);
      if (Math.abs(m_Value + delta - system.getValue()) > 0.0001) {
        System.exit(-1);
      }*/
      //System.out.println("Accepted");
      recorder.recordEndState(m_Value, iter - lastFlipIter);
      lastFlipIter = iter;
      
      if (Double.isInfinite(m_Value)) {
        m_Value = system.getValue();
      } else {
        m_Value += delta;
      }
      recorder.recordNewState(m_Value);
      if (numTriggeredEvents++ == m_MaxTriggeredEvents) {
        break;
      }
    }
    
    if ( m_NumIterations - lastFlipIter - 1  > 0) {
      recorder.recordEndState(m_Value, m_NumIterations - lastFlipIter - 1);
    } 
    return recorder;
  }
  
  protected IRecorder runBodyAndRecord(IAllowsMetropolis system, IRecorder recorder) {

    m_Value = system.getValue();
    if (Double.isNaN(m_Value)) {
      throw new RuntimeException("Initial value for Metropolis algorithm is not a number!");
    }
    return runBodyAndRecordKnownValue(system, recorder);
    
  }

  /**
   * An "optimistic" algorithm is one in which the system is changed first, and then the changes are reversed
   * if the change is not accepted.  A "pessimistic" algorithm is one in which the acceptability of the change
   * is determined before actually making any change, and then the change is only made if it is acceptable.
   * 
   * @param optimistic  True if an optimistic algorithm should be run, false if pessimistic.
   */
  public void setOptimistic(boolean optimistic) {
    m_RunOptimistic = optimistic;
  }

  /**
   * 
   * @return True if this engine runs optimistic algorithms, false otherwise.
   */
  public boolean getOptimistic() {
    return m_RunOptimistic;
  }

  /**
   * 
   * @param kT The Boltzmann "temperature" for the algorithm.  A change is accepted if either it decreases
   * the value fo the system, or if the increase in value is satisfies:
   * 
   * exp(-delta / kT) >= rand(0,1]   where rand(0,1] is a random number uniformly distributed over (0,1].
   */
  public void setTemp(double kT) {
    m_kT = kT;
  }

  /**
   * 
   * @return The value of the parameter kT used by this engine (see setTemp())
   */
  public double getTemp() {
    return m_kT;
  }

  /**
   * 
   * @param iterations  The number of changes that will be attempted
   */
  public void setNumIterations(int iterations) {
    m_NumIterations = iterations;
  }

  /**
   * 
   * @return  The number of changes that the engine attempts
   */
  public int getNumIterations() {
    return m_NumIterations;
  }
  
  /**
   * 
   * @param iterations  The number of changes that will be attempted
   */
  public void setMaxTriggeredEvents(int numEvents) {
    m_MaxTriggeredEvents = numEvents;
  }

  /**
   * 
   * @return  The number of changes that the engine attempts
   */
  public int getMaxTriggeredEvents() {
    return m_MaxTriggeredEvents;
  }

  /**
   * 
   * @return  The final value of the system at the end of the algorithm as tracked by the engine.
   */
  public double getFinalValue() {
    return m_Value;
  }

}
