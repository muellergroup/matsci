/*
 * Created on Aug 9, 2014
 *
 */
package matsci.engine.monte;

public class DummyRecorder implements IRecorder {

  public DummyRecorder() {
  }

  public void recordEndState(double value, double weight) {
  }

  public void recordNewState(double value) {
  }

}
