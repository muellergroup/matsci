/*
 * Created on May 16, 2007
 *
 */
package matsci.model.linear;

import cern.colt.matrix.*;
import matsci.engine.*;
import matsci.model.linear.dist.*;
import matsci.util.arrays.*;

public class DomainWeightEvaluator {

  protected IDomainMatrix m_DomainMatrix;
  protected DoubleMatrix2D m_BareDomainMatrix;

  protected DomainWeightEvaluator m_Function = this;
  
  public DomainWeightEvaluator(IDomainMatrix domainMatrix) {

    m_DomainMatrix = domainMatrix;
    m_BareDomainMatrix = domainMatrix.getMatrix();
    
  }
  
  public State getState(AbstractL2RLSFitter fitter) {
    
    return new State(fitter);
    
  }
  
  public double getValue(AbstractL2RLSFitter fitter) {
    
    return m_DomainMatrix.getExpectedPredictionVariance(fitter);
    
  }
  
  public class State implements IContinuousFunctionState {

    protected AbstractL2RLSFitter m_OrigFitter;
    protected DoubleMatrix2D m_CoreDCore;
    protected DoubleMatrix2D m_LambdaCore;
    protected double m_Constant1stDerivativeTerm = Double.NaN;
    protected double m_Constant2ndDerivativeTerm = Double.NaN;
    
    private double[] m_CurrentParameters;
    protected AbstractL2RLSFitter m_CurrentFitter;
    protected double m_CurrentValue = -1;
    
    protected double m_WeightSum = 0;
    protected double m_ParameterSum = 0;
    
    protected State(AbstractL2RLSFitter fitter) {
      m_OrigFitter = fitter;
      m_CurrentFitter = fitter;
      m_CurrentParameters = new double[fitter.numSamples()];
      for (int sampleNum = 0; sampleNum < fitter.numSamples(); sampleNum++) {
        double weight = m_OrigFitter.getWeight(sampleNum);
        m_CurrentParameters[sampleNum] = weight;
        m_WeightSum += weight;
      }
      m_ParameterSum = m_WeightSum;
    }
    
    protected State(State prevState, double[] parameters) {
      
      m_OrigFitter = prevState.m_OrigFitter;
      m_CurrentParameters = new double[parameters.length];
      for (int varNum = 0; varNum < parameters.length; varNum++) {
        m_CurrentParameters[varNum] = Math.exp(parameters[varNum]);
        m_ParameterSum += m_CurrentParameters[varNum];
      }
      m_WeightSum = prevState.m_WeightSum;
      
      double[] weights = new double[parameters.length];
      double ratio = m_WeightSum / m_ParameterSum;
      for (int sampleNum = 0; sampleNum < parameters.length; sampleNum++) {
        weights[sampleNum] = m_CurrentParameters[sampleNum] * ratio;
      }
      
      m_CurrentFitter = (AbstractL2RLSFitter) m_OrigFitter.setWeights(weights);
    }
    
    public int numParameters() {
      return m_OrigFitter.numSamples();
    }

    public double[] getUnboundedParameters(double[] template) {
      double[] parameters = (template == null) ? new double[this.numParameters()] : template;
      for (int varNum = 0; varNum < parameters.length; varNum++) {
        parameters[varNum] = Math.log(m_CurrentParameters[varNum]);
      }
      return parameters;
    }


    public double[] getGradient(double[] template) {
      
      double[] gradient = (template == null) ? new double[this.numParameters()] : template;
      
      /*DoubleMatrix2D coreInverse = m_CurrentFitter.getCoreInverse();
      
      DoubleMatrix2D coreD = coreInverse.zMult(m_BareDomainMatrix, null);*/
      
      //DoubleMatrix2D coreDcore = coreD.zMult(coreInverse, null);
      
      DoubleMatrix2D coreDcore = this.getCoreDCore();
      double constantTerm = this.get1stDerivativeConstantTerm();
      
      double[][] x = m_CurrentFitter.getInputValues();
      int numSamples = x.length;
      
      //double ratio = m_WeightSum / m_ParameterSum;
      for (int sampleNum = 0; sampleNum < numSamples; sampleNum++) {
        DoubleMatrix1D xRow = DoubleFactory1D.dense.make(x[sampleNum]);
        //gradient[sampleNum] = constantTerm - ratio * coreDcore.zMult(xRow, null).zDotProduct(xRow);
        /*gradient[sampleNum] = ratio * (constantTerm - coreDcore.zMult(xRow, null).zDotProduct(xRow));
        gradient[sampleNum] *= m_CurrentParameters[sampleNum];*/
        gradient[sampleNum] = constantTerm - coreDcore.zMult(xRow, null).zDotProduct(xRow);
        gradient[sampleNum] *= m_CurrentFitter.getWeight(sampleNum);
      }
      
      //double[] slowGradient = this.getSlowGradient(null);
      return gradient;
    }
    
    public IContinuousFunctionState setUnboundedParameters(double[] parameters) {
      
      //System.arraycopy(parameters, 0, m_CurrentParameters, 0, parameters.length);
      return new State(this, parameters);

    }

    public double getValue() {
      if (m_CurrentValue < 0) {
        m_CurrentValue = m_Function.getValue(m_CurrentFitter);
      }
      return m_CurrentValue;
    }
    
    public AbstractL2RLSFitter getFitter() {
      return m_CurrentFitter;
    }
    
    protected DoubleMatrix2D getCoreDCore() {
      
      if (m_CoreDCore == null) {
        DoubleMatrix2D coreInverse = m_CurrentFitter.getCoreInverse();
        DoubleMatrix2D coreD = coreInverse.zMult(m_BareDomainMatrix, null);
        m_CoreDCore = coreD.zMult(coreInverse, null);
      }

      return m_CoreDCore;
    }
    
    protected DoubleMatrix2D getLambdaCore() {
      
      if (m_LambdaCore == null) {
        DoubleMatrix2D coreInverse = m_CurrentFitter.getCoreInverse();
        DoubleMatrix2D regMatrix = m_CurrentFitter.getRegularizerMatrixColt();
        m_LambdaCore = regMatrix.zMult(coreInverse, null);
      }

      return m_LambdaCore;
    }
    

    protected double get1stDerivativeConstantTerm() {
      
      DoubleMatrix2D coreDcore = this.getCoreDCore();
      if (Double.isNaN(m_Constant1stDerivativeTerm)) {
        double currentValue = this.getValue();
        double[][] regMatrix = m_CurrentFitter.getRegularizerMatrix(); 
        
        double trace = 0;
        for (int rowNum = 0; rowNum < regMatrix.length; rowNum++) {
          double[] row = regMatrix[rowNum];
          for (int colNum = 0; colNum < row.length; colNum++) {
            trace += row[colNum] * coreDcore.getQuick(rowNum, colNum);
          }
        }
        
        //m_Constant1stDerivativeTerm = (currentValue - trace) / m_ParameterSum;
        m_Constant1stDerivativeTerm = (currentValue - trace) / m_WeightSum;
      }
      
      return m_Constant1stDerivativeTerm;
      
    }
    
    protected double get2ndDerivativeConstantTerm() {
      
      if (Double.isNaN(m_Constant2ndDerivativeTerm)) {
        
        DoubleMatrix2D coreDcore = this.getCoreDCore();
        DoubleMatrix2D lambdaCore = this.getLambdaCore(); 
        DoubleMatrix2D coreDcoreLambdacore = coreDcore.zMult(lambdaCore, null);
        double[][] regMatrix = m_CurrentFitter.getRegularizerMatrix(); 
        
        double trace = 0;
        for (int rowNum = 0; rowNum < regMatrix.length; rowNum++) {
          double[] row = regMatrix[rowNum];
          for (int colNum = 0; colNum < row.length; colNum++) {
            double val = row[colNum];
            trace += val * coreDcoreLambdacore.getQuick(rowNum, colNum);
            trace -= val * coreDcore.getQuick(rowNum, colNum);
          }
        }
        
        //m_Constant1stDerivativeTerm = (currentValue - trace) / m_ParameterSum;
        m_Constant2ndDerivativeTerm = 2 * trace / (m_WeightSum * m_WeightSum);
      }
      
      return m_Constant2ndDerivativeTerm;
      
    }
    
    public double get1stDerivativeForNewInput(double[] inputValues) {

      DoubleMatrix1D xRow = DoubleFactory1D.dense.make(inputValues);
      return this.getDirect1stDerivativeForNewInput(xRow);
     
    }
    
    protected double getDirect1stDerivativeForNewInput(DoubleMatrix1D xRow) {
      
      DoubleMatrix2D coreDcore = this.getCoreDCore();
      double constantTerm = this.get1stDerivativeConstantTerm();
      int n = m_CurrentFitter.numSamples();
      double returnValue = constantTerm - coreDcore.zMult(xRow, null).zDotProduct(xRow) * (n / m_WeightSum);
      
      /*double slowValue = this.getSlow1stDerivativeForNewInput(xRow);
      double ratio = slowValue / returnValue;
      //return slowValue;*/
      
      return returnValue;
    }
    
    public double get2ndDerivativeForNewInput(double[] inputValues) {
      
      DoubleMatrix1D xRow = DoubleFactory1D.dense.make(inputValues);
      return this.getDirect2ndDerivativeForNewInput(xRow);
      
    }
    
    protected double getDirect2ndDerivativeForNewInput(DoubleMatrix1D xRow) {
      
      DoubleMatrix2D coreDcore = this.getCoreDCore();
      DoubleMatrix2D core = m_CurrentFitter.getCoreInverse();
      DoubleMatrix2D lambdaCore = this.getLambdaCore();
      double constantTerm = this.get2ndDerivativeConstantTerm();
      double n = m_CurrentFitter.numSamples();

      DoubleMatrix1D xCoreDCore = coreDcore.zMult(xRow, null);
      DoubleMatrix1D CoreX = core.zMult(xRow, null);
      DoubleMatrix1D lambdaCoreX = lambdaCore.zMult(xRow, null);
      
      double returnValue = xCoreDCore.zDotProduct(xRow) * xRow.zDotProduct(CoreX) * n * n;
      //returnValue += xCoreDCore.zDotProduct(lambdaCoreX) * n;
      returnValue += 2 * xCoreDCore.zDotProduct(lambdaCoreX) * n;
      returnValue -= xCoreDCore.zDotProduct(xRow) * n;
      returnValue *= 2 / (m_WeightSum * m_WeightSum);
      returnValue += constantTerm;
      
      /*double term1 = xCoreDCore.zDotProduct(xRow) * xRow.zDotProduct(CoreX) * n * n * 2 / (m_WeightSum * m_WeightSum);
      double term2 = xCoreDCore.zDotProduct(lambdaCoreX) * n * 2 / (m_WeightSum * m_WeightSum);
      double term3 = xCoreDCore.zDotProduct(xRow) * n * 2 / (m_WeightSum * m_WeightSum);
      
      double slowValue = this.getSlow2ndDerivativeForNewInput(xRow);
      double ratio = slowValue / returnValue;
      //return slowValue;
      
      double returnValue2 = returnValue + xCoreDCore.zDotProduct(lambdaCoreX) * n * 2 / (m_WeightSum * m_WeightSum);*/
      
      return returnValue;
    }
    
    protected double getSlow2ndDerivativeForNewInput(DoubleMatrix1D xRow) {
      
      double increment = 1E-6;
      
      m_CurrentFitter.allowIncrementalUpdates(false);
      m_CurrentFitter.setNoSingularGuarantee(false);

      AbstractL2RLSFitter tempFitter = (AbstractL2RLSFitter) m_CurrentFitter.addSamples(new double[][] {xRow.toArray()}, new double[] {0}, new double[] {increment});
      
      double[] newWeights = ArrayUtils.appendElement(m_CurrentFitter.getWeights(), increment);
      double scaleFactor = m_WeightSum / (m_WeightSum + increment);
      for (int sampleNum = 0; sampleNum < newWeights.length; sampleNum++) {
        newWeights[sampleNum] *= scaleFactor;
      }
      tempFitter = (AbstractL2RLSFitter) tempFitter.setWeights(newWeights);
      
      double step1 = m_DomainMatrix.getExpectedPredictionVariance(tempFitter);

      tempFitter = (AbstractL2RLSFitter) m_CurrentFitter.addSamples(new double[][] {xRow.toArray()}, new double[] {0}, new double[] {2 * increment});
      
      newWeights = ArrayUtils.appendElement(m_CurrentFitter.getWeights(), 2 * increment);
      scaleFactor = m_WeightSum / (m_WeightSum + 2*increment);
      for (int sampleNum = 0; sampleNum < newWeights.length; sampleNum++) {
        newWeights[sampleNum] *= scaleFactor;
      }
      tempFitter = (AbstractL2RLSFitter) tempFitter.setWeights(newWeights);
      
      double step2 = m_DomainMatrix.getExpectedPredictionVariance(tempFitter);

      double grad1 = (step2 - step1) / increment;
      double grad2 = (step1 - this.getValue()) / increment;
      double gradgrad = (grad1 - grad2) / increment;
      
      double fastGrad = this.getDirect1stDerivativeForNewInput(xRow);
      double fastGradgrad = (grad2 - fastGrad) / (increment / 2);
      return gradgrad;
      
    }

    protected double getSlow1stDerivativeForNewInput(DoubleMatrix1D xRow) {
      
      double increment = 1E-8;    
      
      AbstractL2RLSFitter tempFitter = (AbstractL2RLSFitter) m_CurrentFitter.addSamples(new double[][] {xRow.toArray()}, new double[] {0}, new double[] {2*increment});
      
      double[] newWeights = ArrayUtils.appendElement(m_CurrentFitter.getWeights(), 2*increment);
      double scaleFactor = m_WeightSum / (m_WeightSum + 2*increment);
      for (int sampleNum = 0; sampleNum < newWeights.length; sampleNum++) {
        newWeights[sampleNum] *= scaleFactor;
      }
      tempFitter = (AbstractL2RLSFitter) tempFitter.setWeights(newWeights);
      
      double step2 = m_DomainMatrix.getExpectedPredictionVariance(tempFitter);
      
      return ((step2 - this.getValue()) / (2*increment));
      
    }
    
    public double estimateQuadMinimumForNewInput(double[] inputValues) {
      
      DoubleMatrix1D xRow = DoubleFactory1D.dense.make(inputValues);
      double a = this.getDirect2ndDerivativeForNewInput(xRow);
      double b = this.getDirect1stDerivativeForNewInput(xRow);
      /*if (b > 0) {
        System.out.println("Positive derivative!");
      }*/
      //b = Math.min(b, 0); // Make sure the sign of b is correct
      if (b >= 0 || a <= 0) {return Double.POSITIVE_INFINITY;}
      //return this.getValue() - 4 * ((b*b) / (2*a));  // TODO:  Why a factor of 4?
      return this.getValue() - ((b*b) / (2*a));  
      
    }
    
    public double getNegativeOptimalWeight(double[] inputValues) {
      
      DoubleMatrix1D xRow = DoubleFactory1D.dense.make(inputValues);
      double a = this.getDirect2ndDerivativeForNewInput(xRow);
      double b = this.getDirect1stDerivativeForNewInput(xRow);
      if (b >= 0 || a <= 0) {return Double.POSITIVE_INFINITY;}
      return b / a;
      
    }
    
    public double[] getSlowGradient(double[] template) {
      double[] gradient = (template == null) ? new double[this.numParameters()] : template;
      double[] origParameters = this.getUnboundedParameters(null);
      double[] pertParam = new double[origParameters.length];
      double dP = 0.001;
      for (int sampleNum = 0; sampleNum < this.numParameters(); sampleNum++) {
        System.arraycopy(origParameters, 0, pertParam, 0, origParameters.length);
        pertParam[sampleNum] += dP;
        this.setUnboundedParameters(pertParam);
        double plusValue = this.getValue();
        pertParam[sampleNum] -= 2 * dP;
        this.setUnboundedParameters(pertParam);
        double minusValue = this.getValue();
        
        gradient[sampleNum] = (plusValue - minusValue) / (2*dP);
      }
      this.setUnboundedParameters(origParameters);
      return gradient;
    }
    
  }
}
