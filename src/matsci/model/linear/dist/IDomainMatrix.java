/*
 * Created on May 26, 2006
 *
 */
package matsci.model.linear.dist;

import matsci.model.linear.*;
import cern.colt.matrix.*;

public interface IDomainMatrix {

  public double getExpectedPredictionVariance(LeastSquaresFitter fitter);
  public double getExpectedPredictionVariance(LeastSquaresFitter fitter, double oldValue, double[][] newValues, double[] weights);
  //public double getExpectedPredictionVariance(L2DiagRLSFitter fitter, double oldValue, double[][] newValues, double[] weights);

  public DoubleMatrix2D getMatrix();
  
}
