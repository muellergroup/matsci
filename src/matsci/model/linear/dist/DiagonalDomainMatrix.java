/*
 * Created on May 27, 2006
 *
 */
package matsci.model.linear.dist;


import cern.colt.matrix.*;
import matsci.model.linear.LeastSquaresFitter;

public class DiagonalDomainMatrix extends AbstractLSDomainMatrix {

  private final DoubleMatrix1D m_DistributionDiagonal;
  
  public DiagonalDomainMatrix(double[] distributionDiagonal) {
    m_DistributionDiagonal = DoubleFactory1D.dense.make(distributionDiagonal);
  }

  public double getExpectedPredictionVariance(LeastSquaresFitter fitter) {
    
    DoubleMatrix2D covBeta = fitter.getCovBeta();
    if (covBeta == null || fitter.numSamples() == 0) {return Double.POSITIVE_INFINITY;}
    double returnValue = 0;
    for (int rowNum = 0; rowNum < m_DistributionDiagonal.size(); rowNum++) {
      returnValue += covBeta.getQuick(rowNum, rowNum) * m_DistributionDiagonal.getQuick(rowNum);
    }
    return returnValue;
  }
  
  protected double getOverlapProduct(DoubleMatrix2D left, DoubleMatrix2D right) {
    double returnValue = 0;
    for (int rowNum = 0; rowNum < m_DistributionDiagonal.size(); rowNum++) {
        DoubleMatrix1D leftRow = left.viewRow(rowNum);
        DoubleMatrix1D rightCol = right.viewColumn(rowNum);
        returnValue += m_DistributionDiagonal.get(rowNum) * leftRow.zDotProduct(rightCol);
    }
    return returnValue;
  }

  public DoubleMatrix2D getMatrix() {
    return DoubleFactory2D.dense.diagonal(m_DistributionDiagonal);
  }

}
