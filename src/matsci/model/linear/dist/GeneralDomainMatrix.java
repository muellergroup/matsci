/*
 * Created on May 26, 2006
 *
 */
package matsci.model.linear.dist;

import matsci.model.linear.LeastSquaresFitter;
import cern.colt.matrix.*;
import cern.colt.matrix.linalg.*;
import cern.jet.math.Functions;

public class GeneralDomainMatrix extends AbstractLSDomainMatrix {

  private final DoubleMatrix2D m_DomainMatrix;
  
  public GeneralDomainMatrix(double[][] distributionMatrix) {
    m_DomainMatrix = DoubleFactory2D.dense.make(distributionMatrix);
  }
  
  public double getExpectedPredictionVariance(LeastSquaresFitter fitter) {
    
    DoubleMatrix2D covBeta = fitter.getCovBeta();
    if (covBeta == null || fitter.numSamples() == 0) {return Double.POSITIVE_INFINITY;}
    /*if (covBeta.size() > 1) {
      System.out.println("Condition: " + Algebra.DEFAULT.cond(covBeta));
      System.out.println("Determinant: " + Algebra.DEFAULT.det(covBeta));
    }*/
    Functions functions = Functions.functions;
    return covBeta.aggregate(m_DomainMatrix, functions.plus, functions.mult);
    
  }
  
  protected double getOverlapProduct(DoubleMatrix2D left, DoubleMatrix2D right) {
    double returnValue = 0;
    for (int rowNum = 0; rowNum < m_DomainMatrix.rows(); rowNum++) {
      for (int colNum = 0; colNum < m_DomainMatrix.columns(); colNum++) {
        DoubleMatrix1D leftRow = left.viewRow(rowNum);
        DoubleMatrix1D rightCol = right.viewColumn(colNum);
        returnValue += m_DomainMatrix.getQuick(rowNum, colNum) * leftRow.zDotProduct(rightCol);
      }
    }
    return returnValue;
  }

  public DoubleMatrix2D getMatrix() {
    return m_DomainMatrix.copy();
  }
 
}
