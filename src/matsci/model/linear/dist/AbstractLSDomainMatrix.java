/*
 * Created on May 27, 2006
 *
 */
package matsci.model.linear.dist;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.jet.math.Functions;
import matsci.model.linear.L2DiagRLSFitter;
import matsci.model.linear.LeastSquaresFitter;
import matsci.util.arrays.MatrixInverter;

public abstract class AbstractLSDomainMatrix implements IDomainMatrix {

  public AbstractLSDomainMatrix() {
    super();
  }

  public double getExpectedPredictionVariance(LeastSquaresFitter fitter, double oldValue, double[][] newInputs, double[] weights) {

    if (newInputs.length == 0) {return oldValue;}
    
    if (Double.isInfinite(oldValue) || Double.isNaN(oldValue) || !fitter.allowsIncrementalUpdates()) {
      return this.getSlowVariance(fitter, newInputs, weights);
    }

    DoubleMatrix2D covBeta = fitter.getCovBeta();
    if (covBeta == null) {return Double.POSITIVE_INFINITY;}
    //DoubleMatrix2D xT = this.scaleNewSamples(newInputs, weights);
    DoubleMatrix2D xT = DoubleFactory2D.dense.make(newInputs);

    DoubleMatrix2D ax = covBeta.zMult(xT, null, 1, 0, false, true);
    DoubleMatrix2D denominator = xT.zMult(ax, null);
    for (int rowNum = 0; rowNum < denominator.rows(); rowNum++) {
      double value = denominator.getQuick(rowNum, rowNum);
      //denominator.setQuick(rowNum, rowNum, value + 1);
      denominator.setQuick(rowNum, rowNum, value + 1.0/weights[rowNum]);
    }
    
    MatrixInverter inverter = new MatrixInverter(denominator);
    denominator = inverter.getInverse();
    if (denominator == null) {return Double.POSITIVE_INFINITY;}
    DoubleMatrix2D left = ax;
    DoubleMatrix2D right = denominator.zMult(ax, null, 1, 0, false, true);

    return oldValue - this.getOverlapProduct(left, right);
  }

  /*
  public double getExpectedPredictionVariance(L2DiagRLSFitter fitter, double oldValue, double[][] newInputs, double[] weights) {
    
    if (newInputs.length == 0) {return oldValue;}
    
    if (Double.isInfinite(oldValue) || Double.isNaN(oldValue) || !fitter.allowsIncrementalUpdates()) {
      return this.getSlowVariance(fitter, newInputs, weights);
    }

    DoubleMatrix2D covBeta = fitter.getCovBeta();
    if (covBeta == null) {return Double.POSITIVE_INFINITY;}
    
    DoubleMatrix2D xT = this.scaleNewSamples(newInputs, weights);
    DoubleMatrix2D A = fitter.getCoreInverse();
    if (A == null) {return Double.POSITIVE_INFINITY;}
    DoubleMatrix2D ABA = covBeta;
    
    DoubleMatrix2D ax = A.zMult(xT, null, 1, 0, false, true);
    DoubleMatrix2D c = xT.zMult(ax, null);
    DoubleMatrix2D ABAx = ABA.zMult(xT, null, 1, 0, false, true);
    DoubleMatrix2D d = xT.zMult(ABAx, null);
    
    for (int rowNum = 0; rowNum < c.rows(); rowNum++) {
      double cValue = c.getQuick(rowNum, rowNum);
      c.setQuick(rowNum, rowNum, cValue + 1);
      double dValue = d.getQuick(rowNum, rowNum);
      d.setQuick(rowNum, rowNum, dValue + 1);
    }
    MatrixInverter inverter = new MatrixInverter(c);
    DoubleMatrix2D cInverse = inverter.getInverse();
    if (cInverse == null) {return Double.POSITIVE_INFINITY;}
    
    // The right term (cInvxTa)
    DoubleMatrix2D right = cInverse.zMult(ax, null, 1, 0, false, true);

    // The left term (axcInvD-2ABAx)
    DoubleMatrix2D left = right.zMult(d, ABAx, 1, -2, true, false);
    
    double returnValue = oldValue + this.getOverlapProduct(left, right);
    //System.out.println("FAST: " + returnValue + ", SLOW: " + this.getSlowVariance(fitter, newInputs, weights));
    return returnValue;
  }*/
  
  /*
  protected DoubleMatrix2D scaleNewSamples(double[][] newValues, double[] weights) {
    DoubleMatrix2D x = DoubleFactory2D.dense.make(newValues);
    for (int rowNum = 0; rowNum < x.rows(); rowNum++) {
      double weightRoot = Math.sqrt(weights[rowNum]);
      for (int colNum = 0; colNum < x.columns(); colNum++) {
        double value = x.getQuick(rowNum, colNum);
        x.setQuick(rowNum, colNum, value * weightRoot);
      }
    }
    return x;
  }*/
  

  protected double getSlowVariance(LeastSquaresFitter fitter, double[][] newValues, double[] weights) {

    double[] dummyOutput = new double[newValues.length];
    boolean oldAllowedIncrementalUpdate = fitter.allowsIncrementalUpdates();
    fitter.allowIncrementalUpdates(false);
    LeastSquaresFitter newFitter = (LeastSquaresFitter) fitter.addSamples(newValues, dummyOutput, weights);
    fitter.allowIncrementalUpdates(oldAllowedIncrementalUpdate);
    return this.getExpectedPredictionVariance(newFitter);

  }
  
  protected abstract double getOverlapProduct(DoubleMatrix2D left, DoubleMatrix2D right);

}
