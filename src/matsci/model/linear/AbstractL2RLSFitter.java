/*
 * Created on May 17, 2007
 *
 */
package matsci.model.linear;

import java.util.Arrays;

import cern.colt.matrix.DoubleFactory1D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import matsci.model.reg.IRegGenerator;
import matsci.model.reg.IRegularizedFitter;
import matsci.util.arrays.ArrayUtils;
import matsci.util.arrays.MatrixInverter;

public abstract class AbstractL2RLSFitter extends LeastSquaresFitter implements IRegularizedFitter {

  protected static final int LKO_REG_CV_SCORE = 11;
  protected int m_LKO_REG_KValue = 1;
  
  //protected final DoubleMatrix2D m_Regularizer;
  
  protected double[] m_LKO_Reg_CV_Scores = new double[0];
  
  protected AbstractL2RLSFitter(DoubleMatrix2D inputValues, DoubleMatrix1D outputValues, DoubleMatrix1D weights) {
    super(inputValues, outputValues, weights);
  }
  
  protected AbstractL2RLSFitter(int numVars) {
    super(numVars);
  }
  
  protected AbstractL2RLSFitter(double[][] inputValues, double[] outputValues) {
    super(inputValues, outputValues);
  }
  
  protected AbstractL2RLSFitter(double[][] inputValues, double[] outputValues, double[] weights) {
    super(inputValues, outputValues, weights);
  }
  
  /*
  protected LeastSquaresFitter getFitter(DoubleMatrix2D inputValues, DoubleMatrix1D outputValues, DoubleMatrix1D weights) {
    return this.getFitter(inputValues, outputValues, m_Regularizer, weights);
  }*/
  
  /*protected LeastSquaresFitter getFitter(DoubleMatrix2D inputValues, DoubleMatrix1D outputValues, DoubleMatrix2D regularizer, DoubleMatrix1D weights) {
    L2FullRLSFitter returnFitter = new L2FullRLSFitter(inputValues, outputValues, regularizer, weights);
    this.copySettingsTo(returnFitter);
    return returnFitter;
  }*/
  
  /*
  protected void calculateCoreMatrix() throws SingularInputException {
    if (m_CoreMatrix == null) {
      DoubleMatrix2D xx = m_InputValues.zMult(m_InputValues, null, 1, 0, true, false);
      for (int rowNum = 0; rowNum < xx.rows(); rowNum++) {
        for (int colNum = 0; colNum < xx.columns(); colNum++) {
          double oldValue = xx.getQuick(rowNum, colNum);
          double newValue = oldValue + m_Regularizer.getQuick(rowNum, colNum);
          xx.setQuick(rowNum, colNum, newValue);
        }
      }
      m_CoreMatrix = new MatrixInverter(xx);
      m_CoreMatrix.updateIncrementally(m_AllowIncrementalUpdate);
      m_CoreMatrix.setNoSingularGuarantee(m_NoSingularGuarantee);
    }
    if (m_CoreMatrix.isSingular()) {
      throw SINGULAR_INPUT_EXCEPTION;
    }
  }*/
  
  /*public DoubleMatrix2D getXtX() {
    try {
      this.calculateCoreMatrix();
    } catch (SingularInputException e) {} // Doesn't matter
    
    DoubleMatrix2D returnMatrix = m_CoreMatrix.getQuickMatrix().copy();
    for (int rowNum = 0; rowNum < returnMatrix.rows(); rowNum++) {
      for (int colNum = 0; colNum < returnMatrix.columns(); colNum++) {
        double value = returnMatrix.getQuick(rowNum, colNum); 
        value -= m_Regularizer.getQuick(rowNum, colNum);
        returnMatrix.setQuick(rowNum, colNum, value);
      }
    }
    return returnMatrix;
  }*/
  
  /*protected double calculateGCVScore() {
    
    double rms = this.getRMSError();
    
    try {
      this.calculateCovBeta();
    } catch (SingularInputException e) {
      return Double.NaN;
    }
    
    double trace = m_InputValues.rows() - m_InputValues.columns();
    for (int rowNum = 0; rowNum < m_CovBeta.rows(); rowNum++) {
      for (int colNum = 0; colNum < m_CovBeta.columns(); colNum++) {
        trace += m_CovBeta.getQuick(rowNum, colNum) * m_Regularizer.getQuick(colNum, rowNum);
      }
    }
    
    if (trace < 1E-12) {return Double.NaN;}
    double scaleFactor = ((double) m_InputValues.rows()) / trace;
    return rms * scaleFactor;
  }*/
  
  /*protected double calculateExpectedErrorVariance() {
    //DoubleMatrix2D xTx = this.getXtX();
    //MatrixInverter inverter = new MatrixInverter(xTx);
   // if (inverter.isSingular()) {return Double.NaN;}
    //DoubleMatrix1D unregCoeff = inverter.getQuickInverse().zMult(m_XOutput, null);
    //DoubleMatrix1D unregPredOut = m_InputValues.zMult(unregCoeff, null);
    
    double rms = this.getRMSError();
    double rss = rms * rms * m_InputValues.rows();
    
    try {
      this.calculateCovBeta();
    } catch (SingularInputException e) {
      return Double.NaN;
    }
    
    double trace = m_InputValues.rows() - m_InputValues.columns();
    for (int rowNum = 0; rowNum < m_CovBeta.rows(); rowNum++) {
      for (int colNum = 0; colNum < m_CovBeta.columns(); colNum++) {
        trace += m_CovBeta.getQuick(rowNum, colNum) * m_Regularizer.getQuick(colNum, rowNum);
      }
    }
    
    return rss / trace;

  }*/
  
  public double scoreFit() {
    
    switch (m_ScoringMethod) {
        case LKO_REG_CV_SCORE:
          return this.getLKORegCVScore(m_LKO_REG_KValue);
        default:
          return super.scoreFit();
    }
  }

  public double getSlowLKORegCVScore(int k) {
    
    int[] kView = new int[k];
    int[] nonKView = new int[m_InputValues.rows() - k];
    int numSlices = m_InputValues.rows() / k;
    int[] columnView =new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }
    
    double cvSquared = 0;
    double[][] regularizer =this.getRegularizerMatrix();
    for (int sliceNum = 0; sliceNum < numSlices; sliceNum++) {
      int kIndex = 0;
      int nonKIndex = 0;
      for (int rowNum = 0; rowNum < m_InputValues.rows(); rowNum++) {
        if (rowNum >= sliceNum * k && rowNum < (sliceNum + 1) * k) {
          kView[kIndex++] = rowNum;
        } else {
          nonKView[nonKIndex++] = rowNum;
        }
      }
      DoubleMatrix1D yTrain = m_OutputValues.viewSelection(nonKView).copy();
      DoubleMatrix2D xTrain = m_InputValues.viewSelection(nonKView, columnView).copy();
      DoubleMatrix1D weights = null;
      if (m_Weights == null) {
        weights = DoubleFactory1D.dense.make(nonKView.length, 1);
      } else {
        weights = m_Weights.viewSelection(nonKView).copy();
      }
      LeastSquaresFitter fitter = this.getFitter(xTrain, yTrain, weights);

      DoubleMatrix2D xTest = m_InputValues.viewSelection(kView, columnView).copy();
      DoubleMatrix1D yPred = fitter.predictOutput(xTest);
      if (yPred == null) {return Double.NaN;}
      DoubleMatrix1D yTest = m_OutputValues.viewSelection(kView).copy();
      
      for (int rowNum = 0; rowNum < yPred.size(); rowNum++) {
        double pred = yPred.getQuick(rowNum);
        double test = yTest.getQuick(rowNum);
        double delta = pred - test;
        cvSquared += delta * delta;
      }
      
      double[] coefficients = fitter.getCoefficients();
      double regTerm = 0;
      for (int rowNum = 0; rowNum < coefficients.length; rowNum++) {
        double coefficient1 = coefficients[rowNum];
        double[] regRow = regularizer[rowNum];
        for (int colNum = 0; colNum < coefficients.length; colNum++) {
          double coefficient2 = coefficients[colNum];
          regTerm += coefficient1 * coefficient2 * regRow[colNum];
        }
      }
      cvSquared += k*regTerm;
      
    }

    return Math.sqrt(cvSquared / (k * numSlices));

  }
  
  protected void calculateLKORegCVScore(int k) {

    if (this.getKnownLKORegCVScore(k) >= 0) {return;}
    
    try {
        this.calculateCoreMatrix();
        this.calculatePredictedValues();
    } catch (SingularInputException e) {
      this.setKnownLKORegCVScore(k, Double.NaN);
      return;
    }
    
    int[] kView = new int[k];
    int numSlices = m_InputValues.rows() / k;
    int[] columnView =new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }
    DoubleMatrix1D numerator = DoubleFactory1D.dense.make(new double[k]);
    // Checks to see if the columns span the input space and the output is exactly the input
    boolean singular = false;
    double cvSquared = 0;
    DoubleMatrix2D inverse = m_CoreMatrix.getQuickInverse();
    
    double[] coefficients = this.getCoefficients();
    double[][] regularizer = this.getRegularizerMatrix();
    
    for (int sliceNum = 0; sliceNum < numSlices; sliceNum++) {
      for (int index = 0; index < k; index++) {
        int rowNum = sliceNum * k + index;
        kView[index] = rowNum;
        double delta = m_OutputValues.getQuick(rowNum) - m_PredictedValues.getQuick(rowNum);
        numerator.setQuick(index, delta);
      }
      DoubleMatrix2D xRows = m_InputValues.viewSelection(kView, columnView).copy();
      DoubleMatrix2D inverseXt = inverse.zMult(xRows, null, -1, 0, false, true);
      DoubleMatrix2D denominator = xRows.zMult(inverseXt, null);
      for (int rowNum = 0; rowNum < denominator.rows(); rowNum++) {
        denominator.setQuick(rowNum, rowNum, denominator.getQuick(rowNum, rowNum) + 1);
      }
      DoubleMatrix2D denomInverse = new MatrixInverter(denominator).getQuickInverse();
      if (denomInverse == null) {
        singular = true;
        break;
        //this.setKnownLKOCVScore(k, Double.NaN);
        //return;
      }
      //if (Math.sqrt(numerator.zDotProduct(numerator) / numerator.size()) > 0.0001) {singular = false;}  // Arbitrary check
      DoubleMatrix1D diff = denomInverse.zMult(numerator, null);
      for (int rowNum = 0; rowNum < diff.size(); rowNum++) {
        double cvDelta = diff.getQuick(rowNum);
        double delta = numerator.getQuick(rowNum);
        if (cvDelta * delta < delta * delta) { // Prevents sign changes and unrealistically lower cv deltas
          singular = true; 
          break;
        }
      }
      
      DoubleMatrix1D betaDelta = inverseXt.zMult(diff, null);
      double regTerm = 0;
      for (int rowNum = 0; rowNum < coefficients.length; rowNum++) {
        double newBeta1 = coefficients[rowNum] + betaDelta.getQuick(rowNum);
        double[] regRow = regularizer[rowNum];
        for (int colNum = 0; colNum < coefficients.length; colNum++) {
          double newBeta2 = coefficients[colNum] + betaDelta.getQuick(colNum);
          regTerm += newBeta1 * newBeta2 * regRow[colNum];
        }
      }
      
      cvSquared += diff.zDotProduct(diff) + k*regTerm;
    }
    double score = singular ? this.getSlowLKOCVScore(k) : Math.sqrt(cvSquared / (k * numSlices));
    
    //double score = this.getSlowLKOCVScore(k);
    this.setKnownLKORegCVScore(k, score);
  }
  
  public void useLKORegCVScore(int k) {
    m_ScoringMethod = LKO_REG_CV_SCORE;
    m_LKO_REG_KValue = k;
  }
  
  protected void setKnownLKORegCVScore(int k, double value) {
    if (k >= m_LKO_Reg_CV_Scores.length) {
      double[] incrementArray = new double[k - m_LKO_Reg_CV_Scores.length + 1];
      Arrays.fill(incrementArray, -1);
      m_LKO_Reg_CV_Scores = ArrayUtils.appendArray(m_LKO_Reg_CV_Scores, incrementArray);
    }
    m_LKO_Reg_CV_Scores[k] = value;
  }
  
  protected double getKnownLKORegCVScore(int k) {
    if (k >= m_LKO_Reg_CV_Scores.length) {
      return -1;
    }
    return m_LKO_Reg_CV_Scores[k];
  }
  
  public double getLKORegCVScore(int k) {
    this.calculateLKORegCVScore(k);
    return this.getKnownLKORegCVScore(k);
  }
  
  /*public L2DiagRLSFitter addVariables(double[][] inputValues, double[] regularizerVariables) {
    L2DiagRLSFitter returnFitter = (L2DiagRLSFitter) super.addVariables(inputValues);
    DoubleMatrix1D newRegularizer = DoubleFactory1D.dense.make(regularizerVariables);
    return (L2DiagRLSFitter) returnFitter.getFitter(returnFitter.m_InputValues, returnFitter.m_OutputValues, newRegularizer, returnFitter.m_Weights);
  }*/
  
  /*public L2DiagRLSFitter addVariables(double[][] inputValues, double[] regularizerVariables, int[] indices) {
    L2DiagRLSFitter returnFitter = this.addVariables(inputValues, regularizerVariables);
    Algebra linalg = new Algebra();
    int[] permutation = ArrayUtils.getInsertionPermutation(m_Regularizer.size(), indices);
    DoubleMatrix1D newRegularizer = linalg.permute(returnFitter.m_Regularizer, permutation, null);
    return (L2DiagRLSFitter) returnFitter.getFitter(returnFitter.m_InputValues, returnFitter.m_OutputValues, newRegularizer, returnFitter.m_Weights);
  }*/
  
  /*public IFitter addVariables(double[][] inputValues) {
    double[] regularizerVariables = (inputValues.length == 0) ? new double[0] : new double[inputValues[0].length];
    return this.addVariables(inputValues, regularizerVariables);
  }*/
  
  /*public IFitter addVariables(double[][] inputValues, int[] indices) {
    double[] regularizerVariables = new double[indices.length];
    return this.addVariables(inputValues, regularizerVariables, indices);
  }*/
  
  /*public IFitter removeVariables(int[] indicesToRemove) {
    L2FullRLSFitter returnFitter = (L2FullRLSFitter) super.removeVariables(indicesToRemove);
    int[] keeperView = ArrayUtils.getMissingIndices(indicesToRemove, m_Regularizer.size());
    DoubleMatrix2D newRegularizer = m_Regularizer.viewSelection(keeperView, keeperView).copy();
    return (L2FullRLSFitter) returnFitter.getFitter(returnFitter.m_InputValues, returnFitter.m_OutputValues, newRegularizer, returnFitter.m_Weights);
  }*/
  
  /*public IFitter clearVariables() {
    L2FullRLSFitter returnFitter = (L2FullRLSFitter) super.clearVariables();
    DoubleMatrix2D newRegularizer = DoubleFactory2D.dense.make(new double[0][0]);
    return (L2FullRLSFitter) returnFitter.getFitter(returnFitter.m_InputValues, returnFitter.m_OutputValues, newRegularizer, returnFitter.m_Weights);
  }*/
  
  /*protected void calculateCovBeta() throws SingularInputException {
    if (m_CovBeta == null) {
      this.calculateCoreMatrix();
      DoubleMatrix2D xTx = this.getXtX();
      DoubleMatrix2D coreInverse = m_CoreMatrix.getQuickInverse();
      m_CovBeta = coreInverse.zMult(xTx, null).zMult(coreInverse, null);
    }
  }*/
  
  /*public IRegularizedFitter setRegularizer(double[][] newRegularizer) {
    
    DoubleMatrix2D regularizer = DoubleFactory2D.dense.make(newRegularizer);
    return (IRegularizedFitter) this.getFitter(m_InputValues, m_OutputValues, regularizer, m_Weights);
  }*/
  
  /*public IRegularizedFitter setRegularizer(IRegGenerator generator, double[] parameters) {
    
    ILinearCovRegGenerator linearCovGenerator = (ILinearCovRegGenerator) generator;
    double[][] reg = linearCovGenerator.getRegularizer(parameters, null);
    return this.setRegularizer(reg);
    
  }*/
  
  public abstract DoubleMatrix2D getRegularizerMatrixColt();
  public abstract double[][] getRegularizerMatrix();
  public abstract IRegularizedFitter setRegularizer(IRegGenerator generator, double[] parameters);
  
  /*public double getRegularizer(int rowNum, int colNum) {
    return m_Regularizer.getQuick(rowNum, colNum);
  }
  
  public double[][] getRegularizer() {
    return m_Regularizer.toArray();
  }*/

}
