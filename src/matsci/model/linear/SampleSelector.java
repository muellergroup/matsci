/*
 * Created on Sep 15, 2005
 *
 */
package matsci.model.linear;

import matsci.engine.monte.metropolis.*;
import matsci.util.arrays.*;
import cern.colt.matrix.*;
import cern.colt.matrix.linalg.Algebra;
/**
 * @author Tim Mueller
 *
 */
public abstract class SampleSelector implements IAllowsMetropolis {
  
  protected double m_KnownScore;
  protected DoubleMatrix2D m_KnownInputs;
  protected MatrixInverter m_KnownInverse;
  
  // Only one of these should be non-null
  protected double[][] m_OverlapMatrix;
  protected double[] m_OverlapDiagonal;
  
  protected Event m_Event;

  public SampleSelector(double[][] knownValues, double[] overlapDiagonal) {
    
    DoubleMatrix2D m_KnownValues = DoubleFactory2D.dense.make(knownValues);
    DoubleMatrix2D xTx = m_KnownValues.zMult(m_KnownValues, null, 1, 0, true, false);
    m_KnownInverse = new MatrixInverter(xTx);
    m_OverlapDiagonal = ArrayUtils.copyArray(overlapDiagonal);
    m_KnownScore = this.scaleMatrix(m_KnownInverse.getQuickInverse());
    m_Event = new Event();
  }
  
  /**
   * 
   */
  public SampleSelector(double[][] knownValues, double[][] overlapMatrix) {
    m_KnownInputs = DoubleFactory2D.dense.make(knownValues);
    DoubleMatrix2D xTx = m_KnownInputs.zMult(m_KnownInputs, null, 1, 0, true, false);
    m_KnownInverse = new MatrixInverter(xTx);
    m_OverlapDiagonal = this.isDiagonal(overlapMatrix);
    if (m_OverlapDiagonal == null) {m_OverlapMatrix = ArrayUtils.copyArray(m_OverlapMatrix);}
    m_KnownScore = this.scaleMatrix(m_KnownInverse.getQuickInverse());
    m_Event = new Event();
  }
  
  protected double[] isDiagonal(double[][] matrix) {
    
    double[] returnArray = new double[matrix.length];
    for (int rowNum = 0; rowNum < matrix.length; rowNum++) {
      double[] row = matrix[rowNum];
      if (row.length != matrix.length) {return null;} // It's not a square matrix
      returnArray[rowNum] = matrix[rowNum][rowNum];
      for (int colNum = 0; colNum < row.length; colNum++) {
        if (colNum == rowNum) {continue;}
        if (row[colNum] != 0) {return null;} // It's not diagonal
      }
    }
    return returnArray;
    
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getValue()
   */
  public double getValue() {
    return this.calculateScore(this.getCurrentSample());
  }
  
  public double calculateScore(double[] testSample) {
    if (m_KnownInverse.isSingular()) {return this.slowCalculateScore(testSample);}
    
    DoubleMatrix2D sampleMatrix = DoubleFactory2D.dense.make(new double[][] {testSample});
    DoubleMatrix2D product = sampleMatrix.zMult(m_KnownInverse.getQuickInverse(), null);
    double numerator = this.scaleMatrix(product.zMult(product, null, 1, 0, true, false));
    double denominator = 1 + product.zMult(sampleMatrix, null, 1, 0, false, true).getQuick(0, 0);
    
    return Math.sqrt(m_KnownScore - (numerator / denominator));
  }
  
  public double getKnownScore() {
    return m_KnownScore;
  }
  
  private double slowCalculateScore(double[] testSample) {
    
    DoubleMatrix2D delta2D = DoubleFactory2D.dense.make(new double[][] {testSample});
    DoubleMatrix2D allCorrelations = DoubleFactory2D.dense.appendRows(m_KnownInputs, delta2D);
    
    DoubleMatrix2D xx = allCorrelations.zMult(allCorrelations,  null, 1, 0, true, false);
    DoubleMatrix2D xx_Inverse = new MatrixInverter(xx).getQuickInverse();
    
    if (xx_Inverse == null) {return Double.NaN;}
    
    return Math.sqrt(this.scaleMatrix(xx_Inverse));
  }
  
  private double scaleMatrix(DoubleMatrix2D matrix) {
    
    if (matrix == null) {return Double.NaN;}
    if (matrix.size() == 0) {return Double.NaN;}
    
    double returnValue = 0;
    
    if (m_OverlapMatrix == null) { // We can just manage the diagonal
      for (int varNum = 0; varNum < m_OverlapDiagonal.length; varNum++) {
        returnValue += matrix.getQuick(varNum, varNum) * m_OverlapDiagonal[varNum];
      }
    } else {
      for (int rowNum = 0; rowNum < m_OverlapMatrix.length; rowNum++) {
        double[] overlapRow = m_OverlapMatrix[rowNum];
        for (int colNum = 0; colNum < overlapRow.length; colNum++) {
          returnValue += matrix.getQuick(rowNum, colNum) * overlapRow[colNum];
        }
      }
    }
    
    return returnValue;
    
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getRandomEvent()
   */
  public IMetropolisEvent getRandomEvent() {
    m_Event.setDelta(this.getRandomStateDelta(m_Event.m_Delta));
    return m_Event;
  }
  
  public abstract double[] getCurrentSample();
  public abstract double[] getSampleForDelta(Object delta);
  public abstract Object getRandomStateDelta(Object oldDelta);
  public abstract void applyDelta(Object delta);
  
  protected class Event implements IMetropolisEvent {

    protected Object m_Delta;
    protected Object m_OldState;
    
    public Event() {
    }
    
    public void setDelta(Object delta) {
      m_Delta = delta;
      m_OldState = getSnapshot(m_OldState);
    }
    
    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#trigger()
     */
    public void trigger() {
      applyDelta(m_Delta);
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#getDelta()
     */
    public double getDelta() {
      if (m_Delta == null) {return Double.POSITIVE_INFINITY;}
      double oldScore = getValue();
      double newScore = calculateScore(getSampleForDelta(m_Delta));
      return newScore - oldScore;
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#triggerGetDelta()
     */
    public double triggerGetDelta() {
      if (m_Delta == null) {return Double.POSITIVE_INFINITY;}
      double oldScore = getValue();
      applyDelta(m_Delta);
      double newScore = getValue();
      return newScore - oldScore;
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#reverse()
     */
    public void reverse() {
      setState(m_OldState);
    }
    
  }

}
