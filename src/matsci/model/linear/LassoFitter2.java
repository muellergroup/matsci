/*
 * Created on Feb 14, 2007
 *
 */
package matsci.model.linear;

import java.util.Arrays;

import cern.colt.matrix.DoubleFactory1D;
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import matsci.model.IFitter;
import matsci.model.reg.ILinearRegGenerator;
import matsci.model.reg.IRegGenerator;
import matsci.model.reg.IRegularizedFitter;
import matsci.util.arrays.ArrayUtils;
import matsci.util.arrays.MatrixInverter;

public class LassoFitter2 implements IRegularizedFitter, ILinearFitter {

  // TODO replace this with an enum someday.
  protected int m_ScoringMethod = BIC;
  protected int m_LKO_KValue = 1;

  protected static final int LKO_CV_SCORE = 1;
  protected static final int RMS_SCORE = 2;
  protected static final int UNWEIGHTED_LKO_CV_SCORE = 3;
  protected static final int BIC = 4;
  protected static final int AIC = 5;
  protected static final int SCALED_AIC = 6;
  protected static final int SCALED_BIC = 7;
  protected static final int PSEUDO_GCV = 8;
  
  protected final DoubleMatrix2D m_InputValues;
  protected final DoubleMatrix1D m_OutputValues;
  protected final double[] m_Weights;
  protected final double[] m_Regularizer;
  
  protected static final double m_RegTolerance = 1E-60;
  protected final int[] m_ZeroRegVars;
  //protected final int[] m_NonZeroRegVars;
  
  protected LeastSquaresFitter m_LSFitter;
  
  protected DoubleMatrix1D m_PredictedOutput;
  protected DoubleMatrix1D m_Coefficients;
  protected int[] m_ActiveVariables;
  
  protected double m_AIC = -1;
  protected double m_BIC = -1;
  protected double m_RMS = -1;
  protected double m_UnweightedRMS = -1;
  protected double m_LASSOScore = -1;
  protected double[] m_LKO_CV_Scores = new double[0];
  protected double[] m_Unweighted_LKO_CV_Scores = new double[0];
  protected double m_PSEUDO_GCV = -1;
  
  public LassoFitter2(double[][] inputValues, double[] outputValues) {
    this(inputValues, outputValues, oneWeights(outputValues.length), new double[inputValues[0].length]);
  }
  
  protected static double[] oneWeights(int numSamples) {
    double[] returnArray = new double[numSamples];
    Arrays.fill(returnArray, 1);
    return returnArray;
  }
  
  protected LassoFitter2(DoubleMatrix2D scaledInput, DoubleMatrix1D scaledOutput, double[] weights, double[] regularizer) {
    m_InputValues = scaledInput;
    m_OutputValues = scaledOutput;
    m_Weights = ArrayUtils.copyArray(weights);
    //regularizer[0] = 1E-5;
    m_Regularizer = ArrayUtils.copyArray(regularizer);

    int numZeroReg = 0;
    for (int varNum = 0; varNum < regularizer.length; varNum++) {
      double varReg = regularizer[varNum];
      if (Math.abs(varReg) <= m_RegTolerance) {numZeroReg++;}
    }
    m_ZeroRegVars = new int[numZeroReg];
    int totalVarIndex = 0;
    for (int zeroRegIndex = 0; zeroRegIndex < numZeroReg; zeroRegIndex++) {
      double varReg = regularizer[totalVarIndex++];
      while (Math.abs(varReg) > m_RegTolerance) {
        varReg = regularizer[totalVarIndex++];
      }
      m_ZeroRegVars[zeroRegIndex] = totalVarIndex - 1;
    }
    
    m_LSFitter = new LeastSquaresFitter(scaledInput, scaledOutput, DoubleFactory1D.dense.make(weights));
    if (m_LSFitter.getCoreInverse() == null) {
      throw new RuntimeException("Input values must be linearly independent to use the Lasso Fitter");
    }
  }
  
  public LassoFitter2(double[][] inputValues, double[] outputValues, double[] weights, double[] regularizer) {
    m_InputValues = DoubleFactory2D.dense.make(inputValues);
    m_OutputValues = DoubleFactory1D.dense.make(outputValues);
    m_Weights = ArrayUtils.copyArray(weights);
    //regularizer[0] = 1E-5;
    m_Regularizer = ArrayUtils.copyArray(regularizer);
    m_LSFitter = new LeastSquaresFitter(inputValues, outputValues, weights);
    if (m_LSFitter.getCoreInverse() == null) {
      throw new RuntimeException("Input values must be linearly independent to use the Lasso Fitter");
    }
    
    int numZeroReg = 0;
    for (int varNum = 0; varNum < regularizer.length; varNum++) {
      double varReg = regularizer[varNum];
      if (Math.abs(varReg) <= m_RegTolerance) {numZeroReg++;}
    }
    m_ZeroRegVars = new int[numZeroReg];
    int totalVarIndex = 0;
    for (int zeroRegIndex = 0; zeroRegIndex < numZeroReg; zeroRegIndex++) {
      double varReg = regularizer[totalVarIndex++];
      while (Math.abs(varReg) > m_RegTolerance) {
        varReg = regularizer[totalVarIndex++];
      }
      m_ZeroRegVars[zeroRegIndex] = totalVarIndex - 1;
    }
    
    this.scaleInput(m_InputValues, weights);
    this.scaleOutput(m_OutputValues, weights);    
  }
  
  protected LassoFitter2 getFitter(DoubleMatrix2D inputValues, DoubleMatrix1D outputValues, double[] weights, double[] regularizer) {
    LassoFitter2 returnFitter = new LassoFitter2(inputValues, outputValues, weights, regularizer);
    this.copySettingsTo(returnFitter);
    return returnFitter;
  }
  
  public void copySettingsTo(LassoFitter2 fitter) {
    fitter.m_ScoringMethod = m_ScoringMethod;
  }
  
  protected void scaleInput(DoubleMatrix2D inputValues, double[] weights) {
    
    for (int rowNum = 0; rowNum < inputValues.rows(); rowNum++) {
      double weightRoot = Math.sqrt(weights[rowNum]);
      for (int colNum = 0; colNum < inputValues.columns(); colNum++) {
        double inputValue = inputValues.getQuick(rowNum, colNum);
        inputValues.setQuick(rowNum, colNum, inputValue * weightRoot);
      }
    }
    
  }

  protected void scaleOutput(DoubleMatrix1D outputValues, double[] weights) {
    for (int rowNum = 0; rowNum < outputValues.size(); rowNum++) {
      double weightRoot = Math.sqrt(weights[rowNum]);
      double outputValue = outputValues.getQuick(rowNum);
      outputValues.setQuick(rowNum, outputValue * weightRoot);
    }
  }
  
  public LeastSquaresFitter getLSFitter() {
    return m_LSFitter;
  }
  
  public double[] getCoefficients() {
    this.runModifiedLARS();
    return m_Coefficients.toArray();
  }

  public IFitter addSamples(double[][] inputValues, double[] outputValues, double[] weights) {
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(inputValues);
    DoubleMatrix1D newOutput = DoubleFactory1D.dense.make(outputValues);
    this.scaleInput(newInput, weights);
    this.scaleOutput(newOutput, weights);
    double[] allWeights = ArrayUtils.appendArray(m_Weights, weights);
    DoubleMatrix2D allInput = DoubleFactory2D.dense.appendRows(m_InputValues, newInput);
    DoubleMatrix1D allOutput = DoubleFactory1D.dense.append(m_OutputValues, newOutput);
    return this.getFitter(allInput, allOutput, allWeights, m_Regularizer);
  }

  public IFitter addSamples(double[][] inputValues, double[] outputValues) {
    double[] newWeights = new double[inputValues.length];
    Arrays.fill(newWeights, 1);
    return this.addSamples(inputValues, outputValues, newWeights);
  }

  public IFitter removeSamples(int[] rowsToRemove) {
    int[] rowView = ArrayUtils.getMissingIndices(rowsToRemove, m_InputValues.rows());
    int[] columnView = ArrayUtils.getMissingIndices(new int[0], m_InputValues.columns());
    
    DoubleMatrix2D newInput = m_InputValues.viewSelection(rowView, columnView).copy();
    DoubleMatrix1D newOutput =m_OutputValues.viewSelection(rowView).copy();
    double[] newWeights = new double[rowView.length];
    for (int newRowNum = 0; newRowNum < rowView.length; newRowNum++) {
      newWeights[newRowNum] = m_Weights[rowView[newRowNum]];
    }
    return this.getFitter(newInput, newOutput, newWeights, m_Regularizer);
  }

  public IFitter removeVariables(int[] varsToRemove) {
    int[] columnView = ArrayUtils.getMissingIndices(varsToRemove, m_InputValues.columns());
    int[] rowView = ArrayUtils.getMissingIndices(new int[0], m_InputValues.rows());

    DoubleMatrix2D newInput = m_InputValues.viewSelection(rowView, columnView).copy();
    double[] newRegularizer = new double[rowView.length];
    for (int newVarNum = 0; newVarNum < columnView.length; newVarNum++) {
      newRegularizer[newVarNum] = m_Regularizer[columnView[newVarNum]];
    }
    return this.getFitter(newInput, m_OutputValues, m_Weights, newRegularizer);
  }

  /*
  public IFitter addVariables(double[][] inputValues, int[] indices) {
    LassoFitter addedFitter = (LassoFitter) this.addVariables(inputValues);
    Algebra linalg = new Algebra();
    int[] permutation = ArrayUtils.getInsertionPermutation(m_InputValues.columns(), indices);
    
    linalg.permuteColumns(addedFitter.m_InputValues, permutation, null);
    double[] oldRegularizer = ArrayUtils.copyArray(m_Regularizer);
    double[] newRegularizer = new double[addedFitter.m_Regularizer.length];
    for (int varIndex = 0; varIndex < permutation.length; varIndex++) {
      newRegularizer[varIndex] = oldRegularizer[permutation[varIndex]];
    }
    
    return this.getFitter(addedFitter.m_InputValues, addedFitter.m_OutputValues, addedFitter.m_Weights, newRegularizer);
  }*/

  public IFitter addVariables(double[][] inputValues) {
    if (inputValues.length == 0) {
      return this.getFitter(this.m_InputValues, this.m_OutputValues, this.m_Weights, this.m_Regularizer);
    }
    double[] regularizer = new double[inputValues[0].length];
    return this.addVariables(inputValues, regularizer);
  }
  
  public IFitter addVariables(double[][] inputValues, double[] regularizer) {
    DoubleMatrix2D inputDelta = DoubleFactory2D.dense.make(inputValues);
    this.scaleInput(inputDelta, m_Weights);
    double[] newRegularizer = ArrayUtils.appendArray(m_Regularizer, regularizer);
    DoubleMatrix2D newInput = DoubleFactory2D.dense.appendColumns(m_InputValues, inputDelta);
    return this.getFitter(newInput, m_OutputValues, m_Weights, newRegularizer); 
  }

  public int numVariables() {
    return m_InputValues.columns();
  }

  public int numSamples() {
    return m_InputValues.rows();
  }
  
  public double[][] getInputValues() {
    double[][] returnArray = new double[m_InputValues.rows()][m_InputValues.columns()];
    for (int sampleNum = 0; sampleNum < m_InputValues.rows(); sampleNum++) {
      for (int varNum = 0; varNum < m_InputValues.columns(); varNum++) {
        returnArray[sampleNum][varNum] = this.getInputValue(sampleNum, varNum);
      }
    }
    return returnArray;
  }

  public double getInputValue(int sampleNum, int varNum) {
    double scaledInput = m_InputValues.get(sampleNum, varNum);
    double weightRoot = Math.sqrt(m_Weights[sampleNum]);
    return scaledInput / weightRoot;
  }

  public double getOutputValue(int sampleNum) {
    double weightRoot = Math.sqrt(m_Weights[sampleNum]);
    return m_OutputValues.get(sampleNum) / weightRoot;
  }
  
  public double[] getOutputValues() {
    double[] returnArray = new double[m_OutputValues.size()];
    for (int sampleNum = 0; sampleNum < returnArray.length; sampleNum++) {
      returnArray[sampleNum] = this.getOutputValue(sampleNum);
    }
    return returnArray;
  }
  
  public double getPredictedOutput(int rowNum) {
    if (m_PredictedOutput == null) {
      this.runModifiedLARS();
    }
    
    double weight = this.getWeight(rowNum);
    return m_PredictedOutput.getQuick(rowNum) / Math.sqrt(weight);
  }

  public double getWeight(int sampleNum) {
    return m_Weights[sampleNum];
  }
  
  public double getWeightSum() {
    double returnValue = 0;
    for (int sampleNum = 0; sampleNum < m_Weights.length; sampleNum++) {
      returnValue += m_Weights[sampleNum];
    }
    return returnValue;
  }

  public double[] getWeights() {
    return ArrayUtils.copyArray(m_Weights);
  }

  public IFitter clearVariables() {
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(m_OutputValues.size(), 0);
    return this.getFitter(newInput, m_OutputValues, m_Weights, new double[0]);
  }

  public IFitter clearSamples() {
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(0, m_InputValues.columns());
    DoubleMatrix1D newOutput = DoubleFactory1D.dense.make(0);
    return this.getFitter(newInput, newOutput, new double[0], m_Regularizer);
  }

  public double scoreFit() {
    
    switch (m_ScoringMethod) {
        case AIC:
          return this.getAIC();
        case BIC:
          return this.getBIC();
        case SCALED_AIC:
          return this.getScaledAIC();
        case SCALED_BIC:
          return this.getScaledBIC();
        case RMS_SCORE:
          return this.getRMSError();
        case LKO_CV_SCORE:
          return this.getLKOCVScore(m_LKO_KValue);
        case PSEUDO_GCV:
          return this.getPseudoGCVScore();
        default:
          throw new RuntimeException("Unknown scoring method.");
    }
  }
  
  public void useScaledAIC() {
    m_ScoringMethod = SCALED_AIC;
  }
  
  public void useScaledBIC() {
    m_ScoringMethod = SCALED_BIC;
  }
  
  public void useAIC() {
    m_ScoringMethod = AIC;
  }
  
  public void useBIC() {
    m_ScoringMethod = BIC;
  }
  
  public void usePseudoGCV() {
    m_ScoringMethod = PSEUDO_GCV;
  }
  
  public void useLKOCVScore(int k) {
    m_ScoringMethod = LKO_CV_SCORE;
    m_LKO_KValue = k;
  }
  
  public void useRMS() {
    m_ScoringMethod = RMS_SCORE;
  }
  
  protected double[] verifyPredictedValues() {
    this.runModifiedLARS();
    double[] returnArray = new double[m_InputValues.rows()];
    double[] coefficients = this.getCoefficients();
    for (int varIndex = 0; varIndex < m_ActiveVariables.length; varIndex++) {
      int varNum = m_ActiveVariables[varIndex];
      double coefficient = coefficients[varNum];
      for (int sampleNum = 0; sampleNum < returnArray.length; sampleNum++) {
        returnArray[sampleNum] += coefficient * this.getInputValue(sampleNum, varNum);
      }
    }
    
    return returnArray;
  }
  
  public double getLKOCVScore(int k) {
    this.calculateSlowLKOCVScore(k);
    //System.out.println("LASSO Score: " + this.getLASSOScore() + ", L" + k + "O CV Score:" + this.getKnownLKOCVScore(k));
    return this.getKnownLKOCVScore(k);
  }
  
  public double getUnweightedRMSError() {
    if (m_UnweightedRMS < 0) {
      this.calculateUnweightedRMS();
    }
    return m_UnweightedRMS;
  }
  
  public double getRMSError() {
    if (m_RMS < 0) {
      this.calculateRMS();
    }
    return m_RMS;
  }
  
  public double getLASSOScore() {
    if (m_LASSOScore < 0) {
      this.calculateLASSOPenalty();
    }
    return m_LASSOScore;
  }
  
  public double getBIC() {
    if (m_BIC < 0) {
      this.calculateBIC();
    }
    return m_BIC;
  }
  
  public double getAIC() {
    if (m_AIC < 0) {
      this.calculateAIC();
    }
    return m_AIC;
  }
  
  public double getScaledBIC() {
    if (m_BIC < 0) {
      this.calculateBIC();
    }
    double sigSqEstimate = this.getLSFitter().getExpectedErrorVariance();
    double returnValue = m_BIC * sigSqEstimate;
    //System.out.println("Scaled BIC: " + returnValue);
    return returnValue;
  }
  
  public double getScaledAIC() {
    if (m_AIC < 0) {
      this.calculateAIC();
    }
    double sigSqEstimate = this.getLSFitter().getExpectedErrorVariance();
    double returnValue = m_AIC * sigSqEstimate;
    //System.out.println("Scaled AIC: " + returnValue);
    return returnValue;
  }
  
  public double getPseudoGCVScore() {
    if (m_PSEUDO_GCV < 0) {
      m_PSEUDO_GCV = this.calculatePseudoGCV();
    }
    return m_PSEUDO_GCV;
  }
  
  protected double getKnownLKOCVScore(int k) {
    if (k >= m_LKO_CV_Scores.length) {
      return -1;
    }
    return m_LKO_CV_Scores[k];
  }
  
  protected void setKnownLKOCVScore(int k, double value) {
    if (k >= m_LKO_CV_Scores.length) {
      double[] incrementArray = new double[k - m_LKO_CV_Scores.length + 1];
      Arrays.fill(incrementArray, -1);
      m_LKO_CV_Scores = ArrayUtils.appendArray(m_LKO_CV_Scores, incrementArray);
    }
    m_LKO_CV_Scores[k] = value;
  }
  
  public double[] getCVErrors(boolean useWeights) {
    
    this.runModifiedLARS();
    
    int[] kView = new int[1];
    int[] nonKView = new int[m_InputValues.rows() - 1];
    int numSlices = m_InputValues.rows();
    int[] columnView =new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }
    
    double[] returnArray = new double[m_InputValues.rows()];
    double[] nonKWeights = new double[m_Weights.length - 1];
    for (int sliceNum = 0; sliceNum < numSlices; sliceNum++) {
      int kIndex = 0;
      int nonKIndex = 0;
      for (int rowNum = 0; rowNum < m_InputValues.rows(); rowNum++) {
        if (rowNum == sliceNum) {
          kView[kIndex++] = rowNum;
        } else {
          nonKWeights[nonKIndex] = m_Weights[rowNum];
          nonKView[nonKIndex++] = rowNum;
        }
      }
      DoubleMatrix1D yTrain = m_OutputValues.viewSelection(nonKView).copy();
      DoubleMatrix2D xTrain = m_InputValues.viewSelection(nonKView, columnView).copy();
      LassoFitter2 fitter = this.getFitter(xTrain, yTrain, nonKWeights, m_Regularizer);

      DoubleMatrix2D xTest = m_InputValues.viewSelection(kView, columnView).copy();
      DoubleMatrix1D yPred = fitter.predictOutput(xTest);
      //DoubleMatrix1D yTest = m_OutputValues.viewSelection(kView).copy();
      
      double pred = yPred.getQuick(0);
      double test = m_OutputValues.getQuick(sliceNum); //yTest.getQuick(0);
      double weightRoot = useWeights ? 1 : Math.sqrt(this.getWeight(sliceNum));
      returnArray[sliceNum] = (pred-test) / weightRoot;
    }
    return returnArray;

  }
  
  // From the original LASSO paper by Tibrishani
  protected double calculatePseudoGCV() {
    
    this.runModifiedLARS();
    
    if (m_Coefficients == null) {
      return Double.NaN;
    }
    
    DoubleMatrix2D xTx = m_LSFitter.getCoreMatrix();
    //DoubleMatrix2D xTxOrig = xTx.copy();
    for (int rowNum = 0; rowNum < xTx.rows(); rowNum++) {
      double coefficient = m_Coefficients.getQuick(rowNum);
      if (coefficient == 0) {continue;}
      double oldVal = xTx.getQuick(rowNum, rowNum);
      double newVal = oldVal + m_Regularizer[rowNum] / Math.abs(coefficient);
      xTx.setQuick(rowNum, rowNum, newVal);
    }
    MatrixInverter inverter = new MatrixInverter(xTx);
    //DoubleMatrix2D product = inverter.getInverse().zMult(xTxOrig, null);
    double trace = m_InputValues.rows() - m_InputValues.columns();
    //double trace2 = xTx.rows();
    for (int rowNum = 0; rowNum < xTx.rows(); rowNum++) {
      //trace2 -= product.getQuick(rowNum, rowNum);
      double coefficient = m_Coefficients.getQuick(rowNum);
      if (coefficient == 0) {continue;}
      trace += inverter.getInverseElement(rowNum, rowNum) * m_Regularizer[rowNum] / Math.abs(coefficient);
    }
    
    if (trace < 1E-12) {return Double.POSITIVE_INFINITY;}
    double rms = this.getRMSError();
    double scaleFactor = ((double) m_InputValues.rows()) / trace;
    return rms * scaleFactor;
  }
  
  protected void calculateSlowLKOCVScore(int k) {
    
    if (this.getKnownLKOCVScore(k) >= 0) {return;}
    
    this.runModifiedLARS();
    
    int[] kView = new int[k];
    int[] nonKView = new int[m_InputValues.rows() - k];
    int numSlices = m_InputValues.rows() / k;
    int[] columnView =new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }
    
    double cvSquared = 0;
    double[] nonKWeights = new double[m_Weights.length - k];
    //double sumWeights =0;
    for (int sliceNum = 0; sliceNum < numSlices; sliceNum++) {
      int kIndex = 0;
      int nonKIndex = 0;
      for (int rowNum = 0; rowNum < m_InputValues.rows(); rowNum++) {
        if (rowNum >= sliceNum * k && rowNum < (sliceNum + 1) * k) {
          kView[kIndex++] = rowNum;
          //sumWeights += m_Weights[rowNum];
        } else {
          nonKWeights[nonKIndex] = m_Weights[rowNum];
          nonKView[nonKIndex++] = rowNum;
        }
      }
      DoubleMatrix1D yTrain = m_OutputValues.viewSelection(nonKView).copy();
      DoubleMatrix2D xTrain = m_InputValues.viewSelection(nonKView, columnView).copy();
      LassoFitter2 fitter= null;
      try {
        fitter = this.getFitter(xTrain, yTrain, nonKWeights, m_Regularizer);
      } catch (Exception e) {
        this.setKnownLKOCVScore(k, Double.POSITIVE_INFINITY);
        return;
      }
      DoubleMatrix2D xTest = m_InputValues.viewSelection(kView, columnView).copy();
      DoubleMatrix1D yPred = fitter.predictOutput(xTest);
      DoubleMatrix1D yTest = m_OutputValues.viewSelection(kView).copy();
      
      for (int rowNum = 0; rowNum < yPred.size(); rowNum++) {
        double pred = yPred.getQuick(rowNum);
        double test = yTest.getQuick(rowNum);
        double delta = pred - test;
        cvSquared += delta * delta;
      }
    }

    this.setKnownLKOCVScore(k, Math.sqrt(cvSquared / (k * numSlices)));
    //this.setKnownLKOCVScore(k, Math.sqrt(cvSquared / sumWeights));

  }
  
  protected DoubleMatrix1D predictOutput(DoubleMatrix2D inputValues) {

    this.runModifiedLARS();
    return (inputValues.zMult(m_Coefficients, null));
  
  }
  
  protected void calculateBIC() {
    LeastSquaresFitter fitter = this.getLSFitter();
    double sigSqEstimate = fitter.getExpectedErrorVariance();
    double rms = this.getRMSError();
    double n = m_InputValues.rows();
    double degreesOfFreedom = m_ActiveVariables.length;
    m_BIC = (rms * rms / sigSqEstimate) + Math.log(n) * degreesOfFreedom / n;
    //System.out.println("RMS: " + rms + ", BIC: " + m_BIC + ", LASSO: " + this.getLASSOScore());
  }
  
  protected void calculateAIC() {
    LeastSquaresFitter fitter = this.getLSFitter();
    double sigSqEstimate = fitter.getExpectedErrorVariance();
    double rms = this.getRMSError();
    double n = m_InputValues.rows();
    double degreesOfFreedom = m_ActiveVariables.length;
    m_AIC = (rms * rms / sigSqEstimate) + 2 * degreesOfFreedom / n;
    //System.out.println("RMS: " + rms + ", AIC: " + m_AIC + ", LASSO: " + this.getLASSOScore());
  }
  
  protected void calculateUnweightedRMS() {
    this.runModifiedLARS();
    double totalDelta = 0;
    for (int rowNum = 0; rowNum < m_OutputValues.size(); rowNum++) {
      double delta = m_OutputValues.getQuick(rowNum) - m_PredictedOutput.getQuick(rowNum);
      totalDelta += delta * delta / this.getWeight(rowNum);
    }
    m_UnweightedRMS = Math.sqrt(totalDelta / m_OutputValues.size());
  }
  
  protected void calculateRMS() {
    this.runModifiedLARS();
    double totalDelta = 0;
    for (int rowNum = 0; rowNum < m_OutputValues.size(); rowNum++) {
      double delta = m_OutputValues.getQuick(rowNum) - m_PredictedOutput.getQuick(rowNum);
      totalDelta += delta * delta;
    }
    //double[] verifiedPrediction = this.verifyPredictedValues();
    m_RMS = Math.sqrt(totalDelta / m_OutputValues.size());
  }
  
  protected void calculateLASSOPenalty() {
    double penalty = this.getRMSError();
    penalty *= penalty;
    penalty *= m_OutputValues.size();
    penalty /= 2;
    double[] coefficients =this.getCoefficients();
    for (int varNum = 0; varNum < m_Coefficients.size(); varNum++) {
      double coefficient = Math.abs(coefficients[varNum]);
      double reg = m_Regularizer[varNum];
      penalty += coefficient * reg;
    }
    m_LASSOScore = penalty;
  }
  
  protected void runModifiedLARS() {
    
    if (m_Coefficients != null) { // It's already been run
      return;
    }
    
    int[] allRows = ArrayUtils.getMissingIndices(new int[0], m_InputValues.rows());

    DoubleMatrix1D coefficients = DoubleFactory1D.dense.make(m_InputValues.columns());
    DoubleMatrix1D predictedOutput = null;
    DoubleMatrix1D remainingOutput = m_OutputValues;
    int[] activeVariables = m_ZeroRegVars;
    if (m_ZeroRegVars.length > 0) {
      DoubleMatrix2D zeroInput = m_InputValues.viewSelection(allRows, m_ZeroRegVars).copy();
      LeastSquaresFitter lsFitter = new LeastSquaresFitter(zeroInput, m_OutputValues, null);
      double[] lsCoefficients = lsFitter.getCoefficients();
      for (int activeIndex =0; activeIndex < activeVariables.length; activeIndex++) {
        int varNum = activeVariables[activeIndex];
        coefficients.setQuick(varNum, lsCoefficients[activeIndex]);
      }
      predictedOutput = DoubleFactory1D.dense.make(lsFitter.getPredictedOutput());
      if (m_ZeroRegVars.length == m_InputValues.columns()) { // It's just a least-squres fit.
        m_Coefficients = coefficients;
        m_PredictedOutput = predictedOutput;
        m_ActiveVariables = m_ZeroRegVars;
        return;
      }
      remainingOutput = DoubleFactory1D.dense.make(m_OutputValues.size());
      for (int rowNum = 0; rowNum < predictedOutput.size(); rowNum++) {
        remainingOutput.setQuick(rowNum, m_OutputValues.getQuick(rowNum) - predictedOutput.getQuick(rowNum));
      }
    } else {
      predictedOutput = DoubleFactory1D.dense.make(m_OutputValues.size());
    }
    
    DoubleMatrix1D correlations = m_InputValues.zMult(remainingOutput, null, 1, 0, true);
    double maxCorrelation = -1;
    int firstVar = -1;
    for (int varNum = 0; varNum < correlations.size(); varNum++) {
      double reg = m_Regularizer[varNum];
      //if (Double.isInfinite(reg)) {continue;}
      if (ArrayUtils.arrayContains(m_ZeroRegVars, varNum)) {continue;}
      double absCorrelation = Math.abs(correlations.getQuick(varNum));
      absCorrelation /= reg;
      if (absCorrelation > maxCorrelation) {
        firstVar = varNum;
        maxCorrelation = absCorrelation;
      }
    }
    
    activeVariables = ArrayUtils.appendElement(activeVariables, firstVar);
    int[] inactiveVariables = ArrayUtils.getMissingIndices(activeVariables, m_InputValues.columns());
    DoubleMatrix2D xA = m_InputValues.viewSelection(allRows, activeVariables).copy();
    DoubleMatrix2D gA = xA.zMult(xA, null, 1, 0, true, false);
    MatrixInverter gAInverter = new MatrixInverter(gA);
    gAInverter.setNoSingularGuarantee(true);
    gAInverter.updateIncrementally(true);
    // LOOP HERE
    DoubleMatrix1D a = null;
    DoubleMatrix1D uA = null;
    while (activeVariables.length < m_InputValues.columns()) {
      
      //DoubleMatrix2D gAVerify = xA.zMult(xA, null, 1, 0, true, false);
      DoubleMatrix2D gAInverse = gAInverter.getInverse();
      double[] regArray = new double[activeVariables.length];
      for (int varIndex = 0; varIndex < activeVariables.length; varIndex++) {
        double correlation = correlations.getQuick(activeVariables[varIndex]);
        double sign = correlation < 0 ? -1 : 1;
        regArray[varIndex] = m_Regularizer[activeVariables[varIndex]] * sign;
      }
      DoubleMatrix1D activeReg = DoubleFactory1D.dense.make(regArray);
      DoubleMatrix1D wA = gAInverse.zMult(activeReg, null);
      double bigA = 1 / Math.sqrt(wA.zDotProduct(activeReg));
      for (int activeIndex = 0; activeIndex < wA.size(); activeIndex++) {
        wA.setQuick(activeIndex, wA.getQuick(activeIndex) * bigA);
      }
      uA = xA.zMult(wA, uA);
      a = m_InputValues.zMult(uA, a, 1, 0, true);
      
      // Find what the breaking point should be
      double gamma = Double.POSITIVE_INFINITY;
      int nextIndex = -1;
      //int nextSign = -1;
      for (int inactiveIndex = 0; inactiveIndex < inactiveVariables.length; inactiveIndex++) {
        int varNum = inactiveVariables[inactiveIndex];
        double cj = correlations.getQuick(varNum);
        double reg = m_Regularizer[varNum];
        if (Double.isInfinite(reg)) {continue;}
        double aj = a.getQuick(varNum);
        cj /= reg;
        aj /= reg;
        double gamCandidate1 = (maxCorrelation - cj) / (bigA - aj);
        double gamCandidate2 = (maxCorrelation + cj) / (bigA + aj);
        if (gamCandidate1 <= 0 && gamCandidate2 <= 0) {continue;}
        double gamCandidate;
        if (gamCandidate1 <= 0) {
          gamCandidate = gamCandidate2;
        } else if (gamCandidate2 <= 0) {
          gamCandidate = gamCandidate1;
        } else {
          gamCandidate = Math.min(gamCandidate1, gamCandidate2);
        }
        
        if (gamCandidate < gamma) {
          gamma = gamCandidate;
          nextIndex = inactiveIndex;
          //nextSign = ((cj - gamCandidate * aj) < 0) ? -1 : 1;
        }
      }
      
      // Handle the case in which we can't use any of the other variables
      if (nextIndex < 0) {
        m_Coefficients = coefficients;
        m_PredictedOutput = predictedOutput;
        m_ActiveVariables= activeVariables;
        //System.out.println("Lasso Score: " + this.getLASSOScore());
        return;
      }
      
      // Find the point at which we should break for lasso
      double gammaLasso = Double.POSITIVE_INFINITY;
      int lassoIndex = -1;
      for (int activeIndex = 0; activeIndex < activeVariables.length; activeIndex++) {
        int varNum = activeVariables[activeIndex];
        double reg = m_Regularizer[varNum];
        if (Double.isInfinite(reg)) {continue;}
        double dj = wA.getQuick(activeIndex);
        /*double sign = correlations.getQuick(varNum) < 0 ? -1 : 1;
        dj *= sign;*/
        dj *= reg;
        double betaj = coefficients.getQuick(varNum);
        double gammaj = -betaj / dj;
        if (gammaj <= 0) {continue;}
        if (gammaj < gammaLasso) {
          gammaLasso = gammaj;
          lassoIndex = activeIndex;
        }
      }
      
      /**
       * Handle the case where the minimum occurs before the active set changes
       */
      double gammaMin = Math.max((maxCorrelation  - 1) / bigA, 0);
      if (gammaMin < gammaLasso && gammaMin < gamma) {
        //gammaMin += 5E-4;
        this.incrementCoefficients(coefficients, wA, activeVariables, gammaMin);
        this.incrementPredictedValues(predictedOutput, uA, gammaMin);
        //DoubleMatrix1D recalcPredOutput = m_InputValues.zMult(coefficients, null, 1, 0, false);
        m_Coefficients = coefficients;
        m_PredictedOutput = predictedOutput;
        m_ActiveVariables= activeVariables;
        //System.out.println("Lasso Score: " + this.getLASSOScore());
        return;
      }
      
      /**
       * Handle the case where the lasso sign change occurs before the active set increases
       */
      if (gammaLasso < gamma) {
        this.incrementCoefficients(coefficients, wA, activeVariables, gammaLasso);
        this.incrementPredictedValues(predictedOutput, uA, gammaLasso);
        this.incrementCorrelations(correlations, a, gammaLasso);
        int varNum = activeVariables[lassoIndex];
        activeVariables = ArrayUtils.removeElement(activeVariables, lassoIndex);
        inactiveVariables = ArrayUtils.appendElement(inactiveVariables, varNum);
        int[] removedIndex = new int[] {lassoIndex};
        int[] remainingIndices = ArrayUtils.getMissingIndices(removedIndex, xA.columns());
        xA = xA.viewSelection(allRows, remainingIndices);
        gAInverter.shrinkMatrix(removedIndex, removedIndex);
        maxCorrelation -= gammaLasso * bigA;
      } else { // A Normal lasso update
        this.incrementCoefficients(coefficients, wA, activeVariables, gamma);
        this.incrementPredictedValues(predictedOutput, uA, gamma);
        this.incrementCorrelations(correlations, a, gamma);
        //DoubleMatrix1D recalcPredOutput = m_InputValues.zMult(coefficients, null, 1, 0, false);
        int varNum = inactiveVariables[nextIndex];
        activeVariables = ArrayUtils.appendElement(activeVariables, varNum);
        inactiveVariables = ArrayUtils.removeElement(inactiveVariables, nextIndex);
        DoubleMatrix2D nextInput = m_InputValues.viewSelection(allRows, new int[] {varNum}).copy();
        /*if (nextSign < 0) { 
          for (int rowNum = 0; rowNum < nextInput.rows(); rowNum++) {
            nextInput.setQuick(rowNum, 0, nextInput.getQuick(rowNum, 0) * -1);
          }
        }*/
        DoubleMatrix2D newU = xA.zMult(nextInput, null, 1, 0, true, false);
        DoubleMatrix2D newD = nextInput.zMult(nextInput, null, 1, 0, true, false);
        xA = DoubleFactory2D.dense.appendColumns(xA, nextInput);
        gAInverter.growMatrix(newU, newU.viewDice().copy(), newD);
        maxCorrelation -= gamma * bigA;
      }
    }
    
    m_Coefficients = coefficients;
    m_PredictedOutput = predictedOutput;
    m_ActiveVariables= activeVariables;
  }
  
  protected void incrementCorrelations(DoubleMatrix1D correlations, DoubleMatrix1D a, double gamma) {
    for (int rowNum = 0; rowNum < correlations.size(); rowNum++) {
      double oldValue = correlations.getQuick(rowNum);
      double newValue = oldValue - a.getQuick(rowNum) * gamma;
      correlations.setQuick(rowNum, newValue);
    }
  }
  
  protected void incrementPredictedValues(DoubleMatrix1D predictedOutput, DoubleMatrix1D uA, double gamma) {
    for (int sampleNum = 0; sampleNum < predictedOutput.size(); sampleNum++) {
      double oldValue = predictedOutput.getQuick(sampleNum);
      double newValue = oldValue + uA.getQuick(sampleNum) * gamma;
      predictedOutput.setQuick(sampleNum, newValue);
    }
  }
  
  protected void incrementCoefficients(DoubleMatrix1D coefficients, DoubleMatrix1D wA, int[] activeVariables, double gamma) {
    for (int activeIndex = 0; activeIndex < activeVariables.length; activeIndex++) {
      int varNum = activeVariables[activeIndex];
      double oldCoefficient = coefficients.getQuick(varNum);
      //double sign = correlations.getQuick(varNum) < 0 ? -1 : 1;
      double newCoefficient = oldCoefficient + wA.getQuick(activeIndex) * gamma; // * sign;
      coefficients.setQuick(varNum, newCoefficient);
    }
  }

  public double[] getRegularizer() {
    return ArrayUtils.copyArray(m_Regularizer);
  }
  
  protected double getNonZeroAverage(double[] regularizer) {
    double sum = 0;
    int numNonZero = 0;
    for (int varNum = 0; varNum < regularizer.length; varNum++) {
      double value = regularizer[varNum];
      if (value == 0) {continue;}
      sum += value;
      numNonZero++;
    }
    return sum / numNonZero;
  }

  public IRegularizedFitter setRegularizer(double[] regularizer) {
    return this.getFitter(m_InputValues, m_OutputValues, m_Weights, regularizer);
  }
  
  public IRegularizedFitter setRegularizer(IRegGenerator generator, double[] parameters) {
    
    ILinearRegGenerator linearGenerator = (ILinearRegGenerator) generator;
    double[] reg = linearGenerator.getRegularizer(parameters, null);
    return this.setRegularizer(reg);
    
  }

  public IRegularizedFitter setRegularizer(int varNum, double newReg) {
    
    double[] regularizer = ArrayUtils.copyArray(m_Regularizer);
    regularizer[varNum] = newReg;
    return this.getFitter(m_InputValues, m_OutputValues, m_Weights, regularizer);
  }

  public double[] getPredictionErrors(boolean useWeights) {
    double[] returnArray = new double[this.numSamples()];

    for (int rowNum = 0; rowNum < this.numSamples(); rowNum++) {
      double weightRoot = useWeights ? 1 : Math.sqrt(this.getWeight(rowNum));
      returnArray[rowNum] = (m_OutputValues.getQuick(rowNum) - this.getPredictedOutput(rowNum)) / weightRoot;
    }
    return returnArray;
  }
  
}
