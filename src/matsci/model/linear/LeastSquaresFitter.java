package matsci.model.linear;

import matsci.model.IFitter;
import matsci.util.arrays.ArrayUtils;
import matsci.util.arrays.MatrixInverter;
import cern.colt.function.DoubleDoubleFunction;
import cern.colt.matrix.DoubleFactory1D;
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import cern.jet.math.PlusMult;
import java.util.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class LeastSquaresFitter implements ILinearFitter {

  // TODO replace this with an enum someday.
  protected int m_ScoringMethod = LKO_CV_SCORE;
  protected int m_LKO_KValue = 1;
  protected static final int LKO_CV_SCORE = 1;
  protected static final int RMS_SCORE = 2;
  protected static final int UNWEIGHTED_LOO_CV_SCORE = 3;
  protected static final int GENERALIZED_CV_SCORE = 4;
  protected static final int MEAN_ABSOLUTE_ERROR = 5;
  
  protected boolean m_AllowIncrementalUpdate = false;
  protected boolean m_NoSingularGuarantee = false;
  
  // I'm fairly sure this is not the recommended practice for exceptions, but it's internal and faster
  protected final static SingularInputException SINGULAR_INPUT_EXCEPTION = new SingularInputException();
  
  protected final DoubleMatrix2D m_InputValues;
  protected final DoubleMatrix1D m_OutputValues;
  protected final DoubleMatrix1D m_Weights;

  // This is (XtX)^-1 or equivalent.
  protected MatrixInverter m_CoreMatrix = null;  
  protected DoubleMatrix2D m_CovBeta = null;
  protected DoubleMatrix1D m_XOutput = null;
  protected DoubleMatrix1D m_Coefficients = null;
  protected DoubleMatrix1D m_PredictedValues = null;

  // Initializing to -1 tells us that we haven't calculated this yet
  //protected double m_LOO_CV_Score = -1;
  //protected double m_Unweighted_LOO_CV_Score = -1;
  protected double m_RMS = -1;
  protected double m_Unweighted_RMS = -1;
  protected double m_ExpectedSigmaSquared = -1;
  protected double[] m_LKO_CV_Scores = new double[0];
  protected double[] m_Unweighted_LKO_CV_Scores = new double[0];
  protected double m_GCV_Score = -1;
  protected double m_MeanAbsoluteError = -1;
  
  protected LeastSquaresFitter(DoubleMatrix2D inputValues, DoubleMatrix1D outputValues, DoubleMatrix1D weights) {
    m_InputValues = inputValues;
    m_OutputValues = outputValues;
    m_Weights = weights;
  }
  
  public LeastSquaresFitter(int numVars) {
    this (DoubleFactory2D.dense.make(0, numVars), DoubleFactory1D.dense.make(0), DoubleFactory1D.dense.make(0));
  }

  public LeastSquaresFitter() {
    this(new double[0][0], new double[0]);
  }
  
  public LeastSquaresFitter(double[][] inputValues, double[] outputValues) {
    this(DoubleFactory2D.dense.make(inputValues), DoubleFactory1D.dense.make(outputValues), null);
  }
    
  public LeastSquaresFitter(double[][] inputValues, double[] outputValues, double[] weights) {
    this(DoubleFactory2D.dense.make(inputValues), DoubleFactory1D.dense.make(outputValues), DoubleFactory1D.dense.make(weights));
    this.weightMatrix(m_InputValues, m_Weights);
    this.weightMatrix(m_OutputValues, m_Weights);
  }
  
  /*
  public LeastSquaresFitter(LeastSquaresFitter sourceFitter, double[][] inputValues, double[] outputValues) {
    this(sourceFitter, DoubleFactory2D.dense.make(inputValues), DoubleFactory1D.dense.make(outputValues), null);
  }
  
  public LeastSquaresFitter(LeastSquaresFitter sourceFitter, double[][] inputValues, double[] outputValues, double[] weights) {
    this(sourceFitter, DoubleFactory2D.dense.make(inputValues), DoubleFactory1D.dense.make(outputValues), DoubleFactory1D.dense.make(weights));
    this.weightMatrix(m_InputValues, m_Weights);
    this.weightMatrix(m_OutputValues, m_Weights);
  }*/
  
  /**
   * Use this method so that classes that extend this one are able to return classes of their type.
   * @param sourceFitter
   * @return
   */
  protected LeastSquaresFitter getFitter(DoubleMatrix2D inputValues, DoubleMatrix1D outputValues, DoubleMatrix1D weights) {
    LeastSquaresFitter returnFitter = new LeastSquaresFitter(inputValues, outputValues, weights);
    this.copySettingsTo(returnFitter);
    return returnFitter;
  }
  
  public void copySettingsTo(LeastSquaresFitter fitter) {
    fitter.m_AllowIncrementalUpdate = m_AllowIncrementalUpdate;
    fitter.m_NoSingularGuarantee = m_NoSingularGuarantee;
    fitter.m_ScoringMethod = m_ScoringMethod;
    fitter.m_LKO_KValue = m_LKO_KValue;
  }
  
  protected void weightMatrix(DoubleMatrix2D matrix, DoubleMatrix1D weights) {
    if (weights == null) {return;}
    if (matrix.rows() != weights.size()) {
      throw new RuntimeException("Number of rows in matrix differs from number of weights");
    }
    for (int rowNum = 0; rowNum < matrix.rows(); rowNum++) {
      double weightRoot = Math.sqrt(weights.getQuick(rowNum));
      for (int colNum = 0; colNum < matrix.columns(); colNum++) {
        double oldValue = matrix.getQuick(rowNum, colNum);
        matrix.setQuick(rowNum, colNum, oldValue * weightRoot);
      }
    }
  }
  
  protected void weightMatrix(DoubleMatrix1D vector, DoubleMatrix1D weights) {
    if (weights == null) {return;}
    if (vector.size() != weights.size()) {
      throw new RuntimeException("Number of rows in vector differs from number of weights");
    }
    for (int rowNum = 0; rowNum < vector.size(); rowNum++) {
      double weightRoot = Math.sqrt(weights.getQuick(rowNum));
      double oldValue = vector.getQuick(rowNum);
      vector.setQuick(rowNum, oldValue * weightRoot);
    }
  }
  
  public void allowIncrementalUpdates(boolean value) {
    m_AllowIncrementalUpdate = value;
    if (m_CoreMatrix != null) {
      m_CoreMatrix.updateIncrementally(value);
    }
  }
  
  public boolean allowsIncrementalUpdates() {
    return m_AllowIncrementalUpdate;
  }
  
  public void setNoSingularGuarantee(boolean value) {
    m_NoSingularGuarantee = value;
    if (m_CoreMatrix != null) {
      m_CoreMatrix.setNoSingularGuarantee(value);
    }
  }
  
  public boolean getNoSingularGuarantee() {
    return m_NoSingularGuarantee;
  }
  
  public int numVariables() {
    return m_InputValues.columns();
  }
  
  public int numSamples() {
    return m_InputValues.rows();
  }
  
  public double getWeight(int rowNum) {
    return (m_Weights == null) ? 1 : m_Weights.getQuick(rowNum);
  }
  
  public double getInputValue(int rowNum, int colNum) {
    double weightRoot = (m_Weights == null) ? 1 : Math.sqrt(m_Weights.getQuick(rowNum));
    if (weightRoot == 0) {
      System.out.println("Zero weight");
    }
    return m_InputValues.getQuick(rowNum, colNum) / weightRoot;
  }
  
  public double[][] getInputValues() {
    double[][] returnArray = new double[this.numSamples()][this.numVariables()];
    for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
      double[] row = returnArray[rowNum];
      for (int colNum = 0; colNum < row.length; colNum++) {
        row[colNum] = this.getInputValue(rowNum, colNum);
      }
    }
    return returnArray;
  }

  public double getOutputValue(int rowNum) {
    double weightRoot = (m_Weights == null) ? 1 : Math.sqrt(m_Weights.getQuick(rowNum));
    return m_OutputValues.getQuick(rowNum) / weightRoot;
  }
  
  public double getRSquared() {
    
    return 1 - this.getRMSError() * this.getRMSError() / this.getOutputVariance();
  }
  
  public double getOutputVariance() {
    int numSamples = this.numSamples();
    double sum = 0;
    double sumSq = 0;
    for (int sampleNum = 0; sampleNum < m_OutputValues.size(); sampleNum++) {
      double value = m_OutputValues.getQuick(sampleNum);
      sum += value;
      sumSq += value * value;
    }
    double avg = sum / numSamples;
    double sqAvgX = (avg * avg);
    double avgXSq = sumSq / numSamples;
    return avgXSq - sqAvgX;
  }
  
  public double getOutputStdDev() {
    return Math.sqrt(this.getOutputVariance());
  }
  
  public double getUnweightedOutputStdDev() {
    int numSamples = this.numSamples();
    double sum = 0;
    double sumSq = 0;
    for (int sampleNum = 0; sampleNum < m_OutputValues.size(); sampleNum++) {
      double value = this.getOutputValue(sampleNum);
      sum += value;
      sumSq += value * value;
    }
    double avg = sum / numSamples;
    double sqAvgX = (avg * avg);
    double avgXSq = sumSq / numSamples;
    return Math.sqrt(avgXSq - sqAvgX);
  }
  
  public double[] getWeights() {
    if (m_Weights != null) {
      return m_Weights.toArray();
    } else {
      double[] returnArray = new double[this.numSamples()];
      Arrays.fill(returnArray, 1);
      return returnArray;
    }
  }
  
  public double getPredictedOutput(int rowNum) {
    if (m_PredictedValues == null) {
      try {
        this.calculatePredictedValues();
      } catch (SingularInputException e) {
        return Double.NaN;
      }
    }
    
    double weight = this.getWeight(rowNum);
    return m_PredictedValues.getQuick(rowNum) / Math.sqrt(weight);
  }
  
  public double[] getPredictedOutput() {
    if (m_PredictedValues == null) {
      try {
        this.calculatePredictedValues();
      } catch (SingularInputException e) {
        return null;
      }
    }
    
    double[] returnArray = new double[m_PredictedValues.size()];
    for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
      returnArray[rowNum] = this.getPredictedOutput(rowNum);
    }
    return returnArray;
  }

  protected void calculateCoreMatrix() throws SingularInputException {
    if (m_CoreMatrix == null) {
      m_CoreMatrix = new MatrixInverter(m_InputValues.zMult(m_InputValues, null, 1, 0, true, false));
      m_CoreMatrix.updateIncrementally(m_AllowIncrementalUpdate);
      m_CoreMatrix.setNoSingularGuarantee(m_NoSingularGuarantee);
    }
    if (m_CoreMatrix.isSingular()) {
      throw SINGULAR_INPUT_EXCEPTION;
    }
  }

  protected void calculateXOutput() {
    if (m_XOutput == null) {
      m_XOutput = m_InputValues.zMult(m_OutputValues, null, 1, 0, true);
    }
  }

  protected void calculateCoefficients() throws SingularInputException {
    if (m_Coefficients == null) {
      this.calculateCoreMatrix();
      this.calculateXOutput();
      m_Coefficients = m_CoreMatrix.getQuickInverse().zMult(m_XOutput, null);
    }
  }

  protected void calculatePredictedValues() throws SingularInputException {
    if (m_PredictedValues == null) {
      this.calculateCoefficients();
      m_PredictedValues = m_InputValues.zMult(m_Coefficients, null);
    }
  }
  
  protected void calculateCovBeta() throws SingularInputException {
    if (m_CovBeta == null) {
      this.calculateCoreMatrix();
      m_CovBeta = m_CoreMatrix.getQuickInverse();
    }
  }
  
  protected double getKnownLKOCVScore(int k) {
    if (k >= m_LKO_CV_Scores.length) {
      return -1;
    }
    return m_LKO_CV_Scores[k];
  }
  
  protected void setKnownLKOCVScore(int k, double value) {
    if (k >= m_LKO_CV_Scores.length) {
      double[] incrementArray = new double[k - m_LKO_CV_Scores.length + 1];
      Arrays.fill(incrementArray, -1);
      m_LKO_CV_Scores = ArrayUtils.appendArray(m_LKO_CV_Scores, incrementArray);
    }
    m_LKO_CV_Scores[k] = value;
  }
  
  protected double getKnownUnweightedLKOCVScore(int k) {
    if (k >= m_Unweighted_LKO_CV_Scores.length) {
      return -1;
    }
    return m_Unweighted_LKO_CV_Scores[k];
  }
  
  protected void setKnownUnweightedLKOCVScore(int k, double value) {
    if (k >= m_Unweighted_LKO_CV_Scores.length) {
      double[] incrementArray = new double[k - m_Unweighted_LKO_CV_Scores.length + 1];
      Arrays.fill(incrementArray, -1);
      m_Unweighted_LKO_CV_Scores = ArrayUtils.appendArray(m_Unweighted_LKO_CV_Scores, incrementArray);
    }
    m_Unweighted_LKO_CV_Scores[k] = value;
  }

  protected void calculateLOOCVScore() {

    if (this.getKnownLKOCVScore(1) >= 0) {return;}
    //this.calculateLKOCVScore(1);
    /*
    if (m_InputValues.rows() <= m_InputValues.columns()) { // The CV score is overdetermined
      m_LOO_CV_Score = Double.NaN;
      return;
    }*/
    
    try {
	    this.calculateCoreMatrix();
	    this.calculatePredictedValues();
    } catch (SingularInputException e) {
      this.setKnownLKOCVScore(1, Double.NaN);
      return;
    }
    
    DoubleMatrix2D denominatorMatrix = DoubleFactory2D.dense.make(1, 1);
    // Checks to see if the columns span the input space and the output is exactly the input
    boolean singular = false;
    double cvSquared = 0;
    double weightSum = 0;
    DoubleMatrix2D coreInverse = m_CoreMatrix.getQuickInverse();
    //System.out.println(m_CoreMatrix.verify());
    int numRows = m_InputValues.rows();
    for (int rowNum = 0; rowNum < numRows; rowNum++) {
      DoubleMatrix1D xRow = m_InputValues.viewRow(rowNum);
      double product = xRow.zDotProduct(coreInverse.zMult(xRow, null));
      double denominator = (1 - product);
      if (denominator <= 0 || denominator > 1) { // The denominator, I believe, should always be positive because the core should be positive definite
        singular = true;
        break;
      }
      denominatorMatrix.setQuick(0, 0, denominator);
      DoubleMatrix2D denomInverse = new MatrixInverter(denominatorMatrix).getQuickInverse();
      if (denomInverse == null) {singular = true; break;} // For consistency with LKO CV Score
      //if (Math.abs(denominator) < 1E-2) {singular = true; break;} // Arbitrary check, from SMW formula.
      double numerator = (m_OutputValues.getQuick(rowNum) - m_PredictedValues.getQuick(rowNum));
      //if (Math.abs(numerator) > 0.0001) {singular = false;} // Arbitrary check
      double ratio = numerator / denominator;
      //System.out.println(numerator + " / " + denominator + " = " + ratio);
      cvSquared += ratio * ratio;
      weightSum += this.getWeight(rowNum);
    }
    double score = singular ? this.getSlowLOOCVScore() : Math.sqrt(cvSquared / weightSum);
    //double slowScore = this.getSlowLOOCVScore();
    //m_LOO_CV_Score = this.getSlowCVScore();
    this.setKnownLKOCVScore(1, score);
  }
  
  protected void calculateLKOCVScore(int k) {

    if (this.getKnownLKOCVScore(k) >= 0) {return;}
    
    try {
        this.calculateCoreMatrix();
        this.calculatePredictedValues();
    } catch (SingularInputException e) {
      this.setKnownLKOCVScore(k, Double.NaN);
      return;
    }
    
    int[] kView = new int[k];
    int numSlices = m_InputValues.rows() / k;
    int[] columnView =new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }
    DoubleMatrix1D numerator = DoubleFactory1D.dense.make(new double[k]);
    // Checks to see if the columns span the input space and the output is exactly the input
    boolean singular = false;
    double cvSquared = 0;
    double weightSum = 0;
    DoubleMatrix2D inverse = m_CoreMatrix.getQuickInverse();
    for (int sliceNum = 0; sliceNum < numSlices; sliceNum++) {
      for (int index = 0; index < k; index++) {
        int rowNum = sliceNum * k + index;
        kView[index] = rowNum;
        double delta = m_OutputValues.getQuick(rowNum) - m_PredictedValues.getQuick(rowNum);
        weightSum += this.getWeight(rowNum);
        numerator.setQuick(index, delta);
      }
      DoubleMatrix2D xRows = m_InputValues.viewSelection(kView, columnView).copy();
      DoubleMatrix2D denominator = xRows.zMult(inverse.zMult(xRows, null, -1, 0, false, true), null);
      for (int rowNum = 0; rowNum < denominator.rows(); rowNum++) {
        denominator.setQuick(rowNum, rowNum, denominator.getQuick(rowNum, rowNum) + 1);
      }
      DoubleMatrix2D denomInverse = new MatrixInverter(denominator).getQuickInverse();
      if (denomInverse == null) {
        singular = true;
        break;
        //this.setKnownLKOCVScore(k, Double.NaN);
        //return;
      }
      /*if (Math.sqrt(numerator.zDotProduct(numerator) / numerator.size()) > 0.0001) { // At the very least, it should 
        singular = false;
      }  */
      DoubleMatrix1D diff = denomInverse.zMult(numerator, null);
      for (int rowNum = 0; rowNum < diff.size(); rowNum++) {
        double cvDelta = diff.getQuick(rowNum);
        double delta = numerator.getQuick(rowNum);
        if (cvDelta * delta < delta * delta) { // Prevents sign changes and unrealistically lower cv deltas
          singular = true; 
          break;
        }
      }
      
      cvSquared += diff.zDotProduct(diff);
    }
    //double score = singular ? this.getSlowLKOCVScore(k) : Math.sqrt(cvSquared / (k * numSlices));
    double score = singular ? this.getSlowLKOCVScore(k) : Math.sqrt(cvSquared / weightSum);
    //m_LOO_CV_Score = this.getSlowCVScore();
    this.setKnownLKOCVScore(k, score);
  }
  
  public double[] getPredictionErrors(boolean useWeights) {
    
    try {
      this.calculateCoreMatrix();
      this.calculatePredictedValues();
    } catch (SingularInputException e) {
      return null;
    }
    
    double[] returnArray = new double[this.numSamples()];

    for (int rowNum = 0; rowNum < this.numSamples(); rowNum++) {
      double weightRoot = useWeights ? 1 : Math.sqrt(this.getWeight(rowNum));
      returnArray[rowNum] = (m_OutputValues.getQuick(rowNum) - m_PredictedValues.getQuick(rowNum)) / weightRoot;
    }
    return returnArray;
  }
  
  public double[] getCVErrors(boolean useWeights) {

    /*if (m_InputValues.rows() <= m_InputValues.columns()) { // The CV score is overdetermined
     return null;
    }*/
    
    try {
        this.calculateCoreMatrix();
        this.calculatePredictedValues();
    } catch (SingularInputException e) {
      return null;
    }
    
    int numRows = m_InputValues.rows();
    double[] returnArray = new double[numRows];
    DoubleMatrix2D coreInverse = m_CoreMatrix.getQuickInverse();
    for (int rowNum = 0; rowNum < numRows; rowNum++) {
      DoubleMatrix1D xRow = m_InputValues.viewRow(rowNum);
      double product = xRow.zDotProduct(coreInverse.zMult(xRow, null));
      double denominator = (1 - product);
      if (Math.abs(denominator) < 1E-6) {// Arbitrary check
        return this.getSlowCVErrors();
      } 
      double numerator = (m_OutputValues.getQuick(rowNum) - m_PredictedValues.getQuick(rowNum));
      double ratio = numerator / denominator;
      double weightRoot = useWeights ? 1 : Math.sqrt(this.getWeight(rowNum));
      returnArray[rowNum]= ratio / weightRoot;
    }

    return returnArray;
  }
  
  public double[] getSlowCVErrors() {

    int[] cols = new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < cols.length; colNum++) {
      cols[colNum] = colNum;
    }

    int[] rows = new int[m_InputValues.rows() - 1];
    double[] returnArray = new double[m_InputValues.rows()];
    for (int xRowNum = 0; xRowNum < m_InputValues.rows(); xRowNum++) {
      int offSet = 0;
      for (int rowNum = 0; rowNum < rows.length; rowNum++) {
        if (rowNum == xRowNum) {offSet = 1;} 
        rows[rowNum] = rowNum + offSet;
      }
      DoubleMatrix2D tempInputs = m_InputValues.viewSelection(rows, cols).copy();
      DoubleMatrix1D tempOutputs = m_OutputValues.viewSelection(rows).copy();
      LeastSquaresFitter tempFitter = this.getFitter(tempInputs, tempOutputs, m_Weights);
      DoubleMatrix1D xRow = m_InputValues.viewRow(xRowNum);
      double delta = tempFitter.predictOutput(xRow) - m_OutputValues.get(xRowNum);
      returnArray[xRowNum] = delta;
    }
    return returnArray;
  }
  
  protected void calculateUnweightedLOOCVScore() {
    if (this.getKnownUnweightedLKOCVScore(1) >= 0) {return;}

    /*
    if (m_InputValues.rows() <= m_InputValues.columns()) { // The CV score is overdetermined
      m_Unweighted_LOO_CV_Score = Double.NaN;
      return;
    }*/
    
    try {
	    this.calculateCoreMatrix();
	    this.calculatePredictedValues();
    } catch (SingularInputException e) {
      this.setKnownUnweightedLKOCVScore(1, Double.NaN);
      return;
    }
    
    boolean singular = true;
    double cvSquared = 0;
    int numRows = m_InputValues.rows();
    DoubleMatrix2D coreInverse = m_CoreMatrix.getQuickInverse();
    for (int rowNum = 0; rowNum < numRows; rowNum++) {
      DoubleMatrix1D xRow = m_InputValues.viewRow(rowNum);
      double product = xRow.zDotProduct(coreInverse.zMult(xRow, null));
      double denominator = (1 - product);
      if (denominator <= 0 || denominator > 1) { // The denominator, I believe, should always be positive because the core should be positive definite
        singular = true;
        break;
      }
      double numerator = (m_OutputValues.getQuick(rowNum) - m_PredictedValues.getQuick(rowNum));
      if (Math.abs(numerator) > 0.000001) {singular = false;} // Arbitrary check
      double ratio = numerator / denominator;
      double weight = this.getWeight(rowNum);
      cvSquared += ratio * ratio / weight;
    }
    
    double score = singular ? this.getSlowUnweightedLOOCVScore() : Math.sqrt(cvSquared / numRows);
    //m_Unweighted_LOO_CV_Score = Math.sqrt(cvSquared / numRows);  
    this.setKnownUnweightedLKOCVScore(1, score);
  }
  
  protected double calculateSelectLOOCVScore(boolean[] isSampleRelevant, boolean useWeights) {

    if (m_InputValues.rows() <= m_InputValues.columns()) { // The CV score is overdetermined
      return Double.NaN;
    }
    
    if (isSampleRelevant.length != this.numSamples()) {
      throw new RuntimeException("Array of sample relevance indicators must have same length as the number of total samples.");
    }
    
    try {
        this.calculateCoreMatrix();
        this.calculatePredictedValues();
    } catch (SingularInputException e) {
      return Double.NaN;
    }
    
    double cvSquared = 0;
    int numRows = m_InputValues.rows();
    DoubleMatrix2D coreInverse = m_CoreMatrix.getQuickInverse();
    int numKeepers = 0;
    for (int rowNum = 0; rowNum < numRows; rowNum++) {
      if (!isSampleRelevant[rowNum]) {continue;}
      numKeepers++;
      DoubleMatrix1D xRow = m_InputValues.viewRow(rowNum);
      double product = xRow.zDotProduct(coreInverse.zMult(xRow, null));
      double denominator = (1 - product);
      double numerator = (m_OutputValues.getQuick(rowNum) - m_PredictedValues.getQuick(rowNum));
      double ratio = numerator / denominator;
      double weight = useWeights ? 1 : this.getWeight(rowNum);
      cvSquared += ratio * ratio / weight;
    }
    return Math.sqrt(cvSquared / numKeepers);  
  }
  
  protected void calculateUnweightedRMS() {
    if (m_Unweighted_RMS >= 0) {return;}
    
    try {
      this.calculatePredictedValues();
    } catch (SingularInputException e) {
      m_Unweighted_RMS = Double.NaN;
      return;
    }
    
    m_Unweighted_RMS = 0;
    for (int row = 0; row < m_PredictedValues.size(); row++) {
      double diff = m_PredictedValues.getQuick(row) - m_OutputValues.getQuick(row);
      m_Unweighted_RMS += diff * diff / this.getWeight(row);
    }
    m_Unweighted_RMS = Math.sqrt(m_Unweighted_RMS / m_PredictedValues.size());
  }
  
  protected void calculateRMS() {
    if (m_RMS >= 0) {return;}
    
    try {
      this.calculatePredictedValues();
    } catch (SingularInputException e) {
      m_RMS = Double.NaN;
      return;
    }
    
    m_RMS = 0;
    for (int row = 0; row < m_PredictedValues.size(); row++) {
      double diff = m_PredictedValues.getQuick(row) - m_OutputValues.getQuick(row);
      m_RMS += diff * diff;
    }
    m_RMS = Math.sqrt(m_RMS / m_PredictedValues.size());
  }

  public double[] getCoefficients() {
    
    try {
      this.calculateCoefficients();
    } catch (SingularInputException e) {
      return null;
    }
    
    return m_Coefficients.toArray();
  }
  
  public double getLKOCVScore(int k) {
    this.calculateLKOCVScore(k);
    return this.getKnownLKOCVScore(k);
  }

  public double getLOOCVScore() {
    //return this.getSlowLOOCVScore();
    this.calculateLOOCVScore();
    //double slowLOOCVScore = this.getSlowLOOCVScore();
    double looCVScore = this.getKnownLKOCVScore(1);
    return looCVScore;
  }
  
  public double getUnweightedLOOCVScore() {
    this.calculateUnweightedLOOCVScore();
    return this.getKnownUnweightedLKOCVScore(1);
  }

  public double getSlowLOOCVScore() {

    int[] cols = new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < cols.length; colNum++) {
      cols[colNum] = colNum;
    }

    int[] rows = new int[m_InputValues.rows() - 1];
    double cvSq = 0;
    //double weightSum = 0;
    for (int xRowNum = 0; xRowNum < m_InputValues.rows(); xRowNum++) {
      int offSet = 0;
      for (int rowNum = 0; rowNum < rows.length; rowNum++) {
        if (rowNum == xRowNum) {offSet = 1;} 
        rows[rowNum] = rowNum + offSet;
      }
      DoubleMatrix2D tempInputs = m_InputValues.viewSelection(rows, cols).copy();
      DoubleMatrix1D tempOutputs = m_OutputValues.viewSelection(rows).copy();
      DoubleMatrix1D weights = null;
      if (m_Weights == null) {
        weights = DoubleFactory1D.dense.make(rows.length, 1);
      } else {
        weights = m_Weights.viewSelection(rows).copy();
      }
      LeastSquaresFitter tempFitter = this.getFitter(tempInputs, tempOutputs, weights);
      DoubleMatrix1D xRow = m_InputValues.viewRow(xRowNum);
      double delta = tempFitter.predictOutput(xRow) - m_OutputValues.get(xRowNum);
      cvSq += delta * delta;
      //weightSum += this.getWeight(xRowNum);
    }

    return Math.sqrt(cvSq / m_InputValues.rows());
    //return Math.sqrt(cvSq / weightSum);

  }
  

  public double getSlowUnweightedLOOCVScore() {

    int[] cols = new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < cols.length; colNum++) {
      cols[colNum] = colNum;
    }

    int[] rows = new int[m_InputValues.rows() - 1];
    double cvSq = 0;
    //double weightSum = 0;
    for (int xRowNum = 0; xRowNum < m_InputValues.rows(); xRowNum++) {
      int offSet = 0;
      for (int rowNum = 0; rowNum < rows.length; rowNum++) {
        if (rowNum == xRowNum) {offSet = 1;} 
        rows[rowNum] = rowNum + offSet;
      }
      DoubleMatrix2D tempInputs = m_InputValues.viewSelection(rows, cols).copy();
      DoubleMatrix1D tempOutputs = m_OutputValues.viewSelection(rows).copy();
      DoubleMatrix1D weights = null;
      if (m_Weights == null) {
        weights = DoubleFactory1D.dense.make(rows.length, 1);
      } else {
        weights = m_Weights.viewSelection(rows).copy();
      }
      LeastSquaresFitter tempFitter = this.getFitter(tempInputs, tempOutputs, weights);
      DoubleMatrix1D xRow = m_InputValues.viewRow(xRowNum);
      double delta = tempFitter.predictOutput(xRow) - m_OutputValues.get(xRowNum);
      double weight = this.getWeight(xRowNum);
      cvSq += delta * delta / weight;
      //weightSum += this.getWeight(xRowNum);
    }

    return Math.sqrt(cvSq / m_InputValues.rows());
    //return Math.sqrt(cvSq / weightSum);

  }

  public double getSlowLKOCVScore(int k) {
    
    int[] kView = new int[k];
    int[] nonKView = new int[m_InputValues.rows() - k];
    int numSlices = m_InputValues.rows() / k;
    int[] columnView =new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }
    
    double cvSquared = 0;
    for (int sliceNum = 0; sliceNum < numSlices; sliceNum++) {
      int kIndex = 0;
      int nonKIndex = 0;
      for (int rowNum = 0; rowNum < m_InputValues.rows(); rowNum++) {
        if (rowNum >= sliceNum * k && rowNum < (sliceNum + 1) * k) {
          kView[kIndex++] = rowNum;
        } else {
          nonKView[nonKIndex++] = rowNum;
        }
      }
      DoubleMatrix1D yTrain = m_OutputValues.viewSelection(nonKView).copy();
      DoubleMatrix2D xTrain = m_InputValues.viewSelection(nonKView, columnView).copy();
      DoubleMatrix1D weights = null;
      if (m_Weights == null) {
        weights = DoubleFactory1D.dense.make(nonKView.length, 1);
      } else {
        weights = m_Weights.viewSelection(nonKView).copy();
      }
      LeastSquaresFitter fitter = this.getFitter(xTrain, yTrain, weights);

      DoubleMatrix2D xTest = m_InputValues.viewSelection(kView, columnView).copy();
      DoubleMatrix1D yPred = fitter.predictOutput(xTest);
      if (yPred == null) {return Double.NaN;}
      DoubleMatrix1D yTest = m_OutputValues.viewSelection(kView).copy();
      
      for (int rowNum = 0; rowNum < yPred.size(); rowNum++) {
        double pred = yPred.getQuick(rowNum);
        double test = yTest.getQuick(rowNum);
        double delta = pred - test;
        cvSquared += delta * delta;
      }
      
    }

    return Math.sqrt(cvSquared / (k * numSlices));

  }
  
  public double getRMSError() {
    this.calculateRMS();
    return m_RMS; 
  }
  
  public double getUnweightedRMSError() {
    this.calculateUnweightedRMS();
    return m_Unweighted_RMS; 
  }
  
  public double getExpectedErrorVariance() {
    if (m_ExpectedSigmaSquared < 0) {
      m_ExpectedSigmaSquared = this.calculateExpectedErrorVariance();
    }
    return m_ExpectedSigmaSquared;
  }
  
  public double getGeneralizedCVScore() {
    if (m_GCV_Score < 0) {
      m_GCV_Score = this.calculateGCVScore();
    }
    return m_GCV_Score;
  }
  
  public double getMeanAbsoluteError() {
    if (m_MeanAbsoluteError < 0) {
      m_MeanAbsoluteError = this.calculateMeanAbsoluteError();
    }
    return m_MeanAbsoluteError;
  }
  
  protected double calculateMeanAbsoluteError() {
    double returnValue = 0;
    if (m_PredictedValues == null) {
      try {
        this.calculatePredictedValues();
      } catch (SingularInputException e) {
        return Double.NaN;
      }
    }
    for (int row = 0; row < m_PredictedValues.size(); row++) {
      double diff = m_PredictedValues.getQuick(row) - m_OutputValues.getQuick(row);
      returnValue += Math.abs(diff);
    }
    return returnValue / m_PredictedValues.size();
  }
  
  
  protected double calculateGCVScore() {
    double scaleFactor = ((double) m_InputValues.rows()) / (m_InputValues.rows() - m_InputValues.columns());
    if (scaleFactor < 0) {return Double.NaN;}
    return this.getRMSError() * scaleFactor;
  }
  
  protected double calculateExpectedErrorVariance() {
    double rms = this.getRMSError();
    double rss = rms * rms * m_InputValues.rows();
    return rss / (m_InputValues.rows() - m_InputValues.columns());
  }
  
  public double getWeightSum() {
    double returnValue = 0;
    for (int sampleNum = 0; sampleNum < this.numSamples(); sampleNum++) {
      returnValue += this.getWeight(sampleNum);
    }
    return returnValue;
  }
  
  public DoubleMatrix2D getCovBeta() {
    try {
      this.calculateCovBeta();
    } catch (SingularInputException e) {
      return null;
    }
    return m_CovBeta.copy();
  }
  
  public DoubleMatrix2D getCoreMatrix() {
    try {
      this.calculateCoreMatrix();
    } catch (SingularInputException e) {
      return null;
    }
    return m_CoreMatrix.getMatrix();
  }
  
  public DoubleMatrix2D getCoreInverse() {
    try {
      this.calculateCoreMatrix();
    } catch (SingularInputException e) {
      return null;
    }
    return m_CoreMatrix.getInverse();
  }

  /**
   * This needs to be multiplied by the estimated error (residual) variance before using in most cases.
   * 
   * @param inputValues
   * @return
   */
  public double getPredictionVariance(double[] inputValues) {
    DoubleMatrix2D covBeta = this.getCovBeta();
    if (covBeta == null) {return Double.POSITIVE_INFINITY;}
    DoubleMatrix1D inputRow = DoubleFactory1D.dense.make(inputValues);
    return covBeta.zMult(inputRow, null).zDotProduct(inputRow);
  }
  
  public double[] getPredictionVariance() {
    double[] returnArray = new double[m_InputValues.rows()];
    DoubleMatrix2D covBeta = this.getCovBeta();
    for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
      DoubleMatrix1D inputRow = m_InputValues.viewRow(rowNum).copy();
      returnArray[rowNum] = covBeta.zMult(inputRow, null).zDotProduct(inputRow);
    }
    return returnArray;
  }
  
  public double[] predictOutput(double[][] inputValues) {
    return this.predictOutput(DoubleFactory2D.dense.make(inputValues)).toArray();
  }
  
  public double predictOutput(double[] inputValues) {
    return this.predictOutput(DoubleFactory1D.dense.make(inputValues));
  }

  protected double predictOutput(DoubleMatrix1D inputValues) {
    
    try {
      this.calculateCoefficients();
    } catch (SingularInputException e) {
      return Double.NaN;
    }
    return (m_Coefficients.zDotProduct(inputValues));
  }
  
  protected DoubleMatrix1D predictOutput(DoubleMatrix2D inputValues) {
    
    try {
      this.calculateCoefficients();
    } catch (SingularInputException e) {
      return null;
    }
    return (inputValues.zMult(m_Coefficients, null));
  }
  
  public IFitter addSamples(double[][] inputValues, double[] outputValues, double[] weights) {
    
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(inputValues);
    DoubleMatrix1D newOutput = DoubleFactory1D.dense.make(outputValues);
    DoubleMatrix1D newWeights = DoubleFactory1D.dense.make(weights);
    
    this.weightMatrix(newInput, newWeights);
    this.weightMatrix(newOutput, newWeights);
    
    if (m_InputValues.columns() == 0) {
      return this.getFitter(newInput, newOutput, newWeights);
    }
    
    DoubleMatrix2D changedInput = (m_InputValues.rows() == 0) ? newInput : DoubleFactory2D.dense.appendRows(m_InputValues, newInput);
    DoubleMatrix1D changedOutput = (m_InputValues.rows() == 0) ? newOutput : DoubleFactory1D.dense.append(m_OutputValues, newOutput);

    DoubleMatrix1D oldWeights = m_Weights;
    if (oldWeights == null) {
      oldWeights = DoubleFactory1D.dense.make(m_OutputValues.size());
      oldWeights.assign(1);
    } 
    DoubleMatrix1D changedWeights = (m_InputValues.rows() == 0) ? newWeights : DoubleFactory1D.dense.append(oldWeights, newWeights);
    
    LeastSquaresFitter returnFitter = this.getFitter(changedInput, changedOutput, changedWeights);

    if (!m_AllowIncrementalUpdate) {return returnFitter;}
 
    if (m_CoreMatrix != null) {
      returnFitter.m_CoreMatrix = m_CoreMatrix.copy();
      returnFitter.m_CoreMatrix.changeMatrixSMW(newInput, newInput, 1);
    } 
    
    if (m_XOutput != null) {
      returnFitter.m_XOutput = newInput.zMult(newOutput, m_OutputValues.copy(), 1, 1, true);
    }
    
    return returnFitter;
  }
  
  public IFitter addSamples(double[][] inputValues, double[] outputValues) {
    
    if (inputValues.length != outputValues.length) {
      throw new RuntimeException("Number of input values to be added is not the same as the number of output values.");
    }
    
    DoubleMatrix1D changedWeights = null;
    if (m_Weights != null) {
      DoubleMatrix1D newWeights = DoubleFactory1D.dense.make(outputValues.length);
      newWeights.assign(1);
      changedWeights = DoubleFactory1D.dense.append(m_Weights, newWeights);
    }
 
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(inputValues);
    DoubleMatrix1D newOutput = DoubleFactory1D.dense.make(outputValues);
    
    DoubleMatrix2D changedInput =DoubleFactory2D.dense.appendRows(m_InputValues, newInput);
    DoubleMatrix1D changedOutput = DoubleFactory1D.dense.append(m_OutputValues, newOutput);

    LeastSquaresFitter returnFitter = this.getFitter(changedInput, changedOutput, changedWeights);

    if (!m_AllowIncrementalUpdate) {return returnFitter;}
 
    if (m_CoreMatrix != null) {
      returnFitter.m_CoreMatrix = m_CoreMatrix.copy();
      returnFitter.m_CoreMatrix.changeMatrixSMW(newInput, newInput, 1);
    } 
    
    if (m_XOutput != null) {
      returnFitter.m_XOutput = newInput.zMult(newOutput, m_OutputValues.copy(), 1, 1, true);
    }
    
    return returnFitter;
  }
  
  public IFitter clearSamples() {
    DoubleMatrix2D newInputValues = DoubleFactory2D.dense.make(0, m_InputValues.rows());
    DoubleMatrix1D newOuputValues =DoubleFactory1D.dense.make(0);
    DoubleMatrix1D newWeights = DoubleFactory1D.dense.make(0);
    return this.getFitter(newInputValues, newOuputValues, newWeights);
  }
  
  public IFitter removeSamples(int[] rowsToRemove) {

    int[] rowView = ArrayUtils.getMissingIndices(rowsToRemove, m_InputValues.rows());
    int[] columnView = ArrayUtils.getMissingIndices(new int[0], m_InputValues.columns());
    /*
    int[] rowView = new int[m_InputValues.rows() - rowsToRemove.length];
    int[] columnView = new int[m_InputValues.columns()];
    int viewIndex =0;
    for (int rowNum = 0; rowNum < m_InputValues.rows(); rowNum++) {
      if (ArrayUtils.arrayContains(rowsToRemove, rowNum)) {continue;}
      rowView[viewIndex++] = rowNum;
    }
    
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }*/
    
    DoubleMatrix2D changedInput = m_InputValues.viewSelection(rowView, columnView).copy();
    DoubleMatrix1D changedOutput =m_OutputValues.viewSelection(rowView).copy();
    DoubleMatrix1D changedWeights = null;
    if (m_Weights != null) {
      changedWeights = m_Weights.viewSelection(rowView).copy();
    }
    
    LeastSquaresFitter returnFitter = this.getFitter(changedInput, changedOutput, changedWeights);

    if (!m_AllowIncrementalUpdate) {return returnFitter;}
    
    if (m_CoreMatrix != null) {
      DoubleMatrix2D removedInput = m_InputValues.viewSelection(rowsToRemove, columnView);
      returnFitter.m_CoreMatrix = m_CoreMatrix.copy();
      returnFitter.m_CoreMatrix.changeMatrixSMW(removedInput, removedInput, -1);
    }
    
    if (m_XOutput != null) {
      DoubleMatrix2D removedInput = m_InputValues.viewSelection(rowsToRemove, columnView);
      DoubleMatrix1D removedOutput = m_OutputValues.viewSelection(rowsToRemove);
      returnFitter.m_XOutput = removedInput.zMult(removedOutput, m_XOutput.copy(), -1, 1, true);
    }
    
    return returnFitter;
  }
  
  public IFitter clearVariables() {
    DoubleMatrix2D changedInput = DoubleFactory2D.dense.make(new double[m_OutputValues.size()][0]);
    LeastSquaresFitter returnFitter = this.getFitter(changedInput, m_OutputValues, m_Weights); // don't have to copy m_OutputValues because this matrix should never change
    return returnFitter;
  }
  
  public IFitter removeVariables(int[] varsToRemove) {
    
    int[] columnView = ArrayUtils.getMissingIndices(varsToRemove, m_InputValues.columns());
    int[] rowView = ArrayUtils.getMissingIndices(new int[0], m_InputValues.rows());
    /*int[] columnView = new int[m_InputValues.columns() - varsToRemove.length];
    int viewIndex = 0;
    for (int colNum = 0; colNum < m_InputValues.columns(); colNum++) {
      if (ArrayUtils.arrayContains(varsToRemove, colNum)) {continue;}
      columnView[viewIndex++] = colNum;
    }

    int[] rowView = new int[m_InputValues.rows()];
    for (int rowNum = 0; rowNum < rowView.length; rowNum++) {
      rowView[rowNum] = rowNum;
    }*/
    
    // The copy here creates a DenseDoubleMatrix2D, which is faster than a view.
    DoubleMatrix2D changedInput = m_InputValues.viewSelection(rowView, columnView).copy();
    LeastSquaresFitter returnFitter = this.getFitter(changedInput, m_OutputValues, m_Weights);

    if (!m_AllowIncrementalUpdate) {return returnFitter;}
    
    if (m_CoreMatrix != null) {
      returnFitter.m_CoreMatrix = m_CoreMatrix.copy();
      returnFitter.m_CoreMatrix.shrinkMatrix(varsToRemove, varsToRemove);
    }
    
    if (m_XOutput != null) {
      returnFitter.m_XOutput = m_XOutput.viewSelection(columnView);
    }
    
    return returnFitter;
  }
  
  public IFitter addVariables(double[][] inputValues) {
    
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(inputValues);
    this.weightMatrix(newInput, m_Weights);
    
    DoubleMatrix2D changedInput;
    if (m_InputValues.rows() == 0) {
      changedInput = newInput;
    } else {
      changedInput = DoubleFactory2D.dense.appendColumns(m_InputValues, newInput);
    }
    
    LeastSquaresFitter returnFitter = this.getFitter(changedInput, m_OutputValues, m_Weights);

    if (!m_AllowIncrementalUpdate) {return returnFitter;}
    
    if (m_CoreMatrix != null) {
      returnFitter.m_CoreMatrix = m_CoreMatrix.copy();
      DoubleMatrix2D u = m_InputValues.zMult(newInput, null, 1, 0, true, false);
      DoubleMatrix2D v = u.viewDice();
      DoubleMatrix2D d = newInput.zMult(newInput, null, 1, 0, true, false);
      returnFitter.m_CoreMatrix.growMatrix(u, v, d);
    }
    
    if (m_XOutput != null) {
      DoubleMatrix1D newXOutput = newInput.zMult(m_OutputValues, null, 1, 0, true);
      returnFitter.m_XOutput = DoubleFactory1D.dense.append(m_XOutput, newXOutput);
    }
    
    return returnFitter;
  }
  
  public IFitter addVariables(double[][] inputValues, int[] indices) {
    
    LeastSquaresFitter returnFitter = (LeastSquaresFitter) this.addVariables(inputValues);
    Algebra linalg = new Algebra();
    int[] permutation = ArrayUtils.getInsertionPermutation(m_InputValues.columns(), indices);
    
    linalg.permuteColumns(returnFitter.m_InputValues, permutation, null);
    
    if (returnFitter.m_CoreMatrix != null) {
      returnFitter.m_CoreMatrix.permuteRows(permutation);
      returnFitter.m_CoreMatrix.permuteColumns(permutation);
    }
    
    return returnFitter;   
  }
  
  public LeastSquaresFitter setWeights(double[] weights) {
    
    if (m_Weights == null) {return this.scaleWeights(weights);}
    
    DoubleMatrix1D newWeights = DoubleFactory1D.dense.make(weights);
    DoubleMatrix2D newInputs = DoubleFactory2D.dense.make(m_InputValues.rows(), m_InputValues.columns());
    DoubleMatrix1D newOutputs = DoubleFactory1D.dense.make(m_OutputValues.size());
    for (int sampleNum = 0; sampleNum < this.numSamples(); sampleNum++) {
      double relativeWeightRoot = Math.sqrt(weights[sampleNum] / m_Weights.getQuick(sampleNum));
      double newOutputValue = m_OutputValues.getQuick(sampleNum) * relativeWeightRoot;
      newOutputs.setQuick(sampleNum, newOutputValue);
      for (int varNum = 0; varNum < newInputs.columns(); varNum++) {
        double newInputValue = m_InputValues.getQuick(sampleNum, varNum) * relativeWeightRoot;
        newInputs.setQuick(sampleNum, varNum, newInputValue);
      }
    }

    return this.getFitter(newInputs, newOutputs, newWeights);
  }
  
  public LeastSquaresFitter setOutputValues(double[] newValues) {
    
    DoubleMatrix1D newOutputs = DoubleFactory1D.dense.make(newValues);
    LeastSquaresFitter returnFitter = this.getFitter(m_InputValues, newOutputs, m_Weights);
    returnFitter.m_CovBeta = m_CovBeta;
    returnFitter.m_CoreMatrix = m_CoreMatrix;
    return returnFitter;
  }
  
  public LeastSquaresFitter scaleWeights(double[] scaleWeights) {
    
    DoubleMatrix1D newWeights = DoubleFactory1D.dense.make(scaleWeights);
    DoubleMatrix2D newInputs = m_InputValues.copy();
    DoubleMatrix1D newOutputs = m_OutputValues.copy();
    this.weightMatrix(newInputs, newWeights);
    this.weightMatrix(newOutputs, newWeights);

    if (m_Weights != null) {
      for (int sampleNum = 0; sampleNum < this.numSamples(); sampleNum++) {
        double newWeight = m_Weights.getQuick(sampleNum) * scaleWeights[sampleNum];
        newWeights.setQuick(sampleNum, newWeight);
      }
    }
    
    return this.getFitter(newInputs, newOutputs, newWeights);
  }
  
  public void useUnweightedLOOCVScore() {
    m_ScoringMethod = UNWEIGHTED_LOO_CV_SCORE;
  }
  
  public void useLOOCVScore() {
    m_ScoringMethod = LKO_CV_SCORE;
    m_LKO_KValue = 1;
  }
  
  public void useLKOCVScore(int k) {
    m_ScoringMethod = LKO_CV_SCORE;
    m_LKO_KValue = k;
  }
  
  public void useRMSScore() {
    m_ScoringMethod = RMS_SCORE;
  }
  
  public void useGCVScore() {
    m_ScoringMethod = GENERALIZED_CV_SCORE;
  }
  
  public void useMeanAbsoluteError() {
    m_ScoringMethod = MEAN_ABSOLUTE_ERROR;
  }
  
  public double scoreFit() {
    
    switch (m_ScoringMethod) {
	    case LKO_CV_SCORE:
          if (m_LKO_KValue == 1) {
	    	return this.getLOOCVScore();
          } else {
            return this.getLKOCVScore(m_LKO_KValue);
          }
	    case RMS_SCORE:
	      return this.getRMSError();
	    case UNWEIGHTED_LOO_CV_SCORE:
	      return this.getUnweightedLOOCVScore();
        case GENERALIZED_CV_SCORE:
          return this.getGeneralizedCVScore();
        case MEAN_ABSOLUTE_ERROR:
          return this.getMeanAbsoluteError();
    	default:
    	  throw new RuntimeException("Unknown scoring method.");
    }
  }
  
  public String getScoringMethod() {
	  switch (m_ScoringMethod) {
	    case LKO_CV_SCORE:
	    	return "LKO CV Score:  k=" + m_LKO_KValue;
	    case RMS_SCORE:
	      return "RMS";
        case UNWEIGHTED_LOO_CV_SCORE:
          return "Unweighted LOO CV Score";
        case GENERALIZED_CV_SCORE:
          return "Generalized CV Score";
        case MEAN_ABSOLUTE_ERROR:
          return "Mean absolute error";
	  	default:
	  	  return "Unknown scoring method.";
	  }
  }
  
  public String verifyInverse() {
    if (m_CoreMatrix == null) {return "null";}
    return m_CoreMatrix.verify();
  }
  
  protected static class SingularInputException extends Exception {}
  
}
