/*
 * Created on Jun 15, 2006
 *
 */
package matsci.model.linear;

import cern.colt.matrix.*;
import cern.colt.matrix.linalg.Algebra;

import java.util.Arrays;
import matsci.model.IFitter;
import matsci.model.linear.LeastSquaresFitter.SingularInputException;
import matsci.model.reg.ILinearRegGenerator;
import matsci.model.reg.IRegGenerator;
import matsci.model.reg.IRegularizedFitter;
import matsci.util.arrays.*;

public class LassoFitter implements ILinearFitter, IRegularizedFitter {

  // TODO replace this with an enum someday.
  protected int m_ScoringMethod = BIC;
  protected int m_LKO_KValue = 1;

  protected static final int LKO_CV_SCORE = 1;
  protected static final int RMS_SCORE = 2;
  protected static final int UNWEIGHTED_LKO_CV_SCORE = 3;
  protected static final int BIC = 4;
  protected static final int AIC = 5;
  protected static final int SCALED_AIC = 6;
  protected static final int SCALED_BIC = 7;
  
  protected final DoubleMatrix2D m_InputValues;
  protected final DoubleMatrix1D m_OutputValues;
  protected final double[] m_Weights;
  protected final double[] m_Regularizer;
  
  protected final int[] m_ZeroRegVars;
  protected final int[] m_NonZeroRegVars;
  protected final double m_Lambda;
  
  protected LeastSquaresFitter m_LSFitter;
  
  protected DoubleMatrix1D m_PredictedOutput;
  protected DoubleMatrix1D m_Coefficients;
  protected int[] m_ActiveVariables;
  
  protected double m_AIC = -1;
  protected double m_BIC = -1;
  protected double m_RMS = -1;
  protected double m_UnweightedRMS = -1;
  protected double m_LASSOScore = -1;
  protected double[] m_LKO_CV_Scores = new double[0];
  protected double[] m_Unweighted_LKO_CV_Scores = new double[0];
  
  public LassoFitter(double[][] inputValues, double[] outputValues) {
    this(inputValues, outputValues, oneWeights(outputValues.length), new double[inputValues[0].length]);
  }
  
  protected static double[] oneWeights(int numSamples) {
    double[] returnArray = new double[numSamples];
    Arrays.fill(returnArray, 1);
    return returnArray;
  }
  
  protected LassoFitter(DoubleMatrix2D scaledInput, DoubleMatrix1D scaledOutput, double[] weights, double[] regularizer) {
    m_InputValues = scaledInput;
    m_OutputValues = scaledOutput;
    m_Weights = ArrayUtils.copyArray(weights);
    m_Regularizer = ArrayUtils.copyArray(regularizer);
    
    int[] zeroRegVars = new int[0];
    int[] nonZeroRegVars = new int[0];
    double totalNonZeroReg = 0;
    for (int varNum = 0; varNum < regularizer.length; varNum++) {
      double varReg = regularizer[varNum];
      if (varReg == 0) {
        zeroRegVars = ArrayUtils.appendElement(zeroRegVars, varNum);
      } else {
        nonZeroRegVars = ArrayUtils.appendElement(nonZeroRegVars, varNum);
        totalNonZeroReg += varReg;
      }
    }
    m_ZeroRegVars = zeroRegVars;
    m_NonZeroRegVars = nonZeroRegVars;
    //m_Lambda = m_InputValues.rows() * totalNonZeroReg / (2 * m_NonZeroRegVars.length);
    m_Lambda =totalNonZeroReg / m_NonZeroRegVars.length;
    
    DoubleMatrix2D weightedInput = scaledInput.copy();
    this.unregInput(weightedInput, regularizer);
    m_LSFitter = new LeastSquaresFitter(weightedInput, scaledOutput, DoubleFactory1D.dense.make(weights));
    if (m_LSFitter.getCoreInverse() == null) {
      throw new RuntimeException("Input values must be linearly independent to use the Lasso Fitter");
    }
  }
  
  public LassoFitter(double[][] inputValues, double[] outputValues, double[] weights, double[] regularizer) {
    m_InputValues = DoubleFactory2D.dense.make(inputValues);
    m_OutputValues = DoubleFactory1D.dense.make(outputValues);
    m_Weights = ArrayUtils.copyArray(weights);
    m_Regularizer = ArrayUtils.copyArray(regularizer);
    m_LSFitter = new LeastSquaresFitter(inputValues, outputValues, weights);
    if (m_LSFitter.getCoreInverse() == null) {
      throw new RuntimeException("Input values must be linearly independent to use the Lasso Fitter");
    }
    
    int[] zeroRegVars = new int[0];
    int[] nonZeroRegVars = new int[0];
    double totalNonZeroReg = 0;
    for (int varNum = 0; varNum < regularizer.length; varNum++) {
      double varReg = regularizer[varNum];
      if (varReg == 0) {
        zeroRegVars = ArrayUtils.appendElement(zeroRegVars, varNum);
      } else {
        nonZeroRegVars = ArrayUtils.appendElement(nonZeroRegVars, varNum);
        totalNonZeroReg += varReg;
      }
    }
    m_ZeroRegVars = zeroRegVars;
    m_NonZeroRegVars = nonZeroRegVars;
    //m_Lambda = m_InputValues.rows() * totalNonZeroReg / (2 * m_NonZeroRegVars.length);
    m_Lambda =totalNonZeroReg / m_NonZeroRegVars.length;
    
    this.scaleInput(m_InputValues, weights, regularizer);
    this.scaleOutput(m_OutputValues, weights);
  }
  
  protected LassoFitter getFitter(DoubleMatrix2D inputValues, DoubleMatrix1D outputValues, double[] weights, double[] regularizer) {
    LassoFitter returnFitter = new LassoFitter(inputValues, outputValues, weights, regularizer);
    this.copySettingsTo(returnFitter);
    return returnFitter;
  }
  
  public void copySettingsTo(LassoFitter fitter) {
    fitter.m_ScoringMethod = m_ScoringMethod;
  }
  
  protected double regOverAvg(double value, double reg) {
    //double avgLambda = 2 * m_Lambda / m_InputValues.rows();
    double avgLambda = m_Lambda;
    return (reg == 0) ? value : value * reg / avgLambda;
  }
  
  protected double avgOverReg(double value, double reg) {
    //double avgLambda = 2 * m_Lambda / m_InputValues.rows();
    double avgLambda = m_Lambda;
    return (reg == 0) ? value : value * avgLambda / reg;
  }
  
  protected void unregInput(DoubleMatrix2D inputValues, double[] regularizer) {
    for (int rowNum = 0; rowNum < inputValues.rows(); rowNum++) {
      for (int colNum = 0; colNum < inputValues.columns(); colNum++) {
        double inputValue = inputValues.getQuick(rowNum, colNum);
        double reg = regularizer[colNum];
        inputValue = this.regOverAvg(inputValue, reg);
        inputValues.setQuick(rowNum, colNum, inputValue);
      }
    }
  }

  protected void scaleInput(DoubleMatrix2D inputValues, double[] weights, double[] regularizer) {
    
    for (int rowNum = 0; rowNum < inputValues.rows(); rowNum++) {
      double weightRoot = Math.sqrt(weights[rowNum]);
      for (int colNum = 0; colNum < inputValues.columns(); colNum++) {
        double inputValue = inputValues.getQuick(rowNum, colNum);
        double reg = regularizer[colNum];
        inputValue = this.avgOverReg(inputValue, reg);
        inputValues.setQuick(rowNum, colNum, inputValue * weightRoot);
      }
    }
    
  }

  protected void scaleOutput(DoubleMatrix1D outputValues, double[] weights) {
    for (int rowNum = 0; rowNum < outputValues.size(); rowNum++) {
      double weightRoot = Math.sqrt(weights[rowNum]);
      double outputValue = outputValues.getQuick(rowNum);
      outputValues.setQuick(rowNum, outputValue * weightRoot);
    }
  }
  
  public LeastSquaresFitter getLSFitter() {
    return m_LSFitter;
  }
  
  public double[] getCoefficients() {
    this.runModifiedLARS();
    double[] returnArray = m_Coefficients.toArray();
    for (int varNum = 0; varNum < returnArray.length; varNum++) {
      double reg = m_Regularizer[varNum];
      returnArray[varNum] = this.avgOverReg(returnArray[varNum], reg);
    }
    return returnArray;
  }

  public IFitter addSamples(double[][] inputValues, double[] outputValues, double[] weights) {
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(inputValues);
    DoubleMatrix1D newOutput = DoubleFactory1D.dense.make(outputValues);
    this.scaleInput(newInput, weights, m_Regularizer);
    this.scaleOutput(newOutput, weights);
    double[] allWeights = ArrayUtils.appendArray(m_Weights, weights);
    DoubleMatrix2D allInput = DoubleFactory2D.dense.appendRows(m_InputValues, newInput);
    DoubleMatrix1D allOutput = DoubleFactory1D.dense.append(m_OutputValues, newOutput);
    return this.getFitter(allInput, allOutput, allWeights, m_Regularizer);
  }

  public IFitter addSamples(double[][] inputValues, double[] outputValues) {
    double[] newWeights = new double[inputValues.length];
    Arrays.fill(newWeights, 1);
    return this.addSamples(inputValues, outputValues, newWeights);
  }

  public IFitter removeSamples(int[] rowsToRemove) {
    int[] rowView = ArrayUtils.getMissingIndices(rowsToRemove, m_InputValues.rows());
    int[] columnView = ArrayUtils.getMissingIndices(new int[0], m_InputValues.columns());
    
    DoubleMatrix2D newInput = m_InputValues.viewSelection(rowView, columnView).copy();
    DoubleMatrix1D newOutput =m_OutputValues.viewSelection(rowView).copy();
    double[] newWeights = new double[rowView.length];
    for (int newRowNum = 0; newRowNum < rowView.length; newRowNum++) {
      newWeights[newRowNum] = m_Weights[rowView[newRowNum]];
    }
    return this.getFitter(newInput, newOutput, newWeights, m_Regularizer);
  }

  public IFitter removeVariables(int[] varsToRemove) {
    int[] columnView = ArrayUtils.getMissingIndices(varsToRemove, m_InputValues.columns());
    int[] rowView = ArrayUtils.getMissingIndices(new int[0], m_InputValues.rows());

    DoubleMatrix2D newInput = m_InputValues.viewSelection(rowView, columnView).copy();
    double[] newRegularizer = new double[rowView.length];
    for (int newVarNum = 0; newVarNum < columnView.length; newVarNum++) {
      newRegularizer[newVarNum] = m_Regularizer[columnView[newVarNum]];
    }
    return this.getFitter(newInput, m_OutputValues, m_Weights, newRegularizer);
  }

  public IFitter addVariables(double[][] inputValues, int[] indices) {
    LassoFitter addedFitter = (LassoFitter) this.addVariables(inputValues);
    Algebra linalg = new Algebra();
    int[] permutation = ArrayUtils.getInsertionPermutation(m_InputValues.rows(), indices);
    
    DoubleMatrix2D newInput = linalg.permuteRows(addedFitter.m_InputValues, permutation, null);
    double[] oldRegularizer = ArrayUtils.copyArray(m_Regularizer);
    double[] newRegularizer = new double[addedFitter.m_Regularizer.length];
    for (int varIndex = 0; varIndex < permutation.length; varIndex++) {
      newRegularizer[varIndex] = oldRegularizer[permutation[varIndex]];
    }
    
    return this.getFitter(newInput, addedFitter.m_OutputValues, addedFitter.m_Weights, newRegularizer);
  }

  public IFitter addVariables(double[][] inputValues) {
    if (inputValues.length == 0) {
      return this.getFitter(this.m_InputValues, this.m_OutputValues, this.m_Weights, this.m_Regularizer);
    }
    double[] regularizer = new double[inputValues[0].length];
    return this.addVariables(inputValues, regularizer);
  }
  
  public IFitter addVariables(double[][] inputValues, double[] regularizer) {
    DoubleMatrix2D inputDelta = DoubleFactory2D.dense.make(inputValues);
    this.scaleInput(inputDelta, m_Weights, regularizer);
    double[] newRegularizer = ArrayUtils.appendArray(m_Regularizer, regularizer);
    DoubleMatrix2D newInput = DoubleFactory2D.dense.appendColumns(m_InputValues, inputDelta);
    return this.getFitter(newInput, m_OutputValues, m_Weights, newRegularizer); 
  }

  public int numVariables() {
    return m_InputValues.columns();
  }

  public int numSamples() {
    return m_InputValues.rows();
  }

  public double getInputValue(int sampleNum, int varNum) {
    double scaledInput = m_InputValues.get(sampleNum, varNum);
    double varReg = m_Regularizer[varNum];
    scaledInput = this.regOverAvg(scaledInput, varReg);
    double weightRoot = Math.sqrt(m_Weights[sampleNum]);
    return scaledInput / weightRoot;
  }
  
  public double[][] getInputValues() {
    double[][] returnArray = new double[m_InputValues.rows()][m_InputValues.columns()];
    for (int sampleNum = 0; sampleNum < m_InputValues.rows(); sampleNum++) {
      for (int varNum = 0; varNum < m_InputValues.columns(); varNum++) {
        returnArray[sampleNum][varNum] = this.getInputValue(sampleNum, varNum);
      }
    }
    return returnArray;
  }

  public double getOutputValue(int sampleNum) {
    double weightRoot = Math.sqrt(m_Weights[sampleNum]);
    return m_OutputValues.get(sampleNum) / weightRoot;
  }
  
  public double[] getOutputValues() {
    double[] returnArray = new double[m_OutputValues.size()];
    for (int sampleNum = 0; sampleNum < returnArray.length; sampleNum++) {
      returnArray[sampleNum] = this.getOutputValue(sampleNum);
    }
    return returnArray;
  }
  
  public double getPredictedOutput(int rowNum) {
    if (m_PredictedOutput == null) {
      this.runModifiedLARS();
    }
    
    double weight = this.getWeight(rowNum);
    return m_PredictedOutput.getQuick(rowNum) / Math.sqrt(weight);
  }

  public double getWeight(int sampleNum) {
    return m_Weights[sampleNum];
  }
  
  public double[] getWeights() {
    return ArrayUtils.copyArray(m_Weights);
  }

  public IFitter clearVariables() {
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(m_OutputValues.size(), 0);
    return this.getFitter(newInput, m_OutputValues, m_Weights, new double[0]);
  }

  public IFitter clearSamples() {
    DoubleMatrix2D newInput = DoubleFactory2D.dense.make(0, m_InputValues.columns());
    DoubleMatrix1D newOutput = DoubleFactory1D.dense.make(0);
    return this.getFitter(newInput, newOutput, new double[0], m_Regularizer);
  }

  public double scoreFit() {
    
    switch (m_ScoringMethod) {
        case AIC:
          return this.getAIC();
        case BIC:
          return this.getBIC();
        case SCALED_AIC:
          return this.getScaledAIC();
        case SCALED_BIC:
          return this.getScaledBIC();
        case RMS_SCORE:
          return this.getRMSError();
        case LKO_CV_SCORE:
          return this.getLKOCVScore(m_LKO_KValue);
        default:
          throw new RuntimeException("Unknown scoring method.");
    }
  }
  
  public void useScaledAIC() {
    m_ScoringMethod = SCALED_AIC;
  }
  
  public void useScaledBIC() {
    m_ScoringMethod = SCALED_BIC;
  }
  
  public void useAIC() {
    m_ScoringMethod = AIC;
  }
  
  public void useBIC() {
    m_ScoringMethod = BIC;
  }
  
  public void useLKOCVScore(int k) {
    m_ScoringMethod = LKO_CV_SCORE;
    m_LKO_KValue = k;
  }
  
  public void useRMS() {
    m_ScoringMethod = RMS_SCORE;
  }
  
  protected double[] verifyPredictedValues() {
    this.runModifiedLARS();
    double[] returnArray = new double[m_InputValues.rows()];
    double[] coefficients = this.getCoefficients();
    for (int varIndex = 0; varIndex < m_ActiveVariables.length; varIndex++) {
      int varNum = m_ActiveVariables[varIndex];
      double coefficient = coefficients[varNum];
      for (int sampleNum = 0; sampleNum < returnArray.length; sampleNum++) {
        returnArray[sampleNum] += coefficient * this.getInputValue(sampleNum, varNum);
      }
    }
    
    return returnArray;
  }
  
  public double getLKOCVScore(int k) {
    this.calculateSlowLKOCVScore(k);
    //System.out.println("LASSO Score: " + this.getLASSOScore() + ", L" + k + "O CV Score:" + this.getKnownLKOCVScore(k));
    return this.getKnownLKOCVScore(k);
  }
  
  public double getUnweightedRMSError() {
    if (m_UnweightedRMS < 0) {
      this.calculateUnweightedRMS();
    }
    return m_UnweightedRMS;
  }
  
  public double getRMSError() {
    if (m_RMS < 0) {
      this.calculateRMS();
    }
    return m_RMS;
  }
  
  public double getLASSOScore() {
    if (m_LASSOScore < 0) {
      this.calculateLASSOPenalty();
    }
    return m_LASSOScore;
  }
  
  public double getBIC() {
    if (m_BIC < 0) {
      this.calculateBIC();
    }
    return m_BIC;
  }
  
  public double getAIC() {
    if (m_AIC < 0) {
      this.calculateAIC();
    }
    return m_AIC;
  }
  
  public double getScaledBIC() {
    if (m_BIC < 0) {
      this.calculateBIC();
    }
    double sigSqEstimate = this.getLSFitter().getExpectedErrorVariance();
    double returnValue = m_BIC * sigSqEstimate;
    //System.out.println("Scaled BIC: " + returnValue);
    return returnValue;
  }
  
  public double getScaledAIC() {
    if (m_AIC < 0) {
      this.calculateAIC();
    }
    double sigSqEstimate = this.getLSFitter().getExpectedErrorVariance();
    double returnValue = m_AIC * sigSqEstimate;
    //System.out.println("Scaled AIC: " + returnValue);
    return returnValue;
  }
  
  protected double getKnownLKOCVScore(int k) {
    if (k >= m_LKO_CV_Scores.length) {
      return -1;
    }
    return m_LKO_CV_Scores[k];
  }
  
  protected void setKnownLKOCVScore(int k, double value) {
    if (k >= m_LKO_CV_Scores.length) {
      double[] incrementArray = new double[k - m_LKO_CV_Scores.length + 1];
      Arrays.fill(incrementArray, -1);
      m_LKO_CV_Scores = ArrayUtils.appendArray(m_LKO_CV_Scores, incrementArray);
    }
    m_LKO_CV_Scores[k] = value;
  }
  
  public double[] getCVErrors(boolean useWeights) {
    
    this.runModifiedLARS();
    
    int[] kView = new int[1];
    int[] nonKView = new int[m_InputValues.rows() - 1];
    int numSlices = m_InputValues.rows();
    int[] columnView =new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }
    
    double[] returnArray = new double[m_InputValues.rows()];
    double[] nonKWeights = new double[m_Weights.length - 1];
    for (int sliceNum = 0; sliceNum < numSlices; sliceNum++) {
      int kIndex = 0;
      int nonKIndex = 0;
      for (int rowNum = 0; rowNum < m_InputValues.rows(); rowNum++) {
        if (rowNum == sliceNum) {
          kView[kIndex++] = rowNum;
        } else {
          nonKWeights[nonKIndex] = m_Weights[rowNum];
          nonKView[nonKIndex++] = rowNum;
        }
      }
      DoubleMatrix1D yTrain = m_OutputValues.viewSelection(nonKView).copy();
      DoubleMatrix2D xTrain = m_InputValues.viewSelection(nonKView, columnView).copy();
      LassoFitter fitter = this.getFitter(xTrain, yTrain, nonKWeights, m_Regularizer);

      DoubleMatrix2D xTest = m_InputValues.viewSelection(kView, columnView).copy();
      DoubleMatrix1D yPred = fitter.predictOutput(xTest);
      //DoubleMatrix1D yTest = m_OutputValues.viewSelection(kView).copy();
      
      double pred = yPred.getQuick(0);
      double test = m_OutputValues.getQuick(sliceNum); //yTest.getQuick(0);
      double weightRoot = useWeights ? 1 : Math.sqrt(this.getWeight(sliceNum));
      returnArray[sliceNum] = (pred-test) / weightRoot;
    }
    return returnArray;

  }
  
  protected void calculateSlowLKOCVScore(int k) {
    
    if (this.getKnownLKOCVScore(k) >= 0) {return;}
    
    this.runModifiedLARS();
    
    int[] kView = new int[k];
    int[] nonKView = new int[m_InputValues.rows() - k];
    int numSlices = m_InputValues.rows() / k;
    int[] columnView =new int[m_InputValues.columns()];
    for (int colNum = 0; colNum < columnView.length; colNum++) {
      columnView[colNum] = colNum;
    }
    
    double cvSquared = 0;
    double[] nonKWeights = new double[m_Weights.length - k];
    for (int sliceNum = 0; sliceNum < numSlices; sliceNum++) {
      int kIndex = 0;
      int nonKIndex = 0;
      for (int rowNum = 0; rowNum < m_InputValues.rows(); rowNum++) {
        if (rowNum >= sliceNum * k && rowNum < (sliceNum + 1) * k) {
          kView[kIndex++] = rowNum;
        } else {
          nonKWeights[nonKIndex] = m_Weights[rowNum];
          nonKView[nonKIndex++] = rowNum;
        }
      }
      DoubleMatrix1D yTrain = m_OutputValues.viewSelection(nonKView).copy();
      DoubleMatrix2D xTrain = m_InputValues.viewSelection(nonKView, columnView).copy();
      LassoFitter fitter = this.getFitter(xTrain, yTrain, nonKWeights, m_Regularizer);

      DoubleMatrix2D xTest = m_InputValues.viewSelection(kView, columnView).copy();
      DoubleMatrix1D yPred = fitter.predictOutput(xTest);
      DoubleMatrix1D yTest = m_OutputValues.viewSelection(kView).copy();
      
      for (int rowNum = 0; rowNum < yPred.size(); rowNum++) {
        double pred = yPred.getQuick(rowNum);
        double test = yTest.getQuick(rowNum);
        double delta = pred - test;
        cvSquared += delta * delta;
      }
    }

    this.setKnownLKOCVScore(k, Math.sqrt(cvSquared / (k * numSlices)));

  }
  
  protected DoubleMatrix1D predictOutput(DoubleMatrix2D inputValues) {

    this.runModifiedLARS();
    return (inputValues.zMult(m_Coefficients, null));
  
  }
  
  protected void calculateBIC() {
    LeastSquaresFitter fitter = this.getLSFitter();
    double sigSqEstimate = fitter.getExpectedErrorVariance();
    double rms = this.getRMSError();
    double n = m_InputValues.rows();
    double degreesOfFreedom = m_ActiveVariables.length;
    m_BIC = (rms * rms / sigSqEstimate) + Math.log(n) * degreesOfFreedom / n;
    //System.out.println("RMS: " + rms + ", BIC: " + m_BIC + ", LASSO: " + this.getLASSOScore());
  }
  
  protected void calculateAIC() {
    LeastSquaresFitter fitter = this.getLSFitter();
    double sigSqEstimate = fitter.getExpectedErrorVariance();
    double rms = this.getRMSError();
    double n = m_InputValues.rows();
    double degreesOfFreedom = m_ActiveVariables.length;
    m_AIC = (rms * rms / sigSqEstimate) + 2 * degreesOfFreedom / n;
    //System.out.println("RMS: " + rms + ", AIC: " + m_AIC + ", LASSO: " + this.getLASSOScore());
  }
  
  protected void calculateUnweightedRMS() {
    this.runModifiedLARS();
    double totalDelta = 0;
    for (int rowNum = 0; rowNum < m_OutputValues.size(); rowNum++) {
      double delta = m_OutputValues.getQuick(rowNum) - m_PredictedOutput.getQuick(rowNum);
      totalDelta += delta * delta / this.getWeight(rowNum);
    }
    m_UnweightedRMS = Math.sqrt(totalDelta / m_OutputValues.size());
  }
  
  protected void calculateRMS() {
    this.runModifiedLARS();
    double totalDelta = 0;
    for (int rowNum = 0; rowNum < m_OutputValues.size(); rowNum++) {
      double delta = m_OutputValues.getQuick(rowNum) - m_PredictedOutput.getQuick(rowNum);
      totalDelta += delta * delta;
    }
    //double[] verifiedPrediction = this.verifyPredictedValues();
    m_RMS = Math.sqrt(totalDelta / m_OutputValues.size());
  }
  
  protected void calculateLASSOPenalty() {
    double penalty = this.getRMSError();
    penalty *= penalty;
    penalty *= m_OutputValues.size();
    penalty /= 2;
    double[] coefficients =this.getCoefficients();
    for (int varNum = 0; varNum < m_Coefficients.size(); varNum++) {
      double coefficient = Math.abs(coefficients[varNum]);
      double reg = m_Regularizer[varNum];
      penalty += coefficient * reg;
    }
    m_LASSOScore = penalty;
  }
  
  protected void runModifiedLARS() {
    
    if (m_Coefficients != null) { // It's already been run
      return;
    }
    
    if (m_ZeroRegVars.length > 0) {
      throw new RuntimeException("I don't know yet how to deal with zero-regularized variables.");
    }
    
    DoubleMatrix1D predictedOutput = DoubleFactory1D.dense.make(m_OutputValues.size());
    DoubleMatrix1D coefficients = DoubleFactory1D.dense.make(m_InputValues.columns());
    int[] allRows = ArrayUtils.getMissingIndices(new int[0], m_InputValues.rows());
    DoubleMatrix1D correlations = m_InputValues.zMult(m_OutputValues, null, 1, 0, true);
    double maxCorrelation = 0;
    int firstVar = -1;
    double sign = 1;
    for (int varNum = 0; varNum < correlations.size(); varNum++) {
      double absCorrelation = Math.abs(correlations.getQuick(varNum));
      if (absCorrelation > maxCorrelation) {
        firstVar = varNum;
        maxCorrelation = absCorrelation;
        sign = correlations.getQuick(varNum) / absCorrelation;
      }
    }
    
    /*if (sign < 0) {
      System.out.println("NEG!");
    }*/
    
    int[] activeVariables = new int[] {firstVar};
    int[] inactiveVariables = ArrayUtils.getMissingIndices(activeVariables, m_InputValues.columns());
    
    DoubleMatrix2D xA = m_InputValues.viewSelection(allRows, activeVariables).copy();
    for (int rowNum = 0; rowNum < m_InputValues.rows(); rowNum++) {
      xA.setQuick(rowNum, 0, xA.getQuick(rowNum, 0) * sign);
    }
    DoubleMatrix2D gA = xA.zMult(xA, null, 1, 0, true, false);
    MatrixInverter gAInverter = new MatrixInverter(gA);
    gAInverter.setNoSingularGuarantee(true);
    gAInverter.updateIncrementally(true);
    // LOOP HERE
    int numActiveVariables = 0;
    int numTotalVars = m_InputValues.columns();
    while (numActiveVariables < numTotalVars) {
      
      //DoubleMatrix2D gAVerify = xA.zMult(xA, null, 1, 0, true, false);
      DoubleMatrix2D gAInverse = gAInverter.getInverse();
      double[] onesArray = new double[gAInverse.columns()];
      Arrays.fill(onesArray, 1);
      DoubleMatrix1D ones = DoubleFactory1D.dense.make(onesArray);
      DoubleMatrix1D wA = gAInverse.zMult(ones, null);
      double bigA = 1 / Math.sqrt(wA.zDotProduct(ones));
      for (int activeIndex = 0; activeIndex < wA.size(); activeIndex++) {
        wA.setQuick(activeIndex, wA.getQuick(activeIndex) * bigA);
      }
      DoubleMatrix1D uA = xA.zMult(wA, null);
      DoubleMatrix1D a = m_InputValues.zMult(uA, null, 1, 0, true);
      
      // Find what the breaking point should be
      double gamma = Double.POSITIVE_INFINITY;
      int nextIndex = -1;
      int nextSign = -1;
      for (int inactiveIndex = 0; inactiveIndex < inactiveVariables.length; inactiveIndex++) {
        int varNum = inactiveVariables[inactiveIndex];
        double cj = correlations.getQuick(varNum);
        double aj = a.getQuick(varNum);
        double gamCandidate1 = (maxCorrelation - cj) / (bigA - aj);
        double gamCandidate2 = (maxCorrelation + cj) / (bigA + aj);
        if (gamCandidate1 <= 0 && gamCandidate2 <= 0) {continue;}
        double gamCandidate;
        if (gamCandidate1 <= 0) {
          gamCandidate = gamCandidate2;
        } else if (gamCandidate2 <= 0) {
          gamCandidate = gamCandidate1;
        } else {
          gamCandidate = Math.min(gamCandidate1, gamCandidate2);
        }
        
        if (gamCandidate < gamma) {
          gamma = gamCandidate;
          nextIndex = inactiveIndex;
          nextSign = ((cj - gamCandidate * aj) < 0) ? -1 : 1;
        }
      }
      
      // Find the point at which we should break for lasso
      double gammaLasso = Double.POSITIVE_INFINITY;
      int lassoIndex = -1;
      for (int activeIndex = 0; activeIndex < activeVariables.length; activeIndex++) {
        int varNum = activeVariables[activeIndex];
        double dj = correlations.getQuick(varNum) < 0 ? -1 * wA.getQuick(activeIndex) : wA.getQuick(activeIndex);
        double betaj = coefficients.getQuick(varNum);
        double gammaj = -betaj / dj;
        if (gammaj <= 0) {continue;}
        if (gammaj < gammaLasso) {
          gammaLasso = gammaj;
          lassoIndex = activeIndex;
        }
      }
      
      /**
       * Handle the case where the minimum occurs before the active set changes
       */
      double gammaMin = Math.max((maxCorrelation - m_Lambda) / bigA, 0);
      if (gammaMin < gammaLasso && gammaMin < gamma) {
        //gammaMin += 5E-4;
        this.incrementCoefficients(coefficients, wA, correlations,  activeVariables, gammaMin);
        this.incrementPredictedValues(predictedOutput, uA, gammaMin);
        //DoubleMatrix1D recalcPredOutput = m_InputValues.zMult(coefficients, null, 1, 0, false);
        m_Coefficients = coefficients;
        m_PredictedOutput = predictedOutput;
        m_ActiveVariables= activeVariables;
        //System.out.println("Orig lasso score: " + this.getLASSOScore());
        return;
      }
      
      /**
       * Handle the case where the lasso sign change occurs before the active set increases
       */
      if (gammaLasso < gamma) {
        this.incrementCoefficients(coefficients, wA, correlations, activeVariables, gammaLasso);
        this.incrementPredictedValues(predictedOutput, uA, gammaLasso);
        this.incrementCorrelations(correlations, a, gammaLasso);
        int varNum = activeVariables[lassoIndex];
        activeVariables = ArrayUtils.removeElement(activeVariables, lassoIndex);
        inactiveVariables = ArrayUtils.appendElement(inactiveVariables, varNum);
        int[] removedIndex = new int[] {lassoIndex};
        int[] remainingIndices = ArrayUtils.getMissingIndices(removedIndex, xA.columns());
        xA = xA.viewSelection(allRows, remainingIndices);
        gAInverter.shrinkMatrix(removedIndex, removedIndex);
        maxCorrelation -= gammaLasso * bigA;
      } else { // A Normal lasso update
        this.incrementCoefficients(coefficients, wA, correlations, activeVariables, gamma);
        this.incrementPredictedValues(predictedOutput, uA, gamma);
        this.incrementCorrelations(correlations, a, gamma);
        //DoubleMatrix1D recalcPredOutput = m_InputValues.zMult(coefficients, null, 1, 0, false);
        int varNum = inactiveVariables[nextIndex];
        activeVariables = ArrayUtils.appendElement(activeVariables, varNum);
        inactiveVariables = ArrayUtils.removeElement(inactiveVariables, nextIndex);
        DoubleMatrix2D nextInput = m_InputValues.viewSelection(allRows, new int[] {varNum}).copy();
        if (nextSign < 0) { 
          for (int rowNum = 0; rowNum < nextInput.rows(); rowNum++) {
            nextInput.setQuick(rowNum, 0, nextInput.getQuick(rowNum, 0) * -1);
          }
        }
        DoubleMatrix2D newU = xA.zMult(nextInput, null, 1, 0, true, false);
        DoubleMatrix2D newD = nextInput.zMult(nextInput, null, 1, 0, true, false);
        xA = DoubleFactory2D.dense.appendColumns(xA, nextInput);
        gAInverter.growMatrix(newU, newU.viewDice().copy(), newD);
        maxCorrelation -= gamma * bigA;
      }
      numActiveVariables = xA.columns();
    }
    
    m_Coefficients = coefficients;
    m_PredictedOutput = predictedOutput;
    m_ActiveVariables= activeVariables;
  }
  
  protected void incrementCorrelations(DoubleMatrix1D correlations, DoubleMatrix1D a, double gamma) {
    for (int rowNum = 0; rowNum < correlations.size(); rowNum++) {
      double oldValue = correlations.getQuick(rowNum);
      double newValue = oldValue - a.getQuick(rowNum) * gamma;
      correlations.setQuick(rowNum, newValue);
    }
  }
  
  protected void incrementPredictedValues(DoubleMatrix1D predictedOutput, DoubleMatrix1D uA, double gamma) {
    for (int sampleNum = 0; sampleNum < predictedOutput.size(); sampleNum++) {
      double oldValue = predictedOutput.getQuick(sampleNum);
      double newValue = oldValue + uA.getQuick(sampleNum) * gamma;
      predictedOutput.setQuick(sampleNum, newValue);
    }
  }
  
  protected void incrementCoefficients(DoubleMatrix1D coefficients, DoubleMatrix1D wA, DoubleMatrix1D correlations, int[] activeVariables, double gamma) {
    for (int activeIndex = 0; activeIndex < activeVariables.length; activeIndex++) {
      int varNum = activeVariables[activeIndex];
      double oldCoefficient = coefficients.getQuick(varNum);
      double sign = correlations.getQuick(varNum) < 0 ? -1 : 1;
      double newCoefficient = oldCoefficient + wA.getQuick(activeIndex) * gamma * sign;
      coefficients.setQuick(varNum, newCoefficient);
    }
  }

  public double[] getRegularizer() {
    return ArrayUtils.copyArray(m_Regularizer);
  }
  
  protected double getNonZeroAverage(double[] regularizer) {
    double sum = 0;
    int numNonZero = 0;
    for (int varNum = 0; varNum < regularizer.length; varNum++) {
      double value = regularizer[varNum];
      if (value == 0) {continue;}
      sum += value;
      numNonZero++;
    }
    return sum / numNonZero;
  }

  public IRegularizedFitter setRegularizer(double[] regularizer) {
    
    double newLambdaAverage = this.getNonZeroAverage(regularizer);
    //double newLambda = newLambdaAverage * m_InputValues.rows() / 2;
    double oldLambdaAverage = m_Lambda / m_InputValues.rows();
    
    DoubleMatrix2D newInput = m_InputValues.copy();
    for (int colNum = 0; colNum < newInput.columns(); colNum++) {
      double oldReg = m_Regularizer[colNum];
      double newReg = regularizer[colNum];
      double oldScale = (oldReg == 0) ? 1 : oldLambdaAverage / oldReg;
      //double oldScale = (oldReg == 0) ? 1 : m_Lambda / oldReg;
      double newScale = (newReg == 0) ? 1 : newLambdaAverage / newReg;
      //double newScale = (newReg == 0) ? 1 : newLambda / newReg;
      double scaleFactor = newScale / oldScale;
      for (int rowNum = 0; rowNum < newInput.rows(); rowNum++) {
        double oldValue = newInput.getQuick(rowNum, colNum);
        newInput.setQuick(rowNum, colNum, oldValue * scaleFactor);
      }
    }
    
    return this.getFitter(newInput, m_OutputValues, m_Weights, regularizer);
  }
  
  public IRegularizedFitter setRegularizer(double regParam) {
    double[] regularizer = new double[this.numVariables()];
    Arrays.fill(regularizer, regParam);
    return this.setRegularizer(regularizer);
  }
  
  public IRegularizedFitter setRegularizer(IRegGenerator generator, double[] parameters) {
    
    ILinearRegGenerator linearGenerator = (ILinearRegGenerator) generator;
    double[] reg = linearGenerator.getRegularizer(parameters, null);
    return this.setRegularizer(reg);
    
  }

  public IRegularizedFitter setRegularizer(int varNum, double newReg) {
    
    double[] regularizer = ArrayUtils.copyArray(m_Regularizer);
    regularizer[varNum] = newReg;
    double newLambdaAverage = this.getNonZeroAverage(regularizer);
    double newLambda = newLambdaAverage * m_InputValues.rows() / 2;
    
    DoubleMatrix2D newInput = m_InputValues.copy();
    double oldReg = m_Regularizer[varNum];
    double oldScale = (oldReg == 0) ? 1 : m_Lambda / oldReg;
    double newScale = (newReg == 0) ? 1 : newLambda / newReg;
    double scaleFactor = newScale / oldScale;
    for (int rowNum = 0; rowNum < newInput.rows(); rowNum++) {
      double oldValue = newInput.getQuick(rowNum, varNum);
      newInput.setQuick(rowNum, varNum, oldValue * scaleFactor);
    }
    
    return this.getFitter(newInput, m_OutputValues, m_Weights, regularizer);
  }

}
