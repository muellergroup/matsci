/*
 * Created on Aug 6, 2005
 *
 */
package matsci.model.linear;

import java.util.Arrays;

import matsci.model.IFitter;
import matsci.util.arrays.ArrayUtils;
/**
 * @author Tim Mueller
 *
 */
public class GroupLeastSquaresFitter implements ILinearFitter {

  private LeastSquaresFitter m_Fitter;
  private int[] m_GroupNumbers; // The group numbers for each of the samples.
  private double[] m_GroupWeights; // Indexed by group number.
  private double[] m_IndividualWeights; // We need to keep track of this separately in case group weights are set to 0

  private boolean m_UseGroupCV;
  private boolean m_UseGroupCVWeights;
  private int m_GroupToUse;
  
  /**
   * This method assumes that all the weights have already been set and should only be used internally.
   * 
   * @param groupNumbers
   * @param groupWeights
   * @param newFitter
   */
  protected GroupLeastSquaresFitter(GroupLeastSquaresFitter oldFitter, int[] groupNumbers, double[] groupWeights, double[] individualWeights, LeastSquaresFitter newFitter) {
    m_Fitter = newFitter;
    m_GroupNumbers = groupNumbers;
    m_GroupWeights = groupWeights;
    m_IndividualWeights = individualWeights;
    m_UseGroupCV = oldFitter.m_UseGroupCV;
    m_UseGroupCVWeights = oldFitter.m_UseGroupCVWeights;
    m_GroupToUse = oldFitter.m_GroupToUse;
  }
  
  public GroupLeastSquaresFitter(LeastSquaresFitter sourceFitter, int[] groupNumbers, double[] groupWeights) {
    m_IndividualWeights = sourceFitter.getWeights();
    double[] weightScale = new double[sourceFitter.numSamples()];
    for (int sampleNum = 0; sampleNum < weightScale.length; sampleNum++) {
      weightScale[sampleNum] = groupWeights[groupNumbers[sampleNum]];
    }
    m_Fitter = sourceFitter.scaleWeights(weightScale);
    m_GroupNumbers = ArrayUtils.copyArray(groupNumbers);
    m_GroupWeights = ArrayUtils.copyArray(groupWeights);
  }
  
  public GroupLeastSquaresFitter(double[][] allInputValues, double[] allOutputValues, double[] individualWeights, int[] groupNumbers, double[] groupWeights) {

    m_IndividualWeights = ArrayUtils.copyArray(individualWeights);
    m_GroupNumbers = ArrayUtils.copyArray(groupNumbers);
    m_GroupWeights = ArrayUtils.copyArray(groupWeights);
    double[] allWeights = getFullWeights();
    m_Fitter = new LeastSquaresFitter(allInputValues, allOutputValues, allWeights);
  }
  
  public GroupLeastSquaresFitter(double[][][] groupedInputValues, double[][] groupedOutputValues, double[][] groupedWeights, double[] groupWeights) {
    
    if (groupedInputValues.length != groupedOutputValues.length) {
      throw new RuntimeException("Number of groups for input values doesn't equal number of groups for output values!");
    }
    
    int numTotalSamples = 0;
    for (int groupNum = 0; groupNum < groupedInputValues.length; groupNum++) {
      numTotalSamples += groupedInputValues[groupNum].length;
    }
    
    // Initialize arrays with data about the groups
    double[][] allInputValues = new double[numTotalSamples][];
    double[] allOutputValues = new double[numTotalSamples];
    m_IndividualWeights = new double[numTotalSamples];
    Arrays.fill(m_IndividualWeights, 1);
    if (groupWeights != null) {
      m_GroupWeights = ArrayUtils.copyArray(groupWeights);
    } else {
      m_GroupWeights = new double[groupedInputValues.length];
      Arrays.fill(m_GroupWeights, 1);
    }
    m_GroupNumbers = new int[numTotalSamples];
    int sampleNum = 0;
    for (int groupNum = 0; groupNum < groupedInputValues.length; groupNum++) {
      double[][] inputValuesForGroup = groupedInputValues[groupNum];
      double[] outputValuesForGroup = groupedOutputValues[groupNum];
      if (inputValuesForGroup.length != outputValuesForGroup.length) {
        throw new RuntimeException("Number of input values and output values in each group must be the same.");
      }
      double samplesPerGroup = inputValuesForGroup.length;
      for (int groupSampleNum = 0; groupSampleNum < samplesPerGroup; groupSampleNum++) {
        allInputValues[sampleNum] = inputValuesForGroup[groupSampleNum];
        allOutputValues[sampleNum] = outputValuesForGroup[groupSampleNum];
        if (groupedWeights != null) {m_IndividualWeights[sampleNum] = groupedWeights[groupNum][groupSampleNum];}
        m_GroupNumbers[sampleNum] = groupNum;
        sampleNum++;
      }
    }
    
    double[] allWeights = this.getFullWeights();
    if (groupWeights == null && groupedWeights == null) {
      m_Fitter = new LeastSquaresFitter(allInputValues, allOutputValues);
    } else {
      m_Fitter = new LeastSquaresFitter(allInputValues, allOutputValues, allWeights);
    }
  }
  

  public GroupLeastSquaresFitter(double[][][] groupedInputValues, double[][] groupedOutputValues, double[] groupWeights) {
    this(groupedInputValues, groupedOutputValues, null, groupWeights);
  }
  
  protected double[] getFullWeights() {
    
    double[] returnArray = new double[m_IndividualWeights.length];
    
    if (m_IndividualWeights != null) {
      for (int sampleNum = 0; sampleNum < returnArray.length; sampleNum++) {
        returnArray[sampleNum] = m_GroupWeights[m_GroupNumbers[sampleNum]] * m_IndividualWeights[sampleNum];
      }
    }
    
    return returnArray;
  }

  /* (non-Javadoc)
   * @see matsci.model.linear.ILinearFitter#getCoefficients()
   */
  public double[] getCoefficients() {
    return m_Fitter.getCoefficients();
  }
  
  /* Creates a new group for the added samples
   * 
   * (non-Javadoc)
   * @see matsci.model.IFitter#addSamples(double[][], double[])
   */
  /*public IFitter addSamples(double[][] inputValues, double[] outputValues, double[] weights) {
    int[] groupNumbers = new int[inputValues.length];
    Arrays.fill(groupNumbers, m_GroupWeights.length);
    return this.addSamples(inputValues, outputValues, groupNumbers, weights);
  }*/
  
  /* Creates a new group for the added samples
   * 
   * (non-Javadoc)
   * @see matsci.model.IFitter#addSamples(double[][], double[], double[])
   */
  public IFitter addSamples(double[][] inputValues, double[] outputValues) {
    int[] groupNumbers = new int[inputValues.length];
    Arrays.fill(groupNumbers, m_GroupWeights.length);
    double[] newWeights = new double[inputValues.length];
    Arrays.fill(newWeights, 1);
    return this.addSamples(inputValues, outputValues, newWeights, groupNumbers);
  }
  
  public IFitter addSamples(double[][] inputValues, double[] outputValues, double[] weights) {
    int[] groupNumbers = new int[inputValues.length];
    Arrays.fill(groupNumbers, m_GroupWeights.length);
    return this.addSamples(inputValues, outputValues, weights, groupNumbers);
  }
  
  public IFitter addSamples(double[][] newInputValues, double[] newOutputValues, double[] newWeights, int[] groupNumbers) {
    double[] newIndividualWeights = ArrayUtils.appendArray(m_IndividualWeights, newWeights);
    int[] newGroupNumbers = ArrayUtils.appendArray(m_GroupNumbers, groupNumbers);
    
    int maxGroupNumber = m_GroupWeights.length - 1;
    for (int sampleNum = 0; sampleNum < groupNumbers.length; sampleNum++) {
      int groupNumber = groupNumbers[sampleNum];
      maxGroupNumber = Math.max(groupNumber, maxGroupNumber);
      if (groupNumber < m_GroupWeights.length) {
        newWeights[sampleNum] *= m_GroupWeights[groupNumber];
      }
    }
    double[] newGroupWeights = new double[maxGroupNumber + 1];
    Arrays.fill(newGroupWeights, 1);
    System.arraycopy(m_GroupWeights, 0, newGroupWeights, 0, m_GroupWeights.length);

    LeastSquaresFitter newFitter = (LeastSquaresFitter) m_Fitter.addSamples(newInputValues, newOutputValues, newWeights);
    return new GroupLeastSquaresFitter(this, newGroupNumbers, newGroupWeights, newIndividualWeights, newFitter);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#removeSamples(int[])
   */
  public IFitter removeSamples(int[] rowsToRemove) {
    int[] newGroupNumbers = m_GroupNumbers;
    double[] newIndividualWeights = m_IndividualWeights;
    for (int sampleNum = 0; sampleNum < rowsToRemove.length; sampleNum++) {
      newGroupNumbers = ArrayUtils.removeElement(newGroupNumbers, rowsToRemove[sampleNum]);
      newIndividualWeights =ArrayUtils.removeElement(newIndividualWeights, rowsToRemove[sampleNum]);
    }
    LeastSquaresFitter returnFitter = (LeastSquaresFitter) m_Fitter.removeSamples(rowsToRemove);
    return new GroupLeastSquaresFitter(this, newGroupNumbers, m_GroupWeights, newIndividualWeights, returnFitter);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#removeVariables(int[])
   */
  public IFitter removeVariables(int[] varsToRemove) {
    LeastSquaresFitter newFitter = (LeastSquaresFitter) m_Fitter.removeVariables(varsToRemove);
    return new GroupLeastSquaresFitter(this, m_GroupNumbers, m_GroupWeights, m_IndividualWeights, newFitter);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#addVariables(double[][], int[])
   */
  public IFitter addVariables(double[][] inputValues, int[] indices) {
    LeastSquaresFitter newFitter = (LeastSquaresFitter) m_Fitter.addVariables(inputValues, indices);
    return new GroupLeastSquaresFitter(this, m_GroupNumbers, m_GroupWeights, m_IndividualWeights, newFitter);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#addVariables(double[][])
   */
  public IFitter addVariables(double[][] inputValues) {
    LeastSquaresFitter newFitter = (LeastSquaresFitter) m_Fitter.addVariables(inputValues);
    return new GroupLeastSquaresFitter(this, m_GroupNumbers, m_GroupWeights, m_IndividualWeights, newFitter);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#numVariables()
   */
  public int numVariables() {
    return m_Fitter.numVariables();
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#numSamples()
   */
  public int numSamples() {
    return m_Fitter.numSamples();
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#getInputValue(int, int)
   */
  public double getInputValue(int sampleNum, int varNum) {
    return m_Fitter.getInputValue(sampleNum, varNum);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#getOutputValue(int)
   */
  public double getOutputValue(int sampleNum) {
    return m_Fitter.getOutputValue(sampleNum);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#clearVariables()
   */
  public IFitter clearVariables() {
    LeastSquaresFitter newFitter = (LeastSquaresFitter) m_Fitter.clearVariables();
    return new GroupLeastSquaresFitter(this, m_GroupNumbers, m_GroupWeights, m_IndividualWeights, newFitter);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#clearSamples()
   */
  public IFitter clearSamples() {
    LeastSquaresFitter newFitter = (LeastSquaresFitter) m_Fitter.clearSamples();
    double[] newWeights = ArrayUtils.copyArray(m_GroupWeights);
    return new GroupLeastSquaresFitter(this, new int[0], newWeights, new double[0], newFitter);
  }

  /* (non-Javadoc)
   * @see matsci.model.IFitter#scoreFit()
   */
  public double scoreFit() {
    return m_UseGroupCV ? this.getLOOCVScoreForGroup(m_GroupToUse, m_UseGroupCVWeights) : m_Fitter.scoreFit();
  }
  
  public LeastSquaresFitter getLeastSquaresFitter() {
    return m_Fitter;
  }
  
  public int numGroups() {
    return m_GroupWeights.length;
  }
  
  public double getGroupWeight(int groupNumber) {
    return m_GroupWeights[groupNumber];
  }
  
  public int getGroupNumber(int sampleNumber) {
    return m_GroupNumbers[sampleNumber];
  }
  
  public double getWeight(int sampleNumber) {
    return m_Fitter.getWeight(sampleNumber);
  }
  
  public double[] getWeights() {
    return m_Fitter.getWeights();
  }
  
  public double getIndividualWeight(int sampleNumber) {
    return m_IndividualWeights[sampleNumber];
  }
  
  public void useGroupCVScore(boolean useScore, boolean useWeights, int groupNumber) {
    m_UseGroupCV = useScore;
    m_UseGroupCVWeights = useWeights;
    m_GroupToUse = groupNumber;
  }
  
  public GroupLeastSquaresFitter setWeight(int groupNum, double weight) {
    
    double[] newGroupWeights = ArrayUtils.copyArray(m_GroupWeights);
    newGroupWeights[groupNum] = weight;
    double[] newWeights = new double[this.numSamples()];
    for (int sampleNum = 0; sampleNum < newWeights.length; sampleNum++) {
      newWeights[sampleNum] = newGroupWeights[m_GroupNumbers[sampleNum]] * m_IndividualWeights[sampleNum];
    }
    LeastSquaresFitter newFitter = m_Fitter.setWeights(newWeights);
    return new GroupLeastSquaresFitter(this, m_GroupNumbers, newGroupWeights, m_IndividualWeights, newFitter);
    
  }
  
  public GroupLeastSquaresFitter scaleGroupWeight(int groupNum, double weight) {
    
    double[] newGroupWeights = ArrayUtils.copyArray(m_GroupWeights);
    newGroupWeights[groupNum] *= weight;
    
    double[] scaleWeights = new double[this.numSamples()];
    for (int sampleNum = 0; sampleNum < scaleWeights.length; sampleNum++) {
      if (m_GroupNumbers[sampleNum] == groupNum) {
        scaleWeights[sampleNum] = newGroupWeights[groupNum];
      } else {
        scaleWeights[sampleNum] = 1;
      }
    }
    LeastSquaresFitter newFitter = m_Fitter.scaleWeights(scaleWeights);
    return new GroupLeastSquaresFitter(this, m_GroupNumbers, newGroupWeights, m_IndividualWeights, newFitter);
    
  }
  
  public double getRMSForGroup(int groupNumber) {
    
    double sum = 0;
    int numSamplesForGroup = 0;
    for (int sampleNum = 0; sampleNum < m_GroupNumbers.length; sampleNum++) {
      if (m_GroupNumbers[sampleNum] != groupNumber) {continue;}
      double delta = m_Fitter.getPredictedOutput(sampleNum) - m_Fitter.getOutputValue(sampleNum);
      sum += delta * delta * m_GroupWeights[groupNumber] * m_IndividualWeights[sampleNum];
      numSamplesForGroup++;
    }
    
    return Math.sqrt(sum / numSamplesForGroup);
    
  }
  
  public double getStdDevForGroup(int groupNumber) {
    double sum = 0;
    double sumSq = 0;
    int numSamplesForGroup = 0;
    for (int sampleNum = 0; sampleNum < m_GroupNumbers.length; sampleNum++) {
      if (m_GroupNumbers[sampleNum] != groupNumber) {continue;}
      double output = m_Fitter.getOutputValue(sampleNum);
      sum += output;
      sumSq += output * output;
      numSamplesForGroup++;
    }
    double mean = sum / numSamplesForGroup;
    
    return Math.sqrt((sumSq / numSamplesForGroup) - mean * mean);
  }
  
  public double getUnweightedRMSForGroup(int groupNumber) {
    
    double sum = 0;
    int numSamplesForGroup = 0;
    for (int sampleNum = 0; sampleNum < m_GroupNumbers.length; sampleNum++) {
      if (m_GroupNumbers[sampleNum] != groupNumber) {continue;}
      double delta = m_Fitter.getPredictedOutput(sampleNum) - m_Fitter.getOutputValue(sampleNum);
      sum += delta * delta;
      numSamplesForGroup++;
    }
    
    return Math.sqrt(sum / numSamplesForGroup);
  }
  
  public double getLOOCVScoreForGroup(int groupNumber, boolean useWeights) {
    boolean[] isSampleRelevant = new boolean[m_GroupNumbers.length];
    for (int sampleNum = 0; sampleNum < m_GroupNumbers.length; sampleNum++) {
      isSampleRelevant[sampleNum] = (m_GroupNumbers[sampleNum] == groupNumber);
    }
    return m_Fitter.calculateSelectLOOCVScore(isSampleRelevant, useWeights);
  }

  public double getRMSError() {
    return m_Fitter.getRMSError();
  }

  public double getUnweightedRMSError() {
    return m_Fitter.getUnweightedRMSError();
  }

  public double getLKOCVScore(int k) {
    return m_Fitter.getLKOCVScore(k);
  }

}
