/*
 * Created on Dec 15, 2004
 *
 */
package matsci.model.linear;

import matsci.model.IFitter;

/**
 * @author Tim Mueller
 *
 */
public interface ILinearFitter extends IFitter {
  public abstract double[] getCoefficients();
  
}