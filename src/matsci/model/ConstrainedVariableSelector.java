/*
 * Created on Aug 5, 2005
 *
 */
package matsci.model;

import matsci.util.arrays.ArrayUtils;
import java.util.Arrays;
/**
 * @author Tim Mueller
 *
 */
public class ConstrainedVariableSelector extends VariableSelector {

  // The "Plus" means that the variable itself is included
  private int[][] m_SubVariablesPlus;
  private int[][] m_SuperVariablesPlus;
  private boolean[] m_ChildForcedVariablesPlus;
  
  protected ConstrainedVariableSelector(ConstrainedVariableSelector selector) {
    super(selector);
    m_SubVariablesPlus = ArrayUtils.copyArray(selector.m_SubVariablesPlus);
    m_SuperVariablesPlus = ArrayUtils.copyArray(selector.m_SuperVariablesPlus);
    m_ChildForcedVariablesPlus = ArrayUtils.copyArray(selector.m_ChildForcedVariablesPlus);
  }
  
  public ConstrainedVariableSelector(IFitter fitter, double[][] inputValues, double[] outputValues) {
    super(fitter, inputValues, outputValues);
    m_ChildForcedVariablesPlus = new boolean[this.numAllowedVariables()];
    this.setConstraints(new int[this.numAllowedVariables()][0]);
  }
  
  public void forceVariables(int[] varNums) {
    super.forceVariables(varNums);
    for (int varIndex = 0; varIndex < varNums.length; varIndex++) {
      int[] subVariablesPlus = m_SubVariablesPlus[varNums[varIndex]];
      for (int subIndex = 0; subIndex < subVariablesPlus.length; subIndex++) {
        m_ChildForcedVariablesPlus[subVariablesPlus[subIndex]] = true;
      }
    }
  }
  
  /**
   * @param fitter
   * @param inputValues
   * @param outputValues
   */
  public ConstrainedVariableSelector(IFitter fitter, double[][] inputValues, double[] outputValues, int[][] childIndices) {
    super(fitter, inputValues, outputValues);
    m_ChildForcedVariablesPlus = new boolean[this.numAllowedVariables()];
    this.setConstraints(childIndices);
  }
  
  public VariableSelector copy() {
    return new ConstrainedVariableSelector(this);
  }
  
  public boolean activateVariable(int varIndex) {

    //System.out.println("activating " + varIndex);
    int[] activatedVars = this.activateVariables(m_SubVariablesPlus[varIndex]);
    return (activatedVars.length > 0);
  }
  
  /**
   * The set of variables passed to this method should be consistent with the constraint
   * 
   * @param varIndices
   * @return
   */
  protected int[] activateVariables(int[] varIndices) {
    // Count the variables to activate
    int numVarsToActivate = 0;
    for (int varIndexNum = 0; varIndexNum < varIndices.length; varIndexNum++) {
      if (!this.isVariableActive(varIndices[varIndexNum])) {numVarsToActivate++;}
    }
    
    // Do the activation
    int[] returnArray = new int[numVarsToActivate];
    double[][] newValues = new double[m_InputValues.length][numVarsToActivate];
    int activatedVarNum = 0;
    for (int varIndexNum = 0; varIndexNum < varIndices.length; varIndexNum++) {
      int varIndex = varIndices[varIndexNum];
      if (this.isVariableActive(varIndex)) {continue;}
      returnArray[activatedVarNum] = varIndex;
      this.m_ActiveIndices[varIndex] = this.m_Fitter.numVariables() + activatedVarNum;
      for (int rowNum = 0; rowNum < m_InputValues.length; rowNum++) {
        newValues[rowNum][activatedVarNum] = m_InputValues[rowNum][varIndex];
      }
      activatedVarNum++;
    }
    m_Fitter = this.m_Fitter.addVariables(newValues);
    return returnArray;
  }
  
  public boolean deactivateVariable(int index) {

    //System.out.println("deactivating " + index);
    int[] deactivatedVars = this.deactivateVariables(m_SuperVariablesPlus[index]);
    return (deactivatedVars.length > 0);
  }
  
  /**
   * This assumes that the given variable indices are consistent with the constraint
   * 
   * @param varsToDeactivate
   * @return
   */
  protected int[] deactivateVariables(int[] varIndices) {
    // Count the variables to deactivate
    int numVarsToDeactivate = 0;
    boolean[] varsToDeactivate = new boolean[this.numActiveVariables()];
    for (int varIndexNum = 0; varIndexNum < varIndices.length; varIndexNum++) {
      int varIndex = varIndices[varIndexNum];
      if (isVariableForced(varIndex)) {continue;}
      if (m_ChildForcedVariablesPlus[varIndex]) {continue;}
      if (!this.isVariableActive(varIndex)) {continue;}
      numVarsToDeactivate++;
      varsToDeactivate[this.m_ActiveIndices[varIndex]] = true;
    }
    
    // Create a sorted map of allowed variable indices to fitter variable indices
    int[] allowedVarIndices = new int[this.numActiveVariables()];
    for (int varIndex = 0; varIndex < this.m_ActiveIndices.length; varIndex++) {
      int fittedVarIndex = m_ActiveIndices[varIndex];
      if (fittedVarIndex < 0) {continue;}
      allowedVarIndices[fittedVarIndex] = varIndex;
    }
    
    // Deactivate the variables
    int[] returnArray = new int[numVarsToDeactivate];
    int[] fitterIndices = new int[numVarsToDeactivate];
    int numDeactivatedVars = 0;
    for (int fitterIndex = 0; fitterIndex < varsToDeactivate.length; fitterIndex++) {
      if (varsToDeactivate[fitterIndex]) {
        m_ActiveIndices[allowedVarIndices[fitterIndex]] = -1;
        returnArray[numDeactivatedVars] = allowedVarIndices[fitterIndex];
        fitterIndices[numDeactivatedVars] = fitterIndex;
        numDeactivatedVars++;
      } else {
        m_ActiveIndices[allowedVarIndices[fitterIndex]] -= numDeactivatedVars;
      }
    }
    m_Fitter = this.m_Fitter.removeVariables(fitterIndices);
    
    return returnArray;
  }
  
  public void setConstraints(int[][] childIndices) {
    m_SubVariablesPlus = ArrayUtils.copyArray(childIndices);
    m_SuperVariablesPlus = new int[m_SubVariablesPlus.length][0];
    Arrays.fill(m_ChildForcedVariablesPlus, false);
    
    // This just establishes the super and sub variable relationships
    for (int varIndex = 0; varIndex < m_SubVariablesPlus.length; varIndex++) {
      int[] varChildIndices = m_SubVariablesPlus[varIndex];
      m_SubVariablesPlus[varIndex] = ArrayUtils.appendElement(m_SubVariablesPlus[varIndex], varIndex);
      m_SuperVariablesPlus[varIndex] = ArrayUtils.appendElement(m_SuperVariablesPlus[varIndex], varIndex);
      for (int childIndexNum = 0; childIndexNum < varChildIndices.length; childIndexNum++) {
        m_SuperVariablesPlus[varChildIndices[childIndexNum]] = ArrayUtils.appendElement(m_SuperVariablesPlus[varChildIndices[childIndexNum]], varIndex);
        if (this.isVariableForced(varIndex)) {
          m_ChildForcedVariablesPlus[varChildIndices[childIndexNum]] = true;
        }
      }
    }
    
    // Re-set the state with the new constraint
    Object oldState =this.getSnapshot(null);
    this.deactivateAllVariables();
    this.setState(oldState);
  }

}
