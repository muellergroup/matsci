/*
 * Created on Dec 15, 2004
 *
 */
package matsci.model;

/**
 * @author Tim Mueller
 *
 */
public interface IFitter {
  
  //public abstract IFitter fitData(double[][] inputValues, double[] outputValues);
  //public abstract IFitter addSamples(double[][] inputValues, double[] outputValues);
  public IFitter addSamples(double[][] inputValues, double[] outputValues, double[] weights);
  public IFitter addSamples(double[][] inputValues, double[] outputValues);
  public IFitter removeSamples(int[] rowsToRemove);
  public IFitter removeVariables(int[] varsToRemove); 
  //public IFitter addVariables(double[][] inputValues, int[] indices);
  public IFitter addVariables(double[][] inputValues);
  public int numVariables();
  public int numSamples();
  public double getInputValue(int sampleNum, int varNum);
  public double getOutputValue(int sampleNum);
  public double getWeight(int sampleNum);
  public IFitter clearVariables();
  public IFitter clearSamples();
  public double scoreFit();
  public double getRMSError();
  public double getUnweightedRMSError();
  public double getLKOCVScore(int k);
}