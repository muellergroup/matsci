/*
 * Created on Mar 24, 2007
 *
 */
package matsci.model.reg;

public interface IRegGenerator {
  public double[] changeParametersRandomly(double[] oldParameters);
  public int numParameters();
}
