/*
 * Created on Apr 14, 2006
 *
 */
package matsci.model.reg;

import matsci.model.*;

public interface IRegularizedFitter extends IFitter {
  //public double[] getRegularizer();
  public IRegularizedFitter setRegularizer(IRegGenerator generator, double[] state);
  //public IRegularizedFitter setRegularizer(int varNum, double value);
}
