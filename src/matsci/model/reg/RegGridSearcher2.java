/*
 * Created on Jul 16, 2006
 *
 */
package matsci.model.reg;

import java.util.Arrays;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayIndexer;
import matsci.util.arrays.ArrayUtils;

public class RegGridSearcher2 {

  private final IRegGenerator m_Generator;
  private double m_Tolerance = 1E-4;
  
  public RegGridSearcher2(IRegGenerator generator) {
    m_Generator = generator;
  }
  
  public double[] findMinSimpleLogGrid(IRegularizedFitter fitter, double[] start, double[] scaleFactors, int[] halfGrid) {
    
    int[] minState = this.findMinimumState(fitter, start, scaleFactors, halfGrid);
    if (minState == null) {
      return null;
    }
    return this.getRegVars(minState, halfGrid, start, scaleFactors);
  }

  public double[] findMinLayeredLogGridScan(IRegularizedFitter fitter, double[] initRegVars, int[] halfGrid, double[] initScale, int numLayers) {

    int[] gridCenter = halfGrid;
    int[] fullGrid = new int[initRegVars.length];
    for (int varNum = 0; varNum < gridCenter.length; varNum++) {
      fullGrid[varNum] = gridCenter[varNum] * 2 + 1;
    }
    
    ArrayIndexer indexer = new ArrayIndexer(fullGrid);
    double[] values = new double[indexer.numAllowedStates()];
    double[] nextValues = new double[indexer.numAllowedStates()];
    
    double[] scaleFactors = ArrayUtils.copyArray(initScale);
    double[] currRegVars = initRegVars;
    double[] regularizer = null;
    int[] minState = null;
    int[] offSet =new int[gridCenter.length];
    double oldMinValue = 0;
    for (int layerNum = 0; layerNum < numLayers; layerNum++) {
      //System.out.println("LAYER: " + layerNum);
      Arrays.fill(values, -1);
      // We keep on moving to the local minimum until the local minimum doesn't change much.
      do {

        int minIndex = -1;
        double minValue = Double.POSITIVE_INFINITY;
        int[] state = indexer.getInitialState();
        // Calculate all the values
        do {
          int index = indexer.joinValuesBounded(state);
          if (values[index] < 0) {
            double[] regVars = this.getRegVars(state, gridCenter, currRegVars, scaleFactors);
            //regularizer = m_Generator.getRegularizer(regVars, regularizer);
            //try {
              //values[index] = fitter.setRegularizer(regularizer).scoreFit();
              values[index] = fitter.setRegularizer(m_Generator, regVars).scoreFit();
            //} catch (Exception e) {
              //values[index] = Double.POSITIVE_INFINITY;
            //}
          }
          double value = values[index];
          if (value < minValue) {
            minIndex = index;
            minValue = value;
          }
        } while (indexer.increment(state));
        minState = indexer.splitValue(minIndex);
        //System.out.println(minValue);

        if (Double.isNaN(minValue) || Double.isInfinite(minValue)) {
          return null;
        }
        
        double delta = Math.abs(oldMinValue - minValue);
        oldMinValue = minValue;
        currRegVars = this.getRegVars(minState, gridCenter, currRegVars, scaleFactors);
        
        // For testing and debugging
        /*for (int varNum= 0; varNum < minState.length; varNum++) {
          System.out.print(currRegVars[varNum] + ", ");
        }
        System.out.println(",,,  SCORE: " + minValue);*/
        
        if (Arrays.equals(minState, gridCenter) || (delta < m_Tolerance)) {
          break; // Go on to the next level
        }

        // Shift the center
        for (int varNum = 0; varNum < offSet.length; varNum++) {
          offSet[varNum] = gridCenter[varNum] - minState[varNum];
        }
        Arrays.fill(nextValues, -1);
        int[] oldState = indexer.getInitialState();
        do {
          int oldIndex = indexer.joinValuesBounded(oldState);
          int[] nextState = MSMath.arrayAdd(offSet, oldState);
          boolean outOfBounds =false;
          for (int varNum = 0; varNum < oldState.length; varNum++) {
            if (nextState[varNum] < 0 || nextState[varNum] >= indexer.getDimensionSize(varNum)) {
              outOfBounds = true;
              break;
            }
          }
          if (outOfBounds) continue;
          int newIndex = indexer.joinValuesBounded(nextState);
          nextValues[newIndex] = values[oldIndex];
          //if (oldIndex == minIndex) {System.out.println("match");}
        } while (indexer.increment(oldState));
        double[] tempValues = values;
        values = nextValues;
        nextValues = tempValues;
      } while (true);
      for (int varNum= 0; varNum < scaleFactors.length; varNum++) {
        scaleFactors[varNum] = Math.sqrt(scaleFactors[varNum]);
      }
    }
    return currRegVars;
  
  }
  
  public double[] findMinLayeredLogGridScan(IRegularizedFitter fitter, double[] initRegVars, double[] initScale, int numLayers) {
    int[] halfGrid = new int[initRegVars.length];
    for (int varNum = 0; varNum < halfGrid.length; varNum++) {
      halfGrid[varNum] = initRegVars[varNum] == 0 ? 0 : 1;
    }
    return this.findMinLayeredLogGridScan(fitter, initRegVars, halfGrid, initScale, numLayers);
  }
  
  protected int[] findMinimumState(IRegularizedFitter fitter, double[] start, double[] scaleFactors, int[] halfGrid) {
    
    int[] fullGrid = new int[halfGrid.length];
    for (int varNum = 0; varNum < fullGrid.length; varNum++) {
      fullGrid[varNum] = halfGrid[varNum] * 2 + 1;
    }
    ArrayIndexer gridIndexer = new ArrayIndexer(fullGrid);
    
    double minScore = Double.POSITIVE_INFINITY;
    int[] minState = null;
    
    int[] state = gridIndexer.getInitialState();
    //double[] regularizer = m_Generator.getRegularizer(new double[state.length], null);
    do {
      double[] regVars = this.getRegVars(state, halfGrid, start, scaleFactors);
      //double[] newReg = m_Generator.getRegularizer(regVars, regularizer);
      //double score = fitter.setRegularizer(newReg).scoreFit();
      double score = fitter.setRegularizer(m_Generator, regVars).scoreFit();
      if (score < minScore) {
        minScore = score;
        minState = ArrayUtils.copyArray(state);
      }
    } while (gridIndexer.increment(state));
    
    //System.out.println(minScore);
    return minState;
  }
  
  protected double[] getRegVars(int[] gridPoint, int[] gridCenter, double[] centerRegVars, double[] scaleFactors) {
    double[] regVars = new double[gridPoint.length];
    for (int varNum= 0; varNum < regVars.length; varNum++) {
      double exponent = (gridPoint[varNum] - gridCenter[varNum]);
      regVars[varNum] = centerRegVars[varNum] * Math.pow(scaleFactors[varNum], exponent);
    }
    return regVars;
  }

}
