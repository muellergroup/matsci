/*
 * Created on Jul 14, 2008
 *
 */
package matsci.model.reg;

import java.util.*;
import matsci.util.arrays.ArrayUtils;

public class SmoothingRegGenerator implements ILinearCovRegGenerator {

  private static Random RANDOM = new Random();
  
  private int m_NumVars;
  private double m_Factor = 2;
  
  public SmoothingRegGenerator(int numVars) {
    m_NumVars = numVars;
  }

  public double[][] getRegularizer(double[] parameters, double[][] template) {
    if (template == null) {
      template = new double[m_NumVars][m_NumVars];
    }
    double coupledValue = parameters[0];
    //double coupledValue = m_NumVars / 100.0;
    //double selfValue = parameters[1];
    double selfValue = parameters[0];
    for (int varNum = 0; varNum < template.length; varNum++) {
      template[varNum][varNum] += selfValue;
      if (varNum + 1 < m_NumVars) {
        template[varNum][varNum] += coupledValue;
        template[varNum][varNum + 1] = -coupledValue;
      }
      if (varNum - 1 >= 0) {
        template[varNum][varNum] += coupledValue;
        template[varNum][varNum - 1] = -coupledValue;
      }
    }
    return template;
  }

  public double[] changeParametersRandomly(double[] oldParameters) {
    double[] returnArray = ArrayUtils.copyArray(oldParameters);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }

  public int numParameters() {
    return 1;
  }

}
