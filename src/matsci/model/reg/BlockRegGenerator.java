/*
 * Created on Jul 14, 2008
 *
 */
package matsci.model.reg;

import matsci.util.arrays.*;

public class BlockRegGenerator implements ILinearCovRegGenerator {

  private int m_NumVars;
  private int[] m_BlockStarts;
  private ILinearCovRegGenerator[] m_RegGenerators;
  
  public BlockRegGenerator(int[] blockStarts, int numVars, ILinearCovRegGenerator[] regGenerators) {
    m_NumVars = numVars;
    m_BlockStarts = ArrayUtils.appendElement(blockStarts, numVars);
    m_RegGenerators = (ILinearCovRegGenerator[]) ArrayUtils.copyArray(regGenerators);
    for (int generatorNum = 0; generatorNum < m_RegGenerators.length; generatorNum++) {
      if (m_RegGenerators[generatorNum] == null) {
        int numBlockVars = m_BlockStarts[generatorNum + 1] - m_BlockStarts[generatorNum];
        m_RegGenerators[generatorNum] = new NullRegGenerator(numBlockVars);
      }
    }
  }

  public double[][] getRegularizer(double[] parameters, double[][] template) {
    if (template == null) {
      template = new double[m_NumVars][m_NumVars];
    }
    int currParamNum = 0;
    for (int blockNum = 0; blockNum < m_RegGenerators.length; blockNum++) {
      ILinearCovRegGenerator regGenerator = m_RegGenerators[blockNum];
      double[] blockParams = new double[regGenerator.numParameters()];
      for (int blockParamNum = 0; blockParamNum < blockParams.length; blockParamNum++) {
        blockParams[blockParamNum] = parameters[currParamNum++];
      }
      double[][] blockRegularizer = regGenerator.getRegularizer(blockParams, null);
      int blockStart = m_BlockStarts[blockNum];
      for (int blockVarNum = 0; blockVarNum < blockRegularizer.length; blockVarNum++) {
        int varNum = blockStart + blockVarNum;
        System.arraycopy(blockRegularizer[blockVarNum], 0, template[varNum], blockStart, blockRegularizer[blockVarNum].length);
      }
    }
    
    return template;
  }

  public double[] changeParametersRandomly(double[] oldParameters) {
    
    int currParamNum = 0;
    for (int blockNum = 0; blockNum < m_RegGenerators.length; blockNum++) {
      ILinearCovRegGenerator regGenerator = m_RegGenerators[blockNum];
      double[] blockParams = new double[regGenerator.numParameters()];
      for (int blockParamNum = 0; blockParamNum < blockParams.length; blockParamNum++) {
        blockParams[blockParamNum] = oldParameters[currParamNum++];
      }
      regGenerator.changeParametersRandomly(blockParams);
      int blockStart = m_BlockStarts[blockNum];
      System.arraycopy(blockParams, 0, oldParameters, blockStart, blockParams.length);
    }
    return oldParameters;
  }

  public int numParameters() {
    int numParameters = 0;
    for (int paramNum = 0; paramNum < m_RegGenerators.length; paramNum++) {
      numParameters += m_RegGenerators[paramNum].numParameters();
    }
    return numParameters;
  }
  
  private class NullRegGenerator implements ILinearCovRegGenerator {

    private int m_NumVariables;
    
    public NullRegGenerator(int numVariables) {
      m_NumVariables = numVariables;
    }
    
    public double[][] getRegularizer(double[] parameters, double[][] template) {
      if (template == null) {
        template = new double[m_NumVariables][m_NumVariables];
      }
      return template;
    }

    public double[] changeParametersRandomly(double[] oldParameters) {
      return oldParameters;
    }

    public int numParameters() {
      return 0;
    }
    
  }

}
