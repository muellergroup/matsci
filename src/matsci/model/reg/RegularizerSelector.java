/*
 * Created on Apr 13, 2006
 *
 */
package matsci.model.reg;

import matsci.engine.*;
import matsci.engine.minimizer.*;
import matsci.engine.monte.metropolis.IAllowsMetropolis;
import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.model.linear.*;
import matsci.util.MSMath;
import matsci.util.arrays.*;

public class RegularizerSelector implements IAllowsMetropolis, IContinuousFunctionState {
  
  protected IRegularizedFitter m_Fitter;
  protected double[] m_RegParameters; // These are the regularizer parameters, which must be non-negative
  
  protected IRegGenerator m_Generator;
  protected Event m_Event;
  
  protected double m_MaxAllowedGrad = 1E-5;
  protected double m_Resolution = 1E-4;
  
  public RegularizerSelector(IRegularizedFitter fitter, IRegGenerator generator, double[] regParameters) {
    //m_Fitter = fitter.setRegularizer(generator.getRegularizer(currentState, null));
    m_Fitter = fitter.setRegularizer(generator, regParameters);
    m_Generator = generator;
    m_RegParameters = ArrayUtils.copyArray(regParameters);
    m_Event = new Event();
  }
  
  /*public RegularizerSelector(RegularizerSelector oldSelector, double[] regParameters) {
    m_Generator = oldSelector.m_Generator;
    m_Event = oldSelector.m_Event;
    m_Fitter = oldSelector.m_Fitter.setRegularizer(m_Generator, regParameters);
    m_RegParameters = ArrayUtils.copyArray(regParameters);
  }*/

  public double getValue() {
    return m_Fitter.scoreFit();
  }
/*
  public boolean findMinBFGS() {
    
    double lastMinValue = this.getValue();
    if (Double.isInfinite(lastMinValue) || Double.isNaN(lastMinValue)) {return false;}
    double[] gradient = new double[m_RegParameters.length];
    double[] lastGradient = new double[m_RegParameters.length];
    double[] incState = new double[m_RegParameters.length];
    double[] negIncState = new double[m_RegParameters.length];
    double[] newState = new double[m_RegParameters.length];
    double maxGradSq = m_MaxAllowedGrad * m_MaxAllowedGrad;
    
    double dLnX = 1E-4;
    IRegularizedFitter pertFitter;
    IRegularizedFitter centerFitter;
    double stepSize = 1.0;
    
    double[][] invHessian = new double[m_RegParameters.length][m_RegParameters.length];
    for (int varNum = 0; varNum < invHessian.length; varNum++) {
      invHessian[varNum][varNum] = 1.0;
    }
    
    double[] s = new double[m_RegParameters.length];
    double[] y = new double[m_RegParameters.length];
    double[] invy = new double[m_RegParameters.length];
    double[] approxHessDiag =new double[m_RegParameters.length];
    
    boolean firstPass = true;
    while (true) {

      for (int varNum = 0; varNum < gradient.length; varNum++) {
        System.arraycopy(m_RegParameters, 0, incState, 0, m_RegParameters.length);
        System.arraycopy(m_RegParameters, 0, negIncState, 0, m_RegParameters.length);
        incState[varNum] *= Math.exp(dLnX);
        negIncState[varNum] *= Math.exp(-dLnX);
        pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
        double plusScore = pertFitter.scoreFit();
        pertFitter = m_Fitter.setRegularizer(m_Generator, negIncState);
        double minusScore = pertFitter.scoreFit();
        gradient[varNum] = (plusScore - minusScore) / (2*dLnX);
        y[varNum] = gradient[varNum] - lastGradient[varNum];
        approxHessDiag[varNum] = (plusScore + minusScore - 2*lastMinValue) / (dLnX * dLnX);
      }
      
      double gradMagSq = MSMath.dotProduct(gradient, gradient);
      if (Double.isInfinite(gradMagSq) || Double.isNaN(gradMagSq)) {return false;}
      if (gradMagSq < maxGradSq) {break;}
      
      if (!firstPass) {
        // Update the approximate hessian
        for(int rowNum = 0; rowNum < invHessian.length; rowNum++) {
          double[] row = invHessian[rowNum];
          invy[rowNum] = MSMath.dotProduct(row, y);
        }
        double invsy = 1.0 / MSMath.dotProduct(s, y);
        double yInvy = MSMath.dotProduct(y, invy);
        for (int rowNum = 0; rowNum < invHessian.length; rowNum++) {
          double[] row = invHessian[rowNum];
          for (int colNum = 0; colNum < row.length; colNum++) {
            row[colNum] += s[rowNum]*s[colNum]*(1+yInvy*invsy)*invsy;
            row[colNum] -= (invy[rowNum]*s[colNum] + invy[colNum]*s[rowNum])*invsy;
          }
        }
      } else {
        firstPass = false;
      }
      
      System.arraycopy(gradient, 0, lastGradient, 0, gradient.length);
      
      for(int rowNum = 0; rowNum < invHessian.length; rowNum++) {
        double[] row = invHessian[rowNum];
        s[rowNum] = -MSMath.dotProduct(row, gradient);
      }
      s = MSMath.normalize(s);
      double[] normGrad =MSMath.normalize(gradient);
      
      boolean stuck = (stepSize < dLnX);
      double linStep = stepSize;
      boolean reversed = false;
      while (!stuck) {
        for (int varNum = 0; varNum < incState.length; varNum++) {
          newState[varNum] = m_RegParameters[varNum] * Math.exp(stepSize * s[varNum]);
          incState[varNum] = newState[varNum] * Math.exp(-dLnX * s[varNum]);
          negIncState[varNum] = newState[varNum] * Math.exp(dLnX * s[varNum]);
        }
        //for (int varNum = 0; varNum < incState.length; varNum++) {
        //  newState[varNum] = m_RegParameters[varNum] * Math.exp(-stepSize * normGrad[varNum]);
        //  incState[varNum] = newState[varNum] * Math.exp(dLnX * normGrad[varNum]);
        //  negIncState[varNum] = newState[varNum] * Math.exp(-dLnX * normGrad[varNum]);
        //}

        centerFitter = m_Fitter.setRegularizer(m_Generator, newState);
        double centralScore = centerFitter.scoreFit();
        pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
        double plusScore = pertFitter.scoreFit();
        pertFitter = m_Fitter.setRegularizer(m_Generator, negIncState);
        double minusScore = pertFitter.scoreFit();
        double linGrad  = (plusScore - minusScore) / (2*dLnX);
        
        if ((centralScore < lastMinValue && (linGrad * linGrad < maxGradSq)) || linStep < dLnX) {
          double newValue = centralScore;
          //System.out.println(newValue);
          lastMinValue = newValue;
          m_Fitter = centerFitter;
          System.arraycopy(newState, 0, m_RegParameters, 0, newState.length);
          break;
        }
        double direction = linGrad > 0 ? 1 : -1;
        reversed |= (linGrad < 0);
        linStep *= reversed ? 0.5 : 2;
        stepSize += linStep * direction;
        stuck = (stepSize < dLnX);
      }
      if (stuck) {
        //System.out.println("Stuck");
        break;
      }
    }
    return true;
    
  }
  
  public boolean findMinGradDescent() {
    
    double lastMinValue = this.getValue();
    if (Double.isInfinite(lastMinValue) || Double.isNaN(lastMinValue)) {return false;}
    double[] gradient = new double[m_RegParameters.length];
    double[] lastGradient = new double[m_RegParameters.length];
    double[] incState = new double[m_RegParameters.length];
    double[] negIncState = new double[m_RegParameters.length];
    double maxGradSq = m_MaxAllowedGrad * m_MaxAllowedGrad;
    
    double dLnX = 1E-4;
    //double dx = 1E-4;
    IRegularizedFitter pertFitter;
    double stepSize = 1.0;
    
    while (true) {

      for (int varNum = 0; varNum < gradient.length; varNum++) {
        System.arraycopy(m_RegParameters, 0, incState, 0, m_RegParameters.length);
        System.arraycopy(m_RegParameters, 0, negIncState, 0, m_RegParameters.length);
        //double oldX = incState[varNum];
        //double dx = oldX * dLnX;
        //incState[varNum] += dx;
        //negIncState[varNum] -= dx;
        //pertRegularizer = m_Generator.getRegularizer(incState, pertRegularizer);
        //pertFitter = m_Fitter.setRegularizer(pertRegularizer);
        incState[varNum] *= Math.exp(dLnX);
        negIncState[varNum] *= Math.exp(-dLnX);
        pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
        double plusScore = pertFitter.scoreFit();
        pertFitter = m_Fitter.setRegularizer(m_Generator, negIncState);
        double minusScore = pertFitter.scoreFit();
        //gradient[varNum] = oldX * (pertFitter.scoreFit() - lastMinValue) / dx;
        //gradient[varNum] = (pertFitter.scoreFit() - lastMinValue) / dLnX;
        //gradient[varNum] = (pertFitter.scoreFit() - lastMinValue) / dx;
        //gradient[varNum] = (plusScore - minusScore) / (2*dx);
        gradient[varNum] = (plusScore - minusScore) / (2*dLnX);
        //System.out.println(minusScore + ", " + lastMinValue + ", " + plusScore);
      }
      
      double gradMagSq = MSMath.dotProduct(gradient, gradient);
      if (Double.isInfinite(gradMagSq) || Double.isNaN(gradMagSq)) {return false;}
      if (gradMagSq < maxGradSq) {break;}
      
      double mag = Math.sqrt(gradMagSq);
      //for (int varNum = 0; varNum < gradient.length; varNum++) {
        //gradient[varNum] /= mag;
      //  gradient[varNum] = gradient[varNum] * (2 * stepSize / (3 * mag)) + lastGradient[varNum] * stepSize / 3;
      //}
      //stepSize = MSMath.magnitude(gradient);
      for (int varNum = 0; varNum < gradient.length; varNum++) {
        gradient[varNum] /= mag;
        //gradient[varNum] /= stepSize;
      }
      
      //double dotProduct = MSMath.dotProduct(gradient, lastGradient);
      //stepSize *= Math.exp(dotProduct);
      System.arraycopy(gradient, 0, lastGradient, 0, gradient.length);
      
      double[] newState = ArrayUtils.copyArray(m_RegParameters);
      boolean stuck = (stepSize < dLnX);
      //double direction = 1;
        //while (!stuck) {
        //for (int varNum = 0; varNum < incState.length; varNum++) {
        //  double scale = direction * stepSize;
        //  newState[varNum] *= Math.exp(-scale * gradient[varNum]);
          //newState[varNum] +=  -scale * gradient[varNum];
        //}
        //pertRegularizer = m_Generator.getRegularizer(newState, pertRegularizer);
        //pertFitter = m_Fitter.setRegularizer(pertRegularizer);
        //pertFitter = m_Fitter.setRegularizer(m_Generator, newState);
        //double newValue = pertFitter.scoreFit();
        //double delta = newValue - lastMinValue;
        //if (delta < 0) {
        //  System.out.println(newValue);
        //  lastMinValue = newValue;
        //  m_Fitter = pertFitter;
        //  m_RegParameters = newState;
        //  break;
        //}
        //direction = -1;
        //stepSize /= 2;
        //stuck = (stepSize < dLnX);
        //stuck = (stepSize < dx);
      //}
      double linStep = stepSize;
      boolean reversed = false;
      int numPostReverse = 0;
      while (!stuck) {
        for (int varNum = 0; varNum < incState.length; varNum++) {
          newState[varNum] = m_RegParameters[varNum] * Math.exp(-stepSize * gradient[varNum]);
          incState[varNum] = newState[varNum] * Math.exp(dLnX * gradient[varNum]);
          negIncState[varNum] = newState[varNum] * Math.exp(-dLnX * gradient[varNum]);
        }

        pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
        double plusScore = pertFitter.scoreFit();
        pertFitter = m_Fitter.setRegularizer(m_Generator, negIncState);
        double minusScore = pertFitter.scoreFit();
        double linGrad  = (plusScore - minusScore) / (2*dLnX);

        if ((((plusScore + minusScore) / 2) < lastMinValue) && ((numPostReverse > 2) || (linStep < dLnX))) {
          pertFitter = m_Fitter.setRegularizer(m_Generator, newState);
          double newValue = pertFitter.scoreFit();
          //System.out.println(newValue);
          lastMinValue = newValue;
          m_Fitter = pertFitter;
          m_RegParameters = newState;
          //System.out.println("What?");
          break;
        }
        double direction = linGrad > 0 ? 1 : -1;
        reversed |= (linGrad < 0);
        if (reversed) {numPostReverse++;}
        linStep *= reversed ? 0.5 : 2;
        stepSize += linStep * direction;
        stuck = (stepSize < dLnX);
      }
      if (stuck) {
        //pertFitter = m_Fitter.setRegularizer(m_Generator, newState);
        //double oldValue = pertFitter.scoreFit();
        //double oldDelta = oldValue - lastMinValue;
        //for (int varNum = 0; varNum < incState.length; varNum++) {
        //  double scale = direction * stepSize * 4;
        //  newState[varNum] *= Math.exp(-scale * gradient[varNum]);
          //newState[varNum] +=  -scale * gradient[varNum];
        //}
       
        //pertFitter = m_Fitter.setRegularizer(m_Generator, newState);
        //double newValue = pertFitter.scoreFit();
        //double delta = newValue - lastMinValue;
        //System.out.println("Stuck: " + oldDelta + ", " + delta);
        break;
      }
    }
    return true;
    
  }

  public boolean findMinFletcherReeves() {
    
    double lastMinValue = this.getValue();
    if (Double.isInfinite(lastMinValue) || Double.isNaN(lastMinValue)) {return false;}
    double[] gradient = new double[m_RegParameters.length];
    double[] hVector = new double[m_RegParameters.length];
    double[] nextDirection = new double[m_RegParameters.length];
    double[] lastGradient = new double[m_RegParameters.length];
    double[] incState = new double[m_RegParameters.length];
    double[] negIncState = new double[m_RegParameters.length];
    double maxGradSq = m_MaxAllowedGrad * m_MaxAllowedGrad;
    
    double dLnX = 1E-4;
    IRegularizedFitter pertFitter;
    double stepSize = 1.0;
    
    while (true) {

      for (int varNum = 0; varNum < gradient.length; varNum++) {
        System.arraycopy(m_RegParameters, 0, incState, 0, m_RegParameters.length);
        System.arraycopy(m_RegParameters, 0, negIncState, 0, m_RegParameters.length);
        incState[varNum] *= Math.exp(dLnX);
        negIncState[varNum] *= Math.exp(-dLnX);
        pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
        double plusScore = pertFitter.scoreFit();
        pertFitter = m_Fitter.setRegularizer(m_Generator, negIncState);
        double minusScore = pertFitter.scoreFit();
        gradient[varNum] = (plusScore - minusScore) / (2*dLnX);
      }
      
      double gradMagSq = MSMath.dotProduct(gradient, gradient);
      if (Double.isInfinite(gradMagSq) || Double.isNaN(gradMagSq)) {return false;}
      if (gradMagSq < maxGradSq) {break;}
      
      double gamma = MSMath.dotProduct(gradient, gradient) / MSMath.dotProduct(lastGradient, lastGradient);
      if (Double.isInfinite(gamma)) {gamma = 0;}
      double hMagSq = 0;
      for (int varNum = 0; varNum < hVector.length; varNum++) {
        hVector[varNum] = -gradient[varNum] + gamma*hVector[varNum];
        hMagSq += hVector[varNum] * hVector[varNum];
      }
      double hMag = Math.sqrt(hMagSq);
      
      for (int varNum = 0; varNum < nextDirection.length; varNum++) {
        nextDirection[varNum] = hVector[varNum] / hMag;
      }
      
      System.arraycopy(gradient, 0, lastGradient, 0, gradient.length);
      
      double[] newState = ArrayUtils.copyArray(m_RegParameters);
      boolean stuck = (stepSize < dLnX);
      double linStep = stepSize;
      boolean reversed = false;
      int numPostReverse = 0;
      while (!stuck) {
        for (int varNum = 0; varNum < incState.length; varNum++) {
          newState[varNum] = m_RegParameters[varNum] * Math.exp(stepSize * nextDirection[varNum]);
          incState[varNum] = newState[varNum] * Math.exp(-dLnX * nextDirection[varNum]);
          negIncState[varNum] = newState[varNum] * Math.exp(dLnX * nextDirection[varNum]);
        }

        pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
        double plusScore = pertFitter.scoreFit();
        pertFitter = m_Fitter.setRegularizer(m_Generator, negIncState);
        double minusScore = pertFitter.scoreFit();
        double linGrad  = (plusScore - minusScore) / (2*dLnX);

        if ((((plusScore + minusScore) / 2) < lastMinValue) && ((linGrad * linGrad < maxGradSq) || (linStep < dLnX))) {
          pertFitter = m_Fitter.setRegularizer(m_Generator, newState);
          double newValue = pertFitter.scoreFit();
          //System.out.println(newValue);
          lastMinValue = newValue;
          m_Fitter = pertFitter;
          m_RegParameters = newState;
          break;
        }
        
        double direction = linGrad > 0 ? 1 : -1;
        reversed |= (linGrad < 0);
        if (Double.isInfinite(linGrad) || Double.isNaN(linGrad)) {
          reversed = true;
          direction = -1;
        }
        
        if (reversed) {numPostReverse++;}
        linStep *= reversed ? 0.5 : 2;
        stepSize += linStep * direction;
        stuck = (stepSize < dLnX);
      }
      if (stuck) {
        break;
      }
    }
    return true;
    
  }*/
  
  public boolean findMinPolakRibiere() {
    
    double lastMinValue = this.getValue();
    if (Double.isInfinite(lastMinValue) || Double.isNaN(lastMinValue)) {return false;}
    double[] gradient = new double[m_RegParameters.length];
    double[] hVector = new double[m_RegParameters.length];
    double[] nextDirection = new double[m_RegParameters.length];
    double[] lastGradient = new double[m_RegParameters.length];
    double[] incState = new double[m_RegParameters.length];
    double[] negIncState = new double[m_RegParameters.length];
    double maxGradSq = m_MaxAllowedGrad * m_MaxAllowedGrad;
    
    double dLnX = 1E-3; // Used to be 1E-7, but this caused numerical problems
    IRegularizedFitter pertFitter;
    
    // Used to be 1, but I think that lead to step sizes that were too small (especially after I implemented resolution). 
    // TODO consider making this infinite and just going with hmag.
    double stepSize = 10000.0; 
    
    double[] big = new double[m_RegParameters.length];
    double[] center = new double[big.length];
    double[] small = new double[big.length];
    double[] next = new double[small.length];
    double[] temp = new double[next.length];
    
    IRegularizedFitter bigFitter = null;
    IRegularizedFitter centerFitter = null;
    IRegularizedFitter smallFitter = null;
    IRegularizedFitter nextFitter = null;
    IRegularizedFitter tempFitter = null;
    
    while (true) {

      for (int varNum = 0; varNum < gradient.length; varNum++) {
        System.arraycopy(m_RegParameters, 0, incState, 0, m_RegParameters.length);
        System.arraycopy(m_RegParameters, 0, negIncState, 0, m_RegParameters.length);
        incState[varNum] *= Math.exp(dLnX);
        negIncState[varNum] *= Math.exp(-dLnX);
        pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
        double plusScore = pertFitter.scoreFit();
        pertFitter = m_Fitter.setRegularizer(m_Generator, negIncState);
        double minusScore = pertFitter.scoreFit();
        gradient[varNum] = (plusScore - minusScore) / (2*dLnX);
      }
      
      double gradMagSq = MSMath.dotProduct(gradient, gradient);
      if (Double.isInfinite(gradMagSq) || Double.isNaN(gradMagSq)) {return false;}
      if (gradMagSq < maxGradSq) {
        break;
      }
      
      double numerator = 0;
      for (int varNum = 0; varNum < gradient.length; varNum++) {
        numerator += (gradient[varNum] - lastGradient[varNum]) * gradient[varNum];
      }
      
      double gamma = numerator / MSMath.dotProduct(lastGradient, lastGradient);
      if (Double.isInfinite(gamma)) {gamma = 0;}
      double hMagSq = 0;
      for (int varNum = 0; varNum < hVector.length; varNum++) {
        hVector[varNum] = gradient[varNum] + gamma*hVector[varNum];
        hMagSq += hVector[varNum] * hVector[varNum];
      }
      double hMag = Math.sqrt(hMagSq);
      
      for (int varNum = 0; varNum < nextDirection.length; varNum++) {
        nextDirection[varNum] = hVector[varNum] / hMag;
      }
      
      System.arraycopy(gradient, 0, lastGradient, 0, gradient.length);
      stepSize = Math.min(stepSize, hMag);
      
      double grSmall = (3.0 - Math.sqrt(5)) / 2;
      double grLarge = 1 - grSmall;
      
      System.arraycopy(m_RegParameters, 0, big, 0, m_RegParameters.length);
      double bigX = 0;
      double bigVal = lastMinValue;
      double smallX = 1;
      double smallVal = bigVal + 1;
      while (Double.isNaN(smallVal) || smallVal > bigVal) {
        smallX /= 2;
        if (smallX < dLnX) {break;}
        for (int varNum = 0; varNum < incState.length; varNum++) {
          small[varNum] = m_RegParameters[varNum] * Math.exp(-stepSize * nextDirection[varNum] * smallX);
        }
        smallFitter = m_Fitter.setRegularizer(m_Generator, small);
        smallVal = smallFitter.scoreFit();
      }
      if (smallX < dLnX) {
        //System.out.println(((LeastSquaresFitter)m_Fitter).verifyInverse()); // For debugging
        break;
      }
      double centerVal = smallVal + 1;
      double centerX = smallX;
      while (smallVal < centerVal) {
        temp = center;
        tempFitter = centerFitter;
        center = small;
        centerVal = smallVal;
        centerX = smallX;
        centerFitter = smallFitter;
        smallX *= 1 / grLarge;
        small = temp;
        smallFitter = tempFitter;
        for (int varNum = 0; varNum < small.length; varNum++) {
          small[varNum] = m_RegParameters[varNum] * Math.exp(-stepSize * nextDirection[varNum] * smallX);
        }
        smallFitter = m_Fitter.setRegularizer(m_Generator, small);
        smallVal = smallFitter.scoreFit();
        smallVal = Double.isNaN(smallVal) ? Double.POSITIVE_INFINITY : smallVal;
      }
      double maxDiff = Math.max(bigVal - centerVal, smallVal - centerVal);
      if (maxDiff <= m_Resolution) {
        break;
      }
      
      double linGrad = Double.POSITIVE_INFINITY;
      while ((linGrad * linGrad) > maxGradSq && (maxDiff > m_Resolution)) {
        double nextX = bigX + smallX - centerX;
        for (int varNum = 0; varNum < incState.length; varNum++) {
          next[varNum] = m_RegParameters[varNum] * Math.exp(-stepSize * nextDirection[varNum] * nextX);
        }
        nextFitter = m_Fitter.setRegularizer(m_Generator, next);
        double nextVal = nextFitter.scoreFit();
        if (Double.isNaN(nextVal)) {
          break;
        }
        //nextVal = Double.isNaN(nextVal) ? Double.POSITIVE_INFINITY : nextVal;
        if (nextVal < centerVal) {
          linGrad = (centerVal - nextVal) / (centerX - nextX);
          temp = small;
          tempFitter = smallFitter;
          smallVal = centerVal;
          small = center;
          smallX = centerX;
          smallFitter = centerFitter;
          centerVal = nextVal;
          center = next;
          centerX = nextX;
          centerFitter = nextFitter;
          next = temp;
          nextFitter = tempFitter;
        } else {
          temp = big;
          tempFitter = bigFitter;
          bigVal = smallVal;
          big = small;
          bigX = smallX;
          bigFitter = smallFitter;
          smallVal = nextVal;
          small = next;
          smallX = nextX;
          smallFitter = nextFitter;
          next = temp;
          nextFitter = tempFitter;
          double smallGrad = Math.abs((smallVal - centerVal) / (smallX - centerX));
          double bigGrad = Math.abs((bigVal - centerVal) / (bigX - centerX));
          linGrad = Math.min(smallGrad, bigGrad);
        }
        if ((centerX > smallX && centerX > bigX) || (centerX < smallX && centerX < bigX)) {
          break;
        }
        maxDiff = Math.max(bigVal - centerVal, smallVal - centerVal);
      }
      if (Double.isNaN(centerVal) || Double.isInfinite(centerVal)) {
        return false;
      }
      stepSize = centerX;
      temp = m_RegParameters;
      m_RegParameters = center;
      
      // For debugging
      /*for (int varNum = 0; varNum < m_RegParameters.length; varNum++) {
        System.out.print(m_RegParameters[varNum] + ", ");
      }
      System.out.println();*/
      
      m_Fitter = centerFitter;
      lastMinValue = centerVal;
      center = temp;
    }
    return true;
    
  }

  public IMetropolisEvent getRandomEvent() {
    m_Event.setRegParameters(m_Generator.changeParametersRandomly(m_RegParameters));
    return m_Event;
  }

  public void setState(Object snapshot) {
    double[] regParameters = (double[]) snapshot;
    System.arraycopy(regParameters, 0, m_RegParameters, 0, regParameters.length);
    //m_RegParameters = (double[]) snapshot;
    //m_Fitter = m_Fitter.setRegularizer(m_Generator.getRegularizer(m_RegParameters, null));
    m_Fitter = m_Fitter.setRegularizer(m_Generator, m_RegParameters);
  }
  
  public void setMaxAllowedGrad(double value) {
    m_MaxAllowedGrad = value;
  }
  
  public void setResolution(double value) {
    m_Resolution = value;
  }
  
  public double getResolution() {
    return m_Resolution;
  }

  public Object getSnapshot(Object template) {
    //return this.getUnboundedParameters((double[]) template);
    return ArrayUtils.copyArray(m_RegParameters);
  }
  
  public IRegularizedFitter getFitter() {
    return m_Fitter;
  }
  
  
  /*
  public boolean findMinGenericMinimizer() {
    CGMinimizer minimizer = new CGMinimizer();
    return minimizer.minimize(this);
  }*/

  public int numParameters() {
    return m_Generator.numParameters();
  }
  
  public double[] getBoundedParameters(double[] template) {
    if (template == null) {
      template = new double[m_RegParameters.length];
    }
    System.arraycopy(m_RegParameters, 0, template, 0, template.length);
    return template;
  }

  public double[] getUnboundedParameters(double[] template) {
    double[] parameters = (template == null) ? new double[this.numParameters()] : template;
    //System.arraycopy(m_RegParameters, 0, parameters, 0, parameters.length);
    for (int varNum = 0; varNum < parameters.length; varNum++) {
      parameters[varNum] = Math.log(m_RegParameters[varNum]);
    }
    return parameters;
    /*if (template == null) {
      template = new double[m_RegParameters.length];
    }
    System.arraycopy(m_RegParameters, 0, template, 0, template.length);
    return template;*/
  }

  public double[] getGradient(double[] template) {
    double[] gradient = (template == null) ? new double[this.numParameters()] : template;
    double dLnX = 1E-4;
    double[] incState = new double[m_RegParameters.length];
    IRegularizedFitter pertFitter = null;
    for (int varNum = 0; varNum < gradient.length; varNum++) {
      System.arraycopy(m_RegParameters, 0, incState, 0, m_RegParameters.length);
      incState[varNum] *= Math.exp(dLnX);
      pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
      double plusScore = pertFitter.scoreFit();
      incState[varNum] *= Math.exp(-2 * dLnX);
      pertFitter = m_Fitter.setRegularizer(m_Generator, incState);
      double minusScore = pertFitter.scoreFit();
      gradient[varNum] = (plusScore - minusScore) / (2*dLnX);
    }
    
    return gradient;
  }

  public IContinuousFunctionState setUnboundedParameters(double[] parameters) {
    //System.arraycopy(parameters, 0, m_RegParameters, 0, parameters.length);
    /*for (int varNum = 0; varNum < m_RegParameters.length; varNum++) {
      m_RegParameters[varNum] = Math.exp(parameters[varNum]);
    }
    m_Fitter= m_Fitter.setRegularizer(m_Generator, m_RegParameters);
    return this;*/
    double[] state = new double[parameters.length];
    for (int varNum = 0; varNum < m_RegParameters.length; varNum++) {
      state[varNum] = Math.exp(parameters[varNum]);
    }
    return new RegularizerSelector(m_Fitter, m_Generator, state);
  }
  
  protected class Event implements IMetropolisEvent {

    protected IRegularizedFitter m_OldFitter;
    protected double[] m_OldRegParameters;
    
    protected double[] m_NewRegParameters;
    //private double[] m_NewRegularizer;
    
    public void trigger() {
      m_OldRegParameters = m_RegParameters;
      m_OldFitter = m_Fitter;
      //m_Fitter = m_OldFitter.setRegularizer(m_NewRegularizer);
      m_Fitter = m_OldFitter.setRegularizer(m_Generator, m_NewRegParameters);
      m_RegParameters = m_NewRegParameters;
    }

    public double getDelta() {
      //IRegularizedFitter newFitter = m_OldFitter.setRegularizer(m_NewRegularizer);
      IRegularizedFitter newFitter = m_Fitter.setRegularizer(m_Generator, m_NewRegParameters);
      return newFitter.scoreFit() - m_Fitter.scoreFit();
    }

    public double triggerGetDelta() {
      this.trigger();
      return m_Fitter.scoreFit() - m_OldFitter.scoreFit();
    }

    public void reverse() {
      m_Fitter = m_OldFitter;
      m_RegParameters = m_OldRegParameters;
    }
    
    public void setRegParameters(double[] regParameters) {
      m_NewRegParameters = regParameters;
      //m_NewRegularizer = m_Generator.getRegularizer(newState, m_NewRegularizer);
    }
  }
  
}
