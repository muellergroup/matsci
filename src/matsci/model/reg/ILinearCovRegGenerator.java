/*
 * Created on Mar 24, 2007
 *
 */
package matsci.model.reg;

public interface ILinearCovRegGenerator extends IRegGenerator {
  public double[][] getRegularizer(double[] parameters, double[][] template);
}
