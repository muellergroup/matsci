/*
 * Created on Apr 14, 2006
 *
 */
package matsci.model.reg;

public interface ILinearRegGenerator extends IRegGenerator {
  public double[] getRegularizer(double[] parameters, double[] template);
  public int numVariables();
}
