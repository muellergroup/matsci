/*
 * Created on May 22, 2006
 *
 */
package matsci.model.reg;

import matsci.util.arrays.*;

public class RegGridSearcher {
  
  private final ILinearRegGenerator m_Generator;
  
  public RegGridSearcher(ILinearRegGenerator generator) {
    m_Generator = generator;
  }
  
  public double[] findMinimumLog(IRegularizedFitter fitter, double[] start, double[] scaleFactors, int[] halfGrid) {
    
    int[] minState = this.findMinimumState(fitter, start, scaleFactors, halfGrid);
    return this.getRegVars(minState, halfGrid, start, scaleFactors);
  }
  
  public double[] findRegLogBinary(IRegularizedFitter fitter, double[] start, double[] initScale, int numLayers) {
  
    int[] halfGrid = new int[start.length];
    for (int varNum = 0; varNum < halfGrid.length; varNum++) {
      halfGrid[varNum] = start[varNum] == 0 ? 0 : 2;
    }
    
    double tolerance = 1E-15;
    double score = Double.POSITIVE_INFINITY;
    double[] scaleFactors = ArrayUtils.copyArray(initScale);
    int[] minState = null;
    IRegularizedFitter origFitter = fitter;
    for (int layerNum = 0; layerNum < numLayers; layerNum++) {
      boolean onEdge = true;
      while (onEdge) {
        minState = this.findMinimumState(fitter, start, scaleFactors, halfGrid);
        onEdge = false;
        start = this.getRegVars(minState, halfGrid, start, scaleFactors);
        //double[] startReg = m_Generator.getRegularizer(start, null);
        fitter = origFitter.setRegularizer(m_Generator, start);
        double newScore = fitter.scoreFit();
        for (int varNum = 0; varNum < minState.length; varNum++) {
          double delta = Math.abs(score - newScore);
          if (halfGrid[varNum] == 0 || start[varNum] == 0 || (delta < tolerance)) {continue;}
          if (minState[varNum] == 0 || minState[varNum] == (halfGrid[varNum] * 2 + 1)) {
            onEdge = true;
            break;
          }
        }
        score = newScore;
        for (int varNum= 0; varNum < minState.length; varNum++) {
          System.out.print(start[varNum] + ", ");
        }
        System.out.println(",,,  SCORE: " + score);
        if (!onEdge) {break;}
      }
      for (int varNum = 0; varNum < scaleFactors.length; varNum++) {
        scaleFactors[varNum] = Math.sqrt(scaleFactors[varNum]);
      }
    }
    return start;
  
  }
  
  protected int[] findMinimumState(IRegularizedFitter fitter, double[] start, double[] scaleFactors, int[] halfGrid) {
    
    int[] fullGrid = new int[halfGrid.length];
    for (int varNum = 0; varNum < fullGrid.length; varNum++) {
      fullGrid[varNum] = halfGrid[varNum] * 2 + 1;
    }
    ArrayIndexer gridIndexer = new ArrayIndexer(fullGrid);
    
    double minScore = Double.POSITIVE_INFINITY;
    int[] minState = null;
    
    int[] state = gridIndexer.getInitialState();
    //double[] regularizer = m_Generator.getRegularizer(new double[state.length], null);
    do {
      double[] regVars = this.getRegVars(state, halfGrid, start, scaleFactors);
      //fitter = fitter.setRegularizer(m_Generator.getRegularizer(regVars, regularizer));
      fitter = fitter.setRegularizer(m_Generator, regVars);
      double score = fitter.scoreFit();
      if (score < minScore) {
        minScore = score;
        minState = ArrayUtils.copyArray(state);
      }
    } while (gridIndexer.increment(state));
    
    System.out.println(minScore);
    return minState;
  }
  
  protected double[] getRegVars(int[] state, int[] halfGrid, double[] start, double[] scaleFactors) {
    double[] regVars = new double[state.length];
    for (int varNum= 0; varNum < regVars.length; varNum++) {
      double exponent = (state[varNum] - halfGrid[varNum]);
      regVars[varNum] = start[varNum] * Math.pow(scaleFactors[varNum], exponent);
    }
    return regVars;
  }
}
