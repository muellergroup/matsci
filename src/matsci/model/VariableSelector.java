/*
 * Created on Jun 6, 2005
 *
 */
package matsci.model;

import java.util.Arrays;
import java.util.Random;

import matsci.engine.monte.metropolis.IAllowsMetropolis;
import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class VariableSelector implements IAllowsMetropolis {

  protected IFitter m_Fitter;
  protected final double[][] m_InputValues;
  protected final double[] m_OutputValues;
  
  protected final int[] m_ActiveIndices;
  protected boolean[] m_ForcedVariables; // True if the variable needs to be kept
  protected Event m_Event;
  protected static Random GENERATOR = new Random();
  
  protected VariableSelector(VariableSelector sourceSelector) {
    m_Fitter = sourceSelector.m_Fitter;
    m_InputValues = sourceSelector.m_InputValues;
    m_OutputValues = sourceSelector.m_OutputValues;
    m_ActiveIndices = ArrayUtils.copyArray(sourceSelector.m_ActiveIndices);
    m_ForcedVariables = ArrayUtils.copyArray(sourceSelector.m_ForcedVariables);
    m_Event = new Event();
  }
  
  /**
   * 
   */
  public VariableSelector(IFitter fitter, double[][] inputValues, double[] outputValues) {
    if (inputValues.length == 0) {
      throw new RuntimeException("No input data given");
    }
    
    m_Fitter = fitter.clearVariables();
    m_InputValues = ArrayUtils.copyArray(inputValues);
    m_OutputValues = ArrayUtils.copyArray(outputValues);
    m_ActiveIndices = new int[m_InputValues[0].length];
    Arrays.fill(m_ActiveIndices, -1);
    m_Event = new Event();
    m_ForcedVariables = new boolean[m_ActiveIndices.length];
  }
  
  public VariableSelector copy() {
    return new VariableSelector(this);
  }
  
  public void forceVariables(int[] varNums) {
    for (int varIndex = 0; varIndex < varNums.length; varIndex++) {
      int varNumToForce = varNums[varIndex];
      m_ForcedVariables[varNumToForce] = true;
      this.activateVariable(varNumToForce);
    }
  }
  
  public boolean activateVariable(int index) {
    if (isVariableActive(index)) {return false;}
    double[][] varsToAdd = new double[m_OutputValues.length][1];
    for (int rowNum = 0; rowNum < varsToAdd.length; rowNum++) {
      varsToAdd[rowNum][0] = m_InputValues[rowNum][index];
    }
    m_ActiveIndices[index] = m_Fitter.numVariables();
    m_Fitter = m_Fitter.addVariables(varsToAdd);
    return true;
  }
  
  public IMetropolisEvent getGreedyActivationEvent() {
    
    int minVarNum = -1;
    double minDelta = 0;
    for (int varNum = 0; varNum < this.numAllowedVariables(); varNum++) {
      if (this.isVariableActive(varNum)) {continue;}
      IMetropolisEvent event = this.getToggleEvent(varNum);
      if (event == null) {continue;}
      double delta = event.getDelta();
      if (delta < minDelta) {
        minVarNum = varNum;
        minDelta = delta;
      }
    }
    return this.getToggleEvent(minVarNum);
  }
  
  public IMetropolisEvent getGreedyDeactivationEvent() {
    int minVarNum = -1;
    double minDelta = 0;
    for (int varNum = 0; varNum < this.numAllowedVariables(); varNum++) {
      if (!this.isVariableActive(varNum)) {continue;}
      IMetropolisEvent event = this.getToggleEvent(varNum);
      if (event == null) {continue;}
      double delta = event.getDelta();
      if (delta < minDelta) {
        minVarNum = varNum;
        minDelta = delta;
      }
    }
    return this.getToggleEvent(minVarNum);
  }
  
  public void deactivateAllVariables() {
    for (int varNum = 0; varNum < this.numAllowedVariables(); varNum++) {
      this.deactivateVariable(varNum);
    }
  }
  
  public boolean deactivateVariable(int index) {
    if (!isVariableActive(index)) {return false;}
    if (isVariableForced(index)) {return false;}
    
    int oldIndex = m_ActiveIndices[index];
    m_ActiveIndices[index] = -1;
    for (int varNum = 0; varNum < m_ActiveIndices.length; varNum++) {
      if (m_ActiveIndices[varNum] > oldIndex) {
        m_ActiveIndices[varNum]--;
      }
    }
    int[] varsToRemove = new int[] {oldIndex};
    m_Fitter = m_Fitter.removeVariables(varsToRemove);
    return true;
  }
  
  public boolean isVariableForced(int index) {
    return m_ForcedVariables[index];
  }
  
  public boolean isVariableActive(int index) {
    return (m_ActiveIndices[index] >= 0);
  }
  
  public int numAllowedVariables() {
    if (m_InputValues.length == 0) {return 0;}
    return m_InputValues[0].length;
  }
  
  public int numActiveVariables() {
    return m_Fitter.numVariables();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getValue()
   */
  public double getValue() {
    return m_Fitter.scoreFit();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getRandomEvent()
   */
  public IMetropolisEvent getRandomEvent() {
    
    int varToChange = GENERATOR.nextInt(m_ActiveIndices.length);
    // We don't accept any forced variables
    while (m_ForcedVariables[varToChange]) {
      varToChange = GENERATOR.nextInt(m_ActiveIndices.length);
    }
    return this.getToggleEvent(varToChange);
  }
  
  public IMetropolisEvent getToggleEvent(int varToChange) {
    // We don't accept any forced variables
    if (varToChange < 0 || varToChange >= this.numAllowedVariables()) {return null;}
    if (m_ForcedVariables[varToChange]) {return null;}
    
    m_Event.m_VarToChange = varToChange;
    m_Event.m_OldFitter = m_Fitter;
    m_Event.m_ActivateVariable = (m_ActiveIndices[varToChange] < 0);
    System.arraycopy(m_ActiveIndices, 0, m_Event.m_OldActiveIndices, 0, m_ActiveIndices.length);
    
    return m_Event;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#setState(java.lang.Object)
   */
  public void setState(Object snapshot) {
    boolean[] varsToActivate = (boolean[]) snapshot;
    int activeVarIndex = 0;
    for (int varNum = 0; varNum < varsToActivate.length; varNum++) {
      if (varsToActivate[varNum] || m_ForcedVariables[varNum]) {
        m_ActiveIndices[varNum] = activeVarIndex++;
      } else {
        m_ActiveIndices[varNum] = -1;
      }
    }
    double[][] selectedInput = new double[m_OutputValues.length][activeVarIndex];
    int currSelectedVar = 0;
    for (int varNum = 0; varNum < m_ActiveIndices.length; varNum++) {
      if (m_ActiveIndices[varNum] < 0) {continue;}
      for (int rowNum = 0; rowNum < selectedInput.length; rowNum++) {
        selectedInput[rowNum][currSelectedVar] = m_InputValues[rowNum][varNum];
      }
      currSelectedVar++;
    }
    m_Fitter = m_Fitter.clearVariables();
    m_Fitter = m_Fitter.addVariables(selectedInput);
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#getSnapshot(java.lang.Object)
   */
  public Object getSnapshot(Object template) {
    boolean[] returnArray = (boolean[]) template;
    if (returnArray == null) {returnArray = new boolean[m_ActiveIndices.length];}
    for (int varNum = 0; varNum < m_ActiveIndices.length; varNum++) {
      returnArray[varNum] = (m_ActiveIndices[varNum] >= 0);
    }
    return returnArray;
  }
  
  private boolean validate() {
    for (int varNum = 0; varNum < m_ActiveIndices.length; varNum++) {
      int varIndex = m_ActiveIndices[varNum];
      if (varIndex < 0) {continue;}
      for (int rowNum = 0; rowNum < m_InputValues.length; rowNum++) {
        if (m_Fitter.getInputValue(rowNum, varIndex) != m_InputValues[rowNum][varNum]) {
          return false;
        }
      }
    }
    return true;
  }
  
  public IFitter getFitter() {
    return m_Fitter;
  }
  
  public class Event implements IMetropolisEvent {

    private int m_VarToChange;
    private boolean m_ActivateVariable;
    private IFitter m_OldFitter;
    private int[] m_OldActiveIndices;
    
    public Event() {
      m_OldFitter = m_Fitter;
      m_OldActiveIndices = ArrayUtils.copyArray(m_ActiveIndices);
    }
    
    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#trigger()
     */
    public void trigger() {
      if (m_ActivateVariable) {
        activateVariable(m_VarToChange);
      } else {
        deactivateVariable(m_VarToChange);
      }
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#getDelta()
     */
    public double getDelta() {
      double delta = this.triggerGetDelta();
      this.reverse();
      return delta;
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#triggerGetDelta()
     */
    public double triggerGetDelta() {
      this.trigger();
      return m_Fitter.scoreFit() - m_OldFitter.scoreFit();
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#reverse()
     */
    public void reverse() {
      System.arraycopy(m_OldActiveIndices, 0, m_ActiveIndices, 0, m_ActiveIndices.length);
      m_Fitter = m_OldFitter;
    }
  }

}
