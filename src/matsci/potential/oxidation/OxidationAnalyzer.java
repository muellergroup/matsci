/*
 * Created on Jul 24, 2008
 *
 */
package matsci.potential.oxidation;

import java.util.Arrays;
import java.util.HashMap;

import matsci.*;
import matsci.io.app.log.Status;
import matsci.io.app.param.ParameterParser;
import matsci.io.app.param.Parameters;
import matsci.location.*;
import matsci.structure.*;
import matsci.util.*;
import matsci.util.arrays.*;

/**
 * 
 * This class examines a given structure and determines the valence states for the atoms in the
 * structure.  Several different methods of determining valence states may be used.
 * 
 * @author Tim Mueller
 * 
 */
public class OxidationAnalyzer {
  
  private Params m_Params;
  //private final Structure m_Structure;
  private PartiallyOccupiedStructure m_DisorderedStructure;
  
  // Enumeration of different methods
  private final static int BOND_VALENCE = 0;
  private final static int LIKELY_SYM = 1;
  private final static int LIKELY_NO_SYM = 2;
  private final static int AVERAGE = 3;
  private final static int POLYANION = 4;
  
  private int m_DefaultOxidationType = POLYANION;
  
  //private double[] m_GaussianWidths;
  private double[][] m_BondValenceStates;
  private double[][] m_LikelyOxidationStates;
  private double[][] m_LikelyOxidationStatesNoSym;
  private double[][] m_AverageOxidationStates;
  private double[][] m_PolyionOxidationStates;
  
  private int[] m_BVCoordination;
  
  private double m_LikelyOxidationAverageProbability;
  private double m_LikelyOxidationNoSymAverageProbability;
  private double m_CompositionProbability = Double.NaN;
  
  private Structure.Site[][] m_PolyIonGroups;
  
  private boolean m_AllowSameElements = false;
  
  /**
   * Creates an oxidation analyze for a partially occupied structure.  The oxidation analyzer
   * will use the occupancies to calculate average bond valence sums, and from this it will
   * try to determine a nearly-charge-balanced set of integer oxidation states.
   * 
   * @param sigmaStructure  The partially disordered structure for which we are determining
   * oxidation states.
   */
  public OxidationAnalyzer(PartiallyOccupiedStructure sigmaStructure) {
    
    this(sigmaStructure, new Params(null));
    
  }
  
  /**
   * Creates an oxidation analyze for a partially occupied structure.  The oxidation analyzer
   * will use the occupancies to calculate average bond valence sums, and from this it will
   * try to determine a nearly-charge-balanced set of integer oxidation states.
   * 
   * @param sigmaStructure  The partially disordered structure for which we are determining
   * oxidation states.
   * @param params The paramters for this valence analyzer.  See the sample parameters file for 
   * explanations of what they all do.
   */
  public OxidationAnalyzer(PartiallyOccupiedStructure sigmaStructure, Params params) {
    
    m_DisorderedStructure = sigmaStructure;
    m_Params = params;
    
  }
  
  /**
   * This constructor copies parameters from the given oxidaiton analyzer
   * 
   * @param structure The structure for which we want to know the valence states.
   * @param baseAnalyzer The oxidation analyzer from which settings are to be copied.
   */
  public OxidationAnalyzer(Structure structure, OxidationAnalyzer baseAnalyzer) {
    this(structure, baseAnalyzer.m_Params);
    m_DefaultOxidationType = baseAnalyzer.m_DefaultOxidationType;
  }
  
  /**
   * This constructor just uses the default parameters.
   * 
   * @param structure The structure for which we want to know the valence states.
   */
  public OxidationAnalyzer(Structure structure) {
    this(structure, new Params(null));
  }
  
  /**
   * 
   * @param structure The structure for which we want to know the valence states.
   * @param params The paramters for this valence analyzer.  See the sample parameters file for 
   * explanations of what they all do.
   */
  public OxidationAnalyzer(Structure structure, Params params) {
    this(new PartiallyOccupiedStructure(structure), params);
  }
  
  /**
   * This constructor is just used to access default internal data
   */
  protected OxidationAnalyzer() {
    super();
  }
  
  /**
   * 
   * @return The parameters file used to configure this object.
   */
  public Params getParams() {
    return m_Params;
  }
  
  /**
   * 
   * @return The structure for which we want to know the bond valence states.
   */
  public Structure getStructure() {
    return m_DisorderedStructure.getBaseStructure();
  }
  
  public void calcBVSumForLikeElements(boolean value) {
    m_AllowSameElements = value;
  }
  
  public PartiallyOccupiedStructure assignStates(double[][] states) {
    
    if (states.length != m_DisorderedStructure.numPrimSites()) {
      throw new RuntimeException("Number of sites in structure does not match number of oxidation states.");
    }
    
    double[][] occupancies = m_DisorderedStructure.getOccupancies();
    Species[][] allowedSpecies = m_DisorderedStructure.getAllowedSpecies();
    Structure baseStructure = m_DisorderedStructure.getBaseStructure();
    
    for (int siteNum = 0; siteNum < states.length; siteNum++) {
      double[] statesForSite = states[siteNum];
      if (statesForSite.length != allowedSpecies[siteNum].length) {
        throw new RuntimeException("Number of allowed occupations for site " + siteNum + " does not match number of given oxidation states.");
      }
      for (int specNum = 0; specNum < statesForSite.length; specNum++) {
        allowedSpecies[siteNum][specNum] = allowedSpecies[siteNum][specNum].setOxidationState(statesForSite[siteNum]);
      }
    }
    
    return new PartiallyOccupiedStructure(baseStructure, allowedSpecies, occupancies);
    
  }
  
  public Structure assignStates(double[] states) {
    
    if (states.length != m_DisorderedStructure.numPrimSites()) {
      throw new RuntimeException("Number of sites in structure does not match number of oxidation states.");
    }
    
    StructureBuilder builder = new StructureBuilder(m_DisorderedStructure.getBaseStructure());
    
    for (int siteNum = 0; siteNum < states.length; siteNum++) {
      builder.setSiteSpecies(siteNum, builder.getSiteSpecies(siteNum).setOxidationState(states[siteNum]));
    }
    
    return new Structure(builder);
    
  }
  
  /**
   * 
   * @return The oxidation states as calculated using the bond valence model, where the 
   * bond valence parameter Rij is calculated as described in JASC 113, 3226.  Anions 
   * and cations for each bond are determined by comparing electronegativities.
   */
  public double[][] getDisorderedBondValenceStates() {
    
    this.calcBondValenceSums();
    
    return ArrayUtils.copyArray(m_BondValenceStates);
    
  }
  
  /**
   * 
   * @return The number of Voronoi neighbors used to calculated the bond valence sum, where only anion-cation 
   * pairs are considered.  Anions and cations for each bond are determined by comparing electronegativities.
   * The i<i>th</i> element correcponds to the i<i>th</i> defining site.
   * 
   */
  public int[] getBondValenceCoordination() {
    
    this.calcBondValenceSums();
    
    return ArrayUtils.copyArray(m_BVCoordination);
    
  }
  
  /**
   * This method uses information from the ICSD, Wikipedia, and the bond valence model
   * to assign a probability to a given oxidation state for a given atom.  The most likely 
   * combination of oxidation states that preserves charge neutrality is maintained.  In addition
   * to the charge neutrality constraint, 
   * 
   * @param useSymmetry Whether or not the oxidation states should be constrained so that 
   * symmetrically equivalent sites have the same oxidation states. 
   * @return The oxidation states for the atoms in the given structure, where the nth element of 
   * the array are the oxidation states for the alloewd sepcies in the nth defining site in the structure.
   */
  public double[][] getDisorderedLikelyOxidationStates(boolean useSymmetry) {
    
    if (useSymmetry) {
      this.calcLikelyOxidationStatesSym();
      return ArrayUtils.copyArray(m_LikelyOxidationStates);
    } else {
      this.calcLikelyOxidationStatesNoSym();
      return ArrayUtils.copyArray(m_LikelyOxidationStatesNoSym);
    }
    
  }
  
  /**
   * This method provides a metric of the degree to which the most likely charge-neutral combination of
   * oxidation states differs from the most likely individual oxidation states.  
   * 
   * @param useSymmetry Whether or not the oxidation states should be constrained so that 
   * symmetrically equivalent sites have the same oxidation states. 
   * @return A score between zero (not inclusive) and one (inclusive).  A score of one means that in the 
   * most likely charge-neutral combination of oxidation states, all atoms are in their most likely 
   * individual states.  The lower the score, the greater the degree to which the atoms needed to be 
   * placed in low-probability oxidation states to maintain charge balance.
   * 
   */
  public double getLikelyOxidationStateScore(boolean useSymmetry) {
    
    if (useSymmetry) {
      this.calcLikelyOxidationStatesSym();
      return m_LikelyOxidationAverageProbability;
    } else {
      this.calcLikelyOxidationStatesNoSym();
      return m_LikelyOxidationNoSymAverageProbability;
    }
    
  }
  
  /**
   * This method provides a metric of the minimum degree to which a charge-neutral combination of
   * oxidation states must differ from the most likely individual oxidation states.  The bond valence sums
   * are only used to determine the sign of the charge and the weights of zero oxidation states. 
   * 
   * @return A score between zero (not inclusive) and one (inclusive).  A score of one means that in the 
   * most likely charge-neutral combination of oxidation states, all atoms are in their most likely 
   * individual states.  The lower the score, the greater the degree to which the atoms needed to be 
   * placed in low-probability oxidation states to maintain charge balance.
   * 
   */
  public double getCompositionScore() {

    this.calcCompositionScore();
    return m_CompositionProbability;
    
  }
  
  
  /**
   * 
   * @return True if the default method is the bond valence method
   */
  public boolean usesBondValenceAsDefault() {
    return (m_DefaultOxidationType == BOND_VALENCE);
  }
  
  /**
   * 
   * @return True if the default method is likely oxidation states with symmetry
   */
  public boolean usesLikelyAsDefault() {
    return (m_DefaultOxidationType == LIKELY_SYM);
  }
  

  /**
   * 
   * @return True if the default method is likely oxidation states with no symmetry
   */
  public boolean usesLikelyNoSymAsDefault() {
    return (m_DefaultOxidationType == LIKELY_NO_SYM);
  }
  

  /**
   * 
   * @return True if the default method is average oxidation states.
   */
  public boolean usesAverageAsDefault() {
    return (m_DefaultOxidationType == AVERAGE);
  }
  
  /**
   * Set the default method to the bond valence method.
   *
   */
  public void useBondValenceAsDefault() {
    m_DefaultOxidationType = BOND_VALENCE;
  }
  
  /**
   * Set the default method to likely oxidation states with symmetry.
   *
   */
  public void useLikelyAsDefault() {
    m_DefaultOxidationType = LIKELY_SYM;
  }
  
  /**
   * Set the default method to likely oxidation states with no symmetry.
   *
   */
  public void useLikelyNoSymAsDefault() {
    m_DefaultOxidationType = LIKELY_NO_SYM;
  }
  
  /**
   * Set the default method to average oxidation states.
   *
   */
  public void useAverageAsDefault() {
    m_DefaultOxidationType = AVERAGE;
  }
  
  /**
   * Set the default method to polyanion oxidation states.
   *
   */
  public void usePolyanionAsDefault() {
    m_DefaultOxidationType = POLYANION;
  }
  
  /**
   * 
   * @return The same as getDisorderedOxidationStates, but only returns the oxidation states
   * for the first element allowed at each site.  This is included for backwards-compatibility with 
   * the oxidation analyzer that did not allow disorder.
   */
  public double[] getOxidationStates() {
    
    switch(m_DefaultOxidationType) {
       case BOND_VALENCE: return this.getBondValenceStates();
       case LIKELY_SYM: return this.getLikelyOxidationStates(true);
       case LIKELY_NO_SYM: return this.getLikelyOxidationStates(false);
       case AVERAGE: return this.getAverageOxidationStates();
       case POLYANION: return this.getPolyionOxidationStates();
    }
    
    return null;   
  }
  
  /**
   * 
   * @return The oxidation states for the atoms in the given structure, where the nth element of 
   * the array are the oxidation states for the alloewd sepcies in the nth defining site in the structure.
   */
  public double[][] getDisorderedOxidationStates() {
    
    switch(m_DefaultOxidationType) {
       case BOND_VALENCE: return this.getDisorderedBondValenceStates();
       case LIKELY_SYM: return this.getDisorderedLikelyOxidationStates(true);
       case LIKELY_NO_SYM: return this.getDisorderedLikelyOxidationStates(false);
       case AVERAGE: return this.getDisorderedAverageOxidationStates();
    }
    
    return null;   
  }
  
  /**
   * 
   * @return The score for the oxidation states generated using the default method, or Double.NaN
   * if not such score exists.
   */
  public double getOxidationStateScore() {
    
    switch(m_DefaultOxidationType) {
       case BOND_VALENCE: return Double.NaN;
       case LIKELY_SYM: return this.getLikelyOxidationStateScore(true);
       case LIKELY_NO_SYM: return this.getLikelyOxidationStateScore(false);
       case AVERAGE: return this.getLikelyOxidationStateScore(false);
    }
    
    return Double.NaN;   
  }
  
  /**
   * @param useSym Whether symmetry should be taken into account when calculating the states
   * 
   * @return The same as getDisorderedLikelyOxidationStates(boolean), but only returns the oxidation states
   * for the first element allowed at each site.  This is included for backwards-compatibility with 
   * the oxidation analyzer that did not allow disorder.
   * 
   */
  public double[] getLikelyOxidationStates(boolean useSym) {
    
    if (useSym) {
      this.calcLikelyOxidationStatesSym();
      double[] returnArray = new double[m_LikelyOxidationStates.length];
      for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
        returnArray[siteNum] = m_LikelyOxidationStates[siteNum][0];
      }
      return returnArray;
    } 
    
    this.calcLikelyOxidationStatesNoSym();
    double[] returnArray = new double[m_LikelyOxidationStatesNoSym.length];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = m_LikelyOxidationStatesNoSym[siteNum][0];
    }
    return returnArray;
    
  }
  
  /**
   * 
   * @return The same as getDisorderedBondValenceStates, but only returns the oxidation states
   * for the first element allowed at each site.  This is included for backwards-compatibility with 
   * the oxidation analyzer that did not allow disorder.
   */
  public double[] getBondValenceStates() {
    this.calcBondValenceSums();
    double[] returnArray = new double[m_BondValenceStates.length];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = m_BondValenceStates[siteNum][0];
    }
    return returnArray;
  }
  
  /**
   * @return The same as getDisorderedAverageOxidationStates, but only returns the oxidation states
   * for the first element allowed at each site.  This is included for backwards-compatibility with 
   * the oxidation analyzer that did not allow disorder.
   */
  public double[] getAverageOxidationStates() {
    this.calcAverageOxidationStates();
    double[] returnArray = new double[m_AverageOxidationStates.length];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = m_AverageOxidationStates[siteNum][0];
    }
    return returnArray;
  }
  
  /**
   * @return The same as getDisorderedPolyionOxidationStates, but only returns the oxidation states
   * for the first element allowed at each site.  This is included for backwards-compatibility with 
   * the oxidation analyzer that did not allow disorder.
   */
  public double[] getPolyionOxidationStates() {
    this.calcPolyionOxidationStates();
    double[] returnArray = new double[m_PolyionOxidationStates.length];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = m_PolyionOxidationStates[siteNum][0];
    }
    return returnArray;
  }
  
  /**
   * @return The same as getDisorderedAverageOxidationStates, but the oxidation states are averaged
   * across all atoms in each polyion, where a polyion is defined as a group of fully occupied sites 
   * with the same species in which each site in the group bound to another site in the group.  Two
   * sites are considered bound to each other if the bond valence between them is greater than the 
   * maximum bond valence for bonds including one of the sites times POLYION_NEIGHBOR_BV_FACTOR.
   */
  public double[][] getDisorderedPolyionOxidationStates() {
    this.calcPolyionOxidationStates();
    return ArrayUtils.copyArray(m_PolyionOxidationStates);
  }
  
  /**
   * This method evaluates the likely valence states for all sites with symmetry turned off.  The
   * sites are then grouped so that all sites with the same element and bond-valence value are
   * placed in the same group.  The average valence state across each group is returned for each
   * site in the group.
   * 
   * It should be noted that the grouping is <i>not</i> among symmetrically equivalent sites.  For
   * the purpose of calculating likely valence states, sites with the same element and bond valence
   * value are indistinguishable, and thus are treated as equivalent.  In real life, it is extremely
   * unlikely for symmetrically distinct sites to have the same bond valence value, but for 
   * artificially constructed unrelaxed structures it is possible.
   * 
   * @return The oxidation states for the atoms in the given structure, where the nth element of 
   * the array are the oxidation states for the allowed sepcies in the nth defining site in the structure.
   */
  public double[][] getDisorderedAverageOxidationStates() {
    
    this.calcAverageOxidationStates();
    return ArrayUtils.copyArray(m_AverageOxidationStates);
    
  }
  
  protected void calcCompositionScore() {
    
    if (! Double.isNaN(m_CompositionProbability)) {return;}
    
    double[][] bvSums = this.getDisorderedBondValenceStates();
    
    Element[] elements = m_DisorderedStructure.getAllAllowedElements();
    double[] anionOccupancies = new double[elements.length];
    double[] cationOccupancies = new double[elements.length];
    double[] avgAnionBVSums = new double[elements.length];
    double[] avgCationBVSums = new double[elements.length];
    
    for (int siteNum = 0; siteNum < m_DisorderedStructure.numDefiningSites(); siteNum++) {
      for (int specNum = 0; specNum < m_DisorderedStructure.numAllowedSpecies(siteNum); specNum++) {
        double bvSum = bvSums[siteNum][specNum];
        if (bvSum == 0) {continue;}
        double[] occupancies = (bvSum < 0) ? anionOccupancies : cationOccupancies;
        double[] avgBVSums = (bvSum < 0) ? avgAnionBVSums : avgCationBVSums;
        Element element = m_DisorderedStructure.getSiteSpecies(siteNum, specNum).getElement();
        for (int elNum = 0; elNum < elements.length; elNum++) {
          if (elements[elNum] == element) {
            occupancies[elNum] += m_DisorderedStructure.getSiteOccupancy(siteNum, specNum);
            avgBVSums[elNum] += bvSum * m_DisorderedStructure.getSiteOccupancy(siteNum, specNum);
            break;
          }
        }
      }
    }
    
    for (int elNum = 0; elNum < elements.length; elNum++) {
      avgAnionBVSums[elNum] /= anionOccupancies[elNum];
      avgCationBVSums[elNum] /= cationOccupancies[elNum];
    }
    
    // TODO figure out a way to do this without relying on integers
    double[] allOxidationStates = new double[0];
    double[] allProbabilities = new double[0];
    int[] numStates = new int[0];
    int[] setLabels = new int[0];
    int currSetNum = 0;
    for (int elNum = 0; elNum < anionOccupancies.length; elNum++) {
      Element element = elements[elNum];
      double[] oxidationStates = this.getAllowedOxidationStates(element);
      for (int chargeSign = -1; chargeSign < 2; chargeSign += 2) {
        double[] occupancies = (chargeSign < 0) ? anionOccupancies: cationOccupancies;
        double[] avgBVSums = (chargeSign < 0) ? avgAnionBVSums : avgCationBVSums;
        int occupancy = (int) Math.round(occupancies[elNum]);
        if (occupancy == 0) {continue;}
        double[] probs = scaleToUnitMax(this.getProbabilitiesSimple(element, avgBVSums[elNum]));
        int[] set = new int[probs.length];
        int[] numAllowedStates = new int[probs.length];
        Arrays.fill(set, currSetNum++);
        Arrays.fill(numAllowedStates, occupancy + 1);
        
        allOxidationStates = ArrayUtils.appendArray(allOxidationStates, oxidationStates);
        allProbabilities = ArrayUtils.appendArray(allProbabilities, probs);
        numStates = ArrayUtils.appendArray(numStates, numAllowedStates);
        setLabels = ArrayUtils.appendArray(setLabels, set);
      }
    }
    
    int[] setSizes = new int[currSetNum];
    
    int[] map = ArrayUtils.getSortPermutation(allProbabilities);
    map = ArrayUtils.reverseArray(map);
    
    int numNonZero = 0;
    for (numNonZero = 0; numNonZero < map.length; numNonZero++) {
      if (allProbabilities[map[numNonZero]] == 0) {
        break;
      }
    }
    
    if (numNonZero == 0) {
      m_CompositionProbability = 1; // Usually No cations or anions
      return;
    }
    
    double[] sortedProbabilities = new double[numNonZero];
    double[] sortedOxidationStates = new double[numNonZero];
    int[] sortedSetLabels = new int[numNonZero];
    int[] sortedNumStates = new int[numNonZero];
    
    for (int stateNum = 0; stateNum < numNonZero; stateNum++) {
      sortedProbabilities[stateNum] = allProbabilities[map[stateNum]];
      sortedOxidationStates[stateNum] = allOxidationStates[map[stateNum]];
      sortedNumStates[stateNum] = numStates[map[stateNum]];
      int setNum = setLabels[map[stateNum]];
      sortedSetLabels[stateNum] = setNum;
      setSizes[setNum]++;
    }
    
    ArrayIndexer.Filter[] setFilters = new ArrayIndexer.Filter[setSizes.length];
    int[][] setIndices = new int[setSizes.length][];
    
    for (int setNum = 0; setNum < setSizes.length; setNum++) {
      setIndices[setNum] = new int[setSizes[setNum]];
      int[] numStatesForSet = new int[setSizes[setNum]];
      int setIndexNum = 0;
      for (int stateNum = 0; stateNum < sortedSetLabels.length; stateNum++) {
        if (sortedSetLabels[stateNum] == setNum) {
          setIndices[setNum][setIndexNum] = stateNum;
          numStatesForSet[setIndexNum] = sortedNumStates[stateNum];
          setIndexNum++;
        }
      }
      setFilters[setNum] = new ArrayIndexer.FixedSumFilter(numStatesForSet, numStatesForSet[0] - 1);
    }
    
    ArrayIndexer.SubSetFilter elementCountFilter = new ArrayIndexer.SubSetFilter(setIndices, setFilters);
    ArrayIndexer.FixedSumFilter chargeBalanceFilter = new ArrayIndexer.FixedSumFilter(sortedNumStates, sortedOxidationStates, 0);
    //ProbabilityFilter probabilityFilter = new ProbabilityFilter(sortedProbabilities);
    ArrayIndexer.LowIndexFilter probabilityFilter = new ArrayIndexer.LowIndexFilter(sortedProbabilities.length);
    
    ArrayIndexer.Filter[] filters = new ArrayIndexer.Filter[] {elementCountFilter, chargeBalanceFilter, probabilityFilter};
    ArrayIndexer indexer = new ArrayIndexer(sortedNumStates);
    long softEndTime = System.currentTimeMillis() + m_Params.getSoftTimeout();
    long hardEndTime = System.currentTimeMillis() + m_Params.getHardTimeout();
    int[] currentState = indexer.getInitialState(filters, (hardEndTime - System.currentTimeMillis()));
    int[] bestState = ArrayUtils.copyArray(currentState);
    if (currentState != null) {
      probabilityFilter.setBestState(currentState);
      while (indexer.increment(currentState, filters, (hardEndTime - System.currentTimeMillis()))) {
        if (System.currentTimeMillis() > softEndTime) {
          Status.warning("Reached soft timeout of " + m_Params.getSoftTimeout() + " milliseconds when searching for likely oxidation states without symmetry.");
          Status.warning("Using most likely oxidation states found so far.");
          break;
        }
        probabilityFilter.setBestState(currentState);
        System.arraycopy(currentState, 0, bestState, 0, currentState.length);
      }
      int maxOccupiedIndex = probabilityFilter.getMaxOccupiedIndex();
      m_CompositionProbability = (maxOccupiedIndex < 0) ? 0 : sortedProbabilities[maxOccupiedIndex];
    }
    
    //System.currentTimeMillis();
    //m_CompositionProbability = Math.pow(probabilityFilter.getMaxProbability(), 1.0 / m_DisorderedStructure.numDefiningSites());
    
  }
  
  protected static double[] scaleToUnitMax(double[] values) {
    double maxValue = ArrayUtils.maxElement(values);
    return MSMath.arrayDivide(values, maxValue);
  }
  
  protected boolean areOccupanciesCloseEnough(double[] array1, double[] array2) {
    
    double tolerance = 1.01;
    if ((array1 == null) || (array2 == null)) {
      return false;
    }
    if (array1.length != array2.length) {
      return false;
    }
    for (int i = 0; i < array1.length; i++) {
      double ratio = (array1[i] / array2[i]);
      if ((ratio > tolerance)  || (1/ratio > tolerance)) {
        return false;
      }
    }
    
    return true;
    
  }
  
  protected Structure.Site[][] groupEquivalentSitesByAllowedSpeciesAndOccupancy() {
    
    Structure structure = this.getStructure();
    Structure.Site[][] equivalentSites = structure.groupEquivalentSites();
    Structure.Site[][] returnArray = new Structure.Site[0][];
    for (int groupNum = 0; groupNum < equivalentSites.length; groupNum++) {
      Structure.Site[] group = equivalentSites[groupNum];
      Structure.Site[][] newGroups = this.groupSitesByAllowedSpeciesAndOccupancy(group);
      if (newGroups.length > 1) {
        Status.warning("Equivalent sites have different allowed species and occupancies.");
      }
      returnArray = (Structure.Site[][]) ArrayUtils.appendArray(returnArray, newGroups);
    }
    return returnArray;
  }
  
  protected Structure.Site[][] groupSitesByAllowedSpeciesAndOccupancy() {
    return this.groupSitesByAllowedSpeciesAndOccupancy(m_DisorderedStructure.getBaseStructure().getDefiningSites());
  }
  
  protected Structure.Site[][] groupSitesByAllowedSpeciesAndOccupancy(Structure.Site[] sites) {
    
    //Structure structure = this.getStructure();
    
    Species[][] distinctSpecieSets = new Species[sites.length][];
    double[][] distinctOccSets = new double[sites.length][];
    
    int[] numSitesPerSpecieSet = new int[sites.length];
    int numSpecieSets = 0;
    for (int siteNum = 0; siteNum < sites.length; siteNum++) {
      int siteIndex = sites[siteNum].getIndex();
      Species[] specSet = m_DisorderedStructure.getAllowedSpecies(siteIndex);
      double[] occSet = m_DisorderedStructure.getSiteOccupancies(siteIndex);
      for (int prevSpecNum = 0; prevSpecNum < distinctSpecieSets.length; prevSpecNum++) {
        if (Arrays.equals(distinctSpecieSets[prevSpecNum], specSet) && this.areOccupanciesCloseEnough(occSet, distinctOccSets[prevSpecNum])) {
          numSitesPerSpecieSet[prevSpecNum]++;
          break;
        }
        if (distinctSpecieSets[prevSpecNum] == null) {
          distinctSpecieSets[prevSpecNum] = specSet;
          distinctOccSets[prevSpecNum] = occSet;
          numSitesPerSpecieSet[prevSpecNum]++;
          numSpecieSets = prevSpecNum+1;
          break;
        }
      }
    }
    
    Structure.Site[][] returnArray = new Structure.Site[numSpecieSets][];
    for (int specNum = 0; specNum < returnArray.length; specNum++) {
      returnArray[specNum] = new Structure.Site[numSitesPerSpecieSet[specNum]];
    }
    int[] specIndices = new int[returnArray.length];
    
    for (int siteNum = 0; siteNum < sites.length; siteNum++) {
      int siteIndex = sites[siteNum].getIndex();
      Species[] specSet = m_DisorderedStructure.getAllowedSpecies(siteIndex);
      double[] occSet = m_DisorderedStructure.getSiteOccupancies(siteIndex);
      for (int specNum = 0; specNum < returnArray.length; specNum++) {
        if (Arrays.equals(distinctSpecieSets[specNum], specSet) && this.areOccupanciesCloseEnough(occSet, distinctOccSets[specNum])) {
          returnArray[specNum][specIndices[specNum]] = sites[siteNum];
          specIndices[specNum]++;
        }
      }
    }
    
    return returnArray;
    
  }
  
  protected Structure.Site[][] groupSitesByBondValence(double tolerance) {
    
    this.calcBondValenceSums();
    //double tolerance = m_Params.getLikelyOxidationNoSymBondValenceTolerance();
    //double tolerance = 1E-4;
    
    Structure.Site[][] sitesByAllowedSpecies = this.groupSitesByAllowedSpeciesAndOccupancy();
    Structure.Site[][] returnArray = new Structure.Site[0][];
    
    for (int orbitNum = 0; orbitNum < sitesByAllowedSpecies.length; orbitNum++) {
      
      Structure.Site[] sites = sitesByAllowedSpecies[orbitNum];
      double[][] bondValences = new double[sites.length][];
      double[] firstValences = new double[sites.length];
      for (int siteNum = 0; siteNum < bondValences.length; siteNum++) {
        bondValences[siteNum] = ArrayUtils.copyArray(m_BondValenceStates[sites[siteNum].getIndex()]);
        firstValences[siteNum] = bondValences[siteNum][0];
      }
      
      int[] sortOrder = ArrayUtils.getSortPermutation(firstValences);
      
      double[] lastStates = new double[bondValences[0].length];
      Arrays.fill(lastStates, Double.NEGATIVE_INFINITY);
      int numGroups = 0;
      for (int siteNum = 0; siteNum < sortOrder.length; siteNum++) {
        int siteIndex = sortOrder[siteNum];
        double[] bondValenceStates = bondValences[siteIndex];
        boolean delta = false;
        for (int stateNum = 0; stateNum < bondValenceStates.length; stateNum++) {
          double bvState = bondValenceStates[stateNum];
          double lastState = lastStates[stateNum];
          if ((bvState - lastState > tolerance) || ((bvState > 0) && (lastState < 0))) { // Split anions and cations
            delta = true;
          }
        }
        if (delta) {
          numGroups++;
          lastStates = bondValenceStates;
        }
      }
      
      int[] numGroupsPerSite = new int[numGroups];
      Structure.Site[][] siteGroups = new Structure.Site[numGroups][];
      lastStates = new double[bondValences[0].length];
      Arrays.fill(lastStates, Double.NEGATIVE_INFINITY);
      int groupNum = -1;
      for (int siteNum = 0; siteNum < sortOrder.length; siteNum++) {
        int siteIndex = sortOrder[siteNum];
        double[] bondValenceStates = bondValences[siteIndex];
        boolean delta = false;
        for (int stateNum = 0; stateNum < bondValenceStates.length; stateNum++) {
          double bvState = bondValenceStates[stateNum];
          double lastState = lastStates[stateNum];
          if ((bvState - lastState > tolerance) || ((bvState > 0) && (lastState < 0))) { // Split anions and cations
            delta = true;
          }
        }
        if (delta) {
          groupNum++;
          lastStates = bondValenceStates;
        }
        numGroupsPerSite[groupNum]++;
      }
      
      int siteIndex = 0;
      for (groupNum = 0; groupNum < numGroupsPerSite.length; groupNum++) {
        Structure.Site[] groupSites = new Structure.Site[numGroupsPerSite[groupNum]];
        for (int groupSiteNum = 0; groupSiteNum < groupSites.length; groupSiteNum++) {
          groupSites[groupSiteNum] = sites[sortOrder[siteIndex++]];
        }
        siteGroups[groupNum] = groupSites;
      }
      
      returnArray = (Structure.Site[][]) ArrayUtils.appendArray(returnArray, siteGroups);
    }
    
    /*Structure.Site[][] siteGroups = new Structure.Site[0][];
    for (int siteNum = 0; siteNum < sortOrder.length; siteNum++) {
      int siteIndex = sortOrder[siteNum];
      Structure.Site site = m_Structure.getDefiningSite(siteIndex);
      double bondValenceState = m_BondValenceStates[siteIndex];
      boolean knownGroup = false;
      for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
        Structure.Site knownSite = siteGroups[groupNum][0];
        double knownState = m_BondValenceStates[knownSite.getIndex()];
        if ((Math.abs(knownState - bondValenceState) < tolerance) && (site.getSpecie() == knownSite.getSpecie())) {
          knownGroup = true;
          siteGroups[groupNum] = (Structure.Site[]) ArrayUtils.appendElement(siteGroups[groupNum], site);
          break;
        }
      }
      if (!knownGroup) {
        siteGroups = (Structure.Site[][]) ArrayUtils.appendElement(siteGroups, new Structure.Site[] {site});
      }
    }*/
    return returnArray;
  }
  
  protected Structure.Site[][] groupSitesByPolyions(double minBVTerm) {
    
    if (m_PolyIonGroups != null) {
      return (Structure.Site[][]) ArrayUtils.copyArray(m_PolyIonGroups);
    }
    
    Structure structure = this.getStructure();
    
    Structure.Site[][] returnArray = new Structure.Site[0][];
    Structure.Site[] allSites = this.getStructure().getDefiningSites();
    int[] returnGroupNums = new int[allSites.length];
    Arrays.fill(returnGroupNums, -1);
    
    double searchRadius = m_Params.getBondValenceSearchRadius();
    
    for (int siteIndex = 0; siteIndex < allSites.length; siteIndex++) {
      Structure.Site site = allSites[siteIndex];
      int[] neighborGroupNums = new int[0];
      if (m_DisorderedStructure.numAllowedSpecies(site.getIndex()) == 1) { // Only group fully occupied sites
        //Structure.Site[] neighbors = structure.getNearestNeighbors(site, nnTolerance);
        Structure.Site[] neighbors = structure.getNearbySites(site.getCoords(), searchRadius, false);
        double[] bvTerms = new double[neighbors.length];

        for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
          Structure.Site neighbor = neighbors[neighborNum];
          bvTerms[neighborNum] = getBondValence(site.getSpecies().getElement(), neighbor.getSpecies().getElement(), site.distanceFrom(neighbor));
        }
        double maxBV = bvTerms[ArrayUtils.maxElementIndex(bvTerms)];
        double minAllowedBV = maxBV * minBVTerm;
        
        for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
          if (bvTerms[neighborNum] < minAllowedBV) {continue;}
          Structure.Site neighbor = neighbors[neighborNum];
          if (m_DisorderedStructure.numAllowedSpecies(neighbor.getIndex()) > 1) {continue;}
          if (m_DisorderedStructure.getAllowedSpecies(site.getIndex(), 0) != m_DisorderedStructure.getAllowedSpecies(neighbor.getIndex(), 0)) {continue;}

          int returnGroupNum = returnGroupNums[neighbor.getIndex()];
          if ((returnGroupNum >= 0) && !(ArrayUtils.arrayContains(neighborGroupNums, returnGroupNum))) {
            neighborGroupNums = ArrayUtils.appendElement(neighborGroupNums, returnGroupNums[neighbor.getIndex()]);
          }
        }
      }
      if (neighborGroupNums.length == 0) {
        returnGroupNums[siteIndex] = returnArray.length;
        returnArray = (Structure.Site[][]) ArrayUtils.appendElement(returnArray, new Structure.Site[] {site});
        continue; 
      }
      
      Arrays.sort(neighborGroupNums);
      int keeperGroupNum = neighborGroupNums[0];
      Structure.Site[] keeperGroup = returnArray[keeperGroupNum];
      for (int groupIndex = neighborGroupNums.length - 1; groupIndex > 0; groupIndex--) {
        int neighborGroupNum = neighborGroupNums[groupIndex];
        keeperGroup = (Structure.Site[]) ArrayUtils.appendArray(keeperGroup, returnArray[neighborGroupNum]);
        returnArray = (Structure.Site[][]) ArrayUtils.removeElement(returnArray, neighborGroupNum);
      }
      keeperGroup = (Structure.Site[]) ArrayUtils.appendElement(keeperGroup, site);
      returnArray[keeperGroupNum] = keeperGroup;
      for (int groupNum = 0; groupNum < returnArray.length; groupNum++) {
        Structure.Site[] group = returnArray[groupNum];
        for (int siteNum = 0; siteNum < group.length; siteNum++) {
          int returnSiteIndex = group[siteNum].getIndex();
          returnGroupNums[returnSiteIndex] = groupNum;
        }
      }
    }
    
    m_PolyIonGroups = returnArray;
    return (Structure.Site[][]) ArrayUtils.copyArray(m_PolyIonGroups);
  }
  
  protected void calcPolyionOxidationStates() {

    if (m_PolyionOxidationStates != null) {return;}
    Structure structure = this.getStructure();
    m_PolyionOxidationStates = new double[structure.numDefiningSites()][];
    for (int siteNum = 0; siteNum < m_PolyionOxidationStates.length; siteNum++) {
      m_PolyionOxidationStates[siteNum] = new double[m_DisorderedStructure.numAllowedSpecies(siteNum)];
    }
    
    this.calcAverageOxidationStates();
    
    //Structure.Site[][] siteGroups = m_Structure.groupEquivalentSites();
    //Structure.Site[][] siteGroups = this.groupSitesByBondValence(m_Params.getAverageBVTolerance());
    Structure.Site[][] siteGroups = this.groupSitesByPolyions(m_Params.getMinBVForPolyion());
    
    double[][] groupSums = new double[siteGroups.length][];
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site[] sites = siteGroups[groupNum];
      groupSums[groupNum] = new double[m_DisorderedStructure.numAllowedSpecies(sites[0].getIndex())];
      for (int siteNum = 0; siteNum < sites.length; siteNum++) {
        groupSums[groupNum] = MSMath.arrayAdd(groupSums[groupNum], m_AverageOxidationStates[sites[siteNum].getIndex()]);
      }
    }
    
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site[] sites = siteGroups[groupNum];
      double[] average = MSMath.arrayDivide(groupSums[groupNum], siteGroups[groupNum].length);
      for (int siteNum = 0; siteNum < sites.length; siteNum++) {
        m_PolyionOxidationStates[sites[siteNum].getIndex()] = average;
      }
    }
    
  }
  
  protected void calcAverageOxidationStates() {
    
    if (m_AverageOxidationStates != null) {return;}
    Structure structure = this.getStructure();
    m_AverageOxidationStates = new double[structure.numDefiningSites()][];
    for (int siteNum = 0; siteNum < m_AverageOxidationStates.length; siteNum++) {
      m_AverageOxidationStates[siteNum] = new double[m_DisorderedStructure.numAllowedSpecies(siteNum)];
    }
    
    this.calcLikelyOxidationStatesNoSym();
    
    //Structure.Site[][] siteGroups = m_Structure.groupEquivalentSites();
    Structure.Site[][] siteGroups = this.groupSitesByBondValence(m_Params.getAverageBVTolerance());
    
    double[][] groupSums = new double[siteGroups.length][];
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site[] sites = siteGroups[groupNum];
      groupSums[groupNum] = new double[m_DisorderedStructure.numAllowedSpecies(sites[0].getIndex())];
      for (int siteNum = 0; siteNum < sites.length; siteNum++) {
        groupSums[groupNum] = MSMath.arrayAdd(groupSums[groupNum], m_LikelyOxidationStatesNoSym[sites[siteNum].getIndex()]);
      }
    }
    
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site[] sites = siteGroups[groupNum];
      double[] average = MSMath.arrayDivide(groupSums[groupNum], siteGroups[groupNum].length);
      for (int siteNum = 0; siteNum < sites.length; siteNum++) {
        m_AverageOxidationStates[sites[siteNum].getIndex()] = average;
      }
    }
    
  }
  
  protected void calcLikelyOxidationStatesSym() {

    if (m_LikelyOxidationStates != null) {return;}
    Structure structure = this.getStructure();
    //Structure.Site[][] siteGroups = structure.groupEquivalentSites();
    Structure.Site[][] siteGroups = m_DisorderedStructure.groupSitesBySublattice();
    
    int numSiteSpecGroups = 0;
    int numTotalSiteSpecs = 0;
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site site = siteGroups[groupNum][0];
      numSiteSpecGroups += m_DisorderedStructure.numAllowedSpecies(site.getIndex());
      numTotalSiteSpecs += m_DisorderedStructure.numAllowedSpecies(site.getIndex()) * siteGroups[groupNum].length;
    }
    
    Structure.Site[][] spreadSiteGroups = new Structure.Site[numSiteSpecGroups][];
    Species[] spreadAllowedSpecies = new Species[numSiteSpecGroups];
    int[] allowedSpecIndices = new int[numSiteSpecGroups];
    int siteSpecGroupNum = 0;
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site site = siteGroups[groupNum][0];
      Species[] allowedSpecies = m_DisorderedStructure.getAllowedSpecies(site.getIndex());
      for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {
        spreadSiteGroups[siteSpecGroupNum] = siteGroups[groupNum];
        spreadAllowedSpecies[siteSpecGroupNum] = allowedSpecies[specNum];
        allowedSpecIndices[siteSpecGroupNum] = specNum;
        siteSpecGroupNum++;
      }
    }
    siteGroups = spreadSiteGroups;
    
    double[] minOxidationStates = new double[numSiteSpecGroups];
    double[][] shiftedOxidationStates = new double[numSiteSpecGroups][];
    double[][] probabilities = new double[numSiteSpecGroups][];
    int[] numStates = new int[numSiteSpecGroups];
    int totalNumSites = 0;
    double shiftedNeutrality = 0;
    double oxidationTolerance = 0.01;
    
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site site = siteGroups[groupNum][0];
      Species spec = spreadAllowedSpecies[groupNum];
      int specIndex = allowedSpecIndices[groupNum];
      int atomicNumber = spec.getElement().getAtomicNumber();
      
      if (atomicNumber < 0 || atomicNumber >= m_OxidationStates.length) {
        throw new RuntimeException("I don't know how to find likely valence states for atomic number " + atomicNumber + ".");
      }
      
      int sitesPerGroup = siteGroups[groupNum].length;
      
      double[] initProbabilities = this.getProbabilities(site)[specIndex];
      
      for (int stateNum = 0; stateNum < initProbabilities.length; stateNum++) {
        initProbabilities[stateNum] = Math.pow(initProbabilities[stateNum], sitesPerGroup); // To be consistent with translational-symmetry-only version
      }
      double[] initOxidationStates = getAllowedOxidationStates(spec.getElement());
      double occupancy = m_DisorderedStructure.getSiteOccupancy(site.getIndex(),specIndex);
      initOxidationStates = MSMath.arrayMultiply(initOxidationStates, occupancy);
      
      numStates[groupNum] = initProbabilities.length;
      
      int[] map = ArrayUtils.getSortPermutation(initProbabilities);
      probabilities[groupNum] = new double[initProbabilities.length];
      shiftedOxidationStates[groupNum] = new double[initProbabilities.length];
      double maxProb = initProbabilities[ArrayUtils.maxElementIndex(initProbabilities)];
      double minState = initOxidationStates[ArrayUtils.minElementIndex(initOxidationStates)];
      
      totalNumSites += sitesPerGroup;
      
      for (int stateNum = 0; stateNum < map.length; stateNum++) {
        int reverseIndex = map.length - stateNum - 1;
        probabilities[groupNum][reverseIndex] = initProbabilities[map[stateNum]] / maxProb;
        shiftedOxidationStates[groupNum][reverseIndex] = (initOxidationStates[map[stateNum]] - minState) * sitesPerGroup;
      }
      shiftedNeutrality -= minState * sitesPerGroup;
      minOxidationStates[groupNum] = minState;
    }
    
    // We want to put the toss-up valences early in the array
    double[] secondChoices = new double[probabilities.length];
    for (int groupNum = 0; groupNum < secondChoices.length; groupNum++) {
      secondChoices[groupNum] = (probabilities[groupNum].length > 1) ? probabilities[groupNum][1] : 0;
    }
    int[] groupSortMap = ArrayUtils.getSortPermutation(secondChoices);
    Structure.Site[][] sortedSiteGroups = new Structure.Site[siteGroups.length][];
    double[][] sortedProbabilities =new double[probabilities.length][];
    double[][] sortedShiftedStates = new double[shiftedOxidationStates.length][];
    double[] sortedMinStates = new double[minOxidationStates.length];
    int[] sortedNumStates = new int[numStates.length];
    Species[] sortedAllowedSpecies = new Species[spreadAllowedSpecies.length];
    int[] sortedAllowedSpecIndices = new int[allowedSpecIndices.length];
    for (int groupNum = 0; groupNum < groupSortMap.length; groupNum++) {
      int sourceIndex = groupSortMap[groupSortMap.length - groupNum - 1];
      sortedSiteGroups[groupNum] = siteGroups[sourceIndex];
      sortedProbabilities[groupNum] = probabilities[sourceIndex];
      sortedShiftedStates[groupNum] = shiftedOxidationStates[sourceIndex];
      sortedMinStates[groupNum] = minOxidationStates[sourceIndex];
      sortedNumStates[groupNum] = numStates[sourceIndex];
      sortedAllowedSpecies[groupNum] = spreadAllowedSpecies[sourceIndex];
      sortedAllowedSpecIndices[groupNum] = allowedSpecIndices[sourceIndex];
    }
    siteGroups = sortedSiteGroups;
    probabilities = sortedProbabilities;
    shiftedOxidationStates = sortedShiftedStates;
    minOxidationStates = sortedMinStates;
    numStates = sortedNumStates;
    spreadAllowedSpecies = sortedAllowedSpecies;
    allowedSpecIndices = sortedAllowedSpecIndices;
    
    ArrayIndexer indexer = new ArrayIndexer(numStates);
    int[] stateIndices = indexer.getInitialState();
    int[] minIndices = new int[stateIndices.length];
    Arrays.fill(minIndices, -1);
    
    double maxProbability = 0;
    long startTime = System.currentTimeMillis();
    long softTimeOut = m_Params.getSoftTimeout();
    long hardTimeOut = m_Params.getHardTimeout();
    do {
      double shiftedOxidation = 0;
      double probability = 1;
      boolean legal = true;
      //for (int groupNum = 0; groupNum < stateIndices.length; groupNum++) {
      for (int groupNum = stateIndices.length - 1; groupNum >= 0; groupNum--) {
        int stateNum = stateIndices[groupNum];
        double shiftedOxidationState = shiftedOxidationStates[groupNum][stateNum];
        shiftedOxidation += shiftedOxidationState;
        if (shiftedOxidation - oxidationTolerance > shiftedNeutrality) {
          indexer.branchEnd(groupNum, stateIndices);
          break;
        }
        probability *= probabilities[groupNum][stateNum];
        /*if ((probability == 0) && (probabilities[groupNum][stateNum] != 0)) {
          System.out.println("Numerical problem");
        }*/
        if ((probability < maxProbability) || probability == 0) {
        //if (probability < maxProbability) {
          indexer.branchEnd(groupNum, stateIndices);
          break;
        }
      }
      if (legal && (probability > maxProbability) && (Math.abs(shiftedOxidation - shiftedNeutrality) < oxidationTolerance)) {
        maxProbability = probability;
        System.arraycopy(stateIndices, 0, minIndices, 0, stateIndices.length);
      }
      long elapsedTime = System.currentTimeMillis() - startTime;
      if (elapsedTime > hardTimeOut) {
        Status.warning("Reached hard timeout of " + m_Params.getHardTimeout() + " milliseconds when searching for likely oxidation states.");
        Status.warning("Using most likely oxidation states found so far.");
        break;
      }
      if (elapsedTime > softTimeOut) {
        Status.warning("Reached soft timeout of " + softTimeOut + " milliseconds when searching for likely oxidation states.");
        if (minIndices[0] >= 0) {
          Status.warning("Using most likely oxidation states found so far.");
          break;
        }
        Status.warning("No oxidation states found so far.  Resetting time counter.");
        startTime = System.currentTimeMillis();
        hardTimeOut -= softTimeOut;
      }
    } while (indexer.increment(stateIndices));
    
    if (minIndices[0] < 0) {
      throw new RuntimeException("Could not find valid oxidation states");
    }
    
    m_LikelyOxidationStates = new double[structure.numDefiningSites()][];
    for (int siteNum = 0; siteNum < m_LikelyOxidationStates.length; siteNum++) {
      m_LikelyOxidationStates[siteNum] = new double[m_DisorderedStructure.numAllowedSpecies(siteNum)];
    }
    m_LikelyOxidationAverageProbability = Math.pow(maxProbability, 1.0 / numTotalSiteSpecs);
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site[] sites = siteGroups[groupNum];
      int stateNum = minIndices[groupNum];
      int specIndex = allowedSpecIndices[groupNum];
      
      for (int siteNum = 0; siteNum < sites.length; siteNum++) {
        Structure.Site site = sites[siteNum];
        double occupancy = m_DisorderedStructure.getSiteOccupancy(site.getIndex(), specIndex);
        double oxidationState = Math.round(((shiftedOxidationStates[groupNum][stateNum] / siteGroups[groupNum].length) + minOxidationStates[groupNum]) / occupancy);
        m_LikelyOxidationStates[site.getIndex()][specIndex] = oxidationState;
      }
    }
  }
  
  protected void calcLikelyOxidationStatesNoSym() {
    
    if (m_LikelyOxidationStatesNoSym != null) {return;}
    Structure structure = this.getStructure();
    Structure.Site[][] siteGroups = this.groupSitesByBondValence(m_Params.getLikelyNoSymBVTolerance());
    
    int numSiteSpecGroups = 0;
    int numTotalSiteSpecs = 0;
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site site = siteGroups[groupNum][0];
      numSiteSpecGroups += m_DisorderedStructure.numAllowedSpecies(site.getIndex());
      numTotalSiteSpecs += m_DisorderedStructure.numAllowedSpecies(site.getIndex()) * siteGroups[groupNum].length;
    }
    
    Structure.Site[][] spreadSiteGroups = new Structure.Site[numSiteSpecGroups][];
    Species[] spreadAllowedSpecies = new Species[numSiteSpecGroups];
    int[] allowedSpecIndices = new int[numSiteSpecGroups];
    int siteSpecGroupNum = 0;
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site site = siteGroups[groupNum][0];
      Species[] allowedSpecies = m_DisorderedStructure.getAllowedSpecies(site.getIndex());
      for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {
        spreadSiteGroups[siteSpecGroupNum] = siteGroups[groupNum];
        spreadAllowedSpecies[siteSpecGroupNum] = allowedSpecies[specNum];
        allowedSpecIndices[siteSpecGroupNum] = specNum;
        siteSpecGroupNum++;
      }
    }
    siteGroups = spreadSiteGroups;
    
    double[] minOxidationStates = new double[numSiteSpecGroups];
    double[][] shiftedOxidationStates = new double[numSiteSpecGroups][];
    double[][] probabilities = new double[numSiteSpecGroups][];
    int[] numStates = new int[numSiteSpecGroups];
    int totalNumSiteSpecs = 0;
    double shiftedNeutrality = 0;
    double oxidationTolerance = m_Params.getOxidationTolerance();

    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site site = siteGroups[groupNum][0];
      Species spec = spreadAllowedSpecies[groupNum];
      int specIndex = allowedSpecIndices[groupNum];
      int atomicNumber = spec.getElement().getAtomicNumber();
      
      if (atomicNumber < 0 || atomicNumber >= m_OxidationStates.length) {
        throw new RuntimeException("I don't know how to find likely valence states for atomic number " + atomicNumber + ".");
      }
      
      int sitesPerGroup = siteGroups[groupNum].length;
      
      double[] initProbabilities = this.getProbabilities(site)[specIndex];
      double[] initOxidationStates = getAllowedOxidationStates(spec.getElement());
      double occupancy = m_DisorderedStructure.getSiteOccupancy(site.getIndex(),specIndex);
      initOxidationStates = MSMath.arrayMultiply(initOxidationStates, occupancy);
      
      numStates[groupNum] = initProbabilities.length;
      
      int[] map = ArrayUtils.getSortPermutation(initProbabilities);
      probabilities[groupNum] = new double[initProbabilities.length];
      shiftedOxidationStates[groupNum] = new double[initProbabilities.length];
      double maxProb = initProbabilities[ArrayUtils.maxElementIndex(initProbabilities)];
      double minState = initOxidationStates[ArrayUtils.minElementIndex(initOxidationStates)];
      
      totalNumSiteSpecs += sitesPerGroup;
      for (int stateNum = 0; stateNum < map.length; stateNum++) {
        int reverseIndex = map.length - stateNum - 1;
        probabilities[groupNum][reverseIndex] = initProbabilities[map[stateNum]] / maxProb;
        shiftedOxidationStates[groupNum][reverseIndex] = initOxidationStates[map[stateNum]] - minState;
      }
      shiftedNeutrality -= minState * sitesPerGroup;
      minOxidationStates[groupNum] = minState;
    }
    
    // We want to put the toss-up valences early in the array
    double[] secondChoices = new double[probabilities.length];
    for (int groupNum = 0; groupNum < secondChoices.length; groupNum++) {
      secondChoices[groupNum] = (probabilities[groupNum].length > 1) ? probabilities[groupNum][1] : 0;
    }
    int[] groupSortMap = ArrayUtils.getSortPermutation(secondChoices);
    Structure.Site[][] sortedSiteGroups = new Structure.Site[siteGroups.length][];
    double[][] sortedProbabilities =new double[probabilities.length][];
    double[][] sortedShiftedStates = new double[shiftedOxidationStates.length][];
    double[] sortedMinStates = new double[minOxidationStates.length];
    int[] sortedNumStates = new int[numStates.length];
    Species[] sortedAllowedSpecies = new Species[spreadAllowedSpecies.length];
    int[] sortedAllowedSpecIndices = new int[allowedSpecIndices.length];
    for (int groupNum = 0; groupNum < groupSortMap.length; groupNum++) {
      int sourceIndex = groupSortMap[groupSortMap.length - groupNum - 1];
      sortedSiteGroups[groupNum] = siteGroups[sourceIndex];
      sortedProbabilities[groupNum] = probabilities[sourceIndex];
      sortedShiftedStates[groupNum] = shiftedOxidationStates[sourceIndex];
      sortedMinStates[groupNum] = minOxidationStates[sourceIndex];
      sortedNumStates[groupNum] = numStates[sourceIndex];
      sortedAllowedSpecies[groupNum] = spreadAllowedSpecies[sourceIndex];
      sortedAllowedSpecIndices[groupNum] = allowedSpecIndices[sourceIndex];
    }
    siteGroups = sortedSiteGroups;
    probabilities = sortedProbabilities;
    shiftedOxidationStates = sortedShiftedStates;
    minOxidationStates = sortedMinStates;
    numStates = sortedNumStates;
    spreadAllowedSpecies = sortedAllowedSpecies;
    allowedSpecIndices = sortedAllowedSpecIndices;
    
    // If we're not using symmetry, flatten the site groups
    int[] groupIndices = null;
    groupIndices = new int[totalNumSiteSpecs];
    Structure.Site[][] flattenedSiteGroups = new Structure.Site[totalNumSiteSpecs][1];
    double[][] flattenedProbabilities =new double[totalNumSiteSpecs][];
    double[][] flattenedShiftedStates = new double[totalNumSiteSpecs][];
    double[] flattenedMinStates = new double[totalNumSiteSpecs];
    int[] flattenedNumStates = new int[totalNumSiteSpecs];
    Species[] flattenedAllowedSpecies = new Species[totalNumSiteSpecs];
    int[] flattenedSpecIndices = new int[totalNumSiteSpecs];
    int totalSiteNum = 0;
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site[] sites = siteGroups[groupNum];
      for (int siteNum = 0; siteNum < sites.length; siteNum++) {
        groupIndices[totalSiteNum] = groupNum;
        flattenedSiteGroups[totalSiteNum][0] = sites[siteNum];
        flattenedProbabilities[totalSiteNum] = probabilities[groupNum];
        flattenedShiftedStates[totalSiteNum] = shiftedOxidationStates[groupNum];
        flattenedMinStates[totalSiteNum] = minOxidationStates[groupNum];
        flattenedNumStates[totalSiteNum] = numStates[groupNum];
        flattenedAllowedSpecies[totalSiteNum] = spreadAllowedSpecies[groupNum];
        flattenedSpecIndices[totalSiteNum] = allowedSpecIndices[groupNum];
        totalSiteNum++;
      }
    }
    siteGroups = flattenedSiteGroups;
    probabilities = flattenedProbabilities;
    shiftedOxidationStates = flattenedShiftedStates;
    minOxidationStates = flattenedMinStates;
    numStates = flattenedNumStates;
    spreadAllowedSpecies = flattenedAllowedSpecies;
    allowedSpecIndices = flattenedSpecIndices;
      
    ArrayIndexer indexer = new ArrayIndexer(numStates);
    int[] stateIndices = indexer.getInitialState();
    int[] minIndices = new int[stateIndices.length];
    Arrays.fill(minIndices, -1);
    
    //double maxProbability = (m_LikelyOxidationStates == null) ? 0 : Math.pow(m_LikelyOxidationAverageProbability, m_Structure.numDefiningSites()) / m_Params.getLikelyNoSymSeedFactor();
    double maxProbability = 0;
    long startTime = System.currentTimeMillis();
    long softTimeOut = m_Params.getSoftTimeout();
    long hardTimeOut = m_Params.getHardTimeout();
    do {
      double shiftedOxidation = 0;
      double probability = 1;
      boolean legal = true;
      //for (int groupNum = 0; groupNum < stateIndices.length; groupNum++) {
      for (int groupNum = stateIndices.length - 1; groupNum >= 0; groupNum--) {
        int stateNum = stateIndices[groupNum];
        double shiftedOxidationState = shiftedOxidationStates[groupNum][stateNum];
        shiftedOxidation += shiftedOxidationState;
        if (shiftedOxidation - oxidationTolerance > shiftedNeutrality) {
          indexer.branchEnd(groupNum, stateIndices);
          break;
        }
        if (groupNum != stateIndices.length - 1) {// Insist upon a single ordering
          //double prevShiftedOxidationState = shiftedOxidationStates[groupNum + 1][stateIndices[groupNum + 1]];
          if ((groupIndices[groupNum] == groupIndices[groupNum+1]) && (stateNum > stateIndices[groupNum + 1])) { // Most probable first
          //if ((groupIndices[groupNum] == groupIndices[groupNum+1]) && (shiftedOxidationState > prevShiftedOxidationState)) { 
            legal = false;
            indexer.branchEnd(groupNum, stateIndices);
            break;
          }
        }
        probability *= probabilities[groupNum][stateNum];
        if ((probability < maxProbability) || probability == 0) {
        //if (probability < maxProbability) {
          indexer.branchEnd(groupNum, stateIndices);
          break;
        }
      }
      if (legal && (probability > maxProbability) && (Math.abs(shiftedOxidation - shiftedNeutrality) < oxidationTolerance)) {
        maxProbability = probability;
        System.arraycopy(stateIndices, 0, minIndices, 0, stateIndices.length);
      }
      long elapsedTime = System.currentTimeMillis() - startTime;
      if (elapsedTime > hardTimeOut) {
        Status.warning("Reached hard timeout of " + m_Params.getHardTimeout() + " milliseconds when searching for likely oxidation states without symmetry.");
        if (minIndices[0] >= 0) {
          Status.warning("Using most likely oxidation states found so far.");
        } else {
          Status.warning("Failed to find oxidation states in allotted time.  Consider increasing timeout, checking structure, or enabling symmetry.");
        }
        break;
      }
      if (elapsedTime > softTimeOut) {
        Status.warning("Reached soft timeout of " + softTimeOut + " milliseconds when searching for likely oxidation states without symmetry.");
        if (minIndices[0] >= 0) {
          Status.warning("Using most likely oxidation states found so far.");
          break;
        }
        Status.warning("No oxidation states found so far.  Resetting time counter.");
        startTime = System.currentTimeMillis();
        hardTimeOut -= softTimeOut;
      }
    } while (indexer.increment(stateIndices));
    
    if (minIndices[0] < 0) {
      throw new RuntimeException("Could not find valid oxidation states.");
    }
    
    m_LikelyOxidationStatesNoSym = new double[structure.numDefiningSites()][];
    for (int siteNum = 0; siteNum < m_LikelyOxidationStatesNoSym.length; siteNum++) {
      m_LikelyOxidationStatesNoSym[siteNum] = new double[m_DisorderedStructure.numAllowedSpecies(siteNum)];
    }
    m_LikelyOxidationNoSymAverageProbability = Math.pow(maxProbability, 1.0 / totalNumSiteSpecs);
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {
      Structure.Site[] sites = siteGroups[groupNum];
      int stateNum = minIndices[groupNum];
      int specIndex = allowedSpecIndices[groupNum];
      
      for (int siteNum = 0; siteNum < sites.length; siteNum++) {
        Structure.Site site = sites[siteNum];
        double occupancy = m_DisorderedStructure.getSiteOccupancy(site.getIndex(), specIndex);
        double oxidationState = Math.round(((shiftedOxidationStates[groupNum][stateNum] / siteGroups[groupNum].length) + minOxidationStates[groupNum]) / occupancy);

        m_LikelyOxidationStatesNoSym[sites[siteNum].getIndex()][specIndex] = oxidationState;
      }
    }

  }
  
  /**
   * This method returns a list of all the oxidation states known to this class for a given 
   * element.
   * 
   * @param element The element for which we want a list of known oxidation states.
   * @return An array of all known oxidation states.
   */
  public double[] getAllowedOxidationStates(Element element) {
    int atomicNumber = element.getAtomicNumber();
    if (atomicNumber < 0 || atomicNumber >= m_OxidationStates.length) {return null;}
    
    OxidationState[] knownStates = m_OxidationStates[atomicNumber];
    double[] returnArray = new double[knownStates.length];
    
    for (int stateNum = 0; stateNum < knownStates.length; stateNum++) {
      returnArray[stateNum] = knownStates[stateNum].getOxidationState();
    }
    return returnArray;
  }
  
  protected double[][] getProbabilities(Structure.Site site) {
    
    Species[] allowedSpecies = m_DisorderedStructure.getAllowedSpecies(site.getIndex());
    double[][] returnArray = new double[allowedSpecies.length][];
    
    double commonCount = m_Params.getWikiCommonICSDCount();
    double rareCount = m_Params.getWikiRareICSDCount();
    double noneCount = m_Params.getWikiNoneICSDCount();
    double zeroOxidationProbabilityFactor = m_Params.getZeroOxidationProbabilityFactor();
    
    this.calcBondValenceSums();
    
    for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {
      
      int atomicNumber = allowedSpecies[specNum].getElement().getAtomicNumber();
      if (atomicNumber < 0 || atomicNumber >= m_OxidationStates.length) {
        continue;
      }
      OxidationState[] allowedStates = m_OxidationStates[atomicNumber];
      returnArray[specNum] = new double[allowedStates.length];
      
      double bondValenceValue = m_BondValenceStates[site.getIndex()][specNum];
      
      // This approach is loosely based on Dirilichet priors
      //boolean isAnion = m_Anions[site.getIndex()];
      boolean isAnion = (bondValenceValue < 0);
      boolean isCation = (bondValenceValue > 0);
      boolean allowsZeroValenceAnions = m_Params.allowsZeroValenceAnions();
      boolean allowsZeroValenceCations = m_Params.allowsZeroValenceCations();
      
      //double gaussianWidth  = m_GaussianWidths[site.getIndex()];
      //double denominator = 1 / (2 * gaussianWidth * gaussianWidth);
      double gaussianWidth = (bondValenceValue == 0) ? m_Params.getZeroBVGaussianWidth() : m_Params.getGaussianWidth();
      
      for (int stateNum = 0; stateNum < returnArray[specNum].length; stateNum++) {
        OxidationState state = allowedStates[stateNum];
        double oxidationState = state.getOxidationState();
  
        //double gaussianWidth = (Math.abs(oxidationState) + 1) * Math.sqrt(m_Params.getGaussianWidthScaleFactor());
        //double gaussianWidth = 1;
        if (!state.isInteger()) {continue;}
        if (isAnion && (oxidationState > 0)) {continue;}
        if (isCation && (oxidationState < 0)) {continue;}
        if (isAnion && !allowsZeroValenceAnions && (oxidationState == 0)) {continue;}
        if (isCation && !allowsZeroValenceCations && (oxidationState == 0)) {continue;}
        double stateCount = 0;
        if (!state.isInWiki()) {stateCount += noneCount;}
        if (state.isWikiCommon()) {stateCount += commonCount;} 
        if (state.isWikiRare()) {stateCount += rareCount;} 
        double icsdCount = state.getICSDCount();
        if (icsdCount == 0) {icsdCount = m_Params.getICSDNoneCount();}
        if (state.isZero() && site.getSpecies().getElement() == Element.boron) {
          icsdCount -= m_Params.getBoronICSDZeroToMinusOneShift();
        }
        if ((state.getOxidationState() == -1) && site.getSpecies().getElement() == Element.boron) {
          icsdCount += m_Params.getBoronICSDZeroToMinusOneShift();
        }
        stateCount += icsdCount;
        if (stateCount < 0) {continue;}
        
        double delta = oxidationState - bondValenceValue;
        double denominator = 1 / (2 * gaussianWidth * gaussianWidth);
        /*if (state.isZero()) { // Makes the BV value more important 
          denominator += 1 / (2 * zeroValenceProbabilityFactor * zeroValenceProbabilityFactor);
        }*/
        returnArray[specNum][stateNum] = stateCount * Math.exp(-(delta * delta) * denominator);
        //if (state.isZero()) {returnArray[specNum][stateNum] *= 10000000000000000.0;}
        //if (state.isZero()) {returnArray[specNum][stateNum] *= zeroOxidationProbabilityScale;}
        if (state.isZero() && (bondValenceValue != 0)) {returnArray[specNum][stateNum] *= zeroOxidationProbabilityFactor;}
        /*if (valenceState == 0) {continue;}
        double delta = Math.log(Math.abs(valenceState)) - Math.log(Math.abs(bondValenceValue));
        returnArray[stateNum] = stateCount * Math.exp(-(delta * delta) * denominator) / Math.abs(valenceState); */
        //if (isAnion && (valenceState == 0)) {returnArray[stateNum] *= 0.5;} // 0 valence counts as 1/2 an anion
      }
      
      double mag = MSMath.magnitude(returnArray[specNum]);
      if (mag == 0) {
        throw new RuntimeException("All oxidation states for site with index " + site.getIndex() + " and specie " + site.getSpecies() + " have zero probability!  Check input structure and consider increasing the Gaussian width.");
      }
      
      // Changed 2/15/2010.   This should be normalized as weights!
      returnArray[specNum] = MSMath.normalizeWeights(returnArray[specNum]);
      /*for (int stateNum = 0; stateNum < returnArray[specNum].length; stateNum++) {
        returnArray[specNum][stateNum] /= mag;
      }*/
    }
    return returnArray;
    
  }
  
  /**
   * Only considers bond valence sum to determine whether it is a cation or anion and to
   * weight zero-oxidation states.
   * 
   * @param element
   * @param bvSum
   * @return
   */
  protected double[] getProbabilitiesSimple(Element element, double bvSum) {
    
    double commonCount = m_Params.getWikiCommonICSDCount();
    double rareCount = m_Params.getWikiRareICSDCount();
    double noneCount = m_Params.getWikiNoneICSDCount();
    
    int atomicNumber = element.getAtomicNumber();
    if (atomicNumber < 0 || atomicNumber >= m_OxidationStates.length) {
      return null;
    }
    OxidationState[] allowedStates = m_OxidationStates[atomicNumber];
    double[] returnArray = new double[allowedStates.length];
    
    // This approach is loosely based on Dirilichet priors
    //boolean isAnion = m_Anions[site.getIndex()];
    boolean isAnion = (bvSum < 0);
    boolean isCation = (bvSum > 0);
    boolean allowsZeroValenceAnions = m_Params.allowsZeroValenceAnions();
    boolean allowsZeroValenceCations = m_Params.allowsZeroValenceCations();
    double zeroOxidationProbabilityFactor = m_Params.getZeroOxidationProbabilityFactor();
    
    double gaussianWidth = (bvSum == 0) ? m_Params.getZeroBVGaussianWidth() : m_Params.getGaussianWidth();
    //gaussianWidth *= m_Params.getSimpleScoreGaussianWidthScale();
    
    for (int stateNum = 0; stateNum < returnArray.length; stateNum++) {
      OxidationState state = allowedStates[stateNum];
      double oxidationState = state.getOxidationState();

      if (!state.isInteger()) {continue;}
      if (isAnion && (oxidationState > 0)) {continue;}
      if (isCation && (oxidationState < 0)) {continue;}
      if (isAnion && !allowsZeroValenceAnions && (oxidationState == 0)) {continue;}
      if (isCation && !allowsZeroValenceCations && (oxidationState == 0)) {continue;}
      double stateCount = 0;
      if (!state.isInWiki()) {stateCount += noneCount;}
      if (state.isWikiCommon()) {stateCount += commonCount;} 
      if (state.isWikiRare()) {stateCount += rareCount;} 
      double icsdCount = state.getICSDCount();
      if (icsdCount == 0) {icsdCount = m_Params.getICSDNoneCount();}
      if (state.isZero() && element == Element.boron) {
        icsdCount -= m_Params.getBoronICSDZeroToMinusOneShift();
      }
      if ((state.getOxidationState() == -1) && element == Element.boron) {
        icsdCount += m_Params.getBoronICSDZeroToMinusOneShift();
      }
      stateCount += icsdCount;
      if (stateCount < 0) {continue;}

      double delta = oxidationState - bvSum;
      //if (state.isZero()) {
        double denominator = 1 / (2 * gaussianWidth * gaussianWidth);
        stateCount *= Math.exp(-(delta * delta) * denominator);
      //}
        
      if (state.isZero() && (bvSum != 0)) {stateCount *= zeroOxidationProbabilityFactor;}
      
      returnArray[stateNum] = stateCount;
    }
    
    double mag = MSMath.magnitude(returnArray);
    if (mag == 0) {
      throw new RuntimeException("All oxidation states for element " + element + " with bond valence sum " + bvSum + " have zero probability!  Check input structure and consider increasing the Gaussian width.");
    }
    returnArray= MSMath.normalizeWeights(returnArray);
    return returnArray;
    
  }
  
  /**
   * 
   * @return A String containing information about all known oxidation states for elements.
   */
  public String listOxidationStates() {
    
     String returnString = "";
    
     for (int atomicNumber =1 ; atomicNumber <= Element.numKnownElements(); atomicNumber++) {
       Element element = Element.getElement(atomicNumber);
       OxidationAnalyzer.OxidationState[] oxidationStates = this.getOxidationData(element);
       if (oxidationStates == null) {
         returnString = returnString + "Element: " + element + ", No oxidation data\n";
         continue;
       }
       for (int stateNum = 0; stateNum < oxidationStates.length; stateNum++) {
         OxidationAnalyzer.OxidationState state = oxidationStates[stateNum];
         returnString = returnString + ("Element: " + element);
         returnString = returnString + (", Atomic number: " + atomicNumber);
         returnString = returnString + (", Oxidation state: " + state.getOxidationState());
         returnString = returnString + (", Is integer: " + state.isInteger());
         returnString = returnString + (", ICSD count: " + state.getICSDCount());
         returnString = returnString + (", ICSD fraction: " + state.getICSDFraction());
         returnString = returnString + (", Wikipedia status: " + state.wikiState());
         returnString = returnString + "\n";
       }

     }
     
     return returnString;
    
  }
  
  /**
   * Say, what are those c and r paramters anyway?  And for which elements do we
   * have parameters?  Let's print them out!  From JASC 113, 3226.
   *
   */
  public static void printParameters() {
    
    for (int atomicNumber = 0; atomicNumber < C_PARAMETERS.length; atomicNumber++) {
      Element element = Element.getElement(atomicNumber);
      System.out.println(element.getSymbol() + ", C Parameter: " + C_PARAMETERS[atomicNumber] + ", R Paramter: " + R_PARAMETERS[atomicNumber]);
    }
    
  }
  
  /**
   * 
   * @param element The element for which we want oxidation data
   * @return All known oxidation data for the given element, or null if none is known.
   */
  public OxidationState[] getOxidationData(Element element) {
    
    int atomicNumber = element.getAtomicNumber();
    if (atomicNumber < 1 || atomicNumber >= m_OxidationStates.length) {return null;}
    return (OxidationState[]) ArrayUtils.copyArray(m_OxidationStates[atomicNumber]);
    
  }
  
  /**
   * 
   * @param element The element for which we want oxidation data
   * @return All default oxidation data for the given element, or null if none is known.
   */
  public static OxidationState[] getDefaultOxidationData(Element element) {
    
    OxidationAnalyzer tempAnalyzer =new OxidationAnalyzer();
    return tempAnalyzer.getOxidationData(element);
    
  }
  
  /*public static void parseValenceStates() throws Exception {
    
    FileReader fileReader = new FileReader("cmc/a123/valenceStats.txt");
    LineNumberReader lineReader = new LineNumberReader(fileReader);
    
    String line = lineReader.readLine();
    OxidationState[][] newStates = new OxidationState[OXIDATION_STATES.length][];
    
    while (line != null) {
      
      StringTokenizer st = new StringTokenizer(line);
      String specName = st.nextToken().trim();
      Specie spec = Specie.get(specName);
      OldOxidationAnalyzer.OxidationState[] oxidationStates = OldOxidationAnalyzer.getOxidationStates(spec);
      if (oxidationStates == null) {
        System.out.println("New specie: " + spec);
        line = lineReader.readLine();
        continue;
      }
      for (int stateNum = 0; stateNum < oxidationStates.length; stateNum++) {
        if (oxidationStates[stateNum].isInteger()) {
          oxidationStates[stateNum].m_ICSDCount = 0;
        }
      }
      StringTokenizer st2 = new StringTokenizer(line, "[]");
      while (st2.hasMoreTokens()) {
        st2.nextToken();
        if (!st2.hasMoreTokens()) {break;}
        String data = st2.nextToken();
        StringTokenizer st3 = new StringTokenizer(data, ",");
        int valenceState = Integer.parseInt(st3.nextToken().trim());
        int count = Integer.parseInt(st3.nextToken().trim());
        boolean match = false;
        for (int stateNum = 0; stateNum < oxidationStates.length; stateNum++) {
          if (oxidationStates[stateNum].getOxidationState() == valenceState) {
            match = true;
            oxidationStates[stateNum].m_ICSDCount = count;
            break;
          }
        }
        //if (!match) {
        //System.out.println("New oxdiation state: " + spec + ", " + valenceState + ", " + count);
        //}
        //System.out.println(specName + ", " + st3.nextToken().trim() + ", " + st3.nextToken().trim());
      }
      int atomicNumber = spec.getElement().getAtomicNumber();
      newStates[atomicNumber] = oxidationStates;      
      line = lineReader.readLine();
    }
    
    for (int atomicNumber = 1; atomicNumber < newStates.length; atomicNumber++) {
      
      OxidationState[] oxidationStates = newStates[atomicNumber];
      if (oxidationStates == null) {
        oxidationStates = OXIDATION_STATES[atomicNumber];
      }
      Specie spec = Specie.get(atomicNumber);
      for (int stateNum = 0; stateNum < oxidationStates.length; stateNum++) {
        OxidationState state = oxidationStates[stateNum];
        int icsdCount = state.wasICSDQueried() ? state.getICSDCount() : -1;
        String outline = "new OxidationState( \"" + spec.getElementSymbol() + "\" , " +
            state.getOxidationState() + " , " +
            state.getWikiState() + " , " +
            icsdCount + " )";
        if (stateNum == 0) {outline = "{ " + outline;}
        if (stateNum == oxidationStates.length - 1) {
            outline = outline + " } , ";
          } else {
            outline = outline + " , ";
          }
        System.out.println(outline);
      }
    }

    fileReader.close();
    
  }*/
  
  /**
   * This method parses through the set of all known oxidation numbers for all elements and
   * tries to find any that seem odd. 
   */
  public void findAnomalies() {
    
    for (int atomicNumber = 0; atomicNumber < m_OxidationStates.length; atomicNumber++) {
      OxidationState[] states = m_OxidationStates[atomicNumber];
      for (int stateNum = 0; stateNum < states.length; stateNum++) {
        OxidationState state = states[stateNum];
        if (state.getSpecie().getElement().getAtomicNumber() != atomicNumber) {
          System.out.println("Atomic number is wrong: " + state);
        }
      }
    }
      
    System.out.println();
    System.out.println("------------------------------------------------------------");
    System.out.println();
    
    for (int atomicNumber = 0; atomicNumber < m_OxidationStates.length; atomicNumber++) {
      OxidationState[] states = m_OxidationStates[atomicNumber];
      for (int stateNum = 0; stateNum < states.length; stateNum++) {
        OxidationState state = states[stateNum];
        boolean isInteger = (Math.round(state.getOxidationState()) == state.getOxidationState() );
        if (!state.isInWiki() && isInteger && (state.getICSDCount() > 9)) {
          System.out.println(state);
        }
      }
    }
      
    System.out.println();
    System.out.println("------------------------------------------------------------");
    System.out.println();

    for (int atomicNumber = 0; atomicNumber < m_OxidationStates.length; atomicNumber++) {
       OxidationState[] states = m_OxidationStates[atomicNumber];
        for (int stateNum = 0; stateNum < states.length; stateNum++) {
          OxidationState state = states[stateNum];
          double ratio = state.getICSDFraction();
          if (state.isWikiCommon() && (ratio < 0.01)) {
            System.out.println(state);
          }
        }
     }
    
    System.out.println();
    System.out.println("------------------------------------------------------------");
    System.out.println();

    for (int atomicNumber = 0; atomicNumber < m_OxidationStates.length; atomicNumber++) {
       OxidationState[] states = m_OxidationStates[atomicNumber];
        for (int stateNum = 0; stateNum < states.length; stateNum++) {
          OxidationState state = states[stateNum];
          double ratio = state.getICSDFraction();
          if (state.isWikiRare() && (ratio > 0.1) && (state.getOxidationState() != 0)) {
            System.out.println(state);
          }
        }
     }
  }
  
  protected void calcBondValenceSums() {
    
    if (m_BondValenceStates != null) {return;}
    Structure structure = this.getStructure();
    
    m_BondValenceStates = new double[structure.numDefiningSites()][];
    for (int siteNum = 0; siteNum < m_BondValenceStates.length; siteNum++) {
      m_BondValenceStates[siteNum] = new double[m_DisorderedStructure.numAllowedSpecies(siteNum)];
    }
    m_BVCoordination = new int[structure.numDefiningSites()];
    //m_GaussianWidths = new double[m_Structure.numDefiningSites()];
    
    //Structure.Site[][] siteGroups = structure.groupEquivalentSites();
    Structure.Site[][] siteGroups = this.groupEquivalentSitesByAllowedSpeciesAndOccupancy();
    double searchRadius = m_Params.getBondValenceSearchRadius();
    double minAllowedDistance = m_Params.getMinAllowedBVDistance();
    //double gaussianWidthFactor = m_Params.getGaussianWidthScaleFactor();
    
    for (int groupNum = 0; groupNum < siteGroups.length; groupNum++) {

      Structure.Site site = siteGroups[groupNum][0];
      Species[] allowedSpecies = m_DisorderedStructure.getAllowedSpecies(site.getIndex());
      if (allowedSpecies.length == 1 && (allowedSpecies[0] == Species.vacancy)) {
        continue;
      }
      
      Coordinates siteCoords =site.getCoords();
      //boolean isAnion = m_Anions[site.getIndex()];
      //double direction = isAnion ? -1 : 1;
      Structure.Site[] neighbors = structure.getNearbySites(siteCoords, searchRadius, false);
      if (neighbors.length == 0) {
        String message = "Point with index " + site.getIndex() + " and specie " + site.getSpecies() + " has no neighbors within bond valence search radius of " + searchRadius;
        if (m_Params.allowIsolatedAtoms()) {
          Status.warning(message);
        } else {
          throw new RuntimeException(message);
        }
      }
      Vector[] vectors = new Vector[neighbors.length];
      double[] sqLengths = new double[neighbors.length];
      for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
        vectors[neighborNum] = new Vector(siteCoords, neighbors[neighborNum].getCoords());
        sqLengths[neighborNum] = vectors[neighborNum].innerProductCartesian(vectors[neighborNum]);
      }

      for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {

        double bvState = 0;
        double absBvState = 0;
        int numKeepers = 0;
        
        Element element = allowedSpecies[specNum].getElement();
        if (element == Element.vacancy) {continue;}
        
        double thisENeg = element.getElectronegativityPaulingCRC();
        if (Double.isNaN(thisENeg)) { // We don't know the electronegativity, so valence is probably zero
          Status.warning("Electronegativity for " + element + " is unknown.  Unable to determine cations and anions, so setting " + element + " bond valence sum to zero.");
          continue;
        }
        
        for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
          
          Structure.Site neighbor = neighbors[neighborNum];
          Species[] neighborAllowedSpecies = m_DisorderedStructure.getAllowedSpecies(neighbor.getIndex());
          double distance = neighbor.distanceFrom(siteCoords);
          if (distance < minAllowedDistance) {
            continue;
          }
          
          /*Vector thisVec = vectors[neighborNum];
          boolean vNeighbor = true;
          for (int vecNum = 0; vecNum < vectors.length; vecNum++) {
            //if (sqLengths[vecNum] < sqMinAllowedDistance) {
            //  continue;
            //}
            double dotProduct = vectors[vecNum].innerProductCartesian(thisVec);
            vNeighbor = (dotProduct <= sqLengths[vecNum]);
            if (!vNeighbor) {break;}
          }
          
          if (!vNeighbor) {continue;}*/
          
          boolean keeper = false;
          for (int neighborSpecNum = 0; neighborSpecNum < neighborAllowedSpecies.length; neighborSpecNum++) {
            Element neighborElement = neighborAllowedSpecies[neighborSpecNum].getElement();
            if (neighborElement == Element.vacancy) {continue;}
            if ((!m_AllowSameElements) && (element == neighborElement)) {continue;} // Two of the same
  
            double neighborENeg = neighborElement.getElectronegativityPaulingCRC();
            if (Double.isNaN(neighborENeg)) { // We don't know the electronegativity, so valence is probably zero
              Status.warning("Electronegativity for " + neighborElement + " is unknown.  Unable to determine cations and anions, so setting " + element + "-" + neighborElement + " bond valence to zero.");
              continue;
            }
            
            if (!(ArrayUtils.arrayContains(BV_ELECTRONEGATIVE_ELEMENTS, element) || ArrayUtils.arrayContains(BV_ELECTRONEGATIVE_ELEMENTS, neighborElement))) {
              Status.detail("Found Voronoi neighbors " + element + " and " + neighborElement + " within " + distance + " of each other.  Neither was considered an anion when bond valence parameters were derived.  Setting bond valence to zero.");
              continue;
            }
            
            keeper = true;
            double direction = (thisENeg > neighborENeg) ? -1 : 1;
            
            //if (!(isAnion ^ m_Anions[neighbor.getIndex()])) {continue;} // Two cations or two anions
            double bvTerm = 0;
            try {
              bvTerm = OxidationAnalyzer.getBondValence(element, neighborElement, distance);
            } catch (Exception e) {
              Status.warning(e.toString());
              Status.warning("Setting bond valence sum to zero.");
              continue;
            }
            
            double occupancy = m_DisorderedStructure.getSiteOccupancy(neighbor.getIndex(),neighborSpecNum);
            bvState += direction * bvTerm * occupancy;
            absBvState += bvTerm * occupancy;
          }
          if (keeper) {
            numKeepers++;
          }
        }
        for (int siteNum = 0; siteNum < siteGroups[groupNum].length; siteNum++) {
          Structure.Site groupSite = siteGroups[groupNum][siteNum];
          m_BondValenceStates[groupSite.getIndex()][specNum] = bvState;
          m_BVCoordination[groupSite.getIndex()] = numKeepers;
          //m_GaussianWidths[groupSite.getIndex()] = absBvState * gaussianWidthFactor;
        }
      }
    }
    
  }
  
  /**
   * This function returns the "c" paramter used for generating bond valence paramters.  The
   * table of paramters is taken from JASC 113, 3226.  A RuntimeException is thrown if no
   * paramter is given for the element.
   * 
   * @param element The element for which we want the paramter.
   * @return The c paramter.
   */
  public static double getCParameter(Element element) {
    int atomicNumber = element.getAtomicNumber();
    if (atomicNumber < 0 || atomicNumber >= C_PARAMETERS.length) {
      throw new RuntimeException("I do not know how to calculate a bond valence sum for " + element.getName());
    }
    
    return C_PARAMETERS[atomicNumber];
  }
  
  /**
   * This function returns the "r" paramter used for generating bond valence paramters.  The
   * table of paramters is taken from JASC 113, 3226.  A RuntimeException is thrown if no
   * paramter is given for the element.
   * 
   * @param element The element for which we want the paramter.
   * @return The r paramter.
   */
  public static double getRParameter(Element element) {
    int atomicNumber = element.getAtomicNumber();
    if (atomicNumber < 0 || atomicNumber >= C_PARAMETERS.length) {
      throw new RuntimeException("I do not know how to calculate a bond valence sum for " + element.getName());
    }
    
    return R_PARAMETERS[atomicNumber];
  }
  
  /**
   * This function returns the bond valence parameter (Rij) for a given pair of elements.  The bond valence
   * paramters are calculated according to JASC 113, 3226.  A RuntimeException is thrown if 
   * the bond valence paramters for one of the elements are unknown.
   * 
   * @param element1 The first element in the bond
   * @param element2 The second element in the bond (order doesn't really matter)
   * @return The bond valence parameter Rij.
   */
  public static double getRValue(Element element1, Element element2) {
    
    double cParameter1 = getCParameter(element1);
    double rParameter1 = getRParameter(element1);
    
    double cParameter2 = getCParameter(element2);
    double rParameter2 = getRParameter(element2);
    
    if (Double.isNaN(cParameter1) || Double.isNaN(rParameter1)) {
    	double returnValue = directLookupParameter(element1, element2);
    	if (!Double.isNaN(returnValue)) {return returnValue;}
    	throw new RuntimeException("I do not know how to calculate a bond valence sum for " + element1.getName());
    }
    
    if (Double.isNaN(cParameter2) || Double.isNaN(rParameter2)) {
    	double returnValue = directLookupParameter(element1, element2);
    	if (!Double.isNaN(returnValue)) {return returnValue;}
      throw new RuntimeException("I do not know how to calculate a bond valence sum for " + element2.getName());
    }
    
    double cDiff = Math.sqrt(cParameter1) - Math.sqrt(cParameter2);
    double numerator = rParameter1 * rParameter2 * cDiff * cDiff;
    double denominator = cParameter1 * rParameter1 + cParameter2 * rParameter2;
    
    return rParameter1 + rParameter2 + (numerator / denominator);

  }
  
  /**
   * Returns the bond valence for a bond between the given elements with the given bond length. The bond valence
   * parameters are calculated according to JACS 113, 3226.  A RuntimeException is thrown if 
   * the bond valence parameters for one of the elements are unknown.
   * 
   * @param element1 The first element in the bond
   * @param element2 The second element in the bond (order doesn't really matter)
   * @param bondLength The distance between two sites.
   * @return The bond valence
   */
  public static double getBondValence(Element element1, Element element2, double bondLength) {
    
    double rValue = getRValue(element1, element2);
    return Math.exp((rValue - bondLength) / 0.37);
    
  }
  
  /**
   * 
   * @param element The element for which we want a bond valence sum
   * @return True if the parameters exist, false otherwise.
   */
  public static boolean bvParametersExist(Element element) {
    int atomicNumber = element.getAtomicNumber();
    if (atomicNumber < 0 || atomicNumber >= C_PARAMETERS.length) {
      return false;
    }
    double cParameter = getCParameter(element);
    double rParameter = getRParameter(element);
    
    if (Double.isNaN(cParameter) || Double.isNaN(rParameter)) {
      return false;
    }
    return true;
  }
  
  /**
   * The valence analyzer uses information about the possible oxidation states to determine
   * the likely oxidation state.  Information about a known oxidation state for an element
   * is stored in this class.  For example, one instance of this class may represent O2-, and
   * another may represent Li1+.
   * 
   * @author Tim Mueller
   *
   */
  public class OxidationState {
    
    private Species m_Specie;
    private double m_OxidationState;
    private int  m_WikiStrength;
    private int m_ICSDCount;
    
    /**
     * Constructs a new object for a given element-oxidation state combination.
     * 
     * @param elementSymbol The symbol for the element.
     * @param oxidationState The oxidation state.
     * @param wikiStrength An integer representing whether the given oxidation state exists
     * for the given element in Wikipedia's list, and if so how common it is.  The values are
     * as follows: <br>
     * -1:  Not in Wikipedia's list
     * 0:  In Wikipedia's list, but considered rare. (or less common.)
     * +1:  In Wikipedia's list, and considered "more common." 
     * 
     * @param icsdCount The number of times this state occurs for this element in ICSD, according
     * to Chris Fischer's query.  This is the only source of non-integral oxidation states.  It
     * should be noted that it appears that when people enter data into ICSD and do not assign 
     * oxidation states, the state "0" is given.  For example, CaPo is listed in ICSD as Ca[0] Po[0],
     * although it should probably be Ca2+, Po2-.
     */
    protected OxidationState(String elementSymbol, double oxidationState, int wikiStrength, int icsdCount) {
      m_Specie = Species.get(elementSymbol, -1, oxidationState);
      m_OxidationState = oxidationState;
      m_WikiStrength = wikiStrength;
      m_ICSDCount = icsdCount;
    }
    
    /**
     * @return A message indicating in plane English whether this oxidation state is in 
     * Wikipedia's list of common oxidation states.  Wikipedia's list is based on 
     * <i>Greenwood, N. N.; Earnshaw, A. (1997). Chemistry of the Elements, 2nd Edition</i>, 
     * with some additions, and was downloaded on July 25 2008.  If the oxidation state is 
     * in the list, the commonality of the state is given ("Common" or "Rare").
     */
    public String wikiState() {
      if (m_WikiStrength < 0) {
        return "Not in wikipedia list.";
      } else if (m_WikiStrength == 0) {
        return "Rare.";
      } else if (m_WikiStrength == 1) {
        return "Common.";
      }
      return "Unknown.";
    }
    
    /**
     * 
     * @return The element-oxidation state combination represented by this object.
     */
    public Species getSpecie() {
      return m_Specie;
    }
    
    /**
     * 
     * @return The oxidation state represented by this object.
     */
    public double getOxidationState() {
      return m_OxidationState;
    }
    
    /**
     * 
     * @return True if and only if this state for this element is in Wikipedia's list.
     */
    public boolean isInWiki() {
      return (m_WikiStrength >= 0);
    }
    
    /**
     * Overrides the default value for Wikipedia's list and removes this state from the list.
     */
    public void setWikiNone() {
      m_WikiStrength = -1;
    }
    
    /**
     * 
     * @return True if and only if this state for this element is in Wikipedia's list, and is 
     * listed as "rare."  No zero-valence states are considered "common."
     */
    public boolean isWikiCommon() {
      return (m_WikiStrength == 1);
    }
    
    /**
     * Overrides the default value for Wikipedia's list and makes this state "common."
     */
    public void setWikiCommon() {
      m_WikiStrength = 1;
    }
    
    /**
     * 
     * @return True if and only if this state for this element is in Wikipedia's list, and is 
     * listed as "rare."  All zero-valence states are considered "rare."
     */
    public boolean isWikiRare() {
      return (m_WikiStrength == 0);
    }
    
    /**
     * Overrides the default value for Wikipedia's list and makes this state "rare."
     */
    public void setWikiRare() {
      m_WikiStrength = 0;
    }
    
    /**
     * 
     * @return True if and only if the oxidation state is exactly zero.
     */
    public boolean isZero() {
      return (m_OxidationState == 0);
    }
    
    /**
     * 
     * @return An integer representing whether the given state is not in Wikipedia's list (-1),
     * in Wikipedia's list but rare (0), or in Wikipedia's list but common (+1).
     */
    public int getWikiState() {
      return m_WikiStrength;
    }
    
    /**
     * 
     * @param value An integer representing whether the given state is not in Wikipedia's list 
     * (-1), in Wikipedia's list but rare (0), or in Wikipedia's list but common (+1).  This 
     * overrides the default value.
     */
    public void setWikiState(int value) {
      m_WikiStrength = value;
    }
    
    /**
     * 
     * @return True if and only if this oxidation state is an integer.
     */
    public boolean isInteger() {
      return (m_OxidationState == Math.round(m_OxidationState));
    }
    
    /**
     * 
     * @return True if and only if this oxidation state is in Chris Fischer's list of ICSD
     * oxidation states.
     */
    public boolean isInICSD() {
      return (m_ICSDCount >= 0);
    }
    
    /**
     * 
     * @return The number of time this oxidation state appeared for this element in the ICSD.
     */
    public int getICSDCount() {
      return Math.max(m_ICSDCount, 0);
    }
    
    /**
     * @param value The value with which to override the ICSD count.
     * 
     * Manually override the icsd count with the given value
     */
    public void setICSDCount(int value) {
      m_ICSDCount = value;
    }
    
    /**
     * 
     * @return True if and only if the ICSD was checked for this element
     */
    public boolean wasICSDQueried() {
      return (m_ICSDCount >= 0);
    }
    
    /**
     * 
     * @return The ratio of the number of times this element has this oxidation state in the ICSD
     * number of times this element has any oxidation state in the ICSD.
     */
    public double getICSDFraction() {
      
      int atomicNumber = m_Specie.getElement().getAtomicNumber();
      if (atomicNumber < 0 || atomicNumber >= m_OxidationStates.length) {
        return -1;
      }
      
      OxidationState[] states = m_OxidationStates[atomicNumber];
      int totalICSDCount = 0;
      for (int stateNum = 0; stateNum < states.length; stateNum++) {
        totalICSDCount += states[stateNum].getICSDCount();
      }
      
      return (1.0 * this.getICSDCount() / totalICSDCount);
    }
    
    public String toString() {
      double ratio = this.getICSDFraction();
      return "Specie: " + m_Specie + ", Oxidation state: " + m_OxidationState + ", Wikipedia status: " + this.wikiState() +" ICSD Count: " + m_ICSDCount + ", ICSD Fraction: " +  ratio;
    }
    
  }

  /**
   * These are user-definable parameters that may be used in the construction of the valence analyzer.  
   * See the sample parameters file for a detailed description of each of these paramters.
   * 
   * @author Tim Mueller
   *
   */
  public static class Params extends ParameterParser {
    
    private final long m_SoftTimeout;
    private final long m_HardTimeout;
    
    protected static HashMap<String, String[]> DEFAULT_PARAMS = new HashMap<String, String[]>();
    
    static {
      DEFAULT_PARAMS.put("ALLOW_ZERO_OXIDATION_ANIONS", new String[] {"true"});
      DEFAULT_PARAMS.put("ALLOW_ZERO_OXIDATION_CATIONS", new String[] {"true"});
      DEFAULT_PARAMS.put("LIKELY_OXIDATION_GAUSSIAN_WIDTH", new String[] {"1"});
      //DEFAULT_PARAMS.put("LIKELY_OXIDATION_ZERO_BOND_VALENCE_GAUSSIAN_WIDTH", new String[] {"Infinity"}); // Used for A123, start of Pellion
      DEFAULT_PARAMS.put("LIKELY_OXIDATION_ZERO_BOND_VALENCE_GAUSSIAN_WIDTH", new String[] {"0.6"}); // Was basically redundant with ZERO_OXIDATION_PROBABILITY_FACTOR
      //DEFAULT_PARAMS.put("ZERO_OXIDATION_PROBABILITY_SCALE", new String[] {"1"}); // Used for A123, start of Pellion
      DEFAULT_PARAMS.put("ZERO_OXIDATION_PROBABILITY_FACTOR", new String[] {"0.5"}); 
      DEFAULT_PARAMS.put("BOND_VALENCE_SEARCH_RADIUS", new String[] {"4.5"});
      DEFAULT_PARAMS.put("BOND_VALENCE_ALLOW_ISOLATED_ATOMS", new String[] {"false"});
      DEFAULT_PARAMS.put("OXIDATION_ICSD_NONE_COUNT", new String[] {"-9"});
      DEFAULT_PARAMS.put("OXIDATION_WIKI_NONE_ICSD_COUNT", new String[] {"-5"});
      DEFAULT_PARAMS.put("OXIDATION_WIKI_RARE_ICSD_COUNT", new String[] {"10"});
      DEFAULT_PARAMS.put("OXIDATION_WIKI_COMMON_ICSD_COUNT", new String[] {"50"});
      //DEFAULT_PARAMS.put("LIKELY_OXIDATION_NOSYM_SEED_FACTOR", new String[] {"1.1"});
      DEFAULT_PARAMS.put("LIKELY_OXIDATION_NOSYM_BOND_VALENCE_TOLERANCE", new String[] {"1E-4"});
      DEFAULT_PARAMS.put("AVERAGE_OXIDATION_BOND_VALENCE_TOLERANCE", new String[] {"1E-1"});
      DEFAULT_PARAMS.put("LIKELY_OXIDATION_SOFT_TIMEOUT", new String[] {"60000"});
      DEFAULT_PARAMS.put("LIKELY_OXIDATION_HARD_TIMEOUT", new String[] {"120000"});
      DEFAULT_PARAMS.put("OXIDATION_CHARGE_BALANCE_TOLERANCE", new String[] {"0.001"});
      DEFAULT_PARAMS.put("BOND_VALENCE_MIN_DISTANCE", new String[] {"0.9"});
      DEFAULT_PARAMS.put("POLYION_NEIGHBOR_MIN_BV", new String[] {"0.6"});
      //DEFAULT_PARAMS.put("BORON_ICSD_ZERO_TO_MINUS_ONE_SHIFT", new String[] {"0"}); // Used for A123, first pass Mg
      DEFAULT_PARAMS.put("BORON_ICSD_ZERO_TO_MINUS_ONE_SHIFT", new String[] {"300"}); // A bit of a hack.  Really should try to identify polyions of boron in ICSD, but no time for that.
      DEFAULT_PARAMS.put("SIMPLE_SCORE_GAUSSIAN_WIDTH_SCALE", new String[] {"0.5"}); 
    }
    
    /**
     * This constructor allows you to set timeouts without having to create a Parameters object.
     * 
     * @param softTimeout The soft timeout of the oxidation analyzer
     * @param hardTimeout The hard timeout of the oxidation analyzer.
     */
    public Params(long softTimeout, long hardTimeout) {
      
      super(null, "oxidation analyzer");
      //Status.flow("");
      //Status.flow("Loading oxidation analyzer parameters...");
      //this.setDefaults(m_Keys, m_DefaultValues);
      //this.setDefaultValues();
      
      Status.basic("Overriding parameter:  Setting soft timeout to " + softTimeout);
      m_SoftTimeout = softTimeout;
      
      Status.basic("Overriding parameter:  Setting hard timeout to " + hardTimeout);
      m_HardTimeout = hardTimeout;
    }
    
    /**
     * 
     * @param file The file from which we should initialize the parameters.  For any parameters
     * not included in the file, the default values are used.
     */
    public Params(Parameters file) {
      super(file, "oxidation analyzer");
      //Status.flow("");
      //Status.flow("Loading oxidation analyzer parameters...");
      //this.setDefaults(m_Keys, m_DefaultValues);
      
      m_SoftTimeout = this.longValue("LIKELY_OXIDATION_SOFT_TIMEOUT");
      m_HardTimeout = this.longValue("LIKELY_OXIDATION_HARD_TIMEOUT");
      
    }

    @Override
    protected HashMap<String, String[]> getDefaultValues() {
      return DEFAULT_PARAMS;
    }

    /**
     * 
     * @return The value of "ALLOW_ZERO_OXIDATION_ANIONS"
     */
    public boolean allowsZeroValenceAnions() {
      return this.isTrue("ALLOW_ZERO_OXIDATION_ANIONS");
    }
    
    /**
     * 
     * @return The value of "ALLOW_ZERO_OXIDATION_CATIONS"
     */
    public boolean allowsZeroValenceCations() {
      return this.isTrue("ALLOW_ZERO_OXIDATION_CATIONS");
    }

    /**
     * 
     * @return The value of "LIKELY_OXIDATION_GAUSSIAN_WIDTH"
     */
    public double getGaussianWidth() {
      return this.doubleValue("LIKELY_OXIDATION_GAUSSIAN_WIDTH");
    }
    
    /**
     * 
     * @return The value of "LIKELY_OXIDATION_ZERO_BOND_VALENCE_GAUSSIAN_WIDTH"
     */
    public double getZeroBVGaussianWidth() {
      return this.doubleValue("LIKELY_OXIDATION_ZERO_BOND_VALENCE_GAUSSIAN_WIDTH");
    }
    
    /**
     * For use in cases in which the bond valence sum is not zero.
     * 
     * @return The value of "ZERO_OXIDATION_PROBABILITY_FACTOR"
     */
    public double getZeroOxidationProbabilityFactor() {
      return this.doubleValue("ZERO_OXIDATION_PROBABILITY_FACTOR");
    }
    
    /**
     * 
     * @return The value of "BOND_VALENCE_SEARCH_RADIUS"
     */
    public double getBondValenceSearchRadius() {
      return this.doubleValue("BOND_VALENCE_SEARCH_RADIUS");
    }
    
    /**
     * 
     * @return The value of "BOND_VALENCE_ALLOW_ISOLATED_ATOMS"
     */
    public boolean allowIsolatedAtoms() {
      return this.isTrue("BOND_VALENCE_ALLOW_ISOLATED_ATOMS");
    }

    /**
     * 
     * @return The value of "OXIDATION_ICSD_NONE_COUNT"
     */
    public double getICSDNoneCount() {
      return this.doubleValue("OXIDATION_ICSD_NONE_COUNT");
    }
    
    /**
     * 
     * @return The value of "OXIDATION_WIKI_NONE_ICSD_COUNT"
     */
    public double getWikiNoneICSDCount() {
      return this.doubleValue("OXIDATION_WIKI_NONE_ICSD_COUNT");
    }
    
    /**
     * 
     * @return The value of "OXIDATION_WIKI_RARE_ICSD_COUNT"
     */
    public double getWikiRareICSDCount() {
      return this.doubleValue("OXIDATION_WIKI_RARE_ICSD_COUNT");
    }
    
    /**
     * 
     * @return The value of "OXIDATION_WIKI_COMMON_ICSD_COUNT"
     */
    public double getWikiCommonICSDCount() {
      return this.doubleValue("OXIDATION_WIKI_COMMON_ICSD_COUNT");
    }
    
    /**
     * 
     * @return The value of "LIKELY_OXIDATION_NOSYM_SEED_FACTOR"
     */
    /*public double getLikelyNoSymSeedFactor() {
      return this.doubleValue("LIKELY_OXIDATION_NOSYM_SEED_FACTOR");
    }*/
    
    /**
     * 
     * @return The value of "LIKELY_OXIDATION_NOSYM_BOND_VALENCE_TOLERANCE"
     */
    public double getLikelyNoSymBVTolerance() {
      return this.doubleValue("LIKELY_OXIDATION_NOSYM_BOND_VALENCE_TOLERANCE");
    }
    
    /**
     * 
     * @return The value of "AVERAGE_OXIDATION_BOND_VALENCE_TOLERANCE"
     */
    public double getAverageBVTolerance() {
      return this.doubleValue("AVERAGE_OXIDATION_BOND_VALENCE_TOLERANCE");
    }
    
    /**
     * 
     * @return The value of "LIKELY_OXIDATION_SOFT_TIMEOUT"
     */
    public long getSoftTimeout() {
      return m_SoftTimeout;
    }
    
    /**
     * 
     * @return The value of "LIKELY_OXIDATION_HARD_TIMEOUT"
     */
    public long getHardTimeout() {
      return m_HardTimeout;
    }
    
    /**
     * 
     * @return The value of "OXIDATION_CHARGE_BALANCE_TOLERANCE"
     */
    public double getOxidationTolerance() {
      return this.doubleValue("OXIDATION_CHARGE_BALANCE_TOLERANCE");
    }
    
    /**
     * 
     * @return The value of "BOND_VALENCE_MIN_DISTANCE"
     */
    public double getMinAllowedBVDistance() {
      return this.doubleValue("BOND_VALENCE_MIN_DISTANCE");
    }
    
    /**
     * 
     * @return The value of "POLYION_NEIGHBOR_MIN_BV"
     */
    public double getMinBVForPolyion() {
      return this.doubleValue("POLYION_NEIGHBOR_MIN_BV");
    }
    
    /**
     * 
     * @return The value of "BORON_ICSD_ZERO_TO_MINUS_ONE_SHIFT"
     */
    public int getBoronICSDZeroToMinusOneShift() {
      return this.intValue("BORON_ICSD_ZERO_TO_MINUS_ONE_SHIFT");
    }
    
    /**
     * 
     * @return The value of "SIMPLE_SCORE_GAUSSIAN_WIDTH_SCALE"
     */
    public double getSimpleScoreGaussianWidthScale() {
      return this.doubleValue("SIMPLE_SCORE_GAUSSIAN_WIDTH_SCALE");
    }
    
    
  }
  
  /**
   *  From Acta Cryst. (1991). B47, 192-197
   *  
   *  // TODO fill in all parameters for missing elements
   *  
   * @param element1
   * @param element2
   * @return
   */
  private static double directLookupParameter(Element element1, Element element2) {
	  
	  Element cation = element1.getAtomicNumber() > element2.getAtomicNumber() ? element1 : element2;
	  Element anion = element1.getAtomicNumber() < element2.getAtomicNumber() ? element1 : element2;
	  
	  if (cation == Element.platinum) {
		  if (anion == Element.oxygen) {return 1.8235;}
		  if (anion == Element.fluorine) {return 1.7195;}
		  if (anion == Element.chlorine) {return 2.11;}
	  }
	  
	  if (cation == Element.gold) {
		  if (anion == Element.oxygen) {return 1.833;}
		  if (anion == Element.fluorine) {return 1.81;}
		  if (anion == Element.chlorine) {return 2.17;}
	  }
	  
	  return Double.NaN;
	  
  }
  
  // From JASC 113, 3226
  private static double[] C_PARAMETERS = new double[] {
    0, 0.89, Double.NaN, 0.97, 1.47, 1.6, 2.0, 2.61, 3.15, 3.98, Double.NaN, 1.01, 1.23, 1.47, 1.58, 1.96, 2.35, 2.74, Double.NaN, 0.91, 1.04, 1.2, 1.32, 1.45, 1.56, 1.6, 1.64, 1.7, 1.75, 1.75, 1.66, 1.82, 1.51, 2.23, 2.51, 2.58, Double.NaN, 0.89, 0.99, 1.11, 1.22, 1.23, 1.3, Double.NaN, 1.42, 1.54, 1.35, 1.42, 1.46, 1.49, 1.72, 1.72, 2.72, 2.38, Double.NaN, 0.86, 0.97, 1.08, 1.08, 1.07, 1.07, Double.NaN, 1.07, 1.01, 1.11, 1.1, 1.1, 1.1, 1.11, 1.11, 1.06, 1.14, 1.23, 1.33, 1.4, 1.46, Double.NaN, 1.55, Double.NaN, Double.NaN, 1.44, 1.44, 1.55, 1.67, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, 1.11, Double.NaN, 1.22, 
  };
  
  private static double[] R_PARAMETERS = new double[] {
    0, 0.38, Double.NaN, 1.0, 0.81, 0.79, 0.78, 0.72, 0.63, 0.58, Double.NaN, 1.36, 1.21, 1.13, 1.12, 1.09, 1.03, 0.99, Double.NaN, 1.73, 1.5, 1.34, 1.27, 1.21, 1.16, 1.17, 1.16, 1.09, 1.04, 0.87, 1.07, 1.14, 1.21, 1.21, 1.18, 1.13, Double.NaN, 1.84, 1.66, 1.52, 1.43, 1.4, 1.37, Double.NaN, 1.21, 1.18, 1.11, 1.12, 1.28, 1.34, 1.37, 1.41, 1.4, 1.33, Double.NaN, 2.05, 1.88, 1.71, 1.68, 1.66, 1.64, Double.NaN, 1.61, 1.62, 1.58, 1.56, 1.54, 1.53, 1.51, 1.5, 1.49, 1.47, 1.42, 1.39, 1.38, 1.37, Double.NaN, 1.37, Double.NaN, Double.NaN, 1.32, 1.62, 1.53, 1.54, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, 1.7, Double.NaN, 1.59, 
  };
  
  private static Element[] BV_ELECTRONEGATIVE_ELEMENTS = new Element[] {
    Element.getElement("H"),
    Element.getElement("B"),
    Element.getElement("C"),
    Element.getElement("Si"),
    Element.getElement("N"),
    Element.getElement("P"),
    Element.getElement("As"),
    Element.getElement("Sb"),
    Element.getElement("O"),
    Element.getElement("S"),
    Element.getElement("Se"),
    Element.getElement("Te"),
    Element.getElement("F"),
    Element.getElement("Cl"),
    Element.getElement("Br"),
    Element.getElement("I"),
  };
  
  // From Wikipedia, July 25 2008.  Up to element 104 (Rf).  Also from Chris Fischer's ICSD spreadsheet.
  /*
   * This stuff is from Chris' original query, not the cleaned one
   * private static OxidationState[][] OXIDATION_STATES = new OxidationState[][] {
    { new OxidationState( "Vacancy", -1, 0, -1) },
    { new OxidationState( "H" , -1 , 1 , 104 ), 
      new OxidationState( "H" , 0 , 0 , 130 ), 
      new OxidationState( "H" , 1 , 1 , 4552 ), 
      new OxidationState( "H" , 2 , -1 , 1 ),  }, 
      { new OxidationState( "He" , 0 , 0 , 5 ),  }, 
      { new OxidationState( "Li" , -1 , 0 , 0 ), 
      new OxidationState( "Li" , 0 , 0 , 38 ), 
      new OxidationState( "Li" , 1 , 1 , 2178 ),  }, 
      { new OxidationState( "Be" , 0 , 0 , 5 ), 
      new OxidationState( "Be" , 2 , 1 , 236 ),  }, 
      { new OxidationState( "B" , -3 , -1 , 16 ), 
        new OxidationState( "B" , -2 , -1 , 12 ), 
      new OxidationState( "B" , -1.67 , -1 , 11 ), 
      new OxidationState( "B" , -1 , -1 , 6 ), 
      new OxidationState( "B" , 0 , 0 , 73 ), 
      new OxidationState( "B" , 1 , 0 , 0 ), 
      new OxidationState( "B" , 1.5 , -1 , 47 ), 
      new OxidationState( "B" , 2 , 0 , 0 ), 
      new OxidationState( "B" , 3 , 1 , 1000 ),  }, 
      { new OxidationState( "C" , -4 , 1 , 55 ), 
      new OxidationState( "C" , -3 , 0 , 3 ), 
      new OxidationState( "C" , -2 , 0 , 19 ), 
      new OxidationState( "C" , -1 , 0 , 9 ), 
      new OxidationState( "C" , 0 , 0 , 297 ), 
      new OxidationState( "C" , 1 , 0 , 0 ), 
      new OxidationState( "C" , 1.75 , -1 , 465 ), 
      new OxidationState( "C" , 2 , 0 , 0 ), 
      new OxidationState( "C" , 3 , 0 , 53 ), 
      new OxidationState( "C" , 4 , 1 , 500 ),  }, 
      { new OxidationState( "N" , -3 , 1 , 2315 ), 
      new OxidationState( "N" , -2 , 0 , 0 ), 
      new OxidationState( "N" , -1.67 , -1 , 109 ), 
      new OxidationState( "N" , -1 , 0 , 0 ), 
      new OxidationState( "N" , -0.333 , -1 , 595 ), 
      new OxidationState( "N" , 0 , 0 , 0 ), 
      new OxidationState( "N" , 1 , 0 , 26 ), 
      new OxidationState( "N" , 2 , 0 , 22 ), 
      new OxidationState( "N" , 3 , 1 , 176 ), 
      new OxidationState( "N" , 4 , 0 , 6 ), 
      new OxidationState( "N" , 5 , 1 , 404 ),  }, 
      { new OxidationState( "O" , -2 , 1 , 23959 ), 
      new OxidationState( "O" , -1 , 0 , 184 ), 
      new OxidationState( "O" , 0 , 0 , 402 ), 
      new OxidationState( "O" , 1 , 0 , 0 ), 
      new OxidationState( "O" , 2 , 0 , 0 ),  }, 
      { new OxidationState( "F" , -1 , 1 , 2936 ), 
      new OxidationState( "F" , 0 , 0 , 26 ),  }, 
      { new OxidationState( "Ne" , 0 , 0 , 4 ),  }, 
      { new OxidationState( "Na" , -1 , 0 , 0 ), 
      new OxidationState( "Na" , 0 , 0 , 38 ), 
      new OxidationState( "Na" , 1 , 1 , 2649 ),  }, 
      { new OxidationState( "Mg" , 0 , 0 , 10 ), 
      new OxidationState( "Mg" , 2 , 1 , 1792 ),  }, 
      { new OxidationState( "Al" , 0 , 0 , 55 ), 
      new OxidationState( "Al" , 1 , 0 , 0 ), 
      new OxidationState( "Al" , 3 , 1 , 2279 ),  }, 
      { new OxidationState( "Si" , -4 , 1 , 21 ), 
      new OxidationState( "Si" , -3 , 0 , 0 ), 
      new OxidationState( "Si" , -2 , 0 , 1 ), 
      new OxidationState( "Si" , -1 , 0 , 2 ), 
      new OxidationState( "Si" , 0 , 0 , 62 ), 
      new OxidationState( "Si" , 1 , 0 , 0 ), 
      new OxidationState( "Si" , 2 , 0 , 6 ), 
      new OxidationState( "Si" , 3 , 0 , 7 ), 
      new OxidationState( "Si" , 4 , 1 , 2490 ),  }, 
      { new OxidationState( "P" , -3 , 1 , 53 ), 
      new OxidationState( "P" , -2 , 0 , 25 ), 
      new OxidationState( "P" , -1 , 0 , 8 ), 
      new OxidationState( "P" , 0 , 0 , 93 ), 
      new OxidationState( "P" , 1 , 0 , 0 ), 
      new OxidationState( "P" , 1.5 , -1 , 55 ), 
      new OxidationState( "P" , 2 , 0 , 0 ), 
      new OxidationState( "P" , 3 , 1 , 108 ), 
      new OxidationState( "P" , 4 , 0 , 148 ), 
      new OxidationState( "P" , 5 , 1 , 2264 ), 
      new OxidationState( "P" , 6 , -1 , 1 ),  }, 
      { new OxidationState( "S" , -2 , 1 , 3623 ), 
      new OxidationState( "S" , -1 , 0 , 234 ), 
      new OxidationState( "S" , 0 , 0 , 460 ), 
      new OxidationState( "S" , 1 , 0 , 42 ), 
      new OxidationState( "S" , 2 , 1 , 62 ), 
      new OxidationState( "S" , 3 , 0 , 18 ), 
      new OxidationState( "S" , 4 , 1 , 135 ), 
      new OxidationState( "S" , 5 , 0 , 29 ), 
      new OxidationState( "S" , 6 , 1 , 790 ),  }, 
      { new OxidationState( "Cl" , -1 , 1 , 2358 ), 
      new OxidationState( "Cl" , 0 , 0 , 85 ), 
      new OxidationState( "Cl" , 1 , 1 , 3 ), 
      new OxidationState( "Cl" , 2 , 0 , 0 ), 
      new OxidationState( "Cl" , 3 , 1 , 27 ), 
      new OxidationState( "Cl" , 4 , 0 , 0 ), 
      new OxidationState( "Cl" , 5 , 1 , 40 ), 
      new OxidationState( "Cl" , 6 , 0 , 0 ), 
      new OxidationState( "Cl" , 7 , 1 , 105 ),  }, 
      { new OxidationState( "Ar" , 0 , 0 , 2 ),  }, 
      { new OxidationState( "K" , 0 , 0 , 62 ), 
      new OxidationState( "K" , 1 , 1 , 2621 ),  }, 
      { new OxidationState( "Ca" , 0 , 0 , 45 ), 
      new OxidationState( "Ca" , 1 , -1 , 1 ), 
      new OxidationState( "Ca" , 2 , 1 , 1896 ),  }, 
      { new OxidationState( "Sc" , 0 , 0 , 32 ), 
      new OxidationState( "Sc" , 1 , 0 , 5 ), 
      new OxidationState( "Sc" , 2 , 0 , 9 ), 
      new OxidationState( "Sc" , 3 , 1 , 337 ),  }, 
      { new OxidationState( "Ti" , -1 , 0 , 0 ), 
      new OxidationState( "Ti" , 0 , 0 , 134 ), 
      new OxidationState( "Ti" , 1 , -1 , 1 ), 
      new OxidationState( "Ti" , 2 , 0 , 58 ), 
      new OxidationState( "Ti" , 3 , 0 , 397 ), 
      new OxidationState( "Ti" , 4 , 1 , 1517 ),  }, 
      { new OxidationState( "V" , -1 , 0 , 0 ), 
      new OxidationState( "V" , 0 , 0 , 64 ), 
      new OxidationState( "V" , 1 , 0 , 0 ), 
      new OxidationState( "V" , 2 , 0 , 123 ), 
      new OxidationState( "V" , 3 , 0 , 322 ), 
      new OxidationState( "V" , 4 , 0 , 553 ), 
      new OxidationState( "V" , 5 , 1 , 0 ), 
      new OxidationState( "V" , 5.02 , -1 , 746 ),  }, 
      { new OxidationState( "Cr" , -2 , 0 , 0 ), 
      new OxidationState( "Cr" , -1 , 0 , 0 ), 
      new OxidationState( "Cr" , 0 , 0 , 62 ), 
      new OxidationState( "Cr" , 1 , 0 , 0 ), 
      new OxidationState( "Cr" , 2 , 0 , 187 ), 
      new OxidationState( "Cr" , 3 , 1 , 700 ), 
      new OxidationState( "Cr" , 4 , 0 , 81 ), 
      new OxidationState( "Cr" , 5 , 0 , 42 ), 
      new OxidationState( "Cr" , 6 , 1 , 189 ),  }, 
      { new OxidationState( "Mn" , -3 , 0 , 0 ), 
      new OxidationState( "Mn" , -2 , 0 , 0 ), 
      new OxidationState( "Mn" , -1.5 , -1 , 4 ), 
      new OxidationState( "Mn" , -1 , 0 , 0 ), 
      new OxidationState( "Mn" , 0 , 0 , 78 ), 
      new OxidationState( "Mn" , 1 , 0 , 12 ), 
      new OxidationState( "Mn" , 2 , 1 , 1021 ), 
      new OxidationState( "Mn" , 3 , 0 , 441 ), 
      new OxidationState( "Mn" , 4 , 1 , 814 ), 
      new OxidationState( "Mn" , 5 , 0 , 6 ), 
      new OxidationState( "Mn" , 6 , 0 , 9 ), 
      new OxidationState( "Mn" , 7 , 1 , 17 ),  }, 
      { new OxidationState( "Fe" , -2 , 0 , 0 ), 
      new OxidationState( "Fe" , -1 , 0 , 0 ), 
      new OxidationState( "Fe" , 0 , 0 , 212 ), 
      new OxidationState( "Fe" , 1 , 0 , 13 ), 
      new OxidationState( "Fe" , 2 , 1 , 1275 ), 
      new OxidationState( "Fe" , 3 , 1 , 1490 ), 
      new OxidationState( "Fe" , 4 , 0 , 134 ), 
      new OxidationState( "Fe" , 5 , 0 , 3 ), 
      new OxidationState( "Fe" , 6 , 0 , 5 ),  }, 
      { new OxidationState( "Co" , -1 , 0 , 6 ), 
      new OxidationState( "Co" , 0 , 0 , 106 ), 
      new OxidationState( "Co" , 1 , 0 , 14 ), 
      new OxidationState( "Co" , 2 , 1 , 809 ), 
      new OxidationState( "Co" , 3 , 1 , 357 ), 
      new OxidationState( "Co" , 4 , 0 , 34 ), 
      new OxidationState( "Co" , 5 , 0 , 0 ),  }, 
      { new OxidationState( "Ni" , -1 , 0 , 0 ), 
      new OxidationState( "Ni" , 0 , 0 , 133 ), 
      new OxidationState( "Ni" , 1 , 0 , 23 ), 
      new OxidationState( "Ni" , 2 , 1 , 1133 ), 
      new OxidationState( "Ni" , 3 , 0 , 126 ), 
      new OxidationState( "Ni" , 4 , 0 , 83 ),  }, 
      { new OxidationState( "Cu" , 0 , 0 , 106 ), 
      new OxidationState( "Cu" , 1 , 0 , 1142 ), 
      new OxidationState( "Cu" , 2 , 1 , 1323 ), 
      new OxidationState( "Cu" , 3 , 0 , 482 ), 
      new OxidationState( "Cu" , 4 , 0 , 1 ),  }, 
      { new OxidationState( "Zn" , 0 , 0 , 11 ), 
      new OxidationState( "Zn" , 2 , 1 , 1335 ),  }, 
      { new OxidationState( "Ga" , 0 , 0 , 46 ), 
      new OxidationState( "Ga" , 1 , 0 , 10 ), 
      new OxidationState( "Ga" , 2 , 0 , 16 ), 
      new OxidationState( "Ga" , 3 , 1 , 939 ),  }, 
      { new OxidationState( "Ge" , -4 , 1 , 16 ), 
      new OxidationState( "Ge" , -2 , -1 , 5 ), 
      new OxidationState( "Ge" , -1 , -1 , 2 ), 
      new OxidationState( "Ge" , 0 , 0 , 65 ), 
      new OxidationState( "Ge" , 1 , 0 , 0 ), 
      new OxidationState( "Ge" , 2 , 1 , 63 ), 
      new OxidationState( "Ge" , 3 , 0 , 25 ), 
      new OxidationState( "Ge" , 4 , 1 , 771 ),  }, 
      { new OxidationState( "As" , -3 , 1 , 66 ), 
      new OxidationState( "As" , -2 , -1 , 37 ), 
      new OxidationState( "As" , -1 , -1 , 12 ), 
      new OxidationState( "As" , 0 , 0 , 76 ), 
      new OxidationState( "As" , 2 , 0 , 14 ), 
      new OxidationState( "As" , 3 , 1 , 200 ), 
      new OxidationState( "As" , 4 , -1 , 2 ), 
      new OxidationState( "As" , 5 , 1 , 514 ),  }, 
      { new OxidationState( "Se" , -2 , 1 , 1678 ), 
      new OxidationState( "Se" , -1 , -1 , 222 ), 
      new OxidationState( "Se" , 0 , 0 , 270 ), 
      new OxidationState( "Se" , 1 , -1 , 45 ), 
      new OxidationState( "Se" , 2 , 1 , 14 ), 
      new OxidationState( "Se" , 3 , -1 , 4 ), 
      new OxidationState( "Se" , 4 , 1 , 350 ), 
      new OxidationState( "Se" , 5 , -1 , 1 ), 
      new OxidationState( "Se" , 6 , 1 , 180 ),  }, 
      { new OxidationState( "Br" , -1 , 1 , 1154 ), 
      new OxidationState( "Br" , 0 , 0 , 85 ), 
      new OxidationState( "Br" , 1 , 1 , 5 ), 
      new OxidationState( "Br" , 3 , 1 , 10 ), 
      new OxidationState( "Br" , 4 , 0 , 0 ), 
      new OxidationState( "Br" , 5 , 1 , 51 ), 
      new OxidationState( "Br" , 7 , 1 , 9 ),  }, 
      { new OxidationState( "Kr" , 0 , 0 , 2 ), 
      new OxidationState( "Kr" , 2 , 1 , 8 ),  }, 
      { new OxidationState( "Rb" , 0 , 0 , 31 ), 
      new OxidationState( "Rb" , 1 , 1 , 1287 ),  }, 
      { new OxidationState( "Sr" , 0 , 0 , 27 ), 
      new OxidationState( "Sr" , 2 , 1 , 2185 ),  }, 
      { new OxidationState( "Y" , 0 , 0 , 54 ), 
      new OxidationState( "Y" , 1 , -1 , 4 ), 
      new OxidationState( "Y" , 2 , 0 , 29 ), 
      new OxidationState( "Y" , 3 , 1 , 747 ),  }, 
      { new OxidationState( "Zr" , 0 , 0 , 96 ), 
      new OxidationState( "Zr" , 1 , 0 , 28 ), 
      new OxidationState( "Zr" , 2 , 0 , 35 ), 
      new OxidationState( "Zr" , 3 , 0 , 54 ), 
      new OxidationState( "Zr" , 4 , 1 , 789 ),  }, 
      { new OxidationState( "Nb" , -1 , 0 , 0 ), 
      new OxidationState( "Nb" , 0 , 0 , 127 ), 
      new OxidationState( "Nb" , 1 , -1 , 3 ), 
      new OxidationState( "Nb" , 1.6 , -1 , 111 ), 
      new OxidationState( "Nb" , 2 , 0 , 0 ), 
      new OxidationState( "Nb" , 2.667 , -1 , 107 ), 
      new OxidationState( "Nb" , 3 , 0 , 0 ), 
      new OxidationState( "Nb" , 4 , 0 , 273 ), 
      new OxidationState( "Nb" , 5 , 1 , 1002 ), 
      new OxidationState( "Nb" , 7 , -1 , 2 ),  }, 
      { new OxidationState( "Mo" , -2 , 0 , 0 ), 
      new OxidationState( "Mo" , -1 , 0 , 0 ), 
      new OxidationState( "Mo" , 0 , 0 , 96 ), 
      new OxidationState( "Mo" , 1 , 0 , 0 ), 
      new OxidationState( "Mo" , 2 , 0 , 58 ), 
      new OxidationState( "Mo" , 3 , 0 , 262 ), 
      new OxidationState( "Mo" , 4 , 1 , 89 ), 
      new OxidationState( "Mo" , 5 , 0 , 219 ), 
      new OxidationState( "Mo" , 6 , 1 , 696 ),  }, 
      { new OxidationState( "Tc" , -3 , 0 , 0 ), 
      new OxidationState( "Tc" , -1 , 0 , 0 ), 
      new OxidationState( "Tc" , 0 , 0 , 5 ), 
      new OxidationState( "Tc" , 1 , 0 , 0 ), 
      new OxidationState( "Tc" , 2 , 0 , 3 ), 
      new OxidationState( "Tc" , 3 , 0 , 8 ), 
      new OxidationState( "Tc" , 4 , 1 , 7 ), 
      new OxidationState( "Tc" , 5 , 0 , 0 ), 
      new OxidationState( "Tc" , 6 , 0 , 1 ), 
      new OxidationState( "Tc" , 7 , 1 , 11 ),  }, 
      { new OxidationState( "Ru" , -2 , 0 , 0 ), 
      new OxidationState( "Ru" , 0 , 0 , 48 ), 
      new OxidationState( "Ru" , 1 , 0 , 9 ), 
      new OxidationState( "Ru" , 2 , 0 , 14 ), 
      new OxidationState( "Ru" , 3 , 1 , 0 ), 
      new OxidationState( "Ru" , 3.92 , -1 , 243 ),   
      new OxidationState( "Ru" , 4 , 1 , 0 ), 
      new OxidationState( "Ru" , 5 , 0 , 96 ), 
      new OxidationState( "Ru" , 6 , 0 , 14 ), 
      new OxidationState( "Ru" , 7 , 0 , 1 ), 
      new OxidationState( "Ru" , 8 , 0 , 0 ),  }, 
      { new OxidationState( "Rh" , -1 , 0 , 3 ), 
      new OxidationState( "Rh" , 0 , 0 , 20 ), 
      new OxidationState( "Rh" , 1 , 0 , 3 ), 
      new OxidationState( "Rh" , 2 , 0 , 12 ), 
      new OxidationState( "Rh" , 3 , 1 , 106 ), 
      new OxidationState( "Rh" , 4 , 0 , 36 ), 
      new OxidationState( "Rh" , 5 , 0 , 2 ), 
      new OxidationState( "Rh" , 6 , 0 , 0 ),  }, 
      { new OxidationState( "Pd" , 0 , 0 , 0 ), 
      new OxidationState( "Pd" , 0.7 , -1 , 63 ), 
      new OxidationState( "Pd" , 1 , -1 , 26 ), 
      new OxidationState( "Pd" , 2 , 1 , 210 ), 
      new OxidationState( "Pd" , 3 , -1 , 8 ), 
      new OxidationState( "Pd" , 4 , 1 , 31 ),  }, 
      { new OxidationState( "Ag" , 0 , 0 , 30 ), 
      new OxidationState( "Ag" , 1 , 1 , 879 ), 
      new OxidationState( "Ag" , 2 , 0 , 46 ), 
      new OxidationState( "Ag" , 3 , 0 , 16 ),  }, 
      { new OxidationState( "Cd" , 0 , 0 , 16 ), 
      new OxidationState( "Cd" , 1 , -1 , 2 ), 
      new OxidationState( "Cd" , 2 , 1 , 910 ),  }, 
      { new OxidationState( "In" , -1 , -1 , 1 ), 
      new OxidationState( "In" , 0 , 0 , 71 ), 
      new OxidationState( "In" , 1 , 0 , 95 ), 
      new OxidationState( "In" , 2 , 0 , 38 ), 
      new OxidationState( "In" , 3 , 1 , 760 ),  }, 
      { new OxidationState( "Sn" , -4 , 1 , 1 ), 
      new OxidationState( "Sn" , -1 , -1 , 3 ), 
      new OxidationState( "Sn" , 0 , 0 , 52 ), 
      new OxidationState( "Sn" , 2 , 1 , 308 ), 
      new OxidationState( "Sn" , 3 , -1 , 21 ), 
      new OxidationState( "Sn" , 4 , 1 , 590 ),  }, 
      { new OxidationState( "Sb" , -3 , 1 , 37 ), 
      new OxidationState( "Sb" , -2 , -1 , 22 ), 
      new OxidationState( "Sb" , -1 , -1 , 1 ), 
      new OxidationState( "Sb" , 0 , 0 , 65 ), 
      new OxidationState( "Sb" , 1.33 , -1 , 6 ), 
      new OxidationState( "Sb" , 3 , 1 , 498 ), 
      new OxidationState( "Sb" , 4 , -1 , 5 ), 
      new OxidationState( "Sb" , 5 , 1 , 577 ),  }, 
      { new OxidationState( "Te" , -2 , 1 , 916 ), 
      new OxidationState( "Te" , -1 , -1 , 151 ), 
      new OxidationState( "Te" , 0 , 0 , 390 ), 
      new OxidationState( "Te" , 1 , -1 , 4 ), 
      new OxidationState( "Te" , 2 , 1 , 12 ), 
      new OxidationState( "Te" , 4 , 1 , 324 ), 
      new OxidationState( "Te" , 5 , 0 , 0 ), 
      new OxidationState( "Te" , 6 , 1 , 164 ),  }, 
      { new OxidationState( "I" , -1 , 1 , 1179 ), 
      new OxidationState( "I" , 0 , 0 , 169 ), 
      new OxidationState( "I" , 1 , 1 , 15 ), 
      new OxidationState( "I" , 3 , 1 , 22 ), 
      new OxidationState( "I" , 5 , 1 , 150 ), 
      new OxidationState( "I" , 7 , 1 , 78 ),  }, 
      { new OxidationState( "Xe" , 0 , 0 , 4 ), 
      new OxidationState( "Xe" , 2 , 1 , 22 ), 
      new OxidationState( "Xe" , 4 , 1 , 7 ), 
      new OxidationState( "Xe" , 6 , 1 , 25 ), 
      new OxidationState( "Xe" , 8 , 0 , 4 ),  }, 
      { new OxidationState( "Cs" , 0 , 0 , 42 ), 
      new OxidationState( "Cs" , 1 , 1 , 1707 ),  }, 
      { new OxidationState( "Ba" , 0 , 0 , 58 ), 
      new OxidationState( "Ba" , 2 , 1 , 2843 ),  }, 
      { new OxidationState( "La" , 0 , 0 , 84 ), 
      new OxidationState( "La" , 1 , -1 , 4 ), 
      new OxidationState( "La" , 2 , 0 , 49 ), 
      new OxidationState( "La" , 3 , 1 , 1835 ), 
      new OxidationState( "La" , 4 , -1 , 1 ),  }, 
      { new OxidationState( "Ce" , 0 , 0 , 32 ), 
      new OxidationState( "Ce" , 2 , 0 , 30 ), 
      new OxidationState( "Ce" , 3 , 1 , 303 ), 
      new OxidationState( "Ce" , 4 , 1 , 210 ),  }, 
      { new OxidationState( "Pr" , 0 , 0 , 29 ), 
      new OxidationState( "Pr" , 2 , 0 , 29 ), 
      new OxidationState( "Pr" , 3 , 1 , 476 ), 
      new OxidationState( "Pr" , 4 , 0 , 24 ),  }, 
      { new OxidationState( "Nd" , 0 , 0 , 43 ), 
      new OxidationState( "Nd" , 2 , 0 , 27 ), 
      new OxidationState( "Nd" , 3 , 1 , 763 ), 
      new OxidationState( "Nd" , 4 , -1 , 2 ),  }, 
      { new OxidationState( "Pm" , 0 , 0 , 0 ), 
      new OxidationState( "Pm" , 3 , 1 , 3 ),  }, 
      { new OxidationState( "Sm" , 0 , 0 , 20 ), 
      new OxidationState( "Sm" , 1 , -1 , 1 ), 
      new OxidationState( "Sm" , 2 , 0 , 74 ), 
      new OxidationState( "Sm" , 3 , 1 , 386 ),  }, 
      { new OxidationState( "Eu" , 0 , 0 , 16 ), 
      new OxidationState( "Eu" , 2 , 1 , 180 ), 
      new OxidationState( "Eu" , 3 , 1 , 219 ),
      new OxidationState( "Eu" , 4 , -1 , 4 ),  }, 
      { new OxidationState( "Gd" , 0 , 0 , 35 ), 
      new OxidationState( "Gd" , 1 , 0 , 0 ), 
      new OxidationState( "Gd" , 1.5 , -1 , 24 ), 
      new OxidationState( "Gd" , 2 , 0 , 0 ), 
      new OxidationState( "Gd" , 3 , 1 , 399 ), 
      new OxidationState( "Gd" , 4 , -1 , 3 ),  }, 
      { new OxidationState( "Tb" , 0 , 0 , 17 ), 
      new OxidationState( "Tb" , 1 , 0 , 6 ), 
      new OxidationState( "Tb" , 2 , -1 , 9 ), 
      new OxidationState( "Tb" , 3 , 1 , 228 ), 
      new OxidationState( "Tb" , 4 , 0 , 37 ),  }, 
      { new OxidationState( "Dy" , 0 , 0 , 11 ), 
      new OxidationState( "Dy" , 2 , 0 , 17 ), 
      new OxidationState( "Dy" , 3 , 1 , 219 ),  }, 
      { new OxidationState( "Ho" , 0 , 0 , 9 ), 
      new OxidationState( "Ho" , 2 , -1 , 6 ), 
      new OxidationState( "Ho" , 3 , 1 , 207 ),  }, 
      { new OxidationState( "Er" , 0 , 0 , 20 ), 
        new OxidationState( "Er" , 1 , -1 , 1 ), 
      new OxidationState( "Er" , 1.57 , -1 , 15 ), 
      new OxidationState( "Er" , 2 , -1 , 5 ), 
      new OxidationState( "Er" , 3 , 1 , 294 ),  }, 
      { new OxidationState( "Tm" , 0 , 0 , 8 ), 
      new OxidationState( "Tm" , 2 , 0 , 25 ), 
      new OxidationState( "Tm" , 3 , 1 , 129 ),  }, 
      { new OxidationState( "Yb" , 0 , 0 , 8 ), 
      new OxidationState( "Yb" , 2 , 0 , 71 ), 
      new OxidationState( "Yb" , 3 , 1 , 314 ),  }, 
      { new OxidationState( "Lu" , 0 , 0 , 13 ), 
      new OxidationState( "Lu" , 2 , -1 , 2 ), 
      new OxidationState( "Lu" , 3 , 1 , 178 ),  }, 
      { new OxidationState( "Hf" , 0 , 0 , 37 ), 
      new OxidationState( "Hf" , 2 , 0 , 0 ), 
      new OxidationState( "Hf" , 3 , 0 , 24 ), 
      new OxidationState( "Hf" , 4 , 1 , 141 ),  }, 
      { new OxidationState( "Ta" , -1 , 0 , 0 ), 
      new OxidationState( "Ta" , 0 , 0 , 93 ), 
      new OxidationState( "Ta" , 1 , -1 , 7 ), 
      new OxidationState( "Ta" , 1.2 , -1 , 39 ), 
      new OxidationState( "Ta" , 2 , 0 , 0 ), 
      new OxidationState( "Ta" , 3 , 0 , 58 ), 
      new OxidationState( "Ta" , 4 , 0 , 157 ), 
      new OxidationState( "Ta" , 5 , 1 , 664 ),  }, 
      { new OxidationState( "W" , -2 , 0 , 0 ), 
      new OxidationState( "W" , -1 , 0 , 0 ), 
      new OxidationState( "W" , 0 , 0 , 38 ), 
      new OxidationState( "W" , 1 , 0 , 0 ), 
      new OxidationState( "W" , 2 , 0 , 14 ), 
      new OxidationState( "W" , 3 , 0 , 19 ), 
      new OxidationState( "W" , 4 , 1 , 25 ), 
      new OxidationState( "W" , 5 , 0 , 142 ), 
      new OxidationState( "W" , 6 , 1 , 633 ),  }, 
      { new OxidationState( "Re" , -3 , 0 , 0 ), 
      new OxidationState( "Re" , -1 , 0 , 0 ), 
      new OxidationState( "Re" , 0 , 0 , 22 ), 
      new OxidationState( "Re" , 1 , 0 , 11 ), 
      new OxidationState( "Re" , 2 , 0 , 0 ), 
      new OxidationState( "Re" , 3 , 0 , 77 ), 
      new OxidationState( "Re" , 4 , 1 , 65 ), 
      new OxidationState( "Re" , 5 , 0 , 38 ), 
      new OxidationState( "Re" , 6 , 0 , 40 ), 
      new OxidationState( "Re" , 7 , 0 , 119 ),  }, 
      { new OxidationState( "Os" , -2 , 0 , 0 ), 
      new OxidationState( "Os" , 0 , 0 , 74 ), 
      new OxidationState( "Os" , 1 , 0 , 8 ), 
      new OxidationState( "Os" , 2 , 0 , 5 ), 
      new OxidationState( "Os" , 3 , 0 , 0 ), 
      new OxidationState( "Os" , 4 , 1 , 20 ), 
      new OxidationState( "Os" , 5 , 0 , 11 ), 
      new OxidationState( "Os" , 6 , 0 , 13 ), 
      new OxidationState( "Os" , 7 , 0 , 8 ), 
      new OxidationState( "Os" , 8 , 0 , 17 ),  }, 
      { new OxidationState( "Ir" , -1 , 0 , 1 ), 
      new OxidationState( "Ir" , 0 , 0 , 20 ), 
      new OxidationState( "Ir" , 1 , 0 , 1 ), 
      new OxidationState( "Ir" , 2 , 0 , 3 ), 
      new OxidationState( "Ir" , 3 , 1 , 50 ), 
      new OxidationState( "Ir" , 4 , 1 , 81 ), 
      new OxidationState( "Ir" , 5 , 0 , 51 ), 
      new OxidationState( "Ir" , 6 , 0 , 5 ),  }, 
      { new OxidationState( "Pt" , 0 , 0 , 35 ), 
      new OxidationState( "Pt" , 2 , 1 , 101 ), 
      new OxidationState( "Pt" , 3 , -1 , 10 ), 
      new OxidationState( "Pt" , 4 , 1 , 151 ), 
      new OxidationState( "Pt" , 5 , 0 , 4 ), 
      new OxidationState( "Pt" , 6 , 0 , 1 ),  }, 
      { new OxidationState( "Au" , -7.135 , -1 , 1 ), 
      new OxidationState( "Au" , -1 , 0 , 13 ), 
      new OxidationState( "Au" , 0 , 0 , 20 ), 
      new OxidationState( "Au" , 1 , 0 , 110 ), 
      new OxidationState( "Au" , 2 , 0 , 8 ), 
      new OxidationState( "Au" , 3 , 1 , 142 ), 
      new OxidationState( "Au" , 5 , 0 , 6 ),  }, 
      { new OxidationState( "Hg" , 0 , 0 , 25 ), 
      new OxidationState( "Hg" , 1 , 1 , 100 ), 
      new OxidationState( "Hg" , 2 , 1 , 497 ), 
      new OxidationState( "Hg" , 4 , 0 , 0 ),  }, 
      { new OxidationState( "Tl" , 0 , 0 , 41 ), 
      new OxidationState( "Tl" , 1 , 1 , 605 ), 
      new OxidationState( "Tl" , 2 , -1 , 1 ), 
      new OxidationState( "Tl" , 3 , 1 , 168 ),  }, 
      { new OxidationState( "Pb" , -4 , 0 , 1 ), 
      new OxidationState( "Pb" , 0 , 0 , 45 ), 
      new OxidationState( "Pb" , 1 , -1 , 26 ), 
      new OxidationState( "Pb" , 2 , 1 , 1200 ), 
      new OxidationState( "Pb" , 4 , 1 , 140 ),  }, 
      { new OxidationState( "Bi" , -3 , 0 , 2 ), 
      new OxidationState( "Bi" , -2 , -1 , 2 ), 
      new OxidationState( "Bi" , -1 , -1 , 1 ), 
      new OxidationState( "Bi" , 0 , 0 , 100 ), 
      new OxidationState( "Bi" , 1 , -1 , 12 ), 
      new OxidationState( "Bi" , 2 , -1 , 43 ), 
      new OxidationState( "Bi" , 3 , 1 , 1230 ), 
      new OxidationState( "Bi" , 4 , -1 , 128 ), 
      new OxidationState( "Bi" , 5 , 0 , 113 ),  }, 
      { new OxidationState( "Po" , -2 , 1 , 0 ), 
      new OxidationState( "Po" , 0 , 0 , 3 ), 
      new OxidationState( "Po" , 2 , 1 , 1 ), 
      new OxidationState( "Po" , 4 , 1 , 6 ), 
      new OxidationState( "Po" , 6 , 0 , 0 ),  }, 
      { new OxidationState( "At" , -1 , 1 , -1 ), 
      new OxidationState( "At" , 0 , 0 , -1 ), 
      new OxidationState( "At" , 1 , 1 , -1 ), 
      new OxidationState( "At" , 3 , 0 , -1 ), 
      new OxidationState( "At" , 5 , 0 , -1 ), 
      new OxidationState( "At" , 7 , 0 , -1 ),  }, 
      { new OxidationState( "Rn" , 0 , 0 , -1 ), 
      new OxidationState( "Rn" , 2 , 0 , -1 ),  }, 
      { new OxidationState( "Fr" , 0 , 0 , -1 ), 
      new OxidationState( "Fr" , 1 , 1 , -1 ),  }, 
      { new OxidationState( "Ra" , 0 , 0 , 1 ), 
      new OxidationState( "Ra" , 2 , 1 , 3 ),  }, 
      { new OxidationState( "Ac" , 0 , 0 , 1 ), 
      new OxidationState( "Ac" , 3 , 1 , 8 ),  }, 
      { new OxidationState( "Th" , -2 , -1 , 916 ), 
      new OxidationState( "Th" , -1 , -1 , 151 ), 
      new OxidationState( "Th" , 0 , 0 , 390 ), 
      new OxidationState( "Th" , 1 , -1 , 4 ), 
      new OxidationState( "Th" , 2 , 0 , 12 ), 
      new OxidationState( "Th" , 3 , 0 , 0 ), 
      new OxidationState( "Th" , 4 , 1 , 324 ), 
      new OxidationState( "Th" , 6 , -1 , 164 ),  }, 
      { new OxidationState( "Pa" , 0 , 0 , 3 ), 
      new OxidationState( "Pa" , 3 , 0 , 0 ), 
      new OxidationState( "Pa" , 4 , 0 , 6 ), 
      new OxidationState( "Pa" , 5 , 1 , 27 ),  }, 
      { new OxidationState( "U" , 0 , 0 , 63 ), 
      new OxidationState( "U" , 1 , -1 , 1 ), 
      new OxidationState( "U" , 2 , -1 , 28 ), 
      new OxidationState( "U" , 3 , 0 , 46 ), 
      new OxidationState( "U" , 4 , 0 , 288 ), 
      new OxidationState( "U" , 5 , 0 , 71 ), 
      new OxidationState( "U" , 6 , 1 , 415 ),  }, 
      { new OxidationState( "Np" , 0 , 0 , 8 ), 
      new OxidationState( "Np" , 2 , -1 , 4 ), 
      new OxidationState( "Np" , 3 , 0 , 11 ), 
      new OxidationState( "Np" , 4 , 0 , 19 ), 
      new OxidationState( "Np" , 5 , 1 , 15 ), 
      new OxidationState( "Np" , 6 , 0 , 12 ), 
      new OxidationState( "Np" , 7 , 0 , 4 ),  }, 
      { new OxidationState( "Pu" , 0 , 0 , 9 ), 
      new OxidationState( "Pu" , 2 , -1 , 5 ), 
      new OxidationState( "Pu" , 3 , 0 , 31 ), 
      new OxidationState( "Pu" , 4 , 1 , 15 ), 
      new OxidationState( "Pu" , 5 , 0 , 1 ), 
      new OxidationState( "Pu" , 6 , 0 , 3 ), 
      new OxidationState( "Pu" , 7 , 0 , 0 ),  }, 
      { new OxidationState( "Am" , 0 , 0 , 5 ), 
      new OxidationState( "Am" , 2 , 0 , 4 ), 
      new OxidationState( "Am" , 3 , 1 , 16 ), 
      new OxidationState( "Am" , 4 , 0 , 2 ), 
      new OxidationState( "Am" , 5 , 0 , 2 ), 
      new OxidationState( "Am" , 6 , 0 , 1 ),  }, 
      { new OxidationState( "Cm" , 0 , 0 , 3 ), 
      new OxidationState( "Cm" , 2 , -1 , 8 ), 
      new OxidationState( "Cm" , 3 , 1 , 12 ), 
      new OxidationState( "Cm" , 4 , 0 , 4 ),  }, 
      { new OxidationState( "Bk" , 0 , 0 , 2 ), 
      new OxidationState( "Bk" , 2 , -1 , 1 ), 
      new OxidationState( "Bk" , 3 , 1 , 3 ), 
      new OxidationState( "Bk" , 4 , 0 , 2 ),  }, 
      { new OxidationState( "Cf" , 0 , 0 , 6 ), 
      new OxidationState( "Cf" , 2 , 0 , 1 ), 
      new OxidationState( "Cf" , 3 , 1 , 5 ), 
      new OxidationState( "Cf" , 4 , 0 , 1 ),  }, 
      { new OxidationState( "Es" , 0 , 0 , -1 ), 
      new OxidationState( "Es" , 2 , 0 , -1 ), 
      new OxidationState( "Es" , 3 , 1 , -1 ),  }, 
      { new OxidationState( "Fm" , 0 , 0 , -1 ), 
      new OxidationState( "Fm" , 2 , 0 , -1 ), 
      new OxidationState( "Fm" , 3 , 1 , -1 ),  }, 
      { new OxidationState( "Md" , 0 , 0 , -1 ), 
      new OxidationState( "Md" , 2 , 0 , -1 ), 
      new OxidationState( "Md" , 3 , 1 , -1 ),  }, 
      { new OxidationState( "No" , 0 , 0 , -1 ), 
      new OxidationState( "No" , 2 , 0 , -1 ), 
      new OxidationState( "No" , 3 , 1 , -1 ),  }, 
      { new OxidationState( "Lr" , 0 , 0 , -1 ), 
      new OxidationState( "Lr" , 3 , 1 , -1 ),  }, 
      { new OxidationState( "Rf" , 0 , 0 , -1 ), 
      new OxidationState( "Rf" , 4 , 1 , -1 ),  }, 

  };*/
  
  private OxidationState[][] m_OxidationStates = new OxidationState[][] {
    { new OxidationState( "Vacancy", 0, 0, -1) },
    { new OxidationState( "H" , -1.0 , 1 , 281 ) , 
      new OxidationState( "H" , 0.0 , 0 , 180 ) , 
      new OxidationState( "H" , 1.0 , 1 , 3248 ) , 
      new OxidationState( "H" , 2.0 , -1 , 0 ) } , 
      { new OxidationState( "He" , 0.0 , 0 , 5 ) } , 
      { new OxidationState( "Li" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Li" , 0.0 , 0 , 239 ) , 
      new OxidationState( "Li" , 1.0 , 1 , 1182 ) } , 
      { new OxidationState( "Be" , 0.0 , 0 , 60 ) , 
      new OxidationState( "Be" , 2.0 , 1 , 230 ) } , 
      { new OxidationState( "B" , -3.0 , -1 , 64 ) , 
      new OxidationState( "B" , -2.0 , -1 , 12 ) , 
      new OxidationState( "B" , -1.67 , -1 , 11 ) , 
      new OxidationState( "B" , -1.0 , -1 , 6 ) , 
      new OxidationState( "B" , 0.0 , 0 , 582 ) , 
      new OxidationState( "B" , 1.0 , 0 , 37 ) , 
      new OxidationState( "B" , 1.5 , -1 , 47 ) , 
      new OxidationState( "B" , 2.0 , 0 , 13 ) , 
      new OxidationState( "B" , 3.0 , 1 , 863 ) } , 
      { new OxidationState( "C" , -4.0 , 1 , 119 ) , 
      new OxidationState( "C" , -3.0 , 0 , 10 ) , 
      new OxidationState( "C" , -2.0 , 0 , 70 ) , 
      new OxidationState( "C" , -1.0 , 0 , 43 ) , 
      //new OxidationState( "C" , 0.0 , 0 , 999999653 ) , 
      new OxidationState( "C" , 0.0 , 0 , 653 ) , 
      new OxidationState( "C" , 1.0 , 0 , 23 ) , 
      new OxidationState( "C" , 1.75 , -1 , 465 ) , 
      new OxidationState( "C" , 2.0 , 0 , 683 ) , 
      new OxidationState( "C" , 3.0 , 0 , 80 ) , 
      new OxidationState( "C" , 4.0 , 1 , 790 ) } , 
      { new OxidationState( "N" , -3.0 , 1 , 2026 ) , 
      new OxidationState( "N" , -2.0 , 0 , 105 ) , 
      new OxidationState( "N" , -1.67 , -1 , 109 ) , 
      new OxidationState( "N" , -1.0 , 0 , 93 ) , 
      new OxidationState( "N" , -0.333 , -1 , 595 ) , 
      new OxidationState( "N" , 0.0 , 0 , 258 ) , 
      new OxidationState( "N" , 1.0 , 0 , 33 ) , 
      new OxidationState( "N" , 2.0 , 0 , 35 ) , 
      new OxidationState( "N" , 3.0 , 1 , 204 ) , 
      new OxidationState( "N" , 4.0 , 0 , 5 ) , 
      new OxidationState( "N" , 5.0 , 1 , 272 ) } , 
      { new OxidationState( "O" , -2.0 , 1 , 12850 ) , 
      new OxidationState( "O" , -1.0 , 0 , 76 ) , 
      new OxidationState( "O" , 0.0 , 0 , 229 ) , 
      new OxidationState( "O" , 1.0 , 0 , 0 ) , 
      new OxidationState( "O" , 2.0 , 0 , 0 ) } , 
      { new OxidationState( "F" , -1.0 , 1 , 2405 ) , 
      new OxidationState( "F" , 0.0 , 0 , 13 ) } , 
      { new OxidationState( "Ne" , 0.0 , 0 , 4 ) } , 
      { new OxidationState( "Na" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Na" , 0.0 , 0 , 111 ) , 
      new OxidationState( "Na" , 1.0 , 1 , 2027 ) } , 
      { new OxidationState( "Mg" , 0.0 , 0 , 298 ) , 
      new OxidationState( "Mg" , 2.0 , 1 , 597 ) } , 
      { new OxidationState( "Al" , 0.0 , 0 , 596 ) , 
      new OxidationState( "Al" , 1.0 , 0 , 0 ) , 
      new OxidationState( "Al" , 3.0 , 1 , 831 ) } , 
      { new OxidationState( "Si" , -4.0 , 1 , 92 ) , 
      new OxidationState( "Si" , -3.0 , 0 , 0 ) , 
      new OxidationState( "Si" , -2.0 , 0 , 1 ) , 
      new OxidationState( "Si" , -1.0 , 0 , 8 ) , 
      new OxidationState( "Si" , 0.0 , 0 , 994 ) , 
      //new OxidationState( "Si" , 0.0 , 0 , 99994 ) , 
      new OxidationState( "Si" , 1.0 , 0 , 0 ) , 
      new OxidationState( "Si" , 2.0 , 0 , 6 ) , 
      new OxidationState( "Si" , 3.0 , 0 , 8 ) , 
      new OxidationState( "Si" , 4.0 , 1 , 1203 ) } , 
      { new OxidationState( "P" , -3.0 , 1 , 313 ) , 
      new OxidationState( "P" , -2.0 , 0 , 39 ) , 
      new OxidationState( "P" , -1.0 , 0 , 40 ) , 
      new OxidationState( "P" , 0.0 , 0 , 392 ) , 
      new OxidationState( "P" , 1.0 , 0 , 46 ) , 
      new OxidationState( "P" , 1.5 , -1 , 55 ) , 
      new OxidationState( "P" , 2.0 , 0 , 7 ) , 
      new OxidationState( "P" , 3.0 , 1 , 109 ) , 
      new OxidationState( "P" , 4.0 , 0 , 105 ) , 
      new OxidationState( "P" , 5.0 , 1 , 2052 ) , 
      new OxidationState( "P" , 6.0 , -1 , 1 ) } , 
      { new OxidationState( "S" , -2.0 , 1 , 2054 ) , 
      new OxidationState( "S" , -1.0 , 0 , 123 ) , 
      new OxidationState( "S" , 0.0 , 0 , 261 ) , 
      new OxidationState( "S" , 1.0 , 0 , 13 ) , 
      new OxidationState( "S" , 2.0 , 1 , 62 ) , 
      new OxidationState( "S" , 3.0 , 0 , 24 ) , 
      new OxidationState( "S" , 4.0 , 1 , 187 ) , 
      new OxidationState( "S" , 5.0 , 0 , 27 ) , 
      new OxidationState( "S" , 6.0 , 1 , 794 ) } , 
      { new OxidationState( "Cl" , -1.0 , 1 , 2029 ) , 
      new OxidationState( "Cl" , 0.0 , 0 , 84 ) , 
      new OxidationState( "Cl" , 1.0 , 1 , 2 ) , 
      new OxidationState( "Cl" , 2.0 , 0 , 0 ) , 
      new OxidationState( "Cl" , 3.0 , 1 , 22 ) , 
      new OxidationState( "Cl" , 4.0 , 0 , 0 ) , 
      new OxidationState( "Cl" , 5.0 , 1 , 27 ) , 
      new OxidationState( "Cl" , 6.0 , 0 , 0 ) , 
      new OxidationState( "Cl" , 7.0 , 1 , 94 ) } , 
      { new OxidationState( "Ar" , 0.0 , 0 , 2 ) } , 
      { new OxidationState( "K" , 0.0 , 0 , 119 ) , 
      new OxidationState( "K" , 1.0 , 1 , 2243 ) } , 
      { new OxidationState( "Ca" , 0.0 , 0 , 283 ) , 
      new OxidationState( "Ca" , 1.0 , -1 , 0 ) , 
      new OxidationState( "Ca" , 2.0 , 1 , 1010 ) } , 
      { new OxidationState( "Sc" , 0.0 , 0 , 231 ) , 
      new OxidationState( "Sc" , 1.0 , 0 , 2 ) , 
      new OxidationState( "Sc" , 2.0 , 0 , 15 ) , 
      new OxidationState( "Sc" , 3.0 , 1 , 186 ) } , 
      { new OxidationState( "Ti" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Ti" , 0.0 , 0 , 264 ) , 
      new OxidationState( "Ti" , 1.0 , -1 , 1 ) , 
      new OxidationState( "Ti" , 2.0 , 0 , 28 ) , 
      new OxidationState( "Ti" , 3.0 , 0 , 97 ) , 
      new OxidationState( "Ti" , 4.0 , 1 , 427 ) } , 
      { new OxidationState( "V" , -1.0 , 0 , 0 ) , 
      new OxidationState( "V" , 0.0 , 0 , 141 ) , 
      new OxidationState( "V" , 1.0 , 0 , 2 ) , 
      new OxidationState( "V" , 2.0 , 0 , 39 ) , 
      new OxidationState( "V" , 3.0 , 0 , 170 ) , 
      new OxidationState( "V" , 4.0 , 0 , 230 ) , 
      new OxidationState( "V" , 5.0 , 1 , 443 ) , 
      //new OxidationState( "V" , 5.02 , -1 , 746 ) // Not sure what to do about this, but this ain't right.
      } , 
      { new OxidationState( "Cr" , -2.0 , 0 , 0 ) , 
      new OxidationState( "Cr" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Cr" , 0.0 , 0 , 157 ) , 
      new OxidationState( "Cr" , 1.0 , 0 , 1 ) , 
      new OxidationState( "Cr" , 2.0 , 0 , 86 ) , 
      new OxidationState( "Cr" , 3.0 , 1 , 322 ) , 
      new OxidationState( "Cr" , 4.0 , 0 , 28 ) , 
      new OxidationState( "Cr" , 5.0 , 0 , 27 ) , 
      new OxidationState( "Cr" , 6.0 , 1 , 138 ) } , 
      { new OxidationState( "Mn" , -3.0 , 0 , 0 ) , 
      new OxidationState( "Mn" , -2.0 , 0 , 0 ) , 
      new OxidationState( "Mn" , -1.5 , -1 , 4 ) , 
      new OxidationState( "Mn" , -1.0 , 0 , 8 ) , 
      new OxidationState( "Mn" , 0.0 , 0 , 403 ) , 
      new OxidationState( "Mn" , 1.0 , 0 , 15 ) , 
      new OxidationState( "Mn" , 2.0 , 1 , 567 ) , 
      new OxidationState( "Mn" , 3.0 , 0 , 208 ) , 
      new OxidationState( "Mn" , 4.0 , 1 , 92 ) , 
      new OxidationState( "Mn" , 5.0 , 0 , 10 ) , 
      new OxidationState( "Mn" , 6.0 , 0 , 7 ) , 
      new OxidationState( "Mn" , 7.0 , 1 , 15 ) } , 
      { new OxidationState( "Fe" , -2.0 , 0 , 0 ) , 
      new OxidationState( "Fe" , -1.0 , 0 , 1 ) , 
      new OxidationState( "Fe" , 0.0 , 0 , 504 ) , 
      new OxidationState( "Fe" , 1.0 , 0 , 13 ) , 
      new OxidationState( "Fe" , 2.0 , 1 , 385 ) , 
      new OxidationState( "Fe" , 3.0 , 1 , 588 ) , 
      new OxidationState( "Fe" , 4.0 , 0 , 17 ) , 
      new OxidationState( "Fe" , 5.0 , 0 , 3 ) , 
      new OxidationState( "Fe" , 6.0 , 0 , 2 ) } , 
      { new OxidationState( "Co" , -1.0 , 0 , 11 ) , 
      new OxidationState( "Co" , 0.0 , 0 , 584 ) , 
      new OxidationState( "Co" , 1.0 , 0 , 24 ) , 
      new OxidationState( "Co" , 2.0 , 1 , 472 ) , 
      new OxidationState( "Co" , 3.0 , 1 , 160 ) , 
      new OxidationState( "Co" , 4.0 , 0 , 20 ) , 
      new OxidationState( "Co" , 5.0 , 0 , 0 ) } , 
      { new OxidationState( "Ni" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Ni" , 0.0 , 0 , 841 ) , 
      new OxidationState( "Ni" , 1.0 , 0 , 29 ) , 
      new OxidationState( "Ni" , 2.0 , 1 , 470 ) , 
      new OxidationState( "Ni" , 3.0 , 0 , 52 ) , 
      new OxidationState( "Ni" , 4.0 , 0 , 19 ) } , 
      { new OxidationState( "Cu" , 0.0 , 0 , 383 ) , 
      new OxidationState( "Cu" , 1.0 , 0 , 600 ) , 
      new OxidationState( "Cu" , 2.0 , 1 , 871 ) , 
      new OxidationState( "Cu" , 3.0 , 0 , 35 ) , 
      new OxidationState( "Cu" , 4.0 , 0 , 1 ) } , 
      { new OxidationState( "Zn" , 0.0 , 0 , 238 ) , 
      new OxidationState( "Zn" , 2.0 , 1 , 681 ) } , 
      { new OxidationState( "Ga" , 0.0 , 0 , 430 ) , 
      new OxidationState( "Ga" , 1.0 , 0 , 9 ) , 
      new OxidationState( "Ga" , 2.0 , 0 , 13 ) , 
      new OxidationState( "Ga" , 3.0 , 1 , 433 ) } , 
      { new OxidationState( "Ge" , -4.0 , 1 , 81 ) , 
      new OxidationState( "Ge" , -2.0 , -1 , 4 ) , 
      new OxidationState( "Ge" , -1.0 , -1 , 2 ) , 
      new OxidationState( "Ge" , 0.0 , 0 , 817 ) , 
      new OxidationState( "Ge" , 1.0 , 0 , 0 ) , 
      new OxidationState( "Ge" , 2.0 , 1 , 42 ) , 
      new OxidationState( "Ge" , 3.0 , 0 , 20 ) , 
      new OxidationState( "Ge" , 4.0 , 1 , 538 ) } , 
      { new OxidationState( "As" , -3.0 , 1 , 298 ) , 
      new OxidationState( "As" , -2.0 , -1 , 29 ) , 
      new OxidationState( "As" , -1.0 , -1 , 33 ) , 
      new OxidationState( "As" , 0.0 , 0 , 224 ) , 
      new OxidationState( "As" , 2.0 , 0 , 6 ) , 
      new OxidationState( "As" , 3.0 , 1 , 155 ) , 
      new OxidationState( "As" , 4.0 , -1 , 1 ) , 
      new OxidationState( "As" , 5.0 , 1 , 437 ) } , 
      { new OxidationState( "Se" , -2.0 , 1 , 971 ) , 
      new OxidationState( "Se" , -1.0 , -1 , 108 ) , 
      new OxidationState( "Se" , 0.0 , 0 , 154 ) , 
      new OxidationState( "Se" , 1.0 , -1 , 8 ) , 
      new OxidationState( "Se" , 2.0 , 1 , 25 ) , 
      new OxidationState( "Se" , 3.0 , -1 , 2 ) , 
      new OxidationState( "Se" , 4.0 , 1 , 287 ) , 
      new OxidationState( "Se" , 5.0 , -1 , 0 ) , 
      new OxidationState( "Se" , 6.0 , 1 , 156 ) } , 
      { new OxidationState( "Br" , -1.0 , 1 , 809 ) , 
      new OxidationState( "Br" , 0.0 , 0 , 64 ) , 
      new OxidationState( "Br" , 1.0 , 1 , 6 ) , 
      new OxidationState( "Br" , 3.0 , 1 , 9 ) , 
      new OxidationState( "Br" , 4.0 , 0 , 0 ) , 
      new OxidationState( "Br" , 5.0 , 1 , 31 ) , 
      new OxidationState( "Br" , 7.0 , 1 , 11 ) } , 
      { new OxidationState( "Kr" , 0.0 , 0 , 0 ) , 
      new OxidationState( "Kr" , 2.0 , 1 , 8 ) } , 
      { new OxidationState( "Rb" , 0.0 , 0 , 69 ) , 
      new OxidationState( "Rb" , 1.0 , 1 , 1100 ) } , 
      { new OxidationState( "Sr" , 0.0 , 0 , 194 ) , 
      new OxidationState( "Sr" , 2.0 , 1 , 914 ) } , 
      { new OxidationState( "Y" , 0.0 , 0 , 280 ) , 
      new OxidationState( "Y" , 1.0 , -1 , 3 ) , 
      new OxidationState( "Y" , 2.0 , 0 , 8 ) , 
      new OxidationState( "Y" , 3.0 , 1 , 315 ) } , 
      { new OxidationState( "Zr" , 0.0 , 0 , 292 ) , 
      new OxidationState( "Zr" , 1.0 , 0 , 6 ) , 
      new OxidationState( "Zr" , 2.0 , 0 , 24 ) , 
      new OxidationState( "Zr" , 3.0 , 0 , 21 ) , 
      new OxidationState( "Zr" , 4.0 , 1 , 299 ) } , 
      { new OxidationState( "Nb" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Nb" , 0.0 , 0 , 191 ) , 
      new OxidationState( "Nb" , 1.0 , -1 , 3 ) , 
      new OxidationState( "Nb" , 1.6 , -1 , 111 ) , 
      new OxidationState( "Nb" , 2.0 , 0 , 24 ) , 
      new OxidationState( "Nb" , 2.667 , -1 , 107 ) ,
      new OxidationState( "Nb" , 3.0 , 0 , 44 ) , 
      new OxidationState( "Nb" , 4.0 , 0 , 79 ) , 
      new OxidationState( "Nb" , 5.0 , 1 , 397 ) , 
      new OxidationState( "Nb" , 7.0 , -1 , 2 ) } , 
      { new OxidationState( "Mo" , -2.0 , 0 , 0 ) , 
      new OxidationState( "Mo" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Mo" , 0.0 , 0 , 129 ) , 
      new OxidationState( "Mo" , 1.0 , 0 , 1 ) , 
      new OxidationState( "Mo" , 2.0 , 0 , 54 ) , 
      new OxidationState( "Mo" , 3.0 , 0 , 69 ) , 
      new OxidationState( "Mo" , 4.0 , 1 , 55 ) , 
      new OxidationState( "Mo" , 5.0 , 0 , 81 ) , 
      new OxidationState( "Mo" , 6.0 , 1 , 441 ) } , 
      { new OxidationState( "Tc" , -3.0 , 0 , 0 ) , 
      new OxidationState( "Tc" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Tc" , 0.0 , 0 , 16 ) , 
      new OxidationState( "Tc" , 1.0 , 0 , 0 ) , 
      new OxidationState( "Tc" , 2.0 , 0 , 1 ) , 
      new OxidationState( "Tc" , 3.0 , 0 , 8 ) , 
      new OxidationState( "Tc" , 4.0 , 1 , 5 ) , 
      new OxidationState( "Tc" , 5.0 , 0 , 0 ) , 
      new OxidationState( "Tc" , 6.0 , 0 , 1 ) , 
      new OxidationState( "Tc" , 7.0 , 1 , 9 ) } , 
      { new OxidationState( "Ru" , -2.0 , 0 , 0 ) , 
      new OxidationState( "Ru" , 0.0 , 0 , 284 ) , 
      new OxidationState( "Ru" , 1.0 , 0 , 2 ) , 
      new OxidationState( "Ru" , 2.0 , 0 , 46 ) , 
      new OxidationState( "Ru" , 3.0 , 1 , 43 ) , 
      //new OxidationState( "Ru" , 3.92 , -1 , 243 ) ,  I don't know where this came from, but I don't think it's right.
      new OxidationState( "Ru" , 4.0 , 1 , 56 ) , 
      new OxidationState( "Ru" , 5.0 , 0 , 72 ) , 
      new OxidationState( "Ru" , 6.0 , 0 , 8 ) , 
      new OxidationState( "Ru" , 7.0 , 0 , 1 ) , 
      new OxidationState( "Ru" , 8.0 , 0 , 0 ) } , 
      { new OxidationState( "Rh" , -1.0 , 0 , 1 ) , 
      new OxidationState( "Rh" , 0.0 , 0 , 431 ) , 
      new OxidationState( "Rh" , 1.0 , 0 , 8 ) , 
      new OxidationState( "Rh" , 2.0 , 0 , 12 ) , 
      new OxidationState( "Rh" , 3.0 , 1 , 71 ) , 
      new OxidationState( "Rh" , 4.0 , 0 , 26 ) , 
      new OxidationState( "Rh" , 5.0 , 0 , 2 ) , 
      new OxidationState( "Rh" , 6.0 , 0 , 0 ) } , 
      { new OxidationState( "Pd" , 0.0 , 0 , 472 ) , 
      new OxidationState( "Pd" , 0.7 , -1 , 63 ) , 
      new OxidationState( "Pd" , 1.0 , -1 , 26 ) , 
      new OxidationState( "Pd" , 2.0 , 1 , 205 ) , 
      new OxidationState( "Pd" , 3.0 , -1 , 11 ) , 
      new OxidationState( "Pd" , 4.0 , 1 , 28 ) } , 
      { new OxidationState( "Ag" , 0.0 , 0 , 179 ) , 
      new OxidationState( "Ag" , 1.0 , 1 , 582 ) , 
      new OxidationState( "Ag" , 2.0 , 0 , 36 ) , 
      new OxidationState( "Ag" , 3.0 , 0 , 14 ) } , 
      { new OxidationState( "Cd" , 0.0 , 0 , 158 ) , 
      new OxidationState( "Cd" , 1.0 , -1 , 1 ) , 
      new OxidationState( "Cd" , 2.0 , 1 , 461 ) } , 
      { new OxidationState( "In" , -1.0 , -1 , 1 ) , 
      new OxidationState( "In" , 0.0 , 0 , 526 ) , 
      new OxidationState( "In" , 1.0 , 0 , 55 ) , 
      new OxidationState( "In" , 2.0 , 0 , 21 ) , 
      new OxidationState( "In" , 3.0 , 1 , 379 ) } , 
      { new OxidationState( "Sn" , -4.0 , 1 , 1 ) , 
      new OxidationState( "Sn" , -1.0 , -1 , 0 ) , 
      new OxidationState( "Sn" , 0.0 , 0 , 575 ) , 
      new OxidationState( "Sn" , 2.0 , 1 , 181 ) , 
      new OxidationState( "Sn" , 3.0 , -1 , 12 ) , 
      new OxidationState( "Sn" , 4.0 , 1 , 332 ) } , 
      { new OxidationState( "Sb" , -3.0 , 1 , 177 ) , 
      new OxidationState( "Sb" , -2.0 , -1 , 32 ) , 
      new OxidationState( "Sb" , -1.0 , -1 , 2 ) , 
      new OxidationState( "Sb" , 0.0 , 0 , 335 ) , 
      new OxidationState( "Sb" , 1.33 , -1 , 6 ) , 
      new OxidationState( "Sb" , 3.0 , 1 , 303 ) , 
      new OxidationState( "Sb" , 4.0 , -1 , 5 ) , 
      new OxidationState( "Sb" , 5.0 , 1 , 358 ) } , 
      { new OxidationState( "Te" , -2.0 , 1 , 453 ) , 
      new OxidationState( "Te" , -1.0 , -1 , 77 ) , 
      new OxidationState( "Te" , 0.0 , 0 , 187 ) , 
      new OxidationState( "Te" , 1.0 , -1 , 4 ) , 
      new OxidationState( "Te" , 2.0 , 1 , 14 ) , 
      new OxidationState( "Te" , 4.0 , 1 , 296 ) , 
      new OxidationState( "Te" , 5.0 , 0 , 0 ) , 
      new OxidationState( "Te" , 6.0 , 1 , 142 ) } , 
      { new OxidationState( "I" , -1.0 , 1 , 661 ) , 
      new OxidationState( "I" , 0.0 , 0 , 113 ) , 
      new OxidationState( "I" , 1.0 , 1 , 17 ) , 
      new OxidationState( "I" , 3.0 , 1 , 18 ) , 
      new OxidationState( "I" , 5.0 , 1 , 115 ) , 
      new OxidationState( "I" , 7.0 , 1 , 46 ) } , 
      { new OxidationState( "Xe" , 0.0 , 0 , 3 ) , 
      new OxidationState( "Xe" , 2.0 , 1 , 23 ) , 
      new OxidationState( "Xe" , 4.0 , 1 , 6 ) , 
      new OxidationState( "Xe" , 6.0 , 1 , 23 ) , 
      new OxidationState( "Xe" , 8.0 , 0 , 0 ) } , 
      { new OxidationState( "Cs" , 0.0 , 0 , 72 ) , 
      new OxidationState( "Cs" , 1.0 , 1 , 1521 ) } , 
      { new OxidationState( "Ba" , 0.0 , 0 , 208 ) , 
      new OxidationState( "Ba" , 2.0 , 1 , 1594 ) } , 
      { new OxidationState( "La" , 0.0 , 0 , 309 ) , 
      new OxidationState( "La" , 1.0 , -1 , 4 ) , 
      new OxidationState( "La" , 2.0 , 0 , 25 ) , 
      new OxidationState( "La" , 3.0 , 1 , 512 ) , 
      new OxidationState( "La" , 4.0 , -1 , 0 ) } , 
      { new OxidationState( "Ce" , 0.0 , 0 , 343 ) , 
      new OxidationState( "Ce" , 2.0 , 0 , 19 ) , 
      new OxidationState( "Ce" , 3.0 , 1 , 223 ) , 
      new OxidationState( "Ce" , 4.0 , 1 , 50 ) } , 
      { new OxidationState( "Pr" , 0.0 , 0 , 179 ) , 
      new OxidationState( "Pr" , 2.0 , 0 , 14 ) , 
      new OxidationState( "Pr" , 3.0 , 1 , 243 ) , 
      new OxidationState( "Pr" , 4.0 , 0 , 19 ) } , 
      { new OxidationState( "Nd" , 0.0 , 0 , 241 ) , 
      new OxidationState( "Nd" , 2.0 , 0 , 18 ) , 
      new OxidationState( "Nd" , 3.0 , 1 , 354 ) , 
      new OxidationState( "Nd" , 4.0 , -1 , 2 ) } , 
      { new OxidationState( "Pm" , 0.0 , 0 , 1 ) , 
      new OxidationState( "Pm" , 3.0 , 1 , 2 ) } , 
      { new OxidationState( "Sm" , 0.0 , 0 , 174 ) , 
      new OxidationState( "Sm" , 1.0 , -1 , 0 ) , 
      new OxidationState( "Sm" , 2.0 , 0 , 27 ) , 
      new OxidationState( "Sm" , 3.0 , 1 , 229 ) } , 
      { new OxidationState( "Eu" , 0.0 , 0 , 160 ) , 
      new OxidationState( "Eu" , 2.0 , 1 , 143 ) , 
      new OxidationState( "Eu" , 3.0 , 1 , 140 ) , 
      new OxidationState( "Eu" , 4.0 , -1 , 4 ) } , 
      { new OxidationState( "Gd" , 0.0 , 0 , 242 ) , 
      new OxidationState( "Gd" , 1.0 , 0 , 1 ) , 
      new OxidationState( "Gd" , 1.5 , -1 , 24 ) , 
      new OxidationState( "Gd" , 2.0 , 0 , 10 ) , 
      new OxidationState( "Gd" , 3.0 , 1 , 230 ) , 
      new OxidationState( "Gd" , 4.0 , -1 , 3 ) } , 
      { new OxidationState( "Tb" , 0.0 , 0 , 239 ) , 
      new OxidationState( "Tb" , 1.0 , 0 , 5 ) , 
      new OxidationState( "Tb" , 2.0 , -1 , 9 ) , 
      new OxidationState( "Tb" , 3.0 , 1 , 130 ) , 
      new OxidationState( "Tb" , 4.0 , 0 , 29 ) } , 
      { new OxidationState( "Dy" , 0.0 , 0 , 214 ) , 
      new OxidationState( "Dy" , 2.0 , 0 , 12 ) , 
      new OxidationState( "Dy" , 3.0 , 1 , 142 ) } , 
      { new OxidationState( "Ho" , 0.0 , 0 , 233 ) , 
      new OxidationState( "Ho" , 2.0 , -1 , 9 ) , 
      new OxidationState( "Ho" , 3.0 , 1 , 166 ) } , 
      { new OxidationState( "Er" , 0.0 , 0 , 236 ) , 
      new OxidationState( "Er" , 1.0 , -1 , 1 ) , 
      //new OxidationState( "Er" , 1.57 , -1 , 15 ) ,  // Looks wrong again.
      new OxidationState( "Er" , 2.0 , -1 , 5 ) , 
      new OxidationState( "Er" , 3.0 , 1 , 198 ) } , 
      { new OxidationState( "Tm" , 0.0 , 0 , 146 ) , 
      new OxidationState( "Tm" , 2.0 , 0 , 13 ) , 
      new OxidationState( "Tm" , 3.0 , 1 , 99 ) } , 
      { new OxidationState( "Yb" , 0.0 , 0 , 204 ) , 
      new OxidationState( "Yb" , 2.0 , 0 , 59 ) , 
      new OxidationState( "Yb" , 3.0 , 1 , 190 ) } , 
      { new OxidationState( "Lu" , 0.0 , 0 , 142 ) , 
      new OxidationState( "Lu" , 2.0 , -1 , 1 ) , 
      new OxidationState( "Lu" , 3.0 , 1 , 113 ) } , 
      { new OxidationState( "Hf" , 0.0 , 0 , 155 ) , 
      new OxidationState( "Hf" , 2.0 , 0 , 5 ) , 
      new OxidationState( "Hf" , 3.0 , 0 , 6 ) , 
      new OxidationState( "Hf" , 4.0 , 1 , 97 ) } , 
      { new OxidationState( "Ta" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Ta" , 0.0 , 0 , 117 ) , 
      new OxidationState( "Ta" , 1.0 , -1 , 7 ) , 
   //   new OxidationState( "Ta" , 1.2 , -1 , 39 ) ,  // Another one
      new OxidationState( "Ta" , 2.0 , 0 , 11 ) , 
      new OxidationState( "Ta" , 3.0 , 0 , 13 ) , 
      new OxidationState( "Ta" , 4.0 , 0 , 34 ) , 
      new OxidationState( "Ta" , 5.0 , 1 , 295 ) } , 
      { new OxidationState( "W" , -2.0 , 0 , 0 ) , 
      new OxidationState( "W" , -1.0 , 0 , 0 ) , 
      new OxidationState( "W" , 0.0 , 0 , 66 ) , 
      new OxidationState( "W" , 1.0 , 0 , 0 ) , 
      new OxidationState( "W" , 2.0 , 0 , 16 ) , 
      new OxidationState( "W" , 3.0 , 0 , 10 ) , 
      new OxidationState( "W" , 4.0 , 1 , 21 ) , 
      new OxidationState( "W" , 5.0 , 0 , 28 ) , 
      new OxidationState( "W" , 6.0 , 1 , 310 ) } , 
      { new OxidationState( "Re" , -3.0 , 0 , 0 ) , 
      new OxidationState( "Re" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Re" , 0.0 , 0 , 80 ) , 
      new OxidationState( "Re" , 1.0 , 0 , 11 ) , 
      new OxidationState( "Re" , 2.0 , 0 , 5 ) , 
      new OxidationState( "Re" , 3.0 , 0 , 49 ) , 
      new OxidationState( "Re" , 4.0 , 1 , 38 ) , 
      new OxidationState( "Re" , 5.0 , 0 , 55 ) , 
      new OxidationState( "Re" , 6.0 , 0 , 25 ) , 
      new OxidationState( "Re" , 7.0 , 0 , 89 ) } , 
      { new OxidationState( "Os" , -2.0 , 0 , 0 ) , 
      new OxidationState( "Os" , 0.0 , 0 , 131 ) , 
      new OxidationState( "Os" , 1.0 , 0 , 6 ) , 
      new OxidationState( "Os" , 2.0 , 0 , 12 ) , 
      new OxidationState( "Os" , 3.0 , 0 , 6 ) , 
      new OxidationState( "Os" , 4.0 , 1 , 12 ) , 
      new OxidationState( "Os" , 5.0 , 0 , 9 ) , 
      new OxidationState( "Os" , 6.0 , 0 , 8 ) , 
      new OxidationState( "Os" , 7.0 , 0 , 7 ) , 
      new OxidationState( "Os" , 8.0 , 0 , 5 ) } , 
      { new OxidationState( "Ir" , -1.0 , 0 , 0 ) , 
      new OxidationState( "Ir" , 0.0 , 0 , 251 ) , 
      new OxidationState( "Ir" , 1.0 , 0 , 2 ) , 
      new OxidationState( "Ir" , 2.0 , 0 , 10 ) , 
      new OxidationState( "Ir" , 3.0 , 1 , 37 ) , 
      new OxidationState( "Ir" , 4.0 , 1 , 43 ) , 
      new OxidationState( "Ir" , 5.0 , 0 , 33 ) , 
      new OxidationState( "Ir" , 6.0 , 0 , 4 ) } , 
      { new OxidationState( "Pt" , 0.0 , 0 , 410 ) , 
      new OxidationState( "Pt" , 2.0 , 1 , 93 ) , 
      new OxidationState( "Pt" , 3.0 , -1 , 8 ) , 
      new OxidationState( "Pt" , 4.0 , 1 , 119 ) , 
      new OxidationState( "Pt" , 5.0 , 0 , 2 ) , 
      new OxidationState( "Pt" , 6.0 , 0 , 2 ) } , 
      { 
    	  //new OxidationState( "Au" , -7.135 , -1 , 1 ) ,  // Really?
      new OxidationState( "Au" , -1.0 , 0 , 8 ) , 
      new OxidationState( "Au" , 0.0 , 0 , 323 ) , 
      new OxidationState( "Au" , 1.0 , 0 , 119 ) , 
      new OxidationState( "Au" , 2.0 , 0 , 9 ) , 
      new OxidationState( "Au" , 3.0 , 1 , 128 ) , 
      new OxidationState( "Au" , 5.0 , 0 , 6 ) } , 
      { new OxidationState( "Hg" , 0.0 , 0 , 125 ) , 
      new OxidationState( "Hg" , 1.0 , 1 , 63 ) , 
      new OxidationState( "Hg" , 2.0 , 1 , 366 ) , 
      new OxidationState( "Hg" , 4.0 , 0 , 0 ) } , 
      { new OxidationState( "Tl" , 0.0 , 0 , 134 ) , 
      new OxidationState( "Tl" , 1.0 , 1 , 433 ) , 
      new OxidationState( "Tl" , 2.0 , -1 , 0 ) , 
      new OxidationState( "Tl" , 3.0 , 1 , 103 ) } , 
      { new OxidationState( "Pb" , -4.0 , 0 , 1 ) , 
      new OxidationState( "Pb" , 0.0 , 0 , 214 ) , 
      new OxidationState( "Pb" , 1.0 , -1 , 1 ) , 
      new OxidationState( "Pb" , 2.0 , 1 , 539 ) , 
      new OxidationState( "Pb" , 4.0 , 1 , 65 ) } , 
      { new OxidationState( "Bi" , -3.0 , 0 , 2 ) , 
      new OxidationState( "Bi" , -2.0 , -1 , 2 ) , 
      new OxidationState( "Bi" , -1.0 , -1 , 0 ) , 
      new OxidationState( "Bi" , 0.0 , 0 , 215 ) , 
      new OxidationState( "Bi" , 1.0 , -1 , 8 ) , 
      new OxidationState( "Bi" , 2.0 , -1 , 10 ) , 
      new OxidationState( "Bi" , 3.0 , 1 , 496 ) , 
      new OxidationState( "Bi" , 4.0 , -1 , 0 ) , 
      new OxidationState( "Bi" , 5.0 , 0 , 41 ) } , 
      { new OxidationState( "Po" , -2.0 , 1 , 0 ) , 
      new OxidationState( "Po" , 0.0 , 0 , 22 ) , 
      new OxidationState( "Po" , 2.0 , 1 , 1 ) , 
      new OxidationState( "Po" , 4.0 , 1 , 3 ) , 
      new OxidationState( "Po" , 6.0 , 0 , 0 ) } , 
      { new OxidationState( "At" , -1.0 , 1 , -1 ) , 
      new OxidationState( "At" , 0.0 , 0 , -1 ) , 
      new OxidationState( "At" , 1.0 , 1 , -1 ) , 
      new OxidationState( "At" , 3.0 , 0 , -1 ) , 
      new OxidationState( "At" , 5.0 , 0 , -1 ) , 
      new OxidationState( "At" , 7.0 , 0 , -1 ) } , 
      { new OxidationState( "Rn" , 0.0 , 0 , -1 ) , 
      new OxidationState( "Rn" , 2.0 , 0 , -1 ) } , 
      { new OxidationState( "Fr" , 0.0 , 0 , -1 ) , 
      new OxidationState( "Fr" , 1.0 , 1 , -1 ) } , 
      { new OxidationState( "Ra" , 0.0 , 0 , 0 ) , 
      new OxidationState( "Ra" , 2.0 , 1 , 3 ) } , 
      { new OxidationState( "Ac" , 0.0 , 0 , 1 ) , 
      new OxidationState( "Ac" , 3.0 , 1 , 5 ) } , 
      { new OxidationState( "Th" , -2.0 , -1 , 0 ) , 
      new OxidationState( "Th" , -1.0 , -1 , 0 ) , 
      new OxidationState( "Th" , 0.0 , 0 , 149 ) , 
      new OxidationState( "Th" , 1.0 , -1 , 0 ) , 
      new OxidationState( "Th" , 2.0 , 0 , 7 ) , 
      new OxidationState( "Th" , 3.0 , 0 , 9 ) , 
      new OxidationState( "Th" , 4.0 , 1 , 124 ) , 
      new OxidationState( "Th" , 6.0 , -1 , 0 ) } , 
      { new OxidationState( "Pa" , 0.0 , 0 , 4 ) , 
      new OxidationState( "Pa" , 3.0 , 0 , 2 ) , 
      new OxidationState( "Pa" , 4.0 , 0 , 6 ) , 
      new OxidationState( "Pa" , 5.0 , 1 , 6 ) } , 
      { new OxidationState( "U" , 0.0 , 0 , 238 ) , 
      new OxidationState( "U" , 1.0 , -1 , 0 ) , 
      new OxidationState( "U" , 2.0 , -1 , 4 ) , 
      new OxidationState( "U" , 3.0 , 0 , 31 ) , 
      new OxidationState( "U" , 4.0 , 0 , 125 ) , 
      new OxidationState( "U" , 5.0 , 0 , 40 ) , 
      new OxidationState( "U" , 6.0 , 1 , 264 ) } , 
      { new OxidationState( "Np" , 0.0 , 0 , 62 ) , 
      new OxidationState( "Np" , 2.0 , -1 , 4 ) , 
      new OxidationState( "Np" , 3.0 , 0 , 14 ) , 
      new OxidationState( "Np" , 4.0 , 0 , 11 ) , 
      new OxidationState( "Np" , 5.0 , 1 , 11 ) , 
      new OxidationState( "Np" , 6.0 , 0 , 9 ) , 
      new OxidationState( "Np" , 7.0 , 0 , 0 ) } , 
      { new OxidationState( "Pu" , 0.0 , 0 , 83 ) , 
      new OxidationState( "Pu" , 2.0 , -1 , 5 ) , 
      new OxidationState( "Pu" , 3.0 , 0 , 22 ) , 
      new OxidationState( "Pu" , 4.0 , 1 , 9 ) , 
      new OxidationState( "Pu" , 5.0 , 0 , 1 ) , 
      new OxidationState( "Pu" , 6.0 , 0 , 1 ) , 
      new OxidationState( "Pu" , 7.0 , 0 , 0 ) } , 
      { new OxidationState( "Am" , 0.0 , 0 , 10 ) , 
      new OxidationState( "Am" , 2.0 , 0 , 5 ) , 
      new OxidationState( "Am" , 3.0 , 1 , 12 ) , 
      new OxidationState( "Am" , 4.0 , 0 , 2 ) , 
      new OxidationState( "Am" , 5.0 , 0 , 2 ) , 
      new OxidationState( "Am" , 6.0 , 0 , 0 ) } , 
      { new OxidationState( "Cm" , 0.0 , 0 , 16 ) , 
      new OxidationState( "Cm" , 2.0 , -1 , 5 ) , 
      new OxidationState( "Cm" , 3.0 , 1 , 14 ) , 
      new OxidationState( "Cm" , 4.0 , 0 , 4 ) } , 
      { new OxidationState( "Bk" , 0.0 , 0 , 4 ) , 
      new OxidationState( "Bk" , 2.0 , -1 , 1 ) , 
      new OxidationState( "Bk" , 3.0 , 1 , 5 ) , 
      new OxidationState( "Bk" , 4.0 , 0 , 2 ) } , 
      { new OxidationState( "Cf" , 0.0 , 0 , 1 ) , 
      new OxidationState( "Cf" , 2.0 , 0 , 1 ) , 
      new OxidationState( "Cf" , 3.0 , 1 , 6 ) , 
      new OxidationState( "Cf" , 4.0 , 0 , 1 ) } , 
      { new OxidationState( "Es" , 0.0 , 0 , -1 ) , 
      new OxidationState( "Es" , 2.0 , 0 , -1 ) , 
      new OxidationState( "Es" , 3.0 , 1 , -1 ) } , 
      { new OxidationState( "Fm" , 0.0 , 0 , -1 ) , 
      new OxidationState( "Fm" , 2.0 , 0 , -1 ) , 
      new OxidationState( "Fm" , 3.0 , 1 , -1 ) } , 
      { new OxidationState( "Md" , 0.0 , 0 , -1 ) , 
      new OxidationState( "Md" , 2.0 , 0 , -1 ) , 
      new OxidationState( "Md" , 3.0 , 1 , -1 ) } , 
      { new OxidationState( "No" , 0.0 , 0 , -1 ) , 
      new OxidationState( "No" , 2.0 , 0 , -1 ) , 
      new OxidationState( "No" , 3.0 , 1 , -1 ) } , 
      { new OxidationState( "Lr" , 0.0 , 0 , -1 ) , 
      new OxidationState( "Lr" , 3.0 , 1 , -1 ) } , 
      { new OxidationState( "Rf" , 0.0 , 0 , -1 ) , 
      new OxidationState( "Rf" , 4.0 , 1 , -1 ) } , 
  };
  
}