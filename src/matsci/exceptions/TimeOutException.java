/*
 * Created on Dec 8, 2009
 *
 */
package matsci.exceptions;

public class TimeOutException extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = -1315150041904152655L;

  public TimeOutException(String message) {
    super(message);
  }
  
  public TimeOutException(String message, Exception e) {
    super(message, e);
  }
  
  public TimeOutException() {
    super();
  }

}
