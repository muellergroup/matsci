/*
 * Created on Mar 25, 2016
 *
 */
package matsci.util.arrays;

import java.util.Random;

import matsci.location.basis.CartesianBasis;
import matsci.util.arrays.ArrayUtils;
import matsci.util.MSMath;

public class Matrix3D {
  
  private final double[][] m_Matrix;
  private double[][] m_Inverse;
  
  private double[][] m_EigenVectorsReal;
  private double[] m_EigenValuesReal;
  //private double[][] m_EigvenVectorsImaginary;
  private double[] m_EigenValuesImaginary;
  
  //private static double EIGENVALUE_TOLERANCE = 1E-7;  // I've seen the acos/ cos tranformations  introduce errors this large.

  public Matrix3D(double[][] matrix) {
    
    m_Matrix = ArrayUtils.copyArray(matrix);
    
  }
  
  public boolean isDiagonal() {
    return (m_Matrix[0][1] == 0) && (m_Matrix[0][2] == 0) && (m_Matrix[1][2] == 0) && (m_Matrix[1][0] == 0) && (m_Matrix[2][0] == 0) && (m_Matrix[2][1] == 0);
  }
  
  public double[][] inverse() {
    
      if (m_Inverse == null) {

        m_Inverse = this.calculateRealInverse(m_Matrix);
        
      }
      
      return ArrayUtils.copyArray(m_Inverse);
  }
  
  private double[][] calculateRealInverse(double[][] realMatrix) {
    double[][] inverse = new double[3][3];
    
    double determinant = this.determinant(realMatrix);
    inverse[0][0] = (realMatrix[1][1] * realMatrix[2][2] -
        realMatrix[1][2] * realMatrix[2][1]) / determinant;
    inverse[0][1] = (realMatrix[0][2] * realMatrix[2][1] -
        realMatrix[2][2] * realMatrix[0][1]) / determinant;
    inverse[0][2] = (realMatrix[0][1] * realMatrix[1][2] -
        realMatrix[0][2] * realMatrix[1][1]) / determinant;
    inverse[1][0] = (realMatrix[1][2] * realMatrix[2][0] -
        realMatrix[1][0] * realMatrix[2][2]) / determinant;
    inverse[1][1] = (realMatrix[0][0] * realMatrix[2][2] -
        realMatrix[0][2] * realMatrix[2][0]) / determinant;
    inverse[1][2] = (realMatrix[1][0] * realMatrix[0][2] -
        realMatrix[1][2] * realMatrix[0][0]) / determinant;
    inverse[2][0] = (realMatrix[1][0] * realMatrix[2][1] -
        realMatrix[1][1] * realMatrix[2][0]) / determinant;
    inverse[2][1] = (realMatrix[0][1] * realMatrix[2][0] -
        realMatrix[0][0] * realMatrix[2][1]) / determinant;
    inverse[2][2] = (realMatrix[1][1] * realMatrix[0][0] -
        realMatrix[1][0] * realMatrix[0][1]) / determinant;
    
    return inverse;
  }
  
  public double determinant() {
    return this.determinant(m_Matrix);
  }
  
  public double determinant(double[][] matrix) {
    return
        matrix[0][0] *
        (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1]) -
        matrix[0][1] *
        (matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[2][0]) +
        matrix[0][2] *
        (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
  }
  
  public double[] getEigenValuesRealPart() {
    this.findEigenValues();
    return ArrayUtils.copyArray(m_EigenValuesReal);
  }
  
  public double[] getEigenValuesImaginaryPart() {
    this.findEigenValues();
    return ArrayUtils.copyArray(m_EigenValuesImaginary);
  }
  
  public boolean isEigenvalueReal(int eigenValueNum) {
    this.findEigenValues();
    return m_EigenValuesImaginary[eigenValueNum] == 0;
  }
  
  public double[] getRealEigenVector(int eigenValueNum) {
    this.findRealEigenVectors();
    return m_EigenVectorsReal[eigenValueNum];
  }
  
  public double[][] getRealEigenVectors() {
    this.findRealEigenVectors();
    return ArrayUtils.copyArray(m_EigenVectorsReal);
  }
  
  /**
   * From https://en.wikipedia.org/wiki/Eigenvalue_algorithm
   */
  private void findEigenValues() {
    
    if (m_EigenValuesReal != null) {return;}
    
    m_EigenValuesReal = new double[3];
    m_EigenValuesImaginary = new double[3];
    if (this.isDiagonal()) {
      m_EigenValuesReal[0] = m_Matrix[0][0];
      m_EigenValuesReal[1] = m_Matrix[1][1];
      m_EigenValuesReal[2] = m_Matrix[2][2];
    } else {
      double p1 = m_Matrix[0][2] * m_Matrix[2][0] + m_Matrix[1][2] * m_Matrix[2][1] + m_Matrix[0][1] * m_Matrix[1][0];
      double q = (m_Matrix[0][0] + m_Matrix[1][1] + m_Matrix[2][2]) / 3;
      double[][] qI = ArrayUtils.diagonalMatrix(3, q);
      double[][] matrixMinusqI = MSMath.arrayDiff(m_Matrix, qI);
      double p2 = matrixMinusqI[0][0] * matrixMinusqI[0][0] + matrixMinusqI[1][1] * matrixMinusqI[1][1] + matrixMinusqI[2][2] * matrixMinusqI[2][2] + 2 * p1;  
      boolean isPReal = (p2 >= 0);
      double p = isPReal ? Math.sqrt(p2 / 6) : Math.sqrt(-p2 / 6);

      double det_Bp = this.determinant(matrixMinusqI); // This is det(B * p)
      double r = det_Bp / (p * p * p * 2); // This is det(B) / 2
      double zTerm = r + Math.sqrt(r*r + (isPReal ? -1 : 1));
      
      if ((p==0) || Math.abs(r) > 1E15 || (zTerm == 0)) { // Deal with p=0 and possible numerical issues.  Same whether p is real or imaginary.
        m_EigenValuesReal[0] = q + Math.cbrt(det_Bp);
        m_EigenValuesReal[2] = q - 0.5 * Math.cbrt(det_Bp);
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = Math.sqrt(3)/2 * Math.cbrt(det_Bp);
      } else if (!isPReal) { // There are special values here around p = +/- i but not worth dealing with explicitly here.
        //double zTerm = r + Math.sqrt(r*r + 1);
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTerm) - 1/Math.cbrt(zTerm));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTerm) * Math.cos(4*Math.PI/3) - (1/Math.cbrt(zTerm)) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTerm) * Math.sin(4*Math.PI/3) - (1/Math.cbrt(zTerm)) * Math.sin(-4*Math.PI/3));
        
        // The below is mathematically equivalent but probably not as efficient
        /*double zTermPos = r + Math.sqrt(r*r + 1);
        double zTermNeg = Math.sqrt(r*r + 1) - r;
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTermPos) - Math.cbrt(zTermNeg));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTermPos) * Math.cos(4*Math.PI/3) - (Math.cbrt(zTermNeg)) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTermPos) * Math.sin(4*Math.PI/3) - (Math.cbrt(zTermNeg)) * Math.sin(-4*Math.PI/3));*/
      } else if ((r > -1) && (r < 1)) {
        double phi = Math.acos(r) / 3;
        m_EigenValuesReal[0] = q + 2 * p * Math.cos(phi);
        m_EigenValuesReal[2] = q + 2 * p * Math.cos(phi + (2*Math.PI/3));
      } else if ((r <= -1) && (r > -1 - 1E-14)) { // Same answer whether p is real or imaginary
        m_EigenValuesReal[0] = q + p;
        m_EigenValuesReal[2] = q - 2 * p;
      } else if ((r >= 1) && (r < 1 + 1E-14)) { // Same answer whether p is real or imaginary
        m_EigenValuesReal[0] = q + 2 * p;
        m_EigenValuesReal[2] = q - p;
      } else {
        //double zTerm = r + Math.sqrt(r*r - 1);
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTerm) +  1/Math.cbrt(zTerm));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTerm) * Math.cos(4*Math.PI/3) + Math.cbrt(1/zTerm) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTerm) * Math.sin(4*Math.PI/3) + (Math.cbrt(1/zTerm)) * Math.sin(-4*Math.PI/3));

        // The below is mathematically equivalent but probably not as efficient
        /*double zTermPos = r + Math.sqrt(r*r - 1);
        double zTermNeg = r - Math.sqrt(r*r - 1);
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTermPos) +  Math.cbrt(zTermNeg));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTermPos) * Math.cos(4*Math.PI/3) + Math.cbrt(zTermNeg) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTermPos) * Math.sin(4*Math.PI/3) + (Math.cbrt(zTermNeg)) * Math.sin(-4*Math.PI/3));
        */
      }
      // The below is pre-cleanup
      /*if ((r >= -1) && (r <= 1)) {
        if (!isPReal) {
          System.out.println("Error");
          double zTerm = r + Math.sqrt(r*r + 1);
          m_EigenValuesReal[0] = q + p * (Math.cbrt(zTerm) - 1/Math.cbrt(zTerm));
          m_EigenValuesReal[2] = q + p * (Math.cbrt(zTerm) * Math.cos(4*Math.PI/3) - (1/Math.cbrt(zTerm)) * Math.cos(-4*Math.PI/3));
          m_EigenValuesImaginary[0] = 0;
          m_EigenValuesImaginary[2] = p * (Math.cbrt(zTerm) * Math.sin(4*Math.PI/3) - (1/Math.cbrt(zTerm)) * Math.sin(-4*Math.PI/3));
        } else {
          double phi = Math.acos(r) / 3;
          m_EigenValuesReal[0] = q + 2 * p * Math.cos(phi);
          m_EigenValuesReal[2] = q + 2 * p * Math.cos(phi + (2*Math.PI/3));
        }
      } else if ((r < -1) && (r >= -1 - 1E-14)) { // Same answer whether p is real or imaginary
        m_EigenValuesReal[0] = q + p;
        m_EigenValuesReal[2] = q - 2 * p;
      } else if ((r > 1) && (r <= 1 + 1E-14)) { // Same answer whether p is real or imaginary
        m_EigenValuesReal[0] = q + 2 * p;
        m_EigenValuesReal[2] = q - p;
      } else if ((p==0) || Math.abs(r) > 1E15) { // Deal with p=0 and possible numerical issues
        m_EigenValuesReal[0] = q + Math.cbrt(det_Bp);
        m_EigenValuesReal[2] = q - 0.5 * Math.cbrt(det_Bp);
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = Math.sqrt(3)/2 * Math.cbrt(det_Bp);
      } else if (isPReal) {
        double zTerm = r + Math.sqrt(r*r - 1);
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTerm) +  1/Math.cbrt(zTerm));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTerm) * Math.cos(4*Math.PI/3) + Math.cbrt(1/zTerm) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTerm) * Math.sin(4*Math.PI/3) + (Math.cbrt(1/zTerm)) * Math.sin(-4*Math.PI/3));
*/
        // The below is mathematically equivalent but probably not as efficient
        /*double zTermPos = r + Math.sqrt(r*r - 1);
        double zTermNeg = r - Math.sqrt(r*r - 1);
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTermPos) +  Math.cbrt(zTermNeg));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTermPos) * Math.cos(4*Math.PI/3) + Math.cbrt(zTermNeg) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTermPos) * Math.sin(4*Math.PI/3) + (Math.cbrt(zTermNeg)) * Math.sin(-4*Math.PI/3));
        */
/*      } else {
        double zTerm = r + Math.sqrt(r*r + 1);
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTerm) - 1/Math.cbrt(zTerm));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTerm) * Math.cos(4*Math.PI/3) - (1/Math.cbrt(zTerm)) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTerm) * Math.sin(4*Math.PI/3) - (1/Math.cbrt(zTerm)) * Math.sin(-4*Math.PI/3));
        */
        // The below is mathematically equivalent but probably not as efficient
        /*double zTermPos = r + Math.sqrt(r*r + 1);
        double zTermNeg = Math.sqrt(r*r + 1) - r;
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTermPos) - Math.cbrt(zTermNeg));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTermPos) * Math.cos(4*Math.PI/3) - (Math.cbrt(zTermNeg)) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTermPos) * Math.sin(4*Math.PI/3) - (Math.cbrt(zTermNeg)) * Math.sin(-4*Math.PI/3));
        System.currentTimeMillis();*/
      //}
        
        // The below caused problems with the Gus Hart example on May-June 2018
      /*if (Math.abs(p2) < 1E-13) { // For numerical issues.  Part of code that cuase problems May-Jun 2018
        if (Math.abs(q) < 1E-13) { // TODO maybe replace this with a comparison of Tr(A^2) to Tr(A)
          double determinant = MSMath.determinant(matrixMinusqI); // TODO consider using m_Matrix here.
          double cbrtDeterminant = Math.cbrt(determinant);
          m_EigenValuesReal[0] = cbrtDeterminant;
          m_EigenValuesReal[1] = -cbrtDeterminant / 2;
          m_EigenValuesReal[2] = -cbrtDeterminant / 2;
          m_EigenValuesImaginary[0] = 0;
          m_EigenValuesImaginary[1] = cbrtDeterminant * Math.sqrt(3) / 2;
          m_EigenValuesImaginary[2] = -cbrtDeterminant * Math.sqrt(3) / 2;
        } else {
          m_EigenValuesReal[0] = q;
          m_EigenValuesReal[1] = q;
          m_EigenValuesReal[2] = q;
        }
        return;
      }
      double[][] b = MSMath.arrayDivide(matrixMinusqI, p);
      double r = this.determinant(b) / 2;
      
      if ((r >= -1) && (r <= 1)) {
        double phi = Math.acos(r) / 3;
        m_EigenValuesReal[0] = q + 2 * p * Math.cos(phi);
        m_EigenValuesReal[2] = q + 2 * p * Math.cos(phi + (2*Math.PI/3));
      } else if ((r < -1) && (r >= -1 - 1E-14)) {
        m_EigenValuesReal[0] = q + p;
        m_EigenValuesReal[2] = q - 2 * p;
      } else if ((r > 1) && (r < 1 + 1E-14)) {
        m_EigenValuesReal[0] = q + 2 * p;
        m_EigenValuesReal[2] = q - p;
      } else if (isPReal) {
        double zTerm = r + Math.sqrt(r*r - 1);
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTerm) +  1/Math.cbrt(zTerm));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTerm) * Math.cos(4*Math.PI/3) + (1/Math.cbrt(zTerm)) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTerm) * Math.sin(4*Math.PI/3) + (1/Math.cbrt(zTerm)) * Math.sin(-4*Math.PI/3));
      } else {
        double zTerm = r + Math.sqrt(r*r + 1);
        m_EigenValuesReal[0] = q + p * (Math.cbrt(zTerm) - 1/Math.cbrt(zTerm));
        m_EigenValuesReal[2] = q + p * (Math.cbrt(zTerm) * Math.cos(4*Math.PI/3) - (1/Math.cbrt(zTerm)) * Math.cos(-4*Math.PI/3));
        m_EigenValuesImaginary[0] = 0;
        m_EigenValuesImaginary[2] = p * (Math.cbrt(zTerm) * Math.sin(4*Math.PI/3) - (1/Math.cbrt(zTerm)) * Math.sin(-4*Math.PI/3));
      }*/
      m_EigenValuesReal[1] = 3 * q - m_EigenValuesReal[0]  - m_EigenValuesReal[2]; //since trace(A) = eig1 + eig2 + eig3
      m_EigenValuesImaginary[1] = 0 - m_EigenValuesImaginary[0]  - m_EigenValuesImaginary[2]; //since trace(A) = eig1 + eig2 + eig3

      // Some numerical cleanup for nearly-real eigenvalues
      for (int eigNum = 0; eigNum < m_EigenValuesReal.length; eigNum++) {
        if (m_EigenValuesReal[eigNum] == 0) {continue;}
        double angle = Math.atan(m_EigenValuesImaginary[eigNum] / m_EigenValuesReal[eigNum]);
        if (Math.abs(angle) < CartesianBasis.getAngleToleranceRadians()) {
          double sign = Math.signum(m_EigenValuesReal[eigNum]);
          m_EigenValuesReal[eigNum] = sign * Math.sqrt(m_EigenValuesReal[eigNum] * m_EigenValuesReal[eigNum] + m_EigenValuesImaginary[eigNum] * m_EigenValuesImaginary[eigNum]);
          m_EigenValuesImaginary[eigNum] = 0;
        }
      }
    }
  }
  
  /*
  private double[] findRealEigenVectorNew(int vecNum) {
    
    double tolerance = 1E-15;
    int vecNum2 = (vecNum + 1) % 3;
    int vecNum3 = (vecNum + 2) % 3;
    
    double[][] matrixMinus2I = ArrayUtils.copyArray(m_Matrix);
    for (int dimNum = 0; dimNum < 3; dimNum++) {
      matrixMinus2I[dimNum][dimNum] -= m_EigenValuesReal[vecNum2];
    }
    
    double[][] matrixMinus3I = ArrayUtils.copyArray(m_Matrix);
    for (int dimNum = 0; dimNum < 3; dimNum++) {
      matrixMinus3I[dimNum][dimNum] -= m_EigenValuesReal[vecNum3];
    }
    
    double[][] product = MSMath.matrixMultiply(matrixMinus2I, matrixMinus3I);
    for (int dimNum = 0; dimNum < 3; dimNum++) {
      product[dimNum][dimNum] -= m_EigenValuesImaginary[vecNum2] * m_EigenValuesImaginary[vecNum3];
    }
    
    // Find the linearly independent vectors
    product = MSMath.transpose(product);
    for (int rowNum = 2; rowNum >= 0; rowNum--) {
      double magnitude = MSMath.magnitude(product[rowNum]);
      if (magnitude < tolerance) {
        product = ArrayUtils.removeElement(product, rowNum);
      } else {
        product[rowNum] = MSMath.arrayDivide(product[rowNum], magnitude);
        for (int rowNum2 = rowNum + 1; rowNum2 < 3; rowNum2++) {
          double innerProduct = MSMath.dotProduct(product[rowNum], product[rowNum2]);
          if (Math.abs(innerProduct - 1) < tolerance || Math.abs(innerProduct + 1) < tolerance) {
            product = ArrayUtils.removeElement(product, rowNum);
            break;
          }
        }
      }
    }
    
    if (product.length == 0) {
      double[] returnArray = new double[3];
      returnArray[vecNum] = 1;
      return returnArray;
    } else if (product.length == 1) {
      return product[0];
    } else if (product.length == 2) {
      
    }
    
    
    
  }*/
  
  /**
   * TODO: Later, create another method for complex eigenvectors.  For now, we just need
   * the real ones.
   */
  private void findRealEigenVectors() {
    
    if (m_EigenVectorsReal != null) {return;}
    
    m_EigenVectorsReal = new double[3][];
    for (int valNum = 0; valNum < m_EigenVectorsReal.length; valNum++) {
      if (!isEigenvalueReal(valNum)) {continue;}
      m_EigenVectorsReal[valNum] = this.findRealEigenVector(valNum);
    }
    
   // System.currentTimeMillis();
    
    /*for (int valNum = 0; valNum < m_EigenVectorsReal.length; valNum++) {
      if (!isEigenvalueReal(valNum)) {continue;}
      if (m_EigenVectorsReal[valNum] != null) {continue;} // Probably a duplicate eigenvalue
      double[][] eigenVectors = findRealEigenVectors(valNum);
      if (eigenVectors.length == 0) {
        throw new RuntimeException("Unable to find eigenvector for real eigenvalue.");
      } 
      m_EigenVectorsReal[valNum] = eigenVectors[0];
      
      if (eigenVectors.length == 1) {
        continue;
      }
      
      // We have more than one eigenvector for a given eigenValue.  We need to match things up.
      int vecIndex = 1;
      for (int valNum2 = valNum + 1; valNum2 < m_EigenValuesReal.length; valNum2++) {
        if (!isEigenvalueReal(valNum2)) {continue;}
        if (m_EigenVectorsReal[valNum2] != null) {continue;}
        double delta = Math.abs(m_EigenValuesReal[valNum] - m_EigenValuesReal[valNum2]);
        if (delta < EIGENVALUE_TOLERANCE) { // Probably numerical noise
          if (vecIndex > eigenVectors.length - 1) { // there's a name for this type of matrix, but I forget what it is -- not enough eigenvectors
            m_EigenVectorsReal[valNum2] = eigenVectors[0]; // This is arbitrary, but if we don't do this, then the code could get confused when it later gets multiple eigenvectors for valNum2.
          } else {
            m_EigenVectorsReal[valNum2] = eigenVectors[vecIndex++];
          }
        }
      }
      if (vecIndex != eigenVectors.length) {
        throw new RuntimeException("Found more eigenvectors than corresponding eigenvalues");
      }
      
    }*/
    
  }
  

  /*private double[][] findRealEigenVectors(int valNum) {
    
    if (m_EigenValuesImaginary[valNum] != 0) {
      throw new RuntimeException("Can't find real eigenvector for complex eigenvalue");
    }

    double eigenValue = m_EigenValuesReal[valNum];
    
    double perturbation = CartesianBasis.getAngleToleranceRadians() / 10;  // This seems to work OK.
    double[][] realMatrix = MSMath.transpose(m_Matrix);
    
    realMatrix[0][0] -= m_EigenValuesReal[valNum] - perturbation;
    realMatrix[1][1] -= m_EigenValuesReal[valNum] - perturbation;
    realMatrix[2][2] -= m_EigenValuesReal[valNum] - perturbation;
    
    double[][] inverse = this.calculateRealInverse(realMatrix);
    
    double[][] eigenVectors = new double[3][];
    double[] angles = new double[3];
    int numEigenVectors = 0;
    
    for (int vecNum = 0; vecNum < eigenVectors.length; vecNum++) {

      double[] origVector = MSMath.normalize(inverse[vecNum]);
      double[] eigenVector = origVector;
      
      double angle = 1;
      double eigenValueEstimate = 0;
      int passNum = 0;
      boolean badVec = false;
      while ((Math.abs(angle) > CartesianBasis.getAngleToleranceRadians()) && (Math.abs(Math.PI - angle) > CartesianBasis.getAngleToleranceRadians())) {
        double[] nextVector = MSMath.normalize(MSMath.vectorTimesMatrix(eigenVector, inverse));
        
        // To stop us from going in circles
        if ((passNum > 8) && Math.abs(angle) > Math.PI / 8) {
          badVec = true;
          break;
        }
        
        double dotProduct = MSMath.dotProduct(eigenVector, nextVector);
        dotProduct = Math.min(dotProduct, 1); // For numerical issues.
        dotProduct = Math.max(dotProduct, -1); // For numerical issues.

        angle = Math.acos(dotProduct);
        eigenVector = nextVector;
        passNum++;
      } 
      if (badVec) {continue;}
      
      double[] matrixTimesNextVector = MSMath.matrixTimesVector(m_Matrix, eigenVector);
      eigenValueEstimate = MSMath.magnitude(matrixTimesNextVector);
      if (MSMath.dotProduct(eigenVector, matrixTimesNextVector) < 0) {eigenValueEstimate *= -1;}
      
      double eigenValueDelta = Math.abs(eigenValueEstimate - eigenValue);
      boolean wrongEigenvector = false;
      for (int valNum2 = 0; valNum2 < m_EigenValuesReal.length; valNum2++) {
        if (Math.abs(m_EigenValuesReal[valNum2] - eigenValueEstimate) < eigenValueDelta - EIGENVALUE_TOLERANCE) {
          wrongEigenvector = true; // It's closer to another eigenvalue
          break;
        }
      }
      
      if (wrongEigenvector) {continue;}
      
      eigenVectors[vecNum] = eigenVector;
      angles[vecNum] = angle;
      numEigenVectors++;
    }
    
    // Remove duplicates
    for (int vecNum = 0; vecNum < eigenVectors.length; vecNum++) {
      if (eigenVectors[vecNum] == null) {continue;}
      for (int vecNum2 = vecNum + 1; vecNum2 < eigenVectors.length; vecNum2++) {
        if (eigenVectors[vecNum2] == null) {continue;}
        double dotProduct = MSMath.dotProduct(eigenVectors[vecNum], eigenVectors[vecNum2]);
        dotProduct = Math.min(dotProduct, 1); // For numerical issues.
        dotProduct = Math.max(dotProduct, -1); // For numerical issues.
        double angle = Math.acos(dotProduct);
        if ((Math.abs(angle) < CartesianBasis.getAngleToleranceRadians()) || (Math.abs(Math.PI - angle) < CartesianBasis.getAngleToleranceRadians())) {
          if (angles[vecNum] > angles[vecNum2]) {
            eigenVectors[vecNum] = null;
            numEigenVectors--;
            break;
          } else {
            eigenVectors[vecNum2] = null;
            numEigenVectors--;
          }
        }
      }
    }
    
    // Remove vectors that are spanned by previous vectors
    if (numEigenVectors == 3) {
      double determinant = this.determinant(eigenVectors);
      if (Math.abs(determinant) < Math.sin(CartesianBasis.getAngleToleranceRadians())) {
        int leastOrthogonal = 0;
        double minDotProduct = MSMath.dotProduct(eigenVectors[1], eigenVectors[2]);
        double dotProduct = MSMath.dotProduct(eigenVectors[0], eigenVectors[2]);
        if (dotProduct < minDotProduct) {
          minDotProduct = dotProduct;
          leastOrthogonal = 1;
        }        
       dotProduct = MSMath.dotProduct(eigenVectors[0], eigenVectors[1]);
        if (dotProduct < minDotProduct) {
          leastOrthogonal = 2;
        }
        eigenVectors[leastOrthogonal] = null;
        numEigenVectors--;
      }
    }
    
    // Remove nulls
    if (numEigenVectors != 3) {
      for (int vecNum = eigenVectors.length - 1; vecNum >= 0; vecNum--) {
        if (eigenVectors[vecNum] == null) {
          eigenVectors = ArrayUtils.removeElement(eigenVectors, vecNum);
        }
      }
    }
    
    return eigenVectors;
  }*/
  
  private double[] findRealEigenVector(int valNum) {
    
    //findRealEigenVectorNew(valNum);
    
    // TODO replace this with method based on columns in products of matrices (see wikipedia) 
    
    if (m_EigenValuesImaginary[valNum] != 0) {
      throw new RuntimeException("Can't find real eigenvector for complex eigenvalue");
    }

    double[][] realMatrix = MSMath.transpose(m_Matrix);
    double perturbation = CartesianBasis.getAngleToleranceRadians() / 10;  // This seems to work OK.
    realMatrix[0][0] -= m_EigenValuesReal[valNum] - perturbation;
    realMatrix[1][1] -= m_EigenValuesReal[valNum] - perturbation;
    realMatrix[2][2] -= m_EigenValuesReal[valNum] - perturbation;
    /*double pertMag = 1E-5;
    
    realMatrix[0][0] -= m_EigenValuesReal[valNum] * (1 + pertMag);
    realMatrix[1][1] -= m_EigenValuesReal[valNum] * (1 + pertMag);
    realMatrix[2][2] -= m_EigenValuesReal[valNum] * (1 + pertMag);*/
    
    double[][] inverse = this.calculateRealInverse(realMatrix);
    
    Random generator = new Random();
    double[] eigenVector = new double[] {
        (generator.nextDouble() - 0.5) * 1E-5,
        (generator.nextDouble() - 0.5) * 1E-5,
        (generator.nextDouble() - 0.5) * 1E-5
    };
    eigenVector[valNum] += 1;
    eigenVector = MSMath.normalize(eigenVector);
  
    double angle = 1;
    int numPasses = 0;
    while ((Math.abs(angle) > CartesianBasis.getAngleToleranceRadians() / 100) && (Math.abs(Math.PI - angle) > CartesianBasis.getAngleToleranceRadians() / 100)) {
      double[] nextVector = MSMath.normalize(MSMath.vectorTimesMatrix(eigenVector, inverse));
      
      double dotProduct = MSMath.dotProduct(eigenVector, nextVector);
      dotProduct = Math.min(dotProduct, 1); // For numerical issues.
      dotProduct = Math.max(dotProduct, -1); // For numerical issues.

      angle = Math.acos(dotProduct);
      eigenVector = nextVector;
      
      if (numPasses > 20) { // Sometimes we get stuck (e.g. going in a circle)
        double tolerance =  CartesianBasis.getAngleToleranceRadians() * (numPasses / 20.0);
        tolerance = Math.min(tolerance, 1E-6);
        if ((Math.abs(angle) < tolerance) || (Math.abs(Math.PI - angle) < tolerance)) {
         break;
        }
      }
      numPasses++;
    } 
    
    //System.out.println(numPasses);
    
    return eigenVector;
  }
    
 /* private void reduceMatrices(double[][] realMatrix, double[][] imaginaryMatrix) {

    int maxRowNum = -1;
    double maxMagSq = 0;
    // Find the largest element of the first column.  This is the one we'll keep.
    for (int rowNum = 0; rowNum < 3; rowNum++) {
      double magSq = realMatrix[rowNum][0] * realMatrix[rowNum][0] + imaginaryMatrix[rowNum][0] * imaginaryMatrix[rowNum][0];
      if (magSq > maxMagSq) {
        maxMagSq = magSq;
        maxRowNum = rowNum;
      }
    }
    
    // Eliminate the other rows in the first column.
    if (maxMagSq != 0) {
      for (int rowNum = 0; rowNum < 3; rowNum++) {
        if (rowNum == maxRowNum) {continue;}
        double[] ratio = this.divideComplex(realMatrix[rowNum][0], imaginaryMatrix[rowNum][0], realMatrix[maxRowNum][0], imaginaryMatrix[maxRowNum][0]);
        
        for (int colNum = 0; colNum < realMatrix[rowNum].length; colNum++) {
          realMatrix[rowNum][colNum] -= realMatrix[maxRowNum][colNum] * ratio[0] - imaginaryMatrix[maxRowNum][colNum] * ratio[1];
          imaginaryMatrix[rowNum][colNum] -= realMatrix[maxRowNum][colNum] * ratio[1] + imaginaryMatrix[maxRowNum][colNum] * ratio[0];
        }
      }
    }
    
    // Find the largest element of the second column that isn't the one we kept in the first column.  This is the one we'll keep. 
    int maxRowNum2 = -1;
    double maxMagSq2 = 0;
    for (int rowNum = 0; rowNum < realMatrix.length; rowNum++) {
      if (rowNum == maxRowNum) {continue;}
      double magSq = realMatrix[rowNum][0] * realMatrix[rowNum][0] + imaginaryMatrix[rowNum][0] * imaginaryMatrix[rowNum][0];
      if (magSq > maxMagSq2) {
        maxMagSq2 = magSq;
        maxRowNum2 = rowNum;
      }
    }
    
    // Eliminate the other rows in the second column.
    if (maxMagSq2 != 0) {
      for (int rowNum = 0; rowNum < realMatrix.length; rowNum++) {
        if (rowNum == maxRowNum2) {continue;}
        double[] ratio = this.divideComplex(realMatrix[rowNum][1], imaginaryMatrix[rowNum][1], realMatrix[maxRowNum2][1], imaginaryMatrix[maxRowNum2][1]);
        
        for (int colNum = 1; colNum < realMatrix[rowNum].length; colNum++) {
          realMatrix[rowNum][colNum] -= realMatrix[maxRowNum2][colNum] * ratio[0] - imaginaryMatrix[maxRowNum2][colNum] * ratio[1];
          imaginaryMatrix[rowNum][colNum] -= realMatrix[maxRowNum2][colNum] * ratio[1] + imaginaryMatrix[maxRowNum2][colNum] * ratio[0];
        }
      }
    }
  }*/
  
  /*
  private double[] divideComplex(double realNumerator, double complexNumerator, double realDenominator, double complexDenominator) {
    double[] returnArray = new double[2];
    
    double scaleFactor = realDenominator * realDenominator + complexDenominator * complexDenominator;
    
    // The real value
    returnArray[0] = realNumerator * realDenominator + complexNumerator * complexDenominator / scaleFactor;
    
    // Complex value 
    returnArray[1] = -realNumerator * complexDenominator + complexNumerator * realDenominator / scaleFactor;
    
    return returnArray;
  }*/

}
