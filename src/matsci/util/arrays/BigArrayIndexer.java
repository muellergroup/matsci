/*
 * Created on Jun 6, 2006
 *
 */
package matsci.util.arrays;

import java.math.BigInteger;
import java.util.Arrays;

import matsci.util.arrays.ArrayIndexer.Filter;
import matsci.util.arrays.ArrayIndexer.TimeOutException;

public class BigArrayIndexer {


  private int[] m_DimensionSizes;
  private BigInteger[] m_ConversionCoefficients;
  private BigInteger m_NumStates = BigInteger.ONE;

  public BigArrayIndexer(int[] dimensionSizes) {
    m_DimensionSizes = dimensionSizes;

    m_ConversionCoefficients = new BigInteger[dimensionSizes.length];
    BigInteger multiplier = BigInteger.ONE;
    //for (int termNum = dimensionSizes.length - 1; termNum >= 0; termNum--) {
    for (int termNum = 0; termNum < dimensionSizes.length; termNum++) {
      m_ConversionCoefficients[termNum] = multiplier;
      multiplier = multiplier.multiply(BigInteger.valueOf(dimensionSizes[termNum]));
    }
    m_NumStates = multiplier;
  }

  public int[] splitValue(BigInteger value) {

    int[] returnArray = new int[m_ConversionCoefficients.length];
    BigInteger remainder = value;
    //for (int termNum = 0; termNum < m_ConversionCoefficients.length; termNum++) {
    for (int termNum = m_ConversionCoefficients.length -1; termNum >= 0; termNum--) {
      BigInteger multiplier = m_ConversionCoefficients[termNum];
      BigInteger[] divideAndRemainder = remainder.divideAndRemainder(multiplier);
      returnArray[termNum] = divideAndRemainder[0].intValue(); // integer division
      remainder = divideAndRemainder[1];
    }

    return returnArray;
  }

  public BigInteger joinValuesBounded(int[] terms) {

    BigInteger returnValue = BigInteger.ZERO;
    //for (int termNum = m_ConversionCoefficients.length -1; termNum >= 0; termNum--) {
    for (int termNum =0; termNum < m_ConversionCoefficients.length; termNum++) {
      long modTerm = terms[termNum] % m_DimensionSizes[termNum];
      modTerm = (modTerm < 0) ? modTerm + m_DimensionSizes[termNum] : modTerm;
      BigInteger term = BigInteger.valueOf(modTerm);
      returnValue = returnValue.add(term.multiply(m_ConversionCoefficients[termNum]));
    }
    return returnValue;
  }
  
  public BigInteger joinValues(int[] terms) {

    BigInteger returnValue = BigInteger.ZERO;
    //for (int termNum = m_ConversionCoefficients.length -1; termNum >= 0; termNum--) {
    for (int termNum =0; termNum < m_ConversionCoefficients.length; termNum++) {
      BigInteger term = BigInteger.valueOf(terms[termNum]);
      returnValue = returnValue.add(term.multiply(m_ConversionCoefficients[termNum]));
    }
    return returnValue;
  }
  
  public int[] getInitialState() {
    return new int[this.numDimensions()];
  }
  

  public int[] getInitialState(ArrayIndexer.Filter[] filters) {
    return this.getInitialState(filters, Double.POSITIVE_INFINITY);
  }
  
  public int[] getInitialState(ArrayIndexer.Filter[] filters, double timeOut) {
    
    int[] initialState = this.getInitialState();
    for (int filterNum = 0; filterNum < filters.length; filterNum++) {
      int branchIndex = filters[filterNum].getBranchIndex(initialState);
      if (branchIndex > -1) {
        if (!this.increment(initialState, filters, timeOut)) {
          return null;
        }
      }
    }
    
    return initialState;
  }
  
  public void setInitialState(int[] state) {
    Arrays.fill(state, 0);
  }

  public boolean increment(int[] oldValues) {

    if (oldValues.length != m_DimensionSizes.length) {
      throw new RuntimeException("Given array has the wrong number of dimensions for this indexer");
    }

    for (int termNum = 0; termNum < oldValues.length; termNum++) {
    //for (int termNum = oldValues.length - 1; termNum >= 0; termNum--) {
      int nextValue = oldValues[termNum] + 1;
      if (nextValue < m_DimensionSizes[termNum]) {
        oldValues[termNum] = nextValue;
        return true;
      }
      oldValues[termNum] = 0;
    }

    return false;
  }

  public boolean increment(int[] oldValues, ArrayIndexer.Filter[] filters) {
    
    return this.increment(oldValues, filters, Double.POSITIVE_INFINITY);
    
  }
  
  public boolean increment(int[] oldValues, ArrayIndexer.Filter[] filters, double timeOut) {
    return this.increment(oldValues, filters, timeOut, filters.length);
  }
  
  public boolean increment(int[] oldValues, ArrayIndexer.Filter[] filters, double timeOut, int numForcedFilters) {
    
    long startTime = System.currentTimeMillis();
    
    if (oldValues.length != m_DimensionSizes.length) {
      throw new RuntimeException("Given array has the wrong number of dimensions for this indexer");
    }
    
    boolean passesAllFilters = true;
    do {
      
      if ((System.currentTimeMillis() - startTime) > timeOut) {
        // TODO Fix this
        throw new TimeOutException("Reached timeout of " + timeOut + " milliseconds when searching for valid states in array indexer.");
      }
      
      if (!this.increment(oldValues)) {
        return false;
      }
      int maxBranchIndex = -1;
      //for (int filterNum = 0; filterNum < filters.length; filterNum++) {
      for (int filterNum = 0; filterNum < numForcedFilters; filterNum++) {
        Filter filter = filters[filterNum];
        int filterBranchIndex = filter.getBranchIndex(oldValues);
        maxBranchIndex = Math.max(maxBranchIndex, filterBranchIndex);
      }
      if (maxBranchIndex == -1) {
        for (int filterNum = numForcedFilters; filterNum < filters.length; filterNum++) {
          Filter filter = filters[filterNum];
          int filterBranchIndex = filter.getBranchIndex(oldValues);
          if (filterBranchIndex > -1) {
            maxBranchIndex = filterBranchIndex;
            break;
          }
        }
      }
      
      passesAllFilters = (maxBranchIndex == -1);
      this.branchEnd(maxBranchIndex, oldValues);
    } while (!passesAllFilters);

    return true;
  }

  public void branchEnd(int dimNum, int[] oldValues) {
    for (int nextDim = dimNum - 1; nextDim >= 0; nextDim--) {
    //for (int nextDim = dimNum + 1; nextDim < oldValues.length; nextDim++) {
      oldValues[nextDim] = m_DimensionSizes[nextDim] - 1;
    }
  }

  public int numDimensions() {
    return m_DimensionSizes.length;
  }

  public BigInteger numAllowedStates() {
    return m_NumStates;
  }

  public long getDimensionSize(int dimNum) {
    return m_DimensionSizes[dimNum];
  }
  
  public int[] getDimensionSizes() {
    return ArrayUtils.copyArray(m_DimensionSizes);
  }

  public BigInteger getCoefficient(int dimNum) {
    return m_ConversionCoefficients[dimNum];
  }

}
