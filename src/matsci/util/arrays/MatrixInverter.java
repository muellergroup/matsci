/*
 * Created on Mar 10, 2005
 *
 */
package matsci.util.arrays;

import org.ejml.alg.dense.linsol.AdjustableLinearSolver;
import org.ejml.alg.dense.linsol.InvertUsingSolve;
import org.ejml.alg.dense.linsol.qr.AdjLinearSolverQr;
import org.ejml.data.DenseMatrix64F;
import org.ejml.factory.LinearSolver;
import org.ejml.factory.LinearSolverFactory;
import org.ejml.simple.SimpleMatrix;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;
import cern.colt.matrix.linalg.Property;

/**
 * @author Tim Mueller
 *
 */
public class MatrixInverter {
  
  /**
   * This is because for some reason, the colt library sometimes (rarely) messes up the 
   * inverse and returns a nonsense matrix.  In those cases, re-run the code with this
   * set to "true".  It quickly checks each inverse to make sure the trace of the product
   * of the inverse and the original matrix is the rank of the original matrix.  Running
   * this way is  slower, so use it only when you're sure there's a problem with the inverse.
   */
  public static boolean CATCH_ROGUE_ERRORS = false;

  private DoubleMatrix2D m_Matrix;
  private DoubleMatrix2D m_Inverse;
  private Algebra m_LinearAlgebra = new Algebra();
  
  private boolean m_UpdateIncrementally = false;
  private boolean m_NoSingularGuarantee = false;
  
  private static double HARD_MAX = 10E10; // This is pretty arbitrary
  //private static Property DEFAULT_TOLERANCE = Property.DEFAULT;
  //private static Property DEFAULT_TOLERANCE = new Property(1E-4);
  
  // Changed this on July 21, 2018 from default because some clearly non-singular matrices were being marked as singular.
  private static Property DEFAULT_TOLERANCE = new Property(1E-31);
  
  public MatrixInverter(DoubleMatrix2D matrix, double tolerance) {
    m_Matrix = matrix;
    m_LinearAlgebra.setProperty(new Property(tolerance));
    this.fullInverse();
  }
  
  public MatrixInverter(DoubleMatrix2D matrix) {
    m_Matrix = matrix;
    m_LinearAlgebra.setProperty(DEFAULT_TOLERANCE);
    this.fullInverse();
  }
  
  public MatrixInverter(double[][] matrix) {
    this(DoubleFactory2D.dense.make(matrix));
  }
  
  public MatrixInverter(double[][] matrix, double tolerance) {
    this(DoubleFactory2D.dense.make(matrix), tolerance);
  }
  
  public MatrixInverter(int[][] matrix) {
    this(ArrayUtils.toDoubleArray(matrix));
  }
  
  /**
   * This constructor essentially clones the inverter
   * @param inverter The inverter to clone
   */
  private MatrixInverter(MatrixInverter inverter) {
    m_Matrix = inverter.m_Matrix.copy();
    if (inverter.m_Inverse != null) {
      m_Inverse = inverter.m_Inverse.copy();
    }
    m_UpdateIncrementally = inverter.m_UpdateIncrementally;
    m_NoSingularGuarantee = inverter.m_NoSingularGuarantee;
    m_LinearAlgebra.setProperty(inverter.m_LinearAlgebra.property());
  }
  
  /**
   * Set to false only if it's very unlikely that there will be singular matrices.
   * 
   * @param value
   */
  public void updateIncrementally(boolean value) {
    m_UpdateIncrementally = value;
  }
  
  public boolean updatesIncrementally() {
    return m_UpdateIncrementally;
  }
  
  public void setNoSingularGuarantee(boolean value) {
    m_NoSingularGuarantee = value;
  }
  
  public boolean getNoSingularGuarantee() {
    return m_NoSingularGuarantee;
  }
  
  public void setTolerance(double tolerance) {
    m_LinearAlgebra.setProperty(new Property(tolerance));
    this.fullInverse();
  }
  
  private void fullInverse() {
    m_Inverse = safeInverse(m_Matrix);
  }
  
  private DoubleMatrix2D safeInverse(DoubleMatrix2D matrix) {
    
    SimpleMatrix m = new SimpleMatrix(matrix.toArray());
    SimpleMatrix inverse = m.invert();
    double[][] array = new double[inverse.numRows()][inverse.numCols()];
    for (int rowNum = 0; rowNum < array.length; rowNum++) {
      for (int colNum = 0; colNum < inverse.numCols(); colNum++) {
        array[rowNum][colNum] = inverse.get(rowNum, colNum);
      }
    }
    if ((1+1) == 2) {return DoubleFactory2D.dense.make(array);}
   
    try {
      if (!m_NoSingularGuarantee) {
        if (m_LinearAlgebra.property().isSingular(matrix)) {
          return null;
        }
      }

      /*double[][] array = matrix.toArray();
      DenseMatrix64F newMatrix = new DenseMatrix64F(array);
      SimpleMatrix simpleMatrix = new SimpleMatrix(newMatrix);
      SimpleMatrix inverse = simpleMatrix.invert();
      for (int rowNum = 0; rowNum < array.length; rowNum++) {
        double[] row = array[rowNum];
        for (int colNum = 0; colNum < row.length; colNum++) {
          row[colNum] = inverse.get(rowNum, colNum);
        }
      }
      DoubleMatrix2D returnMatrix = DoubleFactory2D.dense.make(array);*/
      DoubleMatrix2D returnMatrix = m_LinearAlgebra.inverse(matrix);
      // For rogue cases
      if (CATCH_ROGUE_ERRORS) {
        double sum = 0;
        int size = matrix.rows();
        for (int rowNum = 0; rowNum < size; rowNum++) {
          for (int colNum = 0; colNum < size; colNum++) {
            sum += matrix.getQuick(rowNum, colNum) * returnMatrix.getQuick(colNum, rowNum);
          }
        }
        if (Math.round((float) sum) != size) {return null;}
      }
      /*if (!m_NoSingularGuarantee) {
        if (isTooLarge(returnMatrix, 1E6)) {
          return null;
        }
      }*/
      return returnMatrix;
    } catch (IllegalArgumentException e) {
      if (m_NoSingularGuarantee) {
        throw new RuntimeException("Found singular matrix despite no singular guarantee!", e);
      }
      return null;
    }
  }
  
  
  private static boolean isTooLarge(DoubleMatrix2D matrix, double max) {
    if (matrix == null) {return true;}
    for (int row = 0; row < matrix.rows(); row++) {
      for (int col = 0; col < matrix.columns(); col++) {
        if (Math.abs(matrix.getQuick(row, col)) > max) {
          return true;
        }
      }
    }
    return false;
  }
  
  public boolean isSingular() {
    return (m_Inverse == null);
  }
  
  public DoubleMatrix2D getMatrix() {
    return m_Matrix.copy();
  }

  public double[][] getMatrixArray() {
    return m_Matrix.toArray();
  }
  
  public DoubleMatrix2D getQuickMatrix() {
    return m_Matrix;
  }
  
  public int numRows() {
    return m_Matrix.rows();
  }
  
  public int numCols() {
    return m_Matrix.columns();
  }
  
  public double getMatrixElement(int row, int col) {
    return m_Matrix.getQuick(row, col);
  }
  
  public double[][] getInverseArray() {
    if (this.isSingular()) {return null;}
    return m_Inverse.toArray();    
  }
  
  public DoubleMatrix2D getQuickInverse() {
    return m_Inverse;
  }
  
  public DoubleMatrix2D getInverse() {
    if (this.isSingular()) {return null;}
    return m_Inverse.copy();
  }
  
  public double getInverseElement(int row, int col) {
    if (this.isSingular()) {return Double.NaN;}
    return m_Inverse.getQuick(row, col);
  }
  
  /**
   * This is a rank-one Sherman-Morrison-Woodbury update.  The update to the matrix should take the form beta * transpose(u) * v.
   * @param u
   * @param v
   */
  public void changeMatrixSMW(double[] u, double[] v, double beta) {
    
    DoubleMatrix2D uMatrix = DoubleFactory2D.dense.make(new double[][] {u});
    DoubleMatrix2D vMatrix = DoubleFactory2D.dense.make(new double[][] {v});
    
    this.changeMatrixSMW(uMatrix, vMatrix, beta);
  }
  
  /**
   * This is a Sherman-Morrison-Woodbury update.  The update to the matrix should take the form beta * transpose(u) * v.
   * @param u
   * @param v
   */
  public void changeMatrixSMW(double[][] u, double[][] v, double beta) {
    
    DoubleMatrix2D uMatrix = DoubleFactory2D.dense.make(u);
    DoubleMatrix2D vMatrix = DoubleFactory2D.dense.make(v);
    
    this.changeMatrixSMW(uMatrix, vMatrix, beta);
  }
  
  /**
   * This is a Sherman-Morrison-Woodbury update.  The update to the matrix should take the form beta * transpose(u) * v.
   * @param uMatrix
   * @param vMatrix
   */
  public void changeMatrixSMW(DoubleMatrix2D uMatrix, DoubleMatrix2D vMatrix, double beta) {
    // This applies the update to the stored matrix
    try {
      m_Matrix = uMatrix.zMult(vMatrix, m_Matrix, beta, 1, true, false);
    } catch (Exception e) {
      throw new IllegalArgumentException("Unable to do Sherman-Morrison-Woodbury update using given u and v matrices");
    }

    // You can't apply the SMW formula if the original matrix was singular
    if (m_Inverse == null || !m_UpdateIncrementally) {
      this.fullInverse();
      return;
    }
    
    DoubleMatrix2D aInv_u = m_Inverse.zMult(uMatrix, null, 1, 0, false, true);
    DoubleMatrix2D vT_aInv = vMatrix.zMult(m_Inverse, null);
    DoubleMatrix2D identity = DoubleFactory2D.dense.identity(vT_aInv.rows());
    DoubleMatrix2D denominator = vMatrix.zMult(aInv_u, identity, beta, 1, false, false);
    DoubleMatrix2D denominator_Inverse = safeInverse(denominator);
    if (denominator_Inverse == null) {
      this.fullInverse();
      return;
    }
    //DoubleMatrix2D newTerm = aInv_u.zMult(denominator_Inverse, denominator_Inverse, 1, 0, false, false);
    DoubleMatrix2D newTerm = aInv_u.zMult(denominator_Inverse, null, 1, 0, false, false);
    m_Inverse = newTerm.zMult(vT_aInv, m_Inverse, (-1 * beta), 1, false, false);
  }
  
  public void changeDiagonalElement(int diagIndex, double delta) {
    if (delta == 0) {return;}
    m_Matrix.setQuick(diagIndex, diagIndex, m_Matrix.getQuick(diagIndex, diagIndex) + delta);

    // You can't apply the SMW formula if the original matrix was singular
    if (m_Inverse == null || !m_UpdateIncrementally) {
      this.fullInverse();
      return;
    }
    
    double factor = -1 * delta / (delta * m_Inverse.getQuick(diagIndex, diagIndex) + 1);
    double[] row = m_Inverse.viewRow(diagIndex).toArray();
    DoubleMatrix2D view = DoubleFactory2D.dense.make(new double[][] {row});
    m_Inverse = view.zMult(view, m_Inverse, factor, 1, true, false);
  }
  
  /**
   * 
   * @param u
   * @param v
   * @param d
   * @param rows The indices of the new rows
   * @param columns The indices of the new columns
   */
  public void growMatrix(double[][] u, double[][] v, double[][] d, int[] columns, int[] rows) {
    
    this.growMatrix(u, v, d);
    int[] rowPermutation = ArrayUtils.getInsertionPermutation(m_Matrix.rows(), rows);
    int[] columnPermutation = ArrayUtils.getInsertionPermutation(m_Matrix.columns(), columns);
    
    this.permuteRows(rowPermutation);
    this.permuteColumns(columnPermutation);
  }
  
  /**
   * The upper left partition is the current matrix, the upper right is u, the lower left is v, and the lower right is d.
   * 
   * @param u
   * @param v
   * @param d
   */
  public void growMatrix(double[][] u, double[][] v, double[][] d) {
    
    DoubleMatrix2D uMatrix = DoubleFactory2D.dense.make(u);
    DoubleMatrix2D vMatrix = DoubleFactory2D.dense.make(v);
    DoubleMatrix2D dMatrix = DoubleFactory2D.dense.make(d);

    this.growMatrix(uMatrix, vMatrix, dMatrix);
  }
  
  /**
   * The upper left partition is the current matrix, the upper right is u, the lower left is v, and the lower right is d.
   * 
   * @param uMatrix
   * @param vMatrix
   * @param dMatrix
   */
  public void growMatrix(DoubleMatrix2D uMatrix, DoubleMatrix2D vMatrix, DoubleMatrix2D dMatrix) {
    if ((uMatrix.rows() != m_Matrix.rows()) || vMatrix.columns() != m_Matrix.columns()) {
      throw new IllegalArgumentException("Can't grow matrix because u and v dimensions are incompatible with original matrix.");
    }
    
    if ((uMatrix.columns() != dMatrix.columns()) || vMatrix.rows() != dMatrix.rows()) {
      throw new IllegalArgumentException("Can't grow matrix because u and v dimensions are incompatible with d matrix.");
    }
    
    DoubleMatrix2D[][] newMatrix = new DoubleMatrix2D[][] {{m_Matrix, uMatrix}, {vMatrix, dMatrix}};
    m_Matrix = DoubleFactory2D.dense.compose(newMatrix);
    
    if ((m_Matrix.rows() != m_Matrix.columns()) || m_Inverse == null  || !m_UpdateIncrementally) {
      this.fullInverse();
      return;
    }
    
    DoubleMatrix2D aInv_u = m_Inverse.zMult(uMatrix, null);
    DoubleMatrix2D vT_aInv = vMatrix.zMult(m_Inverse, null); 
    //DoubleMatrix2D vT_aInv = m_Inverse.zMult(vMatrix, null, 1, 0, true, true).viewDice(); // Use this if m_Inverse might be a view -- it's a bug in Colt.
    DoubleMatrix2D denominator = vMatrix.zMult(aInv_u, dMatrix, -1, 1, false, false);
    DoubleMatrix2D denominator_Inverse = safeInverse(denominator);
    if (denominator_Inverse == null) {
      this.fullInverse();
      return;
    }
    DoubleMatrix2D invU = aInv_u.zMult(denominator_Inverse, null, -1, 0, false, false);
    DoubleMatrix2D invV = denominator_Inverse.zMult(vT_aInv, null, -1, 0, false, false);
    DoubleMatrix2D invA = aInv_u.zMult(invV, m_Inverse, -1, 1, false, false);

    DoubleMatrix2D[][] newInverse = new DoubleMatrix2D[][] {{invA, invU}, {invV, denominator_Inverse}};
    m_Inverse = DoubleFactory2D.dense.compose(newInverse);
  }
  
  /**
   * 
   * @param rowIndices The indices of the rows we are removing
   * @param columnIndices The indices of the columns we are removing
   */
  public void shrinkMatrix(int[] rowIndices, int[] columnIndices) {
    
    int[] rowKeepView = new int[m_Matrix.rows() - rowIndices.length];
    int[] colKeepView = new int[m_Matrix.columns() - columnIndices.length];
    
    int viewIndex = 0;
    for (int rowNum = 0; rowNum < m_Matrix.rows(); rowNum++) {
      if (ArrayUtils.arrayContains(rowIndices, rowNum)) {continue;}
      rowKeepView[viewIndex++] = rowNum;
    }
    
    viewIndex = 0;
    for (int colNum = 0; colNum < m_Matrix.rows(); colNum++) {
      if (ArrayUtils.arrayContains(columnIndices, colNum)) {continue;}
      colKeepView[viewIndex++] = colNum;
    }
    
    m_Matrix = m_Matrix.viewSelection(rowKeepView, colKeepView).copy();
    
    if (m_Inverse == null || !m_UpdateIncrementally || m_Matrix.rows() != m_Matrix.columns() || (rowIndices.length * 4) > m_Matrix.rows()) {
      this.fullInverse();
      return;
    }
    
    // The rows and columns are transposed when dealing with the inverse
    DoubleMatrix2D invD = m_Inverse.viewSelection(columnIndices, rowIndices).copy();
    DoubleMatrix2D inv_invD = safeInverse(invD);
    if (inv_invD == null) {
      this.fullInverse();
      return;
    }
    
    DoubleMatrix2D invA = m_Inverse.viewSelection(colKeepView, rowKeepView).copy();
    DoubleMatrix2D invU = m_Inverse.viewSelection(colKeepView, rowIndices).copy();
    DoubleMatrix2D invV = m_Inverse.viewSelection(columnIndices, rowKeepView).copy();
    
    DoubleMatrix2D neg_aInv_u = invU.zMult(inv_invD, null);
    m_Inverse = neg_aInv_u.zMult(invV, invA, -1, 1, false, false);
  }
  
  public void swapRows(int row1, int row2) {
    
    int[] permutation = new int[m_Matrix.rows()];
    for (int rowNum = 0; rowNum < permutation.length; rowNum++) {
      permutation[rowNum] = rowNum;
    }
    permutation[row1] = row2;
    permutation[row2] = row1;
    this.permuteRows(permutation);
    
  }
  
  public void permuteRows(int[] permutation) {
    
    if (permutation.length != m_Matrix.rows()) {
      throw new IllegalArgumentException("Row permutation must have the same number of elements as the matrix has rows");
    }
    m_Matrix = m_LinearAlgebra.permuteRows(m_Matrix, permutation, null).copy();
    if (m_Inverse != null) {m_Inverse = m_LinearAlgebra.permuteColumns(m_Inverse, permutation, null).copy();}
    
  }
  
  public void swapColumns(int col1, int col2) {
    
    int[] permutation = new int[m_Matrix.rows()];
    for (int colNum = 0; colNum < permutation.length; colNum++) {
      permutation[colNum] = colNum;
    }
    permutation[col1] = col2;
    permutation[col2] = col1;
    this.permuteColumns(permutation);
  }
  
  public void permuteColumns(int[] permutation) {
    
    if (permutation.length != m_Matrix.columns()) {
      throw new IllegalArgumentException("Row permutation must have the same number of elements as the matrix has rows");
    }
    m_Matrix = m_LinearAlgebra.permuteColumns(m_Matrix, permutation, null).copy();
    if (m_Inverse != null) {m_Inverse = m_LinearAlgebra.permuteRows(m_Inverse, permutation, null).copy();}
    
  }
  
  public String verify() {
    if (m_Inverse == null) {return "Singular";}
    return m_Matrix.zMult(m_Inverse, null).toString();
  }
  
  public String getMatrixString() {
    return m_Matrix.toString();
  }
  
  public String getInverseString() {
    if (m_Inverse == null) {return "null";}
    return m_Inverse.toString();
  }
  
  public MatrixInverter copy() {
    return new MatrixInverter(this);
  }

}
