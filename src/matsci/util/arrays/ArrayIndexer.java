
package matsci.util.arrays;

import java.util.Arrays;
/**
 * <p>Title: </p>
 * <p>Description: This class converts back and forth between an array of integers in mixed base notation
 * and the integer they represent. </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class ArrayIndexer {

  private int[] m_DimensionSizes;
  private int[] m_ConversionCoefficients;
  private int m_NumStates = 1;

  public ArrayIndexer(int numDimensions, int dimensionSizes) {
    this(getDimensionSizes(numDimensions, dimensionSizes));
  }
  
  private static int[] getDimensionSizes(int numDimensions, int dimensionSizes) {
    int[] returnArray = new int[numDimensions];
    Arrays.fill(returnArray, dimensionSizes);
    return returnArray;
  }
  
  public ArrayIndexer(int[] dimensionSizes) {
    m_DimensionSizes = dimensionSizes;

    m_ConversionCoefficients = new int[dimensionSizes.length];
    int multiplier = 1;
    for (int termNum = 0; termNum < dimensionSizes.length; termNum++) {
    //for (int termNum = dimensionSizes.length - 1; termNum >= 0; termNum--) {
      m_ConversionCoefficients[termNum] = multiplier;
      multiplier *= dimensionSizes[termNum];
    }
    m_NumStates = multiplier;
  }
  
  public int getBoundedIndex(int value) {
    
    int returnValue = value % this.numAllowedStates();
    return (returnValue < 0) ? returnValue + this.numAllowedStates() : returnValue;

  }

  public int[] splitValue(int value) {

    int[] returnArray = new int[m_ConversionCoefficients.length];
    return this.splitValue(value, returnArray);
  }
  
  public int[] splitValue(int value, int[] returnArray) {

    int remainder = value;
    for (int termNum = m_ConversionCoefficients.length - 1; termNum >= 0; termNum--) {
    //for (int termNum = 0; termNum < m_ConversionCoefficients.length; termNum++) {
      int multiplier = m_ConversionCoefficients[termNum];
      returnArray[termNum] = remainder / multiplier; // integer division
      remainder = remainder % multiplier;
    }

    return returnArray;
  }
  

  public int[] splitValueBounded(int value) {

    int[] returnArray = new int[m_ConversionCoefficients.length];
    return this.splitValueBounded(value, returnArray);
  }
  
  public int[] splitValueBounded(int value, int[] returnArray) {

    int remainder = value % this.numAllowedStates();
    for (int termNum = m_ConversionCoefficients.length - 1; termNum >= 0; termNum--) {
    //for (int termNum = 0; termNum < m_ConversionCoefficients.length; termNum++) {
      int multiplier = m_ConversionCoefficients[termNum];
      returnArray[termNum] = remainder / multiplier; // integer division
      remainder = remainder % multiplier;
    }

    return returnArray;
  }
 
  public int joinValuesBounded(int[] terms) {

    int returnValue = 0;
    for (int termNum = 0; termNum < m_ConversionCoefficients.length; termNum++) {
    //for (int termNum = m_ConversionCoefficients.length - 1; termNum >= 0; termNum--) {
      double modTerm = terms[termNum] % m_DimensionSizes[termNum];
      modTerm = (modTerm < 0) ? modTerm + m_DimensionSizes[termNum] : modTerm;
      returnValue += (modTerm * m_ConversionCoefficients[termNum]);
    }
    return returnValue;
  }
  
  public int joinValues(int[] terms) {

    int returnValue = 0;
    for (int termNum = 0; termNum < m_ConversionCoefficients.length; termNum++) {
    //for (int termNum = m_ConversionCoefficients.length - 1; termNum >= 0; termNum--) {
      returnValue += (terms[termNum] * m_ConversionCoefficients[termNum]);
    }
    return returnValue;
  }
  
  public int[] getInitialState() {
    return new int[this.numDimensions()];
  }
  
  public int[] getInitialState(ArrayIndexer.Filter[] filters) {
    return this.getInitialState(filters, Double.POSITIVE_INFINITY);
  }
  
  public int[] getInitialState(ArrayIndexer.Filter[] filters, double timeOut) {
    
    int[] initialState = this.getInitialState();
    for (int filterNum = 0; filterNum < filters.length; filterNum++) {
      int branchIndex = filters[filterNum].getBranchIndex(initialState);
      if (branchIndex > -1) {
        if (!this.increment(initialState, filters, timeOut)) {
          return null;
        }
      }
    }
    
    return initialState;
  }
  
  public void setInitialState(int[] state) {
    Arrays.fill(state, 0);
  }

  public boolean increment(int[] oldValues) {

    if (oldValues.length != m_DimensionSizes.length) {
      throw new RuntimeException("Given array has the wrong number of dimensions for this indexer");
    }

    for (int termNum = 0; termNum < oldValues.length; termNum++) {
    //for (int termNum = oldValues.length -1; termNum >= 0; termNum--) {
      int nextValue = oldValues[termNum] + 1;
      if (nextValue < m_DimensionSizes[termNum]) {
        oldValues[termNum] = nextValue;
        return true;
      }
      oldValues[termNum] = 0;
    }

    return false;
  }
  
  public boolean increment(int[] oldValues, ArrayIndexer.Filter[] filters) {
    
    return this.increment(oldValues, filters, Double.POSITIVE_INFINITY);
    
  }
  
  public boolean increment(int[] oldValues, ArrayIndexer.Filter[] filters, double timeOut) {
    return this.increment(oldValues, filters, timeOut, filters.length);
  }
  
  public boolean increment(int[] oldValues, ArrayIndexer.Filter[] filters, double timeOut, int numForced) {
    
    long startTime = System.currentTimeMillis();
    
    if (oldValues.length != m_DimensionSizes.length) {
      throw new RuntimeException("Given array has the wrong number of dimensions for this indexer");
    }
    
    boolean passesAllFilters = true;
    do {
      
      if ((System.currentTimeMillis() - startTime) > timeOut) {
        throw new TimeOutException("Reached timeout of " + timeOut + " milliseconds when searching for valid states in array indexer.");
      }
      
      if (!this.increment(oldValues)) {
        return false;
      }
      int maxBranchIndex = -1;
      //for (int filterNum = 0; filterNum < filters.length; filterNum++) {
      for (int filterNum = 0; filterNum < numForced; filterNum++) {
        Filter filter = filters[filterNum];
        int filterBranchIndex = filter.getBranchIndex(oldValues);
        maxBranchIndex = Math.max(maxBranchIndex, filterBranchIndex);
      }
      if (maxBranchIndex == -1) {
        for (int filterNum = numForced; filterNum < filters.length; filterNum++) {
          Filter filter = filters[filterNum];
          int filterBranchIndex = filter.getBranchIndex(oldValues);
          if (filterBranchIndex > -1) {
            maxBranchIndex = filterBranchIndex;
            break;
          }
        }
      }
      
      passesAllFilters = (maxBranchIndex == -1);
      this.branchEnd(maxBranchIndex, oldValues);
    } while (!passesAllFilters);

    return true;
  }
  
  public void branchEnd(int dimNum, int[] oldValues) {
    for (int nextDim = dimNum - 1; nextDim >= 0; nextDim--) {
    //for (int nextDim = dimNum + 1; nextDim < oldValues.length; nextDim++) {
      oldValues[nextDim] = m_DimensionSizes[nextDim] - 1;
    }
  }

  public int numDimensions() {
    return m_DimensionSizes.length;
  }

  public int numAllowedStates() {
    return m_NumStates;
  }

  public int getDimensionSize(int dimNum) {
    return m_DimensionSizes[dimNum];
  }
  
  public int[] getDimensionSizes() {
    return ArrayUtils.copyArray(m_DimensionSizes);
  }

  public int getCoefficient(int dimNum) {
    return m_ConversionCoefficients[dimNum];
  }
  
  public interface Filter {
    public int getBranchIndex(int[] currentState);
  }
  
  public static class TimeOutException extends matsci.exceptions.TimeOutException {
    /**
     * 
     */
    private static final long serialVersionUID = 5390382636062393286L;

    public TimeOutException(String message) {
      super(message);
    }
  }
  
  public static class SubSetFilter implements Filter {
    
    private int[][] m_SubSetIndices;
    private int[] m_MaxSubSetIndices;
    private Filter[] m_Filters;
    
    private int[][] m_SegmentStates;
    
    public SubSetFilter(int[][] subSetIndices, Filter[] filters) {
      
      m_SubSetIndices = ArrayUtils.copyArray(subSetIndices);
      m_MaxSubSetIndices = new int[filters.length];
      m_SegmentStates = new int[filters.length][];
      for (int filterNum = 0; filterNum < filters.length; filterNum++) {
        m_MaxSubSetIndices[filterNum] = ArrayUtils.maxElement(m_SubSetIndices[filterNum]);
        m_SegmentStates[filterNum] = new int[subSetIndices[filterNum].length];
      }
      
      m_Filters = (Filter[]) ArrayUtils.copyArray(filters);
      
    }
    
    public int getBranchIndex(int[] currentState) {
      
      int maxIndex = -1;
      for (int filterNum = 0; filterNum < m_SegmentStates.length; filterNum++) {
        if (m_MaxSubSetIndices[filterNum] <= maxIndex) {continue;}
        int[] segmentStates = m_SegmentStates[filterNum];
        int[] setIndices = m_SubSetIndices[filterNum];
        for (int stateNum = 0; stateNum < setIndices.length; stateNum++) {
          segmentStates[stateNum] = currentState[setIndices[stateNum]];
        }
        Filter filter = m_Filters[filterNum];
        int filterBranchIndex = filter.getBranchIndex(segmentStates);
        if (filterBranchIndex >= 0) {
          maxIndex = Math.max(maxIndex, setIndices[filterBranchIndex]);
        }
      }
      
      return maxIndex;
    }
  }
  
  // TODO figure out whether we should be using numStates[x] - 1 when calculating max sums
  public static class FixedSumFilter implements Filter {
    
    private double[] m_MinAllowed;
    private double[] m_MaxAllowed;
    private double[] m_Weights;
    
    public FixedSumFilter(int[] numStates, double targetSum) {
      this(numStates, unitWeights(numStates.length), targetSum, 0);
    }
    
    protected static double[] unitWeights(int length) {
      double[] returnArray = new double[length];
      Arrays.fill(returnArray, 1);
      return returnArray;
    }
    
    public FixedSumFilter(int[] numStates, double[] weights, double targetSum) {
      this(numStates, weights, targetSum, 0);
    }
    
    public FixedSumFilter(int[] numStates, double[] weights, double targetSum, double tolerance) {
      
      m_Weights = ArrayUtils.copyArray(weights);
      
      m_MinAllowed = new double[numStates.length];
      m_MaxAllowed = new double[numStates.length];
      if (numStates.length > 0) {
        m_MaxAllowed[0] = targetSum - tolerance;
        m_MinAllowed[0] = targetSum + tolerance;
      }
      for (int i = 1; i < numStates.length; i++) {
        if (weights[i-1] > 0) {
          m_MinAllowed[i] = m_MinAllowed[i-1] - numStates[i-1] * weights[i-1];
          m_MaxAllowed[i] = m_MaxAllowed[i-1];
        } else {
          m_MaxAllowed[i] = m_MaxAllowed[i-1] - numStates[i-1] * weights[i-1];
          m_MinAllowed[i] = m_MinAllowed[i-1];
        }
      }
    }
    
    public int getBranchIndex(int[] currentState) {
      double sum = 0;
      for (int i = currentState.length - 1; i >= 0; i--) {
        sum += currentState[i] * m_Weights[i];
        if ((sum > m_MaxAllowed[i]) || (sum < m_MinAllowed[i])) {
          return i;
        }
      }
      return -1;
    }
    
  }
  
  public static class MaxSumFilter implements Filter {
    
    private double[] m_MaxAllowed;
    private double[] m_Weights;
    
    public MaxSumFilter(int[] numStates, double maxValue) {
      this(numStates, unitWeights(numStates.length), maxValue);
    }
    
    protected static double[] unitWeights(int length) {
      double[] returnArray = new double[length];
      Arrays.fill(returnArray, 1);
      return returnArray;
    }
    
    public MaxSumFilter(int[] numStates, double[] weights, double maxValue) {
      
      m_Weights = ArrayUtils.copyArray(weights);
      
      m_MaxAllowed = new double[numStates.length];
      if (numStates.length > 0) {
        m_MaxAllowed[0] = maxValue;
      }
      for (int i = 1; i < numStates.length; i++) {
        if (weights[i-1] > 0) {
          m_MaxAllowed[i] = m_MaxAllowed[i-1];
        } else {
          m_MaxAllowed[i] = m_MaxAllowed[i-1] - (numStates[i-1] - 1) * weights[i-1];
        }
      }
    }
    
    public int getBranchIndex(int[] currentState) {
      double sum = 0;
      for (int i = currentState.length - 1; i >= 0; i--) {
        sum += currentState[i] * m_Weights[i];
        if (sum > m_MaxAllowed[i]) {
          return i;
        }
      }
      return -1;
    }
    
  }
  
  public static class MinSumFilter implements Filter {
    
    private double[] m_MinAllowed;
    private double[] m_Weights;
    
    public MinSumFilter(int[] numStates, double minValue) {
      this(numStates, unitWeights(numStates.length), minValue);
    }
    
    protected static double[] unitWeights(int length) {
      double[] returnArray = new double[length];
      Arrays.fill(returnArray, 1);
      return returnArray;
    }
    
    public MinSumFilter(int[] numStates, double[] weights, double minValue) {
      
      m_Weights = ArrayUtils.copyArray(weights);
      
      m_MinAllowed = new double[numStates.length];
      if (numStates.length > 0) {
        m_MinAllowed[0] = minValue;
      }
      for (int i = 1; i < numStates.length; i++) {
        if (weights[i-1] > 0) {
          m_MinAllowed[i] = m_MinAllowed[i-1] - (numStates[i-1] - 1) * weights[i-1];
        } else {
          m_MinAllowed[i] = m_MinAllowed[i-1];
        }
      }
    }
    
    public int getBranchIndex(int[] currentState) {
      double sum = 0;
      for (int i = currentState.length - 1; i >= 0; i--) {
        sum += currentState[i] * m_Weights[i];
        if (sum < m_MinAllowed[i]) {
          return i;
        }
      }
      return -1;
    }
    
  }
  
  
  public static class PermutationFilter implements Filter {

    private boolean[] m_SeenIndices;
    
    public PermutationFilter(int numVars) {
      m_SeenIndices = new boolean[numVars];
    }
    
    public int getBranchIndex(int[] currentState) {
      Arrays.fill(m_SeenIndices, false);
      for (int i = currentState.length - 1; i >= 0; i--) {
        if (m_SeenIndices[i]) {
          return i;
        }
        m_SeenIndices[i] = true;
      }
      return -1;
    }
    
  }
  

  public static class LowIndexFilter implements Filter {
    
    private int m_MaxOccupiedIndex;

    public LowIndexFilter(int numStates) {
      m_MaxOccupiedIndex = numStates;
    }
    
    public int getBranchIndex(int[] currentState) {
      for (int i = currentState.length - 1; i >= m_MaxOccupiedIndex; i--) {
        if (currentState[i] != 0) {
          return i;
        }
      }
      return -1;
    }
    
    public void setBestState(int[] currentState) {
      for (int i = currentState.length - 1; i >= 0; i--) {
        if (currentState[i] != 0) {
          m_MaxOccupiedIndex = i;
          return;
        }
      }
    }
    
    public int getMaxOccupiedIndex() {
      return m_MaxOccupiedIndex;
    }
    
  }
  
}
