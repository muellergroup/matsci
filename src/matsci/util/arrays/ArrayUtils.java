package matsci.util.arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

import org.ejml.simple.SimpleMatrix;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class ArrayUtils {
  
  public ArrayUtils() {
  }
  
  public static String toString(double[][] matrix) {
    if (matrix == null) {
      return "null";
    }

    String returnString = "";
    for (int rowNum = 0; rowNum < matrix.length; rowNum++) {
      double[] row = matrix[rowNum];
      if (row == null) {
        returnString = returnString + "null";
      } else {
        for (int colNum = 0; colNum < row.length - 1; colNum++) {
          returnString = returnString + row[colNum] + ", ";
        }
        returnString = returnString + row[row.length - 1];
      }
      returnString = returnString + "\n";        
    }
    
    return returnString;
  }
  
  public static int[][] diagonalMatrix(int size, int value) {
    int[][] returnArray = new int[size][size];
    for (int i = 0; i < returnArray.length; i++) {
      returnArray[i][i] = value;
    }
    return returnArray;
  }
  
  
  public static double[][] diagonalMatrix(int size, double value) {
    double[][] returnArray = new double[size][size];
    for (int i = 0; i < returnArray.length; i++) {
      returnArray[i][i] = value;
    }
    return returnArray;
  }
  
  public static double[][] selectRowsAndColumns(double[][] matrix, int[] rowsAndColumnsToSelect) {
    double[][] returnArray = new double[rowsAndColumnsToSelect.length][rowsAndColumnsToSelect.length];
    for (int row = 0; row < returnArray.length; row++) {
      for (int col = 0; col < rowsAndColumnsToSelect.length; col++) {
        returnArray[row][col] = matrix[rowsAndColumnsToSelect[row]][rowsAndColumnsToSelect[col]];
      }
    }
    return returnArray;
  }
  
  public static int[] addToSet(int[] set, int value) {
    
    if (ArrayUtils.arrayContains(set, value)) {
      return set;
    }
    return ArrayUtils.appendElement(set, value);
    
  }
  
  public static double[][] selectColumns(double[][] matrix, int[] columnsToSelect) {
    double[][] returnArray = new double[matrix.length][columnsToSelect.length];
    for (int row = 0; row < returnArray.length; row++) {
      for (int col = 0; col < columnsToSelect.length; col++) {
        returnArray[row][col] = matrix[row][columnsToSelect[col]];
      }
    }
    return returnArray;
  }
  
  public static double[][] selectRows(double[][] matrix, int[] rowsToSelect) {
    double[][] returnArray = new double[rowsToSelect.length][];
    for (int row = 0; row < rowsToSelect.length; row++) {
      returnArray[row] = ArrayUtils.copyArray(matrix[rowsToSelect[row]]);
    }
    return returnArray;
  }
  
  public static int[] reverseArray(int[] array) {
    int[] returnArray = new int[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[array.length - i - 1];
    }
    return returnArray;
  }
  
  public static double[] reverseArray(double[] array) {
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[array.length - i - 1];
    }
    return returnArray;
  }
  
  public static int[] getSortPermutation(int[] values) {
    int[] map = new int[values.length];
    for (int i = 0; i < map.length; i++) {
      map[i] = i;
    }
    
    getSortPermutation(map, values, 0, map.length - 1);
    return map;
  }
  
  protected static void getSortPermutation(int[] map, int[] values, int left, int right) {
    
    if (left >= right) {return;}
    
    int pivotIndex = map[right];
    double pivotValue = values[pivotIndex];

    int storeIndex = left;
    for (int currIndex = left; currIndex < right; currIndex++) {
      double currValue = values[map[currIndex]];
      if (currValue < pivotValue) {
        int mapIndex = map[currIndex];
        map[currIndex]= map[storeIndex];
        map[storeIndex] = mapIndex;
        storeIndex++;
      }
    }
    
    map[right] = map[storeIndex];
    map[storeIndex] = pivotIndex;
    int newRight = storeIndex - 1;
    while ((newRight > left) && (values[map[newRight]] == pivotValue)) {
      newRight--;
    }
    int newLeft = storeIndex + 1;
    while ((newLeft < right) && (values[map[newLeft]] == pivotValue)) {
      newLeft++;
    }
    getSortPermutation(map, values, left, newRight);
    getSortPermutation(map, values, newLeft, right);
    
  }
  
  public static int[] getSortPermutation(double[] values) {
    int[] map = new int[values.length];
    for (int i = 0; i < map.length; i++) {
      map[i] = i;
    }
    
    getSortPermutation(map, values, 0, map.length - 1);
    return map;
  }
  
  protected static void getSortPermutation(int[] map, double[] values, int left, int right) {
    
    if (left >= right) {return;}
    
    int pivotIndex = map[right];
    double pivotValue = values[pivotIndex];

    int storeIndex = left;
    for (int currIndex = left; currIndex < right; currIndex++) {
      double currValue = values[map[currIndex]];
      if (currValue < pivotValue) {
        int mapIndex = map[currIndex];
        map[currIndex]= map[storeIndex];
        map[storeIndex] = mapIndex;
        storeIndex++;
      }
    }
    
    map[right] = map[storeIndex];
    map[storeIndex] = pivotIndex;
    int newRight = storeIndex - 1;
    while ((newRight > left) && (values[map[newRight]] == pivotValue)) {
      newRight--;
    }
    int newLeft = storeIndex + 1;
    while ((newLeft < right) && (values[map[newLeft]] == pivotValue)) {
      newLeft++;
    }
    getSortPermutation(map, values, left, newRight);
    getSortPermutation(map, values, newLeft, right);
    
  }
  

  public static int[] getSortPermutation(String[] values) {
    int[] map = new int[values.length];
    for (int i = 0; i < map.length; i++) {
      map[i] = i;
    }
    
    getSortPermutation(map, values, 0, map.length - 1);
    return map;
  }
  
  protected static void getSortPermutation(int[] map, String[] values, int left, int right) {
    
    if (left >= right) {return;}
    
    int pivotIndex = map[right];
    String pivotValue = values[pivotIndex];
    int storeIndex = left;
    for (int currIndex = left; currIndex < right; currIndex++) {
      String currValue = values[map[currIndex]];
      if (currValue.compareTo(pivotValue) < 0) {
        int mapIndex = map[currIndex];
        map[currIndex]= map[storeIndex];
        map[storeIndex] = mapIndex;
        storeIndex++;
      }
    }
    
    map[right] = map[storeIndex];
    map[storeIndex] = pivotIndex;
    getSortPermutation(map, values, left, storeIndex - 1);
    getSortPermutation(map, values, storeIndex + 1, right);
    
  }
  
  public static int[] getSetBits(boolean[] array) {
    int numSetBits = 0;
    for (int i = 0; i < array.length; i++) {
      if (array[i]) {numSetBits++;}
    }
    int[] returnArray = new int[numSetBits];
    int returnIndex = 0;
    for (int i = 0; i < array.length; i++) {
      if (array[i]) {returnArray[returnIndex++] = i;}
    }
    return returnArray;
  }
  
  public static int minElementIndex(int[] array) {
    double minimum = Double.POSITIVE_INFINITY;
    int returnIndex = -1;
    for (int i = 0; i < array.length; i++) {
      if (array[i] < minimum) {
        minimum = array[i];
        returnIndex = i;
      }
    }
    return returnIndex;
  }
  
  public static int minElementIndex(double[] array) {
    double minimum = Double.POSITIVE_INFINITY;
    int returnIndex = -1;
    for (int i = 0; i < array.length; i++) {
      if (array[i] < minimum) {
        minimum = array[i];
        returnIndex = i;
      }
    }
    if (returnIndex < 0) {
      return findIndex(array, Double.POSITIVE_INFINITY);
    }
    return returnIndex;
  }
  
  public static int minElementIndex(double[] array, double floor) {
    double minimum = Double.POSITIVE_INFINITY;
    int returnIndex = -1;
    for (int i = 0; i < array.length; i++) {
      if (array[i] < minimum && array[i] > floor) {
        minimum = array[i];
        returnIndex = i;
      }
    }
    if ((returnIndex < 0) && (floor != Double.POSITIVE_INFINITY)) {
      return findIndex(array, Double.POSITIVE_INFINITY);
    }
    return returnIndex;
  }
  
  public static int maxElement(int[] array) {
    return array[maxElementIndex(array)];
  }
  
  public static int maxElementIndex(int[] array) {
    double maximum = Double.NEGATIVE_INFINITY;
    int returnIndex = -1;
    for (int i = 0; i < array.length; i++) {
      if (array[i] > maximum) {
        maximum = array[i];
        returnIndex = i;
      }
    }
    return returnIndex;
  }

  public static double minElement(double[] array) {
    return array[minElementIndex(array)];
  }
  
  public static double maxElement(double[] array) {
    return array[maxElementIndex(array)];
  }
  
  public static int maxElementIndex(double[] array) {
    double maximum = Double.NEGATIVE_INFINITY;
    int returnIndex = -1;
    for (int i = 0; i < array.length; i++) {
      if (array[i] > maximum) {
        maximum = array[i];
        returnIndex = i;
      }
    }
    if (returnIndex < 0) {
      return findIndex(array, Double.NEGATIVE_INFINITY);
    }
    return returnIndex;
  }
  
  public static int maxElementIndex(double[] array, double ceiling) {
    double maximum = Double.NEGATIVE_INFINITY;
    int returnIndex = -1;
    for (int i = 0; i < array.length; i++) {
      if (array[i] > maximum && array[i] < ceiling) {
        maximum = array[i];
        returnIndex = i;
      }
    }
    if ((returnIndex < 0) && (ceiling != Double.NEGATIVE_INFINITY)) {
      return findIndex(array, Double.NEGATIVE_INFINITY);
    }
    return returnIndex;
  }
  
  public static int[][] identityMatrix(int numDimensions) {
    int[][] returnMatrix = new int[numDimensions][numDimensions];
    for (int i = 0; i < numDimensions; i++) {
      returnMatrix[i][i] = 1;
    }
    return returnMatrix;
  }
  
  public static double[][] identityMatrixDouble(int numDimensions) {
    double[][] returnMatrix = new double[numDimensions][numDimensions];
    for (int i = 0; i < numDimensions; i++) {
      returnMatrix[i][i] = 1;
    }
    return returnMatrix;
  }

  public static boolean[] copyArray(boolean[] array) {
    int length = array.length;
    boolean[] newArray = new boolean[length];
    System.arraycopy(array, 0, newArray, 0, length);
    return newArray;
  }
  
  public static boolean[][] copyArray(boolean[][] array) {
    int rows = array.length;
    boolean[][] returnArray = new boolean[rows][];
    for (int i = 0; i<rows; i++) {
      boolean[] row = array[i];
      if (row == null) {continue;}
      returnArray[i] = ArrayUtils.copyArray(row);
    }
    return returnArray;
  }

  public static long[] copyArray(long[] array) {
    int length = array.length;
    long[] newArray = new long[length];
    System.arraycopy(array, 0, newArray, 0, length);
    return newArray;
  }

  public static String[] copyArray(String[] array) {
    int length = array.length;
    String[] newArray = new String[length];
    System.arraycopy(array, 0, newArray, 0, length);
    return newArray;
  }

  public static double[] doubleArray(int[] array) {
    int length = array.length;
    double[] newArray = new double[length];
    for (int i = 0; i < newArray.length; i++) {
      newArray[i] = array[i];
    }
    return newArray;
  }

  public static double[] copyArray(double[] array) {
    int length = array.length;
    double[] newArray = new double[length];
    System.arraycopy(array, 0, newArray, 0, length);
    return newArray;
  }
  
  public static double[][] doubleArray(int[][] array) {
    int rows = array.length;
    double[][] returnArray = new double[rows][];
    for (int i = 0; i<rows; i++) {
      int[] row = array[i];
      if (row == null) {continue;}
      returnArray[i] = ArrayUtils.doubleArray(row);
    }
    return returnArray;
  }

  public static double[][] copyArray(double[][] array) {
    int rows = array.length;
    double[][] returnArray = new double[rows][];
    for (int i = 0; i<rows; i++) {
      double[] row = array[i];
      if (row == null) {continue;}
      returnArray[i] = ArrayUtils.copyArray(row);
    }
    return returnArray;
  }
  
  public static double[][][] copyArray(double[][][] array) {
    int rows = array.length;
    double[][][] returnArray = new double[rows][][];
    for (int i = 0; i<rows; i++) {
      double[][] row = array[i];
      if (row == null) {continue;}
      returnArray[i] = ArrayUtils.copyArray(row);
    }
    return returnArray;
  }
  
  public static double[][][][] copyArray(double[][][][] array) {
    int rows = array.length;
    double[][][][] returnArray = new double[rows][][][];
    for (int i = 0; i<rows; i++) {
      double[][][] row = array[i];
      if (row == null) {continue;}
      returnArray[i] = ArrayUtils.copyArray(row);
    }
    return returnArray;
  }

  public static short[] copyArray(short[] array) {
    int length = array.length;
    short[] newArray = new short[length];
    System.arraycopy(array, 0, newArray, 0, length);
    return newArray;
  }
  
  public static int[] copyArray(int[] array) {
    int length = array.length;
    int[] newArray = new int[length];
    System.arraycopy(array, 0, newArray, 0, length);
    return newArray;
  }

  public static int[][] copyArray(int[][] array) {
    int rows = array.length;
    int[][] returnArray = new int[rows][];
    for (int i = 0; i<rows; i++) {
      int[] row = array[i];
      if (row == null) {continue;}
      returnArray[i] = copyArray(row);
    }
    return returnArray;
  }
  
  public static int[][][] copyArray(int[][][] array) {
    int rows = array.length;
    int[][][] returnArray = new int[rows][][];
    for (int i = 0; i<rows; i++) {
      int[][] row = array[i];
      if (row == null) {continue;}
      returnArray[i] = copyArray(row);
    }
    return returnArray;
  }
  
  public static Object[] copyArray(Object[] array) {
    if (array == null) {return null;}
    Object[] newArray = (Object[]) Array.newInstance(array.getClass().getComponentType(), array.length);
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static Object[][] copyArray(Object[][] array) {
    int rows = array.length;
    Object[][] returnArray = (Object[][]) Array.newInstance(array.getClass().getComponentType(), rows);
    for (int i = 0; i<rows; i++) {
      returnArray[i] = copyArray(array[i]);
    }
    return returnArray;
  }

  public static double[] growArray(double[] array, int increment) {
    double[] newArray = new double[array.length + increment];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static double[][] growArray(double[][] array, int increment) {
    double[][] newArray = new double[array.length + increment][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static double[][][] growArray(double[][][] array, int increment) {
    double[][][] newArray = new double[array.length + increment][][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }
  
  public static double[][][][] growArray(double[][][][] array, int increment) {
    double[][][][] newArray = new double[array.length + increment][][][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static int[] growArray(int[] array, int increment) {
    int[] newArray = new int[array.length + increment];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static int[][] growArray(int[][] array, int increment) {
    int[][] newArray = new int[array.length + increment][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static int[][][] growArray(int[][][] array, int increment) {
    int[][][] newArray = new int[array.length + increment][][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }
  
  public static int[][][][] growArray(int[][][][] array, int increment) {
    int[][][][] newArray = new int[array.length + increment][][][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static short[] growArray(short[] array, int increment) {
    short[] newArray = new short[array.length + increment];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static short[][] growArray(short[][] array, int increment) {
    short[][] newArray = new short[array.length + increment][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static short[][][] growArray(short[][][] array, int increment) {
    short[][][] newArray = new short[array.length + increment][][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }
  
  public static short[][][][] growArray(short[][][][] array, int increment) {
    short[][][][] newArray = new short[array.length + increment][][][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }
  
  public static boolean[] growArray(boolean[] array, int increment) {
    boolean[] newArray = new boolean[array.length + increment];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static boolean[][] growArray(boolean[][] array, int increment) {
    boolean[][] newArray = new boolean[array.length + increment][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }
  
  public static boolean[][][] growArray(boolean[][][] array, int increment) {
    boolean[][][] newArray = new boolean[array.length + increment][][];
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }
  
  public static Object[] growArray(Object[] array, int increment) {
    Object[] newArray = (Object[]) Array.newInstance(array.getClass().getComponentType(), array.length + increment);
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }
  
  public static Object[][] growArray(Object[][] array, int increment) {
    Object[][] newArray = (Object[][]) Array.newInstance(array.getClass().getComponentType(), array.length + increment);
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }
  
  public static Object[][][] growArray(Object[][][] array, int increment) {
    Object[][][] newArray = (Object[][][]) Array.newInstance(array.getClass().getComponentType(), array.length + increment);
    System.arraycopy(array, 0, newArray, 0, array.length);
    return newArray;
  }

  public static boolean[] truncateArray(boolean[] array, int newLength) {
    boolean[] newArray = (boolean[]) Array.newInstance(array.getClass().getComponentType(), newLength);
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static Object[] truncateArray(Object[] array, int newLength) {
    Object[] newArray = (Object[]) Array.newInstance(array.getClass().getComponentType(), newLength);
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static Object[][] truncateArray(Object[][] array, int newLength) {
    Object[][] newArray = (Object[][]) Array.newInstance(array.getClass().getComponentType(), newLength);
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static int[][][] truncateArray(int[][][] array, int newLength) {
    int[][][] newArray = new int[newLength][][];
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static int[][] truncateArray(int[][] array, int newLength) {
    int[][] newArray = new int[newLength][];
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static int[] truncateArray(int[] array, int newLength) {
    int[] newArray = new int[newLength];
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static double[] truncateArray(double[] array, int newLength) {
    double[] newArray = new double[newLength];
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static double[][] truncateArray(double[][] array, int newLength) {
    double[][] newArray = new double[newLength][];
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }

  public static double[][][] truncateArray(double[][][] array, int newLength) {
    double[][][] newArray = new double[newLength][][];
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static double[][][][] truncateArray(double[][][][] array, int newLength) {
    double[][][][] newArray = new double[newLength][][][];
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  public static double[][][][][] truncateArray(double[][][][][] array, int newLength) {
    double[][][][][] newArray = new double[newLength][][][][];
    System.arraycopy(array, 0, newArray, 0, newLength);
    return newArray;
  }
  
  
  public static double[] preTruncateArray(double[] array, int choppedLength) {
    double[] newArray = new double[array.length - choppedLength];
    System.arraycopy(array, choppedLength, newArray, 0, newArray.length);
    return newArray;
  }
  
  public static int[] insertElement(int[] array, int index, int element) {
    int[] returnArray = new int[array.length + 1];
    System.arraycopy(array, 0, returnArray, 0, index);
    System.arraycopy(array, index, returnArray, index+1, array.length - index);
    returnArray[index] = element;
    return returnArray;
  }
  
  public static double[][] insertElement(double[][] array, int index, double[] element) {
    double[][] returnArray = new double[array.length + 1][];
    System.arraycopy(array, 0, returnArray, 0, index);
    System.arraycopy(array, index, returnArray, index+1, array.length - index);
    returnArray[index] = element;
    return returnArray;
  }
  
  public static double[] insertElement(double[] array, int index, double element) {
    double[] returnArray = new double[array.length + 1];
    System.arraycopy(array, 0, returnArray, 0, index);
    System.arraycopy(array, index, returnArray, index+1, array.length - index);
    returnArray[index] = element;
    return returnArray;
  }
  
  
  public static Object[] insertElement(Object[] array, int index, Object element) {
    Object[] returnArray = (Object[]) Array.newInstance(array.getClass().getComponentType(), array.length + 1);
    System.arraycopy(array, 0, returnArray, 0, index);
    System.arraycopy(array, index, returnArray, index+1, array.length - index);
    returnArray[index] = element;
    return returnArray;
  }
  
  public static int[] removeElement(int[] array, int indexToRemove) {
    int[] returnArray = new int[array.length - 1];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static int[][] removeElement(int[][] array, int indexToRemove) {
    int[][] returnArray = new int[array.length - 1][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static int[][][] removeElement(int[][][] array, int indexToRemove) {
    int[][][] returnArray = new int[array.length - 1][][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static int[][][][] removeElement(int[][][][] array, int indexToRemove) {
    int[][][][] returnArray = new int[array.length - 1][][][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static double[] removeElement(double[] array, int indexToRemove) {
    double[] returnArray = new double[array.length - 1];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static double[][] removeElement(double[][] array, int indexToRemove) {
    double[][] returnArray = new double[array.length - 1][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static double[][][] removeElement(double[][][] array, int indexToRemove) {
    double[][][] returnArray = new double[array.length - 1][][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static double[][][][] removeElement(double[][][][] array, int indexToRemove) {
    double[][][][] returnArray = new double[array.length - 1][][][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }

  public static double[][][][][] removeElement(double[][][][][] array, int indexToRemove) {
    double[][][][][] returnArray = new double[array.length - 1][][][][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static short[] removeElement(short[] array, int indexToRemove) {
    short[] returnArray = new short[array.length - 1];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static short[][] removeElement(short[][] array, int indexToRemove) {
    short[][] returnArray = new short[array.length - 1][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static short[][][] removeElement(short[][][] array, int indexToRemove) {
    short[][][] returnArray = new short[array.length - 1][][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static short[][][][] removeElement(short[][][][] array, int indexToRemove) {
    short[][][][] returnArray = new short[array.length - 1][][][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static boolean[] removeElement(boolean[] array, int indexToRemove) {
    boolean[] returnArray = new boolean[array.length - 1];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static boolean[][] removeElement(boolean[][] array, int indexToRemove) {
    boolean[][] returnArray = new boolean[array.length - 1][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static boolean[][][] removeElement(boolean[][][] array, int indexToRemove) {
    boolean[][][] returnArray = new boolean[array.length - 1][][];
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static Object[] removeElement(Object[] array, int indexToRemove) {
    Object[] returnArray = (Object[]) Array.newInstance(array.getClass().getComponentType(), array.length - 1);
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static Object[][] removeElement(Object[][] array, int indexToRemove) {
    Object[][] returnArray = (Object[][]) Array.newInstance(array.getClass().getComponentType(), array.length - 1);
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static Object[][][] removeElement(Object[][][] array, int indexToRemove) {
    Object[][][] returnArray = (Object[][][]) Array.newInstance(array.getClass().getComponentType(), array.length - 1);
    System.arraycopy(array, 0, returnArray, 0, indexToRemove);
    System.arraycopy(array, indexToRemove + 1, returnArray, indexToRemove, returnArray.length -indexToRemove);
    return returnArray;
  }
  
  public static boolean equals(int[][] array1, int[][] array2) {
    int length = array1.length;
    if (length != array2.length) {
      return false;
    }
    for (int row = 0; row < array1.length; row++) {
      if (!Arrays.equals(array1[row], array2[row])) {return false;}
    }
    return true;
  }
  
  public static Object[] appendElement(Object[] array, Object element) {
    Object[] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static Object[][] appendElement(Object[][] array, Object[] element) {
    Object[][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static Object[][][] appendElement(Object[][][] array, Object[][] element) {
    Object[][][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static int[] appendElement(int[] array, int element) {
    int[] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static int[][] appendElement(int[][] array, int[] element) {
    int[][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static int[][][] appendElement(int[][][] array, int[][] element) {
    int[][][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static int[][][][] appendElement(int[][][][] array, int[][][] element) {
    int[][][][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static short[] appendElement(short[] array, short element) {
    short[] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static short[][] appendElement(short[][] array, short[] element) {
    short[][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static short[][][] appendElement(short[][][] array, short[][] element) {
    short[][][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static short[][][][] appendElement(short[][][][] array, short[][][] element) {
    short[][][][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static boolean[] appendElement(boolean[] array, boolean element) {
    boolean[] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static boolean[][] appendElement(boolean[][] array, boolean[] element) {
    boolean[][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static double[] appendElement(double[] array, double element) {
    double[] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static double[][] appendElement(double[][] array, double[] element) {
    double[][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static double[][][] appendElement(double[][][] array, double[][] element) {
    double[][][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static double[][][][] appendElement(double[][][][] array, double[][][] element) {
    double[][][][] returnArray = growArray(array, 1);
    returnArray[returnArray.length - 1] = element;
    return returnArray;
  }
  
  public static int[] appendArray(int[] array, int[] newElements) {
    int oldLength = array.length;
    int[] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static int[][] appendArray(int[][] array, int[][] newElements) {
    int oldLength = array.length;
    int[][] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static int[][][] appendArray(int[][][] array, int[][][] newElements) {
    int oldLength = array.length;
    int[][][] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static boolean[] appendArray(boolean[] array, boolean[] newElements) {
    int oldLength = array.length;
    boolean[] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static boolean[][] appendArray(boolean[][] array, boolean[][] newElements) {
    int oldLength = array.length;
    boolean[][] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static double[] appendArray(double[] array, double[] newElements) {
    int oldLength = array.length;
    double[] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static double[][] appendArray(double[][] array, double[][] newElements) {
    int oldLength = array.length;
    double[][] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static Object[] appendArray(Object[] array, Object[] newElements) {
    int oldLength = array.length;
    Object[] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static Object[][] appendArray(Object[][] array, Object[][] newElements) {
    int oldLength = array.length;
    Object[][] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static Object[][][] appendArray(Object[][][] array, Object[][][] newElements) {
    int oldLength = array.length;
    Object[][][] returnArray = growArray(array, newElements.length);
    System.arraycopy(newElements, 0, returnArray, oldLength, newElements.length);
    return returnArray;
  }
  
  public static boolean arrayContains(Object[] array, Object element) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == element || array[i].equals(element)) {return true;}
    }
    return false;
  }

  public static boolean arrayContains(boolean[] array, boolean value) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == value) {return true;}
    }
    return false; 
  }
  
  public static boolean arrayContains(double[] array, double value) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == value) {return true;}
    }
    return false; 
  }
  
  public static boolean arrayContains(int[] array, int value) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == value) {return true;}
    }
    return false; 
  }
  
  public static boolean arrayContains(short[] array, short value) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == value) {return true;}
    }
    return false; 
  }
  
  
  public static boolean arrayContains(int[][] arrays, int[] array) {
    
    for (int mapNum = 0; mapNum < arrays.length; mapNum++) {
      if (Arrays.equals(arrays[mapNum], array)) {return true;}
    }
    
    return false; 
  }
  
  public static int findIndex(Object[] array, Object target) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == target || array[i].equals(target)) {return i;}
    }
    return -1;
  }

  public static int findIndex(double[] array, double target) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == target) {return i;}
    }
    return -1;
  }
  
  public static int findIndex(short[] array, short target) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == target) {return i;}
    }
    return -1;
  }
  
  public static int findIndex(int[] array, int target) {
    for (int i = 0; i < array.length; i++) {
      if (array[i] == target) {return i;}
    }
    return -1;
  }

  public static boolean setsMatch(int[] set1, int[] set2) {

    int length = set1.length;
    if (length != set2.length) {return false;}
    boolean[] matches = new boolean[length];
    for (int i = 0; i < set1.length; i++) {
      for (int j = 0; j < set2.length; j++) {
        if (matches[j]) {continue;}
        if (set1[i] == set2[j]) {
          matches[j] = true;
          break;
        }
      }
    }

    boolean returnValue = true;
    for (int j = 0; j < matches.length; j++) {
      returnValue &= matches[j];
    }
    return returnValue;
  }
  
  public static double[][] toDoubleArray(int[][] array) {
    double[][] returnArray = new double[array.length][];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = toDoubleArray(array[i]);
    }
    return returnArray;
  }
  
  public static double[] toDoubleArray(int[] array) {
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[i];
    }
    return returnArray;
  }
  
  public static double[][] toArray(SimpleMatrix matrix) {
    double[][] array = new double[matrix.numRows()][matrix.numCols()];
    for (int rowNum = 0; rowNum < array.length; rowNum++) {
      for (int colNum = 0; colNum < array[0].length; colNum++) {
        array[rowNum][colNum] = matrix.get(rowNum, colNum);
      }
    }
    return array;
  }
  
  public static int[] getMissingIndices(int[] indices, int size) {
    int[] returnArray = new int[size - indices.length];
    int viewIndex = 0;
    for (int varNum = 0; varNum < size; varNum++) {
      if (ArrayUtils.arrayContains(indices, varNum)) {continue;}
      returnArray[viewIndex++] = varNum;
    }
    return returnArray;
  }
  
  /**
   * Returns the permutation that inserts the last N elements of an array of size arraySize in the indices given by newIndices
   * It essentially turns an append operation into an insertion.
   * 
   * @param arraySize
   * @param newIndices
   * @return
   */
  public static int[] getInsertionPermutation(int arraySize, int[] newIndices) {
    
    int[] returnArray = new int[arraySize];
    int numInserted = 0;
    for (int index = 0; index < arraySize; index++) {
      int swapNum = 0;
      for (swapNum = 0; swapNum < newIndices.length; swapNum++) {
        if (newIndices[swapNum] == index) {break;}
      }
      if (swapNum != newIndices.length) {
        returnArray[index] = arraySize - newIndices.length + swapNum;
        numInserted++;
      } else {
        returnArray[index] = index - numInserted;
      }
    }
    
    return returnArray;
  }
  

  public static int[] getIdentityPermutation(int arraySize) {
    
    int[] returnArray = new int[arraySize];
    for (int index = 0; index < arraySize; index++) {
      returnArray[index] = index;
    }
    
    return returnArray;
  }
  
  /*
  public static double[] spreadValues(double[][] values) {
    
    int returnSize = 0;
    for (int rowNum = 0; rowNum < values.length; rowNum++) {
      double[] row = values[rowNum];
      if (row == null) {continue;}
      returnSize += row.length;
    }
    
    double[] returnArray = new double[returnSize];
    int returnIndex = 0;
    for (int rowNum = 0; rowNum < values.length; rowNum++) {
      double[] row = values[rowNum];
      if (row == null) {continue;}
      for (int colNum = 0; colNum < row.length; colNum++) {
        returnArray[returnIndex++] = row[colNum];
      }
    }
    
    return returnArray;
  }*/
  
  public static String toString(Object[] array, String delimiter) {
	  String returnString = "";
	  if (array.length == 0) {return returnString;}
	  returnString += array[0];
	  for (int index = 1; index < array.length; index++) {
		  returnString += delimiter + array[index];
	  }
	  return returnString;	  
  }
  
  public static String toString(Object[] array) {
	  return toString(array, ", ");
  }

  
  public static String toString(int[] array, String delimiter) {
	  String returnString = "";
	  if (array.length == 0) {return returnString;}
	  returnString += array[0];
	  for (int index = 1; index < array.length; index++) {
		  returnString += delimiter + array[index];
	  }
	  return returnString;	  
  }
  
  public static String toString(int[] array) {
	  return toString(array, ", ");
  }


  public static String toString(double[] array, String delimiter) {
	  String returnString = "";
	  if (array.length == 0) {return returnString;}
	  returnString += array[0];
	  for (int index = 1; index < array.length; index++) {
		  returnString += delimiter + array[index];
	  }
	  return returnString;	  
  }
  
  public static String toString(double[] array) {
	  return toString(array, ", ");
  }

}