package matsci.util;

import java.util.Arrays;
import java.util.Random;

/*
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.linalg.EigenvalueDecomposition;
import cern.jet.math.Functions;*/

import matsci.util.arrays.ArrayUtils;
import matsci.util.arrays.Matrix3D;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class MSMath {
  
  public static Random RANDOM = new Random();
  
  public static double[][] getRandomRotationMatrix3D() {
    
    double[] quaternion = new double[] {
        RANDOM.nextGaussian(),
        RANDOM.nextGaussian(),
        RANDOM.nextGaussian(),
        RANDOM.nextGaussian()
    };
    
    double[][] returnArray = new double[3][3];
    // From http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/
    double sqw = quaternion[0] * quaternion[0];
    double sqx = quaternion[1] * quaternion[1];
    double sqy = quaternion[2] * quaternion[2];
    double sqz = quaternion[3] * quaternion[3];

    // invs (inverse square length) is only required if quaternion is not already normalised
    double invs = 1 / (sqx + sqy + sqz + sqw);
    returnArray[0][0] = ( sqx - sqy - sqz + sqw)*invs ; // since sqw + sqx + sqy + sqz =1/invs*invs
    returnArray[1][1]  = (-sqx + sqy - sqz + sqw)*invs ;
    returnArray[2][2] = (-sqx - sqy + sqz + sqw)*invs ;
    
    double tmp1 = quaternion[1] * quaternion[2];
    double tmp2 = quaternion[3] * quaternion[0];
    returnArray[1][0] = 2.0 * (tmp1 + tmp2)*invs;
    returnArray[0][1] = 2.0 * (tmp1 - tmp2)*invs;
    
    tmp1 = quaternion[1]*quaternion[3];
    tmp2 = quaternion[2]*quaternion[0];
    returnArray[2][0] = 2.0 * (tmp1 - tmp2)*invs;
    returnArray[0][2] = 2.0 * (tmp1 + tmp2)*invs;
    
    tmp1 = quaternion[2]*quaternion[3];
    tmp2 = quaternion[1]*quaternion[0];
    returnArray[2][1] = 2.0 * (tmp1 + tmp2)*invs;
    returnArray[1][2] = 2.0 * (tmp1 - tmp2)*invs;  
    
    return returnArray;
    
  }
  
  public static int product(int[] array) {
    int returnValue = 1;
    for (int i = 0; i < array.length; i++) {
      returnValue *= array[i];
    }
    return returnValue;
  }
  
  public static int choose(int n, int k) {
        
    int returnValue = 1;
    for (int i = n; i > k; i--) {
      returnValue *= i;
    }
    
    returnValue /= MSMath.factorial(n-k);
    
    return returnValue;
    
  }
  
  public static int[][] getPermutations(int size) {
    
    if (size == 0) {
      return new int[0][];
    }
    
    int numPermutations = MSMath.factorial(size);
    int[][] returnArray = new int[numPermutations][size];
    int[][] prevPermutations = getPermutations(size - 1);
    
    int returnIndex = 0;
    for (int startIndex = 0; startIndex < size; startIndex++) {
      
      for (int permIndex = 0; permIndex < prevPermutations.length; permIndex++) {
        int[] permutation = prevPermutations[permIndex];
        returnArray[returnIndex][0] = startIndex;
        System.arraycopy(permutation, 0, returnArray[returnIndex], 1, size-1);
        for (int i = 1; i < size; i++) {
          if (returnArray[returnIndex][i] >= startIndex) {
            returnArray[returnIndex][i]++;
          }
        }
        returnIndex++;
      }
    }
    
    return returnArray;
    
  }
  
  public static double average(double[] array) {
    double returnValue = 0;
    for (int i = 0; i < array.length; i++) {
      returnValue += array[i];
    }
    return returnValue / array.length;
  }
  
  public static int[] getRandomShuffle(int arrayLength) {
    int[] returnArray = new int[arrayLength];
    for (int varNum = 0; varNum < returnArray.length; varNum++) {
      returnArray[varNum] = varNum;
    }
    for (int varNum = 0; varNum < returnArray.length; varNum++) {
      int increment = RANDOM.nextInt(returnArray.length);
      int swapIndex = increment % returnArray.length;
      int tempValue = returnArray[swapIndex];
      returnArray[swapIndex] = returnArray[varNum];
      returnArray[varNum] = tempValue;
    }
    return returnArray;
  }
  
  // This should only be used for 3x3 or smaller matrices
  public static double[][] simpleLowerTriangularInverse(int[][] smallMatrix) {

    // Just so I don't have to keep calling it "smallMatrix"
    int[][] matrix = smallMatrix;
    
    if (matrix.length == 0) {
      return new double[0][0];
    }
    
    if (matrix.length == 1) {
      return new double[][] {{1 / matrix[0][0]}};
    }
    
    if (matrix.length == 2) {
      double determinant = matrix[0][0] * matrix[1][1];
      double[][] inverseMatrix = new double[2][2];
      inverseMatrix[0][0] = matrix[1][1] / determinant;
      inverseMatrix[1][0] = -matrix[1][0] / determinant;
      inverseMatrix[1][1] = matrix[0][0] / determinant;
      
      return inverseMatrix;
    }
    
    if (matrix.length == 3) {
  
      double[][] inverseMatrix = new double[3][3];
  
      double determinant = matrix[0][0] * matrix[1][1] * matrix[2][2];
      inverseMatrix[0][0] = (matrix[1][1] * matrix[2][2]) / determinant;
      inverseMatrix[1][0] = (-matrix[1][0] * matrix[2][2]) / determinant;
      inverseMatrix[1][1] = (matrix[0][0] * matrix[2][2]) / determinant;
      inverseMatrix[2][0] = (matrix[1][0] * matrix[2][1] -
                             matrix[1][1] * matrix[2][0]) / determinant;
      inverseMatrix[2][1] = (-matrix[0][0] * matrix[2][1]) / determinant;
      inverseMatrix[2][2] = (matrix[1][1] * matrix[0][0]) / determinant;

      return inverseMatrix;
    }
    
    throw new RuntimeException("Cannot yet calculate the simple inverse of a matrix with more than three dimensions.");

  }
  
  // This should only be used for 3x3 or smaller matrices
  public static double[][] simpleLowerTriangularInverse(double[][] smallMatrix) {

    // Just so I don't have to keep calling it "smallMatrix"
    double[][] matrix = smallMatrix;
    
    if (matrix.length == 0) {
      return new double[0][0];
    }
    
    if (matrix.length == 1) {
      return new double[][] {{1 / matrix[0][0]}};
    }
    
    if (matrix.length == 2) {
      double determinant = matrix[0][0] * matrix[1][1];
      double[][] inverseMatrix = new double[2][2];
      inverseMatrix[0][0] = matrix[1][1] / determinant;
      inverseMatrix[1][0] = -matrix[1][0] / determinant;
      inverseMatrix[1][1] = matrix[0][0] / determinant;
      
      return inverseMatrix;
    }
    
    if (matrix.length == 3) {
  
      double[][] inverseMatrix = new double[3][3];
  
      double determinant = matrix[0][0] * matrix[1][1] * matrix[2][2];
      inverseMatrix[0][0] = (matrix[1][1] * matrix[2][2]) / determinant;
      inverseMatrix[1][0] = (-matrix[1][0] * matrix[2][2]) / determinant;
      inverseMatrix[1][1] = (matrix[0][0] * matrix[2][2]) / determinant;
      inverseMatrix[2][0] = (matrix[1][0] * matrix[2][1] -
                             matrix[1][1] * matrix[2][0]) / determinant;
      inverseMatrix[2][1] = (-matrix[0][0] * matrix[2][1]) / determinant;
      inverseMatrix[2][2] = (matrix[1][1] * matrix[0][0]) / determinant;

      return inverseMatrix;
    }
    
    throw new RuntimeException("Cannot yet calculate the simple inverse of a matrix with more than three dimensions.");

  }

  // This should only be used for 3x3 matrices or smaller
  public static double[][] simpleInverse(double[][] smallMatrix) {

    // Just so I don't have to keep calling it "smallMatrix"
    double[][] matrix = smallMatrix;

    double determinant = determinant(matrix);

    if (determinant == 0) {
      throw new RuntimeException("Can't invert a singular matrix.");
    }
    
    if (matrix.length == 0) {
      return new double[0][0];
    }
    
    if (matrix.length == 1) {
      return new double[][] {{1 / determinant}};
    }
    
    if (matrix.length == 2) {
      double[][] inverseMatrix = new double[2][2];
      inverseMatrix[0][0] = matrix[1][1] / determinant;
      inverseMatrix[0][1] = -matrix[0][1] / determinant;
      inverseMatrix[1][0] = -matrix[1][0] / determinant;
      inverseMatrix[1][1] = matrix[0][0] / determinant;
      
      return inverseMatrix;
    }
    
    if (matrix.length == 3) {
  
      double[][] inverseMatrix = new double[3][3];
  
      inverseMatrix[0][0] = (matrix[1][1] * matrix[2][2] -
                             matrix[1][2] * matrix[2][1]) / determinant;
      inverseMatrix[0][1] = (matrix[0][2] * matrix[2][1] -
                             matrix[2][2] * matrix[0][1]) / determinant;
      inverseMatrix[0][2] = (matrix[0][1] * matrix[1][2] -
                             matrix[0][2] * matrix[1][1]) / determinant;
      inverseMatrix[1][0] = (matrix[1][2] * matrix[2][0] -
                             matrix[1][0] * matrix[2][2]) / determinant;
      inverseMatrix[1][1] = (matrix[0][0] * matrix[2][2] -
                             matrix[0][2] * matrix[2][0]) / determinant;
      inverseMatrix[1][2] = (matrix[1][0] * matrix[0][2] -
                             matrix[1][2] * matrix[0][0]) / determinant;
      inverseMatrix[2][0] = (matrix[1][0] * matrix[2][1] -
                             matrix[1][1] * matrix[2][0]) / determinant;
      inverseMatrix[2][1] = (matrix[0][1] * matrix[2][0] -
                             matrix[0][0] * matrix[2][1]) / determinant;
      inverseMatrix[2][2] = (matrix[1][1] * matrix[0][0] -
                             matrix[1][0] * matrix[0][1]) / determinant;

      return inverseMatrix;
    }

    throw new RuntimeException("Cannot yet calculate the simple inverse of a matrix with more than three dimensions.");
  }
  
  // This should only be used for 3x3 matrices
  public static double[][] simpleInverse(int[][] smallMatrix) {

    // Just so I don't have to keep calling it "smallMatrix"
    int[][] matrix = smallMatrix;

    double determinant = determinant(matrix);

    if (determinant == 0) {
      throw new RuntimeException("Can't invert a singular matrix.");
    }
    
    if (matrix.length == 0) {
      return new double[0][0];
    }
    
    if (matrix.length == 1) {
      return new double[][] {{1 / determinant}};
    }
    
    if (matrix.length == 2) {
      double[][] inverseMatrix = new double[2][2];
      inverseMatrix[0][0] = matrix[1][1] / determinant;
      inverseMatrix[0][1] = -matrix[0][1] / determinant;
      inverseMatrix[1][0] = -matrix[1][0] / determinant;
      inverseMatrix[1][1] = matrix[0][0] / determinant;
      
      return inverseMatrix;
    }
    
    if (matrix.length == 3) {
  
      double[][] inverseMatrix = new double[3][3];
  
      inverseMatrix[0][0] = (matrix[1][1] * matrix[2][2] -
                             matrix[1][2] * matrix[2][1]) / determinant;
      inverseMatrix[0][1] = (matrix[0][2] * matrix[2][1] -
                             matrix[2][2] * matrix[0][1]) / determinant;
      inverseMatrix[0][2] = (matrix[0][1] * matrix[1][2] -
                             matrix[0][2] * matrix[1][1]) / determinant;
      inverseMatrix[1][0] = (matrix[1][2] * matrix[2][0] -
                             matrix[1][0] * matrix[2][2]) / determinant;
      inverseMatrix[1][1] = (matrix[0][0] * matrix[2][2] -
                             matrix[0][2] * matrix[2][0]) / determinant;
      inverseMatrix[1][2] = (matrix[1][0] * matrix[0][2] -
                             matrix[1][2] * matrix[0][0]) / determinant;
      inverseMatrix[2][0] = (matrix[1][0] * matrix[2][1] -
                             matrix[1][1] * matrix[2][0]) / determinant;
      inverseMatrix[2][1] = (matrix[0][1] * matrix[2][0] -
                             matrix[0][0] * matrix[2][1]) / determinant;
      inverseMatrix[2][2] = (matrix[1][1] * matrix[0][0] -
                             matrix[1][0] * matrix[0][1]) / determinant;

      return inverseMatrix;
    }

    throw new RuntimeException("Cannot yet calculate the inverse of a matrix with more than three dimensions.");
  }
  
  public static int[][] matrixTimesMatrix(int[][] matrix, int[][] matrix2) {
    
    int[][] returnArray = new int[matrix.length][];
    for (int i = 0; i < matrix.length; i++) {
      returnArray[i] = vectorTimesMatrix(matrix[i], matrix2);
    }
    
    return returnArray;
    
  }
  
  public static int[] vectorTimesMatrix(int[] vector, int[][] matrix, int[] returnArray) {
    
    Arrays.fill(returnArray, 0);
    for (int rowNum = 0; rowNum < vector.length; rowNum++) {
      int[] row = matrix[rowNum];
      double vecVal = vector[rowNum];
      for (int colNum = 0; colNum < returnArray.length; colNum++) {
        returnArray[colNum] += row[colNum] * vecVal;
      }
    }

    return returnArray;
  }
  

  public static int[] vectorTimesMatrix(int[] vector, int[][] matrix) {

    int[] returnArray = new int[matrix.length];
    return vectorTimesMatrix(vector, matrix, returnArray);

  }
  

  public static double[] vectorTimesMatrix(double[] vector, int[][] matrix) {

    double[] returnArray = new double[matrix.length];
    return vectorTimesMatrix(vector, matrix, returnArray);

  }
  
  public static double[] vectorTimesMatrix(double[] vector, int[][] matrix, double[] returnArray) {
    
    Arrays.fill(returnArray, 0);
    for (int rowNum = 0; rowNum < vector.length; rowNum++) {
      int[] row = matrix[rowNum];
      double vecVal = vector[rowNum];
      for (int colNum = 0; colNum < returnArray.length; colNum++) {
        returnArray[colNum] += row[colNum] * vecVal;
      }
    }

    return returnArray;
  }
  
  
  public static double[] vectorTimesMatrix(int[] vector, double[][] matrix, double[] returnArray) {
    
    Arrays.fill(returnArray, 0);
    for (int rowNum = 0; rowNum < vector.length; rowNum++) {
      double[] row = matrix[rowNum];
      double vecVal = vector[rowNum];
      for (int colNum = 0; colNum < returnArray.length; colNum++) {
        returnArray[colNum] += row[colNum] * vecVal;
      }
    }

    return returnArray;
  }

  public static double[] vectorTimesMatrix(int[] vector, double[][] matrix) {

    double[] returnArray = new double[matrix.length];
    return vectorTimesMatrix(vector, matrix, returnArray);

  }
  
  public static double[] vectorTimesMatrix(double[] vector, double[][] matrix, double[] returnArray) {
    
    Arrays.fill(returnArray, 0);
    for (int rowNum = 0; rowNum < vector.length; rowNum++) {
      double[] row = matrix[rowNum];
      double vecVal = vector[rowNum];
      for (int colNum = 0; colNum < returnArray.length; colNum++) {
        returnArray[colNum] += row[colNum] * vecVal;
      }
    }

    return returnArray;
  }

  public static double[] vectorTimesMatrix(double[] vector, double[][] matrix) {

    double[] returnArray = new double[matrix.length];
    return vectorTimesMatrix(vector, matrix, returnArray);

  }  
  
  // This should only be used for 3x3 matrices
  public static double[] matrixTimesVector(double[][] matrix, double[] vector) {

    if (matrix.length == 0) {
      return new double[0];
    }
    
    double[] result = new double[matrix.length];
    
    /*double[][] matrix = threeByThreeArray;
    double[] vector = threeDVector;

    double[] result = new double[3];

    result[0] = matrix[0][0] * vector[0] + matrix[0][1] * vector[1] +
        matrix[0][2] * vector[2];
    result[1] = matrix[1][0] * vector[0] + matrix[1][1] * vector[1] +
        matrix[1][2] * vector[2];
    result[2] = matrix[2][0] * vector[0] + matrix[2][1] * vector[1] +
        matrix[2][2] * vector[2];*/
    
    for (int rowNum = 0; rowNum < matrix.length; rowNum++) {
      double[] row = matrix[rowNum];
      result[rowNum] = MSMath.dotProduct(row, vector);
    }

    return result;
  }
  
  public static double[] matrixTimesVector(int[][] matrix, double[] vector) {

    if (matrix.length == 0) {
      return new double[0];
    }
    
    double[] result = new double[matrix.length];
    
    for (int rowNum = 0; rowNum < matrix.length; rowNum++) {
      int[] row = matrix[rowNum];
      result[rowNum] = MSMath.dotProduct(row, vector);
    }

    return result;
  }
  

  public static double[][] matrixTimesScalar(double[][] matrix, double scalar) {

    int size = matrix.length;
    double[][] returnMatrix = new double[size][];
    for (int i = 0; i<size; i++) {
      returnMatrix[i] = MSMath.vectorTimesScalar(matrix[i], scalar);
    }
    return returnMatrix;
  }
  
  public static int[][] matrixTimesScalar(int[][] matrix, int scalar) {

    int size = matrix.length;
    int[][] returnMatrix = new int[size][];
    for (int i = 0; i<size; i++) {
      returnMatrix[i] = MSMath.vectorTimesScalar(matrix[i], scalar);
    }
    return returnMatrix;
  }

  public static double[] vectorTimesScalar(double[] vector, double scalar) {

    int size = vector.length;
    double[] returnVector = new double[size];
    for (int i = 0; i<size; i++) {
      returnVector[i] = vector[i] * scalar;
    }
    return returnVector;
  }
  
  public static int[] vectorTimesScalar(int[] vector, int scalar) {

    int size = vector.length;
    int[] returnVector = new int[size];
    for (int i = 0; i<size; i++) {
      returnVector[i] = vector[i] * scalar;
    }
    return returnVector;
  }

  public static double[][] matrixMultiply(int[][] matrix1, double[][] matrix2) {

	  if (matrix1.length == 0 && matrix2.length == 0) {return new double[0][0];}
    if (matrix1[0].length != matrix2.length) {
        throw new RuntimeException("Cannot muliply matrices of incomaptible dimensions");
    }
    double[][] returnMatrix = new double[matrix1.length][matrix2[0].length];
    for (int row=0; row < returnMatrix.length; row++) {
      for (int col=0; col < returnMatrix[row].length; col++) {
        for (int counter=0; counter < matrix2.length; counter++) {
          returnMatrix[row][col] += matrix1[row][counter] * matrix2[counter][col];
        }
      }
    }
    return returnMatrix;
  }
  
  public static double[][] matrixMultiply(double[][] matrix1, int[][] matrix2) {

    if (matrix1[0].length != matrix2.length) {
        throw new RuntimeException("Cannot muliply matrices of incomaptible dimensions");
    }
    double[][] returnMatrix = new double[matrix1.length][matrix2[0].length];
    for (int row=0; row < returnMatrix.length; row++) {
      for (int col=0; col < returnMatrix[row].length; col++) {
        for (int counter=0; counter < matrix2.length; counter++) {
          returnMatrix[row][col] += matrix1[row][counter] * matrix2[counter][col];
        }
      }
    }
    return returnMatrix;
  }
  
  public static int[][] matrixMultiply(int[][] matrix1, int[][] matrix2) {

	  if (matrix1.length == 0 && matrix2.length == 0) {return new int[0][];}
    if (matrix1[0].length != matrix2.length) {
        throw new RuntimeException("Cannot muliply matrices of incomaptible dimensions");
    }
    int[][] returnMatrix = new int[matrix1.length][matrix2[0].length];
    for (int row=0; row < returnMatrix.length; row++) {
      for (int col=0; col < returnMatrix[row].length; col++) {
        for (int counter=0; counter < matrix2.length; counter++) {
          returnMatrix[row][col] += matrix1[row][counter] * matrix2[counter][col];
        }
      }
    }
    return returnMatrix;
  }

  public static double[][] matrixMultiply(double[][] matrix1, double[][] matrix2) {
    
    if ((matrix1.length == 0) || (matrix2.length == 0)) {
      return new double[0][0];
    }

    if (matrix1[0].length != matrix2.length) {
        throw new RuntimeException("Cannot multiply matrices of incomaptible dimensions");
    }
    
    double[][] returnMatrix = new double[matrix1.length][matrix2[0].length];
    for (int row=0; row < returnMatrix.length; row++) {
      for (int col=0; col < returnMatrix[row].length; col++) {
        for (int counter=0; counter < matrix2.length; counter++) {
          returnMatrix[row][col] += matrix1[row][counter] * matrix2[counter][col];
        }
      }
    }
    return returnMatrix;
  }
  
  public static double[][] matrixMultiply(double[][] matrix1, double[][] matrix2, double[][] template) {
    
    if ((matrix1.length == 0) || (matrix2.length == 0)) {
      return new double[0][0];
    }

    if (matrix1[0].length != matrix2.length) {
        throw new RuntimeException("Cannot multiply matrices of incomaptible dimensions");
    }
    
    double[][] returnMatrix = (template == null || (template.length == 0) || (template.length != matrix1.length) || (template[0].length != matrix2[0].length))
        ? new double[matrix1.length][matrix2[0].length] 
        : template;


    for (int rowNum = 0; rowNum < template.length; rowNum++) {
      Arrays.fill(returnMatrix[rowNum], 0);
    }
        
    for (int row=0; row < returnMatrix.length; row++) {
      for (int col=0; col < returnMatrix[row].length; col++) {
        for (int counter=0; counter < matrix2.length; counter++) {
          returnMatrix[row][col] += matrix1[row][counter] * matrix2[counter][col];
        }
      }
    }
    return returnMatrix;
  }
  

  public static double[][] matrixMultiply(int[][] matrix1, double[][] matrix2, double[][] template) {
    
    if ((matrix1.length == 0) || (matrix2.length == 0)) {
      return new double[0][0];
    }

    if (matrix1[0].length != matrix2.length) {
        throw new RuntimeException("Cannot multiply matrices of incomaptible dimensions");
    }
    
    double[][] returnMatrix = (template == null || (template.length == 0) || (template.length != matrix1.length) || (template[0].length != matrix2[0].length))
        ? new double[matrix1.length][matrix2[0].length] 
        : template;
    
    for (int rowNum = 0; rowNum < template.length; rowNum++) {
      Arrays.fill(returnMatrix[rowNum], 0);
    }
    
    for (int row=0; row < returnMatrix.length; row++) {
      for (int col=0; col < returnMatrix[row].length; col++) {
        for (int counter=0; counter < matrix2.length; counter++) {
          returnMatrix[row][col] += matrix1[row][counter] * matrix2[counter][col];
        }
      }
    }
    return returnMatrix;
  }

  public static double determinant(double[][] matrix) {

    int dim = matrix.length;

    for (int row = 0; row < dim; row++) {
      if (matrix[row].length != dim) {
        return 0;
      }
    }

    if (dim == 0) {
      return 1;
    } else if (dim == 1) {
      return matrix[0][0];
    } else if (dim == 2) {
      return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
    } else if (dim == 3) {
      return
          matrix[0][0] *
          (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1]) -
          matrix[0][1] *
          (matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[2][0]) +
          matrix[0][2] *
          (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
    }

    throw new RuntimeException("Cannot yet calculate the determinant of a matrix with more than three dimensions.");
  }

  public static int determinant(int[][] matrix) {

    int dim = matrix.length;

    for (int row = 0; row < dim; row++) {
      if (matrix[row].length != dim) {
        return 0;
      }
    }

    if (dim == 0) {
      return 1;
    } else if (dim == 1) {
      return matrix[0][0];
    } else if (dim == 2) {
      return (matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]);
    } else if (dim == 3) {
		  return
		      matrix[0][0] *
		      (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1]) -
		      matrix[0][1] *
		      (matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[2][0]) +
		      matrix[0][2] *
		      (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
    }
    
    throw new RuntimeException("Cannot yet calculate the determinant of a matrix with more than three dimensions.");

  }

  public static double[][] transpose(double[][] matrix) {
    int rows = matrix.length;
    
    if (rows == 0) {return new double[0][0];} // This was commented out.  Why?
    int cols = matrix[0].length;
    double[][] returnMatrix = new double[cols][rows];
    for (int i = 0; i<rows; i++) {
      for (int j = 0; j < cols; j++) {
        returnMatrix[j][i] = matrix[i][j];
      }
    }
    return returnMatrix;
  }
  
  public static int[][] transpose(int[][] matrix) {
    int rows = matrix.length;
    //if (rows == 0) {return new double[0][0];}
    int cols = matrix[0].length;
    int[][] returnMatrix = new int[cols][rows];
    for (int i = 0; i<rows; i++) {
      for (int j = 0; j < cols; j++) {
        returnMatrix[j][i] = matrix[i][j];
      }
    }
    return returnMatrix;
  }

  public static double[] normalize(double[] vector) {
    double mag = magnitude(vector);
    double[] returnVector = new double[vector.length];
    for (int i = 0; i < vector.length; i++) {
      returnVector[i] = vector[i] / mag;
    }
    return returnVector;
  }
  
  public static double[] normalizeWeights(double[] vector) {
    double mag = 0;
    for (int i = 0; i < vector.length; i++) {
      mag += vector[i];
    }
    double[] returnVector = new double[vector.length];
    for (int i = 0; i < vector.length; i++) {
      returnVector[i] = vector.length * vector[i] / mag;
    }
    return returnVector;
  }
  

  public static double[] normalizeToUnitSum(double[] vector) {
    double mag = 0;
    for (int i = 0; i < vector.length; i++) {
      mag += vector[i];
    }
    double[] returnVector = new double[vector.length];
    for (int i = 0; i < vector.length; i++) {
      returnVector[i] = vector[i] / mag;
    }
    return returnVector;
  }

  public static double[] crossProduct(double[] vector1, double[] vector2) {

    if ( (vector1.length != 3) || (vector2.length) != 3) {
      throw new RuntimeException(
          "Must have 3D vectors to calculate cross product");
    }

    double[] result = new double[3];
    result[0] = (vector1[1] * vector2[2]) - (vector1[2] * vector2[1]);
    result[1] = (vector1[2] * vector2[0]) - (vector1[0] * vector2[2]);
    result[2] = (vector1[0] * vector2[1]) - (vector1[1] * vector2[0]);
    return result;

  }
  

  public static double magnitudeSquared(double[] vector) {

    int numCoords = vector.length;
    double magSquared = 0;
    double coord;
    for (int coordNum = numCoords - 1; coordNum >= 0; coordNum--) {
      coord = vector[coordNum];
      magSquared += coord * coord;
    }
    return magSquared;
  }

  public static double magnitude(double[] vector) {
    
    return Math.sqrt(magnitudeSquared(vector));

  }

  public static double dotProduct(double[] vector1, double[] vector2) {

    int numCoords = vector1.length;
    if (numCoords != vector2.length) {
      throw new RuntimeException(
          "Can't take dot product of vectors with different length!");
    }

    double result = 0;
    for (int coordNum = numCoords - 1; coordNum >= 0; coordNum--) {
      result += vector1[coordNum] * vector2[coordNum];
    }

    return result;
  }
  
  public static double dotProduct(int[] vector1, double[] vector2) {

    int numCoords = vector1.length;
    if (numCoords != vector2.length) {
      throw new RuntimeException(
          "Can't take dot product of vectors with different length!");
    }

    double result = 0;
    for (int coordNum = numCoords - 1; coordNum >= 0; coordNum--) {
      result += vector1[coordNum] * vector2[coordNum];
    }

    return result;
  }
  
  public static double dotProduct(double[] vector1, int[] vector2) {

    int numCoords = vector1.length;
    if (numCoords != vector2.length) {
      throw new RuntimeException(
          "Can't take dot product of vectors with different length!");
    }

    double result = 0;
    for (int coordNum = numCoords - 1; coordNum >= 0; coordNum--) {
      result += vector1[coordNum] * vector2[coordNum];
    }

    return result;
  }
  
  public static double dotProduct(int[] vector1, int[] vector2) {

    int numCoords = vector1.length;
    if (numCoords != vector2.length) {
      throw new RuntimeException(
          "Can't take dot product of vectors with different length!");
    }

    double result = 0;
    for (int coordNum = numCoords - 1; coordNum >= 0; coordNum--) {
      result += vector1[coordNum] * vector2[coordNum];
    }

    return result;
  }
  
  public static double[] roundDouble(double[] array) {
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = Math.round(array[i]);
    }
    return returnArray;
  }
  
  public static int[] round(double[] array) {
    int[] returnArray = new int[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = (int) Math.round(array[i]);
    }
    return returnArray;
  }
  
  public static int[][] round(double[][] array) {
    int[][] returnArray = new int[array.length][];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = MSMath.round(array[i]);
    }
    return returnArray;
  }

  public static void roundWithPrecision(double[] array, double precision) {
    for (int i = 0; i < array.length; i++) {
      array[i] = roundWithPrecision(array[i], precision);
    }
  }

  public static double roundWithPrecision(double number, double precision) {
    return roundWithPrecision(number, 0, precision);
  }
  
  public static double roundWithPrecision(double number, double offset, double precision) {
    return Math.round((number - offset) / precision) * precision + offset;
  }
  
  public static int compareWithPrecision(double number1, double number2, double offset, double precision) {
    double rounded1 = roundWithPrecision(number1, offset, precision);
    double rounded2 = roundWithPrecision(number2, offset, precision);
    return Double.compare(rounded1, rounded2);
  }
  
  public static int compareWithPrecision(double[] array1, double[] array2, double offset, double precision) {
    if (array1.length < array2.length) {return -1;}
    if (array2.length < array1.length) {return 1;}
    for (int i = 0; i < array1.length; i++) {
      int elementCompare = compareWithPrecision(array1[i], array2[i], offset, precision);
      if (elementCompare != 0) {
        return elementCompare;
      }
    }
    return 0;
  }
  

  public static int compareWithPrecision(Double[] array1, Double[] array2, double precision) {
    return compareWithPrecision(array1, array2, 0, precision);
  }
  
  public static int compareWithPrecision(Double[] array1, Double[] array2, double offset, double precision) {
    if (array1.length < array2.length) {return -1;}
    if (array2.length < array1.length) {return 1;}
    for (int i = 0; i < array1.length; i++) {
      int elementCompare = compareWithPrecision(array1[i], array2[i], offset, precision);
      if (elementCompare != 0) {
        return elementCompare;
      }
    }
    return 0;
  }
  
  /**
   * Calculated maxArray - minArray
   * @param maxArray
   * @param minArray
   * @return
   */
  public static double[][] arrayDiff(double[][] maxArray, double[][] minArray) {

    int length = minArray.length;
    if (length != maxArray.length) {
      throw new RuntimeException(
          "Can't subtract two arrays of different dimensions!");
    }

    double[][] returnArray = new double[length][];
    for (int i = 0; i < length; i++) {
      returnArray[i] = arrayDiff(maxArray[i], minArray[i]);
    }
    return returnArray;
  }
  
  /**
   * Calculated maxArray - minArray
   * @param maxArray
   * @param minArray
   * @return
   */
  public static double[] arrayDiff(int[] maxArray, double[] minArray) {

    int length = minArray.length;
    if (length != maxArray.length) {
      throw new RuntimeException(
          "Can't subtract two arrays of different dimensions!");
    }

    double[] returnArray = new double[length];
    for (int i = 0; i < length; i++) {
      returnArray[i] = maxArray[i] - minArray[i];
    }
    return returnArray;
  }
  
  

  /**
   * Calculated maxArray - minArray
   * @param maxArray
   * @param minArray
   * @return
   */
  public static double[] arrayDiff(double[] maxArray, double[] minArray) {

    int length = minArray.length;
    if (length != maxArray.length) {
      throw new RuntimeException(
          "Can't subtract two arrays of different dimensions!");
    }

    double[] returnArray = new double[length];
    for (int i = 0; i < length; i++) {
      returnArray[i] = maxArray[i] - minArray[i];
    }
    return returnArray;
  }
  
  /**
   * Calculated maxArray - minArray
   * @param maxArray
   * @param minArray
   * @return
   */
  public static int[] arrayDiff(int[] maxArray, int[] minArray) {

    int length = minArray.length;
    if (length != maxArray.length) {
      throw new RuntimeException(
          "Can't subtract two arrays of different dimensions!");
    }

    int[] returnArray = new int[length];
    for (int i = 0; i < length; i++) {
      returnArray[i] = maxArray[i] - minArray[i];
    }
    return returnArray;
  }
  
  public static double[] arrayAbs(double[] array) {
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = Math.abs(array[i]);
    }
    return returnArray;
  }
  
  public static double[] arrayAverage(double[] maxArray, double[] minArray) {
    int length = minArray.length;
    if (length != maxArray.length) {
      throw new RuntimeException(
          "Can't average two arrays of different dimensions!");
    }
    
    double[] returnArray = new double[length];
    for (int i = 0; i < length; i++) {
      returnArray[i] = (maxArray[i] + minArray[i]) / 2;
    }
    return returnArray;
  }
  
  public static double[][] arraySubtract(double[][] array1, double[][] array2) {

    int length = array1.length;
    if (length != array2.length) {
      throw new RuntimeException(
          "Can't add two arrays of different dimensions!");
    }

    double[][] returnArray = new double[length][];
    for (int i = 0; i < length; i++) {
      returnArray[i] = MSMath.arraySubtract(array1[i], array2[i]);
    }
    return returnArray;
  }
  
  public static double[][] arraySubtractTolerateNull(double[][] array1, double[][] array2) {

    int length = array1.length;
    if (length != array2.length) {
      throw new RuntimeException(
          "Can't add two arrays of different dimensions!");
    }

    double[][] returnArray = new double[length][];
    for (int i = 0; i < length; i++) {
      if (array1[i] == null || array2[i] == null) {continue;}
      returnArray[i] = MSMath.arraySubtract(array1[i], array2[i]);
    }
    return returnArray;
  }
  
  public static double[] arraySubtract(double[] array1, double[] array2) {

    int length = array1.length;
    if (length != array2.length) {
      throw new RuntimeException(
          "Can't add two arrays of different dimensions!");
    }

    double[] returnArray = new double[length];
    for (int i = 0; i < length; i++) {
      returnArray[i] = array1[i] - array2[i];
    }
    return returnArray;
  }
  

  public static double[] arrayAdd(double[] array1, double[] array2) {

    int length = array1.length;
    if (length != array2.length) {
      throw new RuntimeException(
          "Can't add two arrays of different dimensions!");
    }

    double[] returnArray = new double[length];
    for (int i = 0; i < length; i++) {
      returnArray[i] = array2[i] + array1[i];
    }
    return returnArray;
  }
  
  public static double[] fastArrayAdd(double[] array1, double[] array2) {

    for (int i = 0; i < array1.length; i++) {
      array1[i] += array2[i];
    }
    return array1;
  }
  
  public static double[][] arrayAdd(double[][] array1, double[][] array2) {

    int length = array1.length;
    if (length != array2.length) {
      throw new RuntimeException(
          "Can't add two arrays of different dimensions!");
    }

    double[][] returnArray = new double[length][];
    for (int i = 0; i < length; i++) {
      returnArray[i] = MSMath.arrayAdd(array1[i], array2[i]);
    }
    return returnArray;
  }
  
  public static double[] arrayAdd(double[] array1, int[] array2) {

    int length = array1.length;
    if (length != array2.length) {
      throw new RuntimeException(
          "Can't add two arrays of different dimensions!");
    }

    double[] returnArray = new double[length];
    for (int i = 0; i < length; i++) {
      returnArray[i] = array2[i] + array1[i];
    }
    return returnArray;
  }
  
  public static int[] arrayAdd(int[] array1, int[] array2) {

    int length = array1.length;
    if (length != array2.length) {
      throw new RuntimeException(
          "Can't add two arrays of different dimensions!");
    }

    int[] returnArray = new int[length];
    for (int i = 0; i < length; i++) {
      returnArray[i] = array2[i] + array1[i];
    }
    return returnArray;
  }
  
  public static int arraySum(int[] array) {
    int total = 0;
    for (int i = 0; i < array.length; i++) {
      total += array[i];
    }
    return total;
  }
  
  public static double arraySum(double[] array) {
    double total = 0;
    for (int i = 0; i < array.length; i++) {
      total += array[i];
    }
    return total;
  }
  
  public static void arrayDivideInPlace(double[] array, double divisor) {
    for (int i = 0; i < array.length; i++) {
      array[i] /= divisor;
    }
  }
  
  public static double[] arrayDivide(double[] array, double divisor) {
    
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[i] / divisor;
    }
    return returnArray;
  }
  
  public static double[] arrayDivide(int[] array, double divisor) {
	    
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[i] / divisor;
    }
    return returnArray;
  }	  

  public static double[][] arrayDivide(double[][] array, double divisor) {
    
    double[][] returnArray = new double[array.length][];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = arrayDivide(array[i], divisor);
    }
    return returnArray;
  }
  
  public static double[][][] arrayDivide(double[][][] array, double divisor) {
    
    double[][][] returnArray = new double[array.length][][];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = arrayDivide(array[i], divisor);
    }
    return returnArray;
  }
  
  public static int[] arrayDivide(int[] array, int divisor) {
    
    int[] returnArray = new int[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[i] / divisor;
    }
    return returnArray;
  }
  
  public static int[] arrayMultiply(int[] array, int multiplier) {
    
    int[] returnArray = new int[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[i] * multiplier;
    }
    return returnArray;
  }
  
  public static int[][] arrayMultiply(int[][] array, int multiplier) {
    
    int[][] returnArray = new int[array.length][];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = MSMath.arrayMultiply(array[i], multiplier);
    }
    return returnArray;
  }
  
  
  public static double[] arrayMultiply(int[] array, double multiplier) {
    
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[i] * multiplier;
    }
    return returnArray;
  }
  

  public static void arrayMultiplyInPlace(double[] array, double multiplier) {
    
    for (int i = 0; i < array.length; i++) {
      array[i] *= multiplier;
    }
  }
  
  public static double[][] arrayMultiply(double[][] array, double multiplier) {
    
    double[][] returnArray = new double[array.length][];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = MSMath.arrayMultiply(array[i], multiplier);
    }
    return returnArray;
  }
  
  public static double[] arrayPower(double[] array, double power) {
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = Math.pow(array[i], power);
    }
    return returnArray;
  }
  
  public static double[] arrayMultiply(double[] array, double multiplier) {
    
    double[] returnArray = new double[array.length];
    for (int i = 0; i < array.length; i++) {
      returnArray[i] = array[i] * multiplier;
    }
    return returnArray;
  }
  
  public static double[] arrayMultiply(double[] array1, double[] array2) {
    if (array1.length != array2.length) {
      throw new RuntimeException("Arrays must be of same length to be multiplied");
    }
    double[] returnArray = new double[array1.length];
    for (int i = 0; i < array1.length; i++) {
      returnArray[i] = array1[i] * array2[i];
    }
    return returnArray;
  }
  
  public static double[] fastArrayMultiply(double[] array1, double[] array2) {
    for (int i = 0; i < array1.length; i++) {
      array1[i] = array1[i] * array2[i];
    }
    return array1;
  }

  // Finds the least common multiple of an array of ints
  public static int LCM(int[] intArray) {
    
    return LCM(intArray, true);
    
  }
  
  // Finds the least common multiple of an array of ints
  public static int LCM(int[] intArray, boolean ignoreZeros) {
    int[] allFactors = new int[0];
    int[] maxFactorCounts = new int[0];
    for (int i = 0; i < intArray.length; i++) {
      if (intArray[i] == 0) {
        if (ignoreZeros) {continue;}
        return 0;
      }
      int[] factors = primeFactors(intArray[i]);
      int[] factorCounts = new int[maxFactorCounts.length];
      for (int factNum = 0; factNum < factors.length; factNum++) {
        int factor = factors[factNum];
        int factIndex = ArrayUtils.findIndex(allFactors, factor);
        if (factIndex < 0) {
          factIndex = allFactors.length;
          allFactors = ArrayUtils.appendElement(allFactors, factor);
          maxFactorCounts = ArrayUtils.growArray(maxFactorCounts, 1);
          factorCounts = ArrayUtils.growArray(factorCounts, 1);
        }
        factorCounts[factIndex]++;
      }
      for (int knownFactNum = 0; knownFactNum < factorCounts.length; knownFactNum++) {
        maxFactorCounts[knownFactNum] = Math.max(maxFactorCounts[knownFactNum], factorCounts[knownFactNum]);
      }
    }
    
    int returnValue = 1;
    for (int knownFactNum = 0; knownFactNum < maxFactorCounts.length; knownFactNum++) {
      returnValue *= Math.pow(allFactors[knownFactNum], maxFactorCounts[knownFactNum]);
    }
    return returnValue;
  }
  

  // Finds the greatest common factor of an array of ints
  public static int GCF(int[] intArray) {
	  
	int[] cloneArray = new int[intArray.length];
	for (int i = 0; i < cloneArray.length; i++) {
		cloneArray[i] = Math.abs(intArray[i]);
	}
    Arrays.sort(cloneArray);
    
    // Deal with possible zeroes in the array
    int[] factors = null;
    for (int startIndex = 0; startIndex < cloneArray.length; startIndex++) {
    	int value = cloneArray[startIndex];
    	if (value == 0) {continue;}
    	factors = factor(value);
    	break;
    }
    if (factors == null) {return 0;}
    int currFactor;
    boolean foundFactor;
    for (int i = factors.length - 1; i >= 0; i--) {
      currFactor = factors[i];
      foundFactor = true;
      for (int j = 1; j < cloneArray.length; j++) {
        if (cloneArray[j] % currFactor != 0) {
          foundFactor = false;
          break;
        }
      }
      if (foundFactor) {
        return currFactor;
      }
    }
    return 1;
  }
  
  // Finds the greatest common factor of an array of ints
  public static int GCF(int[][] intArray) {
    
    int[] mergedArray = new int[0];
    for (int rowNum = 0; rowNum < intArray.length; rowNum++) {
      mergedArray = ArrayUtils.appendArray(mergedArray, intArray[rowNum]);
    }
    
    return GCF(mergedArray);
    
  }
  
  public static long longFactorial(int value) {
    long returnValue = 1;
    while (value > 1) {
      returnValue *= value;
      value--;
    }
    return returnValue;
  }
  
  public static int factorial(int value) {
    int returnValue = 1;
    while (value > 1) {
      returnValue *= value;
      value--;
    }
    return returnValue;
  }

  // Factors a positive number
  public static int[] factor(int value) {

    int max = (int) Math.floor(Math.sqrt(value));
    // Fist count the number of factors
    int numFactors = 0;
    for (int i = 1; i <= max; i++) {
      if (value % i == 0) {
        numFactors++;
        if (value / i != i) {
          numFactors++;
        }
      }
    }
    int[] factors = new int[numFactors];
    int factNum = 0;
    for (int i = 1; i <= max; i++) {
      if (value % i == 0) {
        factors[factNum] = i;
        factNum++;
        if (value / i != i) {
          factors[numFactors - factNum] = value / i;
        }
      }
    }
    return factors;
  }
  

  // Gets the positive prime factors of a number
  public static int[] primeFactors(int value) {
    
    value = Math.abs(value);
    int[] returnArray = new int[0];
    for (int i = 2; i <= value; i++) {
      if (value % i == 0) {
        returnArray = ArrayUtils.appendElement(returnArray, i);
        returnArray = ArrayUtils.appendArray(returnArray, primeFactors(value / i));
        return returnArray;
      }
    }
    
    return returnArray;
  }
  
  /**
  *
  * @param number The number to factor
  * @param numFactors The number of factors that should be in each set
  * Permutations of the same set of factors are listed as unique sets.
  * @return A complete list of arrays, each numFactors long, that contain a unique set of factors.
  */
 public static int[][] getFactorSets(int number, int numFactors) {

   if (numFactors < 1) {
     return new int[0][0];
   }

   if (numFactors == 1) {
     return new int[][] {{number}};
   }

   int[][] returnArray = new int[0][];

   for (int factor = 1; factor <= number; factor++) {
     if ( (number % factor) == 0) {
       int quotient = number / factor;
       int[][] subFactors = getFactorSets(quotient, numFactors - 1);
       int numOldSets = returnArray.length;
       returnArray = ArrayUtils.growArray(returnArray, subFactors.length);
       for (int newSetNum = 0; newSetNum < subFactors.length; newSetNum++) {
         int[] newSet = new int[numFactors];
         newSet[0] = factor;
         System.arraycopy(subFactors[newSetNum], 0, newSet, 1, numFactors - 1);
         returnArray[numOldSets + newSetNum] = newSet;
       }
     }
   }

   return returnArray;
  }

  public static int modFloor(int dividend, int divisor) {
    return dividend - (divisor * divFloor(dividend, divisor));
  }
  
  public static int divFloor(int dividend, int divisor) {
    int intDiv = dividend / divisor;
    if ((dividend >= 0) && (divisor >= 0)) {return intDiv;}
    if ((dividend <= 0) && (divisor <= 0)) {return intDiv;}
    if (dividend % divisor == 0) {return intDiv;}
    return intDiv - 1;
    //return dividend < 0 ? (divisor < 0? (dividend / divisor) + 1 : (dividend / divisor) -1 ) : (dividend / divisor);
  }
  
  public static int[][] getCombinations(int combinationSize, int[] sourceSet, int currIndex, int[] currCombination) {
    if (currCombination.length == combinationSize) {return new int[][] {currCombination};}
    int[][] returnArray = new int[0][];
    for (int index = currIndex; index < sourceSet.length; index++) {
      int[] nextCombination = ArrayUtils.appendElement(currCombination, sourceSet[index]);
      int[][] nextCombinations = getCombinations(combinationSize, sourceSet, index + 1, nextCombination);
      returnArray = ArrayUtils.appendArray(returnArray, nextCombinations);
    }
    return returnArray;
  }
  
  public static int[][] getCombinations(int combinationSize, int sourceSetSize) {
    return getCombinations(combinationSize, sourceSetSize, 0, new int[0]);
  }
  
  public static int[][] getCombinations(int combinationSize, int sourceSetSize, int currIndex, int[] currCombination) {
    if (currCombination.length == combinationSize) {return new int[][] {currCombination};}
    int[][] returnArray = new int[0][];
    for (int index = currIndex; index < sourceSetSize; index++) {
      int[] nextCombination = ArrayUtils.appendElement(currCombination, index);
      int[][] nextCombinations = getCombinations(combinationSize, sourceSetSize, index + 1, nextCombination);
      returnArray = ArrayUtils.appendArray(returnArray, nextCombinations);
    }
    return returnArray;
  }

  
}
