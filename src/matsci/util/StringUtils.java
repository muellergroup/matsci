/*
 * Created on Mar 7, 2006
 *
 */
package matsci.util;

public class StringUtils {

  public StringUtils() {
    super();
  }
  
  public static String rightAlign(int value, int finalLength) {
    /*String string = "                           " + value;
    return string.substring(string.length() - finalLength);*/
    return rightAlign("" + value, finalLength);
  }
  
  public static String rightAlign(String value, int finalLength) {
    return padLeft(value, finalLength);
    /*String string = "                           " + value;
    return string.substring(string.length() - finalLength);*/
  }
  
  // From http://stackoverflow.com/questions/388461/padding-strings-in-java
  public static String padRight(String s, int totalLength) {
    return String.format("%1$-" + totalLength + "s", s);  
  }
  
  // From http://stackoverflow.com/questions/388461/padding-strings-in-java
  // Removed # because it's unnecessary and threw an exception in Java 1.7
  public static String padLeft(String s, int totalLength) {
     return String.format("%1$" + totalLength + "s", s);  
  }


}
