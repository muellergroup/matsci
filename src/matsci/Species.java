package matsci;

import matsci.Element.ShannonRadiusRecord;
import matsci.util.arrays.ArrayUtils;
/**
 * <p>Title: </p>
 * <p>Description: Description: This class is a factory for species.  It stores an array of known species
 * and if a requested species matches a known one, it returns the known one instead of creating
 * a new object.  A species is considered to be an element + an isotope number</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class Species {

  private final Element m_Element;
  private final int m_Isotope;
  private final double m_Oxidation;
  
  private static double OXIDATION_TOLERANCE = 0.001;

  private Species(String elementSymbol, int isotope, double oxidation) {
    m_Element = Element.getElement(elementSymbol);
    m_Isotope = isotope;
    m_Oxidation = oxidation;
  }

  public static Species get(String specieSymbol) {
    
    if (specieSymbol == null) {
      return null;
    }
    
    int isotopeEnd = 0;
    for (isotopeEnd = 0; isotopeEnd < specieSymbol.length(); isotopeEnd++) {
      if (Character.isLetter(specieSymbol.charAt(isotopeEnd))) {
        break;
      }
    }
    int isotope = (isotopeEnd > 0) ? Integer.parseInt(specieSymbol.substring(0, isotopeEnd)) : -1;
    
    int symbolEnd = isotopeEnd;
    for (symbolEnd = isotopeEnd; symbolEnd < specieSymbol.length(); symbolEnd++) {
      if (!Character.isLetter(specieSymbol.charAt(symbolEnd))) {
        break;
      }
    }
    String elementSymbol = specieSymbol.substring(isotopeEnd, symbolEnd);

    double oxidationState = 0;
    
    if (symbolEnd < specieSymbol.length()) {

	    String endString = specieSymbol.substring(specieSymbol.length() - 1);
	    double sign = (endString.equals("+") ? 1 : (endString.equals("-") ? -1 : 0));
	    
    	if (sign != 0) {
	      String stateString = specieSymbol.substring(symbolEnd, specieSymbol.length() - 1);
	      double magnitude = stateString.length() == 0 ? 1 : Double.parseDouble(stateString);
	      oxidationState = sign * magnitude;
	    }
    }
    
    /*String endString = specieSymbol.substring(specieSymbol.length() - 1);
    double sign = (endString.equals("+") ? 1 : (endString.equals("-") ? -1 : 0));
    int oxidationStart = specieSymbol.length();
    double oxidationState = 0;
    
    if (sign != 0) {
	    oxidationStart = specieSymbol.length() - 2;
	    while (Character.isDigit((specieSymbol.charAt(oxidationStart)))) {
	      oxidationStart--;
	    }
	    oxidationStart++; // It starts one after the last letter
	    if (oxidationStart < specieSymbol.length() - 1) {
	      String stateString = specieSymbol.substring(oxidationStart, specieSymbol.length() - 1);
	      oxidationState = sign * Double.parseDouble(stateString);
	    }
    }
    String elementSymbol = specieSymbol.substring(isotopeEnd, oxidationStart);    
    */
    
    /*String endString = specieSymbol.substring(specieSymbol.length() - 1);
    double oxidationState = endString.equals("+") ? 1 : (endString.equals("-") ? -1 : 0);
    int oxidationStart = specieSymbol.length() - 1;
    while (Character.isDigit((specieSymbol.charAt(oxidationStart)))) {
      oxidationStart--;
    }
    oxidationStart++; // It starts one after the last letter
    if (oxidationStart < specieSymbol.length()) {
      int offSet = oxidationState == 0 ? 0 : 1;
      String stateString = specieSymbol.substring(oxidationStart, specieSymbol.length() - offSet);
      oxidationState *= Double.parseDouble(stateString);
    }*/
    
    return get(elementSymbol, isotope, oxidationState);
  }
  
  public static Species get(int atomicNumber) { // This can be made more efficient
    String elementSymbol = Element.getElement(atomicNumber).getSymbol();
    return get(elementSymbol, -1, 0);
  }
  
  public static Species[] get(String[] names) {
    Species[] returnArray = new Species[names.length];
    for (int specNum = 0; specNum < names.length; specNum++) {
      returnArray[specNum] = get(names[specNum]);
    }
    return returnArray;
  }
  
  protected static boolean oxidationCloseEnough(double oxidation1, double oxidation2) {
    double diff = Math.abs(oxidation1 - oxidation2);
    return diff < OXIDATION_TOLERANCE;
  }
  
  public static Species get(Element element) {
    return get(element.getSymbol());
  }

  public static Species get(String elementSymbol, int isotope, double oxidation) {
    
    // Check to see if its from a standard element
    Element element = Element.getElement(elementSymbol);
    if (element != null) {
      int z = element.getAtomicNumber();
      if (z >= 0) {
        Species[] knownSpecies = SPECIE_BY_ATOMIC_NUMBER[z];
        for (int i = 0; i < knownSpecies.length; i++) {
          Species spec = knownSpecies[i];
          if (spec.getIsotope() == isotope && oxidationCloseEnough(spec.getOxidationState(), oxidation)) {
            return spec;
          }
        }
        Species newSpecie = new Species(elementSymbol, isotope, oxidation);
        SPECIE_BY_ATOMIC_NUMBER[z] = (Species[]) ArrayUtils.appendElement(knownSpecies, newSpecie);
        return newSpecie;
      }
    }
    
    // Check to see if it's from a weird element that we've already seen
    for (int z = Element.numKnownElements(); z < SPECIE_BY_ATOMIC_NUMBER.length; z++) {
      Species[] knownSpecies = SPECIE_BY_ATOMIC_NUMBER[z];
      if (!knownSpecies[0].getElementSymbol().equals(elementSymbol)) {continue;}
      for (int i = 0; i < knownSpecies.length; i++) {
        Species spec = knownSpecies[i];
        if (spec.getIsotope() == isotope && oxidationCloseEnough(spec.getOxidationState(), oxidation)) {
          return spec;
        }
      }
      Species newSpecie = new Species(elementSymbol, isotope, oxidation);
      SPECIE_BY_ATOMIC_NUMBER[z] = (Species[]) ArrayUtils.appendElement(knownSpecies, newSpecie);
      return newSpecie;
    }
    
    // It's a brand new weird element!
    Species newSpecie = new Species(elementSymbol, isotope, oxidation);
    SPECIE_BY_ATOMIC_NUMBER = (Species[][]) ArrayUtils.appendElement(SPECIE_BY_ATOMIC_NUMBER, new Species[] {newSpecie});
    return newSpecie;
  }

  public String getName() {
    String returnString = m_Element.getName();
    if (m_Isotope > 0) {
      returnString += "-" + this.getIsotope();
    }
    return returnString + " " + this.getOxidationString();
  }
  
  public static Species getVacancy() {
    return vacancy;
  }

  public Element getElement() {
    return m_Element;
  }
  
  public String getSymbol() {
    String returnString = this.getElementSymbol();
    if (m_Isotope > 0) {
      returnString = this.getIsotope() + returnString;
    }
    return returnString + this.getOxidationString();
  }
  
  protected String getOxidationString() {
    if (m_Oxidation == 0) {return "";}
    double absValue = Math.abs(m_Oxidation);
    boolean isPositive = (m_Oxidation > 0);
    int intState = (int) Math.round(absValue);
    if (intState == absValue) {
      return isPositive ? intState + "+" : intState + "-";
    }
    return isPositive ? absValue + "+" : absValue + "-";
  }
  
  public String getElementSymbol() {
    return m_Element.getSymbol();
  }
  
  public Species setOxidationState(double oxidationState) {
    return Species.get(this.getElementSymbol(), this.getIsotope(), oxidationState);
  }
  
  public double getOxidationState() {
    return m_Oxidation;
  }
  
  public Element.ShannonRadiusRecord[] getShannonRadii() {
    return m_Element.getShannonRadii(m_Oxidation);
  }
  
  public Element.ShannonRadiusRecord getShannonRadius(int coordinationNumber) {
	  ShannonRadiusRecord[] records = m_Element.getShannonRadii(m_Oxidation);
	  if (records == null) {return null;}
	  for (ShannonRadiusRecord record : records) {
		  if (record.getCoordination() == coordinationNumber) {
			  return record;
		  }
	  }
	  return null;
  }
  
  public double getAverageIonicRadius() {
    Element.ShannonRadiusRecord[] radii = this.getShannonRadii();
    if (radii == null || radii.length == 0) {
      return Double.NaN;
    }
    double avg = 0;
    for (int recNum = 0; recNum < radii.length; recNum++) {
      avg += radii[recNum].getIonicRadius();
    }
    return avg / radii.length;
  }
  
  public double getAverageCrystalRadius() {
    Element.ShannonRadiusRecord[] radii = this.getShannonRadii();
    if (radii == null || radii.length == 0) {
      return Double.NaN;
    }
    double avg = 0;
    for (int recNum = 0; recNum < radii.length; recNum++) {
      avg += radii[recNum].getCrystalRadius();
    }
    return avg / radii.length;
  }
  
  public Element.ShannonRadiusRecord getMaxShannonRadius() {
    Element.ShannonRadiusRecord[] radii = this.getShannonRadii();
    Element.ShannonRadiusRecord returnRadius = null;
    double maxValue = Double.NEGATIVE_INFINITY;
    for (int radNum = 0; radNum < radii.length; radNum++) {
      double radius = radii[radNum].getCrystalRadius();
      if (radius > maxValue) {
        returnRadius = radii[radNum];
        maxValue = radius;
      }
    }
    return returnRadius;
  }
  
  public Element.ShannonRadiusRecord getMinShannonRadius() {
    Element.ShannonRadiusRecord[] radii = this.getShannonRadii();
    Element.ShannonRadiusRecord returnRadius = null;
    double minValue = Double.POSITIVE_INFINITY;
    for (int radNum = 0; radNum < radii.length; radNum++) {
      double radius = radii[radNum].getCrystalRadius();
      if (radius < minValue) {
        returnRadius = radii[radNum];
        minValue = radius;
      }
    }
    return returnRadius;
  }

  public int getIsotope() {
    return m_Isotope;
  }

  public String toString() {
    return this.getSymbol();
  }
  
  public static final Species vacancy = new Species("Vacancy", -1, 0);
  public static final Species hydrogen = new Species("H", -1, 0);
  public static final Species helium = new Species("He", -1, 0);
  public static final Species lithium = new Species("Li", -1, 0);
  public static final Species beryllium = new Species("Be", -1, 0);
  public static final Species boron = new Species("B", -1, 0);
  public static final Species carbon = new Species("C", -1, 0);
  public static final Species nitrogen = new Species("N", -1, 0);
  public static final Species oxygen = new Species("O", -1, 0);
  public static final Species fluorine = new Species("F", -1, 0);
  public static final Species neon = new Species("Ne", -1, 0);
  public static final Species sodium = new Species("Na", -1, 0);
  public static final Species magnesium = new Species("Mg", -1, 0);
  public static final Species aluminum = new Species("Al", -1, 0);
  public static final Species silicon = new Species("Si", -1, 0);
  public static final Species phosphorus = new Species("P", -1, 0);
  public static final Species sulfur = new Species("S", -1, 0);
  public static final Species chlorine = new Species("Cl", -1, 0);
  public static final Species argon = new Species("Ar", -1, 0);
  public static final Species potassium = new Species("K", -1, 0);
  public static final Species calcium = new Species("Ca", -1, 0);
  public static final Species scandium = new Species("Sc", -1, 0);
  public static final Species titanium = new Species("Ti", -1, 0);
  public static final Species vanadium = new Species("V", -1, 0);
  public static final Species chromium = new Species("Cr", -1, 0);
  public static final Species manganese = new Species("Mn", -1, 0);
  public static final Species iron = new Species("Fe", -1, 0);
  public static final Species nickel = new Species("Ni", -1, 0);
  public static final Species cobalt = new Species("Co", -1, 0);
  public static final Species copper = new Species("Cu", -1, 0);
  public static final Species zinc = new Species("Zn", -1, 0);
  public static final Species gallium = new Species("Ga", -1, 0);
  public static final Species germanium = new Species("Ge", -1, 0);
  public static final Species arsenic = new Species("As", -1, 0);
  public static final Species selenium = new Species("Se", -1, 0);
  public static final Species bromine = new Species("Br", -1, 0);
  public static final Species krypton = new Species("Kr", -1, 0);
  public static final Species rubidium = new Species("Rb", -1, 0);
  public static final Species strontium = new Species("Sr", -1, 0);
  public static final Species yttrium = new Species("Y", -1, 0);
  public static final Species zirconium = new Species("Zr", -1, 0);
  public static final Species niobium = new Species("Nb", -1, 0);
  public static final Species molybdenum = new Species("Mo", -1, 0);
  public static final Species technetium = new Species("Tc", -1, 0);
  public static final Species ruthenium = new Species("Ru", -1, 0);
  public static final Species rhodium = new Species("Rh", -1, 0);
  public static final Species palladium = new Species("Pd", -1, 0);
  public static final Species silver = new Species("Ag", -1, 0);
  public static final Species cadmium = new Species("Cd", -1, 0);
  public static final Species indium = new Species("In", -1, 0);
  public static final Species tin = new Species("Sn", -1, 0);
  public static final Species antimony = new Species("Sb", -1, 0);
  public static final Species tellurium = new Species("Te", -1, 0);
  public static final Species iodine = new Species("I", -1, 0);
  public static final Species xenon = new Species("Xe", -1, 0);
  public static final Species cesium  = new Species("Cs", -1, 0);
  public static final Species barium = new Species("Ba", -1, 0);
  public static final Species lanthanum = new Species("La", -1, 0);
  public static final Species cerium = new Species("Ce", -1, 0);
  public static final Species praseodymium = new Species("Pr", -1, 0);
  public static final Species neodymium = new Species("Nd", -1, 0);
  public static final Species promethium = new Species("Pm", -1, 0);
  public static final Species samarium = new Species("Sm", -1, 0);
  public static final Species europium = new Species("Eu", -1, 0);
  public static final Species gadolinium = new Species("Gd", -1, 0);
  public static final Species terbium = new Species("Tb", -1, 0);
  public static final Species dysprosium = new Species("Dy", -1, 0);
  public static final Species holmium = new Species("Ho", -1, 0);
  public static final Species erbium = new Species("Er", -1, 0);
  public static final Species thulium = new Species("Tm", -1, 0);
  public static final Species ytterbium = new Species("Yb", -1, 0);
  public static final Species lutetium = new Species("Lu", -1, 0);
  public static final Species hafnium = new Species("Hf", -1, 0);
  public static final Species tantalum = new Species("Ta", -1, 0);
  public static final Species tungsten  = new Species("W", -1, 0);
  public static final Species rhenium = new Species("Re", -1, 0);
  public static final Species osmium = new Species("Os", -1, 0);
  public static final Species iridium = new Species("Ir", -1, 0);
  public static final Species platinum = new Species("Pt", -1, 0);
  public static final Species gold = new Species("Au", -1, 0);
  public static final Species mercury = new Species("Hg", -1, 0);
  public static final Species thallium = new Species("Tl", -1, 0);
  public static final Species lead = new Species("Pb", -1, 0);
  public static final Species bismuth = new Species("Bi", -1, 0);
  public static final Species polonium = new Species("Po", -1, 0);
  public static final Species astatine = new Species("At", -1, 0);
  public static final Species radon = new Species("Rn", -1, 0);
  public static final Species francium = new Species("Fr", -1, 0);
  public static final Species radium = new Species("Ra", -1, 0);
  public static final Species actinium = new Species("Ac", -1, 0);
  public static final Species protactinium = new Species("Pa", -1, 0);
  public static final Species thorium = new Species("Th", -1, 0);
  public static final Species neptunium = new Species("Np", -1, 0);
  public static final Species uranium = new Species("U", -1, 0);
  public static final Species americium = new Species("Am", -1, 0);
  public static final Species plutonium = new Species("Pu", -1, 0);
  public static final Species curium = new Species("Cm", -1, 0);
  public static final Species berkelium = new Species("Bk", -1, 0);
  public static final Species californium = new Species("Cf", -1, 0);
  public static final Species einsteinium = new Species("Es", -1, 0);
  public static final Species fermium = new Species("Fm", -1, 0);
  public static final Species mendelevium = new Species("Md", -1, 0);
  public static final Species nobelium = new Species("No", -1, 0);
  public static final Species lawrencium = new Species("Lr", -1, 0);
  public static final Species rutherfordium = new Species("Rf", -1, 0);
  public static final Species dubnium = new Species("Db", -1, 0);
  public static final Species seaborgium = new Species("Sg", -1, 0);
  public static final Species bohrium = new Species("Bh", -1, 0);
  public static final Species hassium = new Species("Hs", -1, 0);
  public static final Species meitnerium = new Species("Mt", -1, 0);
  public static final Species darmstadtium = new Species("Ds", -1, 0);
  public static final Species roentgenium = new Species("Rg", -1, 0);
  /*public static final Species ununbium = new Species("Uub", -1, 0);
  public static final Species ununtrium = new Species("Uut", -1, 0);
  public static final Species ununquadium = new Species("Uuq", -1, 0);
  public static final Species ununpentium = new Species("Uup", -1, 0);
  public static final Species ununhexium = new Species("Uuh", -1, 0);
  public static final Species ununseptium = new Species("Uus", -1, 0);
  public static final Species ununoctium = new Species("Uuo", -1, 0);*/
  public static final Species copernicium = new Species("Cn", -1, 0);
  public static final Species nihonium = new Species("Nh", -1, 0);
  public static final Species flerovium = new Species("Fl", -1, 0);
  public static final Species moscovium = new Species("Mc", -1, 0);
  public static final Species livermorium = new Species("Lv", -1, 0);
  public static final Species tennessine = new Species("Ts", -1, 0);
  public static final Species oganesson = new Species("Og", -1, 0);
  
  private static Species[][] SPECIE_BY_ATOMIC_NUMBER = new Species[][] {
    new Species[] {Species.vacancy},
    new Species[] {Species.hydrogen},
    new Species[] {Species.helium},
    new Species[] {Species.lithium},
    new Species[] {Species.beryllium},
    new Species[] {Species.boron},
    new Species[] {Species.carbon},
    new Species[] {Species.nitrogen},
    new Species[] {Species.oxygen},
    new Species[] {Species.fluorine},
    new Species[] {Species.neon},
    new Species[] {Species.sodium},
    new Species[] {Species.magnesium},
    new Species[] {Species.aluminum},
    new Species[] {Species.silicon},
    new Species[] {Species.phosphorus},
    new Species[] {Species.sulfur},
    new Species[] {Species.chlorine},
    new Species[] {Species.argon},
    new Species[] {Species.potassium},
    new Species[] {Species.calcium},
    new Species[] {Species.scandium},
    new Species[] {Species.titanium},
    new Species[] {Species.vanadium},
    new Species[] {Species.chromium},
    new Species[] {Species.manganese},
    new Species[] {Species.iron},
    new Species[] {Species.cobalt},
    new Species[] {Species.nickel},
    new Species[] {Species.copper},
    new Species[] {Species.zinc},
    new Species[] {Species.gallium},
    new Species[] {Species.germanium},
    new Species[] {Species.arsenic},
    new Species[] {Species.selenium},
    new Species[] {Species.bromine},
    new Species[] {Species.krypton},
    new Species[] {Species.rubidium},
    new Species[] {Species.strontium},
    new Species[] {Species.yttrium},
    new Species[] {Species.zirconium},
    new Species[] {Species.niobium},
    new Species[] {Species.molybdenum},
    new Species[] {Species.technetium},
    new Species[] {Species.ruthenium},
    new Species[] {Species.rhodium},
    new Species[] {Species.palladium},
    new Species[] {Species.silver},
    new Species[] {Species.cadmium},
    new Species[] {Species.indium},
    new Species[] {Species.tin},
    new Species[] {Species.antimony},
    new Species[] {Species.tellurium},
    new Species[] {Species.iodine},
    new Species[] {Species.xenon},
    new Species[] {Species.cesium },
    new Species[] {Species.barium},
    new Species[] {Species.lanthanum},
    new Species[] {Species.cerium},
    new Species[] {Species.praseodymium},
    new Species[] {Species.neodymium},
    new Species[] {Species.promethium},
    new Species[] {Species.samarium},
    new Species[] {Species.europium},
    new Species[] {Species.gadolinium},
    new Species[] {Species.terbium},
    new Species[] {Species.dysprosium},
    new Species[] {Species.holmium},
    new Species[] {Species.erbium},
    new Species[] {Species.thulium},
    new Species[] {Species.ytterbium},
    new Species[] {Species.lutetium},
    new Species[] {Species.hafnium},
    new Species[] {Species.tantalum},
    new Species[] {Species.tungsten },
    new Species[] {Species.rhenium},
    new Species[] {Species.osmium},
    new Species[] {Species.iridium},
    new Species[] {Species.platinum},
    new Species[] {Species.gold},
    new Species[] {Species.mercury},
    new Species[] {Species.thallium},
    new Species[] {Species.lead},
    new Species[] {Species.bismuth},
    new Species[] {Species.polonium},
    new Species[] {Species.astatine},
    new Species[] {Species.radon},
    new Species[] {Species.francium},
    new Species[] {Species.radium},
    new Species[] {Species.actinium},
    new Species[] {Species.thorium},
    new Species[] {Species.protactinium},
    new Species[] {Species.uranium},
    new Species[] {Species.neptunium},
    new Species[] {Species.plutonium},
    new Species[] {Species.americium},
    new Species[] {Species.curium},
    new Species[] {Species.berkelium},
    new Species[] {Species.californium},
    new Species[] {Species.einsteinium},
    new Species[] {Species.fermium},
    new Species[] {Species.mendelevium},
    new Species[] {Species.nobelium},
    new Species[] {Species.lawrencium},
    new Species[] {Species.rutherfordium},
    new Species[] {Species.dubnium},
    new Species[] {Species.seaborgium},
    new Species[] {Species.bohrium},
    new Species[] {Species.hassium},
    new Species[] {Species.meitnerium},
    new Species[] {Species.darmstadtium},
    new Species[] {Species.roentgenium},
    new Species[] {Species.copernicium},
    new Species[] {Species.nihonium},
    new Species[] {Species.flerovium},
    new Species[] {Species.moscovium},
    new Species[] {Species.livermorium},
    new Species[] {Species.tennessine},
    new Species[] {Species.oganesson},
  };

}