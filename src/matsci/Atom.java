package matsci;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class Atom {

  private final Species m_Specie;

  public Atom(Species mySpecies) {
    m_Specie = mySpecies;
  }

  public Species getSpecies() {
    return m_Specie;
  }
  
  public String toString() {
    return m_Specie.toString();
  }
}