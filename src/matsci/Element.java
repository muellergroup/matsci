package matsci;

import java.util.HashMap;

import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.arrays.ArrayUtils;
/**
 * <p>Title: </p>
 * <p>Description: This class is a factory for elements.  It stores an array of known elements
 * and if a requested element matches a known one, it returns the known one instead of creating
 * a new object. </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class Element {

  private final String m_Symbol;
  private final String m_Name;
  private final int m_AtomicNumber;
  private final double m_AtomicWeight;
  private final int m_Group;
  private final int m_Period;
  
  private double m_PaulingElectronegativity_CRC = Double.NaN;
  private double m_ARElectronegativity = Double.NaN;
  private ShannonRadiusRecord[] m_ShannonRadii;
  private double m_MeltingPointK = Double.NaN;
  private double m_BoilingPointK = Double.NaN;
  
  private double m_PricePerMol = Double.NaN;
  
  private double m_VanDerWaalsRadius = Double.NaN;
  private double m_CovalentRadius = Double.NaN;
  
  private Structure m_AtomStructure = null;

  private Element(String symbol, String name, int atomicNumber, double atomicWeight, int group, int period) {
    m_Symbol = symbol;
    m_Name = name;
    m_AtomicNumber = atomicNumber;
    m_AtomicWeight = atomicWeight;
    m_Group = group;
    m_Period = period;
  }
  
  public static Element getElement(int atomicNumber) {
    return ELEMENTS_BY_ATOMIC_NUMBER[atomicNumber];
  }
  
  public static int numKnownElements() {
    return ELEMENTS_BY_ATOMIC_NUMBER.length - 1;
  }
  
  public static boolean isKnownElement(String symbol) {
	  Element element = ELEMENTS_BY_SYMBOL.get(symbol);
	  if (element == null) {return false;}
	  return element.getAtomicNumber() > 0; // Check for vacancies or "special" elements.
  }
  
  public static boolean isKnownElementOrVacancy(String symbol) {
	  Element element = ELEMENTS_BY_SYMBOL.get(symbol);
	  return (element == Element.vacancy || (element.getAtomicNumber() > 0));
  }

  public static Element getElement(String symbol) {
    
    Element returnElement = (Element) ELEMENTS_BY_SYMBOL.get(symbol);
    if (returnElement == null) {
      returnElement = new Element(symbol, symbol, -1, -1, -1, -1);
      ELEMENTS_BY_SYMBOL.put(symbol, returnElement);
    }
    
    /*for (int i = 0; i < ELEMENTS_BY_ATOMIC_NUMBER.length; i++) {
      if (ELEMENTS_BY_ATOMIC_NUMBER[i].getSymbol().equals(symbol)) {
        return ELEMENTS_BY_ATOMIC_NUMBER[i];
      }
    }    
    Element newElement = new Element(symbol, symbol, -1, -1, -1, -1);
    ELEMENTS_BY_ATOMIC_NUMBER = (Element[]) ArrayUtils.appendElement(ELEMENTS_BY_ATOMIC_NUMBER, newElement);*/

    return returnElement;
  }

  public String getSymbol() {
    return m_Symbol;
  }

  public String toString() {
    return this.getSymbol();
  }
  
  public int getAtomicNumber() {
    return m_AtomicNumber;
  }
  
  public double getAtomicWeight() {
    return m_AtomicWeight;
  }
  
  public Structure getAtomStructure() {
	  
	  if (m_AtomStructure == null) {

		  StructureBuilder builder = new StructureBuilder();
		  builder.setDescription(this.getSymbol());
		  builder.setCellVectors(LinearBasis.fill3DBasis(new Vector[0]));
		  builder.setVectorPeriodicity(new boolean[] {false, false, false});
		  builder.addSite(CartesianBasis.getInstance().getOrigin(), Species.get(this));
		  
		  m_AtomStructure = new Structure(builder);
	  }
	  return m_AtomStructure;
		
  }
  
  public String getName() {
    return m_Name;
  }
  
  public int getGroup() {
    return m_Group;
  }
  
  public int getPeriod() {
    return m_Period;
  }
  
  public double getElectronegativityPaulingCRC() {
    return m_PaulingElectronegativity_CRC;
  }
  
  public double getElectronegativityAllredRochow() {
	  return m_ARElectronegativity;
  }
  
  public double getMeltingPointK() {
    return m_MeltingPointK;
  }
  
  public double getBoilingPointK() {
    return m_BoilingPointK;
  }
  
  public double getVanDerWaalsRadius() {
    return m_VanDerWaalsRadius;
  }
  
  public double getCovalentRadius() {
	  return m_CovalentRadius;
  }
  
  public int numShannonRadii() {
    return (m_ShannonRadii == null) ? -1 : m_ShannonRadii.length;
  }
  
  public ShannonRadiusRecord getShannonRadius(int radiusNum) {
    return (m_ShannonRadii == null) ? null : m_ShannonRadii[radiusNum];
  }
  
  public ShannonRadiusRecord[] getShannonRadii(double oxidationState) {
    
    ShannonRadiusRecord[] returnArray = new ShannonRadiusRecord[0];
    if (m_ShannonRadii == null) {return null;}
    for (int radiusNum = 0; radiusNum < m_ShannonRadii.length; radiusNum++) {
      ShannonRadiusRecord shannonRadius = m_ShannonRadii[radiusNum];
      if (shannonRadius.getOxidationState() == oxidationState) {
        returnArray = (ShannonRadiusRecord[]) ArrayUtils.appendElement(returnArray, shannonRadius);
      }
    }
    return returnArray;
  }
  
  public ShannonRadiusRecord[] getShannonRadii(String coordination) {
    
    ShannonRadiusRecord[] returnArray = new ShannonRadiusRecord[0];
    for (int radiusNum = 0; radiusNum < m_ShannonRadii.length; radiusNum++) {
      ShannonRadiusRecord shannonRadius = m_ShannonRadii[radiusNum];
      if (shannonRadius.getCoordinationString().equals(coordination)) {
        returnArray = (ShannonRadiusRecord[]) ArrayUtils.appendElement(returnArray, shannonRadius);
      }
    }
    return returnArray;
  }
  
  public ShannonRadiusRecord[] getCationShannonRadii() {
    
    ShannonRadiusRecord[] returnArray = new ShannonRadiusRecord[0];
    for (int radiusNum = 0; radiusNum < m_ShannonRadii.length; radiusNum++) {
      ShannonRadiusRecord shannonRadius = m_ShannonRadii[radiusNum];
      if (shannonRadius.getOxidationState() > 0) {
        returnArray = (ShannonRadiusRecord[]) ArrayUtils.appendElement(returnArray, shannonRadius);
      }
    }
    return returnArray;
  }
  
  public ShannonRadiusRecord[] getCationShannonRadii(String coordination) {
    
    ShannonRadiusRecord[] returnArray = new ShannonRadiusRecord[0];
    for (int radiusNum = 0; radiusNum < m_ShannonRadii.length; radiusNum++) {
      ShannonRadiusRecord shannonRadius = m_ShannonRadii[radiusNum];
      if (shannonRadius.getOxidationState() <= 0) {continue;}
      if (shannonRadius.getCoordinationString().equals(coordination)) {
        returnArray = (ShannonRadiusRecord[]) ArrayUtils.appendElement(returnArray, shannonRadius);
      }
    }
    return returnArray;
  }
  
  public ShannonRadiusRecord[] getShannonRadii() {
    return (ShannonRadiusRecord[]) ArrayUtils.copyArray(m_ShannonRadii);
  }
  
  public ShannonRadiusRecord[] getAnionShannonRadii() {
    
    ShannonRadiusRecord[] returnArray = new ShannonRadiusRecord[0];
    for (int radiusNum = 0; radiusNum < m_ShannonRadii.length; radiusNum++) {
      ShannonRadiusRecord shannonRadius = m_ShannonRadii[radiusNum];
      if (shannonRadius.getOxidationState() < 0) {
        returnArray = (ShannonRadiusRecord[]) ArrayUtils.appendElement(returnArray, shannonRadius);
      }
    }
    return returnArray;
  }
  
  public ShannonRadiusRecord[] getAnionShannonRadii(String coordination) {
    
    ShannonRadiusRecord[] returnArray = new ShannonRadiusRecord[0];
    for (int radiusNum = 0; radiusNum < m_ShannonRadii.length; radiusNum++) {
      ShannonRadiusRecord shannonRadius = m_ShannonRadii[radiusNum];
      if (shannonRadius.getOxidationState() >= 0) {continue;}
      if (shannonRadius.getCoordinationString().equals(coordination)) {
        returnArray = (ShannonRadiusRecord[]) ArrayUtils.appendElement(returnArray, shannonRadius);
      }
    }
    return returnArray;
  }
  
  public ShannonRadiusRecord getShannonRadiusRecord(double oxidationState, String coordination, int spinState) {
    
    for (int radiusNum = 0; radiusNum < m_ShannonRadii.length; radiusNum++) {
      ShannonRadiusRecord shannonRadius = m_ShannonRadii[radiusNum];
      if (shannonRadius.getOxidationState() != oxidationState) {continue;}
      if (!shannonRadius.getCoordinationString().equals(coordination)) {continue;}
      if (shannonRadius.getSpinState() != spinState) {continue;}
      return shannonRadius;
    }
    
    return null;
    
  }
  
  public double getPricePerMol() {
    return m_PricePerMol;
  }
  
  public void setPricePerMol(double value) {
    m_PricePerMol = value;
  }
  
  public class ShannonRadiusRecord {
    
    private final String m_CoordinationString;
    private final double m_OxidationState;
    private final int m_SpinState; // 0 means N/A, -1 low spin, +1 high spin
    private final double m_CrystalRadius;
    private final double m_IonicRadius;
    
    private ShannonRadiusRecord(String coordination, double oxidationState, int spinState, double crystalRadius, double ionicRadius) {
      m_CoordinationString = coordination;
      m_OxidationState = oxidationState;
      m_SpinState = spinState;
      m_CrystalRadius = crystalRadius;
      m_IonicRadius = ionicRadius;
    }
    
    public Element getElement() {
      return Element.this;
    }
    
    public String getCoordinationString() {
      return m_CoordinationString;
    }
    
    public int getCoordination() {
      
      if (m_CoordinationString.contains("IV")) {
        return 4;
      } else if (m_CoordinationString.contains("VIII")) {
        return 8;
      } else if (m_CoordinationString.contains("VII")) {
        return 7;
      } else if (m_CoordinationString.contains("VI")) {
        return 6;
      } else if (m_CoordinationString.contains("V")) {
        return 5;
      } else if (m_CoordinationString.contains("XII")) {
        return 12;
      } else if (m_CoordinationString.contains("XI")) {
        return 11;
      } else if (m_CoordinationString.contains("IX")) {
        return 9;
      } else if (m_CoordinationString.contains("X")) {
        return 10;
      } else if (m_CoordinationString.contains("III")) {
        return 3;
      } else if (m_CoordinationString.contains("II")) {
        return 2;
      } else if (m_CoordinationString.contains("I")) {
        return 1;
      }
      
      return -1;
      
    }
    
    public double getOxidationState() {
      return m_OxidationState;
    }
    
    public double getCrystalRadius() {
      return m_CrystalRadius;
    }
    
    public double getIonicRadius() {
      return m_IonicRadius;
    }
    
    public int getSpinState() {
      return m_SpinState;
    }
    
    public boolean isHighSpin() {
      return (m_SpinState == 1);
    }
    
    public boolean isLowSpin() {
      return (m_SpinState == -1);
    }
    
  }
  
  /**
   * From Wikipedia, June 2 2008.  Cited references are:
   *  Atomic Weights of the Elements 2005, Pure Appl. Chem. 78(11), 2051-2066, 2006. Retrieved May 7, 2008. Atomic weights of elements with atomic numbers 1-27, 29, 31-41, 43-69, 72-109 taken from this source.
    * IUPAC Standard Atomic Weights Revised (2007). Atomic weights of elements with atomic numbers of 28, 30, 42, 70 & 71 taken from this source.
    * WebElements Periodic Table. Retrieved June 30, 2005. Atomic weights of elements with atomic numbers of 110-116 taken from this source.
   */
  public static final Element vacancy = new Element("Vacancy", "vacancy", 0, 0, 0, 0);
  public static final Element hydrogen = new Element("H", "hydrogen", 1,1.00794, 1, 1);
  public static final Element helium = new Element("He", "helium", 2,4.002602, 18, 1);
  public static final Element lithium = new Element("Li", "lithium", 3,6.941, 1, 2);
  public static final Element beryllium = new Element("Be", "beryllium", 4,9.012182, 2, 2);
  public static final Element boron = new Element("B", "boron", 5,10.811, 13, 2);
  public static final Element carbon = new Element("C", "carbon", 6,12.0107, 14, 2);
  public static final Element nitrogen = new Element("N", "nitrogen", 7,14.0067, 15, 2);
  public static final Element oxygen = new Element("O", "oxygen", 8,15.9994, 16, 2);
  public static final Element fluorine = new Element("F", "fluorine", 9,18.9984032, 17, 2);
  public static final Element neon = new Element("Ne", "neon", 10,20.1797, 18, 2);
  public static final Element sodium = new Element("Na", "sodium", 11,22.98976928, 1, 3);
  public static final Element magnesium = new Element("Mg", "magnesium", 12,24.305, 2, 3);
  public static final Element aluminum = new Element("Al", "aluminum", 13,26.9815386, 13, 3);
  public static final Element silicon = new Element("Si", "silicon", 14,28.0855, 14, 3);
  public static final Element phosphorus = new Element("P", "phosphorus", 15,30.973762, 15, 3);
  public static final Element sulfur = new Element("S", "sulfur", 16,32.065, 16, 3);
  public static final Element chlorine = new Element("Cl", "chlorine", 17,35.453, 17, 3);
  public static final Element argon = new Element("Ar", "argon", 18,39.948, 18, 3);
  public static final Element potassium = new Element("K", "potassium", 19,39.0983, 1, 4);
  public static final Element calcium = new Element("Ca", "calcium", 20,40.078, 2, 4);
  public static final Element scandium = new Element("Sc", "scandium", 21,44.955912, 3, 4);
  public static final Element titanium = new Element("Ti", "titanium", 22,47.867, 4, 4);
  public static final Element vanadium = new Element("V", "vanadium", 23,50.9415, 5, 4);
  public static final Element chromium = new Element("Cr", "chromium", 24,51.9961, 6, 4);
  public static final Element manganese = new Element("Mn", "manganese", 25,54.938045, 7, 4);
  public static final Element iron = new Element("Fe", "iron", 26,55.845, 8, 4);
  public static final Element cobalt = new Element("Co", "cobalt", 27,58.933195, 9, 4);
  public static final Element nickel = new Element("Ni", "nickel", 28,58.6934, 10, 4);
  public static final Element copper = new Element("Cu", "copper", 29,63.546, 11, 4);
  public static final Element zinc = new Element("Zn", "zinc", 30,65.38, 12, 4);
  public static final Element gallium = new Element("Ga", "gallium", 31,69.723, 13, 4);
  public static final Element germanium = new Element("Ge", "germanium", 32,72.64, 14, 4);
  public static final Element arsenic = new Element("As", "arsenic", 33,74.9216, 15, 4);
  public static final Element selenium = new Element("Se", "selenium", 34,78.96, 16, 4);
  public static final Element bromine = new Element("Br", "bromine", 35,79.904, 17, 4);
  public static final Element krypton = new Element("Kr", "krypton", 36,83.798, 18, 4);
  public static final Element rubidium = new Element("Rb", "rubidium", 37,85.4678, 1, 5);
  public static final Element strontium = new Element("Sr", "strontium", 38,87.62, 2, 5);
  public static final Element yttrium = new Element("Y", "yttrium", 39,88.90585, 3, 5);
  public static final Element zirconium = new Element("Zr", "zirconium", 40,91.224, 4, 5);
  public static final Element niobium = new Element("Nb", "niobium", 41,92.90638, 5, 5);
  public static final Element molybdenum = new Element("Mo", "molybdenum", 42,95.96, 6, 5);
  public static final Element technetium = new Element("Tc", "technetium", 43,98, 7, 5);
  public static final Element ruthenium = new Element("Ru", "ruthenium", 44,101.07, 8, 5);
  public static final Element rhodium = new Element("Rh", "rhodium", 45,102.90550, 9, 5);
  public static final Element palladium = new Element("Pd", "palladium", 46,106.42, 10, 5);
  public static final Element silver = new Element("Ag", "silver", 47,107.8682, 11, 5);
  public static final Element cadmium = new Element("Cd", "cadmium", 48,112.411, 12, 5);
  public static final Element indium = new Element("In", "indium", 49,114.818, 13, 5);
  public static final Element tin = new Element("Sn", "tin", 50,118.71, 14, 5);
  public static final Element antimony = new Element("Sb", "antimony", 51,121.76, 15, 5);
  public static final Element tellurium = new Element("Te", "tellurium", 52,127.6, 16, 5);
  public static final Element iodine = new Element("I", "iodine", 53,126.90447, 17, 5);
  public static final Element xenon = new Element("Xe", "xenon", 54,131.293, 18, 5);
  public static final Element cesium  = new Element("Cs", "cesium ", 55,132.9054519, 1, 6);
  public static final Element barium = new Element("Ba", "barium", 56,137.327, 2, 6);
  public static final Element lanthanum = new Element("La", "lanthanum", 57,138.90547, 3, 6);
  public static final Element cerium = new Element("Ce", "cerium", 58,140.116, 3, 6);
  public static final Element praseodymium = new Element("Pr", "praseodymium", 59,140.90765, 3, 6);
  public static final Element neodymium = new Element("Nd", "neodymium", 60,144.242, 3, 6);
  public static final Element promethium = new Element("Pm", "promethium", 61,145, 3, 6);
  public static final Element samarium = new Element("Sm", "samarium", 62,150.36, 3, 6);
  public static final Element europium = new Element("Eu", "europium", 63,151.964, 3, 6);
  public static final Element gadolinium = new Element("Gd", "gadolinium", 64,157.25, 3, 6);
  public static final Element terbium = new Element("Tb", "terbium", 65,158.92535, 3, 6);
  public static final Element dysprosium = new Element("Dy", "dysprosium", 66,162.5, 3, 6);
  public static final Element holmium = new Element("Ho", "holmium", 67,164.93032, 3, 6);
  public static final Element erbium = new Element("Er", "erbium", 68,167.259, 3, 6);
  public static final Element thulium = new Element("Tm", "thulium", 69,168.93421, 3, 6);
  public static final Element ytterbium = new Element("Yb", "ytterbium", 70,173.054, 3, 6);
  public static final Element lutetium = new Element("Lu", "lutetium", 71,174.9668, 3, 6);
  public static final Element hafnium = new Element("Hf", "hafnium", 72,178.49, 4, 6);
  public static final Element tantalum = new Element("Ta", "tantalum", 73,180.94788, 5, 6);
  public static final Element tungsten = new Element("W", "tungsten", 74,183.84, 6, 6);
  public static final Element rhenium = new Element("Re", "rhenium", 75,186.207, 7, 6);
  public static final Element osmium = new Element("Os", "osmium", 76,190.23, 8, 6);
  public static final Element iridium = new Element("Ir", "iridium", 77,192.217, 9, 6);
  public static final Element platinum = new Element("Pt", "platinum", 78,195.084, 10, 6);
  public static final Element gold = new Element("Au", "gold", 79,196.966569, 11, 6);
  public static final Element mercury = new Element("Hg", "mercury", 80,200.59, 12, 6);
  public static final Element thallium = new Element("Tl", "thallium", 81,204.3833, 13, 6);
  public static final Element lead = new Element("Pb", "lead", 82,207.2, 14, 6);
  public static final Element bismuth = new Element("Bi", "bismuth", 83,208.9804, 15, 6);
  public static final Element polonium = new Element("Po", "polonium", 84,210, 16, 6);
  public static final Element astatine = new Element("At", "astatine", 85,210, 17, 6);
  public static final Element radon = new Element("Rn", "radon", 86,220, 18, 6);
  public static final Element francium = new Element("Fr", "francium", 87,223, 1, 7);
  public static final Element radium = new Element("Ra", "radium", 88,226, 2, 7);
  public static final Element actinium = new Element("Ac", "actinium", 89,227, 3, 7);
  public static final Element thorium = new Element("Th", "thorium", 90,232.03806, 3, 7);
  public static final Element protactinium = new Element("Pa", "protactinium", 91,231.03588, 3, 7);
  public static final Element uranium = new Element("U", "uranium", 92,238.02891, 3, 7);
  public static final Element neptunium = new Element("Np", "neptunium", 93,237, 3, 7);
  public static final Element plutonium = new Element("Pu", "plutonium", 94,244, 3, 7);
  public static final Element americium = new Element("Am", "americium", 95,243, 3, 7);
  public static final Element curium = new Element("Cm", "curium", 96,247, 3, 7);
  public static final Element berkelium = new Element("Bk", "berkelium", 97,247, 3, 7);
  public static final Element californium = new Element("Cf", "californium", 98,251, 3, 7);
  public static final Element einsteinium = new Element("Es", "einsteinium", 99,252, 3, 7);
  public static final Element fermium = new Element("Fm", "fermium", 100,257, 3, 7);
  public static final Element mendelevium = new Element("Md", "mendelevium", 101,258, 3, 7);
  public static final Element nobelium = new Element("No", "nobelium", 102,259, 3, 7);
  public static final Element lawrencium = new Element("Lr", "lawrencium", 103,262, 3, 7);
  public static final Element rutherfordium = new Element("Rf", "rutherfordium", 104,261, 4, 7);
  public static final Element dubnium = new Element("Db", "dubnium", 105,262, 5, 7);
  public static final Element seaborgium = new Element("Sg", "seaborgium", 106,266, 6, 7);
  public static final Element bohrium = new Element("Bh", "bohrium", 107,264, 7, 7);
  public static final Element hassium = new Element("Hs", "hassium", 108,277, 8, 7);
  public static final Element meitnerium = new Element("Mt", "meitnerium", 109,268, 9, 7);
  public static final Element darmstadtium = new Element("Ds", "darmstadtium", 110,271, 10, 7);
  public static final Element roentgenium = new Element("Rg", "roentgenium", 111,272, 11, 7);
  public static final Element copernicium = new Element("Cn", "copernicium", 112,285, 12, 7);
  public static final Element nihonium = new Element("Nh", "nihonium", 113,284, 13, 7);
  public static final Element flerovium = new Element("Fl", "flerovium", 114,289, 14, 7);
  public static final Element moscovium = new Element("Mc", "moscovium", 115,288, 15, 7);
  public static final Element livermorium = new Element("Lv", "livermorium", 116,292, 16, 7);
  public static final Element tennessine = new Element("Ts", "tennessine", 117, -1, 17, 7);
  public static final Element oganesson = new Element("Og", "oganesson", 118,294, 18, 7);

  private static Element[] ELEMENTS_BY_ATOMIC_NUMBER = new Element[] {
      Element.vacancy,
      Element.hydrogen,
      Element.helium,
      Element.lithium,
      Element.beryllium,
      Element.boron,
      Element.carbon,
      Element.nitrogen,
      Element.oxygen,
      Element.fluorine,
      Element.neon,
      Element.sodium,
      Element.magnesium,
      Element.aluminum,
      Element.silicon,
      Element.phosphorus,
      Element.sulfur,
      Element.chlorine,
      Element.argon,
      Element.potassium,
      Element.calcium,
      Element.scandium,
      Element.titanium,
      Element.vanadium,
      Element.chromium,
      Element.manganese,
      Element.iron,
      Element.cobalt,
      Element.nickel,
      Element.copper,
      Element.zinc,
      Element.gallium,
      Element.germanium,
      Element.arsenic,
      Element.selenium,
      Element.bromine,
      Element.krypton,
      Element.rubidium,
      Element.strontium,
      Element.yttrium,
      Element.zirconium,
      Element.niobium,
      Element.molybdenum,
      Element.technetium,
      Element.ruthenium,
      Element.rhodium,
      Element.palladium,
      Element.silver,
      Element.cadmium,
      Element.indium,
      Element.tin,
      Element.antimony,
      Element.tellurium,
      Element.iodine,
      Element.xenon,
      Element.cesium ,
      Element.barium,
      Element.lanthanum,
      Element.cerium,
      Element.praseodymium,
      Element.neodymium,
      Element.promethium,
      Element.samarium,
      Element.europium,
      Element.gadolinium,
      Element.terbium,
      Element.dysprosium,
      Element.holmium,
      Element.erbium,
      Element.thulium,
      Element.ytterbium,
      Element.lutetium,
      Element.hafnium,
      Element.tantalum,
      Element.tungsten,
      Element.rhenium,
      Element.osmium,
      Element.iridium,
      Element.platinum,
      Element.gold,
      Element.mercury,
      Element.thallium,
      Element.lead,
      Element.bismuth,
      Element.polonium,
      Element.astatine,
      Element.radon,
      Element.francium,
      Element.radium,
      Element.actinium,
      Element.thorium,
      Element.protactinium,
      Element.uranium,
      Element.neptunium,
      Element.plutonium,
      Element.americium,
      Element.curium,
      Element.berkelium,
      Element.californium,
      Element.einsteinium,
      Element.fermium,
      Element.mendelevium,
      Element.nobelium,
      Element.lawrencium,
      Element.rutherfordium,
      Element.dubnium,
      Element.seaborgium,
      Element.bohrium,
      Element.hassium,
      Element.meitnerium,
      Element.darmstadtium,
      Element.roentgenium,
      Element.copernicium,
      Element.nihonium,
      Element.flerovium,
      Element.moscovium,
      Element.livermorium,
      Element.tennessine,
      Element.oganesson,
    };
  
  private static HashMap<String, Element> ELEMENTS_BY_SYMBOL = new HashMap<String, Element>();
  static {
    for (int atomicNumber = 0; atomicNumber < ELEMENTS_BY_ATOMIC_NUMBER.length; atomicNumber++) {
      Element element = ELEMENTS_BY_ATOMIC_NUMBER[atomicNumber];
      ELEMENTS_BY_SYMBOL.put(element.getSymbol(), element);
    }
  }
  
  // From dx.doi.org/10.1039/B801115J (Covalent Radii revisited)
  static {
	  ELEMENTS_BY_ATOMIC_NUMBER[1].m_CovalentRadius = 0.31;
	  ELEMENTS_BY_ATOMIC_NUMBER[2].m_CovalentRadius = 0.28;
	  ELEMENTS_BY_ATOMIC_NUMBER[3].m_CovalentRadius = 1.28;
	  ELEMENTS_BY_ATOMIC_NUMBER[4].m_CovalentRadius = 0.96;
	  ELEMENTS_BY_ATOMIC_NUMBER[5].m_CovalentRadius = 0.84;
	  ELEMENTS_BY_ATOMIC_NUMBER[6].m_CovalentRadius = 0.73;
	  ELEMENTS_BY_ATOMIC_NUMBER[7].m_CovalentRadius = 0.71;
	  ELEMENTS_BY_ATOMIC_NUMBER[8].m_CovalentRadius = 0.66;
	  ELEMENTS_BY_ATOMIC_NUMBER[9].m_CovalentRadius = 0.57;
	  ELEMENTS_BY_ATOMIC_NUMBER[10].m_CovalentRadius = 0.58;
	  ELEMENTS_BY_ATOMIC_NUMBER[11].m_CovalentRadius = 1.66;
	  ELEMENTS_BY_ATOMIC_NUMBER[12].m_CovalentRadius = 1.41;
	  ELEMENTS_BY_ATOMIC_NUMBER[13].m_CovalentRadius = 1.21;
	  ELEMENTS_BY_ATOMIC_NUMBER[14].m_CovalentRadius = 1.11;
	  ELEMENTS_BY_ATOMIC_NUMBER[15].m_CovalentRadius = 1.07;
	  ELEMENTS_BY_ATOMIC_NUMBER[16].m_CovalentRadius = 1.05;
	  ELEMENTS_BY_ATOMIC_NUMBER[17].m_CovalentRadius = 1.02;
	  ELEMENTS_BY_ATOMIC_NUMBER[18].m_CovalentRadius = 1.06;
	  ELEMENTS_BY_ATOMIC_NUMBER[19].m_CovalentRadius = 2.03;
	  ELEMENTS_BY_ATOMIC_NUMBER[20].m_CovalentRadius = 1.76;
	  ELEMENTS_BY_ATOMIC_NUMBER[21].m_CovalentRadius = 1.7;
	  ELEMENTS_BY_ATOMIC_NUMBER[22].m_CovalentRadius = 1.6;
	  ELEMENTS_BY_ATOMIC_NUMBER[23].m_CovalentRadius = 1.53;
	  ELEMENTS_BY_ATOMIC_NUMBER[24].m_CovalentRadius = 1.39;
	  ELEMENTS_BY_ATOMIC_NUMBER[25].m_CovalentRadius = 1.5;
	  ELEMENTS_BY_ATOMIC_NUMBER[26].m_CovalentRadius = 1.42;
	  ELEMENTS_BY_ATOMIC_NUMBER[27].m_CovalentRadius = 1.38;
	  ELEMENTS_BY_ATOMIC_NUMBER[28].m_CovalentRadius = 1.24;
	  ELEMENTS_BY_ATOMIC_NUMBER[29].m_CovalentRadius = 1.32;
	  ELEMENTS_BY_ATOMIC_NUMBER[30].m_CovalentRadius = 1.22;
	  ELEMENTS_BY_ATOMIC_NUMBER[31].m_CovalentRadius = 1.22;
	  ELEMENTS_BY_ATOMIC_NUMBER[32].m_CovalentRadius = 1.2;
	  ELEMENTS_BY_ATOMIC_NUMBER[33].m_CovalentRadius = 1.19;
	  ELEMENTS_BY_ATOMIC_NUMBER[34].m_CovalentRadius = 1.2;
	  ELEMENTS_BY_ATOMIC_NUMBER[35].m_CovalentRadius = 1.2;
	  ELEMENTS_BY_ATOMIC_NUMBER[36].m_CovalentRadius = 1.16;
	  ELEMENTS_BY_ATOMIC_NUMBER[37].m_CovalentRadius = 2.2;
	  ELEMENTS_BY_ATOMIC_NUMBER[38].m_CovalentRadius = 1.95;
	  ELEMENTS_BY_ATOMIC_NUMBER[39].m_CovalentRadius = 1.9;
	  ELEMENTS_BY_ATOMIC_NUMBER[40].m_CovalentRadius = 1.75;
	  ELEMENTS_BY_ATOMIC_NUMBER[41].m_CovalentRadius = 1.64;
	  ELEMENTS_BY_ATOMIC_NUMBER[42].m_CovalentRadius = 1.54;
	  ELEMENTS_BY_ATOMIC_NUMBER[43].m_CovalentRadius = 1.47;
	  ELEMENTS_BY_ATOMIC_NUMBER[44].m_CovalentRadius = 1.46;
	  ELEMENTS_BY_ATOMIC_NUMBER[45].m_CovalentRadius = 1.42;
	  ELEMENTS_BY_ATOMIC_NUMBER[46].m_CovalentRadius = 1.39;
	  ELEMENTS_BY_ATOMIC_NUMBER[47].m_CovalentRadius = 1.45;
	  ELEMENTS_BY_ATOMIC_NUMBER[48].m_CovalentRadius = 1.44;
	  ELEMENTS_BY_ATOMIC_NUMBER[49].m_CovalentRadius = 1.42;
	  ELEMENTS_BY_ATOMIC_NUMBER[50].m_CovalentRadius = 1.39;
	  ELEMENTS_BY_ATOMIC_NUMBER[51].m_CovalentRadius = 1.39;
	  ELEMENTS_BY_ATOMIC_NUMBER[52].m_CovalentRadius = 1.38;
	  ELEMENTS_BY_ATOMIC_NUMBER[53].m_CovalentRadius = 1.39;
	  ELEMENTS_BY_ATOMIC_NUMBER[54].m_CovalentRadius = 1.4;
	  ELEMENTS_BY_ATOMIC_NUMBER[55].m_CovalentRadius = 2.44;
	  ELEMENTS_BY_ATOMIC_NUMBER[56].m_CovalentRadius = 2.15;
	  ELEMENTS_BY_ATOMIC_NUMBER[57].m_CovalentRadius = 2.07;
	  ELEMENTS_BY_ATOMIC_NUMBER[58].m_CovalentRadius = 2.04;
	  ELEMENTS_BY_ATOMIC_NUMBER[59].m_CovalentRadius = 2.03;
	  ELEMENTS_BY_ATOMIC_NUMBER[60].m_CovalentRadius = 2.01;
	  ELEMENTS_BY_ATOMIC_NUMBER[61].m_CovalentRadius = 1.99;
	  ELEMENTS_BY_ATOMIC_NUMBER[62].m_CovalentRadius = 1.98;
	  ELEMENTS_BY_ATOMIC_NUMBER[63].m_CovalentRadius = 1.98;
	  ELEMENTS_BY_ATOMIC_NUMBER[64].m_CovalentRadius = 1.96;
	  ELEMENTS_BY_ATOMIC_NUMBER[65].m_CovalentRadius = 1.94;
	  ELEMENTS_BY_ATOMIC_NUMBER[66].m_CovalentRadius = 1.92;
	  ELEMENTS_BY_ATOMIC_NUMBER[67].m_CovalentRadius = 1.92;
	  ELEMENTS_BY_ATOMIC_NUMBER[68].m_CovalentRadius = 1.89;
	  ELEMENTS_BY_ATOMIC_NUMBER[69].m_CovalentRadius = 1.9;
	  ELEMENTS_BY_ATOMIC_NUMBER[70].m_CovalentRadius = 1.87;
	  ELEMENTS_BY_ATOMIC_NUMBER[71].m_CovalentRadius = 1.87;
	  ELEMENTS_BY_ATOMIC_NUMBER[72].m_CovalentRadius = 1.75;
	  ELEMENTS_BY_ATOMIC_NUMBER[73].m_CovalentRadius = 1.7;
	  ELEMENTS_BY_ATOMIC_NUMBER[74].m_CovalentRadius = 1.62;
	  ELEMENTS_BY_ATOMIC_NUMBER[75].m_CovalentRadius = 1.51;
	  ELEMENTS_BY_ATOMIC_NUMBER[76].m_CovalentRadius = 1.44;
	  ELEMENTS_BY_ATOMIC_NUMBER[77].m_CovalentRadius = 1.41;
	  ELEMENTS_BY_ATOMIC_NUMBER[78].m_CovalentRadius = 1.36;
	  ELEMENTS_BY_ATOMIC_NUMBER[79].m_CovalentRadius = 1.36;
	  ELEMENTS_BY_ATOMIC_NUMBER[80].m_CovalentRadius = 1.32;
	  ELEMENTS_BY_ATOMIC_NUMBER[81].m_CovalentRadius = 1.45;
	  ELEMENTS_BY_ATOMIC_NUMBER[82].m_CovalentRadius = 1.46;
	  ELEMENTS_BY_ATOMIC_NUMBER[83].m_CovalentRadius = 1.48;
	  ELEMENTS_BY_ATOMIC_NUMBER[84].m_CovalentRadius = 1.4;
	  ELEMENTS_BY_ATOMIC_NUMBER[85].m_CovalentRadius = 1.5;
	  ELEMENTS_BY_ATOMIC_NUMBER[86].m_CovalentRadius = 1.5;
	  ELEMENTS_BY_ATOMIC_NUMBER[87].m_CovalentRadius = 2.6;
	  ELEMENTS_BY_ATOMIC_NUMBER[88].m_CovalentRadius = 2.21;
	  ELEMENTS_BY_ATOMIC_NUMBER[89].m_CovalentRadius = 2.15;
	  ELEMENTS_BY_ATOMIC_NUMBER[90].m_CovalentRadius = 2.06;
	  ELEMENTS_BY_ATOMIC_NUMBER[91].m_CovalentRadius = 2;
	  ELEMENTS_BY_ATOMIC_NUMBER[92].m_CovalentRadius = 1.96;
	  ELEMENTS_BY_ATOMIC_NUMBER[93].m_CovalentRadius = 1.9;
	  ELEMENTS_BY_ATOMIC_NUMBER[94].m_CovalentRadius = 1.87;
	  ELEMENTS_BY_ATOMIC_NUMBER[95].m_CovalentRadius = 1.8;
	  ELEMENTS_BY_ATOMIC_NUMBER[96].m_CovalentRadius = 1.69;
  }
  
  // From CRC 88th edition online, August 6, 2008.
  // I modified the crystal radius of N 5+ III to be consistent with the ionic radius
  static {
    ELEMENTS_BY_ATOMIC_NUMBER[1 ].m_PaulingElectronegativity_CRC = 2.2;
    ELEMENTS_BY_ATOMIC_NUMBER[2 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[3 ].m_PaulingElectronegativity_CRC = 0.98;
    ELEMENTS_BY_ATOMIC_NUMBER[4 ].m_PaulingElectronegativity_CRC = 1.57;
    ELEMENTS_BY_ATOMIC_NUMBER[5 ].m_PaulingElectronegativity_CRC = 2.04;
    //ELEMENTS_BY_ATOMIC_NUMBER[5 ].m_PaulingElectronegativity_CRC = 2.6;
    ELEMENTS_BY_ATOMIC_NUMBER[6 ].m_PaulingElectronegativity_CRC = 2.55;
    ELEMENTS_BY_ATOMIC_NUMBER[7 ].m_PaulingElectronegativity_CRC = 3.04;
    ELEMENTS_BY_ATOMIC_NUMBER[8 ].m_PaulingElectronegativity_CRC = 3.44;
    ELEMENTS_BY_ATOMIC_NUMBER[9 ].m_PaulingElectronegativity_CRC = 3.98;
    ELEMENTS_BY_ATOMIC_NUMBER[10 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[11 ].m_PaulingElectronegativity_CRC = 0.93;
    ELEMENTS_BY_ATOMIC_NUMBER[12 ].m_PaulingElectronegativity_CRC = 1.31;
    ELEMENTS_BY_ATOMIC_NUMBER[13 ].m_PaulingElectronegativity_CRC = 1.61;
    ELEMENTS_BY_ATOMIC_NUMBER[14 ].m_PaulingElectronegativity_CRC = 1.9;
    ELEMENTS_BY_ATOMIC_NUMBER[15 ].m_PaulingElectronegativity_CRC = 2.19;
    ELEMENTS_BY_ATOMIC_NUMBER[16 ].m_PaulingElectronegativity_CRC = 2.58;
    ELEMENTS_BY_ATOMIC_NUMBER[17 ].m_PaulingElectronegativity_CRC = 3.16;
    ELEMENTS_BY_ATOMIC_NUMBER[18 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[19 ].m_PaulingElectronegativity_CRC = 0.82;
    ELEMENTS_BY_ATOMIC_NUMBER[20 ].m_PaulingElectronegativity_CRC = 1;
    ELEMENTS_BY_ATOMIC_NUMBER[21 ].m_PaulingElectronegativity_CRC = 1.36;
    ELEMENTS_BY_ATOMIC_NUMBER[22 ].m_PaulingElectronegativity_CRC = 1.54;
    ELEMENTS_BY_ATOMIC_NUMBER[23 ].m_PaulingElectronegativity_CRC = 1.63;
    ELEMENTS_BY_ATOMIC_NUMBER[24 ].m_PaulingElectronegativity_CRC = 1.66;
    ELEMENTS_BY_ATOMIC_NUMBER[25 ].m_PaulingElectronegativity_CRC = 1.55;
    ELEMENTS_BY_ATOMIC_NUMBER[26 ].m_PaulingElectronegativity_CRC = 1.83;
    ELEMENTS_BY_ATOMIC_NUMBER[27 ].m_PaulingElectronegativity_CRC = 1.88;
    ELEMENTS_BY_ATOMIC_NUMBER[28 ].m_PaulingElectronegativity_CRC = 1.91;
    ELEMENTS_BY_ATOMIC_NUMBER[29 ].m_PaulingElectronegativity_CRC = 1.9;
    ELEMENTS_BY_ATOMIC_NUMBER[30 ].m_PaulingElectronegativity_CRC = 1.65;
    ELEMENTS_BY_ATOMIC_NUMBER[31 ].m_PaulingElectronegativity_CRC = 1.81;
    ELEMENTS_BY_ATOMIC_NUMBER[32 ].m_PaulingElectronegativity_CRC = 2.01;
    ELEMENTS_BY_ATOMIC_NUMBER[33 ].m_PaulingElectronegativity_CRC = 2.18; // 2.01 in the online table 104th edition.  Not sure why.
    ELEMENTS_BY_ATOMIC_NUMBER[34 ].m_PaulingElectronegativity_CRC = 2.55;
    ELEMENTS_BY_ATOMIC_NUMBER[35 ].m_PaulingElectronegativity_CRC = 2.96;
    ELEMENTS_BY_ATOMIC_NUMBER[36 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[37 ].m_PaulingElectronegativity_CRC = 0.82;
    ELEMENTS_BY_ATOMIC_NUMBER[38 ].m_PaulingElectronegativity_CRC = 0.95;
    ELEMENTS_BY_ATOMIC_NUMBER[39 ].m_PaulingElectronegativity_CRC = 1.22;
    ELEMENTS_BY_ATOMIC_NUMBER[40 ].m_PaulingElectronegativity_CRC = 1.33;
    ELEMENTS_BY_ATOMIC_NUMBER[41 ].m_PaulingElectronegativity_CRC = 1.6;
    ELEMENTS_BY_ATOMIC_NUMBER[42 ].m_PaulingElectronegativity_CRC = 2.16;
    ELEMENTS_BY_ATOMIC_NUMBER[43 ].m_PaulingElectronegativity_CRC = 2.1;
    ELEMENTS_BY_ATOMIC_NUMBER[44 ].m_PaulingElectronegativity_CRC = 2.2;
    ELEMENTS_BY_ATOMIC_NUMBER[45 ].m_PaulingElectronegativity_CRC = 2.28;
    ELEMENTS_BY_ATOMIC_NUMBER[46 ].m_PaulingElectronegativity_CRC = 2.2;
    ELEMENTS_BY_ATOMIC_NUMBER[47 ].m_PaulingElectronegativity_CRC = 1.93;
    ELEMENTS_BY_ATOMIC_NUMBER[48 ].m_PaulingElectronegativity_CRC = 1.69;
    ELEMENTS_BY_ATOMIC_NUMBER[49 ].m_PaulingElectronegativity_CRC = 1.78;
    ELEMENTS_BY_ATOMIC_NUMBER[50 ].m_PaulingElectronegativity_CRC = 1.96;
    ELEMENTS_BY_ATOMIC_NUMBER[51 ].m_PaulingElectronegativity_CRC = 2.05;
    ELEMENTS_BY_ATOMIC_NUMBER[52 ].m_PaulingElectronegativity_CRC = 2.1;
    ELEMENTS_BY_ATOMIC_NUMBER[53 ].m_PaulingElectronegativity_CRC = 2.66;
    ELEMENTS_BY_ATOMIC_NUMBER[54 ].m_PaulingElectronegativity_CRC = 2.6;
    ELEMENTS_BY_ATOMIC_NUMBER[55 ].m_PaulingElectronegativity_CRC = 0.79;
    ELEMENTS_BY_ATOMIC_NUMBER[56 ].m_PaulingElectronegativity_CRC = 0.89;
    ELEMENTS_BY_ATOMIC_NUMBER[57 ].m_PaulingElectronegativity_CRC = 1.1;
    ELEMENTS_BY_ATOMIC_NUMBER[58 ].m_PaulingElectronegativity_CRC = 1.12;
    ELEMENTS_BY_ATOMIC_NUMBER[59 ].m_PaulingElectronegativity_CRC = 1.13;
    ELEMENTS_BY_ATOMIC_NUMBER[60 ].m_PaulingElectronegativity_CRC = 1.14;
    ELEMENTS_BY_ATOMIC_NUMBER[61 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[62 ].m_PaulingElectronegativity_CRC = 1.17;
    ELEMENTS_BY_ATOMIC_NUMBER[63 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[64 ].m_PaulingElectronegativity_CRC = 1.2;
    ELEMENTS_BY_ATOMIC_NUMBER[65 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[66 ].m_PaulingElectronegativity_CRC = 1.22;
    ELEMENTS_BY_ATOMIC_NUMBER[67 ].m_PaulingElectronegativity_CRC = 1.23;
    ELEMENTS_BY_ATOMIC_NUMBER[68 ].m_PaulingElectronegativity_CRC = 1.24;
    ELEMENTS_BY_ATOMIC_NUMBER[69 ].m_PaulingElectronegativity_CRC = 1.25;
    ELEMENTS_BY_ATOMIC_NUMBER[70 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[71 ].m_PaulingElectronegativity_CRC = 1;
    ELEMENTS_BY_ATOMIC_NUMBER[72 ].m_PaulingElectronegativity_CRC = 1.3;
    ELEMENTS_BY_ATOMIC_NUMBER[73 ].m_PaulingElectronegativity_CRC = 1.5;
    ELEMENTS_BY_ATOMIC_NUMBER[74 ].m_PaulingElectronegativity_CRC = 1.7;
    ELEMENTS_BY_ATOMIC_NUMBER[75 ].m_PaulingElectronegativity_CRC = 1.9;
    ELEMENTS_BY_ATOMIC_NUMBER[76 ].m_PaulingElectronegativity_CRC = 2.2;
    ELEMENTS_BY_ATOMIC_NUMBER[77 ].m_PaulingElectronegativity_CRC = 2.2;
    ELEMENTS_BY_ATOMIC_NUMBER[78 ].m_PaulingElectronegativity_CRC = 2.2;
    ELEMENTS_BY_ATOMIC_NUMBER[79 ].m_PaulingElectronegativity_CRC = 2.4;
    ELEMENTS_BY_ATOMIC_NUMBER[80 ].m_PaulingElectronegativity_CRC = 1.9;
    ELEMENTS_BY_ATOMIC_NUMBER[81 ].m_PaulingElectronegativity_CRC = 1.8;
    ELEMENTS_BY_ATOMIC_NUMBER[82 ].m_PaulingElectronegativity_CRC = 1.8;
    ELEMENTS_BY_ATOMIC_NUMBER[83 ].m_PaulingElectronegativity_CRC = 1.9;
    ELEMENTS_BY_ATOMIC_NUMBER[84 ].m_PaulingElectronegativity_CRC = 2;
    ELEMENTS_BY_ATOMIC_NUMBER[85 ].m_PaulingElectronegativity_CRC = 2.2;
    ELEMENTS_BY_ATOMIC_NUMBER[86 ].m_PaulingElectronegativity_CRC = Double.NaN;
    ELEMENTS_BY_ATOMIC_NUMBER[87 ].m_PaulingElectronegativity_CRC = 0.7;
    ELEMENTS_BY_ATOMIC_NUMBER[88 ].m_PaulingElectronegativity_CRC = 0.9;
    ELEMENTS_BY_ATOMIC_NUMBER[89 ].m_PaulingElectronegativity_CRC = 1.1;
    ELEMENTS_BY_ATOMIC_NUMBER[90 ].m_PaulingElectronegativity_CRC = 1.3;
    ELEMENTS_BY_ATOMIC_NUMBER[91 ].m_PaulingElectronegativity_CRC = 1.5;
    ELEMENTS_BY_ATOMIC_NUMBER[92 ].m_PaulingElectronegativity_CRC = 1.7;
    ELEMENTS_BY_ATOMIC_NUMBER[93 ].m_PaulingElectronegativity_CRC = 1.3;
    ELEMENTS_BY_ATOMIC_NUMBER[94 ].m_PaulingElectronegativity_CRC = 1.3;
  }
  
  /**
   * From .J. Little and M.M. Jones, J. Chem. Ed., 1960, 37, 231.
   * (Some values originally from A.L. Allred and E.G. Rochow, J. Inorg. Nucl. Chem., 1958, 5, 264.)
   * 
   */
  static {
	  ELEMENTS_BY_SYMBOL.get("Li").m_ARElectronegativity = 0.97;
	  ELEMENTS_BY_SYMBOL.get("Na").m_ARElectronegativity = 1.01;
	  ELEMENTS_BY_SYMBOL.get("K").m_ARElectronegativity = 0.91;
	  ELEMENTS_BY_SYMBOL.get("Rb").m_ARElectronegativity = 0.89;
	  ELEMENTS_BY_SYMBOL.get("Cs").m_ARElectronegativity = 0.86;
	  ELEMENTS_BY_SYMBOL.get("Fr").m_ARElectronegativity = 0.86;
	  ELEMENTS_BY_SYMBOL.get("Be").m_ARElectronegativity = 1.47;
	  ELEMENTS_BY_SYMBOL.get("Mg").m_ARElectronegativity = 1.23;
	  ELEMENTS_BY_SYMBOL.get("Ca").m_ARElectronegativity = 1.04;
	  ELEMENTS_BY_SYMBOL.get("Sr").m_ARElectronegativity = 0.99;
	  ELEMENTS_BY_SYMBOL.get("Ba").m_ARElectronegativity = 0.97;
	  ELEMENTS_BY_SYMBOL.get("Ra").m_ARElectronegativity = 0.97;
	  ELEMENTS_BY_SYMBOL.get("Sc").m_ARElectronegativity = 1.2;
	  ELEMENTS_BY_SYMBOL.get("Y").m_ARElectronegativity = 1.11;
	  ELEMENTS_BY_SYMBOL.get("Ti").m_ARElectronegativity = 1.32;
	  ELEMENTS_BY_SYMBOL.get("Zr").m_ARElectronegativity = 1.22;
	  ELEMENTS_BY_SYMBOL.get("Hf").m_ARElectronegativity = 1.23;
	  ELEMENTS_BY_SYMBOL.get("V").m_ARElectronegativity = 1.45;
	  ELEMENTS_BY_SYMBOL.get("Nb").m_ARElectronegativity = 1.23;
	  ELEMENTS_BY_SYMBOL.get("Ta").m_ARElectronegativity = 1.33;
	  ELEMENTS_BY_SYMBOL.get("Cr").m_ARElectronegativity = 1.56;
	  ELEMENTS_BY_SYMBOL.get("Mo").m_ARElectronegativity = 1.3;
	  ELEMENTS_BY_SYMBOL.get("W").m_ARElectronegativity = 1.4;
	  ELEMENTS_BY_SYMBOL.get("Mn").m_ARElectronegativity = 1.6;
	  ELEMENTS_BY_SYMBOL.get("Tc").m_ARElectronegativity = 1.36;
	  ELEMENTS_BY_SYMBOL.get("Re").m_ARElectronegativity = 1.46;
	  ELEMENTS_BY_SYMBOL.get("Fe").m_ARElectronegativity = 1.64;
	  ELEMENTS_BY_SYMBOL.get("Ru").m_ARElectronegativity = 1.42;
	  ELEMENTS_BY_SYMBOL.get("Os").m_ARElectronegativity = 1.52;
	  ELEMENTS_BY_SYMBOL.get("Co").m_ARElectronegativity = 1.7;
	  ELEMENTS_BY_SYMBOL.get("Rh").m_ARElectronegativity = 1.45;
	  ELEMENTS_BY_SYMBOL.get("Ir").m_ARElectronegativity = 1.55;
	  ELEMENTS_BY_SYMBOL.get("Ni").m_ARElectronegativity = 1.75;
	  ELEMENTS_BY_SYMBOL.get("Pd").m_ARElectronegativity = 1.35;
	  ELEMENTS_BY_SYMBOL.get("Pt").m_ARElectronegativity = 1.44;
	  ELEMENTS_BY_SYMBOL.get("Cu").m_ARElectronegativity = 1.75;
	  ELEMENTS_BY_SYMBOL.get("Ag").m_ARElectronegativity = 1.42;
	  ELEMENTS_BY_SYMBOL.get("Au").m_ARElectronegativity = 1.42;
	  ELEMENTS_BY_SYMBOL.get("Zn").m_ARElectronegativity = 1.66;
	  ELEMENTS_BY_SYMBOL.get("Cd").m_ARElectronegativity = 1.46;
	  ELEMENTS_BY_SYMBOL.get("Hg").m_ARElectronegativity = 1.44;
	  ELEMENTS_BY_SYMBOL.get("B").m_ARElectronegativity = 2.01;
	  ELEMENTS_BY_SYMBOL.get("Al").m_ARElectronegativity = 1.47;
	  ELEMENTS_BY_SYMBOL.get("Ga").m_ARElectronegativity = 1.82;
	  ELEMENTS_BY_SYMBOL.get("In").m_ARElectronegativity = 1.49;
	  ELEMENTS_BY_SYMBOL.get("Tl").m_ARElectronegativity = 1.44;
	  ELEMENTS_BY_SYMBOL.get("C").m_ARElectronegativity = 2.5;
	  ELEMENTS_BY_SYMBOL.get("Si").m_ARElectronegativity = 1.74;
	  ELEMENTS_BY_SYMBOL.get("Ge").m_ARElectronegativity = 2.02;
	  ELEMENTS_BY_SYMBOL.get("Sn").m_ARElectronegativity = 1.72;
	  ELEMENTS_BY_SYMBOL.get("Pb").m_ARElectronegativity = 1.55;
	  ELEMENTS_BY_SYMBOL.get("N").m_ARElectronegativity = 3.07;
	  ELEMENTS_BY_SYMBOL.get("P").m_ARElectronegativity = 2.06;
	  ELEMENTS_BY_SYMBOL.get("As").m_ARElectronegativity = 2.2;
	  ELEMENTS_BY_SYMBOL.get("Sb").m_ARElectronegativity = 1.82;
	  ELEMENTS_BY_SYMBOL.get("Bi").m_ARElectronegativity = 1.67;
	  ELEMENTS_BY_SYMBOL.get("O").m_ARElectronegativity = 3.5;
	  ELEMENTS_BY_SYMBOL.get("S").m_ARElectronegativity = 2.44;
	  ELEMENTS_BY_SYMBOL.get("Se").m_ARElectronegativity = 2.48;
	  ELEMENTS_BY_SYMBOL.get("Te").m_ARElectronegativity = 2.01;
	  ELEMENTS_BY_SYMBOL.get("Po").m_ARElectronegativity = 1.76;
	  ELEMENTS_BY_SYMBOL.get("F").m_ARElectronegativity = 4.1;
	  ELEMENTS_BY_SYMBOL.get("Cl").m_ARElectronegativity = 2.83;
	  ELEMENTS_BY_SYMBOL.get("Br").m_ARElectronegativity = 2.74;
	  ELEMENTS_BY_SYMBOL.get("I").m_ARElectronegativity = 2.21;
	  ELEMENTS_BY_SYMBOL.get("At").m_ARElectronegativity = 1.9;
	  ELEMENTS_BY_SYMBOL.get("La").m_ARElectronegativity = 1.08;
	  ELEMENTS_BY_SYMBOL.get("Ce").m_ARElectronegativity = 1.08;
	  ELEMENTS_BY_SYMBOL.get("Pr").m_ARElectronegativity = 1.07;
	  ELEMENTS_BY_SYMBOL.get("Pm").m_ARElectronegativity = 1.07;
	  ELEMENTS_BY_SYMBOL.get("Sm").m_ARElectronegativity = 1.07;
	  ELEMENTS_BY_SYMBOL.get("Eu").m_ARElectronegativity = 1.01;
	  ELEMENTS_BY_SYMBOL.get("Gd").m_ARElectronegativity = 1.11;
	  ELEMENTS_BY_SYMBOL.get("Tb").m_ARElectronegativity = 1.1;
	  ELEMENTS_BY_SYMBOL.get("Dy").m_ARElectronegativity = 1.1;
	  ELEMENTS_BY_SYMBOL.get("Ho").m_ARElectronegativity = 1.1;
	  ELEMENTS_BY_SYMBOL.get("Er").m_ARElectronegativity = 1.11;
	  ELEMENTS_BY_SYMBOL.get("Tm").m_ARElectronegativity = 1.11;
	  ELEMENTS_BY_SYMBOL.get("Yb").m_ARElectronegativity = 1.06;
	  ELEMENTS_BY_SYMBOL.get("Lu").m_ARElectronegativity = 1.14;
	  ELEMENTS_BY_SYMBOL.get("Ac").m_ARElectronegativity = 1;
	  ELEMENTS_BY_SYMBOL.get("Th").m_ARElectronegativity = 1.11;
	  ELEMENTS_BY_SYMBOL.get("Pa").m_ARElectronegativity = 1.14;
	  ELEMENTS_BY_SYMBOL.get("U").m_ARElectronegativity = 1.22;
	  ELEMENTS_BY_SYMBOL.get("Np").m_ARElectronegativity = 1.22;
	  ELEMENTS_BY_SYMBOL.get("Pu").m_ARElectronegativity = 1.22;
  }
  
  // From http://abulafia.mt.ic.ac.uk/shannon/ptable.php, based on R. D. Shannon's Acta Cryst. (1976). A32, 751
  static {
    ELEMENTS_BY_ATOMIC_NUMBER[1].m_ShannonRadii = new ShannonRadiusRecord[2];
    ELEMENTS_BY_ATOMIC_NUMBER[3].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[4].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[5].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[6].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[7].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[8].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[9].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[12].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[13].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[14].m_ShannonRadii = new ShannonRadiusRecord[2];
    ELEMENTS_BY_ATOMIC_NUMBER[15].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[16].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[17].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[21].m_ShannonRadii = new ShannonRadiusRecord[2];
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii = new ShannonRadiusRecord[8];
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii = new ShannonRadiusRecord[10];
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii = new ShannonRadiusRecord[15];
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii = new ShannonRadiusRecord[12];
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii = new ShannonRadiusRecord[9];
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii = new ShannonRadiusRecord[8];
    ELEMENTS_BY_ATOMIC_NUMBER[30].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[31].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[32].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[33].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[34].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[35].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii = new ShannonRadiusRecord[8];
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[39].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii = new ShannonRadiusRecord[8];
    ELEMENTS_BY_ATOMIC_NUMBER[43].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[44].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[45].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[46].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii = new ShannonRadiusRecord[11];
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[49].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[50].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[51].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[53].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[54].m_ShannonRadii = new ShannonRadiusRecord[2];
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii = new ShannonRadiusRecord[10];
    ELEMENTS_BY_ATOMIC_NUMBER[59].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[61].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii = new ShannonRadiusRecord[8];
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii = new ShannonRadiusRecord[9];
    ELEMENTS_BY_ATOMIC_NUMBER[64].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[67].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[68].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[69].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[71].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[72].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[73].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[74].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[75].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[77].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[78].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[79].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii = new ShannonRadiusRecord[12];
    ELEMENTS_BY_ATOMIC_NUMBER[83].m_ShannonRadii = new ShannonRadiusRecord[4];
    ELEMENTS_BY_ATOMIC_NUMBER[84].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[85].m_ShannonRadii = new ShannonRadiusRecord[1];
    ELEMENTS_BY_ATOMIC_NUMBER[87].m_ShannonRadii = new ShannonRadiusRecord[1];
    ELEMENTS_BY_ATOMIC_NUMBER[88].m_ShannonRadii = new ShannonRadiusRecord[2];
    ELEMENTS_BY_ATOMIC_NUMBER[89].m_ShannonRadii = new ShannonRadiusRecord[1];
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_ShannonRadii = new ShannonRadiusRecord[6];
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii = new ShannonRadiusRecord[13];
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[94].m_ShannonRadii = new ShannonRadiusRecord[5];
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_ShannonRadii = new ShannonRadiusRecord[7];
    ELEMENTS_BY_ATOMIC_NUMBER[96].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[97].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[98].m_ShannonRadii = new ShannonRadiusRecord[3];
    ELEMENTS_BY_ATOMIC_NUMBER[102].m_ShannonRadii = new ShannonRadiusRecord[1];
    
    ELEMENTS_BY_ATOMIC_NUMBER[1].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[1].new ShannonRadiusRecord("I",1,0,-0.24,-0.38);
    ELEMENTS_BY_ATOMIC_NUMBER[1].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[1].new ShannonRadiusRecord("II",1,0,-0.04,-0.18);
    ELEMENTS_BY_ATOMIC_NUMBER[3].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[3].new ShannonRadiusRecord("IV",1,0,0.73,0.59);
    ELEMENTS_BY_ATOMIC_NUMBER[3].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[3].new ShannonRadiusRecord("VI",1,0,0.9,0.76);
    ELEMENTS_BY_ATOMIC_NUMBER[3].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[3].new ShannonRadiusRecord("VIII",1,0,1.06,0.92);
    ELEMENTS_BY_ATOMIC_NUMBER[4].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[4].new ShannonRadiusRecord("III",2,0,0.3,0.16);
    ELEMENTS_BY_ATOMIC_NUMBER[4].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[4].new ShannonRadiusRecord("IV",2,0,0.41,0.27);
    ELEMENTS_BY_ATOMIC_NUMBER[4].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[4].new ShannonRadiusRecord("VI",2,0,0.59,0.45);
    ELEMENTS_BY_ATOMIC_NUMBER[5].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[5].new ShannonRadiusRecord("III",3,0,0.15,0.01);
    ELEMENTS_BY_ATOMIC_NUMBER[5].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[5].new ShannonRadiusRecord("IV",3,0,0.25,0.11);
    ELEMENTS_BY_ATOMIC_NUMBER[5].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[5].new ShannonRadiusRecord("VI",3,0,0.41,0.27);
    ELEMENTS_BY_ATOMIC_NUMBER[6].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[6].new ShannonRadiusRecord("III",4,0,0.06,-0.08);
    ELEMENTS_BY_ATOMIC_NUMBER[6].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[6].new ShannonRadiusRecord("IV",4,0,0.29,0.15);
    ELEMENTS_BY_ATOMIC_NUMBER[6].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[6].new ShannonRadiusRecord("VI",4,0,0.3,0.16);
    ELEMENTS_BY_ATOMIC_NUMBER[7].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[7].new ShannonRadiusRecord("IV",-3,0,1.32,1.46);
    ELEMENTS_BY_ATOMIC_NUMBER[7].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[7].new ShannonRadiusRecord("VI",3,0,0.3,0.16);
    ELEMENTS_BY_ATOMIC_NUMBER[7].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[7].new ShannonRadiusRecord("III",5,0,0.036,-0.104); // TODO check this
    ELEMENTS_BY_ATOMIC_NUMBER[7].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[7].new ShannonRadiusRecord("VI",5,0,0.27,0.13);
    ELEMENTS_BY_ATOMIC_NUMBER[8].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[8].new ShannonRadiusRecord("II",-2,0,1.21,1.35);
    ELEMENTS_BY_ATOMIC_NUMBER[8].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[8].new ShannonRadiusRecord("III",-2,0,1.22,1.36);
    ELEMENTS_BY_ATOMIC_NUMBER[8].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[8].new ShannonRadiusRecord("IV",-2,0,1.24,1.38);
    ELEMENTS_BY_ATOMIC_NUMBER[8].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[8].new ShannonRadiusRecord("VI",-2,0,1.26,1.4);
    ELEMENTS_BY_ATOMIC_NUMBER[8].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[8].new ShannonRadiusRecord("VIII",-2,0,1.28,1.42);
    ELEMENTS_BY_ATOMIC_NUMBER[9].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[9].new ShannonRadiusRecord("II",-1,0,1.145,1.285);
    ELEMENTS_BY_ATOMIC_NUMBER[9].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[9].new ShannonRadiusRecord("III",-1,0,1.16,1.3);
    ELEMENTS_BY_ATOMIC_NUMBER[9].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[9].new ShannonRadiusRecord("IV",-1,0,1.17,1.31);
    ELEMENTS_BY_ATOMIC_NUMBER[9].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[9].new ShannonRadiusRecord("VI",-1,0,1.19,1.33);
    ELEMENTS_BY_ATOMIC_NUMBER[9].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[9].new ShannonRadiusRecord("VI",7,0,0.22,0.08);
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[11].new ShannonRadiusRecord("IV",1,0,1.13,0.99);
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[11].new ShannonRadiusRecord("V",1,0,1.14,1);
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[11].new ShannonRadiusRecord("VI",1,0,1.16,1.02);
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[11].new ShannonRadiusRecord("VII",1,0,1.26,1.12);
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[11].new ShannonRadiusRecord("VIII",1,0,1.32,1.18);
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[11].new ShannonRadiusRecord("IX",1,0,1.38,1.24);
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[11].new ShannonRadiusRecord("XII",1,0,1.53,1.39);
    ELEMENTS_BY_ATOMIC_NUMBER[12].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[12].new ShannonRadiusRecord("IV",2,0,0.71,0.57);
    ELEMENTS_BY_ATOMIC_NUMBER[12].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[12].new ShannonRadiusRecord("V",2,0,0.8,0.66);
    ELEMENTS_BY_ATOMIC_NUMBER[12].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[12].new ShannonRadiusRecord("VI",2,0,0.86,0.72);
    ELEMENTS_BY_ATOMIC_NUMBER[12].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[12].new ShannonRadiusRecord("VIII",2,0,1.03,0.89);
    ELEMENTS_BY_ATOMIC_NUMBER[13].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[13].new ShannonRadiusRecord("IV",3,0,0.53,0.39);
    ELEMENTS_BY_ATOMIC_NUMBER[13].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[13].new ShannonRadiusRecord("V",3,0,0.62,0.48);
    ELEMENTS_BY_ATOMIC_NUMBER[13].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[13].new ShannonRadiusRecord("VI",3,0,0.675,0.535);
    ELEMENTS_BY_ATOMIC_NUMBER[14].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[14].new ShannonRadiusRecord("IV",4,0,0.4,0.26);
    ELEMENTS_BY_ATOMIC_NUMBER[14].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[14].new ShannonRadiusRecord("VI",4,0,0.54,0.4);
    ELEMENTS_BY_ATOMIC_NUMBER[15].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[15].new ShannonRadiusRecord("VI",3,0,0.58,0.44);
    ELEMENTS_BY_ATOMIC_NUMBER[15].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[15].new ShannonRadiusRecord("IV",5,0,0.31,0.17);
    ELEMENTS_BY_ATOMIC_NUMBER[15].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[15].new ShannonRadiusRecord("V",5,0,0.43,0.29);
    ELEMENTS_BY_ATOMIC_NUMBER[15].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[15].new ShannonRadiusRecord("VI",5,0,0.52,0.38);
    ELEMENTS_BY_ATOMIC_NUMBER[16].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[16].new ShannonRadiusRecord("VI",-2,0,1.7,1.84);
    ELEMENTS_BY_ATOMIC_NUMBER[16].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[16].new ShannonRadiusRecord("VI",4,0,0.51,0.37);
    ELEMENTS_BY_ATOMIC_NUMBER[16].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[16].new ShannonRadiusRecord("IV",6,0,0.26,0.12);
    ELEMENTS_BY_ATOMIC_NUMBER[16].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[16].new ShannonRadiusRecord("VI",6,0,0.43,0.29);
    ELEMENTS_BY_ATOMIC_NUMBER[17].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[17].new ShannonRadiusRecord("VI",-1,0,1.67,1.81);
    ELEMENTS_BY_ATOMIC_NUMBER[17].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[17].new ShannonRadiusRecord("IIIPY",5,0,0.26,0.12);
    ELEMENTS_BY_ATOMIC_NUMBER[17].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[17].new ShannonRadiusRecord("IV",7,0,0.22,0.08);
    ELEMENTS_BY_ATOMIC_NUMBER[17].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[17].new ShannonRadiusRecord("VI",7,0,0.41,0.27);
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[19].new ShannonRadiusRecord("IV",1,0,1.51,1.37);
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[19].new ShannonRadiusRecord("VI",1,0,1.52,1.38);
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[19].new ShannonRadiusRecord("VII",1,0,1.6,1.46);
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[19].new ShannonRadiusRecord("VIII",1,0,1.65,1.51);
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[19].new ShannonRadiusRecord("IX",1,0,1.69,1.55);
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[19].new ShannonRadiusRecord("X",1,0,1.73,1.59);
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[19].new ShannonRadiusRecord("XII",1,0,1.78,1.64);
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[20].new ShannonRadiusRecord("VI",2,0,1.14,1);
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[20].new ShannonRadiusRecord("VII",2,0,1.2,1.06);
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[20].new ShannonRadiusRecord("VIII",2,0,1.26,1.12);
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[20].new ShannonRadiusRecord("IX",2,0,1.32,1.18);
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[20].new ShannonRadiusRecord("X",2,0,1.37,1.23);
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[20].new ShannonRadiusRecord("XII",2,0,1.48,1.34);
    ELEMENTS_BY_ATOMIC_NUMBER[21].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[21].new ShannonRadiusRecord("VI",3,0,0.885,0.745);
    ELEMENTS_BY_ATOMIC_NUMBER[21].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[21].new ShannonRadiusRecord("VIII",3,0,1.01,0.87);
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[22].new ShannonRadiusRecord("VI",2,0,1,0.86);
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[22].new ShannonRadiusRecord("VI",3,0,0.81,0.67);
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[22].new ShannonRadiusRecord("IV",4,0,0.56,0.42);
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[22].new ShannonRadiusRecord("V",4,0,0.65,0.51);
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[22].new ShannonRadiusRecord("VI",4,0,0.745,0.605);
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[22].new ShannonRadiusRecord("VIII",4,0,0.88,0.74);
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[23].new ShannonRadiusRecord("VI",2,0,0.93,0.79);
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[23].new ShannonRadiusRecord("VI",3,0,0.78,0.64);
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[23].new ShannonRadiusRecord("V",4,0,0.67,0.53);
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[23].new ShannonRadiusRecord("VI",4,0,0.72,0.58);
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[23].new ShannonRadiusRecord("VIII",4,0,0.86,0.72);
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[23].new ShannonRadiusRecord("IV",5,0,0.495,0.355);
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[23].new ShannonRadiusRecord("V",5,0,0.6,0.46);
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[23].new ShannonRadiusRecord("VI",5,0,0.68,0.54);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("VI",2,-1,0.87,0.73);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("VI",2,1,0.94,0.8);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("VI",3,0,0.755,0.615);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("IV",4,0,0.55,0.41);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("VI",4,0,0.69,0.55);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("IV",5,0,0.485,0.345);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("VI",5,0,0.63,0.49);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("VIII",5,0,0.71,0.57);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("IV",6,0,0.4,0.26);
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_ShannonRadii[9] = ELEMENTS_BY_ATOMIC_NUMBER[24].new ShannonRadiusRecord("VI",6,0,0.58,0.44);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("IV",2,1,0.8,0.66);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("V",2,1,0.89,0.75);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("VI",2,-1,0.81,0.67);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("VI",2,1,0.97,0.83);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("VII",2,1,1.04,0.9);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("VIII",2,0,1.1,0.96);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("V",3,0,0.72,0.58);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("VI",3,-1,0.72,0.58);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("VI",3,1,0.785,0.645);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[9] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("IV",4,0,0.53,0.39);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[10] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("VI",4,0,0.67,0.53);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[11] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("IV",5,0,0.47,0.33);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[12] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("IV",6,0,0.395,0.255);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[13] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("IV",7,0,0.39,0.25);
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_ShannonRadii[14] = ELEMENTS_BY_ATOMIC_NUMBER[25].new ShannonRadiusRecord("VI",7,0,0.6,0.46);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("IV",2,1,0.77,0.63);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("IVSQ",2,1,0.78,0.64);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("VI",2,-1,0.75,0.61);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("VI",2,1,0.92,0.78);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("VIII",2,1,1.06,0.92);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("IV",3,1,0.63,0.49);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("V",3,0,0.72,0.58);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("VI",3,-1,0.69,0.55);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("VI",3,1,0.785,0.645);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[9] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("VIII",3,1,0.92,0.78);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[10] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("VI",4,0,0.725,0.585);
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_ShannonRadii[11] = ELEMENTS_BY_ATOMIC_NUMBER[26].new ShannonRadiusRecord("IV",6,0,0.39,0.25);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("IV",2,1,0.72,0.58);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("V",2,0,0.81,0.67);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("VI",2,-1,0.79,0.65);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("VI",2,1,0.885,0.745);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("VIII",2,0,1.04,0.9);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("VI",3,-1,0.685,0.545);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("VI",3,1,0.75,0.61);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("IV",4,0,0.54,0.4);
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[27].new ShannonRadiusRecord("VI",4,1,0.67,0.53);
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[28].new ShannonRadiusRecord("IV",2,0,0.69,0.55);
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[28].new ShannonRadiusRecord("IVSQ",2,0,0.63,0.49);
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[28].new ShannonRadiusRecord("V",2,0,0.77,0.63);
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[28].new ShannonRadiusRecord("VI",2,0,0.83,0.69);
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[28].new ShannonRadiusRecord("VI",3,-1,0.7,0.56);
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[28].new ShannonRadiusRecord("VI",3,1,0.74,0.6);
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[28].new ShannonRadiusRecord("VI",4,-1,0.62,0.48);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("II",1,0,0.6,0.46);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("IV",1,0,0.74,0.6);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("VI",1,0,0.91,0.77);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("IV",2,0,0.71,0.57);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("IVSQ",2,0,0.71,0.57);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("V",2,0,0.79,0.65);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("VI",2,0,0.87,0.73);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("VI",3,-1,0.68,0.54);
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[29].new ShannonRadiusRecord("II",1,0,0.04,-0.1);
    ELEMENTS_BY_ATOMIC_NUMBER[30].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[30].new ShannonRadiusRecord("IV",2,0,0.74,0.6);
    ELEMENTS_BY_ATOMIC_NUMBER[30].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[30].new ShannonRadiusRecord("V",2,0,0.82,0.68);
    ELEMENTS_BY_ATOMIC_NUMBER[30].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[30].new ShannonRadiusRecord("VI",2,0,0.88,0.74);
    ELEMENTS_BY_ATOMIC_NUMBER[30].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[30].new ShannonRadiusRecord("VIII",2,0,1.04,0.9);
    ELEMENTS_BY_ATOMIC_NUMBER[31].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[31].new ShannonRadiusRecord("IV",3,0,0.61,0.47);
    ELEMENTS_BY_ATOMIC_NUMBER[31].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[31].new ShannonRadiusRecord("V",3,0,0.69,0.55);
    ELEMENTS_BY_ATOMIC_NUMBER[31].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[31].new ShannonRadiusRecord("VI",3,0,0.76,0.62);
    ELEMENTS_BY_ATOMIC_NUMBER[32].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[32].new ShannonRadiusRecord("VI",2,0,0.87,0.73);
    ELEMENTS_BY_ATOMIC_NUMBER[32].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[32].new ShannonRadiusRecord("IV",4,0,0.53,0.39);
    ELEMENTS_BY_ATOMIC_NUMBER[32].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[32].new ShannonRadiusRecord("VI",4,0,0.67,0.53);
    ELEMENTS_BY_ATOMIC_NUMBER[33].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[33].new ShannonRadiusRecord("VI",3,0,0.72,0.58);
    ELEMENTS_BY_ATOMIC_NUMBER[33].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[33].new ShannonRadiusRecord("IV",5,0,0.475,0.335);
    ELEMENTS_BY_ATOMIC_NUMBER[33].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[33].new ShannonRadiusRecord("VI",5,0,0.6,0.46);
    ELEMENTS_BY_ATOMIC_NUMBER[34].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[34].new ShannonRadiusRecord("VI",-2,0,1.84,1.98);
    ELEMENTS_BY_ATOMIC_NUMBER[34].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[34].new ShannonRadiusRecord("VI",4,0,0.64,0.5);
    ELEMENTS_BY_ATOMIC_NUMBER[34].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[34].new ShannonRadiusRecord("IV",6,0,0.42,0.28);
    ELEMENTS_BY_ATOMIC_NUMBER[34].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[34].new ShannonRadiusRecord("VI",6,0,0.56,0.42);
    ELEMENTS_BY_ATOMIC_NUMBER[35].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[35].new ShannonRadiusRecord("VI",-1,0,1.82,1.96);
    ELEMENTS_BY_ATOMIC_NUMBER[35].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[35].new ShannonRadiusRecord("IVSQ",3,0,0.73,0.59);
    ELEMENTS_BY_ATOMIC_NUMBER[35].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[35].new ShannonRadiusRecord("IIIPY",5,0,0.45,0.31);
    ELEMENTS_BY_ATOMIC_NUMBER[35].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[35].new ShannonRadiusRecord("IV",7,0,0.39,0.25);
    ELEMENTS_BY_ATOMIC_NUMBER[35].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[35].new ShannonRadiusRecord("VI",7,0,0.53,0.39);
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[37].new ShannonRadiusRecord("VI",1,0,1.66,1.52);
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[37].new ShannonRadiusRecord("VII",1,0,1.7,1.56);
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[37].new ShannonRadiusRecord("VIII",1,0,1.75,1.61);
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[37].new ShannonRadiusRecord("IX",1,0,1.77,1.63);
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[37].new ShannonRadiusRecord("X",1,0,1.8,1.66);
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[37].new ShannonRadiusRecord("XI",1,0,1.83,1.69);
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[37].new ShannonRadiusRecord("XII",1,0,1.86,1.72);
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[37].new ShannonRadiusRecord("XIV",1,0,1.97,1.83);
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[38].new ShannonRadiusRecord("VI",2,0,1.32,1.18);
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[38].new ShannonRadiusRecord("VII",2,0,1.35,1.21);
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[38].new ShannonRadiusRecord("VIII",2,0,1.4,1.26);
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[38].new ShannonRadiusRecord("IX",2,0,1.45,1.31);
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[38].new ShannonRadiusRecord("X",2,0,1.5,1.36);
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[38].new ShannonRadiusRecord("XII",2,0,1.58,1.44);
    ELEMENTS_BY_ATOMIC_NUMBER[39].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[39].new ShannonRadiusRecord("VI",3,0,1.04,0.9);
    ELEMENTS_BY_ATOMIC_NUMBER[39].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[39].new ShannonRadiusRecord("VII",3,0,1.1,0.96);
    ELEMENTS_BY_ATOMIC_NUMBER[39].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[39].new ShannonRadiusRecord("VIII",3,0,1.159,1.019);
    ELEMENTS_BY_ATOMIC_NUMBER[39].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[39].new ShannonRadiusRecord("IX",3,0,1.215,1.075);
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[40].new ShannonRadiusRecord("IV",4,0,0.73,0.59);
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[40].new ShannonRadiusRecord("V",4,0,0.8,0.66);
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[40].new ShannonRadiusRecord("VI",4,0,0.86,0.72);
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[40].new ShannonRadiusRecord("VII",4,0,0.92,0.78);
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[40].new ShannonRadiusRecord("VIII",4,0,0.98,0.84);
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[40].new ShannonRadiusRecord("IX",4,0,1.03,0.89);
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[41].new ShannonRadiusRecord("VI",3,0,0.86,0.72);
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[41].new ShannonRadiusRecord("VI",4,0,0.82,0.68);
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[41].new ShannonRadiusRecord("VIII",4,0,0.93,0.79);
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[41].new ShannonRadiusRecord("IV",5,0,0.62,0.48);
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[41].new ShannonRadiusRecord("VI",5,0,0.78,0.64);
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[41].new ShannonRadiusRecord("VII",5,0,0.83,0.69);
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[41].new ShannonRadiusRecord("VIII",5,0,0.88,0.74);
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[42].new ShannonRadiusRecord("VI",3,0,0.83,0.69);
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[42].new ShannonRadiusRecord("VI",4,0,0.79,0.65);
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[42].new ShannonRadiusRecord("IV",5,0,0.6,0.46);
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[42].new ShannonRadiusRecord("VI",5,0,0.75,0.61);
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[42].new ShannonRadiusRecord("IV",6,0,0.55,0.41);
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[42].new ShannonRadiusRecord("V",6,0,0.64,0.5);
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[42].new ShannonRadiusRecord("VI",6,0,0.73,0.59);
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[42].new ShannonRadiusRecord("VII",6,0,0.87,0.73);
    ELEMENTS_BY_ATOMIC_NUMBER[43].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[43].new ShannonRadiusRecord("VI",4,0,0.785,0.645);
    ELEMENTS_BY_ATOMIC_NUMBER[43].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[43].new ShannonRadiusRecord("VI",5,0,0.74,0.6);
    ELEMENTS_BY_ATOMIC_NUMBER[43].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[43].new ShannonRadiusRecord("IV",7,0,0.51,0.37);
    ELEMENTS_BY_ATOMIC_NUMBER[43].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[43].new ShannonRadiusRecord("VI",7,0,0.7,0.56);
    ELEMENTS_BY_ATOMIC_NUMBER[44].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[44].new ShannonRadiusRecord("VI",3,0,0.82,0.68);
    ELEMENTS_BY_ATOMIC_NUMBER[44].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[44].new ShannonRadiusRecord("VI",4,0,0.76,0.62);
    ELEMENTS_BY_ATOMIC_NUMBER[44].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[44].new ShannonRadiusRecord("VI",5,0,0.705,0.565);
    ELEMENTS_BY_ATOMIC_NUMBER[44].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[44].new ShannonRadiusRecord("IV",7,0,0.52,0.38);
    ELEMENTS_BY_ATOMIC_NUMBER[44].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[44].new ShannonRadiusRecord("IV",8,0,0.5,0.36);
    ELEMENTS_BY_ATOMIC_NUMBER[45].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[45].new ShannonRadiusRecord("VI",3,0,0.805,0.665);
    ELEMENTS_BY_ATOMIC_NUMBER[45].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[45].new ShannonRadiusRecord("VI",4,0,0.74,0.6);
    ELEMENTS_BY_ATOMIC_NUMBER[45].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[45].new ShannonRadiusRecord("VI",5,0,0.69,0.55);
    ELEMENTS_BY_ATOMIC_NUMBER[46].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[46].new ShannonRadiusRecord("II",1,0,0.73,0.59);
    ELEMENTS_BY_ATOMIC_NUMBER[46].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[46].new ShannonRadiusRecord("IVSQ",2,0,0.78,0.64);
    ELEMENTS_BY_ATOMIC_NUMBER[46].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[46].new ShannonRadiusRecord("VI",2,0,1,0.86);
    ELEMENTS_BY_ATOMIC_NUMBER[46].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[46].new ShannonRadiusRecord("VI",3,0,0.9,0.76);
    ELEMENTS_BY_ATOMIC_NUMBER[46].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[46].new ShannonRadiusRecord("VI",4,0,0.755,0.615);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("II",1,0,0.81,0.67);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("IV",1,0,1.14,1);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("IVSQ",1,0,1.16,1.02);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("V",1,0,1.23,1.09);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("VI",1,0,1.29,1.15);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("VII",1,0,1.36,1.22);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("VIII",1,0,1.42,1.28);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("IVSQ",2,0,0.93,0.79);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("VI",2,0,1.08,0.94);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[9] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("IVSQ",3,0,0.81,0.67);
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_ShannonRadii[10] = ELEMENTS_BY_ATOMIC_NUMBER[47].new ShannonRadiusRecord("VI",3,0,0.89,0.75);
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[48].new ShannonRadiusRecord("IV",2,0,0.92,0.78);
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[48].new ShannonRadiusRecord("V",2,0,1.01,0.87);
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[48].new ShannonRadiusRecord("VI",2,0,1.09,0.95);
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[48].new ShannonRadiusRecord("VII",2,0,1.17,1.03);
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[48].new ShannonRadiusRecord("VIII",2,0,1.24,1.1);
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[48].new ShannonRadiusRecord("XII",2,0,1.45,1.31);
    ELEMENTS_BY_ATOMIC_NUMBER[49].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[49].new ShannonRadiusRecord("IV",3,0,0.76,0.62);
    ELEMENTS_BY_ATOMIC_NUMBER[49].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[49].new ShannonRadiusRecord("VI",3,0,0.94,0.8);
    ELEMENTS_BY_ATOMIC_NUMBER[49].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[49].new ShannonRadiusRecord("VIII",3,0,1.06,0.92);
    ELEMENTS_BY_ATOMIC_NUMBER[50].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[50].new ShannonRadiusRecord("IV",4,0,0.69,0.55);
    ELEMENTS_BY_ATOMIC_NUMBER[50].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[50].new ShannonRadiusRecord("V",4,0,0.76,0.62);
    ELEMENTS_BY_ATOMIC_NUMBER[50].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[50].new ShannonRadiusRecord("VI",4,0,0.83,0.69);
    ELEMENTS_BY_ATOMIC_NUMBER[50].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[50].new ShannonRadiusRecord("VII",4,0,0.89,0.75);
    ELEMENTS_BY_ATOMIC_NUMBER[50].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[50].new ShannonRadiusRecord("VIII",4,0,0.95,0.81);
    ELEMENTS_BY_ATOMIC_NUMBER[51].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[51].new ShannonRadiusRecord("IVPY",3,0,0.9,0.76);
    ELEMENTS_BY_ATOMIC_NUMBER[51].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[51].new ShannonRadiusRecord("V",3,0,0.94,0.8);
    ELEMENTS_BY_ATOMIC_NUMBER[51].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[51].new ShannonRadiusRecord("VI",3,0,0.9,0.76);
    ELEMENTS_BY_ATOMIC_NUMBER[51].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[51].new ShannonRadiusRecord("VI",5,0,0.74,0.6);
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[52].new ShannonRadiusRecord("VI",-2,0,2.07,2.21);
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[52].new ShannonRadiusRecord("III",4,0,0.66,0.52);
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[52].new ShannonRadiusRecord("IV",4,0,0.8,0.66);
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[52].new ShannonRadiusRecord("VI",4,0,1.11,0.97);
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[52].new ShannonRadiusRecord("IV",6,0,0.57,0.43);
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[52].new ShannonRadiusRecord("VI",6,0,0.7,0.56);
    ELEMENTS_BY_ATOMIC_NUMBER[53].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[53].new ShannonRadiusRecord("VI",-1,0,2.06,2.2);
    ELEMENTS_BY_ATOMIC_NUMBER[53].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[53].new ShannonRadiusRecord("IIIPY",5,0,0.58,0.44);
    ELEMENTS_BY_ATOMIC_NUMBER[53].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[53].new ShannonRadiusRecord("VI",5,0,1.09,0.95);
    ELEMENTS_BY_ATOMIC_NUMBER[53].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[53].new ShannonRadiusRecord("IV",7,0,0.56,0.42);
    ELEMENTS_BY_ATOMIC_NUMBER[53].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[53].new ShannonRadiusRecord("VI",7,0,0.67,0.53);
    ELEMENTS_BY_ATOMIC_NUMBER[54].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[54].new ShannonRadiusRecord("IV",8,0,0.54,0.4);
    ELEMENTS_BY_ATOMIC_NUMBER[54].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[54].new ShannonRadiusRecord("VI",8,0,0.62,0.48);
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[55].new ShannonRadiusRecord("VI",1,0,1.81,1.67);
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[55].new ShannonRadiusRecord("VIII",1,0,1.88,1.74);
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[55].new ShannonRadiusRecord("IX",1,0,1.92,1.78);
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[55].new ShannonRadiusRecord("X",1,0,1.95,1.81);
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[55].new ShannonRadiusRecord("XI",1,0,1.99,1.85);
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[55].new ShannonRadiusRecord("XII",1,0,2.02,1.88);
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[56].new ShannonRadiusRecord("VI",2,0,1.49,1.35);
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[56].new ShannonRadiusRecord("VII",2,0,1.52,1.38);
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[56].new ShannonRadiusRecord("VIII",2,0,1.56,1.42);
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[56].new ShannonRadiusRecord("IX",2,0,1.61,1.47);
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[56].new ShannonRadiusRecord("X",2,0,1.66,1.52);
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[56].new ShannonRadiusRecord("XI",2,0,1.71,1.57);
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[56].new ShannonRadiusRecord("XII",2,0,1.75,1.61);
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[57].new ShannonRadiusRecord("VI",3,0,1.172,1.032);
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[57].new ShannonRadiusRecord("VII",3,0,1.24,1.1);
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[57].new ShannonRadiusRecord("VIII",3,0,1.3,1.16);
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[57].new ShannonRadiusRecord("IX",3,0,1.356,1.216);
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[57].new ShannonRadiusRecord("X",3,0,1.41,1.27);
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[57].new ShannonRadiusRecord("XII",3,0,1.5,1.36);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("VI",3,0,1.15,1.01);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("VII",3,0,1.21,1.07);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("VIII",3,0,1.283,1.143);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("IX",3,0,1.336,1.196);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("X",3,0,1.39,1.25);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("XII",3,0,1.48,1.34);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("VI",4,0,1.01,0.87);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("VIII",4,0,1.11,0.97);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("X",4,0,1.21,1.07);
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_ShannonRadii[9] = ELEMENTS_BY_ATOMIC_NUMBER[58].new ShannonRadiusRecord("XII",4,0,1.28,1.14);
    ELEMENTS_BY_ATOMIC_NUMBER[59].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[59].new ShannonRadiusRecord("VI",3,0,1.13,0.99);
    ELEMENTS_BY_ATOMIC_NUMBER[59].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[59].new ShannonRadiusRecord("VIII",3,0,1.266,1.126);
    ELEMENTS_BY_ATOMIC_NUMBER[59].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[59].new ShannonRadiusRecord("IX",3,0,1.319,1.179);
    ELEMENTS_BY_ATOMIC_NUMBER[59].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[59].new ShannonRadiusRecord("VI",4,0,0.99,0.85);
    ELEMENTS_BY_ATOMIC_NUMBER[59].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[59].new ShannonRadiusRecord("VIII",4,0,1.1,0.96);
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[60].new ShannonRadiusRecord("VIII",2,0,1.43,1.29);
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[60].new ShannonRadiusRecord("IX",2,0,1.49,1.35);
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[60].new ShannonRadiusRecord("VI",3,0,1.123,0.983);
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[60].new ShannonRadiusRecord("VIII",3,0,1.249,1.109);
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[60].new ShannonRadiusRecord("IX",3,0,1.303,1.163);
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[60].new ShannonRadiusRecord("XII",3,0,1.41,1.27);
    ELEMENTS_BY_ATOMIC_NUMBER[61].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[61].new ShannonRadiusRecord("VI",3,0,1.11,0.97);
    ELEMENTS_BY_ATOMIC_NUMBER[61].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[61].new ShannonRadiusRecord("VIII",3,0,1.233,1.093);
    ELEMENTS_BY_ATOMIC_NUMBER[61].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[61].new ShannonRadiusRecord("IX",3,0,1.284,1.144);
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[62].new ShannonRadiusRecord("VII",2,0,1.36,1.22);
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[62].new ShannonRadiusRecord("VIII",2,0,1.41,1.27);
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[62].new ShannonRadiusRecord("IX",2,0,1.46,1.32);
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[62].new ShannonRadiusRecord("VI",3,0,1.098,0.958);
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[62].new ShannonRadiusRecord("VII",3,0,1.16,1.02);
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[62].new ShannonRadiusRecord("VIII",3,0,1.219,1.079);
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[62].new ShannonRadiusRecord("IX",3,0,1.272,1.132);
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[62].new ShannonRadiusRecord("XII",3,0,1.38,1.24);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("VI",2,0,1.31,1.17);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("VII",2,0,1.34,1.2);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("VIII",2,0,1.39,1.25);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("IX",2,0,1.44,1.3);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("X",2,0,1.49,1.35);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("VI",3,0,1.087,0.947);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("VII",3,0,1.15,1.01);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("VIII",3,0,1.206,1.066);
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[63].new ShannonRadiusRecord("IX",3,0,1.26,1.12);
    ELEMENTS_BY_ATOMIC_NUMBER[64].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[64].new ShannonRadiusRecord("VI",3,0,1.078,0.938);
    ELEMENTS_BY_ATOMIC_NUMBER[64].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[64].new ShannonRadiusRecord("VII",3,0,1.14,1);
    ELEMENTS_BY_ATOMIC_NUMBER[64].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[64].new ShannonRadiusRecord("VIII",3,0,1.193,1.053);
    ELEMENTS_BY_ATOMIC_NUMBER[64].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[64].new ShannonRadiusRecord("IX",3,0,1.247,1.107);
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[65].new ShannonRadiusRecord("VI",3,0,1.063,0.923);
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[65].new ShannonRadiusRecord("VII",3,05,1.12,0.98);
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[65].new ShannonRadiusRecord("VIII",3,0,1.18,1.04);
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[65].new ShannonRadiusRecord("IX",3,0,1.235,1.095);
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[65].new ShannonRadiusRecord("VI",4,0,0.9,0.76);
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[65].new ShannonRadiusRecord("VIII",4,0,1.02,0.88);
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[66].new ShannonRadiusRecord("VI",2,0,51.21,1.07);
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[66].new ShannonRadiusRecord("VII",2,0,1.27,1.13);
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[66].new ShannonRadiusRecord("VIII",2,0,1.33,1.19);
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[66].new ShannonRadiusRecord("VI",3,0,1.052,0.912);
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[66].new ShannonRadiusRecord("VII",3,0,1.11,0.97);
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[66].new ShannonRadiusRecord("VIII",3,0,1.167,1.027);
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[66].new ShannonRadiusRecord("IX",3,0,1.223,1.083);
    ELEMENTS_BY_ATOMIC_NUMBER[67].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[67].new ShannonRadiusRecord("VI",3,0,1.041,0.901);
    ELEMENTS_BY_ATOMIC_NUMBER[67].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[67].new ShannonRadiusRecord("VIII",3,0,1.155,1.015);
    ELEMENTS_BY_ATOMIC_NUMBER[67].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[67].new ShannonRadiusRecord("IX",3,0,1.212,1.072);
    ELEMENTS_BY_ATOMIC_NUMBER[67].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[67].new ShannonRadiusRecord("X",3,0,1.26,1.12);
    ELEMENTS_BY_ATOMIC_NUMBER[68].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[68].new ShannonRadiusRecord("VI",3,0,1.03,0.89);
    ELEMENTS_BY_ATOMIC_NUMBER[68].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[68].new ShannonRadiusRecord("VII",3,0,1.085,0.945);
    ELEMENTS_BY_ATOMIC_NUMBER[68].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[68].new ShannonRadiusRecord("VIII",3,0,1.144,1.004);
    ELEMENTS_BY_ATOMIC_NUMBER[68].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[68].new ShannonRadiusRecord("IX",3,0,1.202,1.062);
    ELEMENTS_BY_ATOMIC_NUMBER[69].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[69].new ShannonRadiusRecord("VI",2,0,1.17,1.03);
    ELEMENTS_BY_ATOMIC_NUMBER[69].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[69].new ShannonRadiusRecord("VII",2,0,1.23,1.09);
    ELEMENTS_BY_ATOMIC_NUMBER[69].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[69].new ShannonRadiusRecord("VI",3,0,1.02,0.88);
    ELEMENTS_BY_ATOMIC_NUMBER[69].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[69].new ShannonRadiusRecord("VIII",3,0,1.134,0.994);
    ELEMENTS_BY_ATOMIC_NUMBER[69].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[69].new ShannonRadiusRecord("IX",3,0,1.192,1.052);
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[70].new ShannonRadiusRecord("VI",2,0,1.16,1.02);
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[70].new ShannonRadiusRecord("VII",2,0,1.22,1.08);
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[70].new ShannonRadiusRecord("VIII",2,0,1.28,1.14);
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[70].new ShannonRadiusRecord("VI",3,0,1.008,0.868);
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[70].new ShannonRadiusRecord("VII",3,0,1.065,0.925);
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[70].new ShannonRadiusRecord("VIII",3,0,1.125,0.985);
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[70].new ShannonRadiusRecord("IX",3,0,1.182,1.042);
    ELEMENTS_BY_ATOMIC_NUMBER[71].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[71].new ShannonRadiusRecord("VI",3,0,1.001,0.861);
    ELEMENTS_BY_ATOMIC_NUMBER[71].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[71].new ShannonRadiusRecord("VIII",3,0,1.117,0.977);
    ELEMENTS_BY_ATOMIC_NUMBER[71].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[71].new ShannonRadiusRecord("IX",3,0,1.172,1.032);
    ELEMENTS_BY_ATOMIC_NUMBER[72].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[72].new ShannonRadiusRecord("IV",4,0,0.72,0.58);
    ELEMENTS_BY_ATOMIC_NUMBER[72].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[72].new ShannonRadiusRecord("VI",4,0,0.85,0.71);
    ELEMENTS_BY_ATOMIC_NUMBER[72].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[72].new ShannonRadiusRecord("VII",4,0,0.9,0.76);
    ELEMENTS_BY_ATOMIC_NUMBER[72].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[72].new ShannonRadiusRecord("VIII",4,0,0.97,0.83);
    ELEMENTS_BY_ATOMIC_NUMBER[73].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[73].new ShannonRadiusRecord("VI",3,0,0.86,0.72);
    ELEMENTS_BY_ATOMIC_NUMBER[73].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[73].new ShannonRadiusRecord("VI",4,0,0.82,0.68);
    ELEMENTS_BY_ATOMIC_NUMBER[73].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[73].new ShannonRadiusRecord("VI",5,0,0.78,0.64);
    ELEMENTS_BY_ATOMIC_NUMBER[73].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[73].new ShannonRadiusRecord("VII",5,0,0.83,0.69);
    ELEMENTS_BY_ATOMIC_NUMBER[73].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[73].new ShannonRadiusRecord("VIII",5,0,0.88,0.74);
    ELEMENTS_BY_ATOMIC_NUMBER[74].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[74].new ShannonRadiusRecord("VI",4,0,0.8,0.66);
    ELEMENTS_BY_ATOMIC_NUMBER[74].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[74].new ShannonRadiusRecord("VI",5,0,0.76,0.62);
    ELEMENTS_BY_ATOMIC_NUMBER[74].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[74].new ShannonRadiusRecord("IV",6,0,0.56,0.42);
    ELEMENTS_BY_ATOMIC_NUMBER[74].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[74].new ShannonRadiusRecord("V",6,0,0.65,0.51);
    ELEMENTS_BY_ATOMIC_NUMBER[74].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[74].new ShannonRadiusRecord("VI",6,0,0.74,0.6);
    ELEMENTS_BY_ATOMIC_NUMBER[75].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[75].new ShannonRadiusRecord("VI",4,0,0.77,0.63);
    ELEMENTS_BY_ATOMIC_NUMBER[75].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[75].new ShannonRadiusRecord("VI",5,0,0.72,0.58);
    ELEMENTS_BY_ATOMIC_NUMBER[75].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[75].new ShannonRadiusRecord("VI",6,0,0.69,0.55);
    ELEMENTS_BY_ATOMIC_NUMBER[75].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[75].new ShannonRadiusRecord("IV",7,0,0.52,0.38);
    ELEMENTS_BY_ATOMIC_NUMBER[75].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[75].new ShannonRadiusRecord("VI",7,0,0.67,0.53);
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[76].new ShannonRadiusRecord("VI",4,0,0.77,0.63);
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[76].new ShannonRadiusRecord("VI",5,0,0.715,0.575);
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[76].new ShannonRadiusRecord("V",6,0,0.63,0.49);
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[76].new ShannonRadiusRecord("VI",6,0,0.685,0.545);
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[76].new ShannonRadiusRecord("VI",7,0,0.665,0.525);
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[76].new ShannonRadiusRecord("IV",8,0,0.53,0.39);
    ELEMENTS_BY_ATOMIC_NUMBER[77].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[77].new ShannonRadiusRecord("VI",3,0,0.82,0.68);
    ELEMENTS_BY_ATOMIC_NUMBER[77].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[77].new ShannonRadiusRecord("VI",4,0,0.765,0.625);
    ELEMENTS_BY_ATOMIC_NUMBER[77].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[77].new ShannonRadiusRecord("VI",5,0,0.71,0.57);
    ELEMENTS_BY_ATOMIC_NUMBER[78].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[78].new ShannonRadiusRecord("IVSQ",2,0,0.74,0.6);
    ELEMENTS_BY_ATOMIC_NUMBER[78].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[78].new ShannonRadiusRecord("VI",2,0,0.94,0.8);
    ELEMENTS_BY_ATOMIC_NUMBER[78].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[78].new ShannonRadiusRecord("VI",4,0,0.765,0.625);
    ELEMENTS_BY_ATOMIC_NUMBER[78].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[78].new ShannonRadiusRecord("VI",5,0,0.71,0.57);
    ELEMENTS_BY_ATOMIC_NUMBER[79].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[79].new ShannonRadiusRecord("VI",1,0,1.51,1.37);
    ELEMENTS_BY_ATOMIC_NUMBER[79].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[79].new ShannonRadiusRecord("IVSQ",3,0,0.82,0.68);
    ELEMENTS_BY_ATOMIC_NUMBER[79].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[79].new ShannonRadiusRecord("VI",3,0,0.99,0.85);
    ELEMENTS_BY_ATOMIC_NUMBER[79].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[79].new ShannonRadiusRecord("VI",5,0,0.71,0.57);
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[80].new ShannonRadiusRecord("III",1,0,1.11,0.97);
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[80].new ShannonRadiusRecord("VI",1,0,1.33,1.19);
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[80].new ShannonRadiusRecord("II",2,0,0.83,0.69);
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[80].new ShannonRadiusRecord("IV",2,0,1.1,0.96);
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[80].new ShannonRadiusRecord("VI",2,0,1.16,1.02);
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[80].new ShannonRadiusRecord("VIII",2,0,1.28,1.14);
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[81].new ShannonRadiusRecord("VI",1,0,1.64,1.5);
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[81].new ShannonRadiusRecord("VIII",1,0,1.73,1.59);
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[81].new ShannonRadiusRecord("XII",1,0,1.84,1.7);
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[81].new ShannonRadiusRecord("IV",3,0,0.89,0.75);
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[81].new ShannonRadiusRecord("VI",3,0,1.025,0.885);
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[81].new ShannonRadiusRecord("VIII",3,0,1.12,0.98);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("IVPY",2,0,1.12,0.98);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("VI",2,0,1.33,1.19);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("VII",2,0,1.37,1.23);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("VIII",2,0,1.43,1.29);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("IX",2,0,1.49,1.35);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("X",2,0,1.54,1.4);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("XI",2,0,1.59,1.45);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("XII",2,0,1.63,1.49);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("IV",4,0,0.79,0.65);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[9] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("V",4,0,0.87,0.73);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[10] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("VI",4,0,0.915,0.775);
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_ShannonRadii[11] = ELEMENTS_BY_ATOMIC_NUMBER[82].new ShannonRadiusRecord("VIII",4,0,1.08,0.94);
    ELEMENTS_BY_ATOMIC_NUMBER[83].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[83].new ShannonRadiusRecord("V",3,0,1.1,0.96);
    ELEMENTS_BY_ATOMIC_NUMBER[83].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[83].new ShannonRadiusRecord("VI",3,0,1.17,1.03);
    ELEMENTS_BY_ATOMIC_NUMBER[83].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[83].new ShannonRadiusRecord("VIII",3,0,1.31,1.17);
    ELEMENTS_BY_ATOMIC_NUMBER[83].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[83].new ShannonRadiusRecord("VI",5,0,0.9,0.76);
    ELEMENTS_BY_ATOMIC_NUMBER[84].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[84].new ShannonRadiusRecord("VI",4,0,1.08,0.94);
    ELEMENTS_BY_ATOMIC_NUMBER[84].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[84].new ShannonRadiusRecord("VIII",4,0,1.22,1.08);
    ELEMENTS_BY_ATOMIC_NUMBER[84].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[84].new ShannonRadiusRecord("VI",6,0,0.81,0.67);
    ELEMENTS_BY_ATOMIC_NUMBER[85].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[85].new ShannonRadiusRecord("VI",7,0,0.76,0.62);
    ELEMENTS_BY_ATOMIC_NUMBER[87].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[87].new ShannonRadiusRecord("VI",1,0,1.94,1.8);
    ELEMENTS_BY_ATOMIC_NUMBER[88].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[88].new ShannonRadiusRecord("VIII",2,0,1.62,1.48);
    ELEMENTS_BY_ATOMIC_NUMBER[88].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[88].new ShannonRadiusRecord("XII",2,0,1.84,1.7);
    ELEMENTS_BY_ATOMIC_NUMBER[89].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[89].new ShannonRadiusRecord("VI",3,0,1.26,1.12);
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[90].new ShannonRadiusRecord("VI",4,0,1.08,0.94);
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[90].new ShannonRadiusRecord("VIII",4,0,1.19,1.05);
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[90].new ShannonRadiusRecord("IX",4,0,1.23,1.09);
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[90].new ShannonRadiusRecord("X",4,0,1.27,1.13);
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[90].new ShannonRadiusRecord("XI",4,0,1.32,1.18);
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[90].new ShannonRadiusRecord("XII",4,0,1.35,1.21);
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[91].new ShannonRadiusRecord("VI",3,0,1.18,1.04);
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[91].new ShannonRadiusRecord("VI",4,0,1.04,0.9);
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[91].new ShannonRadiusRecord("VIII",4,0,1.15,1.01);
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[91].new ShannonRadiusRecord("VI",5,0,0.92,0.78);
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[91].new ShannonRadiusRecord("VIII",5,0,1.05,0.91);
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[91].new ShannonRadiusRecord("IX",5,0,1.09,0.95);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VI",3,0,1.165,1.025);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VI",4,0,1.03,0.89);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VII",4,0,1.09,0.95);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VIII",4,0,1.14,1);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("IX",4,0,1.19,1.05);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("XII",4,0,1.31,1.17);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VI",5,0,0.9,0.76);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[7] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VII",5,0,0.98,0.84);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[8] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("II",6,0,0.59,0.45);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[9] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("IV",6,0,0.66,0.52);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[10] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VI",6,0,0.87,0.73);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[11] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VII",6,0,0.95,0.81);
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_ShannonRadii[12] = ELEMENTS_BY_ATOMIC_NUMBER[92].new ShannonRadiusRecord("VIII",6,0,1,0.86);
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[93].new ShannonRadiusRecord("VI",2,0,1.24,1.1);
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[93].new ShannonRadiusRecord("VI",3,0,1.15,1.01);
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[93].new ShannonRadiusRecord("VI",4,0,1.01,0.87);
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[93].new ShannonRadiusRecord("VIII",4,0,1.12,0.98);
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[93].new ShannonRadiusRecord("VI",5,0,0.89,0.75);
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[93].new ShannonRadiusRecord("VI",6,0,0.86,0.72);
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[93].new ShannonRadiusRecord("VI",7,0,0.85,0.71);
    ELEMENTS_BY_ATOMIC_NUMBER[94].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[94].new ShannonRadiusRecord("VI",3,0,1.14,1);
    ELEMENTS_BY_ATOMIC_NUMBER[94].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[94].new ShannonRadiusRecord("VI",4,0,1,0.86);
    ELEMENTS_BY_ATOMIC_NUMBER[94].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[94].new ShannonRadiusRecord("VIII",4,0,1.1,0.96);
    ELEMENTS_BY_ATOMIC_NUMBER[94].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[94].new ShannonRadiusRecord("VI",5,0,0.88,0.74);
    ELEMENTS_BY_ATOMIC_NUMBER[94].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[94].new ShannonRadiusRecord("VI",6,0,0.85,0.71);
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[95].new ShannonRadiusRecord("VII",2,0,1.35,1.21);
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[95].new ShannonRadiusRecord("VIII",2,0,1.4,1.26);
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[95].new ShannonRadiusRecord("IX",2,0,1.45,1.31);
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_ShannonRadii[3] = ELEMENTS_BY_ATOMIC_NUMBER[95].new ShannonRadiusRecord("VI",3,0,1.115,0.975);
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_ShannonRadii[4] = ELEMENTS_BY_ATOMIC_NUMBER[95].new ShannonRadiusRecord("VIII",3,0,1.23,1.09);
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_ShannonRadii[5] = ELEMENTS_BY_ATOMIC_NUMBER[95].new ShannonRadiusRecord("VI",4,0,0.99,0.85);
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_ShannonRadii[6] = ELEMENTS_BY_ATOMIC_NUMBER[95].new ShannonRadiusRecord("VIII",4,0,1.09,0.95);
    ELEMENTS_BY_ATOMIC_NUMBER[96].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[96].new ShannonRadiusRecord("VI",3,0,1.11,0.97);
    ELEMENTS_BY_ATOMIC_NUMBER[96].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[96].new ShannonRadiusRecord("VI",4,0,0.99,0.85);
    ELEMENTS_BY_ATOMIC_NUMBER[96].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[96].new ShannonRadiusRecord("VIII",4,0,1.09,0.95);
    ELEMENTS_BY_ATOMIC_NUMBER[97].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[97].new ShannonRadiusRecord("VI",3,0,1.1,0.96);
    ELEMENTS_BY_ATOMIC_NUMBER[97].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[97].new ShannonRadiusRecord("VI",4,0,0.97,0.83);
    ELEMENTS_BY_ATOMIC_NUMBER[97].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[97].new ShannonRadiusRecord("VIII",4,0,1.07,0.93);
    ELEMENTS_BY_ATOMIC_NUMBER[98].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[98].new ShannonRadiusRecord("VI",3,0,1.09,0.95);
    ELEMENTS_BY_ATOMIC_NUMBER[98].m_ShannonRadii[1] = ELEMENTS_BY_ATOMIC_NUMBER[98].new ShannonRadiusRecord("VI",4,0,0.961,0.821);
    ELEMENTS_BY_ATOMIC_NUMBER[98].m_ShannonRadii[2] = ELEMENTS_BY_ATOMIC_NUMBER[98].new ShannonRadiusRecord("VIII",4,0,1.06,0.92);
    ELEMENTS_BY_ATOMIC_NUMBER[102].m_ShannonRadii[0] = ELEMENTS_BY_ATOMIC_NUMBER[102].new ShannonRadiusRecord("VI",2,0,1.24,1.1);
  }
  
  static { // From http://en.wikipedia.org/wiki/List_of_elements, Feb 19 2010, based on IUPAC where possible
    ELEMENTS_BY_ATOMIC_NUMBER[1].m_MeltingPointK = 14.175; // Hydrogen
    ELEMENTS_BY_ATOMIC_NUMBER[3].m_MeltingPointK = 453.85; // Lithium
    ELEMENTS_BY_ATOMIC_NUMBER[4].m_MeltingPointK = 1560.15; // Beryllium
    ELEMENTS_BY_ATOMIC_NUMBER[5].m_MeltingPointK = 2573.15; // Boron
    ELEMENTS_BY_ATOMIC_NUMBER[6].m_MeltingPointK = 3948.15; // Carbon
    ELEMENTS_BY_ATOMIC_NUMBER[7].m_MeltingPointK = 63.29; // Nitrogen
    ELEMENTS_BY_ATOMIC_NUMBER[8].m_MeltingPointK = 50.5; // Oxygen
    ELEMENTS_BY_ATOMIC_NUMBER[9].m_MeltingPointK = 53.63; // Fluorine
    ELEMENTS_BY_ATOMIC_NUMBER[10].m_MeltingPointK = 24.703; // Neon
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_MeltingPointK = 371.15; // Sodium
    ELEMENTS_BY_ATOMIC_NUMBER[12].m_MeltingPointK = 923.15; // Magnesium
    ELEMENTS_BY_ATOMIC_NUMBER[13].m_MeltingPointK = 933.4; // Aluminium
    ELEMENTS_BY_ATOMIC_NUMBER[14].m_MeltingPointK = 1683.15; // Silicon
    ELEMENTS_BY_ATOMIC_NUMBER[15].m_MeltingPointK = 317.25; // Phosphorus
    ELEMENTS_BY_ATOMIC_NUMBER[16].m_MeltingPointK = 388.51; // Sulfur
    ELEMENTS_BY_ATOMIC_NUMBER[17].m_MeltingPointK = 172.31; // Chlorine
    ELEMENTS_BY_ATOMIC_NUMBER[18].m_MeltingPointK = 83.96; // Argon
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_MeltingPointK = 336.5; // Potassium
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_MeltingPointK = 1112.15; // Calcium
    ELEMENTS_BY_ATOMIC_NUMBER[21].m_MeltingPointK = 1812.15; // Scandium
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_MeltingPointK = 1933.15; // Titanium
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_MeltingPointK = 2175.15; // Vanadium
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_MeltingPointK = 2130.15; // Chromium
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_MeltingPointK = 1519.15; // Manganese
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_MeltingPointK = 1808.15; // Iron
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_MeltingPointK = 1768.15; // Cobalt
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_MeltingPointK = 1726.15; // Nickel
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_MeltingPointK = 1357.75; // Copper
    ELEMENTS_BY_ATOMIC_NUMBER[30].m_MeltingPointK = 692.88; // Zinc
    ELEMENTS_BY_ATOMIC_NUMBER[31].m_MeltingPointK = 302.91; // Gallium
    ELEMENTS_BY_ATOMIC_NUMBER[32].m_MeltingPointK = 1211.45; // Germanium
    ELEMENTS_BY_ATOMIC_NUMBER[33].m_MeltingPointK = 1090.15; // Arsenic
    ELEMENTS_BY_ATOMIC_NUMBER[34].m_MeltingPointK = 494.15; // Selenium
    ELEMENTS_BY_ATOMIC_NUMBER[35].m_MeltingPointK = 266.05; // Bromine
    ELEMENTS_BY_ATOMIC_NUMBER[36].m_MeltingPointK = 115.93; // Krypton
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_MeltingPointK = 312.79; // Rubidium
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_MeltingPointK = 1042.15; // Strontium
    ELEMENTS_BY_ATOMIC_NUMBER[39].m_MeltingPointK = 1799.15; // Yttrium
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_MeltingPointK = 2125.15; // Zirconium
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_MeltingPointK = 2741.15; // Niobium
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_MeltingPointK = 2890.15; // Molybdenum
    ELEMENTS_BY_ATOMIC_NUMBER[43].m_MeltingPointK = 2473.15; // Technetium
    ELEMENTS_BY_ATOMIC_NUMBER[44].m_MeltingPointK = 2523.15; // Ruthenium
    ELEMENTS_BY_ATOMIC_NUMBER[45].m_MeltingPointK = 2239.15; // Rhodium
    ELEMENTS_BY_ATOMIC_NUMBER[46].m_MeltingPointK = 1825.15; // Palladium
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_MeltingPointK = 1234.15; // Silver
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_MeltingPointK = 594.33; // Cadmium
    ELEMENTS_BY_ATOMIC_NUMBER[49].m_MeltingPointK = 429.91; // Indium
    ELEMENTS_BY_ATOMIC_NUMBER[50].m_MeltingPointK = 505.21; // Tin
    ELEMENTS_BY_ATOMIC_NUMBER[51].m_MeltingPointK = 904.05; // Antimony
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_MeltingPointK = 722.8; // Tellurium
    ELEMENTS_BY_ATOMIC_NUMBER[53].m_MeltingPointK = 386.65; // Iodine
    ELEMENTS_BY_ATOMIC_NUMBER[54].m_MeltingPointK = 161.45; // Xenon
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_MeltingPointK = 301.7; // Caesium
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_MeltingPointK = 1002.15; // Barium
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_MeltingPointK = 1193.15; // Lanthanum
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_MeltingPointK = 1071.15; // Cerium
    ELEMENTS_BY_ATOMIC_NUMBER[59].m_MeltingPointK = 1204.15; // Praseodymium
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_MeltingPointK = 1289.15; // Neodymium
    ELEMENTS_BY_ATOMIC_NUMBER[61].m_MeltingPointK = 1204.15; // Promethium
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_MeltingPointK = 1345.15; // Samarium
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_MeltingPointK = 1095.15; // Europium
    ELEMENTS_BY_ATOMIC_NUMBER[64].m_MeltingPointK = 1585.15; // Gadolinium
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_MeltingPointK = 1630.15; // Terbium
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_MeltingPointK = 1680.15; // Dysprosium
    ELEMENTS_BY_ATOMIC_NUMBER[67].m_MeltingPointK = 1743.15; // Holmium
    ELEMENTS_BY_ATOMIC_NUMBER[68].m_MeltingPointK = 1795.15; // Erbium
    ELEMENTS_BY_ATOMIC_NUMBER[69].m_MeltingPointK = 1818.15; // Thulium
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_MeltingPointK = 1097.15; // Ytterbium
    ELEMENTS_BY_ATOMIC_NUMBER[71].m_MeltingPointK = 1936.15; // Lutetium
    ELEMENTS_BY_ATOMIC_NUMBER[72].m_MeltingPointK = 2500.15; // Hafnium
    ELEMENTS_BY_ATOMIC_NUMBER[73].m_MeltingPointK = 3269.15; // Tantalum
    ELEMENTS_BY_ATOMIC_NUMBER[74].m_MeltingPointK = 3680.15; // Tungsten
    ELEMENTS_BY_ATOMIC_NUMBER[75].m_MeltingPointK = 3453.15; // Rhenium
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_MeltingPointK = 3300.15; // Osmium
    ELEMENTS_BY_ATOMIC_NUMBER[77].m_MeltingPointK = 2716.15; // Iridium
    ELEMENTS_BY_ATOMIC_NUMBER[78].m_MeltingPointK = 2045.15; // Platinum
    ELEMENTS_BY_ATOMIC_NUMBER[79].m_MeltingPointK = 1337.73; // Gold
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_MeltingPointK = 234.43; // Mercury
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_MeltingPointK = 577.15; // Thallium
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_MeltingPointK = 600.75; // Lead
    ELEMENTS_BY_ATOMIC_NUMBER[83].m_MeltingPointK = 544.67; // Bismuth
    ELEMENTS_BY_ATOMIC_NUMBER[84].m_MeltingPointK = 527.15; // Polonium
    ELEMENTS_BY_ATOMIC_NUMBER[85].m_MeltingPointK = 575.15; // Astatine
    ELEMENTS_BY_ATOMIC_NUMBER[86].m_MeltingPointK = 202.15; // Radon
    ELEMENTS_BY_ATOMIC_NUMBER[87].m_MeltingPointK = 300.15; // Francium
    ELEMENTS_BY_ATOMIC_NUMBER[88].m_MeltingPointK = 973.15; // Radium
    ELEMENTS_BY_ATOMIC_NUMBER[89].m_MeltingPointK = 1323.15; // Actinium
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_MeltingPointK = 2028.15; // Thorium
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_MeltingPointK = 1873.15; // Protactinium
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_MeltingPointK = 1405.15; // Uranium
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_MeltingPointK = 913.15; // Neptunium
    ELEMENTS_BY_ATOMIC_NUMBER[94].m_MeltingPointK = 913.15; // Plutonium
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_MeltingPointK = 1267.15; // Americium
    ELEMENTS_BY_ATOMIC_NUMBER[96].m_MeltingPointK = 1340.15; // Curium
    ELEMENTS_BY_ATOMIC_NUMBER[97].m_MeltingPointK = 1259.15; // Berkelium
    ELEMENTS_BY_ATOMIC_NUMBER[98].m_MeltingPointK = 1925.15; // Californium
    ELEMENTS_BY_ATOMIC_NUMBER[99].m_MeltingPointK = 1133.15; // Einsteinium
  }
  
  static { // From http://en.wikipedia.org/wiki/List_of_elements, Feb 19 2010, based on IUPAC where possible
    ELEMENTS_BY_ATOMIC_NUMBER[1].m_BoilingPointK = 20.28; // Hydrogen
    ELEMENTS_BY_ATOMIC_NUMBER[2].m_BoilingPointK = 4.22; // Helium
    ELEMENTS_BY_ATOMIC_NUMBER[3].m_BoilingPointK = 1615; // Lithium
    ELEMENTS_BY_ATOMIC_NUMBER[4].m_BoilingPointK = 2742; // Beryllium
    ELEMENTS_BY_ATOMIC_NUMBER[5].m_BoilingPointK = 4200; // Boron
    ELEMENTS_BY_ATOMIC_NUMBER[6].m_BoilingPointK = 4300; // Carbon
    ELEMENTS_BY_ATOMIC_NUMBER[7].m_BoilingPointK = 77.36; // Nitrogen
    ELEMENTS_BY_ATOMIC_NUMBER[8].m_BoilingPointK = 90.2; // Oxygen
    ELEMENTS_BY_ATOMIC_NUMBER[9].m_BoilingPointK = 85.03; // Fluorine
    ELEMENTS_BY_ATOMIC_NUMBER[10].m_BoilingPointK = 27.07; // Neon
    ELEMENTS_BY_ATOMIC_NUMBER[11].m_BoilingPointK = 1156; // Sodium
    ELEMENTS_BY_ATOMIC_NUMBER[12].m_BoilingPointK = 1363; // Magnesium
    ELEMENTS_BY_ATOMIC_NUMBER[13].m_BoilingPointK = 2792; // Aluminium
    ELEMENTS_BY_ATOMIC_NUMBER[14].m_BoilingPointK = 3538; // Silicon
    ELEMENTS_BY_ATOMIC_NUMBER[15].m_BoilingPointK = 553; // Phosphorus
    ELEMENTS_BY_ATOMIC_NUMBER[16].m_BoilingPointK = 717.8; // Sulfur
    ELEMENTS_BY_ATOMIC_NUMBER[17].m_BoilingPointK = 239.11; // Chlorine
    ELEMENTS_BY_ATOMIC_NUMBER[18].m_BoilingPointK = 87.3; // Argon
    ELEMENTS_BY_ATOMIC_NUMBER[19].m_BoilingPointK = 1032; // Potassium
    ELEMENTS_BY_ATOMIC_NUMBER[20].m_BoilingPointK = 1757; // Calcium
    ELEMENTS_BY_ATOMIC_NUMBER[21].m_BoilingPointK = 3109; // Scandium
    ELEMENTS_BY_ATOMIC_NUMBER[22].m_BoilingPointK = 3560; // Titanium
    ELEMENTS_BY_ATOMIC_NUMBER[23].m_BoilingPointK = 3680; // Vanadium
    ELEMENTS_BY_ATOMIC_NUMBER[24].m_BoilingPointK = 2944; // Chromium
    ELEMENTS_BY_ATOMIC_NUMBER[25].m_BoilingPointK = 2334; // Manganese
    ELEMENTS_BY_ATOMIC_NUMBER[26].m_BoilingPointK = 3134; // Iron
    ELEMENTS_BY_ATOMIC_NUMBER[27].m_BoilingPointK = 3200; // Cobalt
    ELEMENTS_BY_ATOMIC_NUMBER[28].m_BoilingPointK = 3186; // Nickel
    ELEMENTS_BY_ATOMIC_NUMBER[29].m_BoilingPointK = 2835; // Copper
    ELEMENTS_BY_ATOMIC_NUMBER[30].m_BoilingPointK = 1180; // Zinc
    ELEMENTS_BY_ATOMIC_NUMBER[31].m_BoilingPointK = 2477; // Gallium
    ELEMENTS_BY_ATOMIC_NUMBER[32].m_BoilingPointK = 3106; // Germanium
    ELEMENTS_BY_ATOMIC_NUMBER[33].m_BoilingPointK = 887; // Arsenic
    ELEMENTS_BY_ATOMIC_NUMBER[34].m_BoilingPointK = 958; // Selenium
    ELEMENTS_BY_ATOMIC_NUMBER[35].m_BoilingPointK = 332; // Bromine
    ELEMENTS_BY_ATOMIC_NUMBER[36].m_BoilingPointK = 119.93; // Krypton
    ELEMENTS_BY_ATOMIC_NUMBER[37].m_BoilingPointK = 961; // Rubidium
    ELEMENTS_BY_ATOMIC_NUMBER[38].m_BoilingPointK = 1655; // Strontium
    ELEMENTS_BY_ATOMIC_NUMBER[39].m_BoilingPointK = 3609; // Yttrium
    ELEMENTS_BY_ATOMIC_NUMBER[40].m_BoilingPointK = 4682; // Zirconium
    ELEMENTS_BY_ATOMIC_NUMBER[41].m_BoilingPointK = 5017; // Niobium
    ELEMENTS_BY_ATOMIC_NUMBER[42].m_BoilingPointK = 4912; // Molybdenum
    ELEMENTS_BY_ATOMIC_NUMBER[43].m_BoilingPointK = 5150; // Technetium
    ELEMENTS_BY_ATOMIC_NUMBER[44].m_BoilingPointK = 4423; // Ruthenium
    ELEMENTS_BY_ATOMIC_NUMBER[45].m_BoilingPointK = 3968; // Rhodium
    ELEMENTS_BY_ATOMIC_NUMBER[46].m_BoilingPointK = 3236; // Palladium
    ELEMENTS_BY_ATOMIC_NUMBER[47].m_BoilingPointK = 2435; // Silver
    ELEMENTS_BY_ATOMIC_NUMBER[48].m_BoilingPointK = 1040; // Cadmium
    ELEMENTS_BY_ATOMIC_NUMBER[49].m_BoilingPointK = 2345; // Indium
    ELEMENTS_BY_ATOMIC_NUMBER[50].m_BoilingPointK = 2875; // Tin
    ELEMENTS_BY_ATOMIC_NUMBER[51].m_BoilingPointK = 1860; // Antimony
    ELEMENTS_BY_ATOMIC_NUMBER[52].m_BoilingPointK = 1261; // Tellurium
    ELEMENTS_BY_ATOMIC_NUMBER[53].m_BoilingPointK = 457.4; // Iodine
    ELEMENTS_BY_ATOMIC_NUMBER[54].m_BoilingPointK = 165.03; // Xenon
    ELEMENTS_BY_ATOMIC_NUMBER[55].m_BoilingPointK = 944; // Caesium
    ELEMENTS_BY_ATOMIC_NUMBER[56].m_BoilingPointK = 2170; // Barium
    ELEMENTS_BY_ATOMIC_NUMBER[57].m_BoilingPointK = 3737; // Lanthanum
    ELEMENTS_BY_ATOMIC_NUMBER[58].m_BoilingPointK = 3716; // Cerium
    ELEMENTS_BY_ATOMIC_NUMBER[59].m_BoilingPointK = 3793; // Praseodymium
    ELEMENTS_BY_ATOMIC_NUMBER[60].m_BoilingPointK = 3347; // Neodymium
    ELEMENTS_BY_ATOMIC_NUMBER[61].m_BoilingPointK = 3273; // Promethium
    ELEMENTS_BY_ATOMIC_NUMBER[62].m_BoilingPointK = 2067; // Samarium
    ELEMENTS_BY_ATOMIC_NUMBER[63].m_BoilingPointK = 1802; // Europium
    ELEMENTS_BY_ATOMIC_NUMBER[64].m_BoilingPointK = 3546; // Gadolinium
    ELEMENTS_BY_ATOMIC_NUMBER[65].m_BoilingPointK = 3503; // Terbium
    ELEMENTS_BY_ATOMIC_NUMBER[66].m_BoilingPointK = 2840; // Dysprosium
    ELEMENTS_BY_ATOMIC_NUMBER[67].m_BoilingPointK = 2993; // Holmium
    ELEMENTS_BY_ATOMIC_NUMBER[68].m_BoilingPointK = 3503; // Erbium
    ELEMENTS_BY_ATOMIC_NUMBER[69].m_BoilingPointK = 2223; // Thulium
    ELEMENTS_BY_ATOMIC_NUMBER[70].m_BoilingPointK = 1469; // Ytterbium
    ELEMENTS_BY_ATOMIC_NUMBER[71].m_BoilingPointK = 3675; // Lutetium
    ELEMENTS_BY_ATOMIC_NUMBER[72].m_BoilingPointK = 4876; // Hafnium
    ELEMENTS_BY_ATOMIC_NUMBER[73].m_BoilingPointK = 5731; // Tantalum
    ELEMENTS_BY_ATOMIC_NUMBER[74].m_BoilingPointK = 5828; // Tungsten
    ELEMENTS_BY_ATOMIC_NUMBER[75].m_BoilingPointK = 5869; // Rhenium
    ELEMENTS_BY_ATOMIC_NUMBER[76].m_BoilingPointK = 5285; // Osmium
    ELEMENTS_BY_ATOMIC_NUMBER[77].m_BoilingPointK = 4701; // Iridium
    ELEMENTS_BY_ATOMIC_NUMBER[78].m_BoilingPointK = 4098; // Platinum
    ELEMENTS_BY_ATOMIC_NUMBER[79].m_BoilingPointK = 3129; // Gold
    ELEMENTS_BY_ATOMIC_NUMBER[80].m_BoilingPointK = 630; // Mercury
    ELEMENTS_BY_ATOMIC_NUMBER[81].m_BoilingPointK = 1746; // Thallium
    ELEMENTS_BY_ATOMIC_NUMBER[82].m_BoilingPointK = 2022; // Lead
    ELEMENTS_BY_ATOMIC_NUMBER[83].m_BoilingPointK = 1837; // Bismuth
    ELEMENTS_BY_ATOMIC_NUMBER[84].m_BoilingPointK = 1235; // Polonium
    ELEMENTS_BY_ATOMIC_NUMBER[85].m_BoilingPointK = 610; // Astatine
    ELEMENTS_BY_ATOMIC_NUMBER[86].m_BoilingPointK = 211.3; // Radon
    ELEMENTS_BY_ATOMIC_NUMBER[87].m_BoilingPointK = 950; // Francium
    ELEMENTS_BY_ATOMIC_NUMBER[88].m_BoilingPointK = 2010; // Radium
    ELEMENTS_BY_ATOMIC_NUMBER[89].m_BoilingPointK = 3471; // Actinium
    ELEMENTS_BY_ATOMIC_NUMBER[90].m_BoilingPointK = 5061; // Thorium
    ELEMENTS_BY_ATOMIC_NUMBER[91].m_BoilingPointK = 4300; // Protactinium
    ELEMENTS_BY_ATOMIC_NUMBER[92].m_BoilingPointK = 4404; // Uranium
    ELEMENTS_BY_ATOMIC_NUMBER[93].m_BoilingPointK = 4273; // Neptunium
    ELEMENTS_BY_ATOMIC_NUMBER[94].m_BoilingPointK = 3501; // Plutonium
    ELEMENTS_BY_ATOMIC_NUMBER[95].m_BoilingPointK = 2880; // Americium
    ELEMENTS_BY_ATOMIC_NUMBER[96].m_BoilingPointK = 3383; // Curium
    ELEMENTS_BY_ATOMIC_NUMBER[97].m_BoilingPointK = 983; // Berkelium
    ELEMENTS_BY_ATOMIC_NUMBER[98].m_BoilingPointK = 1173; // Californium

  }
  
  // From periodic table spreadsheet, mostly based on USGS data.  Roughly $US from year 2009.
  static {
    Element.hydrogen.setPricePerMol(9.42258E-06);
    Element.helium.setPricePerMol(0.054614837);
    Element.lithium.setPricePerMol(0.024233807);
    Element.beryllium.setPricePerMol(4.826238682);
    Element.boron.setPricePerMol(0.037274598);
    Element.carbon.setPricePerMol(0.007657733);
    Element.nitrogen.setPricePerMol(0.003143103);
    Element.oxygen.setPricePerMol(1.88462E-05);
    Element.fluorine.setPricePerMol(0.006745952);
    Element.neon.setPricePerMol(1.509106203);
    Element.sodium.setPricePerMol(0.001881483);
    Element.magnesium.setPricePerMol(0.022939059);
    Element.aluminum.setPricePerMol(0.015564032);
    Element.silicon.setPricePerMol(0.001334623);
    Element.phosphorus.setPricePerMol(0.002534893);
    Element.sulfur.setPricePerMol(0.001058178);
    Element.chlorine.setPricePerMol(0.001871903);
    Element.argon.setPricePerMol(0.019118003);
    Element.potassium.setPricePerMol(0.00841239);
    Element.calcium.setPricePerMol(0.005607714);
    Element.scandium.setPricePerMol(127.4285566);
    Element.titanium.setPricePerMol(0.039437798);
    Element.vanadium.setPricePerMol(1.165048406);
    Element.chromium.setPricePerMol(0.068771989);
    Element.manganese.setPricePerMol(0.04532385);
    Element.iron.setPricePerMol(0.008477575);
    Element.cobalt.setPricePerMol(3.031780757);
    Element.nickel.setPricePerMol(0.83789524);
    Element.copper.setPricePerMol(0.258154237);
    Element.zinc.setPricePerMol(0.118337591);
    Element.gallium.setPricePerMol(46.15937309);
    Element.germanium.setPricePerMol(89.57165243);
    Element.arsenic.setPricePerMol(0.075260246);
    Element.selenium.setPricePerMol(2.802669408);
    Element.bromine.setPricePerMol(0.082796525);
    Element.krypton.setPricePerMol(10.14278317);
    Element.rubidium.setPricePerMol(98.15122152);
    Element.strontium.setPricePerMol(0.091485794);
    Element.yttrium.setPricePerMol(2.53358535);
    Element.zirconium.setPricePerMol(0.103437069);
    Element.niobium.setPricePerMol(1.685392705);
    Element.molybdenum.setPricePerMol(2.736074484);
    Element.ruthenium.setPricePerMol(1741.638179);
    Element.rhodium.setPricePerMol(1773.267514);
    Element.palladium.setPricePerMol(1833.829376);
    Element.silver.setPricePerMol(28.0461503);
    Element.cadmium.setPricePerMol(0.227765142);
    Element.indium.setPricePerMol(57.37894808);
    Element.tin.setPricePerMol(1.370473711);
    Element.antimony.setPricePerMol(0.423173759);
    Element.tellurium.setPricePerMol(8.334352224);
    Element.iodine.setPricePerMol(2.335144324);
    Element.xenon.setPricePerMol(100.7057802);
    Element.cesium.setPricePerMol(218.6997112);
    Element.barium.setPricePerMol(0.014320772);
    Element.lanthanum.setPricePerMol(64.174341);
    Element.cerium.setPricePerMol(64.73544);
    Element.praseodymium.setPricePerMol(100.4390086);
    Element.neodymium.setPricePerMol(85.67856);
    Element.samarium.setPricePerMol(59.54256);
    Element.europium.setPricePerMol(1303.8597);
    Element.gadolinium.setPricePerMol(83.028);
    Element.terbium.setPricePerMol(272.7158148);
    Element.dysprosium.setPricePerMol(107.25);
    Element.holmium.setPricePerMol(261.2495952);
    Element.erbium.setPricePerMol(160.06782);
    Element.thulium.setPricePerMol(1449.455436);
    Element.ytterbium.setPricePerMol(3654.6048);
    Element.lutetium.setPricePerMol(1732.1733);
    Element.hafnium.setPricePerMol(47.39984724);
    Element.tantalum.setPricePerMol(38.0442236);
    Element.tungsten.setPricePerMol(3.382501716);
    Element.rhenium.setPricePerMol(231.4477633);
    Element.osmium.setPricePerMol(3277.526286);
    Element.iridium.setPricePerMol(3312.334925);
    Element.platinum.setPricePerMol(3361.618443);
    Element.gold.setPricePerMol(2880.28057);
    Element.mercury.setPricePerMol(1.914880282);
    Element.thallium.setPricePerMol(462.559483);
    Element.lead.setPricePerMol(0.303862944);
    Element.bismuth.setPricePerMol(2.457032718);
    Element.thorium.setPricePerMol(22.84680546);
  }
  
  /**
   * From Bondi A (1964) Journal of Physical Chemistry 68:441
   * via http://www.periodictable.com/Properties/A/VanDerWaalsRadius.v.html
   * (Sept 27, 2013)
   */
  static {
    Element.hydrogen.m_VanDerWaalsRadius = 1.2;
    Element.zinc.m_VanDerWaalsRadius = 1.39;
    Element.helium.m_VanDerWaalsRadius = 1.4;
    Element.copper.m_VanDerWaalsRadius = 1.4;
    Element.fluorine.m_VanDerWaalsRadius = 1.47;
    Element.oxygen.m_VanDerWaalsRadius = 1.52;
    Element.neon.m_VanDerWaalsRadius = 1.54;
    Element.nitrogen.m_VanDerWaalsRadius = 1.55;
    Element.mercury.m_VanDerWaalsRadius = 1.55;
    Element.cadmium.m_VanDerWaalsRadius = 1.58;
    Element.nickel.m_VanDerWaalsRadius = 1.63;
    Element.palladium.m_VanDerWaalsRadius = 1.63;
    Element.gold.m_VanDerWaalsRadius = 1.66;
    Element.carbon.m_VanDerWaalsRadius = 1.7;
    Element.silver.m_VanDerWaalsRadius = 1.72;
    Element.magnesium.m_VanDerWaalsRadius = 1.73;
    Element.chlorine.m_VanDerWaalsRadius = 1.75;
    Element.platinum.m_VanDerWaalsRadius = 1.75;
    Element.phosphorus.m_VanDerWaalsRadius = 1.8;
    Element.sulfur.m_VanDerWaalsRadius = 1.8;
    Element.lithium.m_VanDerWaalsRadius = 1.82;
    Element.arsenic.m_VanDerWaalsRadius = 1.85;
    Element.bromine.m_VanDerWaalsRadius = 1.85;
    Element.uranium.m_VanDerWaalsRadius = 1.86;
    Element.gallium.m_VanDerWaalsRadius = 1.87;
    Element.argon.m_VanDerWaalsRadius = 1.88;
    Element.selenium.m_VanDerWaalsRadius = 1.9;
    Element.indium.m_VanDerWaalsRadius = 1.93;
    Element.thallium.m_VanDerWaalsRadius = 1.96;
    Element.iodine.m_VanDerWaalsRadius = 1.98;
    Element.krypton.m_VanDerWaalsRadius = 2.02;
    Element.lead.m_VanDerWaalsRadius = 2.02;
    Element.tellurium.m_VanDerWaalsRadius = 2.06;
    Element.silicon.m_VanDerWaalsRadius = 2.1;
    Element.xenon.m_VanDerWaalsRadius = 2.16;
    Element.tin.m_VanDerWaalsRadius = 2.17;
    Element.sodium.m_VanDerWaalsRadius = 2.27;
    Element.potassium.m_VanDerWaalsRadius = 2.75;
  }
}
