/*
 * Created on Jun 12, 2013
 *
 */
package matsci.location;

import matsci.location.basis.CartesianBasis;

public class Line {
  
  private final Vector m_DefiningVector;
  
  public Line(Coordinates coords1, Coordinates coords2) {
    this(new Vector(coords1, coords2));
  }

  public Line(Vector definingVector) {
    m_DefiningVector = definingVector;
  }
  
  public Vector getDefiningVector() {
    return m_DefiningVector;
  }
  
  public boolean passesThrough(Coordinates coords) {
    
    return this.distanceFrom(coords) < CartesianBasis.getPrecision();
    
    /*if (m_DefiningVector.getTail().isCloseEnoughTo(coords)) {return true;}
    Vector newVector = new Vector(m_DefiningVector.getTail(), coords);
    return m_DefiningVector.isParallelTo(newVector);*/
    
  }
  
  public Coordinates getClosestPoint(Coordinates coords) {
    
    Coordinates head = this.getDefiningVector().getHead();
    Vector w = new Vector(this.getDefiningVector().getHead(), coords);
    Vector u = this.getDefiningVector().unitVectorCartesian();
    
    double dotProduct = w.innerProductCartesian(u);
    return head.translateBy(u.multiply(dotProduct));
    
  }
  
  public double distanceFrom(Coordinates coords) {
    
    return this.getClosestPoint(coords).distanceFrom(coords);
    
  }
  
  // From http://geomalgorithms.com/a07-_distance.html
  public double distanceFrom(Line axis) {
    
    Coordinates head1 = this.getDefiningVector().getHead();
    Coordinates head2 = axis.getDefiningVector().getHead();
    
    Vector u = this.getDefiningVector().unitVectorCartesian();
    Vector v = axis.getDefiningVector().unitVectorCartesian();
    Vector w = new Vector(head1, head2);
    
    //double a = 1;
    double b = u.innerProductCartesian(v);
    //double c = 1;
    double d = u.innerProductCartesian(w);
    double e = v.innerProductCartesian(w);
    
    double denominator = (1 - b * b);
    if (denominator < 1E-8) { // the lines are effectively parallel
      Coordinates nearestPoint = head2.translateBy(v.multiply(-e));
      return head1.distanceFrom(nearestPoint);
    }
    
    double s = (d - b * e) / denominator;
    double t = (b * d - e) / denominator;
    
    Coordinates coords1 = head1.translateBy(u.multiply(s));
    Coordinates coords2 = head2.translateBy(v.multiply(t));
    
    return coords1.distanceFrom(coords2);
    
  }
  
  public Coordinates getClosestPoint(Line line) {
    
    Coordinates head1 = this.getDefiningVector().getHead();
    Coordinates head2 = line.getDefiningVector().getHead();
    
    Vector u = this.getDefiningVector().unitVectorCartesian();
    Vector v = line.getDefiningVector().unitVectorCartesian();
    Vector w = new Vector(head1, head2);
    
    //double a = 1;
    double b = u.innerProductCartesian(v);
    //double c = 1;
    double d = u.innerProductCartesian(w);
    double e = v.innerProductCartesian(w);
    
    double denominator = (1 - b * b);
    if (denominator < 1E-8) { // the lines are effectively parallel
      return head2.translateBy(v.multiply(-e));
    }
    
    //double s = (d - b * e) / denominator;
    double t = (b * d - e) / denominator;
    
    //Coordinates coords1 = head1.translateBy(u.multiply(s));
    Coordinates coords2 = head2.translateBy(v.multiply(t));
    
    return coords2;
    
  }
  
  public Coordinates getIntersection(Line line) {
    
    Coordinates head1 = this.getDefiningVector().getHead();
    Coordinates head2 = line.getDefiningVector().getHead();
    
    Vector u = this.getDefiningVector().unitVectorCartesian();
    Vector v = line.getDefiningVector().unitVectorCartesian();
    Vector w = new Vector(head1, head2);
    
    //double a = 1;
    double b = u.innerProductCartesian(v);
    //double c = 1;
    double d = u.innerProductCartesian(w);
    double e = v.innerProductCartesian(w);
    
    double denominator = (1 - b * b);
    if (denominator < 1E-8) { // the lines are effectively parallel
      Coordinates nearestPoint = head2.translateBy(v.multiply(-e));
      if (head1.distanceFrom(nearestPoint) < CartesianBasis.getPrecision()) {
        return new Vector(head1, nearestPoint).getMidPoint(); // Should in theory be able to return either
      }
      return null;
    }
    
    double s = (d - b * e) / denominator;
    double t = (b * d - e) / denominator;
    
    Coordinates coords1 = head1.translateBy(u.multiply(s));
    Coordinates coords2 = head2.translateBy(v.multiply(t));
    
    if (coords1.distanceFrom(coords2) < CartesianBasis.getPrecision()) {
      return new Vector(coords1, coords2).getMidPoint();
    }
    return null;
    
  }
  
  

}
