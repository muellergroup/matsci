/*
 * Created on Jun 16, 2013
 *
 */
package matsci.location.basis;

import matsci.location.Coordinates;
import matsci.util.MSMath;

public abstract class AbstractLinearBasis extends AbstractBasis {

  public AbstractLinearBasis() {
  }
  
  public double[] transformToCartesianDirection(double[] direction) {
    double[] cartArray = this.transformToCartesian(direction);
    Coordinates origin = this.getOrigin();
    for (int dimNum = 0; dimNum < cartArray.length; dimNum++) {
      cartArray[dimNum] -= origin.cartCoord(dimNum);
    }
    return cartArray;
    /*double[] thisOrigin = this.getOrigin().getCartesianArray();
    return MSMath.arraySubtract(cartArray, thisOrigin);*/
  }
  
  public double[] transformFromCartesianDirection(double[] direction) { 
    double[] cartArray = this.getOrigin().getCartesianArray();
    //Coordinates origin = this.getOrigin();
    for (int dimNum = 0; dimNum < cartArray.length; dimNum++) {
      cartArray[dimNum] += direction[dimNum];
    }
    //double[] cartArray = MSMath.arrayAdd(direction, thisOrigin);
    return this.transformFromCartesian(cartArray);
  }

  public abstract Coordinates getOrigin();

}
