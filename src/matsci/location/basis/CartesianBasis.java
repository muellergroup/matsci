package matsci.location.basis;

import matsci.location.Coordinates;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

// This class is a singleton
public class CartesianBasis extends AbstractLinearBasis {

  private final static CartesianBasis m_Instance = new CartesianBasis();  // The global Cartesian basis, since it's final.
  private final static Coordinates m_Origin = new Coordinates(new double[] {0,0,0}, m_Instance); // TODO Consider if origin is always 3-d

  private static double m_PRECISION = 0.01;
  private static double m_PRECISION_SQUARED = m_PRECISION * m_PRECISION; // so we don't have to take the square root later

  private static double m_ANGLE_TOLERANCE_DEGREES = 0.001;
  
  private CartesianBasis() {
  }

  public double[] transformToCartesian(double[] coordArray) {
    return ArrayUtils.copyArray(coordArray);
  }
  
  public double[] transformToCartesian(int[] coordArray) {
    double[] returnArray = new double[coordArray.length];
    for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
      returnArray[dimNum] = coordArray[dimNum];
    }
    return returnArray;
  }

  public double[] transformFromCartesian(double[] coordArray) {
    return ArrayUtils.copyArray(coordArray);
  }

  public static void setPrecision(double precision) {
    m_PRECISION = precision;
    m_PRECISION_SQUARED = m_PRECISION * m_PRECISION;
  }

  public static double getPrecision() {
    return m_PRECISION;
  }

  public static double getPrecisionSquared() {
    return m_PRECISION_SQUARED;
  }
  
  public static void setAngleToleranceDegrees(double toleranceDegrees) {
    m_ANGLE_TOLERANCE_DEGREES = toleranceDegrees;
  }
  
  public static void setAngleToleranceRadians(double toleranceRadians) {
    m_ANGLE_TOLERANCE_DEGREES = Math.toDegrees(toleranceRadians);
  }
  
  public static double getAngleToleranceDegrees() {
    return m_ANGLE_TOLERANCE_DEGREES;
  }
  
  public static double getAngleToleranceRadians() {
    return Math.toRadians(m_ANGLE_TOLERANCE_DEGREES);
  }

  /*
   * Returns the global Cartesian Coordinates, since m_Instance is *final*.
   */
  public static CartesianBasis getInstance() {
    return m_Instance;
  }
  
  public Coordinates getOrigin() {
    return m_Origin;
  }
  
  public int numDimensions() {
    return m_Origin.numCoords();
  }
  
  public double[] transformToCartesianDirection(double[] direction) { 
    return ArrayUtils.copyArray(direction);
  }
  
  public double[] transformFromCartesianDirection(double[] direction) {  
    return ArrayUtils.copyArray(direction);
  }

  public String toString() {
    return "Cartesian";
  }

}
