/*
 * Created on Feb 25, 2005
 *
 */
package matsci.location.basis;

import matsci.location.Coordinates;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
/**
 * @author Tim Mueller
 *
 */
public class CartesianScaleBasis extends AbstractLinearBasis {

  private final double m_Scale;
  
  /**
   * Use to represent coordinates that are in cartesian coordinates where the units are not Angstroms.
   * 
   * @param scale Should be the length of the new unit, in Angstroms
   */
  public CartesianScaleBasis(double scale) {
    m_Scale = scale;
  }

  /* (non-Javadoc)
   * @see matsci.location.basis.AbstractBasis#transformToCartesian(double[])
   */
  public double[] transformToCartesian(double[] coordArray) {
    return MSMath.arrayMultiply(coordArray, m_Scale);
  }
  
  /* (non-Javadoc)
   * @see matsci.location.basis.AbstractBasis#transformToCartesian(int[])
   */
  public double[] transformToCartesian(int[] coordArray) {
    return MSMath.arrayMultiply(coordArray, m_Scale);
  }

  /* (non-Javadoc)
   * @see matsci.location.basis.AbstractBasis#transformFromCartesian(double[])
   */
  public double[] transformFromCartesian(double[] coordArray) {
    return MSMath.arrayDivide(coordArray, m_Scale);
  }
  
  public double getScale() {
    return m_Scale;
  }

  /* (non-Javadoc)
   * @see matsci.location.basis.AbstractBasis#numDimensions()
   */
  public int numDimensions() {
    return CartesianBasis.getInstance().numDimensions();
  }

  @Override
  public Coordinates getOrigin() {
    return CartesianBasis.getInstance().getOrigin();
  }
  
  public double[] transformToCartesianDirection(double[] direction) { 
    return this.transformToCartesian(direction);
  }
  
  public double[] transformFromCartesianDirection(double[] direction) {  
    return this.transformFromCartesian(direction);
  }

}
