package matsci.location.basis;

import matsci.location.Coordinates;
/**
 * <p>Title: </p>
 * <p>Description:  A Basis is a way of specifying a point in space using an array of double values.  The
 * standard basis is the Cartesian Basis, which are simply typical three-dimensional Cartesian coordinates.
 * Other possible bases include cylindrical and spherical coordinates.
 * All other bases are defined relative to a global Cartesian coordinate system.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public abstract class AbstractBasis {

  public AbstractBasis() {
  }

  /**
   * 
   * @param coordArray An array of double values representing a point in space
   * @param basis The basis corresponding to the given coordinate array
   * @return An array of double values representing the same point in space using this basis.
   */
  public double[] getArrayFrom(double[] coordArray, AbstractBasis basis) {
    if (basis == this) {return coordArray;}
    double[] cArray = basis.transformToCartesian(coordArray);
    return this.transformFromCartesian(cArray);
  }
  
  /**
   * 
   * @param coords An array of coordinate objects we wish to convert to this basis
   * @return An array of coordinate objects representing the same points in space in the same order.  The
   * new array uses this basis to represent the points.
   */
  public Coordinates[] getCoordinatesFrom(Coordinates[] coords) {
    
    Coordinates[] returnArray = new Coordinates[coords.length];
    for (int coordNum = 0; coordNum < returnArray.length; coordNum++) {
      returnArray[coordNum] = coords[coordNum].getCoordinates(this);
    }
    return returnArray;
  }

  /**
   * 
   * @param coordArray An array of double values that when used together with this basis represent a 
   * point in space.
   * @return An array of double values giving the Cartesian coordinates of the given point in space.
   */
  public abstract double[] transformToCartesian(double[] coordArray);
  
  /**
   * 
   * @param coordArray An array of double values that when used together with this basis represent a 
   * point in space.
   * @return An array of double values giving the Cartesian coordinates of the given point in space.
   */
  public abstract double[] transformToCartesian(int[] coordArray);
  
  
  /**
   * 
   * @param coordArray An array of double values that are the Cartesian coordinates for a point in space
   * @return An array of double values that represent the same point in space in the basis encapsulated by
   * this object.
   */
  public abstract double[] transformFromCartesian(double[] coordArray);
  
  /**
   * 
   * @return The number of Cartesian dimensions that can be represented by this basis
   */
  public abstract int numDimensions();

}
