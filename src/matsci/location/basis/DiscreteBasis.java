package matsci.location.basis;

import matsci.location.Coordinates;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class DiscreteBasis extends AbstractBasis {

  private final Structure m_Structure;

  public DiscreteBasis(Structure struct) {
    m_Structure = struct;
  }
  
  public double[] transformToCartesian(int[] intCoordArray) {

    BravaisLattice lattice = m_Structure.getDefiningLattice();

    Structure.Site site = m_Structure.getDefiningSite(intCoordArray[intCoordArray.length - 1]);
    double[] cartCoordArray = site.getCoords().getCartesianArray();

    for (int vectNum = 0; vectNum < lattice.numPeriodicVectors(); vectNum++) {
      double[] vectCartArray = lattice.getPeriodicVector(vectNum).getCartesianDirection();
      for (int coordNum = 0; coordNum < vectCartArray.length; coordNum++) {
        cartCoordArray[coordNum] += vectCartArray[coordNum] * intCoordArray[vectNum];
      }
    }

    return cartCoordArray;
  }

  public double[] transformToCartesian(double[] intCoordArray) {

    BravaisLattice lattice = m_Structure.getDefiningLattice();

    Structure.Site site = m_Structure.getDefiningSite((int) intCoordArray[intCoordArray.length - 1]);
    double[] cartCoordArray = site.getCoords().getCartesianArray();

    for (int vectNum = 0; vectNum < lattice.numPeriodicVectors(); vectNum++) {
      double[] vectCartArray = lattice.getPeriodicVector(vectNum).getCartesianDirection();
      for (int coordNum = 0; coordNum < vectCartArray.length; coordNum++) {
        cartCoordArray[coordNum] += vectCartArray[coordNum] * intCoordArray[vectNum];
      }
    }

    return cartCoordArray;
  }

  public double[] transformFromCartesian(double[] cartCoordArray) {

    /**
     * Check to see if this site even exists on the lattice
     * And if it does, get its index inside the primitive cell.
     */
    BravaisLattice lattice = m_Structure.getDefiningLattice();
    LinearBasis cellBasis = lattice.getLatticeBasis();
    double[] directCoordArray = cellBasis.transformFromCartesian(cartCoordArray);
    //Coordinates directCoords = new Coordinates(cellBasis.transformFromCartesian(cartCoordArray), cellBasis);
    Coordinates cartCoords = new Coordinates(cartCoordArray, CartesianBasis.getInstance());
    //Structure.Site primSite = m_Structure.getDefiningSite(directCoords);
    Structure.Site primSite = m_Structure.getDefiningSite(cartCoords);
    if (primSite == null) {
      throw new RuntimeException("Couldn't find site for coordinates: " + cartCoords);
    }
    int siteIndex = primSite.getIndex();

    /**
     * Construct the return array.  Find the x,y, and z offsets of the primitive cell containing this site.
     * For the fourth coordinate, use the primitive index of this site.
     */
    double[] returnArray = new double[lattice.numPeriodicVectors() + 1];
    //Coordinates innerPrimCoords = primSite.getCoords().getCoordinates(cellBasis);
    double[] innerPrimArray = primSite.getCoords().getCoordArray(cellBasis);
    int returnIndex = 0;
    for (int i = 0; i < innerPrimArray.length; i++) {
      if (!lattice.isDimensionPeriodic(i)) {continue;}
      //returnArray[i] = (int) Math.round(directCoords.coord(i) - innerPrimCoords.coord(i));
      returnArray[returnIndex++] = (int) Math.round(directCoordArray[i] - innerPrimArray[i]);
    }
    returnArray[returnArray.length -1] = siteIndex;
    return returnArray;
  }
  
  public int numDimensions() {
    //return m_Structure.getCellVectors().length + m_Structure.getNonPeriodicVectors().length;
    return m_Structure.getCellVectors().length;
  }

}