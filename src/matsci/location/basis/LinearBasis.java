package matsci.location.basis;

import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class LinearBasis extends AbstractLinearBasis {

  private final Coordinates m_Origin;
  private final double[] m_CartesianOrigin;
  private final double[][] m_BasisToCartesian;
  private final double[][] m_CartesianToBasis;

  public LinearBasis(Vector[] basisVectors) {
    this(CartesianBasis.getInstance().getOrigin(), basisVectors);
  }
  
  public LinearBasis(Coordinates origin, Vector[] basisVectors) {
    super();
    m_Origin = origin;
    m_CartesianOrigin = origin.getCartesianArray();
    m_BasisToCartesian = new double[basisVectors.length][];
    for (int vecNum = 0; vecNum < basisVectors.length; vecNum++) {
      m_BasisToCartesian[vecNum] = basisVectors[vecNum].getCartesianDirection();
    }
    m_CartesianToBasis = MSMath.simpleInverse(m_BasisToCartesian);
  }
  
  public LinearBasis(Coordinates origin, double[][] basisToCartesian) {
    super();
    m_Origin = origin;
    m_CartesianOrigin = origin.getCartesianArray();
    m_BasisToCartesian = basisToCartesian;
    m_CartesianToBasis = MSMath.simpleInverse(basisToCartesian);
  }

  public LinearBasis(double[][] basisToCartesian) {
    super();
    m_Origin = CartesianBasis.getInstance().getOrigin();
    m_CartesianOrigin = m_Origin.getCartesianArray();
    m_BasisToCartesian = basisToCartesian;
    m_CartesianToBasis = MSMath.simpleInverse(basisToCartesian);
  }

  public double[] transformToCartesian(double[] coordArray) {
    double[] relativeArray = MSMath.vectorTimesMatrix(coordArray, m_BasisToCartesian);
    return MSMath.arrayAdd(relativeArray, m_CartesianOrigin);
  }
  
  public double[] transformToCartesian(double[] coordArray, double[] returnArray) {
    double[] relativeArray = MSMath.vectorTimesMatrix(coordArray, m_BasisToCartesian, returnArray);
    return MSMath.arrayAdd(relativeArray, m_CartesianOrigin);
  }
  
  public double[] transformToCartesian(int[] coordArray) {
    double[] relativeArray = MSMath.vectorTimesMatrix(coordArray, m_BasisToCartesian);
    return MSMath.arrayAdd(relativeArray, m_CartesianOrigin);
  }
  
  public double[] transformToCartesian(int[] coordArray, double[] returnArray) {
    double[] relativeArray = MSMath.vectorTimesMatrix(coordArray, m_BasisToCartesian, returnArray);
    return MSMath.arrayAdd(relativeArray, m_CartesianOrigin);
  }

  public double[] transformFromCartesian(double[] coordArray) {
    double[] relativeArray = MSMath.arrayDiff(coordArray, m_CartesianOrigin);
    return MSMath.vectorTimesMatrix(relativeArray, m_CartesianToBasis);
  }
  
  public double[] transformFromCartesian(double[] coordArray, double[] returnArray) {
    double[] relativeArray = MSMath.arrayDiff(coordArray, m_CartesianOrigin);
    return MSMath.vectorTimesMatrix(relativeArray, m_CartesianToBasis, returnArray);
  }

  public double[][] getBasisToCartesian() {
    return ArrayUtils.copyArray(m_BasisToCartesian);
  }

  public double[][] getCartesianToBasis() {
    return ArrayUtils.copyArray(m_CartesianToBasis);
  }
  
  public Coordinates getOrigin() {
    return m_Origin;
  }
  
  public static Vector[] fill3DBasis(Vector[] latticeVectors, Vector[] nonPeriodicVectors) {
    
    int numTotalVectors = latticeVectors.length + nonPeriodicVectors.length;
    Vector[] givenVectors = new Vector[numTotalVectors];
    for (int vecNum = 0; vecNum < givenVectors.length; vecNum++) {
      if (vecNum < latticeVectors.length) {
        givenVectors[vecNum] = latticeVectors[vecNum];
      } else {
        givenVectors[vecNum] = nonPeriodicVectors[vecNum - latticeVectors.length];
      }
    }
    
    return fill3DBasis(givenVectors);
    
  }

  public static Vector[] fill3DBasis(Vector[] givenVectors) {
    
    if (givenVectors.length > 3) {
      throw new RuntimeException("More than three vectors given for a 3-d basis");
    }
    
    Vector[] returnVectors = new Vector[3];
    System.arraycopy(givenVectors, 0, returnVectors, 0, givenVectors.length);
  
    // This cell spans all of space
    if (givenVectors.length == 0) {
      returnVectors[0] = new Vector(new double[] {1,0,0}, CartesianBasis.getInstance());
      returnVectors[1] = new Vector(new double[] {0,1,0}, CartesianBasis.getInstance());
      returnVectors[2] = new Vector(new double[] {0,0,1}, CartesianBasis.getInstance());
    }
    
    if (givenVectors.length == 1) {
      Vector nextVector = givenVectors[0].crossProductCartesian(new Vector(new double[] {1,0,0}, CartesianBasis.getInstance()));
      if (nextVector.length() == 0) {
        nextVector = givenVectors[0].crossProductCartesian(new Vector(new double[] {0,1,0}, CartesianBasis.getInstance()));
      }
      returnVectors[1] = nextVector.unitVectorCartesian();
      returnVectors[2] = nextVector.crossProductCartesian(givenVectors[0]).unitVectorCartesian();
    }
    
    if (givenVectors.length == 2) {
      returnVectors[2] = givenVectors[0].crossProductCartesian(givenVectors[1]).unitVectorCartesian();
    }
    
    return returnVectors;
  }
  
  public int numDimensions() {
    return m_BasisToCartesian.length;
  }
  
  public double[] transformToCartesianDirection(double[] direction) { 
    return MSMath.vectorTimesMatrix(direction, m_BasisToCartesian);
    
    /*double[] cartArray = this.transformToCartesian(direction);
    return MSMath.arraySubtract(cartArray, m_CartesianOrigin);*/
  }
  
  public double[] transformFromCartesianDirection(double[] direction) { 
    /*double[] cartArray = MSMath.arrayAdd(direction, m_CartesianOrigin);
    return this.transformFromCartesian(cartArray);*/
    return MSMath.vectorTimesMatrix(direction, m_CartesianToBasis);
  }

}
