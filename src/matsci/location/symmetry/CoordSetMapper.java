/*
 * Created on Feb 20, 2005
 *
 */
package matsci.location.symmetry;

import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */

public class CoordSetMapper {

  private double[][] m_Distances;
  private double[] m_DistancesFromOrigin;
  
  private Coordinates m_Origin;
  private Coordinates[] m_BaseCoords;
  
  private boolean m_FixOrigin = false;
  
  /**
   * The coords here should be the smaller or the same side as sets to which we are comparing.
   */
  public CoordSetMapper(Coordinates origin, Coordinates[] coords) {
    m_Origin = origin;
    m_BaseCoords = (Coordinates[]) ArrayUtils.copyArray(coords);
    m_Distances = calculateDistances(coords);
    m_DistancesFromOrigin = this.calculateDistancesFromOrigin(coords);
  }
  
  public CoordSetMapper(Coordinates[] baseSet) {
    this(CartesianBasis.getInstance().getOrigin(), baseSet);
  }
  
  public void fixOrigin(boolean value) {
    m_FixOrigin = value;
  }
  
  public boolean isOriginFixed() {
    return m_FixOrigin;
  }
  
  public Coordinates[] getBaseCoords() {
    return (Coordinates[]) ArrayUtils.copyArray(m_BaseCoords);
  }
  
  public int numBaseCoords() {
    return m_BaseCoords.length;
  }
  
  public Coordinates getBaseCoords(int coordNum) {
    return m_BaseCoords[coordNum];
  }
  
  public double getBaseDistance(int baseIndex1, int baseIndex2) {
    return m_Distances[baseIndex1][baseIndex2];
  }
  
  public double getBaseDistanceFromOrigina(int baseIndex) {
    return m_DistancesFromOrigin[baseIndex];
  }
  
  protected static double[][] calculateDistances(Coordinates[] coords) {
    
    double[][] returnArray = new double[coords.length][coords.length];
    for (int coordNum = 0; coordNum < coords.length; coordNum++) {
      for (int coordNum2 = 0; coordNum2 <= coordNum; coordNum2++) {
        returnArray[coordNum][coordNum2] = coords[coordNum].distanceFrom(coords[coordNum2]);
        returnArray[coordNum2][coordNum] = returnArray[coordNum][coordNum2];
      }
    }
    return returnArray;
    
  }
  
  protected double[] calculateDistancesFromOrigin(Coordinates[] coords) {

    double[] returnArray = new double[coords.length];
    for (int coordNum = 0; coordNum < coords.length; coordNum++) {
      returnArray[coordNum] = coords[coordNum].distanceFrom(m_Origin);
    }
    return returnArray;
    
  }
  
  public boolean areCongruent(Coordinates[] coords) {
    if (coords == null) {return false;}
    if (coords.length != m_BaseCoords.length) {return false;}
    int[][] congruentMaps = this.mapCoordinates(coords, true);
    return (congruentMaps.length != 0);
  }
  
  public int[][] mapCoordinates(Coordinates[] bigSet, boolean returnOne) {
    
    double[][] distances = calculateDistances(bigSet);
    double[] distancesFromOrigin = this.calculateDistancesFromOrigin(bigSet);
    int[][] maps = this.mapCoordinates(bigSet, distances, distancesFromOrigin, new int[m_BaseCoords.length], new boolean[bigSet.length], 0, new int[0][], returnOne);
    
    // I added this when I introduced angle tolerance because for some oddly shaped cells, 
    // multiple maps could be returned where the distances were within the Cartesian tolerance.
    // It doesn't seem to be necessary for reasonably shaped cells, but things can actually go
    // faster with this left in.
    for (int mapNum = maps.length - 1; mapNum >= 0; mapNum--) {
      int[] map = maps[mapNum];
      if (!this.areMapAnglesValid(bigSet, map)) {
        maps = ArrayUtils.removeElement(maps, mapNum);
      }
    }
    return maps;
  }
  
  
  protected boolean areMapAnglesValid(Coordinates[] bigSet, int[] map) {
    
    for (int siteNum = 0; siteNum < map.length; siteNum++) {
      Coordinates base1 = m_BaseCoords[siteNum];
      Coordinates mapped1 = bigSet[map[siteNum]];
      for (int siteNum2 = 0; siteNum2 < siteNum; siteNum2++) {
        Coordinates base2 = m_BaseCoords[siteNum2];
        Coordinates mapped2 = bigSet[map[siteNum2]];
        for (int siteNum3 = 0; siteNum3 < siteNum2; siteNum3++) {
          Coordinates base3 = m_BaseCoords[siteNum3];
          Coordinates mapped3 = bigSet[map[siteNum3]];
          double baseAngle1 = base1.angle(base2, base3);
          double mappedAngle1 = mapped1.angle(mapped2, mapped3);
          if (Math.abs(baseAngle1 - mappedAngle1) > CartesianBasis.getAngleToleranceRadians()) {
            return false;
          }
          double baseAngle2 = base2.angle(base1, base3);
          double mappedAngle2 = mapped2.angle(mapped1, mapped3);
          if (Math.abs(baseAngle2 - mappedAngle2) > CartesianBasis.getAngleToleranceRadians()) {
            return false;
          }
          double baseAngle3 = base3.angle(base1, base2);
          double mappedAngle3 = mapped3.angle(mapped1, mapped2);
          if (Math.abs(baseAngle3 - mappedAngle3) > CartesianBasis.getAngleToleranceRadians()) {
            return false;
          }
        }
      }
    }
    return true;
  }
  
  protected int[][] mapCoordinates(Coordinates[] bigSet, double[][] distances, double[] distancesFromOrigin, int[] currentMap, boolean[] mappedIndices, int baseCoordNum, int[][] allMaps, boolean returnOne) {
    
    if (baseCoordNum == currentMap.length) {
      return ArrayUtils.appendElement(allMaps, ArrayUtils.copyArray(currentMap));
    }
    
    for (int coordIndex = 0; coordIndex < distancesFromOrigin.length; coordIndex++) {
      //Coordinates newCoords = bigSet[coordIndex];
      if (mappedIndices[coordIndex]) {continue;}
      
      if (m_FixOrigin) {
        double distDifference = Math.abs(distancesFromOrigin[coordIndex] - m_DistancesFromOrigin[baseCoordNum]);
        //double distDifference = Math.abs((newCoords.distanceFrom(m_Origin)) - m_DistancesFromOrigin[baseCoordNum]);
        /*if (distDifference < 1E-3 &&  distDifference > 1E-5) {
          System.currentTimeMillis();
        }*/
        if ( distDifference > CartesianBasis.getPrecision()) {continue;}
      }
      
      boolean match = this.allowsMap(baseCoordNum, coordIndex, bigSet);
      for (int prevCoord = 0; prevCoord < baseCoordNum; prevCoord++) {
        if (match == false) {break;}
        //double distance = bigSet[currentMap[prevCoord]].distanceFrom(newCoords);
        double distance = distances[coordIndex][currentMap[prevCoord]];
        double distDifference = Math.abs(distance - m_Distances[prevCoord][baseCoordNum]);
        if (distDifference > CartesianBasis.getPrecision()) {
          match = false; 
          break;
        }
        //if (!this.allowsMap(baseCoordNum, coordIndex, bigSet)) {match = false;}
      }
      
      if (match) {
        currentMap[baseCoordNum] = coordIndex;
        mappedIndices[coordIndex] = true;
        allMaps = this.mapCoordinates(bigSet, distances, distancesFromOrigin, currentMap, mappedIndices, baseCoordNum+1, allMaps, returnOne);
        mappedIndices[coordIndex] = false;
        if (returnOne && allMaps.length > 0) {return allMaps;}
      }
    }
    
    return allMaps;
  }
  
  // TODO replace this with filters?
  protected boolean allowsMap(int baseCoordNum, int testCoordIndex, Coordinates[] testCoords) {
    return true; // Just a default stub
  }
  
}
