/*
 * Created on Feb 21, 2005
 *
 */
package matsci.location.symmetry;

import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class CoordListMapper extends CoordSetMapper {

  private int[][] m_BaseCoordsByGroup;
  private int[][] m_TestCoordByGroup;
  
  private boolean m_RemovePermutations = true;
  
  public CoordListMapper(Coordinates origin, Coordinates[] coordGroup) {
    super(origin, groupCoords(coordGroup));
    m_BaseCoordsByGroup = getCoordinateMap(coordGroup, this.getBaseCoords());
  }
  
  public CoordListMapper(Coordinates[] baseSet) {
    this(CartesianBasis.getInstance().getOrigin(), baseSet);
  }

  private static Coordinates[] groupCoords(Coordinates[] coordGroup) {
    
    Coordinates[] returnArray = new Coordinates[0];
    
    for (int coordNum = 0; coordNum < coordGroup.length; coordNum++) {
      Coordinates coords =coordGroup[coordNum];
      boolean match = false;
      for (int prevCoordNum = 0; prevCoordNum < returnArray.length; prevCoordNum++) {
        if (coordGroup[coordNum].isCloseEnoughTo(returnArray[prevCoordNum])) {match = true;}
        if (match) {break;}
      }
      if (!match) {
        returnArray = (Coordinates[]) ArrayUtils.appendElement(returnArray, coords);
      }
    }
    
    return returnArray;
  }
  
  private int[][] getCoordinateMap(Coordinates[] fullCoords, Coordinates[] reducedCoords) {
    
    int[][] map = new int[reducedCoords.length][0];
    for (int coordNum = 0; coordNum < fullCoords.length; coordNum++) {
      Coordinates coords = fullCoords[coordNum];
      for (int reducedCoordNum = 0; reducedCoordNum < reducedCoords.length; reducedCoordNum++) {
        if (coords.isCloseEnoughTo(reducedCoords[reducedCoordNum])) {
          map[reducedCoordNum] = ArrayUtils.appendElement(map[reducedCoordNum], coordNum);
        }
      }
    }
    return map;
  }
  
  public void setRemovePermutations(boolean value) {
    m_RemovePermutations = value;
  }
  
  public boolean arePermutationsRemoved() {
    return m_RemovePermutations;
  }
  
  public int[][] mapCoordinates(Coordinates[] coordsToMap, boolean returnOne) {
    
    Coordinates[] groupedCoords = groupCoords(coordsToMap);
    double[][] distances = calculateDistances(groupedCoords);
    double[] distancesFromOrigin = this.calculateDistancesFromOrigin(groupedCoords);
    
    m_TestCoordByGroup = this.getCoordinateMap(coordsToMap, groupedCoords);
    
    int[][] calculatedMap = this.mapCoordinates(groupedCoords, distances, distancesFromOrigin, new int[this.numBaseCoords()], new boolean[distancesFromOrigin.length], 0, new int[0][], returnOne);
    if (returnOne) {return calculatedMap;}
    
    int[][] returnMap;
    if (m_RemovePermutations) {
      returnMap = new int[calculatedMap.length][coordsToMap.length];
      for (int mapNum = 0; mapNum < calculatedMap.length; mapNum++) {
        int[] map = calculatedMap[mapNum];
        for (int groupNum = 0; groupNum < map.length; groupNum++) {
          int[] baseIndices = m_BaseCoordsByGroup[groupNum];
          int[] targetIndices = m_TestCoordByGroup[map[groupNum]];
          for (int indexNum = 0; indexNum < baseIndices.length; indexNum++) {
            returnMap[mapNum][baseIndices[indexNum]] = targetIndices[indexNum];
          }
        }
      }
    } else {
      throw new RuntimeException("Generating Permutations Not Implemented Yet");
    }
    
    return returnMap;
    
  }
  
  protected boolean allowsMap(int baseCoordNum, int testCoordIndex, Coordinates[] testCoords) {
    boolean returnValue = (m_BaseCoordsByGroup[baseCoordNum].length == m_TestCoordByGroup[testCoordIndex].length);
    return returnValue;
  }
  
}
