/*
 * Created on Mar 7, 2005
 *
 */
package matsci.location.symmetry.operations;

import matsci.structure.BravaisLattice;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class OperationGroup {

  private SymmetryOperation[] m_Operations;
  
  public OperationGroup(SymmetryOperation[] operations) {
    
    m_Operations = new SymmetryOperation[operations.length];
    System.arraycopy(operations, 0, m_Operations, 0, operations.length);
    
  }
  
  public int numOperations() {
    return m_Operations.length;
  }
  
  public SymmetryOperation getOperation(int operationNum) {
    return m_Operations[operationNum];
  }
  
  public SymmetryOperation[] getOperations() {
    return (SymmetryOperation[]) ArrayUtils.copyArray(m_Operations);
  }
  
  public static SymmetryOperation[] completeGroup(SymmetryOperation[] operations, BravaisLattice lattice) {

    for (int opNum1 = 0; opNum1 < operations.length; opNum1++) {
      SymmetryOperation op1 = operations[opNum1];
      for (int opNum2 = 0; opNum2 <= opNum1; opNum2++) {
        SymmetryOperation op2 = operations[opNum2];
        //SymmetryOperation combinedOp = new OperationProduct(op1, op2);
        SymmetryOperation combinedOp = op1.getOperationProduct(op2);
        boolean match = false;
        for (int opNum3 = 0; opNum3 < operations.length; opNum3++) {
          if (operations[opNum3].isEquivalentTo(combinedOp, lattice)) {
            match = true;
            break;
          }
        }
        if (!match) {
          operations = (SymmetryOperation[]) ArrayUtils.appendElement(operations, combinedOp);
        }
      }
    }
    
    return operations;

  }
  
  
  public static SymmetryOperation[] trimGroup(SymmetryOperation[] operations, BravaisLattice lattice) {

    for (int opNum1 = 0; opNum1 < operations.length; opNum1++) {
      SymmetryOperation op1 = operations[opNum1];
      for (int opNum2 = 0; opNum2 <= opNum1; opNum2++) {
        SymmetryOperation op2 = operations[opNum2];
        SymmetryOperation combinedOp = op1.getOperationProduct(op2);
        boolean match = false;
        for (int opNum3 = 0; opNum3 < operations.length; opNum3++) {
          if (operations[opNum3].isEquivalentTo(combinedOp, lattice)) {
            match = true;
            break;
          }
        }
        if (match) {continue;}
        SymmetryOperation[] reducedGroup1 = (SymmetryOperation[]) ArrayUtils.removeElement(operations, opNum1);
        SymmetryOperation[] reducedGroup2 = (SymmetryOperation[]) ArrayUtils.removeElement(operations, opNum2);
        reducedGroup1 = trimGroup(reducedGroup1, lattice);
        reducedGroup2 = trimGroup(reducedGroup2, lattice);
        return (reducedGroup1.length > reducedGroup2.length ? reducedGroup1 : reducedGroup2);
      }
    }
    
    return operations;

  }
  
  public static SymmetryOperation[] trimGroup(SymmetryOperation[] operations) {

    return trimGroup(operations, null);

  }
  
  public static SymmetryOperation[] completeGroup(SymmetryOperation[] operations) {
    
    return completeGroup(operations, null);

  }

}
