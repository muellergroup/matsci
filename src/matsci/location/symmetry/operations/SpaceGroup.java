/*
 * Created on Aug 28, 2005
 *
 */
package matsci.location.symmetry.operations;

import java.io.StringReader;
import java.util.Arrays;

import matsci.io.clusterexpansion.PRIM;
import matsci.location.*;
import matsci.location.basis.*;
import matsci.location.symmetry.CoordSetMapper;
import matsci.structure.*;
import matsci.structure.superstructure.*;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
/**
 * @author Tim Mueller
 *
 */
public class SpaceGroup extends OperationGroup {

  private BravaisLattice m_Lattice;
  private int[][][] m_LatticePreservingPointOperators;
  
  private SpaceGroup m_PrimitiveSpaceGroup;
  
  public static enum CrystalSystem {
    TRICLINIC_3D, MONOCLINIC_3D, ORTHORHOMBIC_3D, TETRAGONAL_3D, HEXAGONAL_3D, TRIGONAL_3D, CUBIC_3D, OBLIQUE_2D, RECTANGULAR_2D, SQUARE_2D, HEXAGONAL_2D, LINEAR_1D, POINT_0D
  }
  
  private CrystalSystem m_CrystalSystem = null;
  
  public static enum GeometricCrystalClass {
    TRICLINIC_PEDIAL_3D, TRICLINIC_PINACOIDAL_3D,
    MONOCLINIC_SPHENOIDAL_3D, MONOCLINIC_DOMATIC_3D, MONOCLINIC_PRISMATIC_3D,
    ORTHORHOMBIC_SPHENOIDAL_3D, ORTHORHOMBIC_PYRAMIDAL_3D, ORTHORHOMBIC_BIPYRAMIDAL_3D,
    TETRAGONAL_PYRAMIDAL_3D, TETRAGONAL_DISPHENOIDAL_3D, TETRAGONAL_DIPYRAMIDAL, TETRAGONAL_TRAPEZOIDAL_3D, DITETRAGONAL_PYRAMIDAL_3D, TETRAGONAL_SCALENOIDAL_3D, DITETRAGONAL_DIPYRAMIDAL_3D,
    TRIGONAL_PYRAMIDAL_3D, RHOMBOHEDRAL_3D, TRIGONAL_TRAPEZOIDAL_3D, DITRIGONAL_PYRAMIDAL_3D, DITRIGONAL_SCALAHEDRAL_3D,
    HEXAGONAL_PYRAMIDAL_3D, TRIGONAL_DIPYRAMIDAL_3D, HEXAGONAL_DIPYRAMIDAL_3D, HEXAGONAL_TRAPEZOIDAL_3D, DIHEXAGONAL_PYRAMIDAL_3D, DITRIGONAL_DIPYRAMIDAL_3D, DIHEXAGONAL_DIPYRAMIDAL_3D,
    TETRAHEDRAL_3D, DIPLOIDAL_3D, GYROIDAL_3D, HEXTETRAHEDRAL_3D, HEXOCTAHEDRAL_3D,
    OBLIQUE_1_2D, OBLIQUE_2_2D, 
    RECTANGULAR_M_2D, RECTANGULAR_2MM_2D, 
    SQUARE_4_2D, SQUARE_4MM_2D, 
    HEXAGONAL_3_2D, HEXAGONAL_3M_2D, HEXAGONAL_6_2D, HEXAGONAL_6MM_2D, 
    LINEAR_1_1D, LINEAR_M_1D, 
    POINT_0D
  }
  
  private GeometricCrystalClass m_GeometricCrystalClass = null;
  
  private int m_ArithmeticCrystalClassNumber = -1;
  
  /**
   * These are not necessarily the same as that of the underlying lattice once the symmetry
   * operations of this space group are taken into account.  For example, a lattice that happens
   * to have 90 degree angles may or may not be orthorhombic, depending on whether symmetry
   * forces it to be so.  In practice, this usually won't be a problem for real materials, but 
   * could be an issue for manually constructed cells.
   */
  private BravaisLattice.LatticeSystem m_LatticeSystem = null;
  private BravaisLattice.CrystalFamily m_CrystalFamily = null;
  private BravaisLattice.BravaisType m_BravaisFlock = null;
  
  private SuperLattice m_ConventionalLattice = null;
  private BravaisLattice m_ConventionalPrimLattice = null;
  
  private SpaceGroup m_SymmorphicGroup = null;
  private SpaceGroup m_CentroSymmetricSymmorphicGroup = null;
  
  /**
   * @param operations
   */
  public SpaceGroup(SymmetryOperation[] factorOperations, BravaisLattice lattice) {
    //super(reduceTranslations(factorOperations, lattice)); // This doesn't appear to be necessary
    super(factorOperations);
    m_Lattice = lattice;
    m_LatticePreservingPointOperators = new int[factorOperations.length][][];
  }

  public SpaceGroup(OperationGroup pointGroup, BravaisLattice lattice) {
    this(pointGroup.getOperations(), lattice);
  }
  
  public SpaceGroup getFactoredSpaceGroup(BravaisLattice lattice) {
    SymmetryOperation[] factorOperations = this.getFactorOperations(lattice);
    return new SpaceGroup(factorOperations, lattice);
  }
  
  public static SymmetryOperation[] reduceTranslations(SymmetryOperation[] operations, BravaisLattice lattice) {
    
    SymmetryOperation[] returnArray = new SymmetryOperation[operations.length];
    for (int opNum = 0; opNum < operations.length; opNum++) {
      
      SymmetryOperation op = operations[opNum];
      Vector translation = op.getTranslationVector();
      Coordinates latticeCoords = lattice.getOrigin().translateBy(translation);
      Coordinates nearbyCoords = lattice.getNearestImage(lattice.getOrigin(), latticeCoords);
      
      Vector revTranslation = new Vector(latticeCoords, nearbyCoords);
      returnArray[opNum] = op.addTranslation(revTranslation);
      
    }
    
    return returnArray;
  }
  
  /*
  public SpaceGroup getLatticePreservingSpaceGroup(SuperLattice lattice) {
    int[] latticePreservingOps = lattice.getLatticePreservingOpNums(this);
    SymmetryOperation[] preservingOperations = new SymmetryOperation[latticePreservingOps.length];
    for (int opNum = 0; opNum < latticePreservingOps.length; opNum++) {
      preservingOperations[opNum] = this.getOperation(latticePreservingOps[opNum]); 
    }
    SpaceGroup preservingGroup = new SpaceGroup(preservingOperations, m_Lattice);
    SymmetryOperation[] factorOperations = preservingGroup.getFactorOperations(lattice);
    return new SpaceGroup(factorOperations, lattice);
  }*/
  
  public BravaisLattice getLattice() {
    return m_Lattice;
  }
  
  // TODO Make this protected and use getFactoredSpaceGroup or getLatticePreservingSpaceGroup
  public SymmetryOperation[] getFactorOperations(BravaisLattice lattice) {
    if (lattice.isEquivalentTo(m_Lattice)) {
      return this.getOperations();
    }
    int[][] superToDirect = this.getLattice().getSuperToDirect(lattice);
    return this.getFactorOperations(new SuperLattice(this.getLattice(), superToDirect));
  }
  
  //TODO Make this protected and use getFactoredSpaceGroup or getLatticePreservingSpaceGroup
  public SymmetryOperation[] getFactorOperations(SuperLattice lattice) {
    if (lattice.isEquivalentTo(m_Lattice)) {
      return this.getOperations();
    }
    // We make sure we are dealing with a superlattice that treats our lattice as its primitive lattice
    if (lattice.getPrimLattice() != this.getLattice()) {
      //throw new RuntimeException("Can only get factor operations for lattice that is a superlattice of the lattice for which this space group is defined.");
      int[][] superToDirect = this.getLattice().getSuperToDirect(lattice);
      lattice = new SuperLattice(this.getLattice(), superToDirect);
    }
    
    SymmetryOperation[] returnArray = new SymmetryOperation[this.numOperations() * lattice.numPrimCells()];
    int returnIndex = 0;
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      SymmetryOperation op = this.getOperation(opNum);
      for (int vecNum = 0; vecNum < lattice.numPrimCells(); vecNum++) {
        Vector translation = lattice.getPrimCellVector(vecNum);
        //returnArray[returnIndex++] = new OpPlusTranslation(op, translation);
        returnArray[returnIndex++] = op.addTranslation(translation);
      }
    }
    
    return returnArray;
  }
  

  /**
   * If the given operation contains a lattice-preserving point operation, return 
   * an nxn matrix representing the point operation within the periodic lattice
   * plane, where n is the total number of dimensions.
   * 
   * The difference between this and getLatticePreservingPointOperator is that 
   * this returns an operator that operates within all dimensions, not just the
   * periodic ones.
   * 
   * @param opNum
   * @return
   */
  public int[][] getLatticePreservingPointOperator(int opNum) {
    
    if (opNum < 0 || opNum >= m_LatticePreservingPointOperators.length) {return null;}
    if (m_LatticePreservingPointOperators[opNum] == null) {
      SymmetryOperation op = this.getOperation(opNum);
      m_LatticePreservingPointOperators[opNum] = op.isLatticePreserving(m_Lattice);
    }
    if (m_LatticePreservingPointOperators[opNum] == null) {
      return null;
    }
    return ArrayUtils.copyArray(m_LatticePreservingPointOperators[opNum]);
  }
  
  /**
   * If the given operation contains a lattice-preserving point operation, return 
   * an nxn matrix representing the point operation within the periodic lattice
   * plane, where n is the total number of periodic dimensions.
   * 
   * The difference between this and getLatticePreservingPointOperator is that 
   * this returns an operator that operates only within the plane of the 
   * periodic lattice.
   * 
   * @param opNum
   * @return
   */
  public int[][] getLatticePointOperator(int opNum) {
    
    int[][] latticePreservingOperator = this.getLatticePreservingPointOperator(opNum);
    if (latticePreservingOperator == null) {return null;}
    if (latticePreservingOperator.length == m_Lattice.numPeriodicVectors()) {
      return latticePreservingOperator;
    }

    int[] periodicDimensions = m_Lattice.getPeriodicIndices();
    int[][] latticeOnlyOperator = new int[periodicDimensions.length][periodicDimensions.length];
    for (int rowNum = 0; rowNum < latticeOnlyOperator.length; rowNum++) {
      int[] row = latticeOnlyOperator[rowNum];
      int[] pointRow = latticePreservingOperator[periodicDimensions[rowNum]];
      for (int colNum = 0; colNum < row.length; colNum++) {
        row[colNum] = pointRow[periodicDimensions[colNum]];
      }
    }
    
    return latticeOnlyOperator;
  }
  
  /**
   * This keeps only the operations that move points in a direction parallel to the lattice plane.
   * 
   * @param operations
   * @return
   */
  public SymmetryOperation[] removeSubperiodicOperations(SymmetryOperation[] operations) {
    
    Vector[] testVectors = this.getLattice().getNonPeriodicVectors();
    for (int opNum = operations.length - 1; opNum >= 0; opNum--) {
      SymmetryOperation op = operations[opNum];
      boolean goodOp = true;
      for (int vecNum = 0; vecNum < testVectors.length; vecNum++) {
        Vector testVector = testVectors[vecNum];
        Vector oppedVector = op.operate(testVector);
        if (testVector.subtract(oppedVector).length() > CartesianBasis.getPrecision()) {
          goodOp = false;
          break;
        }
      }
      if (!goodOp) {
        operations = (SymmetryOperation[]) ArrayUtils.removeElement(operations, opNum);
      }
    }
    return operations;
    
  }
  
  
  public SpaceGroup getSymmorphicGroup() {
    
    return this.getSymmorphicGroup(false);
    
  }
  
  /**
   * This is useful to force time reversal symmetry for reciprocal lattices
   * @param addInversion
   * @return
   */
  //public SpaceGroup getSymmorphicGroup(boolean removeSubperiodOperations, boolean addInversion) {
  public SpaceGroup getSymmorphicGroup(boolean addInversion) {
    
    SymmetryOperation[] symmorphicOperations = null;
    if (m_SymmorphicGroup == null) {
        
      symmorphicOperations = this.getLatticePreservingPointOperations(true);
      // Don't set symmorphic group to this, because some operations will include translations, which don't map well to reciprocal space.
      m_SymmorphicGroup = new SpaceGroup(symmorphicOperations, this.getLattice());
      m_SymmorphicGroup.m_SymmorphicGroup = m_SymmorphicGroup;
    }
    
    if (addInversion && m_CentroSymmetricSymmorphicGroup == null) {
      if (symmorphicOperations == null) {
        symmorphicOperations = m_SymmorphicGroup.getOperations();
      }

      for (int opNum = 0; opNum < symmorphicOperations.length; opNum++) {
        if (symmorphicOperations[opNum].isInversion() != null) {
          m_CentroSymmetricSymmorphicGroup = m_SymmorphicGroup;
          break;
        }
      }
      
      // We check to see if we still didn't find it.
      if (m_CentroSymmetricSymmorphicGroup == null) {

        // TODO check this for systems with fewer than 3 periodic dimensions
        double[][] inversionMatrix = ArrayUtils.diagonalMatrix(m_Lattice.numTotalVectors(), -1.0);
        SymmetryOperation inversionOp = new SymmetryOperation(inversionMatrix,  m_Lattice.getLatticeBasis());
        symmorphicOperations = (SymmetryOperation[]) ArrayUtils.appendElement(symmorphicOperations, inversionOp);
        symmorphicOperations = OperationGroup.completeGroup(symmorphicOperations);
        m_CentroSymmetricSymmorphicGroup = new SpaceGroup(symmorphicOperations, this.getLattice());
        m_CentroSymmetricSymmorphicGroup.m_SymmorphicGroup = m_CentroSymmetricSymmorphicGroup;        
        m_CentroSymmetricSymmorphicGroup.m_CentroSymmetricSymmorphicGroup = m_CentroSymmetricSymmorphicGroup;
      }
      
    }
    
    return addInversion ? m_CentroSymmetricSymmorphicGroup : m_SymmorphicGroup;
    
  }
  
  /*public OperationGroup getArithmeticCrystalClassSpaceGroup() {
    
    return this.getArithmeticCrystalClassSpaceGroup(false);
    
  }*/
  

  /**
   * This is useful to force time reversal symmetry for reciprocal lattices
   * 
   * @param addInversion
   * @return
   */
  // TODO:  Fix this.  Should allow for glide and screw operations
  /*public OperationGroup getArithmeticCrystalClassSpaceGroup(boolean addInversion) {
    
    SymmetryOperation[] operations = this.getLatticePointOperations(true);
    if (!addInversion) {
      return new SpaceGroup(operations, m_Lattice);
    }
    
    for (int opNum = 0; opNum < operations.length; opNum++) {
      if (operations[opNum].isInversion() != null) {
        return new SpaceGroup(operations, m_Lattice);
      }
    }
    
    double[][] inversionMatrix = ArrayUtils.diagonalMatrix(m_Lattice.numTotalVectors(), 1);
    for (int vecNum = 0; vecNum < inversionMatrix.length; vecNum++) {
      if (m_Lattice.isDimensionPeriodic(vecNum)) {
        inversionMatrix[vecNum][vecNum] = -1;
      }
    }
    SymmetryOperation inversionOp = new LinearPointOp(inversionMatrix,  m_Lattice.getLatticeBasis());
    operations = (SymmetryOperation[]) ArrayUtils.appendElement(operations, inversionOp);
    operations = OperationGroup.completeGroup(operations, m_Lattice);
    return new SpaceGroup(operations, m_Lattice);
  }*/
  

  public int[][][] getLatticePointOperators(boolean removeDuplicates) {
    
    return this.getLatticePointOperators(removeDuplicates, false, false);
    
  }

  public int[][][] getLatticePointOperators(boolean removeDuplicates, boolean removeIdentity, boolean removeInversion) {
    
    int[][][] returnArray = new int[m_LatticePreservingPointOperators.length][][];
    int returnIndex = 0;
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      if (removeIdentity && this.getOperation(opNum).isIdentity()) {continue;}
      if (removeInversion && (this.getOperation(opNum).isInversion() != null)) {continue;}
      int[][] latticeOnlyOperator = this.getLatticePointOperator(opNum);
      if (latticeOnlyOperator == null) {continue;}
      if (removeDuplicates) {
        boolean duplicate = false;
        for (int prevOpNum = 0; prevOpNum < returnIndex; prevOpNum++) {
          if (ArrayUtils.equals(latticeOnlyOperator, returnArray[prevOpNum])) {
            duplicate = true;
            break;
          }
        }
        if (duplicate) {continue;}
      }
      returnArray[returnIndex++] = latticeOnlyOperator;
    }
    return ArrayUtils.truncateArray(returnArray, returnIndex);
  }
  
  public int[][][] getLatticePreservingPointOperators(boolean removeDuplicates) {
    
    int[][][] returnArray = new int[m_LatticePreservingPointOperators.length][][];
    int returnIndex = 0;
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      int[][] latticeOnlyOperator = this.getLatticePreservingPointOperator(opNum);
      if (latticeOnlyOperator == null) {continue;}
      if (removeDuplicates) {
        boolean duplicate = false;
        for (int prevOpNum = 0; prevOpNum < returnIndex; prevOpNum++) {
          if (ArrayUtils.equals(latticeOnlyOperator, returnArray[prevOpNum])) {
            duplicate = true;
            break;
          }
        }
        if (duplicate) {continue;}
      }
      returnArray[returnIndex++] = latticeOnlyOperator;
    }
    return ArrayUtils.truncateArray(returnArray, returnIndex);

  }
  
  public SymmetryOperation[] getLatticePreservingPointOperations(boolean removeDuplicates) {

    int[][][] latticePointOperators = this.getLatticePreservingPointOperators(removeDuplicates);
   // int[][][] latticePointOperators = this.getLatticePointOperators(removeDuplicates);
    
    SymmetryOperation[] returnArray = new SymmetryOperation[latticePointOperators.length];
    for (int opNum = 0; opNum < returnArray.length; opNum++) {
      double[][] opArray = ArrayUtils.toDoubleArray(latticePointOperators[opNum]);
      returnArray[opNum] = new SymmetryOperation(opArray, m_Lattice.getLatticeBasis());
    }
    return returnArray;
    
  }
  
  /**
   * This removes translations from glide planes and screw axes
   * @return
   */
  private SymmetryOperation[] getStrippedOperations() {
    
    SymmetryOperation[] returnOperations = this.getOperations();
    for (int opNum = 0; opNum < returnOperations.length; opNum++) {
      returnOperations[opNum] = returnOperations[opNum].getPointOperation();
    }
    
    return returnOperations;
  }

  public SymmetryOperation[] findMappingOperations(Coordinates[] fromSet, Coordinates[] toSet, boolean returnOne) {
    
    SymmetryOperation[] returnArray = new SymmetryOperation[0];
    
    if (fromSet.length != toSet.length) {return returnArray;}
    if (fromSet.length == 0) {return this.getFactorOperations(m_Lattice);}
    
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      SymmetryOperation op = this.getOperation(opNum);
      Coordinates[] mappedFrom = op.operate(fromSet);
      int[][] maps = m_Lattice.findTranslationMaps(mappedFrom, toSet, true);
      if (maps.length == 0) {continue;}
      int[] map = maps[0];
      Coordinates fromCoords = mappedFrom[0];
      Coordinates toCoords = toSet[map[0]];
      Vector translation = new Vector(fromCoords, toCoords);
      //SymmetryOperation returnOperation = new OpPlusTranslation(op, translation);
      SymmetryOperation returnOperation = op.addTranslation(translation);
      returnArray = (SymmetryOperation[]) ArrayUtils.appendElement(returnArray, returnOperation);
      if (returnOne) {return returnArray;}
    }
    
    return returnArray;
    
  }
  
  /**
   * Finds any way in which the space group of the cluster expansion can map the coords to each other
   * 
   * @param coordSet1
   * @param coordSet2
   * @param returnOne
   * @return
   */
  public int[][] findSymmetryMaps(Coordinates[] coordSet1, Coordinates[] coordSet2, boolean returnOne) {
    
    int[][] returnArray = new int[0][];
    if (coordSet1 == null || coordSet2 == null) {return returnArray;}
    if (coordSet1.length == 0 || coordSet2.length == 0) {return new int[1][0];} // The empty cluster maps to all clusters with no translation

    Coordinates[] bigSet = coordSet2;
    Coordinates[] smallSet = coordSet1;
    
    if (coordSet1.length > coordSet2.length) {
      bigSet = coordSet1;
      smallSet = coordSet2;
    }
    
    // Just a quick congruency check
    CoordSetMapper setMapper = new CoordSetMapper(smallSet);
    if (setMapper.mapCoordinates(bigSet, true).length == 0) {return returnArray;} 
    
    AbstractBasis latticeBasis = m_Lattice.getLatticeBasis();
    Coordinates[] latticeBigSet = latticeBasis.getCoordinatesFrom(bigSet);
    Coordinates[] latticeSmallSet = latticeBasis.getCoordinatesFrom(smallSet);
    
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      
      SymmetryOperation operation = this.getOperation(opNum);
      Coordinates[] oppedSmallSet = operation.operate(latticeSmallSet);
      int[][] mapsForOperation = m_Lattice.findTranslationMaps(oppedSmallSet, latticeBigSet, returnOne);
      
      if ((mapsForOperation.length > 0) && returnOne) {
        return mapsForOperation;
      }
      
      // Make sure we haven't already seen this map
      for (int newMapNum = 0; newMapNum < mapsForOperation.length; newMapNum++) {
        if (!ArrayUtils.arrayContains(returnArray, mapsForOperation[newMapNum])) {
          returnArray = ArrayUtils.appendElement(returnArray, mapsForOperation[newMapNum]);
        }
      }
    }
    
    return returnArray;
  }

  /*public boolean areCongruent(Coordinates[] coords1, Coordinates[] coords2) {
    if (coords1 == null || coords2 == null) {return false;}
    if (coords1.length != coords2.length) {return false;}
    int[][] congruentMaps = this.findCongruentMaps(coords1, coords2, true);
    return (congruentMaps.length != 0);
  }*/
  
  public boolean areSymmetricallyEquivalent(Coordinates[] coords1, Coordinates[] coords2) {
    
    if (coords1 == null || coords2 == null) {return false;}
    if (coords1.length != coords2.length) {return false;}
    int[][] symmetryMaps = this.findSymmetryMaps(coords1, coords2, true);
    return (symmetryMaps.length != 0);
  }
  
  public boolean areSymmetricallyEquivalent(Coordinates coords1, Coordinates coords2) {
    
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      SymmetryOperation op = this.getOperation(opNum);
      Coordinates oppedCoords1 = op.operate(coords1);
      if (m_Lattice.areTranslationallyEquivalent(oppedCoords1, coords2)) {return true;}
    }
    
    return false;
    
  }
  
  public Coordinates[] getSymmetricallyEquivalentCoords(Coordinates coords) {
    
    Coordinates[] returnArray = new Coordinates[this.numOperations()];
    int returnIndex = 0;
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      SymmetryOperation op = this.getOperation(opNum);
      Coordinates oppedCoords = op.operate(coords);
      boolean match = false;
      for (int prevCoordNum = 0; prevCoordNum < returnIndex; prevCoordNum++) {
        Coordinates prevCoords = returnArray[prevCoordNum];
        if (this.getLattice().areTranslationallyEquivalent(oppedCoords, prevCoords)) {
          match = true;
          break;
        }
      }
      if (!match) {
        returnArray[returnIndex++] = this.getLattice().hardRemainder(oppedCoords);
      }
    }
    return (Coordinates[]) ArrayUtils.truncateArray(returnArray, returnIndex);
    
  }
  
  public boolean areSymmetricallyEquivalent(Coordinates coords1, Coordinates coords2, double tolerance) {
    
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      SymmetryOperation op = this.getOperation(opNum);
      Coordinates oppedCoords1 = op.operate(coords1);
      if (m_Lattice.areTranslationallyEquivalent(oppedCoords1, coords2, tolerance)) {return true;}
    }
    
    return false;
    
  }
  
  /**
   * This method does not take into account the symmetry of the cluster expansion but is useful
   * for pre-screening for clusters that can't possible by symmetrically equivalent.
   * 
   * @param group1
   * @param group2
   * @param returnOne
   * @return
   */
  /*public int[][] findCongruentMaps(Coordinates[] coordSet1, Coordinates[] coordSet2, boolean returnOne) {
    int[][] returnArray = new int[0][];
    if (coordSet1 == null || coordSet2 == null) {return returnArray;}
    if (coordSet1.length == 0 || coordSet2.length == 0) {return new int[1][0];}
    
    Coordinates[] smallSet = coordSet1;
    Coordinates[] bigSet = coordSet2;
    if (coordSet2.length < coordSet1.length) {
      smallSet = coordSet2;
      bigSet = coordSet1;
    }
    
    CoordSetMapper mapper = new CoordSetMapper(smallSet);
    return mapper.mapCoordinates(bigSet, returnOne);
    
  }*/
  
  public SymmetryOperation getInverse(SymmetryOperation op1) {
    int index = this.getInverseIndex(op1);
    if (index < 0) {return null;}
    return this.getOperation(index);
  }
  
  public int getInverseIndex(SymmetryOperation op1) {
    for (int op2Num = 0; op2Num < this.numOperations(); op2Num++) {
      SymmetryOperation op2 = this.getOperation(op2Num);
      //SymmetryOperation jointOperation = new OperationProduct(op1, op2);
      SymmetryOperation jointOperation = op1.getOperationProduct(op2);
      Vector transVector = jointOperation.isPureTranslation();
      if (transVector == null) {continue;}
      if (m_Lattice.isLatticeVector(transVector)) {return op2Num;}
    }
    return -1;
  }
  
  public int getInverseIndex(int opNum) {
    SymmetryOperation op1 = this.getOperation(opNum);
    return this.getInverseIndex(op1);
  }
  
  public boolean isPrimitive() {
    
    int numPureTranslations = 0;
    for (int opNum = 0; opNum < this.numOperations(); opNum++) {
      SymmetryOperation op = this.getOperation(opNum);
      if (op.isPureTranslation() != null) {
        numPureTranslations++;
      }
      if (numPureTranslations > 1) {
        return false;
      }
    }
    
    return true;
    
  }
  
  public SpaceGroup getPrimitiveSpaceGroup() {
    
    if (m_PrimitiveSpaceGroup == null) {
  
      Vector[] potentialVectors = new Vector[this.numOperations()];
      AbstractLinearBasis basis = m_Lattice.getLatticeBasis();
      
      int vecNum = 0;
    
      for (int opNum = 0; opNum < this.numOperations(); opNum++) {
        SymmetryOperation op = this.getOperation(opNum);
        if (op.isPureTranslation() != null) {
          potentialVectors[vecNum++] = new Vector(op.getOriginCenteredTranslation(basis), basis);
        }
      }
      potentialVectors = (Vector[]) ArrayUtils.truncateArray(potentialVectors, vecNum);
      int numPrimCells = potentialVectors.length;
      if (numPrimCells == 1) {
        m_PrimitiveSpaceGroup = this;
        return m_PrimitiveSpaceGroup;
      }
      BravaisLattice newLattice = m_Lattice.getSubLattice(potentialVectors, numPrimCells);
      
      SymmetryOperation[] newOperations = new SymmetryOperation[this.numOperations() / numPrimCells];
      int newOpNum = 0;
      for (int opNum = 0; opNum < this.numOperations(); opNum++) {
        SymmetryOperation operation = this.getOperation(opNum);
        boolean match = false;
        for (int prevOpNum = 0; prevOpNum < newOpNum; prevOpNum++) {
          SymmetryOperation prevOp = newOperations[prevOpNum];
          if (operation.isEquivalentTo(prevOp, newLattice)) {
            match = true;
            break;
          }
        }
        if (!match) {
          newOperations[newOpNum++] = operation;
        }
      }
      m_PrimitiveSpaceGroup = new SpaceGroup(newOperations, newLattice);
    }
    
    return m_PrimitiveSpaceGroup;
    
  }
  
  /**
   * For now, this should only be used on symmorphic groups, as the conventional
   * lattice code does not consider glide planes, etc.
   * @return
   */
  public BravaisLattice getConventionalPrimLattice() {
    if (m_ConventionalPrimLattice == null) {
      m_ConventionalPrimLattice = BravaisLattice.findConventionalPrimLattice(this.getConventionalLattice(), this.getBravaisFlock());
    } 
    return m_ConventionalPrimLattice;
  }
  
  
  /**
   * For now, this should only be used on symmorphic groups, as the conventional
   * lattice code does not consider glide planes, etc.
   * @return
   */
  public SuperLattice getConventionalLattice() {
    if (m_ConventionalLattice == null) {
      SymmetryOperation[] ops = this.getLatticePreservingPointOperations(true);
      m_ConventionalLattice = BravaisLattice.findConventionalLattice(this.getLattice(), ops);
    }
    return m_ConventionalLattice;
  }
  
  public BravaisLattice.LatticeSystem getLatticeSystem() {
    if (m_LatticeSystem == null) {
      m_LatticeSystem = this.findLatticeSystem();
    }
    return m_LatticeSystem;
  }
  
  private BravaisLattice.LatticeSystem findLatticeSystem() {
    
    CrystalSystem crystalSystem = this.getCrystalSystem();
    /*if (crystalSystem.equals(CrystalSystem.HEXAGONAL_3D) || crystalSystem.equals(CrystalSystem.TRIGONAL_3D)) {

      return this.getLattice().getLatticeSystem(); // TODO verify this is OK.  This is not OK!  Problems with cubic lattices
      
    }*/
    if (crystalSystem.equals(CrystalSystem.TRIGONAL_3D)) {
      
      BravaisLattice.LatticeSystem latticeSystem = this.getLattice().getLatticeSystem(); // TODO verify that this is OK
      if (latticeSystem == BravaisLattice.LatticeSystem.CUBIC_3D || latticeSystem == BravaisLattice.LatticeSystem.RHOMBOHEDRAL_3D) {
        return BravaisLattice.LatticeSystem.RHOMBOHEDRAL_3D;
      }
      return BravaisLattice.LatticeSystem.HEXAGONAL_3D;
    }
    return BravaisLattice.LatticeSystem.valueOf(crystalSystem.name());
    
  }
  
  public BravaisLattice.BravaisType getBravaisFlock() {
    if (m_BravaisFlock == null) {
      m_BravaisFlock = this.findBravaisFlock();
    }
    return m_BravaisFlock;
  }
  
  /**
   * For now, this should only be used on symmorphic groups, as the conventional
   * lattice code does not consider glide planes, etc.
   * @return
   */
  private BravaisLattice.BravaisType findBravaisFlock() {
    
    return BravaisLattice.getBravaisType(this.getLatticeSystem(), this.getConventionalLattice());
    
  }
  
  public CrystalSystem getCrystalSystem() {
    if (m_CrystalSystem == null) {
      m_CrystalSystem = this.findCrystalSystem();
    }
    return m_CrystalSystem;
  }
  
  private CrystalSystem findCrystalSystem() {
    
    BravaisLattice.CrystalFamily crystalFamily = this.getCrystalFamily();
    
    SymmetryOperation[] operations = this.getLatticePreservingPointOperations(true);
    
    if (crystalFamily == BravaisLattice.CrystalFamily.HEXAGONAL_3D) {
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        Vector rotation = op.isRotationOrRotoInversion();
        if (rotation == null) {continue;}
        int angle = (int) Math.round(Math.toDegrees(rotation.length()));
        if (angle == 60) {
          return CrystalSystem.HEXAGONAL_3D;
        }
      }
      return CrystalSystem.TRIGONAL_3D;
    }
    return CrystalSystem.valueOf(crystalFamily.name());
    
    // TODO diperiodic space groups and other lower-dimension space groups
    
  }
  
  /**
   * I force the user to specify how many periodic dimensions they think there are
   * because otherwise people might start using the 2D number on a system they think 
   * is 3D.
   * 
   * @param numPeriodicDimensions
   * @return
   */
  public int getArithmeticCrystalClassNumber(int numPeriodicDimensions) {
   
    if (this.getLattice().numPeriodicVectors() != numPeriodicDimensions) {return -1;}
    if (m_ArithmeticCrystalClassNumber < 0) {
      m_ArithmeticCrystalClassNumber = this.findArithmeticCrystalClassNumber();
    }
    return m_ArithmeticCrystalClassNumber;
  }
  
  /**
   * Numbers from International Tables for Crystallography (2006). Vol. C, Chapter 1.4, pp. 15�22.
   * http://it.iucr.org/Cb/ch1o4v0001/ch1o4.pdf
   * @return
   */
  private int findArithmeticCrystalClassNumber() {
    
    GeometricCrystalClass crystalClass = this.getGeometricCrystalClass();
    BravaisLattice lattice = this.getLattice();
    BravaisLattice.BravaisType type;
    
    switch (crystalClass) {
      case TRICLINIC_PEDIAL_3D:
        return 1;        
      case TRICLINIC_PINACOIDAL_3D:
        return 2;
      case MONOCLINIC_SPHENOIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.MONOCLINIC_3D) {return 3;}
        if (type == BravaisLattice.BravaisType.MONOCLINIC_C_3D) {return 4;}
        break;
      case MONOCLINIC_DOMATIC_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.MONOCLINIC_3D) {return 5;}
        if (type == BravaisLattice.BravaisType.MONOCLINIC_C_3D) {return 6;}
        break;
      case MONOCLINIC_PRISMATIC_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.MONOCLINIC_3D) {return 7;}
        if (type == BravaisLattice.BravaisType.MONOCLINIC_C_3D) {return 8;}
        break;
      case ORTHORHOMBIC_SPHENOIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_3D) {return 9;}
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_C_3D) {return 10;}
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_F_3D) {return 11;}
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_I_3D) {return 12;}
        break;
      case ORTHORHOMBIC_PYRAMIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_3D) {return 13;}
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_C_3D) {
          SymmetryOperation[] ops = this.getLatticePreservingPointOperations(true);
          Vector[] conventionalVectors = this.getConventionalLattice().getCellVectors();
          // Use the fact that the c axis is orthogonal to the face-centered axis
          for (int opNum = 0; opNum < ops.length; opNum++) {
            Vector rotation = ops[opNum].isRotation();
            if (rotation == null) {continue;}
            if (rotation.isParallelTo(conventionalVectors[2])) {
              return 14;
            }
            return 15;
          }
        }
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_F_3D) {return 16;}
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_I_3D) {return 17;}
        break;
      case ORTHORHOMBIC_BIPYRAMIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_3D) {return 18;}
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_C_3D) {return 19;}
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_F_3D) {return 20;}
        if (type == BravaisLattice.BravaisType.ORTHORHOMBIC_I_3D) {return 21;}
        break;
      case TETRAGONAL_PYRAMIDAL_3D: 
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.TETRAGONAL_3D) {return 22;}
        if (type == BravaisLattice.BravaisType.TETRAGONAL_I_3D) {return 23;}
        break;
      case TETRAGONAL_DISPHENOIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.TETRAGONAL_3D) {return 24;}
        if (type == BravaisLattice.BravaisType.TETRAGONAL_I_3D) {return 25;}
        break;
      case TETRAGONAL_DIPYRAMIDAL:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.TETRAGONAL_3D) {return 26;}
        if (type == BravaisLattice.BravaisType.TETRAGONAL_I_3D) {return 27;}
        break;
      case TETRAGONAL_TRAPEZOIDAL_3D: 
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.TETRAGONAL_3D) {return 28;}
        if (type == BravaisLattice.BravaisType.TETRAGONAL_I_3D) {return 29;}
        break;
      case DITETRAGONAL_PYRAMIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.TETRAGONAL_3D) {return 30;}
        if (type == BravaisLattice.BravaisType.TETRAGONAL_I_3D) {return 31;}
        break;
      case TETRAGONAL_SCALENOIDAL_3D: 
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.TETRAGONAL_3D) {
          SymmetryOperation[] ops = this.getStrippedOperations();
          Vector[] conventionalVectors = this.getConventionalLattice().getCellVectors();
          for (int opNum = 0; opNum < ops.length; opNum++) {
            Vector rotation = ops[opNum].isRotationOrRotoInversion();
            if (rotation == null) {continue;}
            boolean parallelVec = false;
            for (int vecNum = 0; vecNum < conventionalVectors.length; vecNum++) {
              if (rotation.isParallelTo(conventionalVectors[vecNum])) {
                parallelVec = true;
                break;
              }
            }
            if (!parallelVec) {
              return 33;
            }
          }
          return 32;  
        }
        if (type == BravaisLattice.BravaisType.TETRAGONAL_I_3D) {
          SymmetryOperation[] ops = this.getStrippedOperations();
          Vector[] conventionalVectors = this.getConventionalLattice().getCellVectors();
          for (int opNum = 0; opNum < ops.length; opNum++) {
            Vector rotation = ops[opNum].isRotationOrRotoInversion();
            if (rotation == null) {continue;}
            boolean parallelVec = false;
            for (int vecNum = 0; vecNum < conventionalVectors.length; vecNum++) {
              if (rotation.isParallelTo(conventionalVectors[vecNum])) {
                parallelVec = true;
                break;
              }
            }
            if (!parallelVec) {
              return 34;
            }
          }
          return 35;
        }
        break;
      case DITETRAGONAL_DIPYRAMIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.TETRAGONAL_3D) {return 36;}
        if (type == BravaisLattice.BravaisType.TETRAGONAL_I_3D) {return 37;}
        break;
      case TRIGONAL_PYRAMIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.HEXAGONAL_3D) {return 38;}
        if (type == BravaisLattice.BravaisType.RHOMBOHEDRAL_3D) {return 39;}
        break;
      case RHOMBOHEDRAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.HEXAGONAL_3D) {return 40;}
        if (type == BravaisLattice.BravaisType.RHOMBOHEDRAL_3D) {return 41;}
        break;
      case TRIGONAL_TRAPEZOIDAL_3D: 
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.HEXAGONAL_3D) {
          SymmetryOperation[] ops = this.getStrippedOperations();
          for (int opNum = 0; opNum < ops.length; opNum++) {
            SymmetryOperation op = ops[opNum];
            Vector twoFoldAxis = op.isRotation();
            if (twoFoldAxis == null) {continue;}
            int twoFoldAngle = (int) Math.round(Math.toDegrees(twoFoldAxis.length()));
            if (twoFoldAngle != 180) {continue;} // it's not a two-fold rotation. 
            for (int opNum2 = 0; opNum2 < ops.length; opNum2++) {
              SymmetryOperation op2 = ops[opNum2];
              Vector threeFoldAxis = op2.isRotation();
              if (threeFoldAxis == null) {continue;}
              int threeFoldAngle = (int) Math.round(Math.toDegrees(threeFoldAxis.length()));
              if (threeFoldAngle != 120) {continue;} // it's not a three-fold rotation. 
              Coordinates oppedTail = op.operate(threeFoldAxis.getTail());
              Vector translation = new Vector(threeFoldAxis.getTail(), oppedTail);
              double dotProduct = translation.innerProductCartesian(threeFoldAxis.unitVectorCartesian());
              translation = translation.subtract(threeFoldAxis.resize(dotProduct)); // Subtract out the component parallel to the axes
              if (!lattice.isLatticeVector(translation)) {
                // The rotation axis did not pass through a mirror plane
                return 43; // 321P
              }
            }
          }
          return 42; // 312P
        }
        if (type == BravaisLattice.BravaisType.RHOMBOHEDRAL_3D) {return 44;}
        break;
      case DITRIGONAL_PYRAMIDAL_3D: 
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.HEXAGONAL_3D) {
          SymmetryOperation[] ops = this.getStrippedOperations();
          for (int opNum = 0; opNum < ops.length; opNum++) {
            SymmetryOperation reflection = ops[opNum];
            if (reflection.isReflection() == null) {continue;}
            for (int opNum2 = 0; opNum2 < ops.length; opNum2++) {
              SymmetryOperation rotation = ops[opNum2];
              Vector axis = rotation.isRotation();
              if (axis == null) {continue;}
              Coordinates tail = axis.getTail();
              Coordinates oppedTail = reflection.operate(tail);
              Vector translation = new Vector(tail, oppedTail);
              if (!lattice.isLatticeVector(translation)) {
                // The rotation axis did not pass through a mirror plane
                return 46; // 31mp
              }
            }
            return 45; // 3m1p
          }
        }
        if (type == BravaisLattice.BravaisType.RHOMBOHEDRAL_3D) {return 47;}
        break;
      case DITRIGONAL_SCALAHEDRAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.HEXAGONAL_3D) {
          SymmetryOperation[] ops = this.getStrippedOperations();
          Vector[] conventionalVectors = this.getConventionalLattice().getCellVectors();
          for (int opNum = 0; opNum < ops.length; opNum++) {
            SymmetryOperation rotation = ops[opNum];
            Vector axis = rotation.isRotation();
            if (axis == null) {continue;}
            int angle = (int) Math.round(Math.toDegrees(axis.length()));
            if (angle != 180) {continue;} 
            if (axis.isParallelTo(conventionalVectors[0])) {return 49;}
            if (axis.isParallelTo(conventionalVectors[1])) {return 49;}
          }
          return 48;
        }
        if (type == BravaisLattice.BravaisType.RHOMBOHEDRAL_3D) {return 50;}
        break;
      case HEXAGONAL_PYRAMIDAL_3D:
        return 51;
      case TRIGONAL_DIPYRAMIDAL_3D:
        return 52;
      case HEXAGONAL_DIPYRAMIDAL_3D:
        return 53;
      case HEXAGONAL_TRAPEZOIDAL_3D:
        return 54;
      case DIHEXAGONAL_PYRAMIDAL_3D:
        return 55;
      case DITRIGONAL_DIPYRAMIDAL_3D:
        SymmetryOperation[] ops = this.getStrippedOperations();
        for (int opNum = 0; opNum < ops.length; opNum++) {
          SymmetryOperation threeFoldRotation = ops[opNum];
          Vector threeFoldAxis = threeFoldRotation.isRotationOrRotoInversion();
          if (threeFoldAxis == null) {continue;}
          int threeFoldAngle = (int) Math.round(Math.toDegrees(threeFoldAxis.length()));
          if (threeFoldAngle == 180) {continue;} // it's a two-fold rotation. 
          for (int opNum2 = 0; opNum2 < ops.length; opNum2++) {
            SymmetryOperation reflection = ops[opNum2];
            if (reflection.isReflection() == null) {continue;}
            Vector oppedAxis = reflection.operate(threeFoldAxis);
            int angle = (int) Math.round(threeFoldAxis.angleDegrees(oppedAxis));
            if (angle != 0) {continue;} // wrong reflection
            Vector translation = new Vector(threeFoldAxis.getTail(), oppedAxis.getTail());
            if (!lattice.isLatticeVector(translation)) {
              // The rotation axis did not pass through a mirror plane
              return 57; // -62mp
            }
          }
        }
        return 56; // -6m2p
      case DIHEXAGONAL_DIPYRAMIDAL_3D:
        return 58;
      case TETRAHEDRAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.CUBIC_3D) {return 59;}
        if (type == BravaisLattice.BravaisType.FCC_3D) {return 60;}
        if (type == BravaisLattice.BravaisType.BCC_3D) {return 61;}
        break;
      case DIPLOIDAL_3D: 
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.CUBIC_3D) {return 62;}
        if (type == BravaisLattice.BravaisType.FCC_3D) {return 63;}
        if (type == BravaisLattice.BravaisType.BCC_3D) {return 64;}
        break;
      case GYROIDAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.CUBIC_3D) {return 65;}
        if (type == BravaisLattice.BravaisType.FCC_3D) {return 66;}
        if (type == BravaisLattice.BravaisType.BCC_3D) {return 67;}
        break;
      case HEXTETRAHEDRAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.CUBIC_3D) {return 68;}
        if (type == BravaisLattice.BravaisType.FCC_3D) {return 69;}
        if (type == BravaisLattice.BravaisType.BCC_3D) {return 70;}
        break;
      case HEXOCTAHEDRAL_3D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.CUBIC_3D) {return 71;}
        if (type == BravaisLattice.BravaisType.FCC_3D) {return 72;}
        if (type == BravaisLattice.BravaisType.BCC_3D) {return 73;}
        break;
      
      // End 3D, start 2D  
      case OBLIQUE_1_2D:
        return 1;
      case OBLIQUE_2_2D:
        return 2;
      case RECTANGULAR_M_2D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.RECTANGULAR_2D) {return 3;}
        if (type == BravaisLattice.BravaisType.RHOMBIC_2D) {return 4;}
        break;
      case RECTANGULAR_2MM_2D:
        type = this.getBravaisFlock();
        if (type == BravaisLattice.BravaisType.RECTANGULAR_2D) {return 5;}
        if (type == BravaisLattice.BravaisType.RHOMBIC_2D) {return 6;}
        break;
      case SQUARE_4_2D:
        return 7;
      case SQUARE_4MM_2D:
        return 8;
      case HEXAGONAL_3_2D:
        return 9;
      case HEXAGONAL_3M_2D:
        //ops = this.getLatticePointOperations(true);
        ops = this.getStrippedOperations();
        for (int opNum = 0; opNum < ops.length; opNum++) {
          SymmetryOperation reflection = ops[opNum];
          if (reflection.isReflection() == null) {continue;}
          for (int opNum2 = 0; opNum2 < ops.length; opNum2++) {
            SymmetryOperation threeFoldRotation = ops[opNum2];
            Vector axis = threeFoldRotation.isRotation();
            if (axis == null) {continue;}
            Coordinates tail = axis.getTail();
            Coordinates oppedTail = reflection.operate(tail);
            Vector translation = new Vector(tail, oppedTail);
            if (!lattice.isLatticeVector(translation)) {
              // The rotation axis did not pass through a mirror plane
              return 11;
            }
          }
          return 10;
        }
      case HEXAGONAL_6_2D:
        return 12;
      case HEXAGONAL_6MM_2D:
        return 13;
      
      // End 2D, start 1D
      case LINEAR_1_1D:
        return 1;
      case LINEAR_M_1D:
        return 2;
        
      // End 1D, start 0D
      case POINT_0D:
        return 1;
      
    }
    
    return -1;
  }
  
  public GeometricCrystalClass getGeometricCrystalClass() {
    
    if (m_GeometricCrystalClass == null) {
      m_GeometricCrystalClass = this.findGeometricCrystalClass();
    }
    
    return m_GeometricCrystalClass;
    
  }
    
  private GeometricCrystalClass findGeometricCrystalClass() {
    
    // TODO diperiodic space groups and other lower-dimension groups
    
    CrystalSystem crystalSystem = this.getCrystalSystem();
    SymmetryOperation[] operations = this.getLatticePreservingPointOperations(true);
    int order = operations.length;
    
    if (crystalSystem == CrystalSystem.CUBIC_3D) {
      
      if (order == 12) {return GeometricCrystalClass.TETRAHEDRAL_3D;}
      if (order == 48) {return GeometricCrystalClass.HEXOCTAHEDRAL_3D;}
      if (order == 24) {
        boolean hasMirror = false;
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isInversion() != null) {return GeometricCrystalClass.DIPLOIDAL_3D;}
          hasMirror |= (op.isReflection() != null);
        }
        if (hasMirror) {return GeometricCrystalClass.HEXTETRAHEDRAL_3D;}
        return GeometricCrystalClass.GYROIDAL_3D;
      }
    }
    
    if (crystalSystem == CrystalSystem.TRICLINIC_3D) {
      if (order == 1) {return GeometricCrystalClass.TRICLINIC_PEDIAL_3D;}
      if (order == 2) {return GeometricCrystalClass.TRICLINIC_PINACOIDAL_3D;}
    }
    
    if (crystalSystem == CrystalSystem.MONOCLINIC_3D) {
      if (order == 4) {return GeometricCrystalClass.MONOCLINIC_PRISMATIC_3D;}
      if (order == 2) {
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isReflection() != null) {return GeometricCrystalClass.MONOCLINIC_DOMATIC_3D;}
        }
        return GeometricCrystalClass.MONOCLINIC_SPHENOIDAL_3D;
      }
    }
    
    if (crystalSystem == CrystalSystem.ORTHORHOMBIC_3D) {
      if (order == 8) {return GeometricCrystalClass.ORTHORHOMBIC_BIPYRAMIDAL_3D;}
      if (order == 4) {
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isReflection() != null) {return GeometricCrystalClass.ORTHORHOMBIC_PYRAMIDAL_3D;}
        }
        return GeometricCrystalClass.ORTHORHOMBIC_SPHENOIDAL_3D;
      }
    }
    
    if (crystalSystem == CrystalSystem.TETRAGONAL_3D) {
      if (order == 16) {return GeometricCrystalClass.DITETRAGONAL_DIPYRAMIDAL_3D;}
      if (order == 4) {
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isRotoInversion() != null) {
            return GeometricCrystalClass.TETRAGONAL_DISPHENOIDAL_3D;
          }
        }
        return GeometricCrystalClass.TETRAGONAL_PYRAMIDAL_3D;
      }
      if (order == 8) {          
        int numMirrors = 0;
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isInversion() != null) {return GeometricCrystalClass.TETRAGONAL_DIPYRAMIDAL;}
          if (op.isReflection() != null) {numMirrors++;}
        }
        if (numMirrors == 4) {return GeometricCrystalClass.DITETRAGONAL_PYRAMIDAL_3D;}
        if (numMirrors == 2) {return GeometricCrystalClass.TETRAGONAL_SCALENOIDAL_3D;}
        return GeometricCrystalClass.TETRAGONAL_TRAPEZOIDAL_3D;
      }
    }
    
    if (crystalSystem == CrystalSystem.TRIGONAL_3D) {
      if (order == 3) {return GeometricCrystalClass.TRIGONAL_PYRAMIDAL_3D;}
      if (order == 12) {return GeometricCrystalClass.DITRIGONAL_SCALAHEDRAL_3D;}
      if (order == 6) {
        boolean hasMirror = false;
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isInversion() != null) {return GeometricCrystalClass.RHOMBOHEDRAL_3D;}
          hasMirror |= (op.isReflection() != null);
        }
        if (hasMirror) {return GeometricCrystalClass.DITRIGONAL_PYRAMIDAL_3D;}
        return GeometricCrystalClass.TRIGONAL_TRAPEZOIDAL_3D;
      }
    }
    
    if (crystalSystem == CrystalSystem.HEXAGONAL_3D) {
      if (order == 24) {return GeometricCrystalClass.DIHEXAGONAL_DIPYRAMIDAL_3D;}
      if (order == 6) {
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isReflection() != null) {return GeometricCrystalClass.TRIGONAL_DIPYRAMIDAL_3D;}
        }
        return GeometricCrystalClass.HEXAGONAL_PYRAMIDAL_3D;
      }
      if (order == 12) {
        int numMirrors = 0;
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isInversion() != null) {return GeometricCrystalClass.HEXAGONAL_DIPYRAMIDAL_3D;}
          if (op.isReflection() != null) {numMirrors++;}
        }
        if (numMirrors == 6) {return GeometricCrystalClass.DIHEXAGONAL_PYRAMIDAL_3D;}
        if (numMirrors == 4) {return GeometricCrystalClass.DITRIGONAL_DIPYRAMIDAL_3D;}
        return GeometricCrystalClass.HEXAGONAL_TRAPEZOIDAL_3D;
      }
    }

    if (crystalSystem == CrystalSystem.OBLIQUE_2D) {
      if (order == 1) {return GeometricCrystalClass.OBLIQUE_1_2D;}
      if (order == 2) {return GeometricCrystalClass.OBLIQUE_2_2D;}
    }
    
    if (crystalSystem == CrystalSystem.RECTANGULAR_2D) {
      if (order == 2) {return GeometricCrystalClass.RECTANGULAR_M_2D;}
      if (order == 4) {return GeometricCrystalClass.RECTANGULAR_2MM_2D;}
    }
    
    if (crystalSystem == CrystalSystem.SQUARE_2D) {
      if (order == 4) {return GeometricCrystalClass.SQUARE_4_2D;}
      if (order == 8) {return GeometricCrystalClass.SQUARE_4MM_2D;}
    }
    
    if (crystalSystem == CrystalSystem.HEXAGONAL_2D) {
      if (order == 3) {return GeometricCrystalClass.HEXAGONAL_3_2D;}
      if (order == 12) {return GeometricCrystalClass.HEXAGONAL_6MM_2D;}
      if (order == 6) {        
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          if (op.isReflection() != null) {return GeometricCrystalClass.HEXAGONAL_3M_2D;}
        }
        return GeometricCrystalClass.HEXAGONAL_6_2D;
      }
    }
    
    if (crystalSystem == CrystalSystem.LINEAR_1D) {
      if (order == 1) {return GeometricCrystalClass.LINEAR_1_1D;}
      if (order == 2) {return GeometricCrystalClass.LINEAR_M_1D;}
    }
    
    if (crystalSystem == CrystalSystem.POINT_0D) {
      return GeometricCrystalClass.POINT_0D;
    }
    
    // For now let's just deal with null pointer exceptions
    //throw new RuntimeException("Unknown geometric crystal class");
    return null;
  }
  
  public BravaisLattice.CrystalFamily getCrystalFamily() {
    if (m_CrystalFamily == null) {
      m_CrystalFamily = this.findCrystalFamily();
    }
    return m_CrystalFamily;
  }
  
  private BravaisLattice.CrystalFamily findCrystalFamily() {
    
    SymmetryOperation[] operations = this.getLatticePreservingPointOperations(true);
    return BravaisLattice.getCrystalFamily(operations, this.getLattice().numPeriodicVectors());
    
  }
  /*
  private BravaisLattice.CrystalFamily findCrystalFamily() {
    
    BravaisLattice.CrystalFamily bravaisFamily = m_Lattice.getCrystalFamily();
    
    SymmetryOperation[] operations = this.getLatticePointOperations(true);
    
    if (bravaisFamily == BravaisLattice.CrystalFamily.CUBIC_3D) {
      int num90 = 0;
      int num180 = 0;
      int num120 = 0;
      int numMirror = 0;
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        Vector rotation = op.isRotation();
        if (rotation == null) {continue;}
        int angle = (int) Math.round(Math.toDegrees(rotation.length()));
        if (angle == 90) {num90++;}
        if (angle == 180) {num180++;}
        if (angle == 120) {num120++;}
        if (op.isReflection() != null) {numMirror++;}
      }
      if (num90 == 6) {
        return bravaisFamily;
      } else if (num90 > 0) {
        return BravaisLattice.CrystalFamily.TETRAGONAL_3D;
      } else if (num120 > 0) {
        return BravaisLattice.CrystalFamily.HEXAGONAL_3D;
      } else if ((num180 == 3) || (num180 == 1 && numMirror == 2)) {
        return BravaisLattice.CrystalFamily.ORTHORHOMBIC_3D;
      } else if (num180 == 1 || numMirror == 1) {
        return BravaisLattice.CrystalFamily.MONOCLINIC_3D;
      } else {
        return BravaisLattice.CrystalFamily.TRICLINIC_3D;
      }
    }
    
    if (bravaisFamily == BravaisLattice.CrystalFamily.HEXAGONAL_3D) {
      int num90 = 0;
      int num180 = 0;
      int numMirror = 0;
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        Vector rotation = op.isRotation();
        if (rotation == null) {continue;}
        int angle = (int) Math.round(Math.toDegrees(rotation.length()));
        if (angle == 120) {
          return bravaisFamily;
        }
        if (angle == 90) {num90++;}
        if (angle == 180) {num180++;}
        if (op.isReflection() != null) {numMirror++;}
      }
      if (num90 > 0) {
        return BravaisLattice.CrystalFamily.TETRAGONAL_3D;
      } else if ((num180 == 3) || (num180 == 1 && numMirror == 2)) {
        return BravaisLattice.CrystalFamily.ORTHORHOMBIC_3D;
      } else if (num180 == 1 || numMirror == 1) {
        return BravaisLattice.CrystalFamily.MONOCLINIC_3D;
      } else {
        return BravaisLattice.CrystalFamily.TRICLINIC_3D;
      }
    }
    
    if (bravaisFamily == BravaisLattice.CrystalFamily.TETRAGONAL_3D) {
      int num180 = 0;
      int numMirror = 0;
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        Vector rotation = op.isRotation();
        if (rotation == null) {continue;}
        int angle = (int) Math.round(Math.toDegrees(rotation.length()));
        if (angle == 90) {
          return bravaisFamily;
        }
        if (angle == 180) {num180++;}
        if (op.isReflection() != null) {numMirror++;}
      }
      if ((num180 == 3) || (num180 == 1 && numMirror == 2)) {
        return BravaisLattice.CrystalFamily.ORTHORHOMBIC_3D;
      } else if (num180 == 1 || numMirror == 1) {
        return BravaisLattice.CrystalFamily.MONOCLINIC_3D;
      } else {
        return BravaisLattice.CrystalFamily.TRICLINIC_3D;
      }
    }
    
    if (bravaisFamily == BravaisLattice.CrystalFamily.ORTHORHOMBIC_3D) {
      int num180 = 0;
      int numMirror = 0;
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        Vector rotation = op.isRotation();
        if (rotation == null) {continue;}
        int angle = (int) Math.round(Math.toDegrees(rotation.length()));
        if (angle == 180) {num180++;}
        if (op.isReflection() != null) {numMirror++;}
      }
      if ((num180 == 3) || (num180 == 1 && numMirror == 2)) {
        return BravaisLattice.CrystalFamily.ORTHORHOMBIC_3D;
      } else if (num180 == 1 || numMirror == 1) {
        return BravaisLattice.CrystalFamily.MONOCLINIC_3D;
      } else {
        return BravaisLattice.CrystalFamily.TRICLINIC_3D;
      }
    }
    
    if (bravaisFamily == BravaisLattice.CrystalFamily.MONOCLINIC_3D) {
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        Vector rotation = op.isRotation();
        if (rotation == null) {continue;}
        int angle = (int) Math.round(Math.toDegrees(rotation.length()));
        if (angle == 180) {
          return bravaisFamily;
        }
        if (op.isReflection() != null) {
          return bravaisFamily;
        }
      }
      return BravaisLattice.CrystalFamily.TRICLINIC_3D;
    }
    
    if (bravaisFamily == BravaisLattice.CrystalFamily.SQUARE_2D) {
      int numMirror = 0;
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        Vector rotation = op.isRotation();
        if (rotation == null) {continue;}
        int angle = (int) Math.round(Math.toDegrees(rotation.length()));
        if (angle == 90) {
          return bravaisFamily;
        }
        if (op.isReflection() != null) {numMirror++;}
      }
      if (numMirror > 0) {
        return BravaisLattice.CrystalFamily.RECTANGULAR_2D;
      } else {
        return BravaisLattice.CrystalFamily.OBLIQUE_2D;
      } 
    }
    
    if (bravaisFamily == BravaisLattice.CrystalFamily.RECTANGULAR_2D) {
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        if (op.isReflection() != null) {
          return BravaisLattice.CrystalFamily.RECTANGULAR_2D;
        }
      }
      return BravaisLattice.CrystalFamily.OBLIQUE_2D;
    }
    
    return bravaisFamily;
    
  }
*/
  
  public static int[] ARITHMETIC_GROUP_BY_SPACE_GROUP_3D = new int[] {
    0,
    1,
    2,
    3,
    3,
    4,
    5,
    5,
    6,
    6,
    7,
    7,
    8,
    7,
    7,
    8,
    9,
    9,
    9,
    9,
    10,
    10,
    11,
    12,
    12,
    13,
    13,
    13,
    13,
    13,
    13,
    13,
    13,
    13,
    13,
    14,
    14,
    14,
    15,
    15,
    15,
    15,
    16,
    16,
    17,
    17,
    17,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    18,
    19,
    19,
    19,
    19,
    19,
    19,
    20,
    20,
    21,
    21,
    21,
    21,
    22,
    22,
    22,
    22,
    23,
    23,
    24,
    25,
    26,
    26,
    26,
    26,
    27,
    27,
    28,
    28,
    28,
    28,
    28,
    28,
    28,
    28,
    29,
    29,
    30,
    30,
    30,
    30,
    30,
    30,
    30,
    30,
    31,
    31,
    31,
    31,
    32,
    32,
    32,
    32,
    33,
    33,
    33,
    33,
    34,
    34,
    35,
    35,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    36,
    37,
    37,
    37,
    37,
    38,
    38,
    38,
    39,
    40,
    41,
    42,
    43,
    42,
    43,
    42,
    43,
    44,
    45,
    46,
    45,
    46,
    47,
    47,
    48,
    48,
    49,
    49,
    50,
    50,
    51,
    51,
    51,
    51,
    51,
    51,
    52,
    53,
    53,
    54,
    54,
    54,
    54,
    54,
    54,
    55,
    55,
    55,
    55,
    56,
    56,
    57,
    57,
    58,
    58,
    58,
    58,
    59,
    60,
    61,
    59,
    61,
    62,
    62,
    63,
    63,
    64,
    62,
    64,
    65,
    65,
    66,
    66,
    67,
    65,
    65,
    67,
    68,
    69,
    70,
    68,
    69,
    70,
    71,
    71,
    71,
    71,
    72,
    72,
    72,
    72,
    73,
    73,
  };
  
  private static String[][] SYMMORPHIC_SPACE_GROUP_STRUCTS = new String[4][74];
  private static SpaceGroup[][] SYMMORPHIC_SPACE_GROUPS = new SpaceGroup[4][74];
  
  // TODO
  // Add more dimensions
  // Simplify the structures -- some of these are large and take a long time.
  static {
    SYMMORPHIC_SPACE_GROUP_STRUCTS[3] = new String[74];
    SYMMORPHIC_SPACE_GROUPS[3] = new SpaceGroup[74];
    
    SYMMORPHIC_SPACE_GROUP_STRUCTS[3][18] = "Zinc iridium boride (1/4/3) <s> Ir Zn B </s>\n" + 
        "  1.00000000000000\n" + 
        "     2.79980000000000     0.00000000000000     0.00000000000000\n" + 
        "     0.00000000000000    23.17700000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     2.81960000000000\n" + 
        "  Ir    Zn    B     \n" + 
        "  8     2     6     \n" + 
        "Direct\n" + 
        "     1.00000000000000      0.58333333333333      0.50000000000000   Ir  \n" + 
        "     0.00000000000000      0.41666666666667      0.50000000000000   Ir  \n" + 
        "     0.00000000000000      0.69583333333333      0.00000000000000   Ir  \n" + 
        "     0.00000000000000      0.30416666666667      0.00000000000000   Ir  \n" + 
        "     0.50000000000000      0.08333333333333      0.50000000000000   Ir  \n" + 
        "     0.50000000000000      0.91666666666667      0.50000000000000   Ir  \n" + 
        "     0.50000000000000      0.19583333333333      0.00000000000000   Ir  \n" + 
        "     0.50000000000000      0.80416666666667      0.00000000000000   Ir  \n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Zn  \n" + 
        "     0.50000000000000      0.50000000000000      0.00000000000000   Zn  \n" + 
        "     0.00000000000000      0.87777777777778      0.00000000000000   B  \n" + 
        "     0.00000000000000      0.12222222222222      0.00000000000000   B  \n" + 
        "     0.00000000000000      0.76527777777778      0.50000000000000   B  \n" + 
        "     0.00000000000000      0.23472222222222      0.50000000000000   B  \n" + 
        "     0.50000000000000      0.37777777777778      0.00000000000000   B  \n" + 
        "     0.50000000000000      0.62222222222222      0.00000000000000   B  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][19] = "Catena-di-mue-chloro-dichloroosmium(IV) - HT <s> Os Cl </s>\n" + 
        "  1.00000000000000\n" + 
        "    -3.96450000000000    -4.16300000000000     0.00000000000000\n" + 
        "     0.00000000000000     8.32600000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000    -3.56000000000000\n" + 
        "  Os    Cl    \n" + 
        "  1     4     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Os  \n" + 
        "     0.43055555555556      0.71527777777778      0.00000000000000   Cl  \n" + 
        "     0.56944444444444      0.28472222222222      0.00000000000000   Cl  \n" + 
        "     1.00000000000000      0.81111111111111     -0.50000000000000   Cl  \n" + 
        "     0.00000000000000      0.18888888888889     -0.50000000000000   Cl  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][2] = "Phosphorus - aP42 <s> P </s>\n" + 
        "  1.00000000000000\n" + 
        "    12.19800000000000     0.00000000000000     0.00000000000000\n" + 
        "    -1.78709982205364    12.86244417776092     0.00000000000000\n" + 
        "    -1.98690212256286    -3.51778495571918     5.80801463158203\n" + 
        "  P     \n" + 
        "  42    \n" + 
        "Direct\n" + 
        "     0.84861111111111      0.66944444444444      0.25277777777778   P  \n" + 
        "     0.15138888888889      0.33055555555556      0.74722222222222   P  \n" + 
        "     0.72500000000000      0.52777777777778      0.25138888888889   P  \n" + 
        "     0.27500000000000      0.47222222222222      0.74861111111111   P  \n" + 
        "     0.84861111111111      0.41944444444444      0.25833333333333   P  \n" + 
        "     0.15138888888889      0.58055555555556      0.74166666666667   P  \n" + 
        "     0.72083333333333      0.27638888888889      0.24027777777778   P  \n" + 
        "     0.27916666666667      0.72361111111111      0.75972222222222   P  \n" + 
        "     0.84027777777778      0.16388888888889      0.26111111111111   P  \n" + 
        "     0.15972222222222      0.83611111111111      0.73888888888889   P  \n" + 
        "     0.71250000000000      0.02777777777778      0.26250000000000   P  \n" + 
        "     0.28750000000000      0.97222222222222      0.73750000000000   P  \n" + 
        "     0.84166666666667      0.92777777777778      0.26666666666667   P  \n" + 
        "     0.15833333333333      0.07222222222222      0.73333333333333   P  \n" + 
        "     0.72222222222222      0.77361111111111      0.24166666666667   P  \n" + 
        "     0.27777777777778      0.22638888888889      0.75833333333333   P  \n" + 
        "     0.64444444444444      0.50694444444444      0.70833333333333   P  \n" + 
        "     0.35555555555556      0.49305555555556      0.29166666666667   P  \n" + 
        "     0.58750000000000      0.41805555555556      0.88194444444444   P  \n" + 
        "     0.41250000000000      0.58194444444444      0.11805555555556   P  \n" + 
        "     0.64722222222222      0.25833333333333      0.70833333333333   P  \n" + 
        "     0.35277777777778      0.74166666666667      0.29166666666667   P  \n" + 
        "     0.58472222222222      0.16805555555556      0.87361111111111   P  \n" + 
        "     0.41527777777778      0.83194444444444      0.12638888888889   P  \n" + 
        "     0.63611111111111      0.00416666666667      0.71666666666667   P  \n" + 
        "     0.36388888888889      0.99583333333333      0.28333333333333   P  \n" + 
        "     0.57500000000000      0.91805555555556      0.89166666666667   P  \n" + 
        "     0.42500000000000      0.08194444444444      0.10833333333333   P  \n" + 
        "     0.64027777777778      0.76388888888889      0.71666666666667   P  \n" + 
        "     0.35972222222222      0.23611111111111      0.28333333333333   P  \n" + 
        "     0.58611111111111      0.66527777777778      0.87361111111111   P  \n" + 
        "     0.41388888888889      0.33472222222222      0.12638888888889   P  \n" + 
        "     0.84166666666667      0.59444444444444      0.89861111111111   P  \n" + 
        "     0.15833333333333      0.40555555555556      0.10138888888889   P  \n" + 
        "     0.84305555555556      0.31527777777778      0.90277777777778   P  \n" + 
        "     0.15694444444444      0.68472222222222      0.09722222222222   P  \n" + 
        "     0.83055555555556      0.04583333333333      0.90833333333333   P  \n" + 
        "     0.16944444444444      0.95416666666667      0.09166666666667   P  \n" + 
        "     0.83472222222222      0.86944444444444      0.91111111111111   P  \n" + 
        "     0.16527777777778      0.13055555555556      0.08888888888889   P  \n" + 
        "     0.92361111111111      0.44861111111111      0.82916666666667   P  \n" + 
        "     0.07638888888889      0.55138888888889      0.17083333333333   P  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][20] = "Rubidium cyclo-phosphide (4/6) <s> Rb P </s>\n" + 
        "  1.00000000000000\n" + 
        "    -4.82050000000000     0.00000000000000    -4.50500000000000\n" + 
        "     0.00000000000000    -7.31450000000000    -4.50500000000000\n" + 
        "     0.00000000000000     0.00000000000000     9.01000000000000\n" + 
        "  Rb    P     \n" + 
        "  4     6     \n" + 
        "Direct\n" + 
        "     0.43611111111111      0.00000000000000      0.71805555555556   Rb  \n" + 
        "     0.56388888888889      0.00000000000000      0.28194444444444   Rb  \n" + 
        "     0.50000000000000      0.50000000000000      0.25000000000000   Rb  \n" + 
        "     0.50000000000000      0.50000000000000      0.75000000000000   Rb  \n" + 
        "     0.00000000000000      0.70277777777778      0.85138888888889   P  \n" + 
        "     0.00000000000000      0.29722222222222      0.14861111111111   P  \n" + 
        "     1.00000000000000      0.85277777777778      0.71944444444444   P  \n" + 
        "     0.00000000000000      0.14722222222222      0.28055555555556   P  \n" + 
        "     0.00000000000000      0.85277777777778      0.13333333333333   P  \n" + 
        "     1.00000000000000      0.14722222222222      0.86666666666667   P  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][21] = "Cesium di-mue-thiotetrathiodiphosphate <s> Cs P S </s>\n" + 
        "  1.00000000000000\n" + 
        "    -4.47200000000000    -3.58500000000000    -4.74500000000000\n" + 
        "     0.00000000000000     7.17000000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000    -9.49000000000000\n" + 
        "  Cs    P     S     \n" + 
        "  2     2     6     \n" + 
        "Direct\n" + 
        "     0.52500000000000      0.26250000000000     -0.26250000000000   Cs  \n" + 
        "     0.47500000000000      0.73750000000000     -0.73750000000000   Cs  \n" + 
        "     1.00000000000000      0.00000000000000     -0.65555555555556   P  \n" + 
        "     1.00000000000000      0.00000000000000     -0.34444444444444   P  \n" + 
        "     0.34722222222222      0.17361111111111     -0.67361111111111   S  \n" + 
        "     0.65277777777778      0.82638888888889     -0.32638888888889   S  \n" + 
        "     1.00000000000000      0.23611111111111     -0.76111111111111   S  \n" + 
        "     1.00000000000000      0.76388888888889     -0.23888888888889   S  \n" + 
        "     1.00000000000000      0.23611111111111     -0.23888888888889   S  \n" + 
        "     1.00000000000000      0.76388888888889     -0.76111111111111   S  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][26] = "Vanadium arsenide (3/2) <s> V As </s>\n" + 
        "  1.00000000000000\n" + 
        "     9.41280000000000     0.00000000000000     0.00000000000000\n" + 
        "     0.00000000000000     9.41280000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     3.33610000000000\n" + 
        "  V     As    \n" + 
        "  12    8     \n" + 
        "Direct\n" + 
        "     0.82083333333333      0.13333333333333      0.50000000000000   V  \n" + 
        "     0.17916666666667      0.86666666666667      0.50000000000000   V  \n" + 
        "     0.13333333333333      0.17916666666667      0.50000000000000   V  \n" + 
        "     0.86666666666667      0.82083333333333      0.50000000000000   V  \n" + 
        "     0.71527777777778      0.39722222222222      0.00000000000000   V  \n" + 
        "     0.28472222222222      0.60277777777778      0.00000000000000   V  \n" + 
        "     0.39722222222222      0.28472222222222      0.00000000000000   V  \n" + 
        "     0.60277777777778      0.71527777777778      0.00000000000000   V  \n" + 
        "     0.50000000000000      0.00000000000000      0.50000000000000   V  \n" + 
        "     0.00000000000000      0.50000000000000      0.50000000000000   V  \n" + 
        "     0.50000000000000      0.50000000000000      0.50000000000000   V  \n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   V  \n" + 
        "     0.58750000000000      0.24583333333333      0.50000000000000   As  \n" + 
        "     0.41250000000000      0.75416666666667      0.50000000000000   As  \n" + 
        "     0.24583333333333      0.41250000000000      0.50000000000000   As  \n" + 
        "     0.75416666666667      0.58750000000000      0.50000000000000   As  \n" + 
        "     0.96111111111111      0.28472222222222      0.00000000000000   As  \n" + 
        "     0.03888888888889      0.71527777777778      0.00000000000000   As  \n" + 
        "     0.28472222222222      0.03888888888889      0.00000000000000   As  \n" + 
        "     0.71527777777778      0.96111111111111      0.00000000000000   As  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][27] = "Gallium telluride (2/5) <s> Te Ga </s>\n" + 
        "  1.00000000000000\n" + 
        "    -3.95650000000000    -3.95650000000000    -3.42400000000000\n" + 
        "     0.00000000000000     7.91300000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000    -6.84800000000000\n" + 
        "  Te    Ga    \n" + 
        "  5     2     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Te  \n" + 
        "     0.66388888888889      0.52222222222222     -0.33194444444444   Te  \n" + 
        "     0.33611111111111      0.47777777777778     -0.66805555555556   Te  \n" + 
        "     0.61944444444444      0.14166666666667     -0.80972222222222   Te  \n" + 
        "     0.38055555555556      0.85833333333333     -0.19027777777778   Te  \n" + 
        "     0.00000000000000      0.50000000000000     -0.25000000000000   Ga  \n" + 
        "     0.00000000000000      0.50000000000000     -0.75000000000000   Ga  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][36] = "Dipotassium tetrabromopalladate <s> Pd K Br </s>\n" + 
        "  1.00000000000000\n" + 
        "     7.40900000000000     0.00000000000000     0.00000000000000\n" + 
        "     0.00000000000000     7.40900000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     4.30900000000000\n" + 
        "  Pd    K     Br    \n" + 
        "  1     2     4     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Pd  \n" + 
        "     0.00000000000000      0.50000000000000      0.50000000000000   K  \n" + 
        "     0.50000000000000      0.00000000000000      0.50000000000000   K  \n" + 
        "     0.23333333333333      0.23333333333333      0.00000000000000   Br  \n" + 
        "     0.76666666666667      0.76666666666667      0.00000000000000   Br  \n" + 
        "     0.76666666666667      0.23333333333333      0.00000000000000   Br  \n" + 
        "     0.23333333333333      0.76666666666667      0.00000000000000   Br  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][37] = "Barium bismuth (2:1) <s> Ba Bi </s>\n" + 
        "  1.00000000000000\n" + 
        "    -2.63150000000000    -2.63150000000000    -9.35000000000000\n" + 
        "     0.00000000000000     5.26300000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000   -18.70000000000000\n" + 
        "  Ba    Bi    \n" + 
        "  4     2     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.50000000000000     -0.50000000000000   Ba  \n" + 
        "     0.00000000000000      0.50000000000000      0.00000000000000   Ba  \n" + 
        "     0.00000000000000      0.00000000000000     -0.67361111111111   Ba  \n" + 
        "     0.00000000000000      0.00000000000000     -0.32638888888889   Ba  \n" + 
        "     1.00000000000000      0.00000000000000     -0.86388888888889   Bi  \n" + 
        "     0.00000000000000      0.00000000000000     -0.13611111111111   Bi  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][40] = "Iron(III) chloride - III <s> Fe Cl </s>\n" + 
        "  1.00000000000000\n" + 
        "     6.06300000000000     0.00000000000000     0.00000000000000\n" + 
        "    -3.03150000000000     5.25071202314505     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000001    69.70000000000000\n" + 
        "  Fe    Cl    \n" + 
        "  24    72    \n" + 
        "Direct\n" + 
        "     1.00000000000000      1.00000000000000      0.95833333333333   Fe  \n" + 
        "     1.00000000000000      1.00000000000000      0.04166666666667   Fe  \n" + 
        "     0.66666666666667      0.33333333333333      0.95833333333333   Fe  \n" + 
        "     0.33333333333333      0.66666666666667      0.04166666666667   Fe  \n" + 
        "     0.33333333333333      0.66666666666667      0.87500000000000   Fe  \n" + 
        "     0.66666666666667      0.33333333333333      0.12500000000000   Fe  \n" + 
        "     0.66666666666667      0.33333333333333      0.87500000000000   Fe  \n" + 
        "     0.33333333333333      0.66666666666667      0.12500000000000   Fe  \n" + 
        "     0.66666666666667      0.33333333333333      0.79166666666667   Fe  \n" + 
        "     0.33333333333333      0.66666666666667      0.20833333333333   Fe  \n" + 
        "     0.00000000000000      0.00000000000000      0.79027777777778   Fe  \n" + 
        "     0.00000000000000      1.00000000000000      0.20972222222222   Fe  \n" + 
        "     0.33333333333333      0.66666666666667      0.70833333333333   Fe  \n" + 
        "     0.66666666666667      0.33333333333333      0.29166666666667   Fe  \n" + 
        "     1.00000000000000      0.00000000000000      0.70833333333333   Fe  \n" + 
        "     0.00000000000000      0.00000000000000      0.29166666666667   Fe  \n" + 
        "     0.33333333333333      0.66666666666667      0.62500000000000   Fe  \n" + 
        "     0.66666666666667      0.33333333333333      0.37500000000000   Fe  \n" + 
        "     0.66666666666667      0.33333333333333      0.62500000000000   Fe  \n" + 
        "     0.33333333333333      0.66666666666667      0.37500000000000   Fe  \n" + 
        "     0.66666666666667      0.33333333333333      0.54166666666667   Fe  \n" + 
        "     0.33333333333333      0.66666666666667      0.45833333333333   Fe  \n" + 
        "     0.00000000000000      0.00000000000000      0.54166666666667   Fe  \n" + 
        "     0.00000000000000      0.00000000000000      0.45833333333333   Fe  \n" + 
        "     0.98055555555556      0.66388888888889      0.97777777777778   Cl  \n" + 
        "     0.68333333333333      0.01944444444444      0.97777777777778   Cl  \n" + 
        "     0.33611111111111      0.31666666666667      0.97777777777778   Cl  \n" + 
        "     0.01944444444444      0.33611111111111      0.02222222222222   Cl  \n" + 
        "     0.31666666666667      0.98055555555556      0.02222222222222   Cl  \n" + 
        "     0.66388888888889      0.68333333333333      0.02222222222222   Cl  \n" + 
        "     0.69861111111111      0.66805555555556      0.93888888888889   Cl  \n" + 
        "     0.96944444444445      0.30138888888889      0.93888888888889   Cl  \n" + 
        "     0.33194444444444      0.03055555555556      0.93888888888889   Cl  \n" + 
        "     0.30138888888889      0.33194444444444      0.06111111111111   Cl  \n" + 
        "     0.03055555555556      0.69861111111111      0.06111111111111   Cl  \n" + 
        "     0.66805555555556      0.96944444444445      0.06111111111111   Cl  \n" + 
        "     0.64027777777778      0.98888888888889      0.89583333333333   Cl  \n" + 
        "     0.34861111111111      0.35972222222222      0.89583333333333   Cl  \n" + 
        "     0.01111111111111      0.65138888888889      0.89583333333333   Cl  \n" + 
        "     0.35972222222222      0.01111111111111      0.10416666666667   Cl  \n" + 
        "     0.65138888888889      0.64027777777778      0.10416666666667   Cl  \n" + 
        "     0.98888888888889      0.34861111111111      0.10416666666667   Cl  \n" + 
        "     0.34583333333333      0.00138888888889      0.85833333333333   Cl  \n" + 
        "     0.65555555555556      0.65416666666667      0.85833333333333   Cl  \n" + 
        "     0.99861111111111      0.34444444444444      0.85833333333333   Cl  \n" + 
        "     0.65416666666667      0.99861111111111      0.14166666666667   Cl  \n" + 
        "     0.34444444444444      0.34583333333333      0.14166666666667   Cl  \n" + 
        "     0.00138888888889      0.65555555555556      0.14166666666667   Cl  \n" + 
        "     0.95833333333333      0.66527777777778      0.81250000000000   Cl  \n" + 
        "     0.70694444444445      0.04166666666667      0.81250000000000   Cl  \n" + 
        "     0.33472222222222      0.29305555555556      0.81250000000000   Cl  \n" + 
        "     0.04166666666667      0.33472222222222      0.18750000000000   Cl  \n" + 
        "     0.29305555555556      0.95833333333333      0.18750000000000   Cl  \n" + 
        "     0.66527777777778      0.70694444444445      0.18750000000000   Cl  \n" + 
        "     0.70694444444444      0.66250000000000      0.77083333333333   Cl  \n" + 
        "     0.95555555555556      0.29305555555556      0.77083333333333   Cl  \n" + 
        "     0.33750000000000      0.04444444444444      0.77083333333333   Cl  \n" + 
        "     0.29305555555556      0.33750000000000      0.22916666666667   Cl  \n" + 
        "     0.04444444444444      0.70694444444444      0.22916666666667   Cl  \n" + 
        "     0.66250000000000      0.95555555555556      0.22916666666667   Cl  \n" + 
        "     0.67361111111111      0.97916666666667      0.72916666666667   Cl  \n" + 
        "     0.30555555555556      0.32638888888889      0.72916666666667   Cl  \n" + 
        "     0.02083333333333      0.69444444444444      0.72916666666667   Cl  \n" + 
        "     0.32638888888889      0.02083333333333      0.27083333333333   Cl  \n" + 
        "     0.69444444444444      0.67361111111111      0.27083333333333   Cl  \n" + 
        "     0.97916666666667      0.30555555555556      0.27083333333333   Cl  \n" + 
        "     0.68055555555556      0.69861111111111      0.68888888888889   Cl  \n" + 
        "     0.01805555555556      0.31944444444444      0.68888888888889   Cl  \n" + 
        "     0.30138888888889      0.98194444444444      0.68888888888889   Cl  \n" + 
        "     0.31944444444444      0.30138888888889      0.31111111111111   Cl  \n" + 
        "     0.98194444444444      0.68055555555556      0.31111111111111   Cl  \n" + 
        "     0.69861111111111      0.01805555555556      0.31111111111111   Cl  \n" + 
        "     0.63888888888889      0.99027777777778      0.64444444444444   Cl  \n" + 
        "     0.35138888888889      0.36111111111111      0.64444444444444   Cl  \n" + 
        "     0.00972222222222      0.64861111111111      0.64444444444444   Cl  \n" + 
        "     0.36111111111111      0.00972222222222      0.35555555555556   Cl  \n" + 
        "     0.64861111111111      0.63888888888889      0.35555555555556   Cl  \n" + 
        "     0.99027777777778      0.35138888888889      0.35555555555556   Cl  \n" + 
        "     0.35138888888889      0.99722222222222      0.60694444444444   Cl  \n" + 
        "     0.64583333333333      0.64861111111111      0.60694444444444   Cl  \n" + 
        "     0.00277777777778      0.35416666666667      0.60694444444444   Cl  \n" + 
        "     0.64861111111111      0.00277777777778      0.39305555555556   Cl  \n" + 
        "     0.35416666666667      0.35138888888889      0.39305555555556   Cl  \n" + 
        "     0.99722222222222      0.64583333333333      0.39305555555556   Cl  \n" + 
        "     0.32916666666667      0.29861111111111      0.56111111111111   Cl  \n" + 
        "     0.96944444444444      0.67083333333333      0.56111111111111   Cl  \n" + 
        "     0.70138888888889      0.03055555555556      0.56111111111111   Cl  \n" + 
        "     0.67083333333333      0.70138888888889      0.43888888888889   Cl  \n" + 
        "     0.03055555555556      0.32916666666667      0.43888888888889   Cl  \n" + 
        "     0.29861111111111      0.96944444444444      0.43888888888889   Cl  \n" + 
        "     0.33333333333333      0.03194444444444      0.52222222222222   Cl  \n" + 
        "     0.69861111111111      0.66666666666667      0.52222222222222   Cl  \n" + 
        "     0.96805555555556      0.30138888888889      0.52222222222222   Cl  \n" + 
        "     0.66666666666667      0.96805555555556      0.47777777777778   Cl  \n" + 
        "     0.30138888888889      0.33333333333333      0.47777777777778   Cl  \n" + 
        "     0.03194444444444      0.69861111111111      0.47777777777778   Cl  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][41] = "Palladium phosphide (15/2) <s> Pd P </s>\n" + 
        "  1.00000000000000\n" + 
        "     3.55335000000000    -2.05152757902496    -5.69556666666667\n" + 
        "    -3.55335000000000     6.15458273707487     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000    17.08670000000000\n" + 
        "  Pd    P     \n" + 
        "  15    2     \n" + 
        "Direct\n" + 
        "     0.34583333333333      0.51527777777778      0.15138888888889   Pd  \n" + 
        "     0.85416666666667      0.16944444444444      0.32083333333333   Pd  \n" + 
        "     0.80000000000000      0.31527777777778      0.63611111111111   Pd  \n" + 
        "     0.65416666666667      0.48472222222222      0.84861111111111   Pd  \n" + 
        "     0.14583333333333      0.83055555555556      0.67916666666667   Pd  \n" + 
        "     0.20000000000000      0.68472222222222      0.36388888888889   Pd  \n" + 
        "     0.06250000000000      0.27916666666667      0.22500000000000   Pd  \n" + 
        "     0.71250000000000      0.21666666666667      0.44166666666667   Pd  \n" + 
        "     0.22500000000000      0.50416666666667      0.94583333333333   Pd  \n" + 
        "     0.93750000000000      0.72083333333333      0.77500000000000   Pd  \n" + 
        "     0.28750000000000      0.78333333333333      0.55833333333333   Pd  \n" + 
        "     0.77500000000000      0.49583333333333      0.05416666666667   Pd  \n" + 
        "     0.00000000000000      0.00000000000000      0.92083333333333   Pd  \n" + 
        "     0.00000000000000      1.00000000000000      0.07916666666667   Pd  \n" + 
        "     0.00000000000000      0.00000000000000      0.50000000000000   Pd  \n" + 
        "     0.00000000000000      0.00000000000000      0.71388888888889   P  \n" + 
        "     0.00000000000000      0.00000000000000      0.28611111111111   P  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][48] = "Chromium nitride (2/1) <s> Cr N </s>\n" + 
        "  1.00000000000000\n" + 
        "     4.75200000000000     0.00000000000000     0.00000000000000\n" + 
        "    -2.37600000000000     4.11535271878365     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     4.42900000000000\n" + 
        "  Cr    N     \n" + 
        "  6     3     \n" + 
        "Direct\n" + 
        "     0.66666666666667      0.00000000000000      0.75138888888889   Cr  \n" + 
        "     0.33333333333333      0.33333333333333      0.75138888888889   Cr  \n" + 
        "     1.00000000000000      0.66666666666667      0.75138888888889   Cr  \n" + 
        "     0.33333333333333      1.00000000000000      0.24861111111111   Cr  \n" + 
        "     0.66666666666667      0.66666666666667      0.24861111111111   Cr  \n" + 
        "     0.00000000000000      0.33333333333333      0.24861111111111   Cr  \n" + 
        "     0.33333333333333      0.66666666666667      0.50000000000000   N  \n" + 
        "     0.66666666666667      0.33333333333333      0.50000000000000   N  \n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   N  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][49] = "Magnesium antimonide (3/2) - alpha <s> Mg Sb </s>\n" + 
        "  1.00000000000000\n" + 
        "     4.56800000000000     0.00000000000000     0.00000000000000\n" + 
        "    -2.28400000000000     3.95600404448732     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     7.22900000000000\n" + 
        "  Mg    Sb    \n" + 
        "  3     2     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Mg  \n" + 
        "     0.66666666666667      0.33333333333333      0.36666666666667   Mg  \n" + 
        "     0.33333333333333      0.66666666666667      0.63333333333333   Mg  \n" + 
        "     0.66666666666667      0.33333333333333      0.77222222222222   Sb  \n" + 
        "     0.33333333333333      0.66666666666667      0.22777777777778   Sb  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][50] = "Iron tin (3/2) <s> Fe Sn </s>\n" + 
        "  1.00000000000000\n" + 
        "     2.67200000000000    -1.54267991927468    -6.61500000000000\n" + 
        "    -2.67200000000000     4.62803975782404     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000    19.84500000000000\n" + 
        "  Fe    Sn    \n" + 
        "  6     4     \n" + 
        "Direct\n" + 
        "     0.96666666666667      0.48333333333333      0.87500000000000   Fe  \n" + 
        "     0.51666666666667      0.51666666666667      0.39166666666667   Fe  \n" + 
        "     0.51666666666667      1.00000000000000      0.39166666666667   Fe  \n" + 
        "     0.03333333333333      0.51666666666667      0.12500000000000   Fe  \n" + 
        "     0.48333333333333      0.48333333333333      0.60833333333333   Fe  \n" + 
        "     0.48333333333333      1.00000000000000      0.60833333333333   Fe  \n" + 
        "     0.00000000000000      0.00000000000000      0.89583333333333   Sn  \n" + 
        "     0.00000000000000      1.00000000000000      0.10416666666667   Sn  \n" + 
        "     0.00000000000000      1.00000000000000      0.66805555555556   Sn  \n" + 
        "     0.00000000000000      0.00000000000000      0.33194444444444   Sn  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][53] = "Magnesium imide - deuterated <s> Mg D N </s>\n" + 
        "  1.00000000000000\n" + 
        "    11.57961000000000     0.00000000000000     0.00000000000000\n" + 
        "    -5.78980500000000    10.02823642591633     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     3.68110000000000\n" + 
        "  Mg    D     N     \n" + 
        "  12    12    12    \n" + 
        "Direct\n" + 
        "     0.28472222222222      0.41666666666667      0.00000000000000   Mg  \n" + 
        "     0.13194444444444      0.71527777777778      0.00000000000000   Mg  \n" + 
        "     0.58333333333333      0.86805555555556      0.00000000000000   Mg  \n" + 
        "     0.71527777777778      0.58333333333333      0.00000000000000   Mg  \n" + 
        "     0.86805555555556      0.28472222222222      0.00000000000000   Mg  \n" + 
        "     0.41666666666667      0.13194444444444      0.00000000000000   Mg  \n" + 
        "     0.64305555555556      0.07083333333333      0.50000000000000   Mg  \n" + 
        "     0.42777777777778      0.35694444444444      0.50000000000000   Mg  \n" + 
        "     0.92916666666667      0.57222222222222      0.50000000000000   Mg  \n" + 
        "     0.35694444444444      0.92916666666667      0.50000000000000   Mg  \n" + 
        "     0.57222222222222      0.64305555555556      0.50000000000000   Mg  \n" + 
        "     0.07083333333333      0.42777777777778      0.50000000000000   Mg  \n" + 
        "     0.33888888888889      0.57222222222222      0.50000000000000   D  \n" + 
        "     0.23333333333333      0.66111111111111      0.50000000000000   D  \n" + 
        "     0.42777777777778      0.76666666666667      0.50000000000000   D  \n" + 
        "     0.66111111111111      0.42777777777778      0.50000000000000   D  \n" + 
        "     0.76666666666667      0.33888888888889      0.50000000000000   D  \n" + 
        "     0.57222222222222      0.23333333333333      0.50000000000000   D  \n" + 
        "     0.80416666666667      0.05972222222222      0.00000000000000   D  \n" + 
        "     0.25555555555556      0.19583333333333      0.00000000000000   D  \n" + 
        "     0.94027777777778      0.74444444444444      0.00000000000000   D  \n" + 
        "     0.19583333333333      0.94027777777778      0.00000000000000   D  \n" + 
        "     0.74444444444444      0.80416666666667      0.00000000000000   D  \n" + 
        "     0.05972222222222      0.25555555555556      0.00000000000000   D  \n" + 
        "     0.36805555555556      0.50416666666667      0.50000000000000   N  \n" + 
        "     0.13611111111111      0.63194444444444      0.50000000000000   N  \n" + 
        "     0.49583333333333      0.86388888888889      0.50000000000000   N  \n" + 
        "     0.63194444444444      0.49583333333333      0.50000000000000   N  \n" + 
        "     0.86388888888889      0.36805555555556      0.50000000000000   N  \n" + 
        "     0.50416666666667      0.13611111111111      0.50000000000000   N  \n" + 
        "     0.07638888888889      0.34861111111111      0.00000000000000   N  \n" + 
        "     0.27222222222222      0.92361111111111      0.00000000000000   N  \n" + 
        "     0.65138888888889      0.72777777777778      0.00000000000000   N  \n" + 
        "     0.92361111111111      0.65138888888889      0.00000000000000   N  \n" + 
        "     0.72777777777778      0.07638888888889      0.00000000000000   N  \n" + 
        "     0.34861111111111      0.27222222222222      0.00000000000000   N  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][58] = "Silicon - HP <s> Si </s>\n" + 
        "  1.00000000000000\n" + 
        "     2.52700000000000     0.00000000000000     0.00000000000000\n" + 
        "    -1.26350000000000     2.18844619536328     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     2.37300000000000\n" + 
        "  Si    \n" + 
        "  1     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Si  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][62] = "Cadmium sodium (11/2) <s> Cd Na </s>\n" + 
        "  1.00000000000000\n" + 
        "     9.60500000000000     0.00000000000000     0.00000000000000\n" + 
        "     0.00000000000000     9.60500000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     9.60500000000000\n" + 
        "  Cd    Na    \n" + 
        "  33    6     \n" + 
        "Direct\n" + 
        "     0.50000000000000      0.50000000000000      0.50000000000000   Cd  \n" + 
        "     0.00000000000000      0.23194444444444      0.00000000000000   Cd  \n" + 
        "     0.00000000000000      0.76805555555556      0.00000000000000   Cd  \n" + 
        "     0.00000000000000      0.00000000000000      0.23194444444444   Cd  \n" + 
        "     1.00000000000000      0.00000000000000      0.76805555555556   Cd  \n" + 
        "     0.23194444444444      0.00000000000000      0.00000000000000   Cd  \n" + 
        "     0.76805555555556      0.00000000000000      0.00000000000000   Cd  \n" + 
        "     0.00000000000000      0.16111111111111      0.50000000000000   Cd  \n" + 
        "     1.00000000000000      0.83888888888889      0.50000000000000   Cd  \n" + 
        "     0.50000000000000      0.00000000000000      0.16111111111111   Cd  \n" + 
        "     0.50000000000000      1.00000000000000      0.83888888888889   Cd  \n" + 
        "     0.16111111111111      0.50000000000000      0.00000000000000   Cd  \n" + 
        "     0.83888888888889      0.50000000000000      0.00000000000000   Cd  \n" + 
        "     0.78194444444444      0.21805555555556      0.21805555555556   Cd  \n" + 
        "     0.21805555555556      0.21805555555556      0.78194444444444   Cd  \n" + 
        "     0.21805555555556      0.78194444444444      0.21805555555556   Cd  \n" + 
        "     0.78194444444444      0.78194444444444      0.78194444444444   Cd  \n" + 
        "     0.21805555555556      0.78194444444444      0.78194444444444   Cd  \n" + 
        "     0.78194444444444      0.78194444444444      0.21805555555556   Cd  \n" + 
        "     0.78194444444444      0.21805555555556      0.78194444444444   Cd  \n" + 
        "     0.21805555555556      0.21805555555556      0.21805555555556   Cd  \n" + 
        "     0.65694444444444      0.50000000000000      0.23472222222222   Cd  \n" + 
        "     0.34305555555556      0.50000000000000      0.76527777777778   Cd  \n" + 
        "     0.34305555555556      0.50000000000000      0.23472222222222   Cd  \n" + 
        "     0.65694444444444      0.50000000000000      0.76527777777778   Cd  \n" + 
        "     0.23472222222222      0.65694444444444      0.50000000000000   Cd  \n" + 
        "     0.76527777777778      0.34305555555556      0.50000000000000   Cd  \n" + 
        "     0.23472222222222      0.34305555555556      0.50000000000000   Cd  \n" + 
        "     0.76527777777778      0.65694444444444      0.50000000000000   Cd  \n" + 
        "     0.50000000000000      0.23472222222222      0.65694444444444   Cd  \n" + 
        "     0.50000000000000      0.76527777777778      0.34305555555556   Cd  \n" + 
        "     0.50000000000000      0.23472222222222      0.34305555555556   Cd  \n" + 
        "     0.50000000000000      0.76527777777778      0.65694444444444   Cd  \n" + 
        "     0.00000000000000      0.31111111111111      0.00000000000000   Na  \n" + 
        "     0.00000000000000      0.68888888888889      0.00000000000000   Na  \n" + 
        "     0.00000000000000      0.00000000000000      0.31111111111111   Na  \n" + 
        "     0.00000000000000      0.00000000000000      0.68888888888889   Na  \n" + 
        "     0.31111111111111      0.00000000000000      0.00000000000000   Na  \n" + 
        "     0.68888888888889      0.00000000000000      0.00000000000000   Na  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][63] = "Carbon <s> C </s>\n" + 
        "  1.00000000000000\n" + 
        "    -7.13000000000000    -7.13000000000000     0.00000000000000\n" + 
        "     0.00000000000000    -7.13000000000000    -7.13000000000000\n" + 
        "     0.00000000000000     0.00000000000000    14.26000000000000\n" + 
        "  C     \n" + 
        "  60    \n" + 
        "Direct\n" + 
        "     0.50277777777778      0.39444444444444      0.19722222222222   C  \n" + 
        "     0.00000000000000      0.50277777777778      0.70000000000000   C  \n" + 
        "     0.89722222222222      0.10277777777778      0.80277777777778   C  \n" + 
        "     0.49722222222222      0.60555555555556      0.80277777777778   C  \n" + 
        "     0.00000000000000      0.49722222222222      0.30000000000000   C  \n" + 
        "     0.10277777777778      0.89722222222222      0.19722222222222   C  \n" + 
        "     0.50277777777778      0.60000000000000      0.30000000000000   C  \n" + 
        "     0.89722222222222      0.10277777777778      0.30000000000000   C  \n" + 
        "     0.49722222222222      0.40000000000000      0.70000000000000   C  \n" + 
        "     0.10277777777778      0.89722222222222      0.70000000000000   C  \n" + 
        "     0.00000000000000      0.50277777777778      0.80277777777778   C  \n" + 
        "     0.00000000000000      0.49722222222222      0.19722222222222   C  \n" + 
        "     0.56111111111111      0.22777777777778      0.02916666666667   C  \n" + 
        "     0.83055555555556      0.73055555555556      0.75972222222222   C  \n" + 
        "     0.78888888888889      0.04166666666667      0.80138888888889   C  \n" + 
        "     0.43888888888889      0.77222222222222      0.97083333333333   C  \n" + 
        "     0.16944444444444      0.26944444444444      0.24027777777778   C  \n" + 
        "     0.21111111111111      0.95833333333333      0.19861111111111   C  \n" + 
        "     0.56111111111111      0.65000000000000      0.40972222222222   C  \n" + 
        "     0.83055555555556      0.60833333333333      0.40972222222222   C  \n" + 
        "     0.78888888888889      0.38055555555556      0.40972222222222   C  \n" + 
        "     0.43888888888889      0.35000000000000      0.59027777777778   C  \n" + 
        "     0.16944444444444      0.39166666666667      0.59027777777778   C  \n" + 
        "     0.21111111111111      0.61944444444444      0.59027777777778   C  \n" + 
        "     0.43888888888889      0.35000000000000      0.75972222222222   C  \n" + 
        "     0.16944444444444      0.39166666666667      0.80138888888889   C  \n" + 
        "     0.21111111111111      0.61944444444444      0.02916666666667   C  \n" + 
        "     0.56111111111111      0.65000000000000      0.24027777777778   C  \n" + 
        "     0.83055555555556      0.60833333333333      0.19861111111111   C  \n" + 
        "     0.78888888888889      0.38055555555556      0.97083333333333   C  \n" + 
        "     0.43888888888889      0.77222222222222      0.80138888888889   C  \n" + 
        "     0.16944444444444      0.26944444444444      0.02916666666667   C  \n" + 
        "     0.21111111111111      0.95833333333333      0.75972222222222   C  \n" + 
        "     0.56111111111111      0.22777777777778      0.19861111111111   C  \n" + 
        "     0.83055555555556      0.73055555555556      0.97083333333333   C  \n" + 
        "     0.78888888888889      0.04166666666667      0.24027777777778   C  \n" + 
        "     0.66944444444444      0.96111111111111      0.92916666666667   C  \n" + 
        "     0.89722222222222      0.77222222222222      0.70138888888889   C  \n" + 
        "     0.63055555555556      0.26666666666667      0.96805555555556   C  \n" + 
        "     0.33055555555556      0.03888888888889      0.07083333333333   C  \n" + 
        "     0.10277777777778      0.22777777777778      0.29861111111111   C  \n" + 
        "     0.36944444444445      0.73333333333333      0.03194444444444   C  \n" + 
        "     0.66944444444444      0.70000000000000      0.40138888888889   C  \n" + 
        "     0.89722222222222      0.43333333333333      0.40138888888889   C  \n" + 
        "     0.63055555555556      0.47222222222222      0.40138888888889   C  \n" + 
        "     0.33055555555556      0.30000000000000      0.59861111111111   C  \n" + 
        "     0.10277777777778      0.56666666666667      0.59861111111111   C  \n" + 
        "     0.36944444444444      0.52777777777778      0.59861111111111   C  \n" + 
        "     0.33055555555556      0.30000000000000      0.70138888888889   C  \n" + 
        "     0.10277777777778      0.56666666666667      0.96805555555556   C  \n" + 
        "     0.36944444444444      0.52777777777778      0.92916666666667   C  \n" + 
        "     0.66944444444444      0.70000000000000      0.29861111111111   C  \n" + 
        "     0.89722222222222      0.43333333333333      0.03194444444444   C  \n" + 
        "     0.63055555555556      0.47222222222222      0.07083333333333   C  \n" + 
        "     0.33055555555556      0.03888888888889      0.96805555555556   C  \n" + 
        "     0.10277777777778      0.22777777777778      0.92916666666667   C  \n" + 
        "     0.36944444444445      0.73333333333333      0.70138888888889   C  \n" + 
        "     0.66944444444444      0.96111111111111      0.03194444444444   C  \n" + 
        "     0.89722222222222      0.77222222222222      0.07083333333333   C  \n" + 
        "     0.63055555555556      0.26666666666667      0.29861111111111   C  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][64] = "Potassium <s> K </s>\n" + 
        "  1.00000000000000\n" + 
        "    -7.89450000000000    -7.89450000000000    -7.89450000000000\n" + 
        "     0.00000000000000    15.78900000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000   -15.78900000000000\n" + 
        "  K     \n" + 
        "  81    \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   K  \n" + 
        "     0.00000000000000      0.89583333333333     -0.50000000000000   K  \n" + 
        "     0.00000000000000      0.10416666666667     -0.50000000000000   K  \n" + 
        "     0.00000000000000      0.50000000000000     -0.39583333333333   K  \n" + 
        "     0.00000000000000      0.50000000000000     -0.60416666666667   K  \n" + 
        "     0.20833333333333      0.60416666666667     -0.10416666666667   K  \n" + 
        "     0.79166666666667      0.39583333333333     -0.89583333333333   K  \n" + 
        "     0.00000000000000      0.69861111111111     -0.50000000000000   K  \n" + 
        "     0.00000000000000      0.30138888888889     -0.50000000000000   K  \n" + 
        "     1.00000000000000      0.50000000000000     -0.19861111111111   K  \n" + 
        "     0.00000000000000      0.50000000000000     -0.80138888888889   K  \n" + 
        "     0.60277777777778      0.80138888888889     -0.30138888888889   K  \n" + 
        "     0.39722222222222      0.19861111111111     -0.69861111111111   K  \n" + 
        "     0.36666666666667      0.36666666666667     -0.36666666666667   K  \n" + 
        "     0.63333333333333      0.00000000000000     -0.63333333333333   K  \n" + 
        "     0.63333333333333      0.63333333333333      0.00000000000000   K  \n" + 
        "     0.36666666666667      0.00000000000000      0.00000000000000   K  \n" + 
        "     0.63333333333333      0.63333333333333     -0.63333333333333   K  \n" + 
        "     0.36666666666667      0.00000000000000     -0.36666666666667   K  \n" + 
        "     0.36666666666667      0.36666666666667      0.00000000000000   K  \n" + 
        "     0.63333333333333      0.00000000000000      0.00000000000000   K  \n" + 
        "     0.30833333333333      0.15416666666667     -0.25000000000000   K  \n" + 
        "     0.69166666666667      0.84583333333333     -0.75000000000000   K  \n" + 
        "     0.69166666666667      0.84583333333333     -0.94166666666667   K  \n" + 
        "     0.30833333333333      0.15416666666667     -0.05833333333333   K  \n" + 
        "     0.80833333333333      0.75000000000000     -0.90416666666667   K  \n" + 
        "     0.19166666666667      0.25000000000000     -0.09583333333333   K  \n" + 
        "     0.80833333333333      0.05833333333333     -0.90416666666667   K  \n" + 
        "     0.19166666666667      0.94166666666667     -0.09583333333333   K  \n" + 
        "     0.00000000000000      0.09583333333333     -0.84583333333333   K  \n" + 
        "     0.00000000000000      0.90416666666667     -0.15416666666667   K  \n" + 
        "     1.00000000000000      0.09583333333333     -0.15416666666667   K  \n" + 
        "     1.00000000000000      0.90416666666667     -0.84583333333333   K  \n" + 
        "     0.61666666666667      0.30833333333333     -0.50000000000000   K  \n" + 
        "     0.38333333333333      0.69166666666667     -0.50000000000000   K  \n" + 
        "     0.38333333333333      0.69166666666667     -0.88333333333333   K  \n" + 
        "     0.61666666666667      0.30833333333333     -0.11666666666667   K  \n" + 
        "     0.61666666666667      0.50000000000000     -0.80833333333333   K  \n" + 
        "     0.38333333333333      0.50000000000000     -0.19166666666667   K  \n" + 
        "     0.61666666666667      0.11666666666667     -0.80833333333333   K  \n" + 
        "     0.38333333333333      0.88333333333333     -0.19166666666667   K  \n" + 
        "     0.00000000000000      0.19166666666667     -0.69166666666667   K  \n" + 
        "     0.00000000000000      0.80833333333333     -0.30833333333333   K  \n" + 
        "     1.00000000000000      0.19166666666667     -0.30833333333333   K  \n" + 
        "     0.00000000000000      0.80833333333333     -0.69166666666667   K  \n" + 
        "     0.26111111111111      0.13055555555556     -0.43194444444444   K  \n" + 
        "     0.73888888888889      0.86944444444444     -0.56805555555556   K  \n" + 
        "     0.73888888888889      0.86944444444444     -0.17083333333333   K  \n" + 
        "     0.26111111111111      0.13055555555556     -0.82916666666667   K  \n" + 
        "     0.39722222222222      0.56805555555556     -0.69861111111111   K  \n" + 
        "     0.60277777777778      0.43194444444444     -0.30138888888889   K  \n" + 
        "     0.39722222222222      0.82916666666667     -0.69861111111111   K  \n" + 
        "     0.60277777777778      0.17083333333333     -0.30138888888889   K  \n" + 
        "     1.00000000000000      0.30138888888889     -0.86944444444444   K  \n" + 
        "     0.00000000000000      0.69861111111111     -0.13055555555556   K  \n" + 
        "     0.00000000000000      0.30138888888889     -0.13055555555556   K  \n" + 
        "     0.00000000000000      0.69861111111111     -0.86944444444444   K  \n" + 
        "     0.80833333333333      0.55833333333333     -0.59583333333333   K  \n" + 
        "     0.19166666666667      0.75000000000000     -0.40416666666667   K  \n" + 
        "     0.19166666666667      0.44166666666667     -0.78750000000000   K  \n" + 
        "     0.80833333333333      0.25000000000000     -0.21250000000000   K  \n" + 
        "     0.61666666666667      0.40416666666667     -0.96250000000000   K  \n" + 
        "     0.38333333333333      0.59583333333333     -0.34583333333333   K  \n" + 
        "     0.61666666666667      0.21250000000000     -0.65416666666667   K  \n" + 
        "     0.38333333333333      0.78750000000000     -0.03750000000000   K  \n" + 
        "     0.69166666666667      0.03750000000000     -0.44166666666667   K  \n" + 
        "     0.69166666666667      0.65416666666667     -0.25000000000000   K  \n" + 
        "     0.30833333333333      0.34583333333333     -0.55833333333333   K  \n" + 
        "     0.30833333333333      0.96250000000000     -0.75000000000000   K  \n" + 
        "     0.19166666666667      0.44166666666667     -0.40416666666667   K  \n" + 
        "     0.80833333333333      0.25000000000000     -0.59583333333333   K  \n" + 
        "     0.80833333333333      0.55833333333333     -0.21250000000000   K  \n" + 
        "     0.19166666666667      0.75000000000000     -0.78750000000000   K  \n" + 
        "     0.38333333333333      0.59583333333333     -0.03750000000000   K  \n" + 
        "     0.61666666666667      0.40416666666667     -0.65416666666667   K  \n" + 
        "     0.38333333333333      0.78750000000000     -0.34583333333333   K  \n" + 
        "     0.61666666666667      0.21250000000000     -0.96250000000000   K  \n" + 
        "     0.30833333333333      0.96250000000000     -0.55833333333333   K  \n" + 
        "     0.30833333333333      0.34583333333333     -0.75000000000000   K  \n" + 
        "     0.69166666666667      0.65416666666667     -0.44166666666667   K  \n" + 
        "     0.69166666666667      0.03750000000000     -0.25000000000000   K  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][7] = "Gold(III) silver telluride <s> Au Ag Te </s>\n" + 
        "  1.00000000000000\n" + 
        "     5.12400000000000     0.00000000000000     0.00000000000000\n" + 
        "     0.00000000000000     4.41900000000000     0.00000000000000\n" + 
        "     0.00519200503708     0.00000000000000     7.43699818764827\n" + 
        "  Au    Ag    Te    \n" + 
        "  2     2     4     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Au  \n" + 
        "     0.00000000000000      0.00000000000000      0.50000000000000   Au  \n" + 
        "     0.50000000000000      0.50000000000000      0.00000000000000   Ag  \n" + 
        "     0.50000000000000      0.50000000000000      0.50000000000000   Ag  \n" + 
        "     0.37222222222222      0.00000000000000      0.74861111111111   Te  \n" + 
        "     0.62777777777778      0.00000000000000      0.25138888888889   Te  \n" + 
        "     0.89166666666667      0.50000000000000      0.74166666666667   Te  \n" + 
        "     0.10833333333333      0.50000000000000      0.25833333333333   Te  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][71] = "Polonium - alpha <s> Po </s>\n" + 
        "  1.00000000000000\n" + 
        "     3.35900000000000     0.00000000000000     0.00000000000000\n" + 
        "     0.00000000000000     3.35900000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     3.35900000000000\n" + 
        "  Po    \n" + 
        "  1     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Po  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][72] = "Lead <s> Pb </s>\n" + 
        "  1.00000000000000\n" + 
        "    -2.47500000000000     0.00000000000000    -2.47500000000000\n" + 
        "    -2.47500000000000    -2.47500000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000     4.95000000000000\n" + 
        "  Pb    \n" + 
        "  1     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Pb  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][73] = "Vanadium <s> V </s>\n" + 
        "  1.00000000000000\n" + 
        "    -1.51390000000000    -1.51390000000000    -1.51390000000000\n" + 
        "     0.00000000000000     3.02780000000000     0.00000000000000\n" + 
        "     0.00000000000000     0.00000000000000    -3.02780000000000\n" + 
        "  V     \n" + 
        "  1     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   V  \n" + 
        "";
        SYMMORPHIC_SPACE_GROUP_STRUCTS[3][8] = "Mercury titanium sulfide (1.24/1/2) <s> Hg </s>\n" + 
        "  1.00000000000000\n" + 
        "    -2.96115000000000    -1.37830000000000     0.00000000000000\n" + 
        "     0.00000000000000     2.75660000000000     0.00000000000000\n" + 
        "     1.89240864495256     0.00000000000000    -8.65758820460461\n" + 
        "  Hg    \n" + 
        "  1     \n" + 
        "Direct\n" + 
        "     0.00000000000000      0.00000000000000      0.00000000000000   Hg  \n" + 
        "";
  }
  
  /** 
   * @param arithmeticClassNumber The indices corresponding to the centrosymmetric
   *                              space group within all symmorphic space group. 25
   *                              in total.
   */
  public static SpaceGroup getSymmorphicSpaceGroup(int numDimensions, int arithmeticClassNumber) {
    
    if (SYMMORPHIC_SPACE_GROUPS[numDimensions][arithmeticClassNumber] == null) {
      if (SYMMORPHIC_SPACE_GROUP_STRUCTS[numDimensions][arithmeticClassNumber] == null) {
        return null;
      }
      StringReader reader = new StringReader(SYMMORPHIC_SPACE_GROUP_STRUCTS[numDimensions][arithmeticClassNumber]);
      PRIM prim = new PRIM(reader);
      Structure structure = new Structure(prim);
      SYMMORPHIC_SPACE_GROUPS[numDimensions][arithmeticClassNumber] = structure.getDefiningSpaceGroup();
    }
    
    return SYMMORPHIC_SPACE_GROUPS[numDimensions][arithmeticClassNumber];
  }
  
}
