/*
 * Created on Mar 8, 2005
 *
 */
package matsci.location.symmetry.operations;

import java.util.Arrays;

import org.ejml.alg.dense.decomposition.eig.WatchedDoubleStepQRDecomposition;
import org.ejml.data.Complex64F;
import org.ejml.data.DenseMatrix64F;

import matsci.location.Coordinates;
import matsci.location.Line;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.BravaisLattice;
import matsci.util.MSMath;
import matsci.util.arrays.*;

/**
 * This class represent a space group operation. Each operation has three key elements:
 * 		a basis (Lattice. e.g sym ops in Cubic lattice and Hexagonal lattice are deferent),
 * 		the rotational part (matrix form),
 * 		the translational part (as Cartesian vector).
 * @author Tim Mueller
 *
 */
public class SymmetryOperation {

  private static double EIGENVALUE_TOLERANCE = 1E-6;
  
  private PointOpAnalyzer m_PointOpAnalyzer;

  private final AbstractLinearBasis m_Basis;
  private final double[][] m_PointOperator;
  private double[] m_CartesianTranslator; // This is in cartesian coordinates

  public SymmetryOperation(double[][] pointOperator, double[] translator, AbstractLinearBasis basis) {
    m_Basis = basis;
    m_PointOperator = ArrayUtils.copyArray(pointOperator);
    m_CartesianTranslator = basis.transformToCartesianDirection(translator);
  }
  
  public SymmetryOperation(double[][] pointOperator, AbstractLinearBasis basis) {
    m_Basis = basis;
    m_PointOperator = ArrayUtils.copyArray(pointOperator);
    m_CartesianTranslator = new double[basis.numDimensions()];
  }
  
  public SymmetryOperation(double[][] pointOperator, Vector translator, AbstractLinearBasis basis) {
    this(pointOperator, translator.getDirectionArray(basis), basis);
  }
  
  public SymmetryOperation(double[][] pointOperator, AbstractLinearBasis basis, Coordinates fixedPoint) {
    m_Basis = basis;
    SymmetryOperation pointOp = new SymmetryOperation(pointOperator, basis);
    Coordinates oppedCoords = pointOp.operate(fixedPoint);
    m_PointOperator = ArrayUtils.copyArray(pointOperator);
    m_CartesianTranslator = new Vector(oppedCoords, fixedPoint).getCartesianDirection();
  }
  
  public Coordinates operate(Coordinates coords) {
    double[] cartOperatedArray = this.operateGetCartesian(coords.getArrayCopy(), coords.getBasis());
    return new Coordinates(cartOperatedArray, CartesianBasis.getInstance(), coords.getBasis());
  }
  
  public Coordinates[] operate(Coordinates[] coords) {
    Coordinates[] returnArray = new Coordinates[coords.length];
    for (int pointNum = 0; pointNum < coords.length; pointNum++) {
      returnArray[pointNum] = this.operate(coords[pointNum]);
    }
    return returnArray;
  }
  
  public Vector operate(Vector vector) {
    return new Vector(this.operate(vector.getTail()), this.operate(vector.getHead()));
  }
  
  public Vector[] operate(Vector[] vectors) {
    Vector[] returnArray = new Vector[vectors.length];
    for (int vecNum = 0; vecNum < vectors.length; vecNum++) {
      returnArray[vecNum] = this.operate(vectors[vecNum]);
    }
    return returnArray;
  }
  
  public boolean isEquivalentTo(SymmetryOperation operation) {
    
    int dimension = this.numDimensions();
    
    for (int dimNum = 0; dimNum < dimension; dimNum++) {
      double[] startCoordArray = new double[dimension];
      startCoordArray[dimNum] = 1;
      double[] thisTransformArray = this.operateGetCartesian(startCoordArray, CartesianBasis.getInstance());
      double[] operationTransformArray = operation.operateGetCartesian(startCoordArray, CartesianBasis.getInstance());
      double distSQ = MSMath.magnitude(MSMath.arrayDiff(thisTransformArray, operationTransformArray));
      if (distSQ > CartesianBasis.getPrecisionSquared()) {return false;}
    }
    
    return true;
  }
  
  public SymmetryOperation addTranslation(Vector translation) {
    double[] cartTranslation = translation.getCartesianDirection();
    cartTranslation = MSMath.arrayAdd(cartTranslation, m_CartesianTranslator);
    SymmetryOperation operation = new SymmetryOperation(m_PointOperator, m_Basis);
    operation.m_CartesianTranslator = cartTranslation;
    return operation;
  }
  
  /**
   * 
   * @param op2 This op gets applied first
   * @return
   */
  public SymmetryOperation getOperationProduct(SymmetryOperation op2) {
    
    AbstractLinearBasis basis = (m_Basis == op2.m_Basis) ? m_Basis : CartesianBasis.getInstance();
    
    double[][] pointOp1 = this.getOriginCenteredPointOperator(basis);
    double[][] pointOp2 = op2.getOriginCenteredPointOperator(basis);
    double[][] pointOp = MSMath.matrixMultiply(pointOp2, pointOp1);
    
    double[] translation = this.operateGetCartesian(op2.m_CartesianTranslator, CartesianBasis.getInstance());
    SymmetryOperation returnOp = new SymmetryOperation(pointOp, m_Basis);
    returnOp.m_CartesianTranslator = translation;
    return returnOp;
  }
  
  public AbstractLinearBasis getBasis() {
    return m_Basis;
  }
  
  public boolean isEquivalentTo(SymmetryOperation operation, BravaisLattice lattice) {
    
    if (lattice == null) {
      return this.isEquivalentTo(operation);
    }
    
    int dimension = this.numDimensions();
    
    for (int dimNum = 0; dimNum < dimension; dimNum++) {
      double[] startCoordArray = new double[dimension];
      startCoordArray[dimNum] = 1;
      double[] thisTransformArray = this.operateGetCartesian(startCoordArray, CartesianBasis.getInstance());
      double[] operationTransformArray = operation.operateGetCartesian(startCoordArray, CartesianBasis.getInstance());
      Coordinates thisCoords = new Coordinates(thisTransformArray, CartesianBasis.getInstance());
      Coordinates thatCoords = new Coordinates(operationTransformArray, CartesianBasis.getInstance());
      // Double the tolerance for a product of two operations.
      if (!lattice.areTranslationallyEquivalent(thisCoords, thatCoords, CartesianBasis.getPrecision() * 2)) {return false;}
    }
    
    return true;
  }
  
  public double[] operate(double[] coordArray, AbstractBasis basis) {
    double[] cartOperatedArray = this.operateGetCartesian(coordArray, basis);
    return basis.transformFromCartesian(cartOperatedArray);
  }

  public boolean isIdentity() {
    
    return this.getPointOpAnalyzer().isIdentity();
    
  }
  
  public Vector isPureTranslation() {
    SymmetryOperation testOp = new SymmetryOperation(m_PointOperator, m_Basis);
    if (testOp.isIdentity()) {
      return new Vector(m_CartesianTranslator, CartesianBasis.getInstance());
    }
    return null;
  }
  
  /**
   * The op is considered lattice preserving even if it shifts the lattice
   * 
   * @param lattice
   * @return
   */
  public int[][] isLatticePreserving(BravaisLattice lattice) {
    
    AbstractLinearBasis directBasis = lattice.getLatticeBasis();
    double[] testVector = new double[lattice.numTotalVectors()];
    double[][] pointOperator = this.getOriginCenteredPointOperator(directBasis);
    
    for (int dimNum = 0; dimNum < lattice.numTotalVectors(); dimNum++) {
      if (!lattice.isDimensionPeriodic(dimNum)) {continue;}
      Arrays.fill(testVector, 0);
      testVector[dimNum] = 1;
      double[] oppedVector = MSMath.vectorTimesMatrix(testVector, pointOperator);
      Coordinates oppedCoords = new Coordinates(oppedVector, directBasis);
      if (!lattice.isLatticePoint(oppedCoords)) {return null;}
    }
    
    int[][] intOperator = new int[pointOperator.length][pointOperator.length];
    for (int rowNum = 0; rowNum < pointOperator.length; rowNum++) {
      for (int colNum = 0; colNum < intOperator[rowNum].length; colNum++) {
        intOperator[rowNum][colNum] = (int) Math.round(pointOperator[rowNum][colNum]);
      }
    }
    return intOperator;
  }
  
  public Coordinates isInversion() {
    
    return this.getPointOpAnalyzer().getInversionCenter();
    
  }
  
  /**
   * TODO:   Glide, rotation, rotoInversion, and screw
   */
  public Vector isReflection() {
    
    PointOpAnalyzer pointOpAnalyzer = this.getPointOpAnalyzer();
    return pointOpAnalyzer.getReflectionVector();
  
  }
  
  private PointOpAnalyzer getPointOpAnalyzer() {
    
    if (m_PointOpAnalyzer == null) {
      m_PointOpAnalyzer = new PointOpAnalyzer();
    }
    
    return m_PointOpAnalyzer;
    
  }
  
  /**
   * Assumes this is a linear operation (like all operations). 
   * 
   * 
   * Length of returned axis is angle of rotation in radians
   * @return
   */
  public Vector isRotation() {
    
    return this.getPointOpAnalyzer().getRotationVector(1);

  }

  
  public Vector isRotationOrRotoInversion() {
    Vector rotation = this.isRotation();
    if (rotation != null) {
      return rotation;
    }
    return this.isRotoInversion();
  }
  
  /**
   * Assumes this is a linear operation (like all operations). 
   * 
   * Length of returned axis is angle of rotation in radians
   * @return
   */
  public Vector isRotoInversion() {    
    
    // A reflection is a 180 degree rotoinversion
    //if (this.getPointOpAnalyzer().getReflectionVector() != null) {return null;}
    return this.getPointOpAnalyzer().getRotationVector(-1);
    
  }
  
  public double[][] getOriginCenteredPointOperator(AbstractLinearBasis basis) {
    double[][] testPoints = this.getTestPoints();
    double[] transformedOrigin = this.operateGetCartesian(testPoints[0], basis);
    transformedOrigin = basis.transformFromCartesian(transformedOrigin);
    double[][] returnArray = new double[testPoints.length - 1][];
    for (int pointNum = 1; pointNum < testPoints.length; pointNum++) {
      double[] cartTransformedPoint = this.operateGetCartesian(testPoints[pointNum], basis);
      double[] transformedPoint = basis.transformFromCartesian(cartTransformedPoint);
      returnArray[pointNum -1] = MSMath.arrayDiff(transformedPoint, transformedOrigin);
    }
    return returnArray;
  }
  
  /**
   * Simply removes glide and screw translations
   * @return
   */
  public SymmetryOperation getPointOperation() {
    
    if (this.isPointOperation()) {
      return this;
    }
    
    return  this.addTranslation(this.getTranslationVector().reverse());
    /*if (newOp.getPointOpAnalyzer().getFixedPoint() == null) {
      System.currentTimeMillis();
    }
    return newOp;*/

  }
  
  public Vector getTranslationVector() {
    
    if (this.getPointOpAnalyzer().getFixedPoint() != null) {
      return Vector.getZero3DVector();
    }
    
    Vector translation = this.isPureTranslation();
    if (translation != null) {
      return translation;
    }
    
    SymmetryOperation originCenteredOperation = this.getOriginCenteredPointOperation();
    Coordinates origin = CartesianBasis.getInstance().getOrigin();
    Coordinates oppedOrigin = this.operate(origin);
    Vector oppedVector = new Vector(origin, oppedOrigin);
    
    Vector rotationAxis = originCenteredOperation.isRotation();
    if (rotationAxis != null) {
      rotationAxis = rotationAxis.unitVectorCartesian();
      double dotProduct = oppedVector.innerProductCartesian(rotationAxis);
      return rotationAxis.resize(dotProduct);
    } 
    
    Vector reflectionDirection = originCenteredOperation.isReflection();
    if (reflectionDirection == null) {return null;} // Not sure what this is.
    reflectionDirection = reflectionDirection.unitVectorCartesian();
    double dotProduct = reflectionDirection.innerProductCartesian(oppedVector);
    return oppedVector.subtract(reflectionDirection.resize(dotProduct));
    
  }
  
  public SymmetryOperation getOriginCenteredPointOperation() {
    return new SymmetryOperation(this.getOriginCenteredPointOperator(CartesianBasis.getInstance()), CartesianBasis.getInstance());
  }
  
  public double[] getOriginCenteredCartesianTranslator() {
    return ArrayUtils.copyArray(m_CartesianTranslator);
  }
  
  public double[] getOriginCenteredTranslation(AbstractLinearBasis basis) {
    return basis.transformFromCartesianDirection(m_CartesianTranslator);
    /*double[] origin = new double[basis.numDimensions()];
    return this.operate(origin, basis);*/
  }
  
  public boolean isPointOperation() {
    return (this.getPointOpAnalyzer().getFixedPoint() != null);
   // return (this.getOriginCenteredPointOperation().isEquivalentTo(this));
  }
  
  protected double[][] getTestPoints() {
    
    int numDimensions = this.numDimensions();
    double[][] returnArray = new double[numDimensions + 1][numDimensions];
    for (int pointNum = 1; pointNum < returnArray.length; pointNum++) {
      returnArray[pointNum][pointNum - 1] = 1;
    }
    return returnArray;
  }
  
  public int numDimensions() {
    return m_PointOperator.length;
  }
  
  public double[] operateGetCartesian(double[] coordArray, AbstractBasis basis) {
    double[] thisCoordArray = m_Basis.getArrayFrom(coordArray, basis);
    double[] pointOperatedArray = MSMath.vectorTimesMatrix(thisCoordArray, m_PointOperator);
    double[] cartPointOperatedArray = m_Basis.transformToCartesian(pointOperatedArray);
    double[] cartTranslatedArray = MSMath.arrayAdd(cartPointOperatedArray, m_CartesianTranslator);
    return cartTranslatedArray;
  }
  
  private class PointOpAnalyzer {
        
    private Coordinates m_FixedPoint;
    private double[][] m_TransformationMatrix;
    private double[][] m_Eigenvectors;
    private double[] m_RealEigenvalues;
    private double[] m_ImaginaryEigenvalues;
    
    public PointOpAnalyzer() {
      
      m_FixedPoint = this.findFixedPoint();
      m_TransformationMatrix = this.findCartesianTransfomationMatrix();
      this.getEigenValueDecomposition();
      
    }
    
    public Coordinates getInversionCenter() {
      
      if (m_FixedPoint == null) {return null;}
      
      // First figure out if it's a reflection matrix
      for (int eigNum = 0; eigNum < m_Eigenvectors.length; eigNum++) {
        double realValue = m_RealEigenvalues[eigNum];
        double imaginaryValue = m_ImaginaryEigenvalues[eigNum];
        if (Math.abs(imaginaryValue) > EIGENVALUE_TOLERANCE) {return null;}
        if (Math.abs(realValue + 1) > EIGENVALUE_TOLERANCE) {return null;}
      }
      return m_FixedPoint;
      
    }
    
    public boolean isIdentity() {
      
      if (m_FixedPoint == null) {return false;}
      
      // First figure out if it's a reflection matrix
      for (int eigNum = 0; eigNum < m_Eigenvectors.length; eigNum++) {
        double realValue = m_RealEigenvalues[eigNum];
        double imaginaryValue = m_ImaginaryEigenvalues[eigNum];
        if (Math.abs(imaginaryValue) > EIGENVALUE_TOLERANCE) {return false;}
        if (Math.abs(realValue - 1) > EIGENVALUE_TOLERANCE) {return false;}
      }
      return true;
      
    }
    
    public Coordinates getFixedPoint() {
      return m_FixedPoint;
    }
    
    public Vector getReflectionVector() {
      
      if (m_FixedPoint == null) {return null;}
            
      // First figure out if it's a reflection matrix
      int numPositive = 0;
      int numNegative = 0;
      for (int eigNum = 0; eigNum < m_Eigenvectors.length; eigNum++) {
        double realValue = m_RealEigenvalues[eigNum];
        double imaginaryValue = m_ImaginaryEigenvalues[eigNum];
        if (Math.abs(imaginaryValue) > EIGENVALUE_TOLERANCE) {return null;}
        if (Math.abs(realValue - 1) < EIGENVALUE_TOLERANCE) {
          numPositive++;
        } else if (Math.abs(realValue + 1) < EIGENVALUE_TOLERANCE) {
          numNegative++;
        } else {
          return null;
        }
      }
      if (!((numPositive == 2) && (numNegative == 1))) {return null;}
      
      // Now find the plane of reflection
      double[] normalArray = new double[m_TransformationMatrix.length];
      for (int dimNum = 0; dimNum < normalArray.length; dimNum++) {
        double diagValue = m_TransformationMatrix[dimNum][dimNum];
        diagValue = Math.min(diagValue, 1); // To deal with potential numeric problems
        normalArray[dimNum] = Math.sqrt((1 - diagValue) / 2);
      }
      
      // By default let the first dimension be positive
      if (m_TransformationMatrix[0][1] > EIGENVALUE_TOLERANCE) {normalArray[1] *= -1;}
      if (m_TransformationMatrix[0][2] > EIGENVALUE_TOLERANCE) {normalArray[2] *= -1;}
      if ((Math.abs(normalArray[0]) < EIGENVALUE_TOLERANCE) && (m_TransformationMatrix[1][2] > 0)) {normalArray[1] *= -1;}
      
      return new Vector(normalArray, CartesianBasis.getInstance()).translateTo(m_FixedPoint);
      
    }
    
    /**
     * 
     * @param direction +1 for rotations, -1 for rotoinversions
     * @return
     */
    public Vector getRotationVector(int direction) {
      
      if (m_FixedPoint == null) {return null;}
      
      // Don't return a zero angle
      // Assumes three dimensions
      double sumPlusConstant = -direction;
      for (int dimNum = 0; dimNum < m_TransformationMatrix.length; dimNum++) {
        sumPlusConstant += m_TransformationMatrix[dimNum][dimNum];
      }
      sumPlusConstant = Math.max(sumPlusConstant, -2); // For potential numerical problems
      sumPlusConstant = Math.min(sumPlusConstant, 2);
      // Multiplying by the direction makes this roto-inversion instead of roto-reflection.  
      // This is important to make a three-fold improper rotation have a 120 degree rotation.
      double angle = Math.acos(direction * sumPlusConstant / 2); // The direction makes this roto-inversion instead of roto-reflection.  This is important to make a three-fold improper rotation have a 120 degree rotation.
      if (angle < CartesianBasis.getAngleToleranceRadians()) {return null;}
      if ((direction < 0) && Math.abs(angle - Math.PI) < CartesianBasis.getAngleToleranceRadians()) {return null;} // It's a reflection
      
      // First figure out if it's a rotation matrix
      double determinant = MSMath.determinant(m_TransformationMatrix);
      if (Math.abs(determinant - direction) > EIGENVALUE_TOLERANCE) {return null;}
      
      for (int eigNum = 0; eigNum < m_Eigenvectors.length; eigNum++) {
        double realValue = m_RealEigenvalues[eigNum];
        double imaginaryValue = m_ImaginaryEigenvalues[eigNum];
        double product = realValue * realValue + imaginaryValue * imaginaryValue;
        double magnitude = Math.sqrt(product);
        if (Math.abs(magnitude - 1) > EIGENVALUE_TOLERANCE) {return null;}
      }
      
      // Next find the axis
      int axisNum = -1;
      for (int eigNum = 0; eigNum < m_Eigenvectors.length; eigNum++) {
        double realValue = m_RealEigenvalues[eigNum];
        double imaginaryValue = m_ImaginaryEigenvalues[eigNum];
        if (Math.abs(realValue - direction) > EIGENVALUE_TOLERANCE) {continue;}
        if (Math.abs(imaginaryValue) > EIGENVALUE_TOLERANCE) {continue;}
        if (axisNum >= 0) {return null;} // can't have two axes
        axisNum = eigNum;
      }
      if (axisNum == -1) {return null;}
      
      Vector axis = new Vector(m_Eigenvectors[axisNum], CartesianBasis.getInstance());
      axis = axis.translateTo(m_FixedPoint);
      return axis.resize(angle);
      
    }
    
    private void getEigenValueDecomposition() {
      
      if (m_TransformationMatrix == null) {return;}
      
      if (m_PointOperator.length == 3) {
        Matrix3D matrix = new Matrix3D(m_TransformationMatrix);
        m_Eigenvectors = matrix.getRealEigenVectors();
        m_RealEigenvalues = matrix.getEigenValuesRealPart();
        m_ImaginaryEigenvalues = matrix.getEigenValuesImaginaryPart();
      } else {
        DenseMatrix64F ejmlMatrix = new DenseMatrix64F(m_TransformationMatrix);
        WatchedDoubleStepQRDecomposition decomposition = new WatchedDoubleStepQRDecomposition(true);
        if (!decomposition.decompose(ejmlMatrix)) {
          //throw new RuntimeException("Eigvenvalue decomposition failed for matrix: \n" + ArrayUtils.toString(m_TransformationMatrix));
          //System.out.println("Eigvenvalue decomposition failed for matrix: \n" + ArrayUtils.toString(m_TransformationMatrix));
        }
        
        m_Eigenvectors = new double[m_TransformationMatrix.length][];
        m_RealEigenvalues = new double[m_TransformationMatrix.length];
        m_ImaginaryEigenvalues = new double[m_TransformationMatrix.length];
        for (int vecNum = 0; vecNum < m_Eigenvectors.length; vecNum++) {
          DenseMatrix64F eigenvector = decomposition.getEigenVector(vecNum);
          m_Eigenvectors[vecNum] = (eigenvector == null) ? new double[m_TransformationMatrix.length] : eigenvector.getData();
          Complex64F eigenvalue = decomposition.getEigenvalue(vecNum);
          m_RealEigenvalues[vecNum] = eigenvalue.getReal();
          m_ImaginaryEigenvalues[vecNum] = eigenvalue.getImaginary();
        }
      }
/*      } else {
      
      // The colt library sometimes hangs.  Ugh.
        DoubleMatrix2D coltMatrix = DoubleFactory2D.dense.make(m_TransformationMatrix);
        EigenvalueDecomposition eig = new EigenvalueDecomposition(coltMatrix);
        m_RealEigenvalues =  eig.getRealEigenvalues().toArray();
        m_ImaginaryEigenvalues = eig.getImagEigenvalues().toArray();
        m_Eigenvectors = MSMath.transpose(eig.getV().toArray());
      
      }*/
    }

    private double[][] findCartesianTransfomationMatrix() {
      
      Coordinates symCenter = m_FixedPoint;
      if (symCenter == null) {return null;}
      
      double[] centerArray = symCenter.getCartesianArray();
      int numDimensions = numDimensions();
      double[][] returnArray = new double[numDimensions][];
      for (int dimNum = 0; dimNum < returnArray.length; dimNum++) {
        
        double[] cartArray = ArrayUtils.copyArray(centerArray);
        cartArray[dimNum] += 1;
        double[] oppedArray = operateGetCartesian(cartArray, CartesianBasis.getInstance());
        returnArray[dimNum] = MSMath.arraySubtract(oppedArray, centerArray);
        
      }
      
      // This should clean up some numerical problems.
      double tolerance = 1E-15;
      for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
        for (int colNum = 0; colNum < returnArray[rowNum].length; colNum++) {
          double val = returnArray[rowNum][colNum];
          if (Math.abs(val) < tolerance) {
            returnArray[rowNum][colNum] = 0;
          } else if (Math.abs(val - 1) < tolerance) {
            returnArray[rowNum][colNum] = 1;
          } else if (Math.abs(val + 1) < tolerance) {
            returnArray[rowNum][colNum] = -1;
          }
          
          // This might be faster, but it would also be more disruptive for non-integer entries (e.g. rotations);
          //returnArray[rowNum][colNum] = MSMath.roundWithPrecision(returnArray[rowNum][colNum], 2E-15);
        }
      }
      
      return returnArray;
      
    }
    
   private Coordinates findFixedPoint() {
      
      // We do this by trying to find a point that is equidistant from all operation points.
      // Origin is the only point that is left invariant under point operators, hence we test origin.
      Coordinates origin = CartesianBasis.getInstance().getOrigin();
      
      Vector vec1 = new Vector(origin, operate(origin));
      if (vec1.length() < CartesianBasis.getPrecision()) {
        return vec1.getMidPoint();
      }
      
      Vector vec2 = new Vector(origin, operate(vec1.getHead()));
      if (vec2.length() < CartesianBasis.getPrecision()) {
        return vec1.getMidPoint();
      }
      
      Line equidistantLine = this.getEquidistantPoints(origin, vec1.getHead(), vec2.getHead());
      if (equidistantLine == null) {
        return null;
      }
      
      Vector vec3 = new Vector(origin, operate(vec2.getHead()));
      if (vec3.length() < CartesianBasis.getPrecision()) {
        double[] array1 = origin.getCartesianArray();
        double[] array2 = vec1.getHead().getCartesianArray();
        double[] array3 = vec2.getHead().getCartesianArray();
        double[] returnArray = new double[array1.length];
        for (int dimNum = 0; dimNum < array1.length; dimNum++) {
          returnArray[dimNum] = (array1[dimNum] + array2[dimNum] + array3[dimNum]) / 3;
        }
        return new Coordinates(returnArray, CartesianBasis.getInstance());
      }
      
      Line equidistantLine2 = this.getEquidistantPoints(vec1.getHead(), vec2.getHead(), vec3.getHead());
      if (equidistantLine2 == null) {
        return null;
      }
      
      // Test to see if the points used to generate the lines are the same.
      // If so, then that's the equidistant point.
      if (equidistantLine2.getDefiningVector().getTail().isCloseEnoughTo(equidistantLine.getDefiningVector().getTail())) {
        return equidistantLine.getDefiningVector().getTail();
      }
      
      Coordinates returnCoords =  equidistantLine.getIntersection(equidistantLine2);
      if (returnCoords == null) {return null;}
      if (!operate(returnCoords).isCloseEnoughTo(returnCoords)) {
        return null;
      } // A sanity check.  For some reason this seems to happen with three-fold screw axes
      
      return returnCoords;
      
    }
    
    private Line getEquidistantPoints(Coordinates coords1, Coordinates coords2, Coordinates coords3) {
      
      Vector vec1 = new Vector(coords1, coords2);
      Vector vec2 = new Vector(coords1, coords3);
      
      Vector crossProduct = vec1.crossProductCartesian(vec2);
      if (crossProduct.length() < CartesianBasis.getPrecision()) {
        return null; // The vectors are parallel or antiparallel
      }
      
      /**
       * This next part finds the axis equidistant from the three points
       */
      LinearBasis testBasis = new LinearBasis(LinearBasis.fill3DBasis(new Vector[] {crossProduct}));
      
      double[] vec1Array = vec1.getDirectionArray(testBasis);
      double[] vec2Array = vec2.getDirectionArray(testBasis);
      
      double[][] testArray = new double[][] {
          {vec1Array[1], vec1Array[2]},
          {vec2Array[1], vec2Array[2]},
      };
      
      double[] coord1Array = coords1.getCoordArray(testBasis);
      double[] coord2Array = coords2.getCoordArray(testBasis);
      double[] coord3Array = coords3.getCoordArray(testBasis);
      
      double[] magnitudes = new double[] {
          MSMath.dotProduct(coord2Array, coord2Array) - MSMath.dotProduct(coord1Array, coord1Array),
          MSMath.dotProduct(coord3Array, coord3Array) - MSMath.dotProduct(coord1Array, coord1Array), 
      };
      
      double[] centerArray = MSMath.matrixTimesVector(MSMath.simpleInverse(testArray), magnitudes);
      
      Coordinates centerCoords = new Coordinates(new double[] {0, centerArray[0] / 2, centerArray[1] / 2}, testBasis);
      Coordinates newHead = centerCoords.translateBy(crossProduct);
      
      return new Line(centerCoords, newHead);
      //return new Line(newHead, centerCoords); // Ensures that the equidistant points will be closer to the center coords
      
    }
    

    /*private Coordinates findFixedPoint() {
      
      double[][] identityMatrix = ArrayUtils.diagonalMatrix(m_PointOperator.length, 1.0);
      double[][] matrixDifference = MSMath.arraySubtract(m_PointOperator, identityMatrix);
     
      double determinant = MSMath.determinant(matrixDifference);
      if (Math.abs(determinant) < 1E-7) {
        return null;
      }
      
      double[][] inverse = MSMath.simpleInverse(matrixDifference);
      double[] thisTranslation = m_Basis.transformFromCartesianDirection(m_CartesianTranslator);
      double[] fixedArray = MSMath.vectorTimesMatrix(thisTranslation, inverse);
      fixedArray = MSMath.arrayMultiply(fixedArray, -1);
      Coordinates returnCoords = new Coordinates(fixedArray, m_Basis);
      
      if (!operate(returnCoords).isCloseEnoughTo(returnCoords)) {
        System.currentTimeMillis();
      }
      
      return returnCoords;
    }*/
    
    
  }
  
  

}
