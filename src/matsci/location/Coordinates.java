package matsci.location;

import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.util.arrays.ArrayUtils;
/**
 * <p>Title: </p>
 * <p>Description: An immutable object that contains a numerical array and a basis that together
 * identify a point in space</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class Coordinates implements Comparable {

  private final double[] m_CoordArray;
  private final double[] m_CartesianCoordArray; // Redundant but useful
  private final AbstractBasis m_Basis;

  /**
   *
   * @param coordArray An array of coordinates in a given basis
   * @param basis The basis corresponding to the given array
   */
  public Coordinates(double[] coordArray, AbstractBasis basis) {
    m_Basis = basis;
    m_CoordArray = ArrayUtils.copyArray(coordArray);
    m_CartesianCoordArray = (basis == CartesianBasis.getInstance()) ? m_CoordArray : basis.transformToCartesian(m_CoordArray);
  }
  
  public Coordinates(int[] coordArray, AbstractBasis basis) {
    m_Basis = basis;
    m_CoordArray = new double[coordArray.length];
    for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
      m_CoordArray[dimNum] = coordArray[dimNum];
    }
    m_CartesianCoordArray = (basis == CartesianBasis.getInstance()) ? m_CoordArray : basis.transformToCartesian(m_CoordArray);
  }
  
  /**
   * This constructor is used if the basis that the coordinates are currently in is not the basis they should be stored in
   * 
   * @param coordArray An array of coordinates in a given basis
   * @param basis The basis corresponding to the given array
   * @param storageBasis The basis which should be used to store the coordinates.
   */
  public Coordinates(double[] coordArray, AbstractBasis basis, AbstractBasis storageBasis) {
    m_CartesianCoordArray = basis.transformToCartesian(coordArray);
    m_CoordArray = storageBasis.transformFromCartesian(m_CartesianCoordArray);
    m_Basis = storageBasis;
  }
  
  /**
   * This constructor is used internally to rapidly generate another set of coordinates represening
   * the same point as a known set of coordinates
   * 
   * @param coordArray An array of coordinates in a given basis
   * @param basis The basis corresponding to the given array
   * @param cartesianArray Cartesian coordinates representing the same point
   */
  private Coordinates(double[] coordArray, AbstractBasis basis, double[] cartesianArray) {
    m_CoordArray = ArrayUtils.copyArray(coordArray);
    m_Basis = basis;
    m_CartesianCoordArray = cartesianArray;
  }

  /**
   * This method, combined with coord(i), is useful for iterating through the coordinate array
   *
   * @return The number of elements in the stored coordinate array
   */
  public int numCoords() {
    return m_CoordArray.length;
  }
  
  public int numCartesianCoords() {
    return m_CartesianCoordArray.length;
  }
  
  public double cartesianCoord(int cartCoordNum) {
    return m_CartesianCoordArray[cartCoordNum];
  }

  /**
   *
   * @return The basis that combined with the stored double values allows
   * you to identify a point in space.
   */
  public AbstractBasis getBasis() {
    return m_Basis;
  }

  /**
   *
   * @param i The index of the element of the coordinate array you would like returned
   * @return The element of the coordinate array corresponding to the given index
   */
  public double coord(int i) {
    return m_CoordArray[i];
  }
  
  public double cartCoord(int i) {
    return m_CartesianCoordArray[i];
  }

  /**
   *
   * @param basis A basis in which you would like to represent the point in space represented
   * by this instance.
   * @return A new coordinates object representing the same point in space as this coordinates
   * object but using the given basis.
   */
  public Coordinates getCoordinates(AbstractBasis basis) {
    if (basis == m_Basis) {return this;}
    double[] returnArray = basis.transformFromCartesian(m_CartesianCoordArray);
    return new Coordinates(returnArray, basis, m_CartesianCoordArray);
  }

  /**
   *
   * @param basis A basis in which you would like to represent the point in space represented
   * by this instance
   * @return An array that when combined with the given basis gives the same point in space
   * as this instance.
   */
  public double[] getCoordArray(AbstractBasis basis) {
    if (basis == m_Basis) {return this.getArrayCopy();}
    return basis.transformFromCartesian(m_CartesianCoordArray);
  }

  /**
   *
   * @return An coordinates object that uses the Cartesian basis to represent the same
   * point in space that this object does.
   */
  public Coordinates getCartesianCoords() {
    //return new Coordinates(this.getCartesianArray(), CartesianBasis.getInstance());
    return new Coordinates(m_CartesianCoordArray, CartesianBasis.getInstance(), m_CartesianCoordArray);
  }

  /**
   *
   * @return An array of values that represents the same point in space as represented by this object
   * but in the Cartesian basis.
   */
  public double[] getCartesianArray() {
    return ArrayUtils.copyArray(m_CartesianCoordArray);
  }

  /**
   *
   * @return A copy of the internal coordinates array used by this object.
   * A copy is returned to prevent accidentaly modifications of the internal coordinates array.
   */
  public double[] getArrayCopy() {
    return ArrayUtils.copyArray(m_CoordArray);
  }

  /**
   *
   * @param coords A set of coordinates to compare this one to
   * @return True if the distance between the two points represented by the two sets of coordinates
   * is less than the precision defined on the CartesianBasis.  False if otherwise.
   */
  public boolean isCloseEnoughTo(Coordinates coords) {
    double distance = this.distanceFrom(coords);
    return (distance > CartesianBasis.getPrecision() ? false : true);
  }

  /**
   *
   * @param coords A set of coordinates to comparet this one to
   * @return The distance between the two sets of coordinates
   */
  public double distanceFrom(Coordinates coords) {

    double[] thisCCoordArray = m_CartesianCoordArray;
    double[] newCCoordArray = coords.m_CartesianCoordArray;

    double magsq = 0;
    for (int i = 0; i < thisCCoordArray.length; i++) {
      double difference = newCCoordArray[i] - thisCCoordArray[i];
      magsq += difference * difference;
    }

    return Math.sqrt(magsq);
  }
  
  public double distanceFrom(int[] coordArray, AbstractBasis basis) {
    
    double[] thisCCoordArray = m_CartesianCoordArray;
    double[] newCCoordArray = basis.transformToCartesian(coordArray);

    double magsq = 0;
    for (int i = 0; i < thisCCoordArray.length; i++) {
      double difference = newCCoordArray[i] - thisCCoordArray[i];
      magsq += difference * difference;
    }

    return Math.sqrt(magsq);
    
  }
  
  public double distanceFrom(double[] coordArray, AbstractBasis basis) {
    
    double[] thisCCoordArray = m_CartesianCoordArray;
    double[] newCCoordArray = basis.transformToCartesian(coordArray);

    double magsq = 0;
    for (int i = 0; i < thisCCoordArray.length; i++) {
      double difference = newCCoordArray[i] - thisCCoordArray[i];
      magsq += difference * difference;
    }

    return Math.sqrt(magsq);
    
  }

  /**
   *
   * @param vector A vector indicating the offset of a new point in space relative to the point
   * in space represented by this object.
   * @return A new set of coordinates represening the point in space shifted by the given vector.
   * The basis of the new set of coordinates is the same as the basis of this object.
   */
  public Coordinates translateBy(Vector vector) {
    return this.translateBy(vector.getCartesianDirection(), CartesianBasis.getInstance());
  }
  
  public Coordinates translateBy(double[] translationArray, AbstractLinearBasis basis) {
    double[] cartTranslation = basis.transformToCartesianDirection(translationArray);
    for (int dimNum = 0; dimNum < cartTranslation.length; dimNum++) {
      cartTranslation[dimNum] += m_CartesianCoordArray[dimNum];
    }
    return new Coordinates(cartTranslation, CartesianBasis.getInstance(), cartTranslation);
  }

  /**
   *
   * @param direction A set of coordinates indicating the offset of a new point in space relative to the point
   * in space represented by this object.  The offset is given by the direction between the given set of
   * coordinates and the origin.
   * @return A new set of coordinates representing the point in space shifted by the given offset.
   * The basis of the new set of coordinates is the same as the basis of this object.
   */
  public Coordinates translateBy(Coordinates direction) {
    double[] newCoordArray = direction.getCartesianArray();
    for (int coordNum = 0; coordNum < newCoordArray.length; coordNum++) {
      newCoordArray[coordNum] = newCoordArray[coordNum] + m_CartesianCoordArray[coordNum];
    }
    
    return new Coordinates(newCoordArray, CartesianBasis.getInstance(), newCoordArray);
    //double[] returnArray = m_Basis.transformFromCartesian(newCoordArray);
    //return new Coordinates(returnArray, m_Basis);
  }
  
  /**
   * Return the value of an angle with these coords as the vertex
   * @param coords1
   * @param coords2
   * @return
   */
  public double angle(Coordinates coords1, Coordinates coords2) {
    Vector vector1 = new Vector(this, coords1);
    Vector vector2 = new Vector(this, coords2);
    return vector1.angle(vector2);
  }

  /**
   * This is inconsistent with equals.  The comparison is performed on the internal arrays of
   * coordinates.  The basis is not considered at all.  It is possible for two different objects
   * to return 0 for this operation but false for Equals();
   * 
   * @param Obj Another Coordinates object to compare with this one
   * @return 0 is returned if the arrays are the same length and every element is identical.  
   * If the number of coordinates are the same for both objects, each individual element
   * is compared, starting at index 0.  As soon as this object has a smaller value, -1 is returned.  
   * As sooon as it has a larger value, 1 is returned.  
   * 
   * If this object has fewer coordinates, -1 is returned.  If it has more, 1 is returned.  
   */
  public final int compareTo(Object Obj) {
    Coordinates coords = (Coordinates) Obj;
    int numCoords = coords.numCoords();

    // Deal with the most likely cases first
    if (numCoords == m_CoordArray.length) {
      for (int coordNum = 0; coordNum < numCoords; coordNum++) {
        if (m_CoordArray[coordNum] < coords.m_CoordArray[coordNum]) {
          return -1;
        }
        if (m_CoordArray[coordNum] > coords.m_CoordArray[coordNum]) {
          return 1;
        }
      }
      return 0;
    }
    return (m_CoordArray.length < numCoords ? -1 : 1);
  }
  
  public String toString() {
    String returnString = "Coordinate basis is " + m_Basis + ": {";
    for (int coordNum = 0; coordNum < m_CoordArray.length - 1; coordNum++) {
      returnString += m_CoordArray[coordNum] + ", ";
    }
    returnString += m_CoordArray[m_CoordArray.length - 1] + "}";
    return returnString;
  }
  
  public String toCartesianString() {
    String returnString = "{";
    for (int coordNum = 0; coordNum < m_CartesianCoordArray.length - 1; coordNum++) {
      returnString += m_CartesianCoordArray[coordNum] + ", ";
    }
    returnString += m_CartesianCoordArray[m_CartesianCoordArray.length - 1] + "}";
    return returnString;
  }
}
