package matsci.location;

import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class Vector {

  private static final Vector ZERO_VECTOR_3D = new Vector(new double[3], CartesianBasis.getInstance());
  
  private final Coordinates m_Head;
  private final Coordinates m_Tail;
  private final double[] m_CartesianDirection; // Redundant information, but useful
  
  public Vector(double[] cartArray) {
    m_Tail = CartesianBasis.getInstance().getOrigin();
    m_Head = new Coordinates(cartArray, CartesianBasis.getInstance());
    m_CartesianDirection = ArrayUtils.copyArray(cartArray);
  }
  
  public Vector(Coordinates head) {
    m_Tail = CartesianBasis.getInstance().getOrigin();
    m_Head = head;
    m_CartesianDirection = head.getCartesianArray();
  }

  public Vector(Coordinates tail, Coordinates head) {
    m_Tail = tail;
    m_Head = head;
    double[] cartesianHead = head.getCartesianArray();
    double[] cartesianTail = tail.getCartesianArray();
    m_CartesianDirection = MSMath.arrayDiff(cartesianHead, cartesianTail);
  }

  public Vector(Coordinates tail, double[] direction, AbstractLinearBasis directionBasis) {
    m_Tail = tail;
    m_CartesianDirection = directionBasis.transformToCartesianDirection(direction);
    double[] cartesianTail = tail.getCartesianArray();
    double[] cartesianHead = MSMath.fastArrayAdd(cartesianTail, m_CartesianDirection);
    double[] transformedHead = directionBasis.transformFromCartesian(cartesianHead);
    m_Head = new Coordinates(transformedHead, directionBasis);
  }

  public Vector(double[] direction, AbstractLinearBasis directionBasis) {
    this(directionBasis.getOrigin(), direction, directionBasis);
  }
  
  private Vector(Coordinates tail, double[] cartesianDirection, Coordinates head) {
    m_Tail = tail;
    m_Head = head;
    m_CartesianDirection = ArrayUtils.copyArray(cartesianDirection);
  }
  
  public static Vector getZero3DVector() {
    return ZERO_VECTOR_3D;
  }

  public Coordinates getHead() {
    return m_Head;
  }

  public Coordinates getTail() {
    return m_Tail;
  }
  
  public Coordinates getMidPoint() {
    double[] midPointArray = MSMath.arrayAverage(m_Head.getCartesianArray(), m_Tail.getCartesianArray());
    return new Coordinates(midPointArray, CartesianBasis.getInstance());
  }

  /*
  public Coordinates getDirection() {
    return m_Direction;
  }*/
  
  public double[] getCartesianDirection() {
    return ArrayUtils.copyArray(m_CartesianDirection);
  }
  
  public double[] getDirectionArray(AbstractLinearBasis basis) {
    
    return basis.transformFromCartesianDirection(m_CartesianDirection);
    /*double[] headArray = m_Head.getCoordArray(basis);
    double[] tailArray = m_Tail.getCoordArray(basis);
    return MSMath.arrayDiff(headArray, tailArray);*/
    
  }

  public double length() {
    return m_Tail.distanceFrom(m_Head);
  }

  public Vector translateTo(Coordinates newTail) {
    if (newTail == m_Tail) {return this;}
    return new Vector(newTail, m_CartesianDirection, CartesianBasis.getInstance());
  }

  public double innerProduct(Vector vector, AbstractLinearBasis basis) {
    double[] thisCoordArray = this.getDirectionArray(basis);
    double[] otherCoordArray = vector.getDirectionArray(basis);
    //double[] thisCoordArray = basis.transformFromCartesian(this.getCartesianDirection());
    //double[] otherCoordArray = basis.transformFromCartesian(vector.getCartesianDirection());
    return MSMath.dotProduct(thisCoordArray, otherCoordArray);
  }

  public double innerProductCartesian(Vector vector) {
    double[] thisCoordArray = m_CartesianDirection;
    double[] otherCoordArray = vector.m_CartesianDirection;
    return MSMath.dotProduct(thisCoordArray, otherCoordArray);
  }
  
  public double angle(Vector vector) {
    
    double dotProduct = this.innerProductCartesian(vector);
    double cosTheta = dotProduct / (this.length() * vector.length());
    // Deal with possible numerical problems
    cosTheta = Math.max(cosTheta, -1);
    cosTheta = Math.min(cosTheta, 1);
    return Math.acos(cosTheta);
    
  }
  
  public double angleDegrees(Vector vector) {
    
    return Math.toDegrees(angle(vector));
    
  }

  public Vector crossProduct(Vector vector, AbstractLinearBasis basis) {
    double[] thisCoordArray = this.getDirectionArray(basis);
    double[] otherCoordArray = vector.getDirectionArray(basis);
    //double[] thisCoordArray = basis.transformFromCartesian(this.getCartesianDirection());
    //double[] otherCoordArray = basis.transformFromCartesian(vector.getCartesianDirection());
    return new Vector(this.getTail(), MSMath.crossProduct(thisCoordArray, otherCoordArray), basis);
  }

  public Vector crossProductCartesian(Vector vector) {
    return this.crossProduct(vector, CartesianBasis.getInstance());
  }
  
  public double tripleProduct(Vector vector1, Vector vector2, AbstractLinearBasis basis) {
    return this.crossProduct(vector1, basis).innerProduct(vector2, basis);
  }
  
  public double tripleProductCartesian(Vector vector1, Vector vector2) {
    return this.crossProductCartesian(vector1).innerProductCartesian(vector2);
  }

  public Vector unitVector(AbstractLinearBasis basis) {
    double[] thisCoordArray = this.getDirectionArray(basis);
    //double[] thisCoordArray = basis.transformFromCartesian(this.getCartesianDirection());
    double[] normalizedArray = MSMath.normalize(thisCoordArray);
    return new Vector(this.getHead(), normalizedArray, basis);
  }

  public Vector unitVectorCartesian() {
    return this.unitVector(CartesianBasis.getInstance());
  }
  
  public Vector resize(double newLength) {
    return this.multiply(newLength / this.length());
  }
  
  public Vector add(Vector vector2) {
    //if (vector2 == null) {return this;}
    double[] cartDirection = MSMath.arrayAdd(m_CartesianDirection, vector2.m_CartesianDirection);
    /*double[] cartDirection = this.getCartesianDirection();
    double[] cartDirection2 = vector2.getCartesianDirection();
    if (cartDirection.length != cartDirection2.length) {
      throw new RuntimeException("Can't add vectors of different dimensions");
    }
    for (int coordNum = 0; coordNum < cartDirection2.length; coordNum++) {
      cartDirection[coordNum] += cartDirection2[coordNum];
    }*/
    //double[] thisCoordArray = m_Tail.getBasis().transformFromCartesian(cartDirection);
    return new Vector(m_Tail, cartDirection, CartesianBasis.getInstance());
  }

  public Vector subtract(Vector vector2) {
    //if (vector2 == null) {return this;}
    double[] cartDirection = MSMath.arrayDiff(m_CartesianDirection, vector2.m_CartesianDirection);
    
    /*double[] cartDirection = this.getCartesianDirection();
    double[] cartDirection2 = vector2.getCartesianDirection();
    if (cartDirection.length != cartDirection2.length) {
      throw new RuntimeException("Can't add vectors of different dimensions");
    }
    for (int coordNum = 0; coordNum < cartDirection.length; coordNum++) {
      cartDirection[coordNum] -= cartDirection2[coordNum];
    }*/
    //double[] thisCoordArray = m_Tail.getBasis().transformFromCartesian(cartDirection);
    //return new Vector(m_Tail, thisCoordArray, m_Tail.getBasis());
    return new Vector(m_Tail, cartDirection, CartesianBasis.getInstance());
  }
  
  public Vector multiply(double factor) {
    double[] cartDirection = this.getCartesianDirection();
    for (int coordNum = 0; coordNum < cartDirection.length; coordNum++) {
      cartDirection[coordNum] *= factor;
    }
    //double[] thisCoordArray = m_Tail.getBasis().transformFromCartesian(cartDirection);
    //return new Vector(m_Tail, thisCoordArray, m_Tail.getBasis());
    return new Vector(m_Tail, cartDirection, CartesianBasis.getInstance());
  }
  
  public Vector divide(double factor) {
    return this.multiply(1.0/factor);
  }
  
  public Vector reverse() {
    double[] cartDirection = new double[m_CartesianDirection.length];
    for (int dimNum = 0; dimNum < cartDirection.length; dimNum++) {
      cartDirection[dimNum] = m_CartesianDirection[dimNum] * -1;
    }
    return new Vector(m_Head, cartDirection, m_Tail);
  }
  
  public Vector projectToDirection(Vector vector) {
    
    double length = this.innerProductCartesian(vector) / vector.length();
    return vector.resize(length);
    
  }
  
  public Vector removeDirection(Vector vector) {
    
    double length = this.innerProductCartesian(vector) / vector.length();
    return this.subtract(vector.resize(length));
    
  }
  
  
  public boolean isParallelTo(Vector vector) {
    
    double angle = this.angle(vector);
    if (Math.abs(angle) < CartesianBasis.getAngleToleranceRadians()) {return true;}
    return (Math.abs(Math.PI - angle) < CartesianBasis.getAngleToleranceRadians());
    /*double delta = Math.abs(this.unitVectorCartesian().innerProductCartesian(vector.unitVectorCartesian())) - 1;
    return (Math.abs(delta) < CartesianBasis.getPrecision());*/
  }
  
  public boolean isPerpendicularTo(Vector vector) {
    
    double angle = this.angle(vector);
    double angleDegrees = this.angleDegrees(vector);
    return (Math.abs(Math.PI / 2 - this.angle(vector)) < CartesianBasis.getAngleToleranceRadians());
    
    /*double dotProduct = Math.abs(this.unitVectorCartesian().innerProductCartesian(vector.unitVectorCartesian()));
    return dotProduct < CartesianBasis.getPrecision();*/
  }
  
}