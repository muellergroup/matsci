/*
 * Created on Jul 8, 2010
 *
 */
package matsci.electronicstructure;

public interface IDiscreteBandStructure {

  public abstract int numKPoints();
  public abstract int numBands();
  public double getWeight(int kpointNum);
  public abstract double[] getKPointCoords(int kpointNum);
  public double getBandEnergy(int spinNum, int kpointNum, int bandNum);
  public double getBandOccupancy(int spinNum, int kpointNum, int bandNum);

}
