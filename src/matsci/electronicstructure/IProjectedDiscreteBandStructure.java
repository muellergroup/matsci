/*
 * Created on Jul 8, 2010
 *
 */
package matsci.electronicstructure;

public interface IProjectedDiscreteBandStructure extends IDiscreteBandStructure {
  
  public abstract int numIons();
  public abstract int numOrbitals();
  public abstract String getOrbitalLabel(int orbitalNum);
  public abstract int getOrbitalLabelNum(String orbitalLabel);
  public abstract double getProjection(int spinNum, int kPointNum, int bandNum);
  public abstract double getProjection(int spinNum, int kPointNum, int bandNum, String orbitalLabel);
  public abstract double getProjection(int spinNum, int kPointNum, int bandNum, int ionNum);
  public abstract double getProjection(int spinNum, int kPointNum, int bandNum, int ionNum, String orbitalLabel);

}
