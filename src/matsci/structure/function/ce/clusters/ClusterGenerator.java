/*
 * Created on Feb 24, 2005
 *
 */
package matsci.structure.function.ce.clusters;

import java.util.Arrays;

import matsci.location.basis.*;
import matsci.location.Coordinates;
import matsci.location.symmetry.CoordSetMapper;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.Structure;
import matsci.structure.decorate.DecorationTemplate;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 * TODO Clean this whole class up 
 *
 */
public class ClusterGenerator {
  
  private DecorationTemplate m_ClusterExpansion;
  private boolean[] m_AllowedSites;
  
  public ClusterGenerator(DecorationTemplate clusterExpansion) {
    m_ClusterExpansion = clusterExpansion;
    m_AllowedSites = new boolean[clusterExpansion.getBaseStructure().numDefiningSites()];
    Arrays.fill(m_AllowedSites, true);
  }
  
  public ClusterGenerator(DecorationTemplate clusterExpansion, boolean[] allowedSites) {
    m_ClusterExpansion = clusterExpansion;
    m_AllowedSites = ArrayUtils.copyArray(allowedSites);
  }
  
  public Structure getBaseStructure() {
    return m_ClusterExpansion.getBaseStructure();
  }
  
  public boolean isSigmaSite(int primIndex) {
    return m_ClusterExpansion.isSigmaSite(primIndex);
  }
  
  public SpaceGroup getSpaceGroup() {
    return m_ClusterExpansion.getDefiningSpaceGroup();
  }
  
  public Coordinates[][][] generateClusterCoords(int clusterSize, double distance) {
    
    if (clusterSize == 0) {
      return new Coordinates[1][1][0];
    }
    
    Coordinates[][][] returnArray = new Coordinates[0][][];
    Structure primStructure = this.getBaseStructure();
    AbstractBasis directBasis = primStructure.getDefiningLattice().getLatticeBasis();
    
    for (int siteNum = 0; siteNum < primStructure.numDefiningSites(); siteNum++) {
      if (!this.isSigmaSite(siteNum)) {continue;}
      if (!m_AllowedSites[siteNum]) {continue;}
      Structure.Site definingSite = primStructure.getDefiningSite(siteNum);
      //Coordinates[] nearbyCoords = primStructure.getNearbySiteCoords(definingSite.getCoords(), distance, false);
      Structure.Site[] nearbySites = primStructure.getNearbySites(definingSite.getCoords(), distance, false);
      Coordinates[] keepers = new Coordinates[0];
      //for (int coordNum =0; coordNum < nearbyCoords.length; coordNum++) {
      for (int nearbySiteNum =0; nearbySiteNum < nearbySites.length; nearbySiteNum++) {
        //Coordinates neighbor = nearbyCoords[coordNum].getCoordinates(primStructure.getIntegerBasis());
        //Coordinates neighbor = nearbyCoords[coordNum].getCoordinates(directBasis);
        Coordinates neighbor = nearbySites[nearbySiteNum].getCoords().getCoordinates(directBasis);
        //int primSiteIndex = primStructure.getDefiningSite(neighbor).getIndex();
        /*Coordinates intCoords = nearbyCoords[coordNum].getCoordinates(primStructure.getIntegerBasis());
        int primSiteIndex = (int) intCoords.coord(intCoords.numCoords() -1);*/
        int primSiteIndex = nearbySites[nearbySiteNum].getIndex();
        if (primSiteIndex < siteNum) {continue;}
        if (!this.isSigmaSite(primSiteIndex)) {continue;}
        if (!m_AllowedSites[primSiteIndex]) {continue;}
        //if (!m_ClusterExpansion.isSigmaSite(intCoords)) {continue;}
        keepers = (Coordinates[]) ArrayUtils.appendElement(keepers, neighbor);
      }
      
      Arrays.sort(keepers); // TODO Is this necessary??
      //Coordinates definingSiteCoords = definingSite.getCoords().getCoordinates(primStructure.getIntegerBasis());
      Coordinates definingSiteCoords = definingSite.getCoords().getCoordinates(directBasis);
      Coordinates[] clusterStart = new Coordinates[] {definingSiteCoords};
      returnArray = this.getClusterCoords(returnArray, clusterStart, keepers, clusterSize, distance);
    }
    
    return returnArray;
    
  }
  
  protected Coordinates[][][] getClusterCoords(Coordinates[][][] foundClusters, Coordinates[] foundCoords, Coordinates[] selectionSet, int clusterSize, double distance) {
    
    // We have found a complete cluster
    if (foundCoords.length == clusterSize) {
      return addClusterGroup(foundClusters, foundCoords);
    }
    
    // There is no hope of building a big enough cluster with the remaining coordinates
    if (foundCoords.length + selectionSet.length < clusterSize) {return foundClusters;}
    
    // Find the coordinates from the selection set that are close to each candidate
    for (int candidateNum =0; candidateNum < selectionSet.length; candidateNum++) {
      Coordinates candidateCoords = selectionSet[candidateNum];
      
      // Reduce the selection set to ones that are close enough to the candidate coordinates
      // We also eliminate coordinates we should have already evaluated (ones before this in the selection set)
      Coordinates[] newSelectionSet = new Coordinates[0];
      for (int oldSetIndex = candidateNum + 1; oldSetIndex < selectionSet.length; oldSetIndex++) {
        Coordinates oldSetCoords = selectionSet[oldSetIndex];   
        if (oldSetCoords.distanceFrom(candidateCoords) < distance) {
          newSelectionSet = (Coordinates[]) ArrayUtils.appendElement(newSelectionSet, oldSetCoords);
        }
      }
      
      // We add the candidate coords to the set of found coordinates
      Coordinates[] newFoundCoords = (Coordinates[]) ArrayUtils.appendElement(foundCoords, candidateCoords);
      foundClusters = this.getClusterCoords(foundClusters, newFoundCoords, newSelectionSet, clusterSize, distance);
    }
    
    return foundClusters;
    
  }
  
  /**
   * This method checks to make sure symmetrically equivalent clusters don't already exist in knownClusterCoords
   * 
   * @param knownClusterCoords
   * @param newClusterCoords
   * @return
   */
  public Coordinates[][][] addClusterGroup(Coordinates[][][] knownClusterCoords, Coordinates[] newClusterCoords) {
    
    if (newClusterCoords.length == 0) {return knownClusterCoords;}
    CoordSetMapper setMapper = new CoordSetMapper(newClusterCoords);
    SpaceGroup spaceGroup = this.getSpaceGroup();
    
    // First check to see if we've already generated this cluster group
    for (int groupNum = 0; groupNum < knownClusterCoords.length; groupNum++) {
      Coordinates[][] groupCoords = knownClusterCoords[groupNum];
      if (groupCoords.length == 0) {continue;}
      Coordinates[] sampleCoords = groupCoords[0];
      if (!setMapper.areCongruent(sampleCoords)) {continue;}
      /*if (sampleCoords.length != newClusterCoords.length) {continue;}
      if (setMapper.mapCoordinates(sampleCoords, true).length == 0) {  // A congruency check -- I hope this speeds things up
        continue;
      }*/
      for (int clustNum = 0; clustNum < groupCoords.length; clustNum++) {
        Coordinates[] clustCoords = groupCoords[clustNum];
        if (spaceGroup.getLattice().areTranslationallyEquivalent(clustCoords, newClusterCoords)) {return knownClusterCoords;}
        //if (this.areClustersEquivalent(clustCoords, newClusterCoords)) {
        //  return knownClusterCoords;
        //}
      }
    }
    
    /*Coordinates[][] newGroup = new Coordinates[][] {newClusterCoords}; // This forces the identity operation to be given priority
    for (int opNum = 0; opNum < spaceGroup.numOperations(); opNum++) {
      SymmetryOperation operation = spaceGroup.getOperation(opNum);
      Coordinates[] oppedCoords = operation.operate(newClusterCoords);
      boolean newCluster = true;
      for (int prevClustNum = 0; prevClustNum < newGroup.length; prevClustNum++) {
        Coordinates[] prevClust = newGroup[prevClustNum];
        //if (this.areClustersEquivalent(oppedCoords, prevClust)) {newCluster = false;}
        if (spaceGroup.areTranslationallyEquivalent(oppedCoords, prevClust)) {newCluster = false;}
        if (!newCluster) {break;}
      }
      if (newCluster) {
        newGroup = (Coordinates[][]) ArrayUtils.appendElement(newGroup, oppedCoords);
      }
    }*/
    
    Coordinates[][] newGroup = this.generateClusterOrbit(newClusterCoords);
    Coordinates[][][] returnArray = (Coordinates[][][]) ArrayUtils.appendElement(knownClusterCoords, newGroup);
    return returnArray;
    
  }
  
  public Coordinates[][] generateClusterOrbit(Coordinates[] sampleCoords) {
    
    // Assumes that cluster expansion is primitive
    //SpaceGroup spaceGroup = m_ClusterExpansion.getSpaceGroup();
    SpaceGroup spaceGroup = this.getSpaceGroup();
    Coordinates[][] returnArray = new Coordinates[spaceGroup.numOperations()][];
    returnArray[0] = sampleCoords;
    //Coordinates[][] returnArray = new Coordinates[][] {sampleCoords}; // This forces the identity operation to be given priority
    int returnIndex = 1;
    for (int opNum = 0; opNum < spaceGroup.numOperations(); opNum++) {
      SymmetryOperation operation = spaceGroup.getOperation(opNum);
      Coordinates[] oppedCoords = operation.operate(sampleCoords);
      boolean newCluster = true;
      for (int prevClustNum = 0; prevClustNum < returnArray.length; prevClustNum++) {
        Coordinates[] prevClust = returnArray[prevClustNum];
        if (spaceGroup.getLattice().areTranslationallyEquivalent(oppedCoords, prevClust)) {newCluster = false;}
        if (!newCluster) {break;}
      }
      if (newCluster) {
        //returnArray = (Coordinates[][]) ArrayUtils.appendElement(newGroup, oppedCoords);
        // The below code was left in for a long time!  Removed January 10, 2018.  TKM
        /*if (oppedCoords.length == 2) {
          double dist1 = oppedCoords[0].distanceFrom(oppedCoords[1]);
          double dist2 = sampleCoords[0].distanceFrom(sampleCoords[1]);
          if (Math.abs(dist1 - dist2) > 1E-3) {
            System.currentTimeMillis();
          }
        }*/
        returnArray[returnIndex++] = oppedCoords;
      }
    }
    
    return (Coordinates[][]) ArrayUtils.truncateArray(returnArray, returnIndex);
  }
  
  //public boolean areClustersEquivalent(Coordinates[] oldClustCoords, Coordinates[] newClustCoords) {
  //  
  //  return m_Mapper.areTranslationallyEquivalent(oldClustCoords, newClustCoords);
    
    /*
    if (oldClustCoords.length != newClustCoords.length) {return false;}
    if (oldClustCoords.length == 0) {return true;}
    
    Structure primStructure = m_ClusterExpansion.getPrimStructure();
    
    Coordinates[] indOldCoords = new Coordinates[oldClustCoords.length];
    Coordinates[] intNewCoords = new Coordinates[newClustCoords.length];
    for (int coordNum = 0; coordNum < indOldCoords.length; coordNum++) {
      indOldCoords[coordNum] = oldClustCoords[coordNum].getCoordinates(primStructure.getIntegerBasis());
      intNewCoords[coordNum] = newClustCoords[coordNum].getCoordinates(primStructure.getIntegerBasis());
    }
    Arrays.sort(indOldCoords);
    Arrays.sort(intNewCoords);
    
    // Find the translation that relates the first coordinates in each cluster
    Coordinates sampleOldCoords = indOldCoords[0];
    Coordinates sampleNewCoords = intNewCoords[0];
    double[] translation = new double[sampleOldCoords.numCoords() - 1];    
    if (sampleOldCoords.coord(translation.length) != sampleNewCoords.coord(translation.length)) {
      return false;
    }
    for (int coord = 0; coord < translation.length; coord++) {
      translation[coord] = sampleOldCoords.coord(coord) - sampleNewCoords.coord(coord);
    }
    
    // Now compare the remaining coordinates in the cluster to make sure they're consistent.
    for (int coordNum = 1; coordNum < indOldCoords.length; coordNum++) {
      Coordinates oldCoords = indOldCoords[coordNum];
      Coordinates newCoords = intNewCoords[coordNum];
      if (newCoords.coord(translation.length) != oldCoords.coord(translation.length)) {return false;}
      for (int coord = 0; coord < translation.length; coord++) {
        if (oldCoords.coord(coord) - newCoords.coord(coord) != translation[coord]) {
          return false;
        }
      }
    }
    
    return true;
    */
  //}
  
  public Coordinates[][][] generateClusterCoords(int[] clusterSizeBySublattice, double distance) {
    
    int[] currClusterSizeBySublattice = new int[clusterSizeBySublattice.length];
    if (Arrays.equals(clusterSizeBySublattice, currClusterSizeBySublattice)) {
      return new Coordinates[1][1][0];
    }
    
    Coordinates[][][] returnArray = new Coordinates[0][][];
    Structure primStructure = this.getBaseStructure();
    AbstractBasis directBasis = primStructure.getDefiningLattice().getLatticeBasis();
    
    for (int siteNum = 0; siteNum < primStructure.numDefiningSites(); siteNum++) {
      if (!this.isSigmaSite(siteNum)) {continue;}
      if (!m_AllowedSites[siteNum]) {continue;}
      int subLatticeIndex = m_ClusterExpansion.getSublatticeForSite(siteNum);
      if (clusterSizeBySublattice[subLatticeIndex] == 0) {continue;}
      Structure.Site definingSite = primStructure.getDefiningSite(siteNum);
      //Coordinates[] nearbyCoords = primStructure.getNearbySiteCoords(definingSite.getCoords(), distance, false);
      Structure.Site[] nearbySites = primStructure.getNearbySites(definingSite.getCoords(), distance, false);
      Structure.Site[] keepers = new Structure.Site[0];
      //for (int coordNum =0; coordNum < nearbyCoords.length; coordNum++) {
      for (int nearbySiteNum =0; nearbySiteNum < nearbySites.length; nearbySiteNum++) {
        //Coordinates neighbor = nearbyCoords[coordNum].getCoordinates(primStructure.getIntegerBasis());
        //Coordinates neighbor = nearbyCoords[coordNum].getCoordinates(directBasis);
        Structure.Site neighbor = nearbySites[nearbySiteNum];//.getCoords().getCoordinates(directBasis);
        //int primSiteIndex = primStructure.getDefiningSite(neighbor).getIndex();
        /*Coordinates intCoords = nearbyCoords[coordNum].getCoordinates(primStructure.getIntegerBasis());
        int primSiteIndex = (int) intCoords.coord(intCoords.numCoords() -1);*/
        int primSiteIndex = neighbor.getIndex();
        if (primSiteIndex < siteNum) {continue;}
        if (!this.isSigmaSite(primSiteIndex)) {continue;}
        if (!m_AllowedSites[primSiteIndex]) {continue;}
        //if (!m_ClusterExpansion.isSigmaSite(intCoords)) {continue;}
        keepers = (Structure.Site[]) ArrayUtils.appendElement(keepers, neighbor);
      }
      
      //Arrays.sort(keepers); // TODO Is this necessary??  Let's try removing it in this method and see if it breaks anything.
      //Coordinates definingSiteCoords = definingSite.getCoords().getCoordinates(primStructure.getIntegerBasis());
      Coordinates definingSiteCoords = definingSite.getCoords().getCoordinates(directBasis);
      Coordinates[] clusterStart = new Coordinates[] {definingSiteCoords};
      currClusterSizeBySublattice[subLatticeIndex]++;
      returnArray = this.getClusterCoords(returnArray, clusterStart, keepers, clusterSizeBySublattice, currClusterSizeBySublattice, distance);
      currClusterSizeBySublattice[subLatticeIndex]--;
    }
    
    return returnArray;
    
  }
  
  protected Coordinates[][][] getClusterCoords(Coordinates[][][] foundClusters, Coordinates[] foundCoords, Structure.Site[] selectionSet, int[] clusterSizeBySublattice, int[] currClusterSizeBySublattice, double distance) {
    
    // We have found a complete cluster
    if (Arrays.equals(clusterSizeBySublattice, currClusterSizeBySublattice)) {
      return addClusterGroup(foundClusters, foundCoords);
    }
    
    // There is no hope of building a big enough cluster with the remaining coordinates
    if (foundCoords.length + selectionSet.length < MSMath.arraySum(clusterSizeBySublattice)) {return foundClusters;}
    
    // Find the coordinates from the selection set that are close to each candidate
    for (int candidateNum =0; candidateNum < selectionSet.length; candidateNum++) {
      Structure.Site candidate = selectionSet[candidateNum];
      int subLatticeIndex = m_ClusterExpansion.getSublatticeForSite(candidate.getIndex());
      if (currClusterSizeBySublattice[subLatticeIndex] == clusterSizeBySublattice[subLatticeIndex]) {continue;}
      
      // Reduce the selection set to ones that are close enough to the candidate coordinates
      // We also eliminate coordinates we should have already evaluated (ones before this in the selection set)
      //Structure.Site[] newSelectionSet = new Structure.Site[0];
      Structure.Site[] newSelectionSet = new Structure.Site[selectionSet.length - candidateNum];
      int numKeepers = 0;
      for (int oldSetIndex = candidateNum + 1; oldSetIndex < selectionSet.length; oldSetIndex++) {
        Structure.Site oldSetSite = selectionSet[oldSetIndex];   
        if (oldSetSite.distanceFrom(candidate) < distance) {
          newSelectionSet[numKeepers] = oldSetSite;
          numKeepers++;
          //newSelectionSet = (Structure.Site[]) ArrayUtils.appendElement(newSelectionSet, oldSetSite);
        }
      }
      newSelectionSet = (Structure.Site[]) ArrayUtils.truncateArray(newSelectionSet, numKeepers);
      
      // We add the candidate coords to the set of found coordinates
      currClusterSizeBySublattice[subLatticeIndex]++;
      Coordinates[] newFoundCoords = (Coordinates[]) ArrayUtils.appendElement(foundCoords, candidate.getCoords());
      foundClusters = this.getClusterCoords(foundClusters, newFoundCoords, newSelectionSet, clusterSizeBySublattice, currClusterSizeBySublattice, distance);
      currClusterSizeBySublattice[subLatticeIndex]--;
    }
    
    return foundClusters;
    
  }
  
}
