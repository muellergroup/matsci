/*
 * Created on Mar 17, 2005
 *
 */
package matsci.structure.function.ce.clusters;

import matsci.location.Coordinates;
import matsci.location.symmetry.*;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 * TODO merge this with CoordSetMapper
 */
public class ClusterGroupMapper {

  private SpaceGroup m_SpaceGroup;
  
  public ClusterGroupMapper(SpaceGroup spaceGroup) {
    //super(ce.getPrimStructure());
    m_SpaceGroup = spaceGroup;
  }

  /**
   * Returns true if the first given group is a supergroup of the second given group
   * 
   * @param superGroup The potential superGroup
   * @param subGroup The potenital subGroup
   * @param allowEquals True if "true" should be returned if the two groups are symmetrically equivalent
   * @return True if the potential superGroup is a supergroup of the potential subGroup
   */
  public boolean areSuperSubGroups(IClusterGroup superGroup, IClusterGroup subGroup, boolean allowEquals) {
    
    if (subGroup.numSitesPerCluster() > superGroup.numSitesPerCluster()) {return false;}
    if (!allowEquals && subGroup.numSitesPerCluster() == superGroup.numSitesPerCluster()) {return false;}
    if (subGroup.numSitesPerCluster() == 0) {return true;}
    
    int[][] maps = this.findSymmetryMaps(superGroup, subGroup, true);
    return (maps.length > 0);
    
    //This should be just a quick check for congruency, to rule out sets that can't possibly be symmetric
    /*int[][] congruentMaps = this.findCongruentMaps(superGroup.getSampleCoords(), subGroup.getSampleCoords(), true);
    if (congruentMaps.length == 0) {return false;}
    
    //AbstractBasis basis = m_ClusterExpansion.getPrimStructure().getIntegerBasis();
    AbstractBasis basis = this.getLattice().getLatticeBasis();
    
    Coordinates[] superCoords = superGroup.getSampleCoords();
    superCoords = basis.getCoordinatesFrom(superCoords);

    for (int clustNum = 0; clustNum < subGroup.numPrimClusters(); clustNum++) {
      Coordinates[] subCoords  = subGroup.getPrimClusterCoords(clustNum);
      int[][] maps = this.findTranslationMaps(subCoords, superCoords, true);
      if (maps.length > 0) {return true;}
    }
    return false;*/
  }
  
  public int[][] findSymmetryMaps(IClusterGroup group1, IClusterGroup group2, boolean returnOne) {
    
    int[][] returnArray = new int[0][];
    
    Coordinates[] sample1 = group1.getSampleCoords();
    Coordinates[] sample2 = group2.getSampleCoords();
    
    //This should be just a quick check for congruency, to rule out sets that can't possibly be symmetric
    if (sample1.length < sample2.length) {
      CoordSetMapper setMapper = new CoordSetMapper(sample1);
      if (setMapper.mapCoordinates(sample2, true).length == 0) {return returnArray;}
    } else {
      CoordSetMapper setMapper = new CoordSetMapper(sample2);
      if (setMapper.mapCoordinates(sample1, true).length == 0) {return returnArray;}
    }
    
    for (int coordSetNum = 0; coordSetNum < group2.numPrimClusters(); coordSetNum++) {
      Coordinates[] primCoords2 = group2.getPrimClusterCoords(coordSetNum);
      int[][] maps = m_SpaceGroup.getLattice().findTranslationMaps(sample1, primCoords2, returnOne);
      
      if ((maps.length > 0) && returnOne) {
        return maps;
      }
      
      // Make sure we haven't already seen this map
      for (int newMapNum = 0; newMapNum < maps.length; newMapNum++) {
        if (!ArrayUtils.arrayContains(returnArray, maps[newMapNum])) {
          returnArray = ArrayUtils.appendElement(returnArray, maps[newMapNum]);
        }
      }
    }
   
    return returnArray;
  }

  /**
   * Finds any way in which the space group of the cluster expansion can map the coords to each other
   * 
   * @param coordSet1
   * @param coordSet2
   * @param returnOne
   * @return
   */
  /*
  public int[][] findSymmetryMaps(Coordinates[] coordSet1, Coordinates[] coordSet2, boolean returnOne) {
    
    int[][] returnArray = new int[0][];
    if (coordSet1 == null || coordSet2 == null) {return returnArray;}
    if (coordSet1.length == 0 || coordSet2.length == 0) {return new int[1][0];} // The empty cluster maps to all clusters with no translation

    Coordinates[] bigSet = coordSet2;
    Coordinates[] smallSet = coordSet1;
    
    if (coordSet1.length > coordSet2.length) {
      bigSet = coordSet1;
      smallSet = coordSet2;
    }
    
    AbstractBasis intBasis = m_ClusterExpansion.getPrimStructure().getIntegerBasis();
    Coordinates[] intBigSet = intBasis.getCoordinatesFrom(bigSet);

    OperationGroup spaceGroup = m_ClusterExpansion.getSpaceGroup();
    for (int opNum = 0; opNum < spaceGroup.numOperations(); opNum++) {
      
      SymmetryOperation operation = spaceGroup.getOperation(opNum);
      Coordinates[] oppedSmallSet = operation.operate(smallSet);
      int[][] mapsForOperation = this.findTranslationMaps(oppedSmallSet, intBigSet, returnOne);
      
      if ((mapsForOperation.length > 0) && returnOne) {
        return mapsForOperation;
      }
      
      // Make sure we haven't already seen this map
      for (int newMapNum = 0; newMapNum < mapsForOperation.length; newMapNum++) {
        if (!this.containsMap(returnArray, mapsForOperation[newMapNum])) {
          returnArray = ArrayUtils.appendElement(returnArray, mapsForOperation[newMapNum]);
        }
      }
    }
    
    return returnArray;
  }
  */
/*
  public OperationGroup getFactorGroup() {
    return m_ClusterExpansion.getFactorGroup();
  }*/

  
}
