/*
 * Created on Aug 24, 2005
 *
 */
package matsci.structure.function.ce.clusters;

import matsci.location.Coordinates;
/**
 * @author Tim Mueller
 *
 */
public interface IClusterGroup {
  
  public int numSitesPerCluster();
  public int numPrimClusters();
  public Coordinates[] getSampleCoords();
  public Coordinates[] getPrimClusterCoords(int clustNum);
  public int getGroupNumber();
  public int numFunctionGroups();

}
