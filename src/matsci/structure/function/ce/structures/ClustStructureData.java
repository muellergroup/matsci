package matsci.structure.function.ce.structures;

import java.util.Arrays;

import matsci.structure.*;
import matsci.structure.function.ce.clusters.IClusterGroup;
import matsci.structure.superstructure.SuperData;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class ClustStructureData extends SuperData {

  private double m_Value = Double.NaN;
  private double[] m_Correlations;
  private double[][] m_Derivatives;  //  By base site, then descriptor siteCoord varNum
  private double[][][] m_DerivativeCorrelations; // By base site, then descriptor siteCoord varNum, then functionGroup
  
  private int[][] m_CorrelationIndices;
  private double[] m_Concentrations;
  
  public ClustStructureData(Structure primStructure, int size, int latticeNumber) {
    super(primStructure, size, latticeNumber);
  }

  public void setValue(double value) {
    m_Value = value;
  }

  public double getValue() {
    return m_Value;
  }

  public void setCorrelations(double[][] correlations) {
    //m_Correlations = ArrayUtils.copyArray(correlations);
    int numTotalCorrelations = 0;
    for (int groupNum = 0; groupNum < correlations.length; groupNum++) {
      if (correlations[groupNum] == null) {continue;}
      numTotalCorrelations += correlations[groupNum].length;
    }
    
    m_Correlations = new double[numTotalCorrelations];
    m_CorrelationIndices = new int[correlations.length][];
    
    int correlationIndex = 0;
    for (int clustGroupNum = 0; clustGroupNum < correlations.length; clustGroupNum++) {
      if (correlations[clustGroupNum] == null) {continue;}
      m_CorrelationIndices[clustGroupNum] = new int[correlations[clustGroupNum].length];
      Arrays.fill(m_CorrelationIndices[clustGroupNum], (short) -1);
      for (int functGroupNum = 0; functGroupNum < m_CorrelationIndices[clustGroupNum].length; functGroupNum++) {
        m_Correlations[correlationIndex] = correlations[clustGroupNum][functGroupNum];
        m_CorrelationIndices[clustGroupNum][functGroupNum] = correlationIndex;
        correlationIndex++;
      }
    }
  }
  
  public boolean containsCorrelations(double[][] correlations, double tolerance) {
    for (int groupNum = 0; groupNum < correlations.length; groupNum++) {
      double[] groupCorrelations = correlations[groupNum];
      if (groupCorrelations == null) {continue;}
      int[] correlationIndices = m_CorrelationIndices[groupNum];
      if (groupCorrelations.length > correlationIndices.length) {return false;}
      for (int functGroupNum = 0; functGroupNum < groupCorrelations.length; functGroupNum++) {
        int correlationIndex = correlationIndices[functGroupNum];
        if (correlationIndex < 0) {return false;}
        double delta = m_Correlations[correlationIndex] - groupCorrelations[functGroupNum];
        if (Math.abs(delta) > tolerance) {return false;}
      }
    }
    return true;
  }
  
  public double[][] getCorrelations() {
    double[][] returnArray = new double[m_CorrelationIndices.length][];
    for (int groupNum = 0; groupNum < returnArray.length; groupNum++) {
      int[] correlationIndices = m_CorrelationIndices[groupNum];
      if (correlationIndices == null) {
        continue;
      }
      returnArray[groupNum] = new double[correlationIndices.length];
      for (int functNum = 0; functNum < correlationIndices.length; functNum++) {
        returnArray[groupNum][functNum] = m_Correlations[correlationIndices[functNum]];
      }
    }
    return returnArray;
  }
  
  public void setConcentrations(double[] concentrations) {
    m_Concentrations = ArrayUtils.copyArray(concentrations);
  }
  
  public double[] getConcentrations() {
    if (m_Concentrations == null || m_Concentrations.length == 0) {
      return null;
    }
    return ArrayUtils.copyArray(m_Concentrations);
  }

  public void setCorrelations(double[] correlations, IClusterGroup[] activeGroups) {
    //m_Correlations = groupCorrelations(correlations, activeGroups, setEmptyGroup);
    int maxGroupNumber = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      maxGroupNumber = Math.max(maxGroupNumber, activeGroups[groupIndex].getGroupNumber());
    }
    m_Correlations = ArrayUtils.copyArray(correlations);
    m_CorrelationIndices = new int[maxGroupNumber + 1][];
    int correlationIndex = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      IClusterGroup clustGroup = activeGroups[groupIndex];
      int clustGroupNum = clustGroup.getGroupNumber();
      m_CorrelationIndices[clustGroupNum] = new int[clustGroup.numFunctionGroups()];
      Arrays.fill(m_CorrelationIndices[clustGroupNum], (short) -1);
      for (int functGroupNum = 0; functGroupNum < clustGroup.numFunctionGroups(); functGroupNum++) {
        m_CorrelationIndices[clustGroupNum][functGroupNum] = (short) correlationIndex++;
      }
    }
  }
  
  public double[] getDerivatives(boolean knownValues) {
    
    double[] returnArray = new double[this.numDerivativeCorrelations(knownValues)];
    if (m_DerivativeCorrelations == null) {return returnArray;}
    int returnIndex = 0;
    
    for (int baseSiteNum = 0; baseSiteNum < m_Derivatives.length; baseSiteNum++) {
      double[] derivativesForBase = m_Derivatives[baseSiteNum];
      for (int varNum = 0; varNum < derivativesForBase.length; varNum++) {
        if (knownValues && Double.isNaN(this.getDerivative(baseSiteNum, varNum))) {continue;}
        returnArray[returnIndex++] = this.getDerivative(baseSiteNum, varNum);
      }
    }

    return returnArray;
    
  }
  
  public int numDerivativeCorrelations(boolean knownValues) {
    
    if (m_DerivativeCorrelations == null) {return 0;}
    
    int returnValue = 0;
    for (int baseSiteNum = 0; baseSiteNum < m_Derivatives.length; baseSiteNum++) {
      double[] derivativesForBase = m_Derivatives[baseSiteNum];
      for (int varNum = 0; varNum < derivativesForBase.length; varNum++) {
        if (knownValues && Double.isNaN(derivativesForBase[varNum])) {continue;}
        returnValue++;
      }
    }
    
    return returnValue;
  }
  
  public void setDerivatives(double[][] derivatives) {
    m_Derivatives = ArrayUtils.copyArray(derivatives);
  }
  
  public double getDerivative(int baseSiteNum, int varNum) {
    return m_Derivatives == null ? Double.NaN : m_Derivatives[baseSiteNum][varNum];
  }
  
  /**
   * Given a 1-d array of correlations and a list of cluster groups the correlations correspond to, this
   * function returns the correlations grouped by cluster groups.
   *
   * @param correlations A 1-d array of correlations sorted first by the group order in activeGroups, then by the function number
   * @param activeGroups The groups the given correlations correspond to in the same order as the given correlations
   * @param setEmptyGroup True if you want group number zero to be given a correlation of "1".
   * @return An array of correlations where the first index is the group number and the second is the function number
   */
  /*public static double[][] groupCorrelations(double[] correlations, IClusterGroup[] activeGroups, boolean setEmptyGroup) {

    // Figure out how large the return array needs to be
    int maxGroupNumber = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      maxGroupNumber = Math.max(activeGroups[groupIndex].getGroupNumber(), maxGroupNumber);
    }
    double[][] returnArray = new double[maxGroupNumber + 1][0];

    int numECI = 0;
    int correlationNumber = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      IClusterGroup group = activeGroups[groupIndex];
      int numFunctionGroups = group.numFunctionGroups();
      numECI += numFunctionGroups;
      double[] groupArray = new double[numFunctionGroups];
      for (int functionGroup =0; functionGroup < numFunctionGroups; functionGroup++) {
        groupArray[functionGroup] = correlations[correlationNumber];
        correlationNumber++;
      }
      returnArray[group.getGroupNumber()] = groupArray;
    }

    if (numECI != correlations.length) {
      throw new RuntimeException("Given correlations do not correspond to given groups");
    }

    if (setEmptyGroup) {
      returnArray[0] = new double[] {1};
    }

    return returnArray;
  }*/

  /**
   * The opposite of groupCorrelations
   *
   * @param correlations A set of correlations for which the first index is the group number, the second the function number
   * @param activeGroups The groups for which we want to return the correlations
   * @return A 1-dimensional array of the requested correlations
   */
  /*public static double[] spreadCorrelations(double[][] correlations, IClusterGroup[] activeGroups) {
    int totalFunctionGroups = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      totalFunctionGroups += activeGroups[groupIndex].numFunctionGroups();
    }

    double[] returnArray = new double[totalFunctionGroups];

    int totalFunctNum = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      IClusterGroup group = activeGroups[groupIndex];
      double[] groupCorrelations = correlations[group.getGroupNumber()];
      if (groupCorrelations == null) {
        throw new RuntimeException("Correlations requested for cluster group for which correlations are not known.  Cluster group: " + group.getGroupNumber());
      }
      for (int functNum = 0; functNum < group.numFunctionGroups(); functNum++) {
        returnArray[totalFunctNum++] = groupCorrelations[functNum];
      }
    }
    return returnArray;
  }*/
  
  public double[] getCorrelations(IClusterGroup[] activeGroups) {

    //return spreadCorrelations(m_Correlations, activeGroups);
    int numCorrelationsToReturn = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      numCorrelationsToReturn += activeGroups[groupIndex].numFunctionGroups();
    }
    
    double[] returnArray = new double[numCorrelationsToReturn];
    int returnIndex = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      IClusterGroup activeGroup = activeGroups[groupIndex];
      int[] correlationIndices = m_CorrelationIndices[activeGroup.getGroupNumber()];
      if (correlationIndices == null) {
        throw new RuntimeException("Correlations for given cluster group are unknown");
      }
      for (int functGroupNum = 0; functGroupNum < correlationIndices.length; functGroupNum++) {
        if (correlationIndices[functGroupNum] < 0) {
          throw new RuntimeException("Correlations for given function group are unknown");
        }
        returnArray[returnIndex++] = m_Correlations[correlationIndices[functGroupNum]];
      }
    }
    
    return returnArray;
  }

  public double[] getMultCorrelations(IClusterGroup[] activeGroups) {

    //return spreadCorrelations(m_Correlations, activeGroups);
    int numCorrelationsToReturn = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      numCorrelationsToReturn += activeGroups[groupIndex].numFunctionGroups();
    }
    
    double[] returnArray = new double[numCorrelationsToReturn];
    int returnIndex = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      IClusterGroup activeGroup = activeGroups[groupIndex];
      int[] correlationIndices = m_CorrelationIndices[activeGroup.getGroupNumber()];
      if (correlationIndices == null) {
        throw new RuntimeException("Correlations for given cluster group are unknown");
      }
      for (int functGroupNum = 0; functGroupNum < correlationIndices.length; functGroupNum++) {
        if (correlationIndices[functGroupNum] < 0) {
          throw new RuntimeException("Correlations for given function group are unknown");
        }
        returnArray[returnIndex++] = m_Correlations[correlationIndices[functGroupNum]] * activeGroup.numPrimClusters();
      }
    }
    
    return returnArray;
  }
  
  public static double[] spreadCorrelations(double[][] correlations, IClusterGroup[] activeGroups) {
    int numCorrelationsToReturn = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      numCorrelationsToReturn += activeGroups[groupIndex].numFunctionGroups();
    }
    
    double[] returnArray = new double[numCorrelationsToReturn];
    int returnIndex = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      double[] groupCorrelations = correlations[activeGroups[groupIndex].getGroupNumber()];
      for (int functGroupNum = 0; functGroupNum < groupCorrelations.length; functGroupNum++) {
        returnArray[returnIndex++] = groupCorrelations[functGroupNum];
      }
    }
    
    return returnArray;
  }
  
}