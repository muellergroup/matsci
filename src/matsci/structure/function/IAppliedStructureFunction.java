package matsci.structure.function;

import matsci.structure.Structure;
/**
 * <p>Classes that implement this interface represent the application of a Hamiltonian to 
 * a particular structure. </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * @author Tim Mueller
 * @version 1.0
 */

public interface IAppliedStructureFunction {

  /**
   *
   * @return The structure that the Hamiltonian was applied to.
   */
  public Structure getStructure();

  /**
   * If Hamiltonian has been changed by direct action
   * on the Hamiltonian, this method tells the application of the Hamiltonian
   * to update itself.
   */
  public void refreshFromHamiltonian();

  /**
   * If the structure has been changed by direct action on the structure,
   * this method tells the application of the Hamiltonian to the structure to
   * update itself.
   */
  public void refreshFromStructure();

  /**
   *
   * @return The value of the Hamiltonian applied to the structure.  Note
   * that for efficiency reasons this value might be cached.  If either the
   * structure or Hamiltonian has been modified directly since the
   * value was last calculated, refreshFromHamiltonian() or
   * refreshFromStructure() should be called first.
   */
  public double getValue();

}