/*
 * Created on Mar 14, 2005
 *
 */
package matsci.structure.decorate.exceptions;

/**
 * @author Tim Mueller
 *
 */
public class IllegalOccupationException extends RuntimeException {

  /**
   * 
   */
  public IllegalOccupationException() {
    super();
  }

  /**
   * @param message
   */
  public IllegalOccupationException(String message) {
    super(message);
  }

  /**
   * @param message
   * @param cause
   */
  public IllegalOccupationException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * @param cause
   */
  public IllegalOccupationException(Throwable cause) {
    super(cause);
  }

}
