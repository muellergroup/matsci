package matsci.structure.decorate;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;

import matsci.Atom;
import matsci.Element;
import matsci.Species;
import matsci.location.Coordinates;
import matsci.structure.Structure;
import matsci.structure.decorate.exceptions.IllegalOccupationException;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.arrays.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class SiteDecorator {

  private final Atom[][] m_AllowedAtoms;
  private final Structure.Site[] m_Sites;
  //private final OldBigArrayIndexer m_ConfigIndexer;
  protected final BigArrayIndexer m_ConfigIndexer;
  private final static Random m_Random = new Random();
  private final Species[] m_AllSigmaSpecies;
  private final int[] m_NumSigmaSitesPerSpecies;

  public SiteDecorator(Species[][] allAllowedSpecies, Structure.Site[] sites) {

    m_AllowedAtoms = new Atom[allAllowedSpecies.length][];
    
    Species[] allSigmaSpecies =new Species[0];
    int[] numSitesPerSpecies = new int[0];
    
    int[] allowedSpeciesPerSite = new int[m_AllowedAtoms.length];
    for (int sigmaSiteNum = 0; sigmaSiteNum < m_AllowedAtoms.length; sigmaSiteNum++) {      
      Species[] allowedSpecies = allAllowedSpecies[sigmaSiteNum];
      m_AllowedAtoms[sigmaSiteNum] = new Atom[allowedSpecies.length];
      allowedSpeciesPerSite[sigmaSiteNum] = allowedSpecies.length;
      for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {
        Species species = allowedSpecies[specNum];
        m_AllowedAtoms[sigmaSiteNum][specNum] = new Atom(species);
        if (allowedSpecies.length < 2) {continue;}
        int specIndex = ArrayUtils.findIndex(allSigmaSpecies, species);
        if (specIndex < 0) {
          specIndex = allSigmaSpecies.length;
          allSigmaSpecies = (Species[]) ArrayUtils.appendElement(allSigmaSpecies, species);
          numSitesPerSpecies = ArrayUtils.appendElement(numSitesPerSpecies, 0);
        } 
        numSitesPerSpecies[specIndex]++;
      }
    }
    m_AllSigmaSpecies = allSigmaSpecies;
    m_NumSigmaSitesPerSpecies = numSitesPerSpecies;

    m_Sites = (Structure.Site[]) ArrayUtils.copyArray(sites);
    m_ConfigIndexer = new BigArrayIndexer(allowedSpeciesPerSite);
  }
  
  /*
  public SiteDecorator(Specie[][] allowedPrimSpecies, SuperStructure superStructure) {
    // Count the number of sigma sites and initialize the sigma index map
    int numSigmaSites = superStructure.numDefiningSites();

    // Create the decorator and populate the sigma index map
    int sigmaIndex = 0;
    m_HopSites = new Structure.Site[numSigmaSites];
    m_AllowedAtoms = new Atom[numSigmaSites][];
    int[] allowedSpeciesPerSite =new int[numSigmaSites];
    
    for (int siteNum = 0; siteNum < superStructure.numDefiningSites(); siteNum++) {
      SuperStructure.Site site = (SuperStructure.Site) superStructure.getDefiningSite(siteNum);
      Specie[] allowedSpecies = allowedPrimSpecies[site.getParentSite().getIndex()];
      if (allowedSpecies.length > 1) {
        m_HopSites[sigmaIndex] = site;
        allowedSpeciesPerSite[sigmaIndex] = allowedSpecies.length;
        m_AllowedAtoms[sigmaIndex] = new Atom[allowedSpecies.length];
        for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {
          m_AllowedAtoms[sigmaIndex][specNum] = new Atom(allowedSpecies[specNum]);
        }
        sigmaIndex++;
      }
    }
    
    m_ConfigIndexer = new BigArrayIndexer(allowedSpeciesPerSite);
  }*/
  
  // TODO move this to SuperStructureDecorator
  public SiteDecorator(DecorationTemplate template, SuperStructure structure) {
    
    if (structure.getParentStructure() != template.getBaseStructure()) {
      throw new RuntimeException("Function is not defined for the parent structure of the given superstructure");
    }
    
    // Count the number of sigma sites and initialize the sigma index map
    int numSigmaSites = template.numSigmaSites() * structure.numPrimCells();

    // Create the decorator and populate the sigma index map
    int sigmaIndex = 0;
    m_Sites = new Structure.Site[numSigmaSites];
    m_AllowedAtoms = new Atom[numSigmaSites][];
    
    int[] allowedSpeciesPerSite =new int[numSigmaSites];
    
    for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
      SuperStructure.Site site = (SuperStructure.Site) structure.getDefiningSite(siteNum);
      //Specie[] allowedSpecies = function.getAllowedSpecies(site.getCoords());
      Species[] allowedSpecies = template.getAllowedSpecies(site.getParentSite().getIndex());
      if (allowedSpecies.length > 1) {
        m_Sites[sigmaIndex] = site;
        allowedSpeciesPerSite[sigmaIndex] = allowedSpecies.length;
        m_AllowedAtoms[sigmaIndex] = new Atom[allowedSpecies.length];
        for (int specNum = 0; specNum < allowedSpecies.length; specNum++) {
          m_AllowedAtoms[sigmaIndex][specNum] = new Atom(allowedSpecies[specNum]);
        }
        sigmaIndex++;
      }
    }
    
    // TODO lazy load this
    m_ConfigIndexer = new BigArrayIndexer(allowedSpeciesPerSite);
    
    m_AllSigmaSpecies = template.getAllSigmaSpecies();
    m_NumSigmaSitesPerSpecies = new int[m_AllSigmaSpecies.length];
    for (int specNum= 0; specNum < m_AllSigmaSpecies.length; specNum++) {
      m_NumSigmaSitesPerSpecies[specNum] = structure.numPrimCells() * template.numSigmaSitesForSpecie(specNum);
    }
    
    //this.decorateFromStructure(structure); // Not sure why I commented this out.  
    // I assume it's because exceptions were being thrown.
    // This also has problems because decorateFromStructure calls methods that might be overridden
    // Ah, it's also because the sites are directly taken from the structure, so there is no need.
  }

  public int[] getSigmaStates(BigInteger decorationID) {

    if (m_ConfigIndexer.numAllowedStates().compareTo(decorationID) <= 0) {
      throw new RuntimeException("Given configuration index is larger than the maximum allowed index");
    }

    return m_ConfigIndexer.splitValue(decorationID);
  }
  
  public boolean incrementSigmaStates(int[] oldSigmaStates, ArrayIndexer.Filter[] filters) {
    return m_ConfigIndexer.increment(oldSigmaStates, filters);
  }

  public boolean incrementSigmaStates(int[] oldSigmaStates, ArrayIndexer.Filter[] filters, double timeOut, int numForcedFilters) {
    return m_ConfigIndexer.increment(oldSigmaStates, filters, timeOut, numForcedFilters);
  }

  public boolean incrementSigmaStates(int[] oldSigmaStates, ArrayIndexer.Filter[] filters, double timeOut) {
    return this.incrementSigmaStates(oldSigmaStates, filters, timeOut, filters.length);
  }

  public boolean incrementSigmaStates(int[] oldSigmaStates) {
    return m_ConfigIndexer.increment(oldSigmaStates);
  }
  
  public void setSigmaStates(BigInteger decorationID) {
    this.setSigmaStates(m_ConfigIndexer.splitValue(decorationID));
  }

  public void setSigmaStates(int[] siteStates) {
    if (siteStates.length != m_AllowedAtoms.length) {
      throw new RuntimeException(
      "This structure decorator does not know how to apply the given site states.  The number of sites don't match.");
    }

    for (int siteNum = 0; siteNum < siteStates.length; siteNum++) {
      this.setSigmaState(siteNum, siteStates[siteNum]);
    }
  }
  
  public void setSigmaStates(int[] sites, int[] states) {
    for (int siteNum = 0; siteNum < sites.length; siteNum++) {
      this.setSigmaState(sites[siteNum], states[siteNum]);
    }
  }
  
  public void setSigmaStates(int[][] sites, int[] states) {
    for (int stateNum = 0; stateNum < states.length; stateNum++) {
      int[] sitesForState = sites[stateNum];
      int state = states[stateNum];
      for (int siteNum = 0; siteNum < sitesForState.length; siteNum++) {
        this.setSigmaState(sitesForState[siteNum], state);
      }
    }
  }

  public int[] setInitialStates() {
    this.decorate(BigInteger.ZERO);
    return this.getSigmaStates(BigInteger.ZERO);
  }

  public int[] readSigmaStates(int[] template) throws IllegalOccupationException {
    
    if (template == null) {
      return this.readSigmaStates();
    }
    
    for (int sigmaIndex = 0; sigmaIndex < template.length; sigmaIndex++) {
      template[sigmaIndex] = this.readSigmaState(sigmaIndex);
    }
    return template;
  }
  
  public void decorateFromStructure(Structure sourceStructure) {
    this.setAllowedVacancies();
    for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
      Structure.Site site = m_Sites[sigmaIndex];
      Structure.Site sourceSite = sourceStructure.getSite(site.getCoords());
      Species sourceSpecies;
      if (sourceSite == null) {
        if (this.allowsSpecies(sigmaIndex, Species.getVacancy())) {
          sourceSpecies = Species.getVacancy();
        } else {
          throw new RuntimeException("Source structure does not contain the sites necessary to decorate this structure.");
        }
      } else {
        sourceSpecies = sourceSite.getSpecies();
      }
      int sigmaState = this.getStateForSpecies(sigmaIndex, sourceSpecies);
      this.setSigmaState(sigmaIndex, sigmaState);
    }
  }
  
  public void decorate(BigInteger decorationID) {
    this.setSigmaStates(this.getSigmaStates(decorationID));
  }

  public BigInteger readDecorationID() throws IllegalOccupationException {
    return this.getDecorationID(this.readSigmaStates());
  }
  
  public int countSpeciesOccurences(Species species) {
    int returnValue = 0;
    for (int sigmaIndex = 0; sigmaIndex < m_Sites.length; sigmaIndex++) {
      if (m_Sites[sigmaIndex].getAtom().getSpecies() == species) {returnValue++;}
    }
    return returnValue;
  }
  
  public int readSigmaState(int sigmaIndex) throws IllegalOccupationException {

      Species species = m_Sites[sigmaIndex].getSpecies();
      if (species == null) {species = Species.getVacancy();}
      Atom[] allowedAtoms = m_AllowedAtoms[sigmaIndex];
      for (int specNum = 0; specNum < allowedAtoms.length; specNum++) {
        if (allowedAtoms[specNum].getSpecies() == species) {
          return specNum;
        }
      }
      String allowedSpecies = "";
      for (int specNum = 0; specNum < allowedAtoms.length; specNum++) {
        allowedSpecies += allowedAtoms[specNum].getSpecies() + " ";
      }
      throw new IllegalOccupationException("Structure decorator does not allow species " + species + " at site with sigma index " + sigmaIndex + ". Only {" + allowedSpecies + "} are allowed.");
  }

  public int[] readSigmaStates() throws IllegalOccupationException {
    int[] returnArray = new int[m_Sites.length];
    for (int sigmaIndex = 0; sigmaIndex < returnArray.length; sigmaIndex++) {
      returnArray[sigmaIndex] = this.readSigmaState(sigmaIndex);
    }
    return returnArray;
  }
  
  public BigInteger getDecorationID(int[] sigmaStates) {
    return m_ConfigIndexer.joinValuesBounded(sigmaStates);
  }

  public BigInteger numAllowedDecorations() {
    return m_ConfigIndexer.numAllowedStates();
  }

  public int numSigmaSites() {
    return m_Sites.length;
  }
  
  public int getSigmaIndex(int siteIndex) {
    
    for (int siteNum = 0; siteNum < m_Sites.length; siteNum++) {
      if (m_Sites[siteNum].getIndex() == siteIndex) {
        return siteNum;
      }
    }
    return -1;
    
  }

  public Structure.Site getSigmaSite(int sigmaIndex) {
    return m_Sites[sigmaIndex];
  }
  
  public Species getAllowedSpecies(int sigmaIndex, int specIndex) {

    return m_AllowedAtoms[sigmaIndex][specIndex].getSpecies();
    
  }

  public Species[] getAllowedSpecies(int sigmaIndex) {

    Atom[] allowedAtoms = m_AllowedAtoms[sigmaIndex];
    Species[] returnArray = new Species[allowedAtoms.length];
    for (int atomNum = 0; atomNum < allowedAtoms.length; atomNum++) {
      returnArray[atomNum] = allowedAtoms[atomNum].getSpecies();
    }
    return returnArray;
  }

  public int numAllowedSpecies(int sigmaIndex) {
    return m_AllowedAtoms[sigmaIndex].length;
  }

  public int[] getRandomStates() {

    int[] returnArray = new int[m_Sites.length];

    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      Atom[] allowedAtoms = m_AllowedAtoms[siteNum];
      returnArray[siteNum] = m_Random.nextInt(allowedAtoms.length);
    }

    return returnArray;
  }
  
  /**
   * Caution: this method does not consider whether the sites already contain the given species
   * 
   * @param numSites
   * @param species
   */
  public void decorateRandomly(int numSites, Species species) {
    
    int[] oxygenIndices = this.getSigmaIndicesForSpecies(species);
    int[] sitesToDecorate = this.getRandomSites(numSites, oxygenIndices);
    for (int siteNum = 0; siteNum < sitesToDecorate.length; siteNum++) {
      int sigmaIndex = sitesToDecorate[siteNum];
      int siteState = this.getStateForSpecies(sigmaIndex, species);
      this.setSigmaState(sigmaIndex, siteState);
    }
  }
  
  public void decorateRandomly(int[] sigmaIndices, int numSites, Species species) {
    
    int[] sitesToDecorate = this.getRandomSites(numSites, sigmaIndices);
    for (int siteNum = 0; siteNum < sitesToDecorate.length; siteNum++) {
      int sigmaIndex = sitesToDecorate[siteNum];
      int siteState = this.getStateForSpecies(sigmaIndex, species);
      this.setSigmaState(sigmaIndex, siteState);
    }
    
  }

  public void decorateRandomly() {

    for (int siteNum = 0; siteNum < m_Sites.length; siteNum++) {
      Atom[] allowedAtoms = m_AllowedAtoms[siteNum];
      int siteState = m_Random.nextInt(allowedAtoms.length);
      this.setSigmaState(siteNum, siteState);
    }
  }

  public void decorateWithState(int siteState) {
    for (int siteNum = 0; siteNum < m_Sites.length; siteNum++) {
      this.setSigmaState(siteNum, siteState);
    }
  }
  
  public void decorateWithSpecies(Species species) {
    for (int siteNum = 0; siteNum < m_Sites.length; siteNum++) {
      int state = this.getStateForSpecies(siteNum, species);
      if (state < 0) {continue;}
      this.setSigmaState(siteNum, state);
    }
  }

  public void setSigmaState(int sigmaIndex, int siteState) {
    m_Sites[sigmaIndex].setAtom(m_AllowedAtoms[sigmaIndex][siteState]);
  }
  
  public void setSigmaState(int sigmaIndex, Species species) {
    int siteState = this.getStateForSpecies(sigmaIndex, species);
    if (siteState < 0) {return;}
    this.setSigmaState(sigmaIndex, siteState);
    //m_Sites[sigmaIndex].setAtom(m_AllowedAtoms[sigmaIndex][siteState]); // This bug fixed April 2 2015
  }
  
  public int[][] getRandomSites(int[] numSites) {
    
    int numTotalSites = 0;
    int remainingSites = m_Sites.length;
    int[][] returnArray= new int[numSites.length][0];
    for (int stateNum = 0; stateNum < numSites.length; stateNum++) {
      numTotalSites+= numSites[stateNum];
      returnArray[stateNum] = new int[numSites[stateNum]];
    }

    if (numTotalSites > remainingSites) {
      throw new RuntimeException("Asked to decorate more sites than site decorator knows about.");
    }
    
    boolean[] decoratedSites = new boolean[m_Sites.length];
    int currentSiteIndex = 0;
    for (int stateNum = 0; stateNum < numSites.length; stateNum++) {
      for (int siteNum = 0; siteNum < numSites[stateNum]; siteNum++) {
        while (decoratedSites[currentSiteIndex]) { // Moves to the next undecorated site
          currentSiteIndex = (currentSiteIndex + 1) % decoratedSites.length;
        }
        int increment = m_Random.nextInt(remainingSites);
        for (int numSitesPassed = 0; numSitesPassed < increment;) {
          currentSiteIndex = (currentSiteIndex + 1) % decoratedSites.length;
          if (!decoratedSites[currentSiteIndex]) {numSitesPassed++;}
        }
        decoratedSites[currentSiteIndex] = true;
        returnArray[stateNum][siteNum] = currentSiteIndex;
        remainingSites--;
      }
    }  
    
    return returnArray;
  }
  
  public int[] getSigmaIndicesForSpecies(Species species) {

    int[] returnArray = new int[0];
    for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
      if (this.allowsSpecies(sigmaIndex, species)) {
        returnArray = ArrayUtils.appendElement(returnArray, sigmaIndex);
      }
    }
    return returnArray;
  }
  
  public int[] getRandomSites(int numSites, int[] sigmaIndices) {
    
    int[] returnArray= new int[numSites];
    
    int remainingSites = sigmaIndices.length;
    boolean[] decoratedSites = new boolean[sigmaIndices.length];
    int currentSiteIndex = 0;
    for (int siteNum = 0; siteNum < numSites; siteNum++) {
      while (decoratedSites[currentSiteIndex]) { // Moves to the next undecorated site
        currentSiteIndex = (currentSiteIndex + 1) % decoratedSites.length;
      }
      int increment = m_Random.nextInt(remainingSites);
      for (int numSitesPassed = 0; numSitesPassed < increment;) {
        currentSiteIndex = (currentSiteIndex + 1) % decoratedSites.length;
        if (!decoratedSites[currentSiteIndex]) {numSitesPassed++;}
      }
      decoratedSites[currentSiteIndex] = true;
      returnArray[siteNum] = sigmaIndices[currentSiteIndex];
      remainingSites--;
    }
    
    return returnArray;
  }
  
  public void decorateRandomly(int[] siteStates, int[] numSites) {
    this.setSigmaStates(this.getRandomSites(numSites), siteStates);
  }
  
  public void setAllowedVacancies() {
    this.decorateWithSpecies(Species.getVacancy());
  }
  
  public int getStateForSpecies(int sigmaIndex, Species species) {
    Atom[] allowedAtoms = m_AllowedAtoms[sigmaIndex];
    for (int state = 0; state < allowedAtoms.length; state++) {
      if (allowedAtoms[state].getSpecies() == species) {return state;}
    }
    return -1;
  }
  
  public Species getSpecies(int sigmaIndex) {
    return this.getSigmaSite(sigmaIndex).getSpecies();
  }
  
  public Species getSpeciesForState(int sigmaIndex, int siteState) {
    return m_AllowedAtoms[sigmaIndex][siteState].getSpecies();
  }
  
  public boolean allowsSpecies(int sigmaIndex, Species species) {
    
    Atom[] allowedAtoms = m_AllowedAtoms[sigmaIndex];
    for (int specNum = 0; specNum < allowedAtoms.length; specNum++) {
      if (allowedAtoms[specNum].getSpecies() == species) {return true;}
    }
    return false;
  }
  
  public boolean allowsElement(int sigmaIndex, Element element) {
	    
	    Atom[] allowedAtoms = m_AllowedAtoms[sigmaIndex];
	    for (int specNum = 0; specNum < allowedAtoms.length; specNum++) {
	      if (allowedAtoms[specNum].getSpecies().getElement() == element) {return true;}
	    }
	    return false;
	  }
  
  public Structure.Site[] getPartialStructureSites(Species[] matchSpecies) {
    
    Structure.Site[] matchSites = new Structure.Site[0];
    for (int siteNum = 0; siteNum < m_AllowedAtoms.length; siteNum++) {
      if (m_AllowedAtoms[siteNum].length == matchSpecies.length) {
        int specNum = 0;
        for (specNum = 0; specNum < matchSpecies.length; specNum++) {
          if (m_AllowedAtoms[siteNum][specNum].getSpecies() != matchSpecies[specNum]) {break;}
        }
        if (specNum == matchSpecies.length) { // we found a match
          matchSites = (Structure.Site[]) ArrayUtils.appendElement(matchSites, m_Sites[siteNum]);
        }
      }
    }
    
    return matchSites;
  }
  
  public SiteDecorator getPartialStructureDecorator(Species[] matchSpecies) {
    
    Structure.Site[] matchSites = this.getPartialStructureSites(matchSpecies);
    Species[][] allowedSpecies = new Species[matchSites.length][];
    for (int siteNum = 0; siteNum < allowedSpecies.length; siteNum++) {
      allowedSpecies[siteNum] = matchSpecies;
    }
    return new SiteDecorator(allowedSpecies, matchSites);
  }
  
  public int getIndexForSpecies(Species species) {
    return ArrayUtils.findIndex(m_AllSigmaSpecies, species);
  }
  
  public int getNumSigmaSitesPerSpecies(Species species) {
    int specIndex = this.getIndexForSpecies(species);
    return m_NumSigmaSitesPerSpecies[specIndex];
  }
  
  public Species[] getAllSigmaSpecies() {
    return (Species[]) ArrayUtils.copyArray(m_AllSigmaSpecies);
  }
  
  public int numSigmaSpecies() {
    return m_AllSigmaSpecies.length;
  }
  
  public Species getSigmaSpecies(int sigmaSpecIndex) {
    return m_AllSigmaSpecies[sigmaSpecIndex];
  }
  
  public int getSigmaSpecIndex(Species sigmaSpecie) {
    return ArrayUtils.findIndex(m_AllSigmaSpecies, sigmaSpecie);
  }
  
  public double getSigmaConcentration(Species species) {
    int numSitesPerSpecies = this.getNumSigmaSitesPerSpecies(species);
    double speciesCount = this.countSpeciesOccurences(species);
    return speciesCount / numSitesPerSpecies;
  }
  
  public double[] getSigmaConcentations() {
    double[] returnArray = new double[m_AllSigmaSpecies.length];
    for (int specNum = 0; specNum < m_AllSigmaSpecies.length; specNum++) {
      returnArray[specNum] = this.getSigmaConcentration(m_AllSigmaSpecies[specNum]);
    }
    return returnArray;
  }
  
  public int[] getInitialSigmaStates() {
    return m_ConfigIndexer.getInitialState();
  }
  
  public int[] getInitialSigmaStates(ArrayIndexer.Filter[] filters) {
    return m_ConfigIndexer.getInitialState(filters);
  }
  
  public int[] getInitialSigmaStates(ArrayIndexer.Filter[] filters, double timeOut) {
    return m_ConfigIndexer.getInitialState(filters, timeOut);
  }
  
  public void branchEnd(int index, int[] sigmaStates) {
    m_ConfigIndexer.branchEnd(index, sigmaStates);
  }
  
  public ArrayIndexer.Filter getChargeBalanceFilter(double nonSigmaCharge, double tolerance) {
    
    return new ChargeBalanceFilter(nonSigmaCharge, tolerance);
    
  }
  
  public ArrayIndexer.Filter getMaxCompositionFilter(Species[] species, double[] maxCompositions) {
    
    return new MaxCompositionFilter(species, maxCompositions);
    
  }
  
  /**
   * 
   * @param siteSets A list of different sets of sites, where the sites in the set are indicated by their sigma index.  Any number of sites can be in each site set, and 
   * multiple site sets can contain the same site.  However the same site cannot appear more than once in a given set.
   * @param compositions The target compositions for each set of sites.  The compositions are given in an array that corresponds to the array of Species returned by
   * getAllSigmaSpecies().  The elements of the array are integers that indicate how many sites in the give site set should be of the corresponding species.  For example, if
   * getAllSigmaSpecies() returns {Species.lithium, Species.magnesium} then an array of {3, 1} would mean there should be exactly 3 lithium and 1 magnesium in the site set.
   * @return A filter that only allows for exact matches.
   */
  public ArrayIndexer.Filter getExactCompositionFilter(int[][] siteSets, int[][] compositions) {
    
    return new ExactCompositionFilter(siteSets, compositions);
    
  }
  
  protected class ExactCompositionFilter implements ArrayIndexer.Filter {
    
    private int[][] m_SiteSets;
    private int[][] m_TargetCompositions; // By site set then then by sigma species 
    
    public ExactCompositionFilter(int[][] siteSets, int[][] compositions) {
      
      if (siteSets.length != compositions.length) {
        throw new RuntimeException("The number of sets of sites must equal the number of sets of composition targets.");
      }
      
      m_SiteSets = ArrayUtils.copyArray(siteSets);
      m_TargetCompositions = ArrayUtils.copyArray(compositions);
      
      for (int setNum = 0; setNum < m_TargetCompositions.length; setNum++) {
        int totalComposition = 0;
        for (int specNum = 0; specNum < m_TargetCompositions[setNum].length; specNum++) {
          totalComposition += m_TargetCompositions[setNum][specNum];
        }
        if (totalComposition != m_SiteSets[setNum].length) {
          throw new RuntimeException("The total composition of a site set (" 
              + totalComposition + ") does not equal the number of sigma sites in the site set (" 
              + m_SiteSets[setNum].length + ").");
        }
      }
    }

    public int getBranchIndex(int[] currentState) {
      
      int[][] minPossibleCounts = new int[m_SiteSets.length][SiteDecorator.this.numSigmaSpecies()];
      int[][] maxPossibleCounts = new int[m_SiteSets.length][SiteDecorator.this.numSigmaSpecies()];
      for (int setNum = 0; setNum < maxPossibleCounts.length; setNum++) {
        Arrays.fill(maxPossibleCounts[setNum], m_SiteSets[setNum].length);
      }
      
      for (int sigmaIndex = currentState.length - 1; sigmaIndex >= 0; sigmaIndex--) {
        Species species = SiteDecorator.this.getSpeciesForState(sigmaIndex, currentState[sigmaIndex]);
        int specIndex = SiteDecorator.this.getSigmaSpecIndex(species);
        for (int setNum = 0; setNum < m_SiteSets.length; setNum++) {
          if (!ArrayUtils.arrayContains(m_SiteSets[setNum], sigmaIndex)) {continue;}
          minPossibleCounts[setNum][specIndex]++;
          if (minPossibleCounts[setNum][specIndex] > m_TargetCompositions[setNum][specIndex]) {
            return sigmaIndex;
          }
          for (int sigmaSpecIndex = 0; sigmaSpecIndex < maxPossibleCounts[setNum].length; sigmaSpecIndex++) {
            maxPossibleCounts[setNum][sigmaSpecIndex]--;
          }
          maxPossibleCounts[setNum][specIndex]++;
          if (maxPossibleCounts[setNum][specIndex] < m_TargetCompositions[setNum][specIndex]) {
            return sigmaIndex;
          }
        }
      }
      
      return -1;
    }
    
  }
  
  protected class MaxCompositionFilter implements ArrayIndexer.Filter {

    private Species[] m_ControlledSpecies;
    private int[] m_MaxAllowedCounts;

    public MaxCompositionFilter(Species[] species, double[] maxCompositions) {
      
      m_ControlledSpecies = (Species[]) ArrayUtils.copyArray(species);
      m_MaxAllowedCounts = new int[species.length];
      for (int specNum = 0; specNum < species.length; specNum++) {
        int numSigmaSites = getNumSigmaSitesPerSpecies(species[specNum]);
        m_MaxAllowedCounts[specNum] = (int) Math.floor(maxCompositions[specNum] * numSigmaSites);
      }
      
    }
    
    public int getBranchIndex(int[] currentState) {
      
      int[] countBySpecies = new int[m_MaxAllowedCounts.length];
      for (int sigmaIndex = currentState.length - 1; sigmaIndex >= 0; sigmaIndex--) {
        Species species = getAllowedSpecies(sigmaIndex, currentState[sigmaIndex]);
        int index = ArrayUtils.findIndex(m_ControlledSpecies, species);
        if (index < 0) {continue;}
        countBySpecies[index]++;
        if (countBySpecies[index] > m_MaxAllowedCounts[index]) {return index;}
      }
      return -1;
    }
    
    
    
  }
  
  protected class ChargeBalanceFilter implements ArrayIndexer.Filter {

    // If the total charge, starting from the end, is outside these bounds then it cannot be charge balanced
    private double[] m_MinAllowedChargeBySite;
    private double[] m_MaxAllowedChargeBySite;
    
    private double m_NonSigmaCharge;
    
    public ChargeBalanceFilter(double nonSigmaCharge, double tolerance) {
      
      m_NonSigmaCharge = nonSigmaCharge;
      
      m_MinAllowedChargeBySite = new double[numSigmaSites()];
      m_MaxAllowedChargeBySite = new double[numSigmaSites()];
      
      double maxAllowedCharge = tolerance;
      double minAllowedCharge = -tolerance;
      for (int sigmaIndex = 0; sigmaIndex < m_MinAllowedChargeBySite.length; sigmaIndex++) {
        m_MinAllowedChargeBySite[sigmaIndex] = minAllowedCharge;
        m_MaxAllowedChargeBySite[sigmaIndex] = maxAllowedCharge;
        double minForSite = Double.POSITIVE_INFINITY;
        double maxForSite = Double.NEGATIVE_INFINITY;
        Atom[] allowedAtoms = m_AllowedAtoms[sigmaIndex];
        for (int specNum = 0; specNum < allowedAtoms.length; specNum++) {
          double oxidationState = allowedAtoms[specNum].getSpecies().getOxidationState();
          minForSite = Math.min(minForSite, oxidationState);
          maxForSite = Math.max(maxForSite, oxidationState);
        }
        maxAllowedCharge -= minForSite;
        minAllowedCharge -= maxForSite;
      }
    }

    public int getBranchIndex(int[] currentState) {

      double totalCharge = m_NonSigmaCharge;
      for (int sigmaIndex = currentState.length - 1; sigmaIndex >= 0; sigmaIndex--) {
        totalCharge += m_AllowedAtoms[sigmaIndex][currentState[sigmaIndex]].getSpecies().getOxidationState();
        if (totalCharge < m_MinAllowedChargeBySite[sigmaIndex]) {
          return sigmaIndex;
        }
        if (totalCharge > m_MaxAllowedChargeBySite[sigmaIndex]) {
          return sigmaIndex;
        }
      }
      return -1;
    }
    
  }
 
  
}
