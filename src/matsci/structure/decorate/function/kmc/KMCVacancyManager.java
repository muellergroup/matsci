/*
 * Created on Oct 12, 2014
 *
 */
package matsci.structure.decorate.function.kmc;

import java.util.Random;

import matsci.Species;
import matsci.engine.monte.kmc.IAllowsKMC;
import matsci.engine.monte.kmc.IKMCEvent;
import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.decorate.DecorationTemplate;
import matsci.structure.decorate.function.AppliedDecorationFunction;
import matsci.structure.decorate.function.ce.AbstractAppliedCE;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class KMCVacancyManager implements IAllowsKMC {

  private static final Random RANDOM = new Random();
  
  private int[][] m_AllowedJumpMap; // Default to nearest-neighbors
  
  private final AppliedDecorationFunction m_AppliedFunction;
  private final double m_CutoffDistance;
  private double m_kT;
  
  // I use a permanent dummy event as the list head for simplicity.  It doesn't count for anything.
  private final DummyEvent m_FirstEvent;
  
  private int m_NumEvents;
  private double m_TotalRate;
  
  private boolean m_AllowGrowth = true;
  private boolean m_AllowDissolution = true;
  
  public KMCVacancyManager(AbstractAppliedCE appliedCE) {
    this(appliedCE, getCutoff(appliedCE.getActiveGroups()), true, true);
  }
  
  public KMCVacancyManager(AbstractAppliedCE appliedCE, boolean allowGrowth, boolean allowDissolution) {
    this(appliedCE, getCutoff(appliedCE.getActiveGroups()), allowGrowth, allowDissolution);
  }
  
  protected static double getCutoff(ClusterGroup[] activeGroups) {
    double returnValue = 0;
    for (int groupNum = 0; groupNum < activeGroups.length; groupNum++) {
      returnValue = Math.max(returnValue, activeGroups[groupNum].getMaxDistance());
    }
    return returnValue + CartesianBasis.getPrecision();
  }

  public KMCVacancyManager(AppliedDecorationFunction appliedFunction) {
    this(appliedFunction, Double.POSITIVE_INFINITY, true, true);
  }
  
  public KMCVacancyManager(AppliedDecorationFunction appliedFunction, double cutoffDistance, boolean allowGrowth, boolean allowDissolution) {
    
    m_CutoffDistance = cutoffDistance;
    
    m_AllowGrowth = allowGrowth;
    m_AllowDissolution = allowDissolution;
    m_AppliedFunction = appliedFunction;
    m_FirstEvent = new DummyEvent();
    
    // Build the nearby site map.  
    SuperStructure superStructure = appliedFunction.getSuperStructure();
    SuperLattice superLattice = superStructure.getSuperLattice();
    SuperLattice lattice = superStructure.getSuperLattice();
    m_AllowedJumpMap = new int[appliedFunction.numSigmaSites()][];
    
    // First find the neighbors 
    // Only allows hops between sites on the same sublattice 
    DecorationTemplate template = appliedFunction.getPrimDecorationTemplate();
    int[] primCellOrigin = superLattice.getPrimCellOrigin(0);
    double[] distances = new double[appliedFunction.numSigmaSites()];
    for (int primIndex = 0; primIndex < template.numPrimSites(); primIndex++) {
      if (!template.isSigmaSite(primIndex)) {continue;}
      int subLattice = template.getSublatticeForSite(primIndex);
      int superSiteIndex = superStructure.getSiteIndex(primCellOrigin, primIndex);
      int sigmaIndex = appliedFunction.getSigmaIndex(superSiteIndex);
      Structure.Site baseSite = appliedFunction.getSigmaSite(sigmaIndex);

      // Find the nearest-neighbor distance for sites on this sublattice
      double minDistance = Double.POSITIVE_INFINITY;
      for (int neighborSigmaIndex = 0; neighborSigmaIndex < appliedFunction.numSigmaSites(); neighborSigmaIndex++) {
        if (neighborSigmaIndex == sigmaIndex) {continue;}
        int neighborSubLattice = appliedFunction.getSublattice(neighborSigmaIndex);
        if (neighborSubLattice != subLattice) {continue;}
        Coordinates siteCoords = appliedFunction.getSigmaSite(neighborSigmaIndex).getCoords();
        double distance = lattice.getDistanceInWignerSeitzCell(baseSite.getCoords(), siteCoords);
        distances[neighborSigmaIndex] = distance;
        minDistance = Math.min(minDistance, distance);
      }
      
      // Now find all of the integer coordinates of the neighbors
      int[][] neighborPrimCellLocations = new int[0][];
      int[] neighborPrimIndices = new int[0];
      for (int neighborSigmaIndex = 0; neighborSigmaIndex < appliedFunction.numSigmaSites(); neighborSigmaIndex++) {
        if (neighborSigmaIndex == sigmaIndex) {continue;}
        int neighborSubLattice = appliedFunction.getSublattice(neighborSigmaIndex);
        if (neighborSubLattice != subLattice) {continue;}
        double distance = distances[neighborSigmaIndex];
        if (distance > minDistance + CartesianBasis.getPrecision()) {continue;}
        SuperStructure.Site sigmaSite = (SuperStructure.Site) appliedFunction.getSigmaSite(neighborSigmaIndex);
        neighborPrimCellLocations = ArrayUtils.appendElement(neighborPrimCellLocations, sigmaSite.getPrimCellLocation());
        neighborPrimIndices = ArrayUtils.appendElement(neighborPrimIndices, sigmaSite.getParentSite().getIndex());
      }
      
      // Now build the neighbor map
      for (int primCellNum = 0; primCellNum < superStructure.numPrimCells(); primCellNum++) {
        int[] primCellOffset = superLattice.getPrimCellOrigin(primCellNum);
        int shiftedSuperIndex = superStructure.getSiteIndex(primCellOffset, primIndex);
        int shiftedSigmaIndex = appliedFunction.getSigmaIndex(shiftedSuperIndex);
        m_AllowedJumpMap[shiftedSigmaIndex] = new int[neighborPrimCellLocations.length];
        for (int neighborNum = 0; neighborNum < neighborPrimCellLocations.length; neighborNum++) {
          int[] neighborPrimCell = MSMath.arrayAdd(primCellOffset, neighborPrimCellLocations[neighborNum]);
          int neighborPrimIndex = neighborPrimIndices[neighborNum];
          int neighborSuperIndex = superStructure.getSiteIndex(neighborPrimCell, neighborPrimIndex);
          int neighborSigmaIndex = appliedFunction.getSigmaIndex(neighborSuperIndex);
          m_AllowedJumpMap[shiftedSigmaIndex][neighborNum] = neighborSigmaIndex;
        }
      }
    }
    
    this.refreshFromStructure();

  }
  
  public boolean allowsGrowthEvents() {
    return m_AllowGrowth;
  }
  
  public void allowGrowthEvents(boolean value) {
    m_AllowGrowth = value;
    refreshFromStructure();
  }
  
  public boolean allowsDissolutionEvents() {
    return m_AllowDissolution;
  }
  
  public void allowDissolutionEvents(boolean value) {
    m_AllowDissolution = value;
    refreshFromStructure();
  }
  
  public void setkT(double kT) {
    m_kT = kT;
    this.refreshAllRates();
  }
  
  public double getkT() {
    return m_kT;
  }
  
  public void setTemperature(double tempK) {
    m_kT = tempK * 8.61733238E-5;
    this.refreshAllRates();
  }
  
  public double getTemperature() {
    return m_kT / 8.61733238E-5;
  }
  
  public void refreshAllRates() {
    
    AbstractEvent currEvent = m_FirstEvent.getNextEvent();
    m_TotalRate = 0;
    while (currEvent != null) {
      currEvent.refreshRate();
      m_TotalRate += currEvent.getRate();
      currEvent = currEvent.getNextEvent();
    }
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#setState(java.lang.Object)
   */
  public void setState(Object snapshot) {
    m_AppliedFunction.setSigmaStates((int[]) snapshot);
    this.refreshFromStructure();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#getSnapshot(java.lang.Object)
   */
  public Object getSnapshot(Object template) {
    return m_AppliedFunction.getQuickSigmaStates((int[]) template);
  }
  
  public void addNewEvent(AbstractEvent newEvent) {
    newEvent.setNextEvent(m_FirstEvent.getNextEvent());
    m_FirstEvent.setNextEvent(newEvent);
    
    m_NumEvents++;
    m_TotalRate += newEvent.getRate();
  }
  
  public void refreshEvents(int sigmaIndex) {

    m_NumEvents = 0;
    m_TotalRate = 0;
    
    int[] allowedJumpSigmaIndices = m_AllowedJumpMap[sigmaIndex];
    
    // First clean out all events that might no longer be valid
    // We add them back in later -- maybe not the most efficient, but it's clean.
    AbstractEvent currEvent = m_FirstEvent;
    AbstractEvent nextEvent = m_FirstEvent.getNextEvent();
    while (nextEvent != null) {
      if (nextEvent.dependsOn(sigmaIndex) || nextEvent.dependsOn(allowedJumpSigmaIndices)) {
          nextEvent = nextEvent.getNextEvent();
          currEvent.setNextEvent(nextEvent);
      } else {
        m_NumEvents++;
        
        if (nextEvent.isAffectedBy(sigmaIndex)) {nextEvent.refreshRate();}

        m_TotalRate += nextEvent.getRate();
        currEvent = nextEvent;
        nextEvent = currEvent.getNextEvent();
      }
    }
    
    
    // Dissolution events are now special types of swap events
    /*if (!addGrowthEvents(sigmaIndex)) {
      addDissolutionEvent(sigmaIndex);
    }*/
    
    addGrowthEvents(sigmaIndex);
    for (int jumpNum = 0; jumpNum < allowedJumpSigmaIndices.length; jumpNum++) {
      int jumpSigmaIndex = allowedJumpSigmaIndices[jumpNum];
      /*if (!addGrowthEvents(jumpSigmaIndex)) {
        addDissolutionEvent(jumpSigmaIndex);
      }*/
      addGrowthEvents(jumpSigmaIndex);
      addSwapEvent(sigmaIndex, jumpSigmaIndex);
      int[] remoteJumpIndices = m_AllowedJumpMap[jumpSigmaIndex];
      
      // We have to add in all jumps for the jump sigma indices as well.
      for (int remoteSiteNum = 0; remoteSiteNum < remoteJumpIndices.length; remoteSiteNum++) {
        int remoteSigmaIndex = remoteJumpIndices[remoteSiteNum];
        
        // Don't double-count
        if (ArrayUtils.arrayContains(allowedJumpSigmaIndices, remoteSigmaIndex) && (jumpSigmaIndex > remoteSigmaIndex)) {
          continue;
        }
        
        if (remoteSigmaIndex == sigmaIndex) {continue;}
        addSwapEvent(jumpSigmaIndex, remoteSigmaIndex);
      }
    }
  }
  
  protected boolean addGrowthEvents(int sigmaIndex) {
    if (!m_AllowGrowth) {return false;}
    if (!isValidGrowth(sigmaIndex)) {return false;}
    for (int siteState = 0; siteState < m_AppliedFunction.numAllowedSpecies(sigmaIndex); siteState++) {
      Species species = m_AppliedFunction.getAllowedSpecies(sigmaIndex, siteState);
      if (species == Species.vacancy) {continue;}
      AbstractEvent newEvent = new GrowthEvent(sigmaIndex, species);
      addNewEvent(newEvent);
    }
    return true;
  }
  
  /**
   * Dissolutions are now treated in Swap events.
   * 
   * @param sigmaIndex
   * @return
   */
  protected boolean addDissolutionEvent(int sigmaIndex) {
    if (!m_AllowDissolution) {return false;}
    if (!isValidDissolution(sigmaIndex)) {return false;}
    AbstractEvent newEvent = new DissolutionEvent(sigmaIndex);
    addNewEvent(newEvent);
    return true;
  }
  
  protected boolean addSwapEvent(int sigmaIndex1, int sigmaIndex2) {
    if (!isValidSwap(sigmaIndex1, sigmaIndex2)) {return false;}
    AbstractEvent newEvent = new SwapEvent(sigmaIndex1, sigmaIndex2);
    addNewEvent(newEvent);
    return true;
  }
  
  
  public boolean isValidDissolution(int sigmaIndex) {
    if (m_AppliedFunction.getSpecies(sigmaIndex) == Species.vacancy) {
      return false;
    }
    int[] neighborIndices = m_AllowedJumpMap[sigmaIndex];
    for (int neighborNum = 0; neighborNum < neighborIndices.length; neighborNum++) {
      int neighborSigmaIndex = neighborIndices[neighborNum];
      if (m_AppliedFunction.getSpecies(neighborSigmaIndex) == Species.vacancy) {
        return true;
      }
    }
    return false;
  }
  
  public boolean isValidGrowth(int sigmaIndex) {
    if (m_AppliedFunction.getSpecies(sigmaIndex) != Species.vacancy) {
      return false;
    }
    int[] neighborIndices = m_AllowedJumpMap[sigmaIndex];
    boolean nearVacancy = false;
    boolean nearBulk = false;
    for (int neighborNum = 0; neighborNum < neighborIndices.length; neighborNum++) {
      int neighborSigmaIndex = neighborIndices[neighborNum];
      nearVacancy |= (m_AppliedFunction.getSpecies(neighborSigmaIndex) == Species.vacancy);
      nearBulk |= (m_AppliedFunction.getSpecies(neighborSigmaIndex) != Species.vacancy);
    }
    return nearVacancy && nearBulk;
  }
  
  public boolean isValidSwap(int sigmaIndex1, int sigmaIndex2) {
    
    Species spec1 = m_AppliedFunction.getSpecies(sigmaIndex1);
    Species spec2 = m_AppliedFunction.getSpecies(sigmaIndex2);
    if (spec1 == spec2) {return false;}
    return ((spec1 == Species.vacancy) || (spec2 == Species.vacancy));

    // The following should be unnecessary, given that they're on the same sublattice
    //return (m_AppliedFunction.allowsSpecies(m_SigmaIndices[1], spec1) && m_AppliedFunction.allowsSpecies(m_SigmaIndices[0], spec2));
    
  }

  private void refreshFromStructure() {
    
    m_FirstEvent.setNextEvent(null);
    for (int sigmaIndex = 0; sigmaIndex < m_AllowedJumpMap.length; sigmaIndex++) {
      int[] jumpIndices = m_AllowedJumpMap[sigmaIndex];
      for (int jumpNum = 0; jumpNum < jumpIndices.length; jumpNum++) {
        int jumpSigmaIndex = jumpIndices[jumpNum];
        if (sigmaIndex < jumpSigmaIndex) {continue;} // So we don't double-count events
        addSwapEvent(sigmaIndex, jumpSigmaIndex);
      }
      
      // Add the growth events
      addGrowthEvents(sigmaIndex);
      //addDissolutionEvent(sigmaIndex);
      
    }
  }
  
  public IKMCEvent getRandomEvent() {
    
    double maxRate = m_TotalRate * (1 - RANDOM.nextDouble()); // So it can't be zero
    AbstractEvent currEvent = m_FirstEvent.getNextEvent();
    if (currEvent == null) {
      throw new RuntimeException("No possible events.");
    }
    double cumulativeRate = currEvent.getRate();
    while (cumulativeRate < maxRate) {
      currEvent = currEvent.getNextEvent();
      cumulativeRate += currEvent.getRate();
    }
    
    return currEvent;
  }

  public double getTotalRate() {
    return m_TotalRate;
  }
  
  /**
   * I don't know if this will ever be needed, but it seems useful.
   * 
   * @return The current number of possible events
   */
  public int getNumEvents() {
    return m_NumEvents;
  }
  
  private abstract class AbstractEvent implements IKMCEvent {
    
    public abstract void setNextEvent(AbstractEvent event);
    public abstract AbstractEvent getNextEvent();

    public abstract boolean isValid();
    public abstract double getRate();
    
    public abstract void refreshRate();
    public abstract void trigger();
    
    public abstract boolean dependsOn(int sigmaIndex); // If this changes, the event is no longer valid
    public abstract boolean isAffectedBy(int sigmaIndex); // If this changes, we need to refresh the rate
    
    public boolean dependsOn(int[] sigmaIndices) {
      for (int i = 0; i < sigmaIndices.length; i++) {
        if (this.dependsOn(sigmaIndices[i])) {
          return true;
        }
      }
      return false;
    }
  }
  
  private class SwapEvent extends AbstractEvent {
    
    private int[] m_SigmaIndices;
    private int[] m_NewSiteStates;
    
    private AbstractEvent m_NextEvent;
    private double m_Rate;

    public SwapEvent(int sigmaIndex1, int sigmaIndex2) {
      m_SigmaIndices = new int[] {sigmaIndex1, sigmaIndex2};
      m_NewSiteStates = new int[2];
      
      // This might be a dissolution event
      Species newSpec1 = (m_AllowDissolution && this.isSurroundedByVacancies(sigmaIndex1, sigmaIndex2)) ? Species.vacancy : m_AppliedFunction.getSpecies(sigmaIndex2);
      Species newSpec2 = (m_AllowDissolution && this.isSurroundedByVacancies(sigmaIndex2, sigmaIndex1)) ? Species.vacancy : m_AppliedFunction.getSpecies(sigmaIndex1);
      
      int newSigmaState1 = m_AppliedFunction.getStateForSpecies(sigmaIndex1, newSpec1);
      int newSigmaState2 = m_AppliedFunction.getStateForSpecies(sigmaIndex2, newSpec2);
            
      m_NewSiteStates = new int[] {newSigmaState1, newSigmaState2};
      this.refreshRate();
    }
    
    protected boolean isSurroundedByVacancies(int sigmaIndex, int originSigmaIndex) {

      int[] jumpIndices = m_AllowedJumpMap[sigmaIndex];
      for (int neighborNum = 0; neighborNum < jumpIndices.length; neighborNum++) {
        int neighborIndex = jumpIndices[neighborNum];
        if (neighborIndex == originSigmaIndex) {continue;} // This is where the jump originates
        if (m_AppliedFunction.getSpecies(neighborIndex) != Species.vacancy) {
          return false;
        }
      }
      return true;
      
    }
    
    public void trigger() {
  
      m_AppliedFunction.setSigmaStates(m_SigmaIndices, m_NewSiteStates);
      
      for (int siteNum = 0; siteNum < m_SigmaIndices.length; siteNum++) {
        int sigmaIndex = m_SigmaIndices[siteNum];
        refreshEvents(sigmaIndex);
      }
      
    }
    
    public void refreshRate() {

      double delta = m_AppliedFunction.getDelta(m_SigmaIndices, m_NewSiteStates);
      m_Rate = (delta <= 0) ? 1 : Math.exp(-delta / m_kT);

    }
    
    public double getRate() {
      return m_Rate;
    }
    
    public AbstractEvent getNextEvent() {
      return m_NextEvent;
    }
    
    public void setNextEvent(AbstractEvent event) {
      m_NextEvent = event;
    }
    
    public boolean dependsOn(int sigmaIndex) {
      return (ArrayUtils.arrayContains(m_SigmaIndices, sigmaIndex));
    }
    
    public boolean isAffectedBy(int sigmaIndex) {
      BravaisLattice lattice = m_AppliedFunction.getSuperStructure().getDefiningLattice();
      Coordinates coords = m_AppliedFunction.getSigmaSite(sigmaIndex).getCoords();
      Coordinates thisCoords1 = m_AppliedFunction.getSigmaSite(m_SigmaIndices[0]).getCoords();
      if (lattice.getDistanceInWignerSeitzCell(coords, thisCoords1) < m_CutoffDistance) {return true;}
      
      Coordinates thisCoords2 = m_AppliedFunction.getSigmaSite(m_SigmaIndices[1]).getCoords();
      return (lattice.getDistanceInWignerSeitzCell(coords, thisCoords2) < m_CutoffDistance);
      
    }
    
    public boolean isValid() {

      return isValidSwap(m_SigmaIndices[0], m_SigmaIndices[1]);
      
    }
    
  }
  
  private class DummyEvent extends AbstractEvent {
    
    private AbstractEvent m_NextEvent;
    
    public DummyEvent() {}
    
    public boolean dependsOn(int sigmaIndex) {
      return false;
    }
    
    public boolean isAffectedBy(int sigmaIndex) {
      return false;
    }
    
    public boolean isValid() {
      return true;
    }
    
    public double getRate() {
      return 0;
    }
    
    public void refreshRate() {}
    public void trigger() {}

    public void setNextEvent(AbstractEvent event) {
      m_NextEvent = event;
    }

    public AbstractEvent getNextEvent() {
      return m_NextEvent;
    }
    
  }
  
  
  private class GrowthEvent extends AbstractEvent {
    
    private AbstractEvent m_NextEvent;
    private int m_SigmaIndex;
    private int m_GrowthSpeciesState;
    private double m_Rate;
    
    public GrowthEvent(int sigmaIndex, Species growthSpecies) {
      m_SigmaIndex = sigmaIndex;
      m_GrowthSpeciesState = m_AppliedFunction.getStateForSpecies(sigmaIndex, growthSpecies);
      this.refreshRate();
    }
    
    public void setNextEvent(AbstractEvent event) {
      m_NextEvent = event;
    }

    public AbstractEvent getNextEvent() {
      return m_NextEvent;
    }

    public boolean isValid() {
      return isValidGrowth(m_SigmaIndex);
    }

    public double getRate() {
      return m_Rate;
    }

    public void refreshRate() {
   // The applied function takes care of chemical potentials
      double delta = m_AppliedFunction.getDelta(m_SigmaIndex, m_GrowthSpeciesState); 
      m_Rate = (delta <= 0) ? 1 : Math.exp(-delta / m_kT);
    }

    public void trigger() {
      m_AppliedFunction.setSigmaState(m_SigmaIndex, m_GrowthSpeciesState);

      refreshEvents(m_SigmaIndex);
      
    }

    public boolean dependsOn(int sigmaIndex) {
      return (sigmaIndex == m_SigmaIndex);
    }

    public boolean isAffectedBy(int sigmaIndex) {
      BravaisLattice lattice = m_AppliedFunction.getSuperStructure().getDefiningLattice();
      Coordinates coords = m_AppliedFunction.getSigmaSite(sigmaIndex).getCoords();
      Coordinates thisCoords = m_AppliedFunction.getSigmaSite(m_SigmaIndex).getCoords();
      return (lattice.getDistanceInWignerSeitzCell(coords, thisCoords) < m_CutoffDistance);      
    }
    
  }
  
  /**
   * This is not used any more, as dissolution is treated as a special type of
   * swap.  I left this here in case we want to give people an option to use it
   * at some point.
   * 
   * @author Tim Mueller
   *
   */
  private class DissolutionEvent extends AbstractEvent {

    private AbstractEvent m_NextEvent;
    private int m_SigmaIndex;
    private int m_VacancyState;
    private double m_Rate;
    
    public DissolutionEvent(int sigmaIndex) {
      m_SigmaIndex = sigmaIndex;
      m_VacancyState = m_AppliedFunction.getStateForSpecies(sigmaIndex, Species.vacancy);
      this.refreshRate();
    }
    
    
    public void setNextEvent(AbstractEvent event) {
      m_NextEvent = event;
    }

    public AbstractEvent getNextEvent() {
      return m_NextEvent;
    }

    public boolean isValid() {
      return isValidDissolution(m_SigmaIndex);
    }

    public double getRate() {
      return m_Rate;
    }

    public void refreshRate() {
   // The applied function takes care of chemical potentials
      double delta = m_AppliedFunction.getDelta(m_SigmaIndex, m_VacancyState); 
      if (m_AppliedFunction.getSpecies(m_SigmaIndex) == Species.nickel) {
        System.currentTimeMillis();
      }
      m_Rate = (delta <= 0) ? 1 : Math.exp(-delta / m_kT);
    }

    public void trigger() {
      m_AppliedFunction.setSigmaState(m_SigmaIndex, m_VacancyState);
      
      refreshEvents(m_SigmaIndex);
      
    }

    public boolean dependsOn(int sigmaIndex) {
      return (sigmaIndex == m_SigmaIndex);
    }

    public boolean isAffectedBy(int sigmaIndex) {
      BravaisLattice lattice = m_AppliedFunction.getSuperStructure().getDefiningLattice();
      Coordinates coords = m_AppliedFunction.getSigmaSite(sigmaIndex).getCoords();
      Coordinates thisCoords = m_AppliedFunction.getSigmaSite(m_SigmaIndex).getCoords();
      return (lattice.getDistanceInWignerSeitzCell(coords, thisCoords) < m_CutoffDistance);      
    }
    
  }
  

}
