/*
 * Created on Mar 28, 2007
 *
 */
package matsci.structure.decorate.function.ce.structures;

import matsci.engine.monte.BasicRecorder;
import matsci.structure.decorate.function.ce.*;
import matsci.structure.decorate.function.ce.clusters.*;

public class CorrelationRecorder extends BasicRecorder {

  private AbstractAppliedCE m_AppliedCE;
  private double[][] m_HalfExpectationMatrix;
  private int[] m_ClusterOrbitsByVar;
  private int[] m_FunctionOrbitsByVar;
  
  public CorrelationRecorder(AbstractAppliedCE appliedCE, ClusterGroup[] groupsInOrder) {
    m_AppliedCE = appliedCE;
    int numTotalFunctionGroups = 0;
    for (int groupIndex = 0; groupIndex < groupsInOrder.length; groupIndex++) {
      ClusterGroup group = groupsInOrder[groupIndex];
      numTotalFunctionGroups += group.numFunctionGroups();
    }
    m_HalfExpectationMatrix = new double[numTotalFunctionGroups][];
    for (int rowNum = 0; rowNum < m_HalfExpectationMatrix.length; rowNum++) {
      m_HalfExpectationMatrix[rowNum] = new double[rowNum + 1];
    }
    m_ClusterOrbitsByVar = new int[numTotalFunctionGroups];
    m_FunctionOrbitsByVar = new int[numTotalFunctionGroups];
    int varNum = 0;
    for (int groupIndex = 0; groupIndex < groupsInOrder.length; groupIndex++) {
      ClusterGroup group = groupsInOrder[groupIndex];
      int groupNum = group.getGroupNumber();
      for (int functionGroupNum = 0; functionGroupNum < group.numFunctionGroups(); functionGroupNum++) {
        m_ClusterOrbitsByVar[varNum] = groupNum;
        m_FunctionOrbitsByVar[varNum] = functionGroupNum;
        varNum++;
      }
    }
    
  }
  
  /* (non-Javadoc)
   * @see matsci.engine.monte.IRecorder#recordState(double, double)
   */
  public void recordEndState(double value, double weight) {
    double[][] correlations = m_AppliedCE.getCorrelations();
    double newTotalWeight = m_TotalWeight + weight;
    double correctionRatio = m_TotalWeight / newTotalWeight;
    double incrementRatio = (1 - correctionRatio);
    for (int varNum = 0; varNum < m_ClusterOrbitsByVar.length; varNum++) {
      double[] expectationRow = m_HalfExpectationMatrix[varNum];
      int clustOrbitNum = m_ClusterOrbitsByVar[varNum];
      int functOrbitNum = m_FunctionOrbitsByVar[varNum];
      double correlation = correlations[clustOrbitNum][functOrbitNum];
      for (int prevVarNum = 0; prevVarNum < expectationRow.length; prevVarNum++) {
        int prevClustOrbitNum = m_ClusterOrbitsByVar[prevVarNum];
        int prevFunctOrbitNum = m_FunctionOrbitsByVar[prevVarNum];
        double prevCorrelation = correlations[prevClustOrbitNum][prevFunctOrbitNum];
        double correlationProduct = correlation * prevCorrelation;
        
        double oldAverage = expectationRow[prevVarNum];
        expectationRow[prevVarNum] = (correctionRatio * oldAverage) + (incrementRatio * correlationProduct);
      }
    }

    super.recordEndState(value, weight);
  }
  
  public double[][] getExpectationMatrix() {
    
    int numVars = m_HalfExpectationMatrix.length;
    double[][] returnArray = new double[numVars][numVars];
    for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
      double[] row = m_HalfExpectationMatrix[rowNum];
      for (int colNum = 0; colNum < row.length; colNum++) {
        returnArray[rowNum][colNum] = row[colNum];
        returnArray[colNum][rowNum] = row[colNum];
      }
    }
    return returnArray;
    
  }

}
