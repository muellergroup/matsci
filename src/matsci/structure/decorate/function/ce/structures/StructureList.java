/*
 * Created on Mar 22, 2006
 *
 */
package matsci.structure.decorate.function.ce.structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;

import matsci.Species;
import matsci.io.clusterexpansion.CorrelationsList;
import matsci.io.clusterexpansion.EnerIn;
import matsci.structure.IStructureData;
import matsci.structure.Structure;
//import matsci.structure.config.function.ce.CompactAppliedCE;
import matsci.structure.decorate.exceptions.IllegalOccupationException;
import matsci.structure.decorate.function.ce.AbstractAppliedCE;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.FastAppliedCE;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.function.ce.structures.ClustStructureData;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
import matsci.util.arrays.MatrixInverter;

public class StructureList {

  private ClusterExpansion m_ClusterExpansion;
  
  private ArrayList<ClustStructureData> m_KnownStructures = new ArrayList<ClustStructureData>();
  
  private boolean m_HullStarted = false;
  
  private double[][] m_HullPlanes = new double[0][];
  private BitSet[] m_HullStructuresByPlane = new BitSet[0]; // Rows are planes, columns are structures
  private BitSet m_HullStructures = new BitSet();
  
  public StructureList(ClusterExpansion ce) {
    m_ClusterExpansion = ce;
  }
  
    /**
     * This method reads in the correlations and calculated energies for a set of structures.
     * It assumes that correlations for all functions in all known groups are given.
     * 
     * @param cr The correlations for the structures.  These must be listed in the same order
     * as the energies are listed in the EnerIn.
     * @param er The corresponding energies for those structures.
     */
  public void addCorrelations(CorrelationsList cr, EnerIn er, boolean emptyIncluded) {
    // Default to all groups except the empty group
    this.addCorrelations(cr, er, m_ClusterExpansion.getAllGroups(emptyIncluded));
  }
  
  /**
   * This method reads in correlations and calculated energies for a set of structures.
   * It interprets the correlations as the correlations for the functions in the given 
   * set of groups.
   * 
       * @param cr The correlations for the structures.  These must be listed in the same order
       * as the energies are listed in the EnerIn.
       * @param er The corresponding energies for those structures.
   * @param activeGroups The set of groups for which the correlations are given.
   */
  public void addCorrelations(CorrelationsList cr, EnerIn er, ClusterGroup[] activeGroups) {
  
    // Right now let's try letting the user specify the correlation numbers in the ener.in file
    /*if (cr.numStructures() != er.numEntries()) {
      throw new RuntimeException("The number of correlation structures and structural energies must be the same");
    }*/
    
    // We make sure to always add the correlation for the emtpy group
    ClusterGroup emptyGroup = m_ClusterExpansion.getClusterGroup(0);
    boolean addEmpty = !ArrayUtils.arrayContains(activeGroups, emptyGroup);
    if (addEmpty) {
      activeGroups = (ClusterGroup[]) ArrayUtils.appendArray(new ClusterGroup[] {emptyGroup}, activeGroups);
    }
  
    // Grow the array of calculated structures
    int numNewStructures = er.numEntries();
    //int numOldStructures = m_KnownStructures.size();
    //m_KnownStructures = (ClustStructureData[]) ArrayUtils.growArray(m_KnownStructures, numNewStructures);
  
    // Add in the new structures
    for (int newStructNum = 0; newStructNum < numNewStructures; newStructNum++) {
      ClustStructureData newStructData = new ClustStructureData(m_ClusterExpansion.getBaseStructure(), -1, -1);
      int correlationIndex = er.getEntryNumber(newStructNum)  -1;
      if (correlationIndex >= cr.numStructures()) {
        throw new RuntimeException("Correlation index " + (correlationIndex + 1) + " invalid.  There are only " + cr.numStructures() + " correlations in the correlations file.");
      }
      double[] correlations = cr.getCorrelations(correlationIndex);
      if (addEmpty) {
        correlations = ArrayUtils.appendArray(new double[] {1}, correlations);
      }
      newStructData.setCorrelations(correlations, activeGroups);
      newStructData.setValue(er.getEnergy(newStructNum));
      newStructData.setDescription(er.getLabel(newStructNum));
      double[] concentrations = this.getConcentrationsFromCorrelations(correlations, activeGroups);
      if (concentrations != null) {newStructData.setConcentrations(concentrations);}
      //this.addStructure(newStructData, numOldStructures + newStructNum);
      this.addStructure(newStructData);
      //m_KnownStructures[numOldStructures + newStructNum] = newStructData;
    }
  }
  
  public void addCorrelations(AbstractAppliedCE appliedCE, boolean calculateValues) {
    if (appliedCE.getClusterExpansion() != m_ClusterExpansion) {
      throw new RuntimeException("Given applied cluster expansion is not based on this cluster expansion.");
    }
    ClustStructureData newStructData = new ClustStructureData(m_ClusterExpansion.getBaseStructure(), appliedCE.getSuperStructure().numPrimCells(), appliedCE.getSuperStructure().getLatticeIndex());
    try {
      newStructData.setDecorationID(appliedCE.readDecorationID());
    } catch (IllegalOccupationException e) {} // Fails silently
    
    newStructData.setCorrelations(appliedCE.getCorrelations());
    newStructData.setConcentrations(appliedCE.getSigmaConcentations());
    newStructData.setDescription(appliedCE.getSuperStructure().getDescription());
    
    double value = calculateValues ? appliedCE.getValue() / appliedCE.getSuperStructure().numPrimCells() : Double.NaN;
    newStructData.setValue(value);

    this.addStructure(newStructData);
    //m_KnownStructures = (ClustStructureData[]) ArrayUtils.appendElement(m_KnownStructures, newStructData);
  }
  
  public void addCorrelations(AbstractAppliedCE appliedCE, double value, boolean isNormalized) {
    
    value = isNormalized ? value : value / appliedCE.getSuperStructure().numPrimCells();
    this.addCorrelations(appliedCE, value);
  }
  
  /**
   * 
   * @param appliedCE
   * @param value Should be value per unit cell
   */
  public void addCorrelations(AbstractAppliedCE appliedCE, double value) {
    if (appliedCE.getClusterExpansion() != m_ClusterExpansion) {
      throw new RuntimeException("Given applied cluster expansion is not based on this cluster expansion.");
    }
    ClustStructureData newStructData = new ClustStructureData(m_ClusterExpansion.getBaseStructure(), appliedCE.getSuperStructure().numPrimCells(), appliedCE.getSuperStructure().getLatticeIndex());
    try {
    newStructData.setDecorationID(appliedCE.readDecorationID());
    } catch (IllegalOccupationException e) {} // Fails silently
    
    newStructData.setDescription(appliedCE.getSuperStructure().getDescription());
    newStructData.setCorrelations(appliedCE.getCorrelations());
    newStructData.setConcentrations(appliedCE.getSigmaConcentations());
    newStructData.setValue(value);
    this.addStructure(newStructData);
    //m_KnownStructures = (ClustStructureData[]) ArrayUtils.appendElement(m_KnownStructures, newStructData);
  }
  
  public void addCorrelations(IStructureData structureData, boolean calculateValues) {
    
    AbstractAppliedCE appliedCE = this.getAppliedCE(structureData);
    this.addCorrelations(appliedCE, calculateValues);
  }
  
  public void addCorrelations(IStructureData structureData) {
    this.addCorrelations(structureData, Double.NaN);
  }
  
  public void addCorrelations(IStructureData structureData, double value) {
    this.addCorrelations(structureData, value, true);
  }
  
  protected AbstractAppliedCE getAppliedCE(IStructureData structureData) {
    int[][] superToDirect = m_ClusterExpansion.getBaseStructure().getSuperToDirect(structureData);
    if (superToDirect == null) {
      throw new RuntimeException("Given structure is not a superstructure of the structure for which this cluster expansion is defined");
    }
    
    //CompactAppliedCE appliedCE = new CompactAppliedCE(m_ClusterExpansion, superToDirect);
    FastAppliedCE appliedCE = new FastAppliedCE(m_ClusterExpansion, superToDirect);
    appliedCE.setAllowedVacancies();
    SuperStructure superStructure = appliedCE.getSuperStructure();
    for (int siteNum = 0; siteNum < structureData.numDefiningSites(); siteNum++) {
      Structure.Site site = superStructure.getSite(structureData.getSiteCoords(siteNum));
      site.setSpecies(structureData.getSiteSpecies(siteNum));
    }
    superStructure.setDescription(structureData.getDescription());
    appliedCE.refreshFromStructure();
    appliedCE.activateAllGroups();
    return appliedCE;
  }
  
  public void addCorrelations(IStructureData structureData, double value, boolean isNormalized) {
    
    AbstractAppliedCE appliedCE = this.getAppliedCE(structureData);
    this.addCorrelations(appliedCE, value, isNormalized);
    
    /*
    double[][] correlations = appliedCE.getCorrelations();
    
    ClustStructureData newData = new ClustStructureData(m_ClusterExpansion.getPrimStructure(), appliedCE.getSuperStructure().numPrimCells(), appliedCE.getSuperStructure().getLatticeIndex());  // TODO:  We have information of the lattice type as well
    try {
      newData.setDecorationID(appliedCE.readDecorationID());
    } catch (IllegalOccupationException e) {} // Fails silently
    
    newData.setCorrelations(correlations);
    newData.setConcentrations(appliedCE.getConcentations());
    newData.setDescription(structureData.getDescription());
    
    if (isNormalized) {
      newData.setValue(value);
    } else {
      newData.setValue(value / appliedCE.getSuperStructure().numPrimCells());
    }
    
    this.addStructure(newData);*/
    //m_KnownStructures = (ClustStructureData[]) ArrayUtils.appendElement(m_KnownStructures, newData);
  }
  
  protected void addStructure(ClustStructureData structData) {
    m_KnownStructures.add(structData);
    if (m_HullStarted) {
      addStructureToHull(m_KnownStructures.size() - 1);
    }
  }

  protected void initializeHull() {
    if (m_HullStarted) {return;}
    m_HullStarted = true;
    for (int structIndex = 0; structIndex < m_KnownStructures.size(); structIndex++) {
      this.addStructureToHull(structIndex);
    }
  }
  
  protected void oldAddStructureToHull(int newIndex) {

    if (m_KnownStructures.size() < m_ClusterExpansion.numSigmaSpecies()) {return;}
    double tolerance = 1E-15;
    
    ClustStructureData structData = m_KnownStructures.get(newIndex);
    
    double[] newConcentrations = structData.getConcentrations();
    if (newConcentrations == null || newConcentrations.length == 0) {return;}
    if (newConcentrations.length != m_ClusterExpansion.numSigmaSpecies()) {
      throw new RuntimeException("The number of concentration values is different from the number of sigma species");
    }
    double value = structData.getValue();
    if (Double.isNaN(value)) {return;}
    
    // Find the planes we're below and the structures that define those planes
    boolean[] belowPlane =new boolean[m_HullPlanes.length];
    for (int planeNum = 0; planeNum < m_HullPlanes.length; planeNum++) {
      double hullValue = MSMath.dotProduct(m_HullPlanes[planeNum], newConcentrations);
      belowPlane[planeNum] = (value + tolerance < hullValue);
    }
    
    // Find the hullStructures
    int[] hullStructIndices = new int[0];
    if (m_HullPlanes.length == 0) {
      //for (int knownStructIndex = 0; knownStructIndex < m_KnownStructures.length; knownStructIndex++) {
      for (int knownStructIndex = 0; knownStructIndex <= newIndex; knownStructIndex++) {
        if (knownStructIndex == newIndex) {continue;}
        ClustStructureData knownStructure = m_KnownStructures.get(knownStructIndex);
        if (knownStructure == null) {continue;}
        if (Double.isNaN(knownStructure.getValue())) {continue;}
        double[] knownConcentrations = knownStructure.getConcentrations();
        if (knownConcentrations == null || knownConcentrations.length == 0) {continue;}
        hullStructIndices = ArrayUtils.appendElement(hullStructIndices, knownStructIndex);
      }
    } else {
      hullStructIndices = new int[m_HullStructures.cardinality()];
      int setIndex = -1;
      for (int hullStructNum = 0; hullStructNum < hullStructIndices.length; hullStructNum++) {
        setIndex = m_HullStructures.nextSetBit(setIndex + 1);
        hullStructIndices[hullStructNum] = setIndex;
      }
    }
    
    // Remove all of the planes that we are below
    m_HullStructures.clear();
    boolean deletedPlane = false;
    for (int planeNum = m_HullStructuresByPlane.length-1; planeNum >= 0; planeNum--) {
      if (belowPlane[planeNum]) {
        m_HullPlanes = ArrayUtils.removeElement(m_HullPlanes, planeNum);
        m_HullStructuresByPlane = (BitSet[]) ArrayUtils.removeElement(m_HullStructuresByPlane, planeNum);
        deletedPlane =true;
      } else {
        m_HullStructures.or(m_HullStructuresByPlane[planeNum]);
      }
    }
    
    // Finally build the new planes for this structure
    int[][] planeCombinations = MSMath.getCombinations(m_ClusterExpansion.numSigmaSpecies() -1, hullStructIndices.length, 0, new int[0]);
    if (planeCombinations.length == 0) {return;}
    
    double[][] concentrationMatrix = new double[m_ClusterExpansion.numSigmaSpecies()][];
    double[] values = new double[m_ClusterExpansion.numSigmaSpecies()];
    values[0] = structData.getValue();
    concentrationMatrix[0] = structData.getConcentrations();

    boolean madePlane = false;
    for (int candidatePlaneNum = 0; candidatePlaneNum < planeCombinations.length; candidatePlaneNum++) {
      int[] candidatePlane = planeCombinations[candidatePlaneNum];
      for (int pointNum = 0; pointNum < candidatePlane.length; pointNum++) {
        int structIndex = hullStructIndices[candidatePlane[pointNum]];
        ClustStructureData point = m_KnownStructures.get(structIndex);
        concentrationMatrix[pointNum + 1] = point.getConcentrations();
        values[pointNum + 1] = point.getValue();
      }
      MatrixInverter inverter = new MatrixInverter(concentrationMatrix);
      double[][] inverse = inverter.getInverseArray();
      if (inverse == null) {continue;} 
      double[] planeWeights = MSMath.matrixTimesVector(inverse, values);
      boolean goodPlane = true;
      for (int hullPointNum = 0; hullPointNum < hullStructIndices.length; hullPointNum++) {
        if (ArrayUtils.arrayContains(candidatePlane, hullPointNum)) {continue;}
        ClustStructureData hullPoint = m_KnownStructures.get(hullStructIndices[hullPointNum]);
        double planeValue = MSMath.dotProduct(planeWeights, hullPoint.getConcentrations());
        //System.out.println(planeValue - hullPoint.getValue());
        if (planeValue - tolerance > hullPoint.getValue()) {
          goodPlane = false;
          break;
        }
      }
      if (!goodPlane) {continue;}
      madePlane = true;
      BitSet planeMap = new BitSet();
      planeMap.set(newIndex);
      m_HullStructures.set(newIndex);
      for (int planePointNum =0; planePointNum < candidatePlane.length; planePointNum++) {
        int structIndex = hullStructIndices[candidatePlane[planePointNum]];
        planeMap.set(structIndex);
        m_HullStructures.set(structIndex);
      }
      m_HullStructuresByPlane = (BitSet[]) ArrayUtils.appendElement(m_HullStructuresByPlane, planeMap);
      m_HullPlanes = ArrayUtils.appendElement(m_HullPlanes, planeWeights);
    }
    if (deletedPlane && !madePlane) {
      throw new RuntimeException("Inconsistent hull");
    }
  }
  
  //TODO fix this!!  Fails for multi-species, and for first two entries in structure list being the same
  protected void addStructureToHull(int newIndex) {
    
    int numSigmaSpecies = m_ClusterExpansion.numSigmaSpecies();
    double tolerance = 1E-7;
    double fakeMinValue = -1E12;

    ClustStructureData newStructData = m_KnownStructures.get(newIndex);
    double[] newConcentrations = newStructData.getConcentrations();
    if (newConcentrations == null || newConcentrations.length == 0) {return;}
    if (newConcentrations.length != numSigmaSpecies) {
      throw new RuntimeException("The number of concentration values is different from the number of sigma species");
    }
    
    double value = newStructData.getValue();
    if (Double.isNaN(value)) {return;}
    
    m_HullStructures.set(newIndex);
    boolean newFullRim = (m_HullStructures.cardinality() == numSigmaSpecies || (m_HullPlanes.length == 0));
    //boolean newFullRim = (m_HullStructures.cardinality() == numSigmaSpecies);
    if ((m_HullStructures.cardinality() < numSigmaSpecies) && (m_HullPlanes.length == 0)) { // Create a dummy plane of infinite value
      m_HullPlanes = new double[1][numSigmaSpecies];
      Arrays.fill(m_HullPlanes[0], Double.POSITIVE_INFINITY);
      m_HullStructuresByPlane = new BitSet[] {(BitSet) m_HullStructures.clone()};
      return;
    }
    
    // Find the center of all of the hull points
    double[] centerConcentrations = new double[numSigmaSpecies];
    double minValue = Double.POSITIVE_INFINITY;
    int setIndex = -1;
    for (int edgeStructNum = 0; edgeStructNum < m_HullStructures.cardinality(); edgeStructNum++) {
      setIndex = m_HullStructures.nextSetBit(setIndex + 1);
      ClustStructureData structData = m_KnownStructures.get(setIndex);
      double[] concentrations = structData.getConcentrations();
      for (int specNum = 0; specNum < concentrations.length; specNum++) {
        centerConcentrations[specNum] += concentrations[specNum];
      }
      minValue = Math.min(minValue, newStructData.getValue());
    }
    centerConcentrations = MSMath.arrayDivide(centerConcentrations, m_HullStructures.cardinality());
    fakeMinValue = Math.min(minValue, fakeMinValue) - 1;
    
    // Remove all of the planes that we are below
    m_HullStructures.clear();
    BitSet edgePoints = new BitSet();
    boolean newRim = false;
    for (int planeNum = m_HullStructuresByPlane.length-1; planeNum >= 0; planeNum--) {
      double hullValue = MSMath.dotProduct(m_HullPlanes[planeNum], newConcentrations);
      if (Double.isNaN(hullValue) || (value + tolerance < hullValue)) {
        newRim |= (m_HullStructuresByPlane[planeNum].cardinality() < numSigmaSpecies);
        edgePoints.or(m_HullStructuresByPlane[planeNum]);
        m_HullPlanes = ArrayUtils.removeElement(m_HullPlanes, planeNum);
        m_HullStructuresByPlane = (BitSet[]) ArrayUtils.removeElement(m_HullStructuresByPlane, planeNum);
      } else {
        m_HullStructures.or(m_HullStructuresByPlane[planeNum]);
      }
    }
    
    // Find the common points
    if (newFullRim) {m_HullStructures = (BitSet) edgePoints.clone();} // We keep all points
    edgePoints.and(m_HullStructures); // Only keep the hull points that are used by other planes
    int[] edgeIndices = this.getSetIndices(edgePoints);
    if (newRim) {edgeIndices = ArrayUtils.appendElement(edgeIndices, -1);}
    
    // Finally build the new planes for this structure
    int[][] planeCombinations = new int[0][];
    if (newFullRim) { // We create all planes, not just those including this point
      edgeIndices = ArrayUtils.appendElement(edgeIndices, newIndex);
      planeCombinations = MSMath.getCombinations(numSigmaSpecies, edgeIndices, 0, new int[0]);
    } else { // We only need to add the ones that include this point
      planeCombinations = MSMath.getCombinations(numSigmaSpecies-1, edgeIndices, 0, new int[0]);
      for (int planeIndex = 0; planeIndex < planeCombinations.length; planeIndex++) {
        planeCombinations[planeIndex] = ArrayUtils.appendElement(planeCombinations[planeIndex], newIndex);
      }
    }
    if (planeCombinations.length == 0) {return;} // No new planes to create
    m_HullStructures.set(newIndex); // There are new planes to create, so this structure is on the hull
    
    double[][] concentrationMatrix = new double[numSigmaSpecies][];
    double[] values = new double[numSigmaSpecies];
    values[0] = newStructData.getValue();

    for (int candidatePlaneNum = 0; candidatePlaneNum < planeCombinations.length; candidatePlaneNum++) {
      int[] candidatePlane = planeCombinations[candidatePlaneNum];
      for (int pointNum = 0; pointNum < candidatePlane.length; pointNum++) {
        int structIndex = candidatePlane[pointNum];
        if (structIndex == -1) { // It's a rim plane
          concentrationMatrix[pointNum] = centerConcentrations;
          values[pointNum] = fakeMinValue;
        } else { // It's not
          ClustStructureData point = m_KnownStructures.get(structIndex);
          concentrationMatrix[pointNum] = point.getConcentrations();
          values[pointNum] = point.getValue();
        }
      }
      MatrixInverter inverter = new MatrixInverter(concentrationMatrix);
      double[][] inverse = inverter.getInverseArray();
      if (inverse == null) {continue;} 
      double[] planeWeights = MSMath.matrixTimesVector(inverse, values);
      BitSet planeMap = new BitSet();
      for (int planePointNum =0; planePointNum < candidatePlane.length; planePointNum++) {
        int structIndex = candidatePlane[planePointNum];
        if (structIndex < 0) {continue;}
        planeMap.set(structIndex);
        m_HullStructures.set(structIndex);
      }
      m_HullStructuresByPlane = (BitSet[]) ArrayUtils.appendElement(m_HullStructuresByPlane, planeMap);
      m_HullPlanes = ArrayUtils.appendElement(m_HullPlanes, planeWeights);
    }
    
  }
  
  protected int[] getSetIndices(BitSet bitSet) {
    int[] returnArray = new int[bitSet.cardinality()];
    int setIndex = -1;
    for (int returnStructNum = 0; returnStructNum < returnArray.length; returnStructNum++) {
      setIndex = bitSet.nextSetBit(setIndex + 1);
      returnArray[returnStructNum] = setIndex;
    }
    return returnArray;
  }

  public ClustStructureData[] getHullStructures() {
    this.initializeHull();
    ClustStructureData[] returnArray = new ClustStructureData[m_HullStructures.cardinality()];
    int setIndex = -1;
    for (int hullStructNum = 0; hullStructNum < returnArray.length; hullStructNum++) {
      setIndex = m_HullStructures.nextSetBit(setIndex + 1);
      returnArray[hullStructNum] = m_KnownStructures.get(setIndex);
    }
    return returnArray;
  }
  
  public int numHullPlanes() {
    this.initializeHull();
    return m_HullPlanes.length;
  }
  
  public int[][] getHullStructuresByPlane() {
    this.initializeHull();
    int[][] returnArray = new int[this.numHullPlanes()][];
    for (int planeNum = 0; planeNum < returnArray.length; planeNum++) {
      returnArray[planeNum] = this.getSetIndices(m_HullStructuresByPlane[planeNum]);
    }
    return returnArray;
  }
  
  public double[] getHullPlaneCoefficients(int planeNum) {
    this.initializeHull();
    return ArrayUtils.copyArray(m_HullPlanes[planeNum]);
  }
  
  public double[][] getHullPlaneCoefficients() {
    this.initializeHull();
    return ArrayUtils.copyArray(m_HullPlanes);
  }
  
  public double getHullValue(int knownStructNum) {
    this.initializeHull();
    return this.getHullValue(m_KnownStructures.get(knownStructNum));
  }
  
  public double getHullValue(ClustStructureData structure) {
    this.initializeHull();
    if (structure == null) {return Double.NaN;}
    double[] concentrations = structure.getConcentrations();
    if (concentrations == null || concentrations.length == 0) {return Double.NaN;}
    return this.getHullValue(concentrations);
  }
  
  public double getHullValue(double[] concentrations) {
    this.initializeHull();
    if (concentrations.length != m_ClusterExpansion.numSigmaSpecies()) {
      throw new RuntimeException("Correlations do not correspond to this cluster expansion.");
    }
    double highestValue = Double.NEGATIVE_INFINITY;
    for (int planeNum = 0; planeNum < m_HullPlanes.length; planeNum++) {
      if (m_HullStructuresByPlane[planeNum].cardinality() != concentrations.length) {continue;}
      double planeValue = MSMath.dotProduct(m_HullPlanes[planeNum], concentrations);
      highestValue = Math.max(planeValue, highestValue);
    }
    return highestValue;
  }

  public int[] getHullVerticesForStructure(int structNum) {
    this.initializeHull();
    if (this.isOnHull(structNum)) {
      return new int[] {structNum}; // It's on the hull
    }
    double[] concentrations = m_KnownStructures.get(structNum).getConcentrations();
    if (concentrations == null) {return null;}
    int hullPlaneNum = this.getHullPlaneNum(concentrations);
    return this.getHullPlaneVertices(hullPlaneNum);
  }
  
  public int[] getHullPlaneVertices(int planeNum) {
    return this.getSetIndices(m_HullStructuresByPlane[planeNum]);
  }
  
  /**
   * Returns the fractions in the same order as the vertices are returned by get HullVertices
   * @param concentrations
   * @return
   */
  public double[] getHullVertexFractions(double[] concentrations) {
    this.initializeHull();
    if (concentrations.length != m_ClusterExpansion.numSigmaSpecies()) {
      throw new RuntimeException("Correlations do not correspond to this cluster expansion.");
    }
    int hullPlaneNum = this.getHullPlaneNum(concentrations);
    int[] hullVertices =this.getHullPlaneVertices(hullPlaneNum);
    double[][] vertexConcentrations = new double[hullVertices.length][];
    for (int vertexNum = 0; vertexNum < vertexConcentrations.length; vertexNum++) {
      vertexConcentrations[vertexNum] = m_KnownStructures.get(hullVertices[vertexNum]).getConcentrations();
    }
    MatrixInverter inverter = new MatrixInverter(vertexConcentrations);
    double[][] inverse = inverter.getInverseArray();
    return MSMath.vectorTimesMatrix(concentrations, inverse);
  }
  
  /**
   * Returns the fractions in the same order as the vertices are returned by get HullVertices
   * @param concentrations
   * @return
   */
  public double[] getHullVertexFractions(int structNum) {
    this.initializeHull();
    if (this.isOnHull(structNum)) {
      return new double[] {1}; // It's on the hull
    }
    return this.getHullVertexFractions(m_KnownStructures.get(structNum).getConcentrations());
  }
  
  public int getHullPlaneNum(double[] concentrations) {
    this.initializeHull();
    if (concentrations.length != m_ClusterExpansion.numSigmaSpecies()) {
      throw new RuntimeException("Correlations do not correspond to this cluster expansion.");
    }
    double highestValue = Double.NEGATIVE_INFINITY;
    int returnValue = -1;
    for (int planeNum = 0; planeNum < m_HullPlanes.length; planeNum++) {
      if (m_HullStructuresByPlane[planeNum].cardinality() != concentrations.length) {continue;}
      double planeValue = MSMath.dotProduct(m_HullPlanes[planeNum], concentrations);
      if (planeValue > highestValue) {
        highestValue = planeValue;
        returnValue = planeNum;
      }
    }
    return returnValue;
  }
  
  
  public boolean isOnHull(int structIndex) {
    this.initializeHull();
    return m_HullStructures.get(structIndex);
  }
  
  protected double[] getConcentrationsFromCorrelations(double[] correlations, ClusterGroup[] activeGroups) {
    Species[] sigmaSpecies = m_ClusterExpansion.getAllSigmaSpecies();
    double[] speciesCount = new double[sigmaSpecies.length];
    int totalFunctNum = 0;
    int totalMultiplicity = 0;
    for (int groupNum = 0; groupNum < activeGroups.length; groupNum++) {
      ClusterGroup group = activeGroups[groupNum];
      if (group == null) {continue;}
      if (group.numSitesPerCluster() == 1) {
        totalMultiplicity += group.getMultiplicity();
        int numStates = group.numClusterStates();
        double[][] functValues = new double[numStates][numStates];
        double[] functTotals = new double[numStates];
        Arrays.fill(functValues[0], 1); // This is for the constant function
        functTotals[0] = m_ClusterExpansion.numSigmaSites(); // For the constant function
        for (int functNum = 0; functNum < group.numFunctionGroups(); functNum++) {
          functTotals[functNum + 1] = correlations[totalFunctNum + functNum] * group.getMultiplicity();
          for (int stateNum = 0; stateNum < numStates; stateNum++) {
            functValues[functNum+1][stateNum] = group.getFunctionGroupValue(stateNum, functNum);
          }
        }
        MatrixInverter inverter = new MatrixInverter(functValues);
        double[][] inverse = inverter.getInverseArray();
        if (inverse == null) {
          throw new RuntimeException("Cannot extract concentrations from correlations");
        }
        double[] sitesPerPrimCell = MSMath.matrixTimesVector(inverse, functTotals);
        for (int siteState = 0; siteState < sitesPerPrimCell.length; siteState++) {
          Species species = group.getAllowedSpecies(0, siteState);
          int totalSpeciesIndex = m_ClusterExpansion.getIndexForSigmaSpecie(species);
          speciesCount[totalSpeciesIndex] += sitesPerPrimCell[siteState];
        }
      }
      totalFunctNum += group.numFunctionGroups();
    }
    if (totalMultiplicity != m_ClusterExpansion.numSigmaSites()) {
      throw new RuntimeException("Correlations for all point cluster functions must be included to determine concentrations.");
    }
    for (int speciesIndex = 0; speciesIndex < speciesCount.length; speciesIndex++) {
      speciesCount[speciesIndex] /= m_ClusterExpansion.numSigmaSitesForSpecie(speciesIndex);
    }
    return speciesCount;
  }
  
  public boolean isKnownValue(int structIndex) {
    return !Double.isNaN(m_KnownStructures.get(structIndex).getValue());
  }
  
  /**
   * Given a set of cluster groups, return the known correlations for those groups.  This 
   * returns the correlations for structures in the same order the method getValues() uses.
   * 
   * @param activeGroups The set of groups for which we want the correlations.
   * @return The correlations for the given groups.  The first index corresponds to the
   * structure index, and the next index lists the correlations for the groups in the order
   * in which the groups were given.  First the correlations for the functions in the first
   * group are listed (in the order of the functions in the group), then the correlations for 
   * the functions in the second group, etc.  In other words, the same order as the correlations
   * are listed in the method setECI();
   */
  public double[][] getCorrelations(ClusterGroup[] activeGroups, boolean knownValues) {
    
    int numKnownValues = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (knownValues && Double.isNaN(m_KnownStructures.get(structNum).getValue())) {continue;}
      numKnownValues ++;
    }
    
    double[][] returnArray = new double[numKnownValues][];
    int nextIndex = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (knownValues && Double.isNaN(m_KnownStructures.get(structNum).getValue())) {continue;}
      returnArray[nextIndex++] = m_KnownStructures.get(structNum).getCorrelations(activeGroups);
    }
    return returnArray;
  }
  
  public double[][] getCorrelations(ClusterGroup[] activeGroups, boolean[] includeStructures) {
   
    int numStructures = 0;
    for (int structNum = 0; structNum < includeStructures.length; structNum++) {
      if (includeStructures[structNum]) {numStructures++;}
    }
    
    double[][] returnArray = new double[numStructures][];
    int nextIndex = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (includeStructures[structNum]) {
        returnArray[nextIndex++] = m_KnownStructures.get(structNum).getCorrelations(activeGroups);
      }
    }
    return returnArray;
  }
  
  public double[][] getMultCorrelations(ClusterGroup[] activeGroups, boolean knownValues) {
    
    int numKnownValues = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (knownValues && Double.isNaN(m_KnownStructures.get(structNum).getValue())) {continue;}
      numKnownValues ++;
    }
    
    double[][] returnArray = new double[numKnownValues][];
    int nextIndex = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (knownValues && Double.isNaN(m_KnownStructures.get(structNum).getValue())) {continue;}
      returnArray[nextIndex++] = m_KnownStructures.get(structNum).getMultCorrelations(activeGroups);
    }
    return returnArray;
  }
  
  public double[][] getMultCorrelations(ClusterGroup[] activeGroups, boolean[] includeStructures) {
   
    int numStructures = 0;
    for (int structNum = 0; structNum < includeStructures.length; structNum++) {
      if (includeStructures[structNum]) {numStructures++;}
    }
    
    double[][] returnArray = new double[numStructures][];
    int nextIndex = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (includeStructures[structNum]) {
        returnArray[nextIndex++] = m_KnownStructures.get(structNum).getMultCorrelations(activeGroups);
      }
    }
    return returnArray;
  }
  
  public double[][] getConcentrations(boolean[] includeStructures) {
    int numStructures = 0;
    for (int structNum = 0; structNum < includeStructures.length; structNum++) {
      if (includeStructures[structNum]) {numStructures++;}
    }
    
    double[][] returnArray = new double[numStructures][];
    int nextIndex = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (includeStructures[structNum]) {
        returnArray[nextIndex++] = m_KnownStructures.get(structNum).getConcentrations();
      }
    }
    return returnArray;
  }
  
  /**
   * 
   * @return All known calculated values.  The structures are sorted in the same order
   * as for the method getCorrelations();
   */
  public double[] getValues(boolean knownValues) {
    
    int numKnownValues = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (knownValues && Double.isNaN(m_KnownStructures.get(structNum).getValue())) {continue;}
      numKnownValues ++;
    }
    
    double[] returnArray = new double[numKnownValues];
    int nextIndex = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      double value = m_KnownStructures.get(structNum).getValue();
      if (knownValues && Double.isNaN(value)) {continue;}
      returnArray[nextIndex++] = value;
    }
    return returnArray;
  }
  
  public double[] getValues(boolean[] includeStructures) {
   
    int numStructures = 0;
    for (int structNum = 0; structNum < includeStructures.length; structNum++) {
      if (includeStructures[structNum]) {numStructures++;}
    }
    
    double[] returnArray = new double[numStructures];
    int nextIndex = 0;
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      if (includeStructures[structNum]) {
        returnArray[nextIndex++] = m_KnownStructures.get(structNum).getValue();
      }
    }
    return returnArray;
  }
  
  public double predictValue(ClusterGroup[] activeGroups, int knownStructNum) {
    
    double returnValue  =0;
    ClustStructureData structureData = m_KnownStructures.get(knownStructNum);
    double[] correlations = structureData.getCorrelations(activeGroups);
    int corrIndex = 0;
    for (int clustNum = 0; clustNum < activeGroups.length; clustNum++) {
      ClusterGroup clustOrbit = activeGroups[clustNum];
      for (int functNum = 0; functNum < clustOrbit.numFunctionGroups(); functNum++) {
        returnValue += clustOrbit.getCorrelationCoefficient(functNum) * correlations[corrIndex];
        corrIndex++;
      }
    }
    
    return returnValue;
  }
  
  public double[] predictValues(ClusterGroup[] activeGroups, boolean[] includeStructures) {
    int numStructures = 0;
    for (int structNum = 0; structNum < includeStructures.length; structNum++) {
      if (includeStructures[structNum]) {numStructures++;}
    }
    
    double[] returnArray = new double[numStructures];
    int returnIndex = 0;
    for (int structNum = 0; structNum < includeStructures.length; structNum++) {
      if (!includeStructures[structNum]) {continue;}
      returnArray[returnIndex++] = this.predictValue(activeGroups, structNum);
    }
    
    return returnArray;
  }
  
  public ClustStructureData getKnownStructureData(int knownStructureNum) {
    return m_KnownStructures.get(knownStructureNum);
  }
  
  public void clearKnownStructures() {
    m_KnownStructures = new ArrayList<ClustStructureData>();
    m_HullPlanes = new double[0][];
    m_HullStructuresByPlane = new BitSet[0]; // Rows are planes, columns are structures
    m_HullStructures = new BitSet();
    m_HullStarted = false;
  }
  
  public int numKnownStructures() {
    return m_KnownStructures.size();
  }
  
  public double getHullDistance(int structIndex) {
    this.initializeHull();
    return this.getHullDistance(m_KnownStructures.get(structIndex));
  }
  
  public double getHullDistance(ClustStructureData structData) {
    this.initializeHull();
    return structData.getValue() - this.getHullValue(structData);
  }
  
  public ClustStructureData containsCorrelations(double[][] correlations, double tolerance) {
    for (int structNum = 0; structNum < m_KnownStructures.size(); structNum++) {
      ClustStructureData structureData = m_KnownStructures.get(structNum);
      if (structureData == null) {continue;}
      if (structureData.containsCorrelations(correlations, tolerance)) {return structureData;}
    }
    return null;
  }

}
