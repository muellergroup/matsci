/*
 * Created on May 30, 2006
 *
 */
package matsci.structure.decorate.function.ce.structures;

import java.util.*;
import matsci.engine.monte.metropolis.IAllowsMetropolis;
import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.structure.decorate.function.ce.*;
import matsci.structure.decorate.function.ce.clusters.*;
import matsci.structure.decorate.function.ce.structures.evaluators.*;
import matsci.util.arrays.*;

public class StructureSetSelector implements IAllowsMetropolis {

  protected ClusterGroup[] m_ActiveGroups = new ClusterGroup[0];
  protected double[][] m_CandidateCorrelations = new double[0][];
  protected double[] m_Weights = new double[0];
  protected double[] m_Costs = new double[0];
  protected double m_MaxAllowedCost;
  
  protected AbstractCorrelationEvaluator m_Evaluator;
  
  protected BitSet m_ActiveCorrelations;
  protected double m_CurrentValue;
  protected double m_CurrentCost;
  
  protected Event m_Event = new Event();
  protected static Random m_Generator = new Random();
  
  public StructureSetSelector(AbstractCorrelationEvaluator evaluator) {
    m_Evaluator = evaluator;
    m_ActiveCorrelations = new BitSet();
    this.evaluateCurrentState();
  }
  
  protected void evaluateCurrentState() {
    m_CurrentCost = this.calculateCurrentCost();
    m_CurrentValue = this.calculateValue();
  }
  
  protected double calculateCurrentCost() {
    int structNum = 0; 
    double cost = 0;
    for (int structIndex = 0; structIndex < m_ActiveCorrelations.cardinality(); structIndex++) {
      structNum = m_ActiveCorrelations.nextSetBit(structNum);
      cost += m_Costs[structNum];
      structNum++;
    }
    return cost;
  }

  public boolean initializeMinimum(boolean useEven) {
    
    return useEven ? initializeMinimum(true, false) : initializeMinimum(false, true);
    
  }
  
  public boolean initializeMinimum() {
    
    return initializeMinimum(false, false);
    
  }
  
  protected boolean initializeMinimum(boolean useEven, boolean useOdd) {
    m_ActiveCorrelations.clear();
    this.evaluateCurrentState();
    
    int increment = (useOdd | useEven) ? 2 : 1;
    int[] indicesToAdd = new int[increment];

    int minIndex = ArrayUtils.minElementIndex(m_Costs);
    double lastMinCost = m_Costs[minIndex];
    if (useOdd) {
      if (m_Costs[minIndex] > m_MaxAllowedCost) {return false;}
      this.includeStructure(minIndex);
    }
    
    while (Double.isInfinite(m_CurrentValue)) {
      Arrays.fill(indicesToAdd, -1);
      double minCost = lastMinCost;
      double nextCost = m_CurrentCost;
      for (int stepNum = 0; stepNum < indicesToAdd.length; stepNum++) {
        for (int structNum = 0; structNum < m_Costs.length; structNum++) {
          if (m_ActiveCorrelations.get(structNum)) {continue;}
          if (ArrayUtils.arrayContains(indicesToAdd, structNum)) {continue;}
          if (m_Costs[structNum] <= minCost) {
            minCost = m_Costs[structNum];
            nextCost += minCost;
            indicesToAdd[stepNum] = structNum;
          }
          if (minCost == lastMinCost) {break;}
        }
        lastMinCost = minCost;
        if (nextCost > m_MaxAllowedCost) {
          return false;
        }
      }
      if (ArrayUtils.arrayContains(indicesToAdd, -1)) {
        return false;
      }
      for (int stepNum = 0; stepNum < indicesToAdd.length; stepNum++) {
        this.includeStructure(indicesToAdd[stepNum]);
      }
    }
    return true;
  }
  
  public void setCandidateCorrelations(ClusterExpansion ce, ClusterGroup[] activeGroups, double[] costs) {
    double[] weights = new double[ce.getStructureList().numKnownStructures()];
    Arrays.fill(weights, 1);
    this.setCandidateCorrelations(ce, activeGroups, weights, costs);
  }
  
  public void setCandidateCorrelations(ClusterExpansion ce) {
    double[] costs = new double[ce.getStructureList().numKnownStructures()];
    Arrays.fill(costs, 1);
    this.setCandidateCorrelations(ce, costs);
  }
  
  public void setCandidateCorrelations(ClusterExpansion ce, double[] costs) {
    double[] weights = new double[ce.getStructureList().numKnownStructures()];
    Arrays.fill(weights, 1);
    this.setCandidateCorrelations(ce, weights, costs);
  }

  public void setCandidateCorrelations(ClusterExpansion ce, double[] weights, double[] costs) {
    ClusterGroup[] activeGroups = ce.getAllGroups(true);
    this.setCandidateCorrelations(ce, activeGroups, weights, costs);
  }
  
  public void setCandidateCorrelations(ClusterExpansion ce, ClusterGroup[] activeGroups, double[] weights, double[] costs) {
    m_CandidateCorrelations = ce.getStructureList().getCorrelations(activeGroups, false);
    m_Weights =ArrayUtils.copyArray(weights);
    m_Costs = ArrayUtils.copyArray(costs);
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroups);
    m_Evaluator.setActiveGroups(activeGroups);

    this.evaluateCurrentState();
  }
  
  public void setCandidateCorrelations(double[][] candidateCorrelations, ClusterGroup[] activeGroups, double[] weights, double[] costs) {
    m_CandidateCorrelations = ArrayUtils.copyArray(candidateCorrelations);
    m_Weights =ArrayUtils.copyArray(weights);
    m_Costs = ArrayUtils.copyArray(costs);
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroups);
    m_Evaluator.setActiveGroups(activeGroups);
    
    this.evaluateCurrentState();
  }
  
  public double getCost() {
    return m_CurrentCost;
  }
  
  public void setMaxAllowedCost(double value) {
    m_MaxAllowedCost = value;
    if (m_CurrentCost > m_MaxAllowedCost) {
      m_CurrentValue = Double.POSITIVE_INFINITY;
    } else if (Double.isInfinite(m_CurrentValue)) {
      m_CurrentValue = this.calculateValue();
    }
  }
  
  public boolean isStructureIncluded(int structNum) {
    return m_ActiveCorrelations.get(structNum);
  }
  
  public void toggleStructureSet(int[] structNums) {
    for (int structIndex = 0; structIndex < structNums.length; structIndex++) {
      int structNum = structNums[structIndex];
      double increment = m_ActiveCorrelations.get(structNum) ? -1 : 1;
      m_CurrentCost += increment * m_Costs[structNum];
      m_ActiveCorrelations.flip(structNum);
    }
    if (m_CurrentCost > m_MaxAllowedCost) {
      m_CurrentValue = Double.POSITIVE_INFINITY;
    } else {
      m_CurrentValue = this.calculateValue();
    }
  }
  
  public void toggleStructure(int structNum) {
    if (this.isStructureIncluded(structNum)) {
      this.excludeStructure(structNum); 
    } else {
      this.includeStructure(structNum);
    }
  }
  
  public void includeStructure(int structNum) {
    if (this.isStructureIncluded(structNum)) {return;}
    m_ActiveCorrelations.set(structNum);
    m_CurrentCost += m_Costs[structNum];
    if (m_CurrentCost > m_MaxAllowedCost) {
      m_CurrentValue = Double.POSITIVE_INFINITY;
    } else {
      m_CurrentValue = this.calculateValue();
    }
  }
  
  public void excludeStructure(int structNum) {
    if (!this.isStructureIncluded(structNum)) {return;}
    m_ActiveCorrelations.clear(structNum);
    m_CurrentCost -= m_Costs[structNum];
    if (m_CurrentCost > m_MaxAllowedCost) {
      m_CurrentValue = Double.POSITIVE_INFINITY;
    } else {
      m_CurrentValue = this.calculateValue();
    }
  }
  
  protected double calculateValue() {
    int structNum = 0;
    double[][] correlations = new double[m_ActiveCorrelations.cardinality()][];
    double[] weights = new double[correlations.length];
    for (int structIndex = 0; structIndex < correlations.length; structIndex++) {
      structNum = m_ActiveCorrelations.nextSetBit(structNum);
      correlations[structIndex] = m_CandidateCorrelations[structNum];
      weights[structIndex] = m_Weights[structNum];
      structNum++;
    }
    return m_Evaluator.evaluate(m_ActiveGroups, correlations, weights);
  }

  public double getValue() {
    return m_CurrentValue;
  }

  public IMetropolisEvent getRandomEvent() {
    
    //int structNum1 = GENERATOR.nextInt(m_CandidateCorrelations.length);
    //int structNum2 = (structNum1 + GENERATOR.nextInt(m_CandidateCorrelations.length -1)) % m_CandidateCorrelations.length;
    double currentCost = m_CurrentCost;
    int structNum1 = this.getAllowedSwap(currentCost);
    double costDelta = m_ActiveCorrelations.get(structNum1) ? m_Costs[structNum1] * -1 : m_Costs[structNum1];
    int structNum2 = this.getAllowedSwap(m_CurrentCost + costDelta);
    m_Event.m_StructsToToggle[0] = structNum1;
    m_Event.m_StructsToToggle[1] = structNum2;
    return m_Event;
  }
  
  protected int getAllowedSwap(double currentCost) {
    int numAllowedToSet = 0;
    int numSet = m_ActiveCorrelations.cardinality();
    double maxAllowedCost = m_MaxAllowedCost - currentCost;
    for (int structNum = 0; structNum < m_Costs.length; structNum++) {
      if (m_ActiveCorrelations.get(structNum)) {continue;}
      if (m_Costs[structNum] <= maxAllowedCost) {numAllowedToSet++;}
    }
    
    int numAllowedSwaps = numSet + numAllowedToSet;
    int swapIndex = m_Generator.nextInt(numAllowedSwaps);
    int currIndex = 0;
    for (int structNum = 0; structNum < m_Costs.length; structNum++) {
      if ((m_Costs[structNum] > maxAllowedCost) && !m_ActiveCorrelations.get(structNum)) {continue;}
      if (currIndex == swapIndex) {return structNum;}
      currIndex++;
    }
    return -1;
  }

  public void setState(Object snapshot) {
    boolean[] includedStructures = (boolean[]) snapshot;
    m_ActiveCorrelations.clear();
    for (int structNum = 0; structNum < includedStructures.length; structNum++) {
      if (includedStructures[structNum]) {
        m_ActiveCorrelations.set(structNum);
      }
    }
    this.evaluateCurrentState();
  }

  public Object getSnapshot(Object template) {
    return this.getIncludedStructures(template);
  }
  
  public boolean[] getIncludedStructures(Object template) {
    boolean[] returnArray = (boolean[]) template;
    if (returnArray == null) {
      returnArray = new boolean[m_CandidateCorrelations.length];
    } else {
      Arrays.fill(returnArray, false);
    }
    int structNum = 0;
    for (int structIndex = 0; structIndex < m_ActiveCorrelations.cardinality(); structIndex++) {
      structNum =  m_ActiveCorrelations.nextSetBit(structNum);
      returnArray[structNum] = true;
      structNum++;
    }
    return returnArray;
  }
  
  protected class Event implements IMetropolisEvent {

    protected int[] m_StructsToToggle = new int[2];
    protected double m_OldValue;
    protected double m_OldCost;
    protected BitSet m_OldActiveCorrelations = new BitSet();
    
    public void trigger() {
      m_OldValue = m_CurrentValue;
      m_OldCost = m_CurrentCost;
      m_OldActiveCorrelations.clear();
      m_OldActiveCorrelations.or(m_ActiveCorrelations);
      toggleStructureSet(m_StructsToToggle);
    }

    public double getDelta() {
      double delta = this.triggerGetDelta();
      this.reverse();
      return delta;
    }

    public double triggerGetDelta() {
      this.trigger();
      return m_CurrentValue - m_OldValue;
    }

    public void reverse() {
      m_CurrentValue = m_OldValue;
      m_CurrentCost = m_OldCost;
      m_ActiveCorrelations.clear();
      m_ActiveCorrelations.or(m_OldActiveCorrelations);
    }
  }

}
