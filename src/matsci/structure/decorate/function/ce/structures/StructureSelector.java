/*
 * Created on Mar 31, 2007
 *
 */
package matsci.structure.decorate.function.ce.structures;

import matsci.structure.decorate.function.*;
import matsci.structure.decorate.function.ce.*;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.structures.evaluators.ICorrelationEvaluator;
import matsci.structure.superstructure.SuperStructure;

public class StructureSelector extends AppliedDecorationFunction {

  protected AbstractAppliedCE m_AppliedCE;
  protected ICorrelationEvaluator m_Evaluator;
  protected boolean m_UseWeights = false;
  
  public StructureSelector(AbstractAppliedCE appliedCE, ICorrelationEvaluator evaluator) {
    super(appliedCE.getClusterExpansion(), appliedCE.getSuperStructure());
    m_AppliedCE = appliedCE;
    m_Evaluator = evaluator;
  }
  
  public StructureSelector(ClusterExpansion ce, SuperStructure structure, ICorrelationEvaluator evaluator) {
    super(ce, structure);
    FastAppliedCE appliedCE = new FastAppliedCE(ce, structure);
    m_AppliedCE = new AppliedCECorrelationWrapper(appliedCE);
    m_Evaluator = evaluator;
  }
  
  public StructureSelector(ClusterExpansion ce, int[][] superToDirect, ICorrelationEvaluator evaluator) {
    super(ce, superToDirect);
    FastAppliedCE appliedCE = new FastAppliedCE(ce, this.getSuperStructure());
    m_AppliedCE = new AppliedCECorrelationWrapper(appliedCE);
    m_Evaluator = evaluator;
  }
  
  public void useWeights(boolean value) {
    m_UseWeights = value;
  }
  
  public boolean usesWeights() {
    return m_UseWeights;
  }
  
  protected double getWeight() {
    return m_UseWeights ? m_AppliedCE.getMultiplicity(m_AppliedCE.getQuickSigmaStates(null)) : 1;
  }

  public int getQuickSigmaState(int sigmaIndex) {
    return m_AppliedCE.getQuickSigmaState(sigmaIndex);
  }

  public int[] getQuickSigmaStates(int[] template) {
    return m_AppliedCE.getQuickSigmaStates(template);
  }

  public double getDelta(int sigmaIndex, int newState) {
    
    double[][] correlations = m_AppliedCE.getCorrelations();
    double weight = this.getWeight();
    double oldValue = m_Evaluator.evaluateNewCorrelations(correlations, weight);
    double[][] correlationDelta = m_AppliedCE.getCorrelationDelta(sigmaIndex, newState);
    if (correlationDelta == null) {
      return 0;
    }
    
    this.incrementCorrelations(correlations, correlationDelta);
    return m_Evaluator.evaluateNewCorrelations(correlations, weight) - oldValue;
  }

  /**
   * The way this is written, you should always run optimistic
   */
  public double getDelta(int[] sigmaIndices, int[] newStates) {
    
    int[] oldStates = new int[newStates.length];
    for (int stateNum = 0; stateNum < sigmaIndices.length; stateNum++) {
      oldStates[stateNum] = m_AppliedCE.getQuickSigmaState(sigmaIndices[stateNum]);
    }
    
    double delta = this.setSigmaStatesGetDelta(sigmaIndices, newStates);
    this.setSigmaStates(sigmaIndices, oldStates);
    
    return delta;
    
  }

  public double setSigmaStateGetDelta(int sigmaIndex, int newState) {
    double weight = this.getWeight();
    double[][] correlations = m_AppliedCE.getCorrelations();
    double oldValue = m_Evaluator.evaluateNewCorrelations(correlations, weight);
    double[][] correlationDelta = m_AppliedCE.setSigmaStateGetCorrelationDelta(sigmaIndex, newState);
    if (correlationDelta == null) {
      return 0;
    }
    
    this.incrementCorrelations(correlations, correlationDelta);
    return m_Evaluator.evaluateNewCorrelations(correlations, weight) - oldValue;
  }
  
  public double setSigmaStatesGetDelta(int[] sigmaIndices, int[] newStates) {
    
    double oldValue = this.getValue();
    for (int siteNum = 0; siteNum < sigmaIndices.length; siteNum++) {
      this.setSigmaState(sigmaIndices[siteNum], newStates[siteNum]);
    }
    return this.getValue() - oldValue;
  }
  
  public void setSigmaState(int sigmaIndex, int newState) {
    m_AppliedCE.setSigmaState(sigmaIndex, newState);
  }
  
  public void refreshFromHamiltonian() {
    m_AppliedCE.refreshFromHamiltonian();
  }

  public void refreshFromStructure() {
    m_AppliedCE.refreshFromStructure();
  }
  
  public void activateAllGroups() {
    m_AppliedCE.activateAllGroups();
  }
  
  public boolean activateGroup(ClusterGroup group) {
    boolean returnValue =  m_AppliedCE.activateGroup(group);
    return returnValue;
  }
  
  public void activateGroups(ClusterGroup[] groups) {
    m_AppliedCE.activateGroups(groups);
  }
  
  public void deactivateAllGroups() {
    m_AppliedCE.deactivateAllGroups();
  }
  
  public boolean isGroupActive(ClusterGroup group) {
    return m_AppliedCE.isGroupActive(group);
  }
  
  public ClusterGroup[] getActiveGroups() {
    return m_AppliedCE.getActiveGroups();
  }

  public boolean deactivateGroup(ClusterGroup group) {
    boolean returnValue = m_AppliedCE.deactivateGroup(group);
    return returnValue;
  }
  
  public ICorrelationEvaluator getCorrelationEvaluator() {
    return m_Evaluator;
  }
  
  public double getValue() {
    double[][] correlations = m_AppliedCE.getCorrelations();
    double weight = this.getWeight();
    return m_Evaluator.evaluateNewCorrelations(correlations, weight);
  }
  
  protected void incrementCorrelations(double[][] oldCorrelations, double[][] increment) {
    for (int groupNum = 0; groupNum < increment.length; groupNum++) {
      double[] incrementForGroup = increment[groupNum];
      if (incrementForGroup == null) {continue;}
      double[] correlationsForGroup = oldCorrelations[groupNum];
      for (int functGroupNum = 0; functGroupNum < incrementForGroup.length; functGroupNum++) {
        correlationsForGroup[functGroupNum] += incrementForGroup[functGroupNum];
      }
    }
  }

}
