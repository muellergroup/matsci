/*
 * Created on May 27, 2006
 *
 */
package matsci.structure.decorate.function.ce.structures.evaluators;

import java.util.Arrays;

import matsci.model.linear.AbstractL2RLSFitter;
import matsci.model.linear.L2DiagRLSFitter;
import matsci.model.linear.dist.*;

import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.*;

import matsci.util.arrays.ArrayUtils;

public abstract class AbstractCorrelationEvaluator implements ICorrelationEvaluator {
  
  protected final ClusterExpansion m_ClusterExpansion;
  protected ClusterGroup[] m_ActiveGroups = new ClusterGroup[0];
  protected boolean[] m_IncludedStructures = new boolean[0];
  protected double[] m_Weights;
  
  //protected double[] m_RegValues;
  protected double[] m_RegParameters;
  protected ICERegGenerator m_RegGenerator;
  
  protected AbstractL2RLSFitter m_Fitter;
  protected IDomainMatrix m_InputDistribution;
  
  protected double m_KnownValue;
  protected double m_MaxAllowedValue = 1E7;

  public AbstractCorrelationEvaluator(ClusterExpansion ce) {
    this(ce, 
        new L2DiagRLSFitter(ce.getStructureList().getCorrelations(ce.getAllGroups(true), false), ce.getStructureList().getValues(false)), 
        new LambdaRegGenerator(new ClusterGroup[0]), new double[] {0});
  }
  
  public AbstractCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter) {
    this(ce, fitter,  new LambdaRegGenerator(new ClusterGroup[0]), new double[] {0});
  }
  
  public AbstractCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, ICERegGenerator generator, double[] initParameters) {
    m_ClusterExpansion = ce;
    m_Fitter = fitter;
    if (fitter.numSamples() == ce.getStructureList().numKnownStructures()) {
      m_Weights = fitter.getWeights();
    } else {
      m_Weights =new double[ce.getStructureList().numKnownStructures()];
      Arrays.fill(m_Weights, 1);
    }
    m_IncludedStructures = new boolean[m_Weights.length];
    Arrays.fill(m_IncludedStructures, true);
    m_RegGenerator = generator;
    m_RegParameters = initParameters;
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroups) {
    
    // Check to see if the active groups already are set
    if (Arrays.equals(activeGroups, m_ActiveGroups)) {return;}
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroups);
    m_RegGenerator.setActiveGroups(activeGroups);
    this.refreshFitter();
    m_KnownValue = this.calculateKnownValue();
  }
  
  public void setRegGenerator(ICERegGenerator generator, double[] regParameters) {
    m_RegGenerator = generator;
    m_RegParameters = ArrayUtils.copyArray(regParameters);
    m_RegGenerator.setActiveGroups(this.getActiveGroups());
    this.refreshFitter();
    m_KnownValue = this.calculateKnownValue();
  }
  
  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }
  
  public void setWeights(double[] weights) {
    if (Arrays.equals(m_Weights, weights)) {return;}
    m_Weights = ArrayUtils.copyArray(weights);
    this.refreshFitter();
    m_KnownValue = this.calculateKnownValue();
  }
  
  /*
  public void setClusterRegValues(double[] regValues) {
    if (Arrays.equals(m_RegValues, regValues)) {return;}
    m_RegValues = ArrayUtils.copyArray(regValues);
    this.refreshKnownCorrelations();
    m_KnownValue = this.calculateKnownValue();
  }*/
  
  public void setIncludedStructures(boolean[] includedStructures) {
    if (Arrays.equals(m_IncludedStructures, includedStructures)) {return;}
    m_IncludedStructures = ArrayUtils.copyArray(includedStructures);
    this.refreshFitter();
    m_KnownValue = this.calculateKnownValue();
  }
  
  public void includeAllStructures() {
    int numOldWeights = m_Weights.length;
    boolean[] includedStructures =new boolean[m_ClusterExpansion.getStructureList().numKnownStructures()];
    Arrays.fill(includedStructures, true);
    if (includedStructures.length > numOldWeights) {
      double[] newWeights = new double[includedStructures.length - numOldWeights];
      Arrays.fill(newWeights, 1);
      m_Weights = ArrayUtils.appendArray(m_Weights, newWeights);
    }
    this.setIncludedStructures(includedStructures);
  }
  
  protected void refreshFitter() {

    double[][] knownCorrelations = m_ClusterExpansion.getStructureList().getCorrelations(m_ActiveGroups, m_IncludedStructures);
    double[] weights = this.getSetWeights(m_Weights, m_IncludedStructures);
    double[] dummyOutput = new double[weights.length];
    /*L2DiagRLSFitter newFitter;
    //double[] regularizer = this.getRegularizer();
    if (knownCorrelations.length == 0) {
      //newFitter = new L2DiagRLSFitter(regularizer); 
      newFitter = new L2DiagRLSFitter(); 
    } else {
      //newFitter = new L2DiagRLSFitter(knownCorrelations, dummyOutput, weights, regularizer);
      newFitter = new L2DiagRLSFitter(knownCorrelations, dummyOutput);
      newFitter = (L2DiagRLSFitter) newFitter.setWeights(weights);
    }*/
    
    AbstractL2RLSFitter newFitter = (AbstractL2RLSFitter) m_Fitter.clearSamples();
    if (knownCorrelations.length > 0) {
      newFitter = (AbstractL2RLSFitter) newFitter.addSamples(knownCorrelations, dummyOutput, weights);
    }
 
    //newFitter = (L2DiagRLSFitter) newFitter.setRegularizer(m_RegGenerator, m_RegParameters);
    newFitter = (AbstractL2RLSFitter) newFitter.setRegularizer(m_RegGenerator, m_RegParameters);
    //m_Fitter.copySettingsTo(newFitter);
    m_Fitter = newFitter;
  }
  
  public double evaluateKnownCorrelations(ClusterGroup[] activeGroups) {
    this.setActiveGroups(activeGroups);
    return m_KnownValue;
  }
  
  public double evaluateNewCorrelations(double[][] correlations, double weight) {
    
    int numCorrelations = 0;
    int numGroups = 0;
    for (int groupNum = 0; groupNum < correlations.length; groupNum++) {
      if (correlations[groupNum] == null || correlations[groupNum].length == 0) {continue;}
      numGroups++;
      numCorrelations += correlations[groupNum].length;
    }
    
    // If these correlations don't correspond to the known correlations, fix it.
    ClusterGroup[] activeGroups = new ClusterGroup[numGroups];
    int groupIndex = 0;
    for (int groupNum = 0; groupNum < correlations.length; groupNum++) {
      if (correlations[groupNum] == null || correlations[groupNum].length == 0) {continue;}
      activeGroups[groupIndex] = m_ClusterExpansion.getClusterGroup(groupNum);
      groupIndex++;
    }
    
    double[] flatArray = new double[numCorrelations];
    int correlationNum = 0;
    for (int groupNum = 0; groupNum < correlations.length; groupNum++) {
      if (correlations[groupNum] == null) {continue;}
      for (int functNum = 0; functNum < correlations[groupNum].length; functNum++) {
        flatArray[correlationNum++] = correlations[groupNum][functNum]; 
      }
    }
    
    double[][] correlationSet = new double[][] {flatArray};
    double[] weights = new double[] {weight};
    return this.evaluate(activeGroups, correlationSet, weights);
  }
  
  protected double[] getSetWeights(double[] weights, boolean[] structsToGet) {
    int numSet = 0;
    for (int i = 0; i < weights.length; i++) {
      if (structsToGet[i]) {numSet++;}
    }
    
    double[] returnArray = new double[numSet];
    int returnIndex = 0;
    for (int i = 0; i < weights.length; i++) {
      if (structsToGet[i]) {returnArray[returnIndex++] = weights[i];}
    }
    return returnArray;
  }
  
  /*protected double[] getRegularizer() {
    int numVars = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      numVars += m_ActiveGroups[groupIndex].numFunctionGroups();
    }

    if (m_RegValues == null) {return new double[numVars];}
    double[] returnArray = new double[numVars];
    int totalVarNum = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      double reg = m_RegValues[group.getGroupNumber()];
      for (int functNum = 0; functNum < group.numFunctionGroups(); functNum++) {
        returnArray[totalVarNum++] = reg;
      }
    }
    return returnArray;
  }*/

  protected double calculateKnownValue() {
    
    // We are very careful to make sure the known value is well conditioned here.
    if (m_Fitter.numSamples() == 0) {return Double.POSITIVE_INFINITY;}
    double returnValue = m_InputDistribution.getExpectedPredictionVariance(m_Fitter);
    if (returnValue < 0 || returnValue > m_MaxAllowedValue) {
      return Double.POSITIVE_INFINITY;
    }
    return returnValue;
  }
  
  public double evaluate(ClusterGroup[] activeGroups, double[][] correlations, double[] weights) {
    this.setActiveGroups(activeGroups);
    
    double returnValue = m_InputDistribution.getExpectedPredictionVariance(m_Fitter, m_KnownValue, correlations, weights);
    if (returnValue < 0 || returnValue > m_MaxAllowedValue) {
      return Double.POSITIVE_INFINITY;
    }
    return returnValue;
  }
}
