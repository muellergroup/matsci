/*
 * Created on Mar 28, 2007
 *
 */
package matsci.structure.decorate.function.ce.structures.evaluators;

import java.util.Arrays;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;
import matsci.model.linear.AbstractL2RLSFitter;
import matsci.model.linear.dist.GeneralDomainMatrix;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ICERegGenerator;
import matsci.util.arrays.ArrayUtils;

public class GeneralCorrelationEvaluator extends AbstractCorrelationEvaluator {

  private int[][] m_VarNumMap = new int[0][];
  private DoubleMatrix2D m_FullExpectationMatrix;
  
  public GeneralCorrelationEvaluator(ClusterExpansion ce, double[][] fullExpectationMatrix) {
    super(ce);
    this.init(ce.getAllGroups(true), fullExpectationMatrix);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public GeneralCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, double[][] fullExpectationMatrix) {
    super(ce, fitter);
    this.init(ce.getAllGroups(true), fullExpectationMatrix);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public GeneralCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, double[][] fullExpectationMatrix, double[] weights) {
    super(ce, fitter);
    m_Weights = ArrayUtils.copyArray(weights);
    this.init(ce.getAllGroups(true), fullExpectationMatrix);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public GeneralCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, ClusterGroup[] candidateGroups, double[][] fullExpectationMatrix, double[] weights) {
    super(ce, fitter);
    m_Weights = ArrayUtils.copyArray(weights);
    this.init(candidateGroups, fullExpectationMatrix);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public GeneralCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, ClusterGroup[] candidateGroups, double[][] fullExpectationMatrix) {
    super(ce, fitter);
    this.init(candidateGroups, fullExpectationMatrix);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public GeneralCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, ICERegGenerator generator, double[] initParameters, double[][] fullExpectationMatrix) {
    super(ce, fitter, generator, initParameters);
    this.init(ce.getAllGroups(true), fullExpectationMatrix);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  protected void init(ClusterGroup[] candidateGroups, double[][] fullExpectationMatrix) {
    
    m_FullExpectationMatrix = DoubleFactory2D.dense.make(fullExpectationMatrix);
    
    int maxGroupNumber = -1;
    for (int groupIndex = 0; groupIndex < candidateGroups.length; groupIndex++) {
      ClusterGroup group = candidateGroups[groupIndex];
      maxGroupNumber = Math.max(maxGroupNumber, group.getGroupNumber());
    }
    m_VarNumMap = new int[maxGroupNumber+1][];
    int varNum = 0;
    for (int groupIndex = 0; groupIndex < candidateGroups.length; groupIndex++) {
      ClusterGroup group = candidateGroups[groupIndex];
      m_VarNumMap[group.getGroupNumber()] = new int[group.numFunctionGroups()];
      for (int functGroupNum= 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        m_VarNumMap[group.getGroupNumber()][functGroupNum] = varNum++;
      }
    }
    
    this.refreshActiveExpectations();
  }
  
  protected void refreshFitter() {
    super.refreshFitter();
    this.refreshActiveExpectations();
  }
  
  protected void refreshActiveExpectations() {
    
    int numActiveVars = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      numActiveVars += group.numFunctionGroups();
    }
    
    int[] activeVars = new int[numActiveVars];
    int activeIndex = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      int[] varNums = m_VarNumMap[group.getGroupNumber()];
      if (varNums == null) {
        throw new RuntimeException("Trying to activate cluster that is not allowed: " + group.getGroupNumber());
      }
      for (int varIndex = 0; varIndex < varNums.length; varIndex++) {
        activeVars[activeIndex++] = varNums[varIndex];
      }
    }
    
    double[][] activeExpectations = m_FullExpectationMatrix.viewSelection(activeVars, activeVars).toArray();
    m_InputDistribution = new GeneralDomainMatrix(activeExpectations);
  }

}
