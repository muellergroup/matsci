/*
 * Created on May 27, 2006
 *
 */
package matsci.structure.decorate.function.ce.structures.evaluators;

import matsci.model.linear.*;
import matsci.model.linear.dist.*;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.Structure;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;

public class RMSPredictiveCorrelationEvaluator extends AbstractCorrelationEvaluator {

  //private DoubleMatrix1D[] m_Expectations;
  //private DoubleMatrix2D[] m_Overlaps;
  private double[][] m_Expectations;
  private double[][][] m_Overlaps;
  //private GeneralDomainMatrix m_InputDistribution;
  private double m_NumPrimCells = Double.POSITIVE_INFINITY;
  private double[][][] m_PointGroupProbabilities;

  public RMSPredictiveCorrelationEvaluator(ClusterExpansion ce, double[][][] pointGroupProbabilities) {
    super(ce);
    m_PointGroupProbabilities = ArrayUtils.copyArray(pointGroupProbabilities);
    m_Expectations = new double[pointGroupProbabilities.length][];
    m_Overlaps = new double[pointGroupProbabilities.length][][];
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public RMSPredictiveCorrelationEvaluator(ClusterExpansion ce, L2DiagRLSFitter fitter, double[][][] pointGroupProbabilities) {
    super(ce, fitter);
    m_PointGroupProbabilities = ArrayUtils.copyArray(pointGroupProbabilities);
    m_Expectations = new double[pointGroupProbabilities.length][];
    m_Overlaps = new double[pointGroupProbabilities.length][][];
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  protected void refreshFitter() {
    super.refreshFitter();
    this.refreshInputDistribution();
  }
  
  private void refreshInputDistribution() {
    for (int pointNum = 0; pointNum < m_PointGroupProbabilities.length; pointNum++) {
      this.setExpectations(pointNum);
      this.setOverlaps(pointNum);
    }
    if (m_Expectations.length == 0) {return;}
    int numFunctionGroups = m_Expectations[0].length;
    double[][] distributionMatrix = new double[numFunctionGroups][numFunctionGroups];
    for (int pointNum = 0; pointNum < m_Expectations.length; pointNum++) {
      double[] expectations = m_Expectations[pointNum];
      double[][] overlaps = m_Overlaps[pointNum];
      for (int rowNum = 0; rowNum < distributionMatrix.length; rowNum++) {
        for (int colNum = 0; colNum < distributionMatrix.length; colNum++) {
          distributionMatrix[rowNum][colNum] += ((overlaps[rowNum][colNum] / m_NumPrimCells) + (expectations[rowNum] * expectations[colNum])) / m_Expectations.length;
        }
      }
    }
    m_InputDistribution = new GeneralDomainMatrix(distributionMatrix);
  }

  // Because of correlation between overlapping clusters, the expectation products alone are only an approximation
  private void setOverlaps(int pointNum) {
    
    int numFunctionGroups = m_Expectations[pointNum].length;
    //double[][] overlaps = new double[m_ActiveGroups.length][m_ActiveGroups.length];
    double[][] overlaps = new double[numFunctionGroups][numFunctionGroups];
    ClusterExpansion ce = m_ClusterExpansion;
    Structure primStructure = ce.getBaseStructure();
    
    int totalFunctNum = 0;
    for (int groupNum = 0; groupNum < m_ActiveGroups.length; groupNum++) {
      ClusterGroup activeGroup = m_ActiveGroups[groupNum];
      Coordinates[] sampleCoords = activeGroup.getSampleCoords();
      Structure.Site[] sampleSites = primStructure.getSites(sampleCoords);
      // Iterate over function groups for this group
      for (int functGroupNum = 0; functGroupNum < activeGroup.numFunctionGroups(); functGroupNum++) {
        int[] clustFuncts = activeGroup.getFunctionsForFunctionGroup(functGroupNum);
        int[] sampleSiteFuncts = activeGroup.getSiteFunctions(clustFuncts[0]);
        int prevTotalFunctNum = 0;
        for (int prevGroupNum = 0; prevGroupNum <= groupNum; prevGroupNum++) {
          ClusterGroup prevGroup = m_ActiveGroups[prevGroupNum];
          for (int prevFunctGroupNum = 0; prevFunctGroupNum < prevGroup.numFunctionGroups(); prevFunctGroupNum++) {
            int[] prevClustFuncts = prevGroup.getFunctionsForFunctionGroup(prevFunctGroupNum);
            int[] prevSampleSiteFuncts = prevGroup.getSiteFunctions(prevClustFuncts[0]);
            for (int prevClustImage = 0; prevClustImage < prevGroup.numPrimClusters(); prevClustImage++) {
              Coordinates[] prevClustCoords = prevGroup.getPrimClusterCoords(prevClustImage);
              Structure.Site[] prevClustSites = primStructure.getSites(prevClustCoords);
              double overlapIncrement = this.getOverlapDelta(pointNum, sampleSites, prevClustSites, sampleSiteFuncts, prevSampleSiteFuncts);
              overlaps[totalFunctNum][prevTotalFunctNum] += overlapIncrement;
            }
            overlaps[totalFunctNum][prevTotalFunctNum] *= (double) clustFuncts.length * prevClustFuncts.length / prevGroup.getMultiplicity();
            
            // This next line adjusts for the value derived from the expectations matrix
            overlaps[prevTotalFunctNum][totalFunctNum] = overlaps[totalFunctNum][prevTotalFunctNum];
            prevTotalFunctNum++;
          }
        }

        totalFunctNum++;
      }
    }
    
    m_Overlaps[pointNum] = overlaps;
  }
  
  private double getOverlapDelta(int pointNum, Structure.Site[] sites1, Structure.Site[] sites2, int[] siteFuncts1, int[] siteFuncts2) {
    
    Vector[] translations = new Vector[0];
    for (int siteNum1 = 0; siteNum1 < sites1.length; siteNum1++) {
      Structure.Site site1 = sites1[siteNum1];
      for (int siteNum2 = 0; siteNum2 < sites2.length; siteNum2++) {
        Structure.Site site2 = sites2[siteNum2];
        if (site1.getIndex() != site2.getIndex()) {continue;}
        if (siteFuncts1[siteNum1] != siteFuncts2[siteNum2]) {continue;}
        Vector translation = new Vector(site1.getCoords(), site2.getCoords());
        boolean match = false;
        for (int prevTranslationNum = 0; prevTranslationNum < translations.length; prevTranslationNum++) {
          //if (translations[prevTranslationNum].getDirection().isCloseEnoughTo(translation.getDirection())) {
          if (translations[prevTranslationNum].subtract(translation).length() < CartesianBasis.getPrecision()) {
                match = true;
            break;
          }
        }
        if (match) continue;
        translations = (Vector[]) ArrayUtils.appendElement(translations, translation);
      }
    }
    
    double oldValue = 0;
    if (translations.length != 0) {
      oldValue = this.getExpectation(pointNum, sites1, siteFuncts1, 1) * this.getExpectation(pointNum, sites2, siteFuncts2, 1);
    }
    
    double returnValue = 0;
    for (int translationNum = 0; translationNum < translations.length; translationNum++) {
      Vector translation = translations[translationNum];
      Structure.Site[] overlapSites = new Structure.Site[0];
      Structure.Site[] remainingSites1 = (Structure.Site[]) ArrayUtils.copyArray(sites1);
      Structure.Site[] remainingSites2 = (Structure.Site[]) ArrayUtils.copyArray(sites2);
      int[] remainingFuncts1 = ArrayUtils.copyArray(siteFuncts1);
      int[] remainingFuncts2 = ArrayUtils.copyArray(siteFuncts2);
      int[] overlapFuncts = new int[0];
      for (int siteNum1 = sites1.length-1; siteNum1 >= 0; siteNum1--) {
        Structure.Site site1 = remainingSites1[siteNum1];
        int numRemainingSites2 = remainingSites2.length;
        for (int siteNum2 = numRemainingSites2-1; siteNum2 >= 0; siteNum2--) {
          Structure.Site site2 = remainingSites2[siteNum2];
          if (site1.getIndex() != site2.getIndex()) {continue;}
          if (remainingFuncts1[siteNum1] != remainingFuncts2[siteNum2]) {continue;}
          if (site1.getCoords().translateBy(translation).isCloseEnoughTo(site2.getCoords())) {
            overlapFuncts = ArrayUtils.appendElement(overlapFuncts, remainingFuncts1[siteNum1]);
            remainingFuncts1 = ArrayUtils.removeElement(remainingFuncts1, siteNum1);
            remainingFuncts2 = ArrayUtils.removeElement(remainingFuncts2, siteNum2);
            overlapSites = (Structure.Site[]) ArrayUtils.appendElement(overlapSites, site1);
            remainingSites1 = (Structure.Site[]) ArrayUtils.removeElement(remainingSites1, siteNum1);
            remainingSites2 = (Structure.Site[]) ArrayUtils.removeElement(remainingSites2, siteNum2);
            break;
          }
        }
      }
      
      Structure.Site[] nonOverlapSites = (Structure.Site[]) ArrayUtils.appendArray(remainingSites1, remainingSites2);
      int[] nonOverlapFuncts = ArrayUtils.appendArray(remainingFuncts1, remainingFuncts2);

      double overlapValue = this.getExpectation(pointNum, overlapSites, overlapFuncts, 2) * this.getExpectation(pointNum, nonOverlapSites, nonOverlapFuncts, 1);
      returnValue += overlapValue - oldValue;
    }
    return returnValue;
  }
  
  private double getExpectation(int pointNum, Structure.Site[] sites, int[] siteFuncts, int power) {
    
    ClusterExpansion ce = m_ClusterExpansion;
    SpaceGroup ceSpaceGroup = ce.getSpaceGroup();
    
    // Figure out the point clusters for this group (should be sublattices in next version)
    double returnValue = 1;
    int pointGroupNum = -1;
    for (int siteNum = 0; siteNum < sites.length; siteNum++) {
      double siteValue =0;
      int primSiteIndex = sites[siteNum].getIndex();
      int functNum = siteFuncts[siteNum];
      for (int pointGroupIndex = 0; pointGroupIndex <= ce.getMaxGroupNumber(); pointGroupIndex++) {
        ClusterGroup pointGroup = ce.getClusterGroup(pointGroupIndex);
        if (pointGroup.numSitesPerCluster() != 1) {continue;}
        Coordinates pointCoords = pointGroup.getSampleCoords()[0];
        if (ceSpaceGroup.areSymmetricallyEquivalent(pointCoords, sites[siteNum].getCoords())) {
          pointGroupNum = pointGroup.getGroupNumber();
          break;
        }
      }
      for (int stateNum = 0; stateNum < ce.numAllowedSpecies(primSiteIndex); stateNum++) {
        double probability = m_PointGroupProbabilities[pointNum][pointGroupNum][stateNum];
        double functValue = ce.getFunctionValue(primSiteIndex, stateNum, functNum);
        siteValue += probability * Math.pow(functValue, power);
      }
      returnValue *= siteValue;
    }
    
    return returnValue;
  }
  
  private void setExpectations(int pointNum) {
    
    ClusterExpansion ce = m_ClusterExpansion;
    
    int numTotalFunctGroups = 0;
    for (int groupIndex =0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      numTotalFunctGroups += m_ActiveGroups[groupIndex].numFunctionGroups();
    }
    double[] expectations = new double[numTotalFunctGroups];
    
    int totalFunctNum = 0;
    for (int groupNum = 0; groupNum < m_ActiveGroups.length; groupNum++) {
      ClusterGroup activeGroup = m_ActiveGroups[groupNum];
      Coordinates[] sampleActiveCoords = activeGroup.getSampleCoords();
      Structure.Site[] sampleSites = ce.getBaseStructure().getSites(sampleActiveCoords);
      
      // Now find the expectation value for each function group
      for (int functGroupNum = 0; functGroupNum < activeGroup.numFunctionGroups(); functGroupNum++) {
        int[] clustFuncts = activeGroup.getFunctionsForFunctionGroup(functGroupNum);
        int[] sampleSiteFuncts = activeGroup.getSiteFunctions(clustFuncts[0]);
        expectations[totalFunctNum] = this.getExpectation(pointNum, sampleSites, sampleSiteFuncts, 1) * clustFuncts.length; // This is not the multiplicity, so it's OK.
        totalFunctNum++;
      }
    }
    
    m_Expectations[pointNum] = expectations;
  }
  
  public void setNumPrimCells(double numPrimCells) {
    if (numPrimCells == m_NumPrimCells) {return;}
    m_NumPrimCells = numPrimCells;
    this.refreshFitter();
  }

}
