/*
 * Created on May 27, 2006
 *
 */
package matsci.structure.decorate.function.ce.structures.evaluators;

import java.util.Arrays;

import matsci.model.linear.*;
import matsci.model.linear.dist.*;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix2D;

public class KnownCorrelationEvaluator extends AbstractCorrelationEvaluator {

  private int[][] m_VarNumMap = new int[0][];
  private DoubleMatrix2D m_FullExpectationMatrix;
  
  /*public KnownCorrelationEvaluator(ClusterExpansion ce, LeastSquaresFitter fitter) {
    super(ce, fitter);
    boolean[] structsToOptimize = new boolean[ce.getStructureList().numKnownStructures()];
    Arrays.fill(structsToOptimize, true);
    this.init(ce.getAllGroups(true), structsToOptimize);
    this.setActiveGroups(new ClusterGroup[0]);
  }*/
  
  public KnownCorrelationEvaluator(ClusterExpansion ce, boolean[] structsToOptimize) {
    super(ce);
    m_Weights = new double[ce.getStructureList().numKnownStructures()];
    Arrays.fill(m_Weights, 1);
    this.init(ce.getAllGroups(true), structsToOptimize);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public KnownCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, double[] weights) {
    super(ce, fitter);
    boolean[] structsToOptimize = new boolean[ce.getStructureList().numKnownStructures()];
    Arrays.fill(structsToOptimize, true);
    m_Weights = ArrayUtils.copyArray(weights);
    this.init(ce.getAllGroups(true), structsToOptimize);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public KnownCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, boolean[] structsToOptimize, double[] weights) {
    super(ce, fitter);
    m_Weights = ArrayUtils.copyArray(weights);
    this.init(ce.getAllGroups(true), structsToOptimize);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  public KnownCorrelationEvaluator(ClusterExpansion ce, AbstractL2RLSFitter fitter, ClusterGroup[] candidateGroups, boolean[] structsToOptimize, double[] weights) {
    super(ce, fitter);
    this.init(candidateGroups, structsToOptimize);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  protected void init(ClusterGroup[] candidateGroups, boolean[] structsToOptimize) {
    
    double[] weights = this.getSetWeights(m_Weights, structsToOptimize);
    double totalWeight = 0;
    for (int weightNum = 0; weightNum < weights.length; weightNum++) {
      totalWeight += weights[weightNum];
    }
    
    double[][] correlations = m_ClusterExpansion.getStructureList().getCorrelations(candidateGroups, structsToOptimize);
    DoubleMatrix2D x = DoubleFactory2D.dense.make(correlations);
    for (int rowNum = 0; rowNum < x.rows(); rowNum++) {
      double scaleFactor = weights[rowNum] / totalWeight;
      double scaleRoot = Math.sqrt(scaleFactor);
      for (int colNum = 0; colNum < x.columns(); colNum++) {
        double value = x.getQuick(rowNum, colNum);
        value *= scaleRoot;
        x.setQuick(rowNum, colNum, value);
      }
    }
    m_FullExpectationMatrix = x.zMult(x, null, 1, 0, true, false);
    
    int maxGroupNumber = -1;
    for (int groupIndex = 0; groupIndex < candidateGroups.length; groupIndex++) {
      ClusterGroup group = candidateGroups[groupIndex];
      maxGroupNumber = Math.max(maxGroupNumber, group.getGroupNumber());
    }
    m_VarNumMap = new int[maxGroupNumber+1][];
    int varNum = 0;
    for (int groupIndex = 0; groupIndex < candidateGroups.length; groupIndex++) {
      ClusterGroup group = candidateGroups[groupIndex];
      m_VarNumMap[group.getGroupNumber()] = new int[group.numFunctionGroups()];
      for (int functGroupNum= 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        m_VarNumMap[group.getGroupNumber()][functGroupNum] = varNum++;
      }
    }
  }
  
  protected void refreshFitter() {
    super.refreshFitter();
    this.refreshActiveExpectations();
  }
  
  protected void refreshActiveExpectations() {
    
    int numActiveVars = 0;
    for (int groupIndex =0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      numActiveVars += m_ActiveGroups[groupIndex].numFunctionGroups();
    }
    
    //int numActiveVars = m_Fitter.numVariables();
    int[] activeVars = new int[numActiveVars];
    int activeIndex = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      int[] varNums = m_VarNumMap[group.getGroupNumber()];
      if (varNums == null) {
        throw new RuntimeException("Trying to activate cluster that is not allowed: " + group.getGroupNumber());
      }
      for (int varIndex = 0; varIndex < varNums.length; varIndex++) {
        activeVars[activeIndex++] = varNums[varIndex];
      }
    }
    
    double[][] activeExpectations = m_FullExpectationMatrix.viewSelection(activeVars, activeVars).toArray();
    m_InputDistribution = new GeneralDomainMatrix(activeExpectations);
  }

}
