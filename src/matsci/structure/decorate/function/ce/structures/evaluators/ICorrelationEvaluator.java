/*
 * Created on May 7, 2006
 *
 */
package matsci.structure.decorate.function.ce.structures.evaluators;

import matsci.structure.decorate.function.ce.clusters.*;

public interface ICorrelationEvaluator {

  //public double refreshKnownCorrelations(ClusterGroup[] activeGroups);
  public double evaluateNewCorrelations(double[][] correlations, double weight);
  public double evaluateKnownCorrelations(ClusterGroup[] activeGroups);
  
}
