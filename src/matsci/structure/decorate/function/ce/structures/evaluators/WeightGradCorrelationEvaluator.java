/*
 * Created on Aug 27, 2007
 *
 */
package matsci.structure.decorate.function.ce.structures.evaluators;

import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.model.linear.*;

public class WeightGradCorrelationEvaluator implements ICorrelationEvaluator {

  private DomainWeightEvaluator.State m_WeightEvaluatorState;
  
  public WeightGradCorrelationEvaluator(DomainWeightEvaluator.State weighterState) {
    m_WeightEvaluatorState = weighterState;
  }

  public double evaluateNewCorrelations(double[][] correlations, double weight) {
    
    int numCorrelations = 0;
    for (int groupNum = 0; groupNum < correlations.length; groupNum++) {
      if (correlations[groupNum] == null || correlations[groupNum].length == 0) {continue;}
      numCorrelations += correlations[groupNum].length;
    }
    
    double[] flatArray = new double[numCorrelations];
    int correlationNum = 0;
    for (int groupNum = 0; groupNum < correlations.length; groupNum++) {
      if (correlations[groupNum] == null) {continue;}
      for (int functNum = 0; functNum < correlations[groupNum].length; functNum++) {
        flatArray[correlationNum++] = correlations[groupNum][functNum]; 
      }
    }
    
    //return m_WeightEvaluatorState.getDirect1stDerivativeForNewInput(flatArray);
    return m_WeightEvaluatorState.estimateQuadMinimumForNewInput(flatArray);
    //return m_WeightEvaluatorState.getNegativeOptimalWeight(flatArray);
  }

  public double evaluateKnownCorrelations(ClusterGroup[] activeGroups) {
    throw new RuntimeException("Not implemented yet");
    //return 0;
  }

}
