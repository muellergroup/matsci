/*
 * Created on May 27, 2006
 *
 */
package matsci.structure.decorate.function.ce.structures.evaluators;

import java.util.*;
import matsci.model.linear.dist.*;
import matsci.model.linear.L2DiagRLSFitter;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;

public class TraceCorrelationEvaluator extends AbstractCorrelationEvaluator {

  public TraceCorrelationEvaluator(ClusterExpansion ce, L2DiagRLSFitter fitter) {
    super(ce, fitter);
    this.setActiveGroups(new ClusterGroup[0]);
  }
  
  protected void refreshFitter() {
    super.refreshFitter();
    double[] diag = new double[m_Fitter.numVariables()];
    Arrays.fill(diag, 1);
    m_InputDistribution = new DiagonalDomainMatrix(diag);
  }
  
}
