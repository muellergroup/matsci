/*
 * Created on Jun 8, 2006
 *
 */
package matsci.structure.decorate.function.ce.structures.evaluators;

import java.util.Arrays;

import matsci.model.linear.L2DiagRLSFitter;
import matsci.model.linear.dist.DiagonalDomainMatrix;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.*;

public class MultiplicityCorrelationEvaluator extends AbstractCorrelationEvaluator {

  public MultiplicityCorrelationEvaluator(ClusterExpansion ce, L2DiagRLSFitter fitter) {
    super(ce, fitter);
  }
  
  protected void refreshFitter() {
    super.refreshFitter();
    
    double[] invMultiplicities = new double[m_Fitter.numVariables()];
    int totalFunctNum = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      for (int functGroup = 0; functGroup < group.numFunctionGroups(); functGroup++) {
        int multiplicity = group.getMultiplicity();
        multiplicity /= group.getFunctionsForFunctionGroup(functGroup).length;
        invMultiplicities[totalFunctNum++] = 1.0/multiplicity;
      }
    }
    
    m_InputDistribution = new DiagonalDomainMatrix(invMultiplicities);
  }

}
