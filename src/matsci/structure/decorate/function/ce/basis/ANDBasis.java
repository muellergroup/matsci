/*
 * Created on Dec 2, 2005
 *
 */
package matsci.structure.decorate.function.ce.basis;

public class ANDBasis extends CEBasis {

  public ANDBasis() {
    super();
  }

  public double getFunctionValue(int siteState, int functionNumber, int numAllowedStates) {
    return siteState == 0 ? 0 : 1;
  }

  public boolean isOrthogonal() {
    return false;
  }

}
