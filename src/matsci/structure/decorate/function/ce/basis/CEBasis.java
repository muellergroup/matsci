/*
 * Created on Mar 18, 2005
 *
 */
package matsci.structure.decorate.function.ce.basis;


/**
 * @author Tim Mueller
 *
 */
public abstract class CEBasis {

  public CEBasis() {}
  
  /**
   * This method represents the basis of the cluster expansion.
   * 
   * It is implemented using a default orthogonal basis.  In many ways this makes the analysis
   * of the cluster expansion easier.  If you want to use a different basis, extend this class
   * and override this method.
   * TODO Implement in a general manner and explain here.
   * 
   * @param siteState An integer that uniquely maps to the occupancy of a site.  This is not the 
   * same as "occupation number", which is the evaluation of a site function.  The site state
   * is defined in more detail in the DecorationTemplate documentation.
   * @param functionNumber An integer that represents which function we want to evaluate for the site.  The 
   * index of the function numbers should start at 0, and the function that is always "1" should not be 
   * included.
   * @param numAllowedStates The total number of allowed states at this site.  This determines the
   * set of functions that will be used.
   * @return The evaluation of the selected function when applied to the given site state.
   */
  public abstract double getFunctionValue(int siteState, int functionNumber, int numAllowedStates);

  /**
   * 
   * @return true if this basis is orthognal over all of configuration space.  Orthogonality is defined 
   * as the sum over all configurations of two functions are equal to the number of total configurations if the
   * functions are the same, or zero otherwise.
   */
  public abstract boolean isOrthogonal();

}
