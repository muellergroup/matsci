/*
 * Created on Mar 19, 2005
 *
 */
package matsci.structure.decorate.function.ce.basis;

import matsci.structure.decorate.function.ce.clusters.ClusterGroup;


/**
 * @author Tim Mueller
 *
 */
public class CECosineBasis extends CEBasis {

  private double m_Normalizer = 1 * Math.sqrt(2);
  
  /**
   * @param ce
   */
  public CECosineBasis() {
    super();
  }

  /* 
   * This is the implementation of the discrete cosine transform.
   * 
   * (non-Javadoc)
   * @see matsci.structure.decorate.function.ce.basis.CEBasis#calculateFunctionValue(int, int, int)
   */
  public double getFunctionValue(int siteState, int functionNumber, int numAllowedStates) {
    
    // I think we need to make this explicit -- otherwise, this gets confusing.
    if (functionNumber == numAllowedStates - 1) {return 1;}
    
    // For a long time (until May 2014) I just had this.
    return m_Normalizer * Math.cos(Math.PI * (functionNumber+1) * ((2*siteState) + 1) / (2.0*numAllowedStates));
  }

  /* (non-Javadoc)
   * @see matsci.structure.decorate.function.ce.basis.CEBasis#isOrthogonal()
   */
  public boolean isOrthogonal() {
    return true;
  }
  
  public static double[][] getBinaryTransformationMatrix(ClusterGroup[] allGroups, double concentrationA, boolean scaleByMultiplicity) {
    
    double[][] returnArray = new double[allGroups.length][allGroups.length];
    double tanhu = 2*concentrationA - 1;
    double coshu = Math.sqrt(1 / (1 - tanhu * tanhu));
    
    for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
      ClusterGroup rowGroup = allGroups[rowNum];
      for (int colNum = 0; colNum < returnArray.length; colNum++) {
        ClusterGroup colGroup = allGroups[colNum];
        if (rowGroup == colGroup) {
          int numSites = rowGroup.numSitesPerCluster();
          returnArray[rowNum][colNum] = Math.pow(coshu, numSites);
        } else if (rowGroup.isSubGroupOf(colGroup)) {
          int numRowSites = rowGroup.numSitesPerCluster();
          int numColSites = colGroup.numSitesPerCluster();
          returnArray[rowNum][colNum] = Math.pow(coshu, numColSites) * Math.pow(-tanhu, numColSites - numRowSites);
        }
        if (scaleByMultiplicity) {
          returnArray[rowNum][colNum] *= ((double) colGroup.getMultiplicity()) / rowGroup.getMultiplicity();
        }
      }
    }
    
    return returnArray;
    
  }
  

}
