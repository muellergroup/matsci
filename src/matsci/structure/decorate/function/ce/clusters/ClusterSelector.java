/*
 * Created on Apr 7, 2006
 *
 */
package matsci.structure.decorate.function.ce.clusters;

import java.util.Arrays;
import java.util.Random;

import matsci.engine.monte.metropolis.*;
import matsci.model.ConstrainedVariableSelector;
import matsci.model.linear.*;
import matsci.model.IFitter;
import matsci.util.arrays.ArrayUtils;
import matsci.structure.decorate.function.ce.*;
import matsci.structure.decorate.function.ce.structures.*;

/**
 * This is a wrapper class around a variable selector that makes it useful for cluster expansions
 * 
 * @author Tim Mueller
 *
 */
public class ClusterSelector implements IAllowsMetropolis {

  private static Random m_Generator = new Random();
  
  private final ConstrainedVariableSelector m_VarSelector;
  
  private final ClusterGroup[] m_AllowedGroups;
  private final int[] m_VarStartsByGroupIndex;
  private final int[] m_GroupIndicesByVarNum;
  private final int[] m_GroupIndicesByGroupNum;
  
  private boolean m_SubGroupConstraint;
  
  protected ClusterSelector(ClusterSelector sourceSelector) {
    m_VarSelector = (ConstrainedVariableSelector) sourceSelector.m_VarSelector.copy();
    m_AllowedGroups = sourceSelector.m_AllowedGroups;
    m_VarStartsByGroupIndex = sourceSelector.m_VarStartsByGroupIndex;
    m_GroupIndicesByVarNum = sourceSelector.m_GroupIndicesByVarNum;
    m_GroupIndicesByGroupNum = sourceSelector.m_GroupIndicesByGroupNum;
  }

  public ClusterSelector(ClusterExpansion ce) {
    this(ce.getStructureList(), ce.getAllGroups(true), new LeastSquaresFitter(), getAllIncluded(ce.getStructureList().numKnownStructures()));
  }
  
  public ClusterSelector(ClusterExpansion ce, IFitter fitter) {
    this(ce.getStructureList(), ce.getAllGroups(true), fitter, getAllIncluded(ce.getStructureList().numKnownStructures()));
  }
  
  protected static boolean[] getAllIncluded(int numStructures) {
    boolean[] returnArray = new boolean[numStructures];
    Arrays.fill(returnArray, true);
    return returnArray;
  }
  

  public ClusterSelector(StructureList structureList, ClusterGroup[] allowedGroups, IFitter fitter) {
    this(structureList, allowedGroups, fitter, getAllIncluded(structureList.numKnownStructures()));
  }
  
  public ClusterSelector(StructureList structureList, ClusterGroup[] allowedGroups, IFitter fitter, boolean[] allowedStructures) {
    
    // Initialize the main arrays
    m_AllowedGroups = (ClusterGroup[]) ArrayUtils.copyArray(allowedGroups);
    m_VarStartsByGroupIndex = new int[allowedGroups.length];
    int totalFunctNum = 0;
    int maxGroupNumber = 0;
    for (int groupIndex = 0; groupIndex < allowedGroups.length; groupIndex++) {
      m_VarStartsByGroupIndex[groupIndex] = totalFunctNum;
      totalFunctNum += allowedGroups[groupIndex].numFunctionGroups();
      maxGroupNumber = Math.max(maxGroupNumber, allowedGroups[groupIndex].getGroupNumber());
    }
    m_GroupIndicesByVarNum = new int[totalFunctNum];
    m_GroupIndicesByGroupNum = new int[maxGroupNumber + 1];
    Arrays.fill(m_GroupIndicesByGroupNum, -1);
    totalFunctNum = 0;
    for (int groupIndex = 0; groupIndex < allowedGroups.length; groupIndex++) {
      ClusterGroup group = allowedGroups[groupIndex];
      for (int functNum = 0; functNum < group.numFunctionGroups(); functNum++) {
        m_GroupIndicesByVarNum[totalFunctNum++] = groupIndex;
        //System.out.println("Group number " + group.getGroupNumber() + ", Var index: " + m_VarStartsByGroupIndex[groupIndex]);
      }
      m_GroupIndicesByGroupNum[group.getGroupNumber()] = groupIndex;
    }
    
    // Build the variable selector
    double[][] correlations = structureList.getCorrelations(allowedGroups, allowedStructures);
    double[] values = structureList.getValues(allowedStructures);
    m_VarSelector = new ConstrainedVariableSelector(fitter, correlations, values);
    this.setSubGroupConstraint(false);
  }
  
  public ClusterSelector(ClusterExpansion ce, IFitter fitter, boolean[] allowedStructures) {
    this(ce.getStructureList(), ce.getAllGroups(true), fitter, allowedStructures);
  }
  
  public ClusterSelector copy() {
    return new ClusterSelector(this);
  }
  
  /**
   * This constrains that all function groups in a given cluster must be either 
   * activated or deactivated at the same time.
   * @return
   */
  private int[][] getIndividualClusterConstraints() {
    
    int numVariables = m_GroupIndicesByVarNum.length;
    int[][] returnArray = new int[numVariables][];
    int returnIndex = 0;
    for (int groupIndex = 0; groupIndex < m_AllowedGroups.length; groupIndex++) {
      ClusterGroup group = m_AllowedGroups[groupIndex];
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        returnArray[returnIndex] = new int[group.numFunctionGroups() - 1];
        int subFunctIndex =0;
        for (int subFunctNum = 0; subFunctNum < group.numFunctionGroups(); subFunctNum++) {
          if (subFunctNum == functGroupNum) {continue;}
          returnArray[returnIndex][subFunctIndex++] = subFunctNum;
        }
        returnIndex++;
      }
    }
    return returnArray;
  }
  
  private int[][] getSubGroupConstraints() {
    
    int[][] returnArray = this.getIndividualClusterConstraints();
    int returnIndex = 0;
    for (int groupIndex = 0; groupIndex < m_AllowedGroups.length; groupIndex++) {
      ClusterGroup group = m_AllowedGroups[groupIndex];
      int[] subGroupIndices =new int[0];
      for (int subGroupCount = 0; subGroupCount < group.numSubGroups(); subGroupCount++) {
        ClusterGroup subGroup = group.getSubGroup(subGroupCount);
        if (!this.isGroupAllowed(subGroup)) {continue;}
        int groupIndexForSubGroup = m_GroupIndicesByGroupNum[subGroup.getGroupNumber()];
        int[] indicesForSubGroup = new int[subGroup.numFunctionGroups()];
        for (int functNum = 0; functNum < indicesForSubGroup.length; functNum++) {
          indicesForSubGroup[functNum] = m_VarStartsByGroupIndex[groupIndexForSubGroup] + functNum;
        }
        subGroupIndices = ArrayUtils.appendArray(subGroupIndices, indicesForSubGroup);
      }
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        returnArray[returnIndex] = ArrayUtils.appendArray(returnArray[returnIndex], subGroupIndices);
        returnIndex++;
      }
    }
    return returnArray;
  }
  
  public int numAllowedGroups() {
    return m_AllowedGroups.length;
  }
  
  public boolean isGroupAllowed(ClusterGroup group) {
    int groupNumber = group.getGroupNumber();
    if (groupNumber >= m_GroupIndicesByGroupNum.length) {return false;}
    return (m_GroupIndicesByGroupNum[groupNumber] >= 0);
  }

  public double getValue() {
    return m_VarSelector.getValue();
  }

  public IMetropolisEvent getRandomEvent() {
    
    int varIndex = -1;
    do {
      int groupToChange = m_Generator.nextInt(m_AllowedGroups.length);
      varIndex = m_VarStartsByGroupIndex[groupToChange];
    } while (m_VarSelector.isVariableForced(varIndex));
    //System.out.println(varIndex);
    return m_VarSelector.getToggleEvent(varIndex);
  }
  
  public IMetropolisEvent getGreedyActivationEvent() {
    return m_VarSelector.getGreedyActivationEvent();
  }
  
  public IMetropolisEvent getGreedyDeactivationEvent() {
    return m_VarSelector.getGreedyDeactivationEvent();
  }

  public void setState(Object snapshot) {
    m_VarSelector.deactivateAllVariables();
    ClusterGroup[] groupsToActivate = (ClusterGroup[]) snapshot;
    this.activateGroups(groupsToActivate);
  }
  
  public void activateGroups(ClusterGroup[] groupsToActivate) {
    for (int groupIndex = 0; groupIndex < groupsToActivate.length; groupIndex++) {
      this.activateGroup(groupsToActivate[groupIndex]);
    }
  }
  
  public void deactivateGroups(ClusterGroup[] groupsToDeactivate) {
    for (int groupIndex = 0; groupIndex < groupsToDeactivate.length; groupIndex++) {
      this.deactivateGroup(groupsToDeactivate[groupIndex]);
    }
  }
  
  /**
   * TODO -- Make variable selector handle setting groups of variables at once
   * @param groups
   */
  public void setActiveGroups(ClusterGroup[] groups) {
    
    boolean[] newState = new boolean[m_AllowedGroups.length];
    for (int givenIndex = 0; givenIndex < groups.length; givenIndex++) {
      int allowedIndex = m_GroupIndicesByGroupNum[groups[givenIndex].getGroupNumber()];
      newState[allowedIndex] = true;
    }
    
    for (int stateIndex = 0; stateIndex < newState.length; stateIndex++) {
      int varIndex = m_VarStartsByGroupIndex[stateIndex];
      boolean currentState = m_VarSelector.isVariableActive(varIndex);
      if (newState[stateIndex] && !currentState) {
        this.activateGroup(m_AllowedGroups[stateIndex]);
      } else if (!newState[stateIndex] && currentState) {
        this.deactivateGroup(m_AllowedGroups[stateIndex]);
      }
    }
  }
  
  public void forceGroups(ClusterGroup[] forcedGroups) {
    int numFunctionGroups = 0;
    for (int forcedGroupIndex = 0; forcedGroupIndex < forcedGroups.length; forcedGroupIndex++) {
      numFunctionGroups += forcedGroups[forcedGroupIndex].numFunctionGroups();
    }
    int[] allVariables =new int[numFunctionGroups];
    int totalVarNum = 0;
    for (int forcedGroupIndex = 0; forcedGroupIndex < forcedGroups.length; forcedGroupIndex++) {
      ClusterGroup forcedGroup = forcedGroups[forcedGroupIndex];
      if (!this.isGroupAllowed(forcedGroup)) {
        throw new RuntimeException("You cannot force the cluster selector to use cluster orbit that is not allowed!  Cluster orbit: " + forcedGroup.getGroupNumber());
      }
      int groupIndex = m_GroupIndicesByGroupNum[forcedGroup.getGroupNumber()];
      int varStart = m_VarStartsByGroupIndex[groupIndex];
      for (int functGroupNum = 0; functGroupNum < forcedGroup.numFunctionGroups(); functGroupNum++) {
        allVariables[totalVarNum++] = varStart + functGroupNum;
      }
    }
    m_VarSelector.forceVariables(allVariables);
  }
  
  public boolean activateGroup(ClusterGroup group) {
    if (!this.isGroupAllowed(group)) {return false;}
    int groupIndex =m_GroupIndicesByGroupNum[group.getGroupNumber()];
    int varIndex = m_VarStartsByGroupIndex[groupIndex];
    return m_VarSelector.activateVariable(varIndex);
  }
  
  public boolean deactivateGroup(ClusterGroup group) {
    if (!this.isGroupAllowed(group)) {return false;}
    int groupIndex =m_GroupIndicesByGroupNum[group.getGroupNumber()];
    int varIndex = m_VarStartsByGroupIndex[groupIndex];
    return m_VarSelector.deactivateVariable(varIndex);
  }
  
  public boolean isGroupActive(ClusterGroup group) {
    if (!this.isGroupAllowed(group)) {return false;}
    int groupIndex =m_GroupIndicesByGroupNum[group.getGroupNumber()];
    int varIndex = m_VarStartsByGroupIndex[groupIndex];
    return m_VarSelector.isVariableActive(varIndex);
  }
  
  public int numActiveGroups() {
    int returnValue = 0;
    for (int groupIndex =0; groupIndex < m_AllowedGroups.length; groupIndex++) {
      if (m_VarSelector.isVariableActive(m_VarStartsByGroupIndex[groupIndex])) {
        returnValue++;
      }
    }
    return returnValue;
  }
  
  public ClusterGroup[] getActiveGroups() {
    ClusterGroup[] returnArray = new ClusterGroup[this.numActiveGroups()];
    int returnIndex = 0;
    for (int groupIndex =0; groupIndex < m_AllowedGroups.length; groupIndex++) {
      if (m_VarSelector.isVariableActive(m_VarStartsByGroupIndex[groupIndex])) {
        returnArray[returnIndex++] = m_AllowedGroups[groupIndex];
      }
    }
    return returnArray;
  }

  public Object getSnapshot(Object template) {
    return this.getActiveGroups();
  }
  
  public void setSubGroupConstraint(boolean constraint) {
    if (constraint) {
      m_VarSelector.setConstraints(getSubGroupConstraints());
    } else {
      m_VarSelector.setConstraints(getIndividualClusterConstraints());
    }
    m_SubGroupConstraint = constraint;
  }
  
  public boolean getSubGroupConstraint() {
    return m_SubGroupConstraint;
  }
  
  public IFitter getFitter() {
    return m_VarSelector.getFitter();
  }

}
