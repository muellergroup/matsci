/*
 * Created on Aug 9, 2008
 *
 */
package matsci.structure.decorate.function.ce.clusters;

import cern.jet.stat.*;

import java.util.*;

import matsci.engine.IContinuousFunctionState;
import matsci.structure.decorate.function.ce.structures.*;

import matsci.util.arrays.*;

public class HullWeightEvaluator {

  private StructureList m_StructureList;
  private double m_VarianceScale; // The weight is related to the variance by w = vs / sigi^2
  private int[][] m_HullVertices;
  private double[][] m_VertexFractions;
  private double[] m_HullDistances;
  
  public HullWeightEvaluator(StructureList list, double varianceScale) {
    m_StructureList = list;
    m_VarianceScale = varianceScale;
    m_HullVertices = new int[list.numKnownStructures()][];
    m_VertexFractions = new double[list.numKnownStructures()][];
    m_HullDistances = new double[list.numKnownStructures()];
    Arrays.fill(m_HullDistances, -1);
    for (int structNum = 0; structNum < m_HullVertices.length; structNum++) {
      if (!list.isKnownValue(structNum)) {continue;} // Only deal with known structures
      m_HullVertices[structNum] = list.getHullVerticesForStructure(structNum);
      m_HullDistances[structNum] = list.getHullDistance(structNum);
      m_VertexFractions[structNum] = list.getHullVertexFractions(structNum);
    }
  }
  
  public double getProbabilityOfMaintainingHull(double[] weights) {
    
    double[] variances = new double[weights.length];
    
    double weightSum = 0;
    for (int structNum = 0; structNum < weights.length; structNum++) {
      weightSum += weights[structNum];
    }
    
    double scalingRatio = weights.length / weightSum;
    
    for (int structNum = 0; structNum < weights.length; structNum++) {
      variances[structNum] = m_VarianceScale / (weights[structNum] * scalingRatio);
    }
    
    double probability = 1;
    for (int structNum = 0; structNum < m_StructureList.numKnownStructures(); structNum++) {
      if (!m_StructureList.isKnownValue(structNum)) {continue;}
      if (m_StructureList.isOnHull(structNum)) {continue;} // These are indirectly accounted for
      double distanceFromHull = m_StructureList.getHullDistance(structNum);
      int[] vertices = m_HullVertices[structNum];
      double[] vertexFractions = m_VertexFractions[structNum];
      double hullVariance = 0;
      for (int vertexNum = 0; vertexNum < vertices.length; vertexNum++) {
        double vertexVariance = variances[vertices[vertexNum]];
        hullVariance += vertexVariance * vertexFractions[vertexNum];
      }
      double totalVariance = hullVariance + variances[structNum];
      probability *= (0.5)*(1 + Probability.errorFunction(distanceFromHull / Math.sqrt(totalVariance * 2)));
    }
    
    return probability;
    
  }
  
  public State getStateForWeights(double[] weights) {
    return new State(weights);
  }
  
  public class State implements IContinuousFunctionState {
    
    protected double[] m_CurrentWeights;
    protected double[] m_LogWeights;
    protected double m_WeightSum = 0;
    
    protected State(double[] weights) {
      m_CurrentWeights = ArrayUtils.copyArray(weights);
      m_LogWeights = new double[weights.length];
      for (int structNum = 0; structNum < weights.length; structNum++) {
        double weight = weights[structNum];
        m_LogWeights[structNum] = Math.log(weight);
        m_WeightSum += weight;
      }
    }
    
    protected State(double[] weights, double[] logWeights) {
      m_CurrentWeights = ArrayUtils.copyArray(weights);
      m_LogWeights = ArrayUtils.copyArray(logWeights);
      for (int structNum = 0; structNum < weights.length; structNum++) {
        m_WeightSum += weights[structNum];
      }
    }

    public int numParameters() {
      return m_CurrentWeights.length;
    }

    public double[] getUnboundedParameters(double[] template) {
      if (template == null || template.length != m_CurrentWeights.length) {
        template = new double[m_CurrentWeights.length];
      }
      System.arraycopy(m_LogWeights, 0, template, 0, template.length);
      return template;
    }

    /**
     * The gradient is with respect to the log weights
     */
    public double[] getGradient(double[] template) {
      
      if ((template == null) || (template.length != m_CurrentWeights.length)) {
        template = new double[m_CurrentWeights.length];
      } else {
        Arrays.fill(template, 0);
      }
      
      double[] ddw = new double[template.length];
      double[] normalizedWeights = new double[template.length];
      
      double varianceScale = m_VarianceScale / template.length;

      double[] variances = new double[m_CurrentWeights.length];
      for (int structNum = 0; structNum < variances.length; structNum++) {
        double weight = m_CurrentWeights[structNum];
        double scaledWeight = weight / m_WeightSum;
        variances[structNum] = varianceScale / scaledWeight;
        normalizedWeights[structNum] = scaledWeight;
      }
      
      for (int structNum = 0; structNum < m_StructureList.numKnownStructures(); structNum++) {
        if (!m_StructureList.isKnownValue(structNum)) {continue;}
        if (m_StructureList.isOnHull(structNum)) {continue;} // These are accounted for by the off-hull structures
          
        double distanceFromHull = m_StructureList.getHullDistance(structNum);
        int[] vertices = m_HullVertices[structNum];
        double[] vertexFractions = m_VertexFractions[structNum];
        double hullVariance = 0;
        for (int vertexNum = 0; vertexNum < vertices.length; vertexNum++) {
          double vertexVariance = variances[vertices[vertexNum]];
          hullVariance += vertexVariance * vertexFractions[vertexNum];
        }
        double selfVariance = variances[structNum];
        double totalVariance = hullVariance + selfVariance;
        double probability = (0.5)*(1 + Probability.errorFunction(distanceFromHull / Math.sqrt(totalVariance * 2)));
        
        double derivative = (0.5) * Math.pow(totalVariance, -1.5) * distanceFromHull / (varianceScale * Math.sqrt(2 * Math.PI));
        derivative *= Math.exp(-distanceFromHull * distanceFromHull / (2 * totalVariance));
        
        ddw[structNum] -= selfVariance * selfVariance * derivative / probability; // Because it's the derivative of the negative log
        for (int vertexNum = 0; vertexNum < vertices.length; vertexNum++) {
          int vertexStructNum = vertices[vertexNum];
          double vertexVariance = variances[vertexStructNum];
          double vertexFraction = vertexFractions[vertexNum];
          ddw[vertexStructNum] -= vertexFraction * vertexVariance * vertexVariance * derivative / probability; // Because it's the derivative of the negative log
        }
      }
      
      // Normalize for weights
      for (int structNum = 0; structNum < variances.length; structNum++) {
        double weight = normalizedWeights[structNum];
        template[structNum] += ddw[structNum] * weight;
        for (int ddwNum = 0; ddwNum < ddw.length; ddwNum++) {
          template[structNum] -= ddw[ddwNum] * weight * normalizedWeights[ddwNum];
        }
        //template[structNum] *= template.length; // Reset to the full length
      }
      
      //double[] manTemplate = getSlowGradient(null);
      return template;
      
    }
    
    protected double[] getSlowGradient(double[] template) {
      if ((template == null) || (template.length != m_CurrentWeights.length)) {
        template = new double[m_CurrentWeights.length];
      }
      double delta = 1E-6;
      
      double[] plusIncrement = new double[template.length];
      double[] minusIncrement = new double[template.length];
      
      double plusFactor = Math.exp(delta);
      double minusFactor = Math.exp(-delta);
      
      for (int structNum = 0; structNum < m_CurrentWeights.length; structNum++) {
        System.arraycopy(m_CurrentWeights, 0, plusIncrement, 0, m_CurrentWeights.length);
        plusIncrement[structNum] *= plusFactor;
        System.arraycopy(m_CurrentWeights, 0, minusIncrement, 0, m_CurrentWeights.length);
        minusIncrement[structNum] *= minusFactor;
        double plusValue = -Math.log(getProbabilityOfMaintainingHull(plusIncrement));
        double minusValue = -Math.log(getProbabilityOfMaintainingHull(minusIncrement));
        template[structNum] = (plusValue - minusValue) / (2 * delta);
      }
      
      return template;
    }

    public IContinuousFunctionState setUnboundedParameters(double[] logWeights) {
      double[] weights = new double[logWeights.length];
      for (int structNum = 0; structNum < weights.length; structNum++) {
        weights[structNum] = Math.exp(logWeights[structNum]);
      }
      return new State(weights, logWeights);
    }

    public double getValue() {
      return -Math.log(getProbabilityOfMaintainingHull(m_CurrentWeights));
    }
    
    public double[] getWeights() {
      return ArrayUtils.copyArray(m_CurrentWeights);
    }
    
  }

}
