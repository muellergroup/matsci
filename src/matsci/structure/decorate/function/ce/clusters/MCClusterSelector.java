/*
 * Created on Mar 11, 2005
 *
 */
package matsci.structure.decorate.function.ce.clusters;

import java.util.Arrays;
import java.util.Random;

import matsci.engine.monte.metropolis.IAllowsMetropolis;
import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 * TODO meld this with the general variable selector
 */
public abstract class MCClusterSelector implements IAllowsMetropolis {

  protected ClusterExpansion m_ClusterExpansion;
  protected ClusterGroup[] m_AllowedGroups;
  protected boolean[] m_IsGroupAllowed; // Indexed by groupNumber
  
  // This is how we keep track of the active groups
  protected int m_NumActiveGroups;
  protected int m_NumActiveFunctionGroups;
  protected int[][] m_ActiveGroupIndices; // Also need to include functions
  
  private boolean m_SubGroupConstraint;
  
  private Random m_Generator;  
  protected Event m_Event;
  
  public MCClusterSelector(ClusterExpansion clusterExpansion, ClusterGroup[] allowedGroups) {
    
    m_ClusterExpansion = clusterExpansion;

    // Initialize groupNumber lookup for the allowed groups
    m_AllowedGroups = allowedGroups;
    m_IsGroupAllowed = new boolean[clusterExpansion.getMaxGroupNumber() + 1];
    for (int groupIndex = 0; groupIndex < allowedGroups.length; groupIndex++) {
      ClusterGroup group = allowedGroups[groupIndex];
      if (group.getClusterExpansion() != clusterExpansion) {
        throw new RuntimeException("Given group (" + group.getGroupNumber() + "( does not belong to given cluster expansion");
      }
      m_IsGroupAllowed[group.getGroupNumber()] =true;
    }
    
    // Initialize no active groups.
    m_ActiveGroupIndices = new int[clusterExpansion.getMaxGroupNumber() + 1][];
    for (int groupNumber = 0; groupNumber < m_ActiveGroupIndices.length; groupNumber++) {
      ClusterGroup group = clusterExpansion.getClusterGroup(groupNumber);
      if (group == null) {continue;}
      int[] functionGroupIndices = new int[group.numFunctionGroups()];
      Arrays.fill(functionGroupIndices, -1);
      m_ActiveGroupIndices[groupNumber] = functionGroupIndices;
    }

    m_NumActiveGroups = 0;
    m_NumActiveFunctionGroups = 0;
    
    // For randomly generating events
    m_Generator = new Random();
    m_Event = new Event(this);
  }
  
  public ClusterExpansion getClusterExpansion() {
    return m_ClusterExpansion;
  }
  
  public void setSubGroupConstraint(boolean value) {
    m_SubGroupConstraint = value;
    
    if (value) {
      ClusterGroup[] activeGroups = this.getSortedActiveGroups();
      this.deactivateAllGroups();
      this.activateGroups(activeGroups); // This reactivates the groups with the subgroup constraint enforced
    }
  }
  
  public boolean getSubGroupConstraint() {
    return m_SubGroupConstraint;
  }
  
  public ClusterGroup[] activateGroups(ClusterGroup[] groups) {
    
    int numGroupsActivated = 0;
    boolean[] activatedGroups = new boolean[m_IsGroupAllowed.length];
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      numGroupsActivated += this.countGroupsToActivate(groups[groupIndex], activatedGroups);
    }
    return this.activateGroups(activatedGroups, numGroupsActivated);
  }
  
  public ClusterGroup[] activateGroup(ClusterGroup group) {
    boolean[] activatedGroups = new boolean[m_IsGroupAllowed.length];
    int numActivatedGroups =this.countGroupsToActivate(group, activatedGroups);
    return this.activateGroups(activatedGroups, numActivatedGroups);
  }
  
  protected ClusterGroup[] activateGroups(boolean[] groupsToActivate, int numGroupsToActivate) {
    
    ClusterGroup[] returnArray = new ClusterGroup[numGroupsToActivate];
    int groupIndex = 0;
    for (int groupNum = 0; groupNum < groupsToActivate.length; groupNum++) {
      if (groupsToActivate[groupNum]) {
        for (int functionGroup = 0; functionGroup < m_ActiveGroupIndices[groupNum].length; functionGroup++) {
          m_ActiveGroupIndices[groupNum][functionGroup] = m_NumActiveFunctionGroups++;
        }
        returnArray[groupIndex++] = m_ClusterExpansion.getClusterGroup(groupNum);
        m_NumActiveGroups++;
      }
    }
    return returnArray;
    
  }

  protected int countGroupsToActivate(ClusterGroup group, boolean[] activatedGroups) {
    
    int groupNumber = group.getGroupNumber();
    if (!m_IsGroupAllowed[groupNumber]) {return 0;}
    if ((m_ActiveGroupIndices[groupNumber][0] >= 0) || activatedGroups[groupNumber]) {return 0;}
    
    int numActivatedGroups = 1;
    activatedGroups[groupNumber] = true;
    
    if (m_SubGroupConstraint) {
      int numSubGroups = group.numSubGroups();
      for (int subGroupIndex = 0; subGroupIndex < numSubGroups; subGroupIndex++) {
        numActivatedGroups += this.countGroupsToActivate(group.getSubGroup(subGroupIndex), activatedGroups);
      }
    }

    return numActivatedGroups;
  }
  
  public ClusterGroup[] deactivateGroups(ClusterGroup[] groups) {
    int numGroupsDeactivated = 0;
    boolean[] deactivatedGroups = new boolean[m_IsGroupAllowed.length];
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      numGroupsDeactivated += this.countGroupsToDeactivate(groups[groupIndex], deactivatedGroups);
    }
    return this.deactivateGroups(deactivatedGroups, numGroupsDeactivated);
  }
  
  protected ClusterGroup[] deactivateGroup(ClusterGroup group) {
    boolean[] deactivatedGroups = new boolean[m_IsGroupAllowed.length];
    int numGroupToDeactivate = this.countGroupsToDeactivate(group, deactivatedGroups);
    return this.deactivateGroups(deactivatedGroups, numGroupToDeactivate);
  }
  
  /**
   * Assumes subgroup constraint has already been enforced
   * 
   * @param deactivatedGroups
   */
  protected ClusterGroup[] deactivateGroups(boolean[] deactivatedGroups, int numGroupsToDeactivate) {
    
    ClusterGroup[] returnArray = new ClusterGroup[numGroupsToDeactivate];
    
    // Sort the deactivated groups by index and deactivate the groups as we do
    int[] sortedActiveGroupNums = new int[m_NumActiveFunctionGroups];
    int nextGroupIndex = 0;
    int returnArrayIndex =0;
    for (int groupNum = 0; groupNum < m_ActiveGroupIndices.length; groupNum++) {
      if (m_ActiveGroupIndices[groupNum] == null) {continue;}
      if (m_ActiveGroupIndices[groupNum][0] < 0) {continue;}
      for (int functGroup = 0; functGroup < m_ActiveGroupIndices[groupNum].length; functGroup++) {
        sortedActiveGroupNums[m_ActiveGroupIndices[groupNum][functGroup]] = groupNum;
      }
      if (deactivatedGroups[groupNum]) {
        Arrays.fill(m_ActiveGroupIndices[groupNum], -1);
        returnArray[returnArrayIndex++] = m_ClusterExpansion.getClusterGroup(groupNum);
      }
    }
    
    // Now deactivate them and shift the remaining indices
    int numDeactivatedFunctionGroups = 0;
    int lastGroupNum = -1;
    for (int groupIndex = 0; groupIndex < sortedActiveGroupNums.length; groupIndex++) {
      int groupNum = sortedActiveGroupNums[groupIndex];
      if (lastGroupNum == groupNum) {continue;}
      if (deactivatedGroups[groupNum]) {
        numDeactivatedFunctionGroups += m_ActiveGroupIndices[groupNum].length;
      } else {
        if (numDeactivatedFunctionGroups == 0) {continue;}
        for (int functGroup = 0; functGroup < m_ActiveGroupIndices[groupNum].length; functGroup++) {
          m_ActiveGroupIndices[groupNum][functGroup] -= numDeactivatedFunctionGroups;
        }
      }
      lastGroupNum = groupNum;
    }
    
    m_NumActiveGroups -= numGroupsToDeactivate;
    m_NumActiveFunctionGroups -= numDeactivatedFunctionGroups;
    return returnArray;
    
  }
  
  protected int countGroupsToDeactivate(ClusterGroup group, boolean[] deactivatedGroups) {
    
    int groupNumber = group.getGroupNumber();
    if (m_ActiveGroupIndices[groupNumber] == null) {return 0;}
    if (m_ActiveGroupIndices[groupNumber][0] < 0 || deactivatedGroups[groupNumber]) {return 0;}
    
    int numDeactivatedGroups = 1;
    deactivatedGroups[groupNumber] = true;
    
    if (m_SubGroupConstraint) {
      int numSuperGroups = group.numSuperGroups();
      for (int superGroupIndex = 0; superGroupIndex < numSuperGroups; superGroupIndex++) {
        numDeactivatedGroups += this.countGroupsToDeactivate(group.getSuperGroup(superGroupIndex), deactivatedGroups);
      }
    }
    
    return numDeactivatedGroups;
  }
  
  public void deactivateAllGroups() {
    for (int groupNum = 0; groupNum < m_ActiveGroupIndices.length; groupNum++) {
      if (m_ActiveGroupIndices[groupNum] != null) {
        Arrays.fill(m_ActiveGroupIndices[groupNum], -1);
      }
    }

    m_NumActiveGroups = 0;
    m_NumActiveFunctionGroups = 0;
  }
  
  public void activateAllAllowedGroups() {
    this.activateGroups(m_AllowedGroups);
  }
  
  public boolean isGroupActive(ClusterGroup group) {
    if (m_ActiveGroupIndices[group.getGroupNumber()] == null) {return false;}
    return m_ActiveGroupIndices[group.getGroupNumber()][0] >= 0;
  }
  
  public boolean isGroupAllowed(ClusterGroup group) {
    return m_IsGroupAllowed[group.getGroupNumber()];
  }

  public int numActiveGroups() {
    return m_NumActiveGroups;
  }
  
  public int numAllowedGroups() {
	  return m_AllowedGroups.length;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getRandomEvent()
   */
  public IMetropolisEvent getRandomEvent() {
    ClusterGroup toggleGroup = m_AllowedGroups[m_Generator.nextInt(m_AllowedGroups.length)];
    return this.getToggleEvent(toggleGroup);
  }
  
  public IMetropolisEvent getToggleEvent(ClusterGroup toggleGroup) {
    m_Event.m_Activate = !this.isGroupActive(toggleGroup);
    m_Event.m_GroupToChange = toggleGroup;
    return m_Event;
  }
  
  public boolean areAllSubGroupsActivated(ClusterGroup group) {
    for (int subGroupNum = 0; subGroupNum < group.numSubGroups(); subGroupNum++) {
      ClusterGroup subGroup = group.getSubGroup(subGroupNum);
      if (!this.isGroupAllowed(subGroup)) {continue;}
      if (!this.isGroupActive(subGroup)) {return false;}
    }
    return true;
  }
  
  public boolean areAllSuperGroupsDectivated(ClusterGroup group) {
    for (int superGroupNum = 0; superGroupNum < group.numSuperGroups(); superGroupNum++) {
      ClusterGroup superGroup = group.getSuperGroup(superGroupNum);
      if (!this.isGroupAllowed(superGroup)) {continue;}
      if (this.isGroupActive(superGroup)) {return false;}
    }
    return true;
  }


  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#setInitialState(java.lang.Object)
   */
  public void setState(Object snapshot) {
    this.deactivateAllGroups();
    ClusterGroup[] groupsToActivate = (ClusterGroup[]) snapshot;
    for (int groupIndex = 0; groupIndex < groupsToActivate.length; groupIndex++) {
      this.activateGroups(groupsToActivate);
    } 
  }

  protected int[] getActiveIndices(ClusterGroup group) {
    return ArrayUtils.copyArray(m_ActiveGroupIndices[group.getGroupNumber()]);
  }
  
  public ClusterGroup[] getSortedActiveGroups() {
    ClusterGroup[] returnArray = new ClusterGroup[m_NumActiveGroups];
    
    int returnIndex =0;
    for (int groupNum = 0; groupNum < m_ActiveGroupIndices.length; groupNum++) {
      ClusterGroup group = m_ClusterExpansion.getClusterGroup(groupNum);
      if (group == null) {continue;}
      if (!this.isGroupActive(group)) {continue;}
      returnArray[returnIndex++] = m_ClusterExpansion.getClusterGroup(groupNum);
    }
    
    return returnArray;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#getSnapshot(java.lang.Object)
   */
  public Object getSnapshot(Object template) {
    return this.getSortedActiveGroups();
  }
  
  protected class Event implements IMetropolisEvent {

    private boolean m_Activate;
    private ClusterGroup m_GroupToChange;
    protected MCClusterSelector m_ClustSelector;
    private int[][] m_OldActiveIndices;
    private int m_OldNumActiveGroups;
    private int m_OldNumActiveFunctionGroups;
    
    public Event(MCClusterSelector clustSelector) {
      m_ClustSelector = clustSelector;
      m_OldActiveIndices =new int[clustSelector.m_ActiveGroupIndices.length][];
      for (int groupNum = 0; groupNum < m_OldActiveIndices.length; groupNum++) {
        if (clustSelector.m_ActiveGroupIndices[groupNum] == null) {continue;}
        m_OldActiveIndices[groupNum] = new int[clustSelector.m_ActiveGroupIndices[groupNum].length];
      }
    } 
    
    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#trigger()
     */
    public void trigger() {
      
      int[][] activeGroupIndices =m_ClustSelector.m_ActiveGroupIndices;
      for (int groupNum = 0; groupNum < m_OldActiveIndices.length; groupNum++) {
        if (activeGroupIndices[groupNum] == null) {continue;}
        System.arraycopy(activeGroupIndices[groupNum], 0, m_OldActiveIndices[groupNum], 0, activeGroupIndices[groupNum].length);
      }
      m_OldNumActiveGroups = m_ClustSelector.m_NumActiveGroups;
      m_OldNumActiveFunctionGroups = m_ClustSelector.m_NumActiveFunctionGroups;
      
      if (m_Activate) {
        m_ClustSelector.activateGroup(m_GroupToChange);
      } else {
        m_ClustSelector.deactivateGroup(m_GroupToChange);
      }
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#getDelta()
     */
    public double getDelta() {
      
      double oldValue = m_ClustSelector.getValue();
      this.trigger();
      double delta = m_ClustSelector.getValue() - oldValue;
      this.reverse();
      return delta;
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#triggerGetDelta()
     */
    public double triggerGetDelta() {
      double oldValue = m_ClustSelector.getValue();
      this.trigger();
      return m_ClustSelector.getValue() - oldValue;
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#reverse()
     */
    public void reverse() {
      int[][] activeGroupIndices =m_ClustSelector.m_ActiveGroupIndices;
      for (int groupNum = 0; groupNum < m_OldActiveIndices.length; groupNum++) {
        if (m_OldActiveIndices[groupNum] == null) {continue;}
        System.arraycopy(m_OldActiveIndices[groupNum], 0, activeGroupIndices[groupNum], 0, activeGroupIndices[groupNum].length);
      }
      m_ClustSelector.m_NumActiveGroups = m_OldNumActiveGroups;
      m_ClustSelector.m_NumActiveFunctionGroups = m_OldNumActiveFunctionGroups;
    }
  }
}
