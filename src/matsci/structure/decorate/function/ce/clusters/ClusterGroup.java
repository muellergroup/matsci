package matsci.structure.decorate.function.ce.clusters;

import java.util.Arrays;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.*;
import matsci.structure.*;
import matsci.structure.superstructure.*;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.function.ce.clusters.ClusterGroupMapper;
import matsci.structure.function.ce.clusters.IClusterGroup;
import matsci.util.arrays.ArrayIndexer;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>A cluster group represents a set of symmetrically equivalent clusters for a cluster expansion.
 * Some useful terms for dealing with a cluster group include:</p>
 * <br><b>Cluster state</b> is an integer representing the current occupancies of one of the clusters in this group.
 * This number is generated from a linear combination of the site states in the cluster.  Cluster states range from
 * 0 to numClusterStates - 1.
 * <br><b>State coefficients</b> are the coefficients for the linear transformation from a set of site states to a
 * cluster state.  Note that this requires that the coordinates for the sites in each cluster are ordered in the 
 * same way.  To get a sample of this order, use the getSampleCoords() function.
 * <br><b>Cluster Function Number</b> An integer representing a cluster function.  Each cluster function number
 * corresponds to a set of site function numbers.  The cluster function is defined as the product of those site 
 * functions.  Cluster function numbers range from 0 to numFunctions() -1.
 * <p>Copyright: Copyright (c) 2003</p>
 * @author Tim Mueller
 * @version 1.0
 * TODO Incorporate the concept of symmetrically equivalent functions ("function groups")
 */

public class ClusterGroup implements IClusterGroup {

  private final int m_GroupNum;
  private final ClusterExpansion m_ClusterExpansion;
  private Coordinates[][] m_PrimClusterCoords = new Coordinates[0][];

  private int[] m_PrimSiteIndices;

  private ArrayIndexer m_StateConverter = new ArrayIndexer(new int[0]);
  private ArrayIndexer m_FunctionConverter = new ArrayIndexer(new int[0]);
  private int[] m_FunctionGroupNumbers = new int[0]; // This is the group number listed by function number
  private double[] m_ECI; // This is an array because each group can have multiple functions for higher-order expansions
  
  private ClusterGroup[] m_SubGroups = new ClusterGroup[0];
  private ClusterGroup[] m_SuperGroups = new ClusterGroup[0];
  
  private boolean[] m_UseManualLUT;
  private double[] m_ManualLUT;
  
  private double[][] m_FunctionGroupValues;

  /**
   * This constructor is used when the coordinates for all the cluster in this group are already known.
   * For example, when reading the coordinates from an FCLUST file.
   * 
   * @param groupNum A unique non-negative integer associated with this group
   * @param primClusterCoords Representative clusters for each cluster in this group.  These coordinates
   * can be thought of as the coordinates for the clusters corresponding the the primitive cell for 
   * this cluster expansion.  By translating these coordinates by the periodicity of the primitive cell,
   * we can generate all clusters in this group.  The sites in each cluster do not need to be ordered; they
   * will be sorted internally.
   * @param clusterExpansion The cluster expansion for which this group is defined.
   */
  public ClusterGroup(int groupNum, Coordinates[][] primClusterCoords, ClusterExpansion clusterExpansion) {
    m_GroupNum = groupNum;
    m_ClusterExpansion = clusterExpansion;
    
    for (int clustNum = 0; clustNum < primClusterCoords.length; clustNum++) {
      this.addClusterCoords(primClusterCoords[clustNum]);
    }
    
    this.initializeClusterInfo();
    
    ClusterGroupMapper clusterMapper = m_ClusterExpansion.getClusterMapper();
    for (int prevGroupNum = 0; prevGroupNum <= clusterExpansion.getMaxGroupNumber(); prevGroupNum++) {
      ClusterGroup prevGroup = clusterExpansion.getClusterGroup(prevGroupNum);
      if (prevGroup == null) {continue;}
      if (clusterMapper.areSuperSubGroups(prevGroup, this, false)) {
        prevGroup.addSubGroup(this);
        this.addSuperGroup(prevGroup);
      } else if (clusterMapper.areSuperSubGroups(this, prevGroup, false)) {
        this.addSubGroup(prevGroup);
        prevGroup.addSuperGroup(this);
      }
    }
  }
  
  public Species[] getAllowedSpecies(int clustSiteNum) {
    int primSiteIndex = m_PrimSiteIndices[clustSiteNum];
    return m_ClusterExpansion.getAllowedSpecies(primSiteIndex);
  }
  
  public Species getAllowedSpecies(int clustSiteNum, int siteState) {
    int primSiteIndex = m_PrimSiteIndices[clustSiteNum];
    return m_ClusterExpansion.getAllowedSpecies(primSiteIndex, siteState);
  }
  
  public int getPrimSiteIndex(int clustSiteNum) {
    return m_PrimSiteIndices[clustSiteNum];
  }
  
  public int getSiteState(int clustSiteNum, Species species) {
    int primSiteIndex = this.getPrimSiteIndex(clustSiteNum);
    return m_ClusterExpansion.getSiteState(primSiteIndex, species);
  }

  /**
   *
   * @return A lookup table that matches a given cluster state to the cluster value
   */
  public double[] getLUT() {

    double[] lut = new double[m_StateConverter.numAllowedStates()];
    for (int clusterState = 0; clusterState < lut.length; clusterState++) {
      if (m_UseManualLUT[clusterState]) {
        lut[clusterState] = m_ManualLUT[clusterState];
      } else {
        // TODO consider just calling getValue(clusterState);
        double totalValue = 0;
        for (int functionGroup = 0; functionGroup < this.numFunctionGroups(); functionGroup++) {
          double ECI = this.getECI(functionGroup);
          totalValue += (ECI * this.getFunctionGroupValue(clusterState, functionGroup));
        }
        lut[clusterState] = totalValue;
      }
    }
    return lut;
  }
  
  public double getValue(int clusterState) {
    double totalValue = 0;
    for (int functionGroup = 0; functionGroup < this.numFunctionGroups(); functionGroup++) {
      double ECI = this.getECI(functionGroup);
      totalValue += (ECI * this.getFunctionGroupValue(clusterState, functionGroup));
    }
    return totalValue;
  }
  
  public double getValue(int[] siteStates) {
    return this.getValue(this.getClusterState(siteStates));
  }
  
  public void setLUTValue(int[] siteStates, double value) {
    
    Coordinates[] sampleCoords = this.getSampleCoords();
    if (sampleCoords== null) {
      m_ManualLUT[0] = value;
      m_UseManualLUT[0] = true;
      return;
    }
    
    int[][] allMaps = m_ClusterExpansion.getSpaceGroup().findSymmetryMaps(sampleCoords, sampleCoords, false);
    int[] mappedStates = new int[siteStates.length];
    for (int mapNum = 0; mapNum < allMaps.length; mapNum++) {
      int[] map = allMaps[mapNum];
      for (int siteNum = 0; siteNum < mappedStates.length; siteNum++) {
        mappedStates[siteNum] = siteStates[map[siteNum]];
      }
      int clusterState = this.getClusterState(mappedStates);
      m_ManualLUT[clusterState] = value;
      m_UseManualLUT[clusterState] = true;
    }
  }
  

  public void incrementLUTValue(int[] siteStates, double increment) {
    
    double oldValue = this.getValue(siteStates);
    this.setLUTValue(siteStates, oldValue + increment);
    
  }
  
  
  public void setLUTValue(int clustState, double value) {
    this.setLUTValue(this.getSiteStates(clustState), value);
  }
  
  public int[] getOverlapStates(ClusterGroup subGroup, int[] superSiteStates, boolean returnOne) {
    
    int[] returnArray = new int[0];
    if (subGroup.numSitesPerCluster() >= this.numSitesPerCluster()) {
      return returnArray;
    }
    
    Coordinates[] sampleCoords = this.getSampleCoords();
    Coordinates[] sampleSubCoords = subGroup.getSampleCoords();
    int[][] allMaps = m_ClusterExpansion.getSpaceGroup().findSymmetryMaps(sampleCoords, sampleSubCoords, returnOne);
    int[] mappedStates = new int[sampleSubCoords.length];
    
    for (int mapIndex = 0; mapIndex < allMaps.length; mapIndex++) {
      int[] map = allMaps[mapIndex];
      for (int siteIndex =0; siteIndex < map.length; siteIndex++) {
        mappedStates[siteIndex] = superSiteStates[map[siteIndex]];
        int clustState = subGroup.getClusterState(mappedStates);
        if (!ArrayUtils.arrayContains(returnArray, clustState)) {
          returnArray = ArrayUtils.appendElement(returnArray, clustState);
        }
      }
    }
    
    return returnArray;
  }
  
  public int[] getEquivalentStates(int clustState) {
    int[] siteStates = this.getSiteStates(clustState);
    Coordinates[] sampleCoords = this.getSampleCoords();
    int[] returnArray = new int[this.numClusterStates()];
    Arrays.fill(returnArray, -1);
    int returnIndex = 0;
    
    int[][] allMaps = m_ClusterExpansion.getSpaceGroup().findSymmetryMaps(sampleCoords, sampleCoords, false);
    int[] mappedStates = new int[siteStates.length];
    for (int mapNum = 0; mapNum < allMaps.length; mapNum++) {
      int[] map = allMaps[mapNum];
      for (int siteNum = 0; siteNum < mappedStates.length; siteNum++) {
        mappedStates[siteNum] = siteStates[map[siteNum]];
      }
      int symState = this.getClusterState(mappedStates);
      if (ArrayUtils.arrayContains(returnArray, symState)) {continue;}
      returnArray[returnIndex++] = this.getClusterState(mappedStates);
    }
    returnArray = ArrayUtils.truncateArray(returnArray, returnIndex);
    return returnArray;
  }
  
  public void clearManualLUTValue(int[] siteStates) {
    Coordinates[] sampleCoords = this.getSampleCoords();
    if (sampleCoords== null) {
      m_UseManualLUT[0] = false;
      return;
    }
    
    int[][] allMaps = m_ClusterExpansion.getSpaceGroup().findSymmetryMaps(sampleCoords, sampleCoords, false);
    int[] mappedStates = new int[siteStates.length];
    for (int mapNum = 0; mapNum < allMaps.length; mapNum++) {
      int[] map = allMaps[mapNum];
      for (int siteNum = 0; siteNum < mappedStates.length; siteNum++) {
        mappedStates[siteNum] = siteStates[map[siteNum]];
      }
      int clusterState = this.getClusterState(mappedStates);
      m_UseManualLUT[clusterState] = false;
    }
  }

  /**
   * Evaluates a cluster function given a set of occupancies.  The actual basis used to evaluate the cluster 
   * functions is 
   * 
   * @param siteStates Integers representing the occupancies for the sites of a cluster
   * in this group, listed in the order corresponding to the ordering of sites in this group.
   * @param functionNum An integer representing the cluster function whose value we are calculating.
   * @return The value of the cluster function for the given site occupancies.
   */
  public double getValue(int[] siteStates, int functionNum) {

    int[] siteFunctions = this.getSiteFunctions(functionNum);
    double clusterValue = 1;
    for (int siteNum = 0; siteNum < siteFunctions.length; siteNum++) {
      clusterValue *= m_ClusterExpansion.getFunctionValue(m_PrimSiteIndices[siteNum], siteStates[siteNum], siteFunctions[siteNum]);
    }
    return clusterValue;
  }

  /**
   * 
   * @param clusterState An integer representing the occupancies of the sites of a cluster.  Each set
   * of site states maps onto a unique clusterState.  
   * @param functionNum An integer representing the cluster function whose value we are calculating.
   * @return The value for the given function for the given cluster state.
   */
  public double getValue(int clusterState, int functionNum) {
    return getValue(this.getSiteStates(clusterState), functionNum);
  }
  
  public double getFunctionGroupValue(int[] siteStates, int functionGroup) {
    double returnValue = 0; 
    for (int functNum = 0; functNum < m_FunctionGroupNumbers.length; functNum++) {
      if (m_FunctionGroupNumbers[functNum] == functionGroup) {
        returnValue += this.getValue(siteStates, functNum);
      }
    }
    return returnValue;
  }
  
  public double getFunctionGroupValue(int clusterState, int functionGroup) {
    if (m_FunctionGroupValues == null) {
      this.calcFunctionGroupValues();
    }
    return m_FunctionGroupValues[clusterState][functionGroup];
    //return getFunctionGroupValue(this.getSiteStates(clusterState), functionGroup);
  }
  
  private void calcFunctionGroupValues() {
    
    m_FunctionGroupValues = new double[this.numClusterStates()][this.numFunctionGroups()];
    for (int clusterState = 0; clusterState < m_FunctionGroupValues.length; clusterState++) {
      for (int functionGroup = 0; functionGroup < m_FunctionGroupValues[clusterState].length; functionGroup++) {
        m_FunctionGroupValues[clusterState][functionGroup] = getFunctionGroupValue(this.getSiteStates(clusterState), functionGroup);
      }
    }
    
  }

  /**
   * This method calculates the cluster state for a given set of site states.
   * 
   * @param siteStates Represents the occupancies of the sites in a cluster belonging to this group.  The 
   * order of the site states should correspond to the order of the sites as returned by getSampleCoords().
   * @return
   */
  public int getClusterState(int[] siteStates) {
    return m_StateConverter.joinValuesBounded(siteStates);
  }

  /**
   * Given a cluster state, calculates the site states for the sites that make up the cluster.
   * 
   * @param clusterState A cluster state representing the occupancies for the sites in the cluster.
   * @return An array of site state, representing the occupancies of a the sites for a cluster in this group.
   * The order of the returned site states corresponds to the order of sites returend by getSampleCoords().
   */
  public int[] getSiteStates(int clusterState) {
    return m_StateConverter.splitValue(clusterState);
  }

  /**
   * A cluster state is a linear transformation of site states.  This returns the coefficient for a given 
   * site state used when making the transformation.  This is useful for determining how a change in site state
   * affects the cluster state.
   * 
   * @param siteNum The index for the site for which we are finding the coefficient.  The order of the sites
   * is the same as the order given by the method getSampleCoords();
   * @return
   */
  public int getStateCoefficient(int siteNum) {
    return m_StateConverter.getCoefficient(siteNum);
  }

  /**
   * This method convertes between the cluster function number and the function numbers for the sites that 
   * make up a cluster in this group.  As usual, the order of the returned site function numbers is consistent
   * with the ordering of sites for all the other methods in this class.
   * 
   * @param clustFunctionNum A cluster function number
   * @return The site function numbers corresponding to the cluster function number.
   */
  public int[] getSiteFunctions(int clustFunctionNum) {
    return m_FunctionConverter.splitValue(clustFunctionNum);
  }
  
  public int getClustFunctionNum(int[] siteFunctions) {
    return m_FunctionConverter.joinValues(siteFunctions);
  }
  
  public int[][] getSiteFunctionsForFunctionGroup(int functGroupNum) {
    int[] functs = this.getFunctionsForFunctionGroup(functGroupNum);
    int[][] returnArray = new int[functs.length][];
    for (int functNum = 0; functNum < returnArray.length; functNum++) {
      returnArray[functNum] = this.getSiteFunctions(functs[functNum]);
    }
    return returnArray;
  }
  
  private void addClusterCoords(Coordinates[] coords) {

    // Convert the coordinates to integer coordinates
    //DiscreteBasis integerBasis = m_ClusterExpansion.getPrimStructure().getIntegerBasis();
    //coords = integerBasis.getCoordinatesFrom(coords);
    
    AbstractBasis latticeBasis = m_ClusterExpansion.getBaseStructure().getDefiningLattice().getLatticeBasis();
    coords = latticeBasis.getCoordinatesFrom(coords);
    
    int newIndex = m_PrimClusterCoords.length;
    
    if (newIndex == 0) {
      /*
      int numSites =  coords.length;
      m_PrimSiteIndices = new int[numSites];
      int[] stateBases = new int[numSites];
      int[] functionBases = new int[numSites];

      for (int siteNum = 0; siteNum < numSites; siteNum++) {
        int primIndex = m_ClusterExpansion.getPrimStructure().getDefiningSite(coords[siteNum]).getIndex();
        m_PrimSiteIndices[siteNum] = primIndex;
        Specie[] allowedSpecies = m_ClusterExpansion.getAllowedSpecies(primIndex);
        stateBases[siteNum] = allowedSpecies.length;
        functionBases[siteNum] = allowedSpecies.length - 1;
      }

      m_StateConverter = new ArrayIndexer(stateBases);
      m_FunctionConverter = new ArrayIndexer(functionBases);
      m_ECI = new double[m_FunctionConverter.numAllowedStates()];
      */
    } else {
      
      // We just need to make sure the coordinates are properly ordered
      int[][] coordMaps = m_ClusterExpansion.getSpaceGroup().findSymmetryMaps(m_PrimClusterCoords[0], coords, true);
      if (coordMaps.length == 0) {
        throw new RuntimeException("Cluster coordinates do not belong in this group");
      }
      int[] map = coordMaps[0];
      Coordinates[] orderedCoords = new Coordinates[coords.length];
      for (int coordNum = 0; coordNum < coords.length; coordNum++) {
        orderedCoords[coordNum] = coords[map[coordNum]];
      }
      coords = orderedCoords;
    }

    // Grow the primClusterCoords array
    m_PrimClusterCoords = (Coordinates[][]) ArrayUtils.appendElement(m_PrimClusterCoords, coords);
  }
  
  /**
   * Called in the constructor, this uses sample coordinates to learn information about this type of cluster.
   *
   */
  private void initializeClusterInfo() {
    
    Coordinates[] sampleCoords = this.getSampleCoords();
    if (sampleCoords == null) {return;}
    
    int numSites =  sampleCoords.length;
    m_PrimSiteIndices = new int[numSites];
    int[] stateBases = new int[numSites];
    int[] functionBases = new int[numSites];

    for (int siteNum = 0; siteNum < numSites; siteNum++) {
      int primIndex = m_ClusterExpansion.getBaseStructure().getDefiningSite(sampleCoords[siteNum]).getIndex();
      m_PrimSiteIndices[siteNum] = primIndex;
      Species[] allowedSpecies = m_ClusterExpansion.getAllowedSpecies(primIndex);
      stateBases[siteNum] = allowedSpecies.length;
      functionBases[siteNum] = allowedSpecies.length - 1;
    }

    m_StateConverter = new ArrayIndexer(stateBases);
    m_FunctionConverter = new ArrayIndexer(functionBases);
    
    m_UseManualLUT = new boolean[m_StateConverter.numAllowedStates()];
    m_ManualLUT = new double[m_UseManualLUT.length];
    
    // Now we try to group symmetrically equivalent functions
    m_FunctionGroupNumbers = new int[m_FunctionConverter.numAllowedStates()];
    int[][] allMaps = m_ClusterExpansion.getSpaceGroup().findSymmetryMaps(sampleCoords, sampleCoords, false);
    int[] mappedSiteFuncts = new int[sampleCoords.length];
    int numFunctionGroups = 0;
    for (int functNum = 0; functNum < m_FunctionConverter.numAllowedStates(); functNum++) {
      int[] siteFunctNums = m_FunctionConverter.splitValue(functNum);
      boolean newFunction =true;
      for (int mapNum = 0; mapNum < allMaps.length; mapNum++) {
        int[] map = allMaps[mapNum];
        for (int siteNum = 0; siteNum < mappedSiteFuncts.length; siteNum++) {
          mappedSiteFuncts[siteNum] = siteFunctNums[map[siteNum]];
        }
        int mappedFunctNum = m_FunctionConverter.joinValuesBounded(mappedSiteFuncts);
        if (mappedFunctNum < functNum) {
          m_FunctionGroupNumbers[functNum] = m_FunctionGroupNumbers[mappedFunctNum];
          newFunction = false;
          break;
        }
      }
      if (newFunction) {
        m_FunctionGroupNumbers[functNum] = numFunctionGroups++;
      }
    }
    
    m_ECI = new double[numFunctionGroups];
  }

  public int numSitesPerCluster() {
    return m_PrimSiteIndices.length;
  }

  public int getGroupNumber() {
    return m_GroupNum;
  }

  public ClusterExpansion getClusterExpansion() {
    return m_ClusterExpansion;
  }

  public Coordinates[] getPrimClusterCoords(int index) {
    return (Coordinates[]) ArrayUtils.copyArray(m_PrimClusterCoords[index]);
  }
  
  public Coordinates getAveragePrimClusterCoords(int index) {
    
    if (m_PrimClusterCoords[index].length == 0) {
      return null;
    }
    
    double[] cartArray = new double[m_PrimClusterCoords[index][0].numCartesianCoords()];
    for (int siteNum = 0; siteNum < m_PrimClusterCoords[index].length; siteNum++) {
      Coordinates siteCoords = m_PrimClusterCoords[index][siteNum];
      double[] siteCartArray = siteCoords.getCartesianArray();
      for (int dimNum = 0; dimNum < siteCartArray.length; dimNum++) {
        cartArray[dimNum] += siteCartArray[dimNum];
      }
    }
    
    for (int dimNum = 0; dimNum < cartArray.length; dimNum++) {
      cartArray[dimNum] /= m_PrimClusterCoords[0].length;
    }
    
    return new Coordinates(cartArray, CartesianBasis.getInstance());
  }

  public int numPrimClusters() {
    return m_PrimClusterCoords.length;
  }

  /*
   * Do you really want this, or do you want numFunctionGroups?
  public int numFunctions() {
    return m_FunctionConverter.numAllowedStates();
  }
  */
  
  public int numFunctionGroups() {
    return m_ECI.length;
  }

  public int numClusterStates() {
    return m_StateConverter.numAllowedStates();
  }
  
  public int[] getFunctionsForFunctionGroup(int functGroupNum) {
    int[] returnArray = new int[0];
    for (int functNum = 0; functNum < m_FunctionGroupNumbers.length; functNum++) {
      if (m_FunctionGroupNumbers[functNum] == functGroupNum) {
        returnArray = ArrayUtils.appendElement(returnArray, functNum);
      }
    }
    return returnArray;
  }
  
  public int numFunctions() {
    return m_FunctionGroupNumbers.length;
  }
  
  public int numFunctionsForFunctionGroup(int functGroupNum) {
    int returnValue = 0;
    for (int functNum = 0; functNum < m_FunctionGroupNumbers.length; functNum++) {
      if (m_FunctionGroupNumbers[functNum] == functGroupNum) {
        returnValue++;
      }
    }
    return returnValue;
  }
  
  
  public int getFunctionGroupForFunction(int functionNum) {
    return m_FunctionGroupNumbers[functionNum];
  }

  public void setECI(int functionGroup, double eci) {
    m_ECI[functionGroup] = eci;
  }
  
  public void setCorrelationCoefficient(int functionGroup, double cCoefficient) {
    m_ECI[functionGroup] = cCoefficient / this.getMultiplicity();
  }

  public double getECI(int functionGroup) {
    return m_ECI[functionGroup];
  }
  
  public double getCorrelationCoefficient(int functionGroup) {
    return m_ECI[functionGroup] * this.getMultiplicity();
  }

  public int getMultiplicity() {
    return m_PrimClusterCoords.length;
  }

  public Coordinates[] getSampleCoords() {
    if (m_PrimClusterCoords.length == 0) {return null;}
    return (Coordinates[]) ArrayUtils.copyArray(m_PrimClusterCoords[0]);
  }

  public int numSubGroups() {
    return m_SubGroups.length;
  }

  public ClusterGroup getSubGroup(int index) {
    return m_SubGroups[index];
  }

  public int numSuperGroups() {
    return m_SuperGroups.length;
  }

  public ClusterGroup getSuperGroup(int index) {
    return m_SuperGroups[index];
  }

  /**
   * @param groups Groups whose clusters are subclusters of the 
   * clusters in this group.  This group should not be its own subgroup.
   * @todo Replace this with something in the constructor that automatically
   * generates the supergroup and subgroup relationships
   */
  public void addSubGroups(ClusterGroup[] groups) {
    m_SubGroups = groups;
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      this.addSubGroup(groups[groupIndex]);
      groups[groupIndex].addSuperGroup(this);
    }
  }
  
  private void addSubGroup(ClusterGroup group) {
    
    if (group == this) {return;}
    
    for (int knownGroupNum = 0; knownGroupNum < m_SubGroups.length; knownGroupNum++) {
      if (m_SubGroups[knownGroupNum] == group) {return;}
    }
    
    m_SubGroups = (ClusterGroup[]) ArrayUtils.appendElement(m_SubGroups, group);
  }

  /**
   * @param group A group whose clusters are superclusters of the clusters in this group.
   * This group is not its own supergroup.
   * @todo Replace this as well.
   */
  private void addSuperGroup(ClusterGroup group) {

    for (int knownGroupNum = 0; knownGroupNum < m_SuperGroups.length; knownGroupNum++) {
      if (m_SuperGroups[knownGroupNum] == group) {return;}
    }
    
    m_SuperGroups = (ClusterGroup[]) ArrayUtils.appendElement(m_SuperGroups, group);
  }
  
  public boolean isSuperGroupOf(ClusterGroup group) {
    
    for (int subGroupNum = 0; subGroupNum < m_SubGroups.length; subGroupNum++) {
      if (m_SubGroups[subGroupNum] == group) {return true;}
    }
    return false;
  }
  
  public boolean isSubGroupOf(ClusterGroup group) {
    
    for (int superGroupNum = 0; superGroupNum < m_SuperGroups.length; superGroupNum++) {
      if (m_SuperGroups[superGroupNum] == group) {return true;}
    }
    return false;
  }
  
  public double getMaxDistance() {
    
    if (m_PrimClusterCoords.length == 0) {return Double.NaN;}
    Coordinates[] sampleCoords = m_PrimClusterCoords[0];
    double distance = 0;
    for (int siteNum = 0; siteNum < sampleCoords.length; siteNum++) {
      for (int prevSiteNum = 0; prevSiteNum < siteNum; prevSiteNum++) {
        distance = Math.max(distance, sampleCoords[siteNum].distanceFrom(sampleCoords[prevSiteNum]));
      }
    }
    return distance;
  }
  
  public IStructureData getSampleStructure(Species notCluster, Species cluster) {
    
    Coordinates[] sampleCoords = this.getSampleCoords();
    Structure primStructure = m_ClusterExpansion.getBaseStructure();
    
    double[] minDirectVectors = new double[primStructure.numPeriodicDimensions()];
    int[] maxDirectVectors = new int[minDirectVectors.length];
    int[] periodicIndices = primStructure.getDefiningLattice().getPeriodicIndices();
    for (int coordNum = 0; coordNum < sampleCoords.length; coordNum++) {
      double[] directArray = sampleCoords[coordNum].getCoordArray(primStructure.getDirectBasis());
      for (int dimNum = 0; dimNum < periodicIndices.length; dimNum++) {
        int periodicDim = periodicIndices[dimNum];
        int floor = (int) Math.floor(directArray[periodicDim]);
        int ceil = (int) Math.ceil(directArray[periodicDim]);
        minDirectVectors[dimNum] = Math.min(minDirectVectors[dimNum], floor);
        maxDirectVectors[dimNum] = Math.max(maxDirectVectors[dimNum], ceil);
      }
    }
    
    int[][] superToDirect =new int[periodicIndices.length][periodicIndices.length];
    for (int diagNum= 0; diagNum < superToDirect.length; diagNum++) {
      superToDirect[diagNum][diagNum] = (sampleCoords.length == 0) ? 1 : (int) (2 + (maxDirectVectors[diagNum] - minDirectVectors[diagNum]));
    }
    SuperStructure superStructure = new SuperStructure(primStructure, superToDirect);
    
    for (int siteNum = 0; siteNum < superStructure.numDefiningSites(); siteNum++) {
      SuperStructure.Site site = (SuperStructure.Site) superStructure.getDefiningSite(siteNum);
      if (m_ClusterExpansion.isSigmaSite(site)) {site.setSpecies(notCluster);}
    }
    
    double[] shiftArray = new double[3];
    for (int dimNum = 0; dimNum < periodicIndices.length; dimNum++) {
      shiftArray[periodicIndices[dimNum]] = 1 - minDirectVectors[dimNum];
    }
    Vector translation = new Vector(shiftArray, primStructure.getDirectBasis());
    
    for (int siteNum = 0; siteNum < sampleCoords.length; siteNum++) {
      Coordinates shiftedCoords = sampleCoords[siteNum].translateBy(translation);
      Structure.Site site = superStructure.getDefiningSite(shiftedCoords);
      site.setSpecies(cluster);
    }
    return superStructure;
  }
  
  public String toString() {
    Coordinates[] sampleCoords = this.getSampleCoords();
    if (sampleCoords == null) {return "No defined clusters\n";}
    if (sampleCoords.length == 0) {return "Empty cluster\n";}
    
    String returnString = "Cluster " + this.getGroupNumber() + "\n";
    for (int coordNum = 0; coordNum < sampleCoords.length; coordNum++) {
      returnString = returnString + sampleCoords[coordNum].getCartesianCoords();
      if (coordNum != sampleCoords.length - 1) {returnString = returnString + "\n";}
    }
    return returnString;
  }
}