/*
 * Created on Mar 11, 2005
 *
 */
package matsci.structure.decorate.function.ce.clusters;

import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.model.linear.ILinearFitter;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class FitClusterSelector extends MCClusterSelector {

  protected ILinearFitter m_Fitter;
  protected Event m_FitEvent;
  
  /**
   * @param clusterExpansion
   * @param allowedGroups
   */
  public FitClusterSelector(ClusterExpansion clusterExpansion, ClusterGroup[] allowedGroups, ILinearFitter fitter) {
    super(clusterExpansion, allowedGroups);
    //m_Fitter = clusterExpansion.fitECI(fitter, new ClusterGroup[0], false);
    if (fitter.numSamples() == 0) { // We've been handed a blank fitter
      double[] outputValues = clusterExpansion.getStructureList().getValues(true);
      m_Fitter = (ILinearFitter) fitter.addSamples(new double[outputValues.length][0], outputValues);
    } else { // The fitter has some variables initialized.  Ideally we would check these agains the allowed groups and set the activated group indices accordingly
      m_Fitter = (ILinearFitter) fitter.clearVariables();
    }
    
    if (allowedGroups.length > 0) {
      this.activateGroups(new ClusterGroup[] {allowedGroups[0]});
    }
    m_FitEvent = new Event(this);
    
  }
  
  public FitClusterSelector(ClusterExpansion clusterExpansion, ILinearFitter fitter) {
    this (clusterExpansion, clusterExpansion.getAllGroups(true), fitter);
  }
  
  public ILinearFitter getFitter() {
    return m_Fitter;
  }

  public ClusterGroup[] activateGroup(ClusterGroup groupToActivate) {
    
    ClusterGroup[] returnArray = super.activateGroup(groupToActivate);
    
    if (returnArray.length == 0) {return returnArray;}
    
    double[][] correlations = this.getClusterExpansion().getStructureList().getCorrelations(returnArray, true);
    m_Fitter = (ILinearFitter) m_Fitter.addVariables(correlations);
    
    /*
    System.out.println("Activate " + returnArray.length + ". Tracked: " + m_Fitter.scoreFit() + ". Real: " + m_ClusterExpansion.fitECI(new LeastSquaresFitter(), this.getActiveGroups(), false).scoreFit());
    */
    return returnArray;
  }
  
  public ClusterGroup[] activateGroups(ClusterGroup[] groupToActivate) {
    
    ClusterGroup[] returnArray = super.activateGroups(groupToActivate);
    
    if (returnArray.length == 0) {return returnArray;}
    
    double[][] correlations = this.getClusterExpansion().getStructureList().getCorrelations(returnArray, true);
    m_Fitter = (ILinearFitter) m_Fitter.addVariables(correlations);
    
    /*
    System.out.println("Activate " + returnArray.length + ". Tracked: " + m_Fitter.scoreFit() + ". Real: " + m_ClusterExpansion.fitECI(new LeastSquaresFitter(), this.getActiveGroups(), false).scoreFit());
    */
    return returnArray;
  }
  
  public ClusterGroup[] deactivateGroup(ClusterGroup groupToDeactivate) {
    
    int[][] oldActiveIndices = ArrayUtils.copyArray(this.m_ActiveGroupIndices);
    int oldNumActiveFunctionGroups = this.m_NumActiveFunctionGroups;
    ClusterGroup[] returnArray = super.deactivateGroup(groupToDeactivate);
    
    if (returnArray.length == 0) {return returnArray;}
    
    int[] deactivatedGroupIndices = new int[oldNumActiveFunctionGroups- this.m_NumActiveFunctionGroups];
    int varIndex = 0;
    for (int groupIndex = 0; groupIndex < returnArray.length; groupIndex++) {
      int[] deactivatedIndices = oldActiveIndices[returnArray[groupIndex].getGroupNumber()];
      for (int functGroup = 0; functGroup < deactivatedIndices.length; functGroup++) {
        deactivatedGroupIndices[varIndex++] = deactivatedIndices[functGroup];
      }
    }
    
    m_Fitter = (ILinearFitter) m_Fitter.removeVariables(deactivatedGroupIndices);

    /*
    ClusterGroup[] activeGroups = this.getActiveGroups();
    LeastSquaresFitter realFitter = (LeastSquaresFitter) m_ClusterExpansion.fitECI(new LeastSquaresFitter(), activeGroups, false);
    System.out.println("Deactivate " + returnArray.length + ". Tracked: " + m_Fitter.scoreFit() + ". Real: " + realFitter.scoreFit());
    */
    return returnArray;
  }
  
  public ClusterGroup[] deactivateGroups(ClusterGroup[] groupToDeactivate) {
    
    int[][] oldActiveIndices = ArrayUtils.copyArray(this.m_ActiveGroupIndices);
    int oldNumActiveFunctionGroups = this.m_NumActiveFunctionGroups;
    ClusterGroup[] returnArray = super.deactivateGroups(groupToDeactivate);
    
    if (returnArray.length == 0) {return returnArray;}
    
    int[] deactivatedGroupIndices = new int[oldNumActiveFunctionGroups- this.m_NumActiveFunctionGroups];
    int varIndex = 0;
    for (int groupIndex = 0; groupIndex < returnArray.length; groupIndex++) {
      int[] deactivatedIndices = oldActiveIndices[returnArray[groupIndex].getGroupNumber()];
      for (int functGroup = 0; functGroup < deactivatedIndices.length; functGroup++) {
        deactivatedGroupIndices[varIndex++] = deactivatedIndices[functGroup];
      }
    }
    
    m_Fitter = (ILinearFitter) m_Fitter.removeVariables(deactivatedGroupIndices);

    /*
    ClusterGroup[] activeGroups = this.getActiveGroups();
    LeastSquaresFitter realFitter = (LeastSquaresFitter) m_ClusterExpansion.fitECI(new LeastSquaresFitter(), activeGroups, false);
    System.out.println("Deactivate " + returnArray.length + ". Tracked: " + m_Fitter.scoreFit() + ". Real: " + realFitter.scoreFit());
    */
    return returnArray;
  }
  
  public void deactivateAllGroups() {
    super.deactivateAllGroups();
    m_Fitter = (ILinearFitter) m_Fitter.clearVariables();
  }
  
  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getRandomEvent()
   */
  /*
  public IMetropolisEvent getRandomEvent() {
    m_FitEvent.m_WrappedEvent = super.getRandomEvent();
    return m_FitEvent;
  }*/
  
  public IMetropolisEvent getToggleEvent(ClusterGroup toggleGroup) {
    m_FitEvent.m_WrappedEvent = super.getToggleEvent(toggleGroup);
    return m_FitEvent;
  }
  
  public ClusterGroup greedyActivateGroup() {

    ClusterExpansion ce = this.getClusterExpansion();
    boolean subGroupConstraint = this.getSubGroupConstraint();
    
    ClusterGroup returnGroup = null;
    double minDelta = 0;
    for (int groupNum = 0; groupNum < ce.getMaxGroupNumber(); groupNum++) {
      ClusterGroup group = ce.getClusterGroup(groupNum);
      if (group == null) {continue;}
      if (!this.isGroupAllowed(group)) {continue;}
      if (this.isGroupActive(group)) {continue;}
      if (subGroupConstraint && !this.areAllSubGroupsActivated(group)) {continue;}
      IMetropolisEvent event = this.getToggleEvent(group);
      double delta = event.getDelta();
      if (delta < minDelta) {
        returnGroup = group;
        minDelta = delta;
      }
    }
    return returnGroup;
  }
  
  public ClusterGroup greedyDeactivateGroup() {
    ClusterExpansion ce = this.getClusterExpansion();
    boolean subGroupConstraint = this.getSubGroupConstraint();

    ClusterGroup returnGroup = null;
    double minDelta = 0;
    for (int groupNum = 0; groupNum < ce.getMaxGroupNumber(); groupNum++) {
      ClusterGroup group = ce.getClusterGroup(groupNum);
      if (group == null) {continue;}
      if (!this.isGroupAllowed(group)) {continue;}
      if (!this.isGroupActive(group)) {continue;}
      if (subGroupConstraint && !this.areAllSuperGroupsDectivated(group)) {continue;}
      IMetropolisEvent event = this.getToggleEvent(group);
      double delta = event.getDelta();
      if (delta < minDelta) {
        returnGroup = group;
        minDelta = delta;
      }
    }
    return returnGroup;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getValue()
   */
  public double getValue() {
    
    //double realValue = 0;
    //double realValue = this.getClusterExpansion().fitECI(new LeastSquaresFitter(), this.getActiveGroups(), false).scoreFit();
    //double trackedValue = m_Fitter.scoreFit();
    //System.out.println("Delta'd value: " + trackedValue + "  Recaculated value: " + realValue);
    
    return m_Fitter.scoreFit();
  }
  
  protected class Event implements IMetropolisEvent {
    
    private FitClusterSelector m_Selector;
    private IMetropolisEvent m_WrappedEvent;
    private ILinearFitter m_OldFitter;
   
    public Event(FitClusterSelector selector) {
      m_Selector = selector;
    }
    
    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#trigger()
     */
    public void trigger() {
      m_OldFitter = m_Selector.m_Fitter;
      m_WrappedEvent.trigger();
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#getDelta()
     */
    public double getDelta() {
      
      double oldValue = m_Selector.getValue();
      this.trigger();
      double delta = m_Selector.getValue() - oldValue;
      this.reverse();
      return delta;
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#triggerGetDelta()
     */
    public double triggerGetDelta() {
      double oldValue = m_Selector.getValue();
      this.trigger();
      return m_Selector.getValue() - oldValue;
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#reverse()
     */
    public void reverse() {
      m_Event.reverse();
      m_Selector.m_Fitter = m_OldFitter;
    }
    
  }

}
