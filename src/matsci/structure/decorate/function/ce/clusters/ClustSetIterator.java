/*
 * Created on Jun 3, 2006
 *
 */
package matsci.structure.decorate.function.ce.clusters;

import java.util.Arrays;
import matsci.util.arrays.*;

public class ClustSetIterator {

  protected int[] m_ClusterMap;
  protected int[][] m_RequiredClusters; // These clusters must be active before this one can be
  //protected int[][] m_ConstrainingClusters; // This cluster cannot be deactivated if one of these is
  
  protected ClusterGroup[] m_AllowedClusters;
  protected BigArrayIndexer m_StateIterator;
  
  public ClustSetIterator(ClusterGroup[] allowedClusters) {
    
    // Sort the allowed clusters set so that we can efficiently branch
    m_AllowedClusters = new ClusterGroup[allowedClusters.length];
    int sortedClustIndex = allowedClusters.length - 1;
    int currSize = 0;
    while (sortedClustIndex >= 0) {
      for (int clustIndex = 0; clustIndex < allowedClusters.length; clustIndex++) {
        if (allowedClusters[clustIndex].numSitesPerCluster() == currSize) {
          m_AllowedClusters[sortedClustIndex--] = allowedClusters[clustIndex];
        }
      }
      currSize++;
    }
    
    m_RequiredClusters = new int[m_AllowedClusters.length][0];
    //m_ConstrainingClusters = new int[m_AllowedClusters.length][0];
    
    int maxGroupNumber = 0;
    for (int groupIndex = 0; groupIndex < m_AllowedClusters.length; groupIndex++) {
      maxGroupNumber = Math.max(maxGroupNumber, m_AllowedClusters[groupIndex].getGroupNumber());
    }
    m_ClusterMap = new int[maxGroupNumber + 1];
    Arrays.fill(m_ClusterMap, -1);
    for (int groupIndex = 0; groupIndex < m_AllowedClusters.length; groupIndex++) {
      m_ClusterMap[m_AllowedClusters[groupIndex].getGroupNumber()] = groupIndex;
    }
    
    for (int groupIndex = 0; groupIndex < m_AllowedClusters.length; groupIndex++) {
      ClusterGroup group = m_AllowedClusters[groupIndex];
      for (int superGroupIndex = 0; superGroupIndex < group.numSuperGroups(); superGroupIndex++) {
        int superIndex = m_ClusterMap[group.getSuperGroup(superGroupIndex).getGroupNumber()];
        if (superIndex < 0) {continue;}
        //m_ConstrainingClusters[groupIndex] = ArrayUtils.appendElement(m_ConstrainingClusters[groupIndex], superIndex);
        m_RequiredClusters[superIndex] = ArrayUtils.appendElement(m_RequiredClusters[superIndex], groupIndex);
      }
    }
    
    int[] iteratorArray = new int[m_AllowedClusters.length];
    Arrays.fill(iteratorArray, 2);
    m_StateIterator = new BigArrayIndexer(iteratorArray);
  }
  
  public ClusterGroup[] getInitialSet() {
    return new ClusterGroup[0];
  }
  
  public ClusterGroup[] getNextSet(ClusterGroup[] currentSet) {
    
    int[] currentState = this.getStateForSet(currentSet);
    
    while (m_StateIterator.increment(currentState)) {
      int badIndex = this.isStateAllowed(currentState);
      if (badIndex < 0) {return this.getSetForState(currentState);}
      m_StateIterator.branchEnd(badIndex, currentState);
    }
    
    return null;
  }
  
  /**
   * This is really approximate due to the subcluster constraint
   */
  public double getApproximateProgress(ClusterGroup[] currentSet) {
    int[] state = this.getStateForSet(currentSet);
    return m_StateIterator.joinValuesBounded(state).doubleValue() / m_StateIterator.numAllowedStates().doubleValue();
  }
  
  protected ClusterGroup[] getSetForState(int[] state) {
    
    int numClusts = 0;
    for (int stateIndex = 0; stateIndex < state.length; stateIndex++) {
      numClusts += state[stateIndex];
    }
    
    ClusterGroup[] set = new ClusterGroup[numClusts];
    int setIndex = 0;
    for (int stateIndex = 0; stateIndex < state.length; stateIndex++) {
      if (state[stateIndex] == 0) {continue;}
      set[setIndex++] = m_AllowedClusters[stateIndex];
    }
    return set;
  }
  
  protected int[] getStateForSet(ClusterGroup[] set) {
    int[] state = new int[m_AllowedClusters.length];
    for (int setIndex = 0; setIndex < set.length; setIndex++) {
      int allowedIndex = m_ClusterMap[set[setIndex].getGroupNumber()];
      if (allowedIndex < 0) {
        throw new RuntimeException("Given cluster is not known as an allowed cluster for this cluster iterator.");
      }
      state[allowedIndex] = 1;
    }
    return state;
  }
  
  /**
   * Returns the index of the offending setting
   * @param state
   * @return
   */
  protected int isStateAllowed(int[] state) {
    for (int stateIndex = 0; stateIndex < state.length; stateIndex++) {
      if (state[stateIndex] == 0) {continue;}
      int[] requiredClusts = m_RequiredClusters[stateIndex];
      for (int requiredClustNum = 0; requiredClustNum < requiredClusts.length; requiredClustNum++) {
        int requiredIndex = requiredClusts[requiredClustNum];
        if (state[requiredIndex] != 1) {return stateIndex;}
      }
    }
    return -1;
  }

}
