/*
 * Created on Mar 21, 2005
 *
 */
package matsci.structure.decorate.function.ce;

import java.util.Arrays;


import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.basis.DiscreteBasis;
import matsci.structure.Structure;
import matsci.structure.decorate.exceptions.IllegalOccupationException;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.function.ce.clusters.ClusterGroupMapper;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class FastAppliedCE extends AbstractAppliedCE {

  private final short[] m_SiteStates;

  // The first index is the group
  private ClusterGroup[] m_ActiveSuperGroups = new ClusterGroup[0];
  private double[][] m_ClusterValueLUT = new double[0][]; // The second is the cluster state
  private short[][] m_ClusterStates = new short[0][]; // The second is the "cluster index"
  private double[][][] m_SuperGroupCorrelations = new double[0][][]; // The second index is the superState, the third is the function group
  
  // These are used for the subgroup mapping
  // The first index is the supergroup
  private ClusterGroup[][] m_ActiveSubGroups = new ClusterGroup[0][]; // The second index is the subgroup
  private double[][][][] m_SubGroupCorrelations = new double[0][][][]; // The third index is the superGroup state, and the fourth is the subgroup function group number
  
  // The second index is the prim site index, and the third index is the cluster for that site
  private int[][][] m_Coefficients = new int[0][][];

  // The second index is the sigma site index, and the third is the cluster for that site
  private int[][][] m_ClusterIndices = new int[0][][];
  
  private boolean m_HoldLUT_Refresh = false;

  public FastAppliedCE(ClusterExpansion clusterExpansion, SuperStructure structure) {
    super(clusterExpansion, structure);
    m_SiteStates = new short[this.numSigmaSites()];
    //this.refreshFromStructure(); // No real reason to do this now; eliminating it improves compatibility with superstructuredecorator
    // On second thought, it's counter-intuitive not to do this, and superStructureDecoratro
    // effectively does this in a way that allows illegal occupations.  Here I try the same.
    for (int sigmaIndex = 0; sigmaIndex < m_SiteStates.length; sigmaIndex++) {
      try {
        m_SiteStates[sigmaIndex] = (short) this.readSigmaState(sigmaIndex);
      } catch (IllegalOccupationException e) {
        m_SiteStates[sigmaIndex] = -1;
      }
    }
  }
  
  public FastAppliedCE(ClusterExpansion clusterExpansion, Structure structure) {
    this(clusterExpansion, clusterExpansion.getBaseStructure().getSuperStructure(structure));
  }

  public FastAppliedCE(ClusterExpansion clusterExpansion, int[][] superToDirect) {
    super(clusterExpansion, superToDirect);
    m_SiteStates = new short[this.numSigmaSites()];
    //this.refreshFromStructure();  // No real reason to do this now; eliminating it improves compatibility with superstructuredecorator
  }

  public void activateAllGroups() {
    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    this.activateGroups(clusterExpansion.getAllGroups(true));
    /*
    int maxGroupSize = 0;
    for (int groupNum = 0; groupNum <= clusterExpansion.getMaxGroupNumber(); groupNum++) {
      ClusterGroup group = clusterExpansion.getClusterGroup(groupNum);
      if (group == null) {continue;}
      maxGroupSize = Math.max(maxGroupSize, group.numSitesPerCluster());
    }
    
    for (int groupSize =maxGroupSize; groupSize >= 0; groupSize--) {
      for (int groupNum = 0; groupNum <= clusterExpansion.getMaxGroupNumber(); groupNum++) {
        ClusterGroup group = clusterExpansion.getClusterGroup(groupNum);
        if (group == null) {continue;}
        if (group.numSitesPerCluster() == groupSize) {
          this.activateGroup(group);
        }
      }
    }*/
  }
  
  public void activateGroups(ClusterGroup[] groups) {
    
    m_HoldLUT_Refresh = true;
    // Find the maximum size of these groups
    int maxGroupSize = 0;
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      maxGroupSize = Math.max(maxGroupSize, groups[groupIndex].numSitesPerCluster());
    }
    
    for (int groupSize =maxGroupSize; groupSize >= 0; groupSize--) {
      for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
        if (groups[groupIndex].numSitesPerCluster() == groupSize) {
          this.activateGroup(groups[groupIndex]);
        }
      }
    }

    m_HoldLUT_Refresh = false;
    this.refreshFromHamiltonian();
  }

  public boolean activateGroup(ClusterGroup group) {

    if (this.isGroupActive(group)) {return false;}

    // Check to see if this is a subGroup of any known superGroups
    for (int superGroupIndex =0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      ClusterGroup superGroup = m_ActiveSuperGroups[superGroupIndex];
      if (superGroup.isSuperGroupOf(group)) {
        this.activateSubGroup(group, superGroupIndex);
        return true;
      }
    }
    
    this.activateSuperGroup(group);
    
    // Check to see if any known superGroups are subGroups of this
    for (int superGroupIndex =0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      ClusterGroup superGroup = m_ActiveSuperGroups[superGroupIndex];
      if (superGroup.isSubGroupOf(group)) {
        this.deactivateSuperGroup(superGroupIndex); // Removes it as a superGroup
        this.activateGroup(superGroup); // Adds it back in, this time as a subGroup
      }
    }
    
    return true;
  }
  
  private void activateSubGroup(ClusterGroup group, int superGroupIndex) {
    
    m_ActiveSubGroups[superGroupIndex] = (ClusterGroup[]) ArrayUtils.appendElement(m_ActiveSubGroups[superGroupIndex], group);
    
    m_SubGroupCorrelations[superGroupIndex] = ArrayUtils.growArray(m_SubGroupCorrelations[superGroupIndex], 1);
    this.refreshLUT(superGroupIndex);
  }
  
  private void activateSuperGroup(ClusterGroup group) {
   
    int groupIndex = m_ActiveSuperGroups.length;

    // Add this to the active groups array
    m_ActiveSuperGroups = (ClusterGroup[]) ArrayUtils.appendElement(m_ActiveSuperGroups, group);
    
    // Create empty lists for subgroups of this group
    m_ActiveSubGroups = (ClusterGroup[][]) ArrayUtils.appendElement(m_ActiveSubGroups, new ClusterGroup[0]);
    m_SubGroupCorrelations = ArrayUtils.appendElement(m_SubGroupCorrelations, new double[0][][]);

    // Refresh the lookup table for this group
    m_ClusterValueLUT = ArrayUtils.growArray(m_ClusterValueLUT, 1);
    m_SuperGroupCorrelations =ArrayUtils.growArray(m_SuperGroupCorrelations, 1);
    this.refreshLUT(groupIndex);

    // Initialize the cluster coefficients for this group
    m_Coefficients = ArrayUtils.growArray(m_Coefficients, 1);
    this.initCoefficients(groupIndex);

    // Initialize the cluster indices and the cluster states
    m_ClusterStates = ArrayUtils.growArray(m_ClusterStates, 1);
    m_ClusterIndices = ArrayUtils.growArray(m_ClusterIndices, 1);
    this.initClusters(groupIndex);
  }
  
  public boolean deactivateGroup(ClusterGroup group) {
    
    // Find the index of the given group
    int groupIndex;
    for (groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex++) {
      if (m_ActiveSuperGroups[groupIndex] == group) {
        this.deactivateSuperGroup(groupIndex);
        return true;
      }
    }
   
    for (int superGroupIndex = 0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      ClusterGroup[] subGroups = m_ActiveSubGroups[superGroupIndex];
      for (int subGroupIndex = 0; subGroupIndex < subGroups.length; subGroupIndex++) {
        if (subGroups[subGroupIndex] == group) {
          this.deactivateSubGroup(superGroupIndex, subGroupIndex);
          return true;
        }
      }
    }
    
    return false;
  }
  
  private void deactivateSubGroup(int superGroupIndex, int subGroupIndex) {
    
    m_ActiveSubGroups[superGroupIndex] = (ClusterGroup[]) ArrayUtils.removeElement(m_ActiveSubGroups[superGroupIndex], subGroupIndex);
    m_SubGroupCorrelations[superGroupIndex] = ArrayUtils.removeElement(m_SubGroupCorrelations[superGroupIndex], subGroupIndex);
    this.refreshLUT(superGroupIndex);
  }
  
  private void deactivateSuperGroup(int superGroupIndex) {
    
    // From here on out we just shrink the arrays by removing the given group
    m_ActiveSuperGroups = (ClusterGroup[]) ArrayUtils.removeElement(m_ActiveSuperGroups, superGroupIndex);
    m_ClusterValueLUT = ArrayUtils.removeElement(m_ClusterValueLUT, superGroupIndex);
    m_SuperGroupCorrelations =ArrayUtils.removeElement(m_SuperGroupCorrelations, superGroupIndex);
    m_ClusterStates = ArrayUtils.removeElement(m_ClusterStates, superGroupIndex);
    m_Coefficients = ArrayUtils.removeElement(m_Coefficients, superGroupIndex);
    m_ClusterIndices =ArrayUtils.removeElement(m_ClusterIndices, superGroupIndex);
    
    // Now we manage the subGroups
    ClusterGroup[] subGroups = m_ActiveSubGroups[superGroupIndex];
    m_ActiveSubGroups = (ClusterGroup[][]) ArrayUtils.removeElement(m_ActiveSubGroups, superGroupIndex);
    m_SubGroupCorrelations = ArrayUtils.removeElement(m_SubGroupCorrelations, superGroupIndex);
    this.activateGroups(subGroups);
    
  }
  
  public void deactivateAllGroups() {

    m_ActiveSuperGroups = new ClusterGroup[0];
    m_ClusterValueLUT = new double[0][];
    m_SuperGroupCorrelations = new double[0][][];
    m_ClusterStates = new short[0][];
    m_Coefficients = new int[0][][];
    m_ClusterIndices = new int[0][][];
    
    m_ActiveSubGroups = new ClusterGroup[0][];
    m_SubGroupCorrelations = new double[0][][][];
  }

  public boolean isGroupActive(ClusterGroup group) {
    for (int superGroupIndex =0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      if (m_ActiveSuperGroups[superGroupIndex] == group) {return true;}
      ClusterGroup[] subGroups = m_ActiveSubGroups[superGroupIndex];
      for (int subGroupIndex =0; subGroupIndex < subGroups.length; subGroupIndex++) {
        if (subGroups[subGroupIndex] == group) {return true;}
      }
    }
    
    return false;
  }
  
  public ClusterGroup[] getActiveGroups() {
    int numActiveGroups = m_ActiveSuperGroups.length;
    for (int superGroupIndex = 0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      numActiveGroups += m_ActiveSubGroups[superGroupIndex].length;
    }
    
    ClusterGroup[] returnArray = new ClusterGroup[numActiveGroups];
    int returnIndex =0;
    for (int superGroupIndex = 0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      returnArray[returnIndex++] = m_ActiveSuperGroups[superGroupIndex];
      ClusterGroup[] subGroups =m_ActiveSubGroups[superGroupIndex];
      for (int subGroupIndex = 0; subGroupIndex < subGroups.length; subGroupIndex++) {
        returnArray[returnIndex++] = subGroups[subGroupIndex];
      }
    }
    
    return returnArray;
  }

  public void refreshFromHamiltonian() {
    for (int groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex++) {
      this.refreshLUT(groupIndex);
    }
  }

  /**
   * Handles the correlation cache as well
   * 
   * @param superGroupIndex
   */
  private void refreshLUT(int superGroupIndex) {
    
    if (m_HoldLUT_Refresh) {
      return;
    }
    
    ClusterGroupMapper mapper = this.getClusterExpansion().getClusterMapper();
    
    m_ClusterValueLUT[superGroupIndex] = m_ActiveSuperGroups[superGroupIndex].getLUT();
    ClusterGroup superGroup = m_ActiveSuperGroups[superGroupIndex];
    
    m_SuperGroupCorrelations[superGroupIndex] = new double[superGroup.numClusterStates()][superGroup.numFunctionGroups()];

    // Cache the values for the supergroup
    for (int superState = 0; superState < superGroup.numClusterStates(); superState++) {
      double[] stateCorrelations = m_SuperGroupCorrelations[superGroupIndex][superState];
      for (int functionGroup = 0; functionGroup < stateCorrelations.length; functionGroup++) {
        stateCorrelations[functionGroup] += superGroup.getFunctionGroupValue(superState, functionGroup);
      }
    }
    
    for (int subGroupIndex =0; subGroupIndex < m_ActiveSubGroups[superGroupIndex].length; subGroupIndex++) {
      ClusterGroup subGroup = m_ActiveSubGroups[superGroupIndex][subGroupIndex];
      double[] subLUT = m_ActiveSubGroups[superGroupIndex][subGroupIndex].getLUT();
      double[][] subCorrelations = new double[superGroup.numClusterStates()][subGroup.numFunctionGroups()];
      m_SubGroupCorrelations[superGroupIndex][subGroupIndex] = subCorrelations;
      int[][] maps = mapper.findSymmetryMaps(superGroup, subGroup, false);
	  for (int superState = 0; superState < superGroup.numClusterStates(); superState++) {
	
	    int[] superSiteStates = superGroup.getSiteStates(superState);
	    double[] stateCorrelations = subCorrelations[superState];
	      
	    for (int mapNum = 0; mapNum < maps.length; mapNum++) {
	      int[] map = maps[mapNum];

          int[] subSiteStates = new int[map.length];
          for (int subSiteIndex = 0; subSiteIndex < subSiteStates.length; subSiteIndex++) {
            subSiteStates[subSiteIndex] = superSiteStates[map[subSiteIndex]];
          }
          int subState = subGroup.getClusterState(subSiteStates);
          double increment = subLUT[subState] * subGroup.getMultiplicity() / (superGroup.getMultiplicity() * maps.length);
          m_ClusterValueLUT[superGroupIndex][superState] += increment;

          for (int functionGroup = 0; functionGroup < subGroup.numFunctionGroups(); functionGroup++) {
            increment = subGroup.getFunctionGroupValue(subState, functionGroup) / maps.length; // We rescale when we return the correlations for greater precision
            stateCorrelations[functionGroup] += increment;
          }
        }
      }
    }
  }

  private void initCoefficients(int groupIndex) {

    ClusterGroup group = m_ActiveSuperGroups[groupIndex];
    ClusterExpansion clusterExpansion = this.getClusterExpansion();

    int[][] primCoefficients = new int[clusterExpansion.numPrimSites()][0];
    int[] seenClusterIndices = new int[this.numSigmaSites()];
    Arrays.fill(seenClusterIndices, -1);

    for (int clustNum = 0; clustNum < group.numPrimClusters(); clustNum++) {

      // Record the coefficients
      Structure.Site[] clustSites = this.getStructure().getSites(group.getPrimClusterCoords(clustNum));
      for (int siteNum = 0; siteNum < clustSites.length; siteNum++) {
        SuperStructure.Site site = (SuperStructure.Site) clustSites[siteNum];
        int sigmaIndex = this.getSigmaIndex(site.getIndex());
        int primIndex = site.getParentSite().getIndex();
        int seenClustIndex = seenClusterIndices[sigmaIndex];
        if (seenClustIndex == -1) {
          seenClustIndex = primCoefficients[primIndex].length;
          primCoefficients[primIndex] = ArrayUtils.growArray(primCoefficients[primIndex], 1);
          seenClusterIndices[sigmaIndex] = seenClustIndex;
        }
        primCoefficients[primIndex][seenClustIndex] += group.getStateCoefficient(siteNum);
      }

      // Reset the seen cluster indices
      for (int siteNum = 0; siteNum < clustSites.length; siteNum++) {
        seenClusterIndices[this.getSigmaIndex(clustSites[siteNum].getIndex())] = -1;
      }
    }

    m_Coefficients[groupIndex] = primCoefficients;
  }

  protected void initClusters(int groupIndex) {

    // Some variables we will use below
    ClusterGroup group = m_ActiveSuperGroups[groupIndex];
    SuperStructure structure = this.getSuperStructure();
    DiscreteBasis integerBasis = structure.getParentStructure().getIntegerBasis();
    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    int numPrimCells = structure.numPrimCells();

    // Size the arrays
    short[] clusterStates = new short[group.numPrimClusters() * numPrimCells];
    int[][] clusterIndices = new int[this.numSigmaSites()][];
    int[][] coefficients = m_Coefficients[groupIndex];
    for (int siteNum = 0; siteNum < clusterIndices.length; siteNum++) {
      int primIndex = ((SuperStructure.Site) this.getSigmaSite(siteNum)).getParentSite().getIndex();
      clusterIndices[siteNum] = new int[coefficients[primIndex].length];
    }

    // Populate the arrays
    int[] siteClustIndices = new int[clusterExpansion.numPrimSites()];
    int siteClustIndex = 0;
    for (int clustNum = 0; clustNum < group.numPrimClusters(); clustNum++) {
      Coordinates[] clustCoords = group.getPrimClusterCoords(clustNum);

      boolean[] seenSites = new boolean[this.numSigmaSites()]; // resets this to all false
      for (int siteNum = 0; siteNum < clustCoords.length; siteNum++) {
        Coordinates intCoords = clustCoords[siteNum].getCoordinates(integerBasis);
        double[] intCoordArray = intCoords.getArrayCopy();
        int primIndex = (int) intCoordArray[intCoordArray.length - 1];
        int firstSigmaIndex = this.getSigmaIndex(structure.getSiteIndex(intCoords));
        boolean newSite = false;
        if (!seenSites[firstSigmaIndex]) {
          siteClustIndex = siteClustIndices[primIndex]++;
          newSite = true;
        }
        seenSites[firstSigmaIndex] = true;
        for (int imageNum = 0; imageNum < numPrimCells; imageNum++) {
          int[] cellCoords = this.getSuperStructure().getSuperLattice().getPrimCellOrigin(imageNum);          
          //int[] cellCoords = structure.getSuperSiteLattice().getPrimCellOrigin(imageNum);
          for (int coordNum = 0; coordNum < cellCoords.length; coordNum++) {
            cellCoords[coordNum] = cellCoords[coordNum] + (int) intCoordArray[coordNum];
          }
          int sigmaIndex = this.getSigmaIndex(structure.getSiteIndex(cellCoords, primIndex));
          int clustIndex = (clustNum * numPrimCells) + imageNum;
          if (newSite) {clusterIndices[sigmaIndex][siteClustIndex] = clustIndex;}
          clusterStates[clustIndex] += m_SiteStates[sigmaIndex] * group.getStateCoefficient(siteNum);
        }
      }
    }

    m_ClusterStates[groupIndex] = clusterStates;
    m_ClusterIndices[groupIndex] = clusterIndices;
  }

  public double getValue() {
    double value = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex++) {
      short[] clusterStates = m_ClusterStates[groupIndex];
      double[] lut = m_ClusterValueLUT[groupIndex];
      for (int clustNum = 0; clustNum < clusterStates.length; clustNum++) {
        value += lut[clusterStates[clustNum]];
      }
    }
    
    // TODO replace the need for this (and getChemPotDelta) by modifying lookup tables
    for (int sigmaSite = 0; sigmaSite < this.numSigmaSites(); sigmaSite++) {
      value += this.getChemPot(sigmaSite);
    }
    //return value / this.getSuperStructure().numPrimCells();
    return value;
  }

  public double[][] setSigmaStateGetCorrelationDelta(int sigmaIndex, int newState) {

    super.setSigmaState(sigmaIndex, newState); // Actually decorates the lattice
    
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta == 0) {return null;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();

    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    double[][] returnArray = new double[clusterExpansion.getMaxGroupNumber() + 1][];
    for (int superGroupIndex = 0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      ClusterGroup superGroup = m_ActiveSuperGroups[superGroupIndex];
      int[] coefficients = m_Coefficients[superGroupIndex][primIndex];
      double[] superGroupDeltas = new double[superGroup.numFunctionGroups()];
      int[] clusterIndices = m_ClusterIndices[superGroupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[superGroupIndex];
      
      // Create the subGroup Correlation arrays
      for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[superGroupIndex].length; subGroupIndex++) {
        ClusterGroup subGroup = m_ActiveSubGroups[superGroupIndex][subGroupIndex];
        returnArray[subGroup.getGroupNumber()] = new double[subGroup.numFunctionGroups()];
      }
      
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        int clusterIndex = clusterIndices[clustNum];
        short oldClustState = clusterStates[clusterIndex];
        short newClustState = (short) (oldClustState + coefficients[clustNum] * siteStateDelta);
        for (int functionGroup = 0; functionGroup < superGroupDeltas.length; functionGroup++) {
          superGroupDeltas[functionGroup] -= m_SuperGroupCorrelations[superGroupIndex][oldClustState][functionGroup];
          superGroupDeltas[functionGroup] += m_SuperGroupCorrelations[superGroupIndex][newClustState][functionGroup];
        }
        clusterStates[clusterIndex] = newClustState;
        
        // Handle the subgroups
        for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[superGroupIndex].length; subGroupIndex++) {
          ClusterGroup subGroup = m_ActiveSubGroups[superGroupIndex][subGroupIndex];
          double[] subCorrelations = returnArray[subGroup.getGroupNumber()];
          for (int functionGroup = 0; functionGroup < subGroup.numFunctionGroups(); functionGroup++) {
            subCorrelations[functionGroup] -= m_SubGroupCorrelations[superGroupIndex][subGroupIndex][oldClustState][functionGroup];
            subCorrelations[functionGroup] += m_SubGroupCorrelations[superGroupIndex][subGroupIndex][newClustState][functionGroup];
          }
        }
        
      }
      returnArray[superGroup.getGroupNumber()] = superGroupDeltas;
    }

    // Scale by the multiplicities
    int numPrimCells = this.getSuperStructure().numPrimCells();
    for (int groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveSuperGroups[groupIndex];
      double multiplicity = 1.0 / (group.getMultiplicity() * numPrimCells);
      double[] groupCorrelations = returnArray[group.getGroupNumber()];
      for (int functionGroup = 0; functionGroup < groupCorrelations.length; functionGroup++) {
        groupCorrelations[functionGroup] *= multiplicity;
      }
      
      for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[groupIndex].length; subGroupIndex++) {
        ClusterGroup subGroup = m_ActiveSubGroups[groupIndex][subGroupIndex];
        double[] subGroupCorrelations = returnArray[subGroup.getGroupNumber()];
        for (int functionGroup = 0; functionGroup < subGroupCorrelations.length; functionGroup++) {
          subGroupCorrelations[functionGroup] *= multiplicity;
        }
      }
    }

    m_SiteStates[sigmaIndex] = (short) newState;
    return returnArray;
  }
  
  public double[][] getCorrelationDelta(int sigmaIndex, int newState) {
    
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta == 0) {return null;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();

    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    double[][] returnArray = new double[clusterExpansion.getMaxGroupNumber() + 1][];
    for (int superGroupIndex = 0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      ClusterGroup superGroup = m_ActiveSuperGroups[superGroupIndex];
      int[] coefficients = m_Coefficients[superGroupIndex][primIndex];
      double[] superGroupDeltas = new double[superGroup.numFunctionGroups()];
      int[] clusterIndices = m_ClusterIndices[superGroupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[superGroupIndex];
      
      // Create the subGroup Correlation arrays
      for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[superGroupIndex].length; subGroupIndex++) {
        ClusterGroup subGroup = m_ActiveSubGroups[superGroupIndex][subGroupIndex];
        returnArray[subGroup.getGroupNumber()] = new double[subGroup.numFunctionGroups()];
      }
      
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        int clusterIndex = clusterIndices[clustNum];
        short oldClustState = clusterStates[clusterIndex];
        short newClustState = (short) (oldClustState + coefficients[clustNum] * siteStateDelta);
        for (int functionGroup = 0; functionGroup < superGroupDeltas.length; functionGroup++) {
          superGroupDeltas[functionGroup] -= m_SuperGroupCorrelations[superGroupIndex][oldClustState][functionGroup];
          superGroupDeltas[functionGroup] += m_SuperGroupCorrelations[superGroupIndex][newClustState][functionGroup];
        }
        
        // Handle the subgroups
        for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[superGroupIndex].length; subGroupIndex++) { 
          ClusterGroup subGroup = m_ActiveSubGroups[superGroupIndex][subGroupIndex];
          double[] subCorrelations = returnArray[subGroup.getGroupNumber()];
          for (int functionGroup = 0; functionGroup < subGroup.numFunctionGroups(); functionGroup++) {
            subCorrelations[functionGroup] -= m_SubGroupCorrelations[superGroupIndex][subGroupIndex][oldClustState][functionGroup];
            subCorrelations[functionGroup] += m_SubGroupCorrelations[superGroupIndex][subGroupIndex][newClustState][functionGroup];
          }
        }
        
      }
      returnArray[superGroup.getGroupNumber()] = superGroupDeltas;
    }

    // Scale by the multiplicities
    int numPrimCells = this.getSuperStructure().numPrimCells();
    for (int groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveSuperGroups[groupIndex];
      double multiplicity = 1.0 / (group.getMultiplicity() * numPrimCells);
      double[] groupCorrelations = returnArray[group.getGroupNumber()];
      for (int functionGroup = 0; functionGroup < groupCorrelations.length; functionGroup++) {
        groupCorrelations[functionGroup] *= multiplicity;
      }
      
      for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[groupIndex].length; subGroupIndex++) {
        ClusterGroup subGroup = m_ActiveSubGroups[groupIndex][subGroupIndex];
        double[] subGroupCorrelations = returnArray[subGroup.getGroupNumber()];
        for (int functionGroup = 0; functionGroup < subGroupCorrelations.length; functionGroup++) {
          subGroupCorrelations[functionGroup] *= multiplicity;
        }
      }
    }

    return returnArray;
    
  }

  public double[][] getCorrelations() {

    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    double[][] returnArray = new double[clusterExpansion.getMaxGroupNumber() + 1][];
    for (int superGroupIndex = 0; superGroupIndex < m_ActiveSuperGroups.length; superGroupIndex++) {
      ClusterGroup group = m_ActiveSuperGroups[superGroupIndex];
      short[] clusterStates = m_ClusterStates[superGroupIndex];
      double[] groupCorrelations = new double[group.numFunctionGroups()];
      
      // Create the subGroup Correlation arrays
      for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[superGroupIndex].length; subGroupIndex++) {
        ClusterGroup subGroup = m_ActiveSubGroups[superGroupIndex][subGroupIndex];
        returnArray[subGroup.getGroupNumber()] = new double[subGroup.numFunctionGroups()];
      }
      
      for (int clustNum = 0; clustNum < clusterStates.length; clustNum++) {
        int superState = clusterStates[clustNum];
        for (int functionGroup = 0; functionGroup < groupCorrelations.length; functionGroup++) {
          groupCorrelations[functionGroup] += m_SuperGroupCorrelations[superGroupIndex][superState][functionGroup];
        }
        
        // Handle the subgroups
        for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[superGroupIndex].length; subGroupIndex++) {
          ClusterGroup subGroup = m_ActiveSubGroups[superGroupIndex][subGroupIndex];
          double[] subCorrelations = returnArray[subGroup.getGroupNumber()];
          for (int functionGroup = 0; functionGroup < subGroup.numFunctionGroups(); functionGroup++) {
            subCorrelations[functionGroup] += m_SubGroupCorrelations[superGroupIndex][subGroupIndex][superState][functionGroup];
          }
        }
        
      }

      returnArray[group.getGroupNumber()] = groupCorrelations;
    }

    // Scale the by the multiplicities
    int numPrimCells = this.getSuperStructure().numPrimCells();
    for (int groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex ++) {
      ClusterGroup group = m_ActiveSuperGroups[groupIndex];
      double multiplicity = 1.0 / (group.getMultiplicity() * numPrimCells);
      double[] groupCorrelations = returnArray[group.getGroupNumber()];
      for (int functionGroup = 0; functionGroup < groupCorrelations.length; functionGroup++) {
        groupCorrelations[functionGroup] *= multiplicity;
      }
      
      for (int subGroupIndex = 0; subGroupIndex < m_ActiveSubGroups[groupIndex].length; subGroupIndex++) {
        ClusterGroup subGroup = m_ActiveSubGroups[groupIndex][subGroupIndex];
        double[] subGroupCorrelations = returnArray[subGroup.getGroupNumber()];
        for (int functionGroup = 0; functionGroup < subGroupCorrelations.length; functionGroup++) {
          subGroupCorrelations[functionGroup] *= multiplicity;
        }
      }
    }
    
    return returnArray;
  }
  
  public void setSigmaState(int sigmaIndex, int newState) {

    super.setSigmaState(sigmaIndex, newState);
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta ==0) {return;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();
    for (int groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex++) {
      int[] coefficients = m_Coefficients[groupIndex][primIndex];
      int[] clusterIndices = m_ClusterIndices[groupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        clusterStates[clusterIndices[clustNum]] += coefficients[clustNum] * siteStateDelta;
      }
    }
    m_SiteStates[sigmaIndex] = (short) newState;

  }

  public double getDelta(int sigmaIndex, int newState) {
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta ==0) {return 0;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();
    //double delta = 0;
    double delta = this.getChemPotDelta(sigmaIndex, newState);
    for (int groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex++) {
      int[] coefficients = m_Coefficients[groupIndex][primIndex];
      int[] clusterIndices = m_ClusterIndices[groupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      double[] lut = m_ClusterValueLUT[groupIndex];
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        int oldClusterState = clusterStates[clusterIndices[clustNum]];
        int newClusterState = oldClusterState + (coefficients[clustNum] * siteStateDelta);
        delta += lut[newClusterState] - lut[oldClusterState];
      }
    }
    //return delta / this.getSuperStructure().numPrimCells();
    return delta;
    
  }
  
  public double setSigmaStateGetDelta(int sigmaIndex, int newState) {

    super.setSigmaState(sigmaIndex, newState);
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta ==0) {return 0;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();
    //double delta = 0;
    double delta = this.getChemPotDelta(sigmaIndex, newState);
    for (int groupIndex = 0; groupIndex < m_ActiveSuperGroups.length; groupIndex++) {
      int[] coefficients = m_Coefficients[groupIndex][primIndex];
      int[] clusterIndices = m_ClusterIndices[groupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      double[] lut = m_ClusterValueLUT[groupIndex];
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        int clusterIndex = clusterIndices[clustNum];
        short oldClusterState = clusterStates[clusterIndex];
        short newClusterState = (short) (oldClusterState + (coefficients[clustNum] * siteStateDelta));
        clusterStates[clusterIndex] = newClusterState;
        delta += lut[newClusterState] - lut[oldClusterState];
      }
    }
    m_SiteStates[sigmaIndex] = (short) newState;
    //return delta / this.getSuperStructure().numPrimCells();
    return delta;
  }
  
  public double getDelta(int[] sigmaIndices, int[] newStates) {
    
    int[] oldStates = new int[newStates.length - 1];
    double returnValue = 0;
    int siteNum;
    for (siteNum = 0; siteNum < oldStates.length; siteNum++) {
      int sigmaIndex = sigmaIndices[siteNum];
      oldStates[siteNum] = m_SiteStates[sigmaIndex];
      returnValue += this.setSigmaStateGetDelta(sigmaIndex, newStates[siteNum]);
    }
    
    returnValue += this.getDelta(sigmaIndices[siteNum], newStates[siteNum]);
    
    for (siteNum = 0; siteNum < oldStates.length; siteNum++) {
      this.setSigmaState(sigmaIndices[siteNum], oldStates[siteNum]);
    }
    
    return returnValue;
  }

  public int getQuickSigmaState(int sigmaIndex) {
    return m_SiteStates[sigmaIndex];
  }
  
  public int[] getQuickSigmaStates(int[] template) {
    if (template == null) {
      template = new int[m_SiteStates.length];
    }
    
    for (int siteNum = 0; siteNum < template.length; siteNum++) {
      template[siteNum] = m_SiteStates[siteNum];
    }
    
    return template;
  }
  
}
