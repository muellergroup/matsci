package matsci.structure.decorate.function.ce;

import java.util.Arrays;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.basis.DiscreteBasis;
import matsci.structure.Structure;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>This is a complicated, but efficient way to evaluate a general cluster
 * expansion for sites with any number of allowed occupancies.  The memory 
 * usage of this method scales roughly with the sum of the number of sites in 
 * each active cluster in the supercell.  This speed of this class is highly dependent on
 * whether the total memory usage fits in the processor cache.</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * @author Tim Mueller
 * @version 1.0
 */

public class GeneralAppliedCE extends AbstractAppliedCE {

  private short[] m_SiteStates;

  // The first index is the group
  private ClusterGroup[] m_ActiveGroups = new ClusterGroup[0];
  private double[][] m_ClusterValueLUT = new double[0][];
  private short[][] m_ClusterStates = new short[0][];

  // The second index is the prim site index
  private short[][][] m_Coefficients = new short[0][][];

  // The second index is the sigma site index
  private int[][][] m_ClusterIndices = new int[0][][];

  public GeneralAppliedCE(ClusterExpansion clusterExpansion, SuperStructure structure) {
    super(clusterExpansion, structure);
    m_SiteStates = new short[this.numSigmaSites()];
    //this.refreshFromStructure();  // No real reason to do this now; eliminiating it improves compatibility with superstructuredecorator
  }

  public GeneralAppliedCE(ClusterExpansion clusterExpansion, int[][] superToDirect) {
    super(clusterExpansion, superToDirect);
    m_SiteStates = new short[this.numSigmaSites()];
    //this.refreshFromStructure();  // No real reason to do this now; eliminiating it improves compatibility with superstructuredecorator
  }

  public void activateAllGroups() {
    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    for (int groupNum = 0; groupNum <= clusterExpansion.getMaxGroupNumber(); groupNum++) {
      ClusterGroup group = clusterExpansion.getClusterGroup(groupNum);
      if (group != null) {
        this.activateGroup(group);
      }
    }
  }
  
  public void activateGroups(ClusterGroup[] groups) {
    for (int groupIndex = 0; groupIndex < groups.length; groupIndex++) {
      this.activateGroup(groups[groupIndex]);
    }
  }

  public boolean activateGroup(ClusterGroup group) {

    if (this.isGroupActive(group)) {return false;}

    int groupIndex = m_ActiveGroups.length;

    // Add this to the active groups array
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.appendElement(m_ActiveGroups, group);

    // Refresh the lookup table for this group
    m_ClusterValueLUT = ArrayUtils.growArray(m_ClusterValueLUT, 1);
    this.refreshLUT(groupIndex);

    // Initialize the cluster coefficients for this group
    m_Coefficients = ArrayUtils.growArray(m_Coefficients, 1);
    this.initCoefficients(groupIndex);

    // Initialize the cluster indices and the cluster states
    m_ClusterStates = ArrayUtils.growArray(m_ClusterStates, 1);
    m_ClusterIndices = ArrayUtils.growArray(m_ClusterIndices, 1);
    this.initClusters(groupIndex);
    
    return true;
  }
  
  public boolean deactivateGroup(ClusterGroup group) {
    
    // Find the index of the given group
    int groupIndex;
    for (groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      if (m_ActiveGroups[groupIndex] == group) {break;}
    }
    
    if (groupIndex == m_ActiveGroups.length) {return false;}
    
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.removeElement(m_ActiveGroups, groupIndex);
    m_ClusterValueLUT = ArrayUtils.removeElement(m_ClusterValueLUT, groupIndex);
    m_ClusterStates = ArrayUtils.removeElement(m_ClusterStates, groupIndex);
    m_Coefficients = ArrayUtils.removeElement(m_Coefficients, groupIndex);
    m_ClusterIndices =ArrayUtils.removeElement(m_ClusterIndices, groupIndex);
    
    return true;
  }
  
  public void deactivateAllGroups() {

    m_ActiveGroups = new ClusterGroup[0];
    m_ClusterValueLUT = new double[0][];
    m_ClusterStates = new short[0][];
    m_Coefficients = new short[0][][];
    m_ClusterIndices = new int[0][][];
  }

  public boolean isGroupActive(ClusterGroup group) {
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      if (m_ActiveGroups[groupIndex] == group) {return true;}
    }
    return false;
  }
  
  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }

  public void refreshFromHamiltonian() {
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      this.refreshLUT(groupIndex);
    }
  }

  private void refreshLUT(int groupIndex) {
    m_ClusterValueLUT[groupIndex] = m_ActiveGroups[groupIndex].getLUT();
  }

  private void initCoefficients(int groupIndex) {

    ClusterGroup group = m_ActiveGroups[groupIndex];
    ClusterExpansion clusterExpansion = this.getClusterExpansion();

    short[][] primCoefficients = new short[clusterExpansion.numPrimSites()][0];
    int[] seenClusterIndices = new int[this.numSigmaSites()];
    Arrays.fill(seenClusterIndices, -1);

    for (int clustNum = 0; clustNum < group.numPrimClusters(); clustNum++) {

      // Record the coefficients
      Structure.Site[] clustSites = this.getStructure().getSites(group.getPrimClusterCoords(clustNum));
      for (int siteNum = 0; siteNum < clustSites.length; siteNum++) {
        SuperStructure.Site site = (SuperStructure.Site) clustSites[siteNum];
        int sigmaIndex = this.getSigmaIndex(site.getIndex());
        int primIndex = site.getParentSite().getIndex();
        int seenClustIndex = seenClusterIndices[sigmaIndex];
        if (seenClustIndex == -1) {
          seenClustIndex = primCoefficients[primIndex].length;
          primCoefficients[primIndex] = ArrayUtils.growArray(primCoefficients[primIndex], 1);
          seenClusterIndices[sigmaIndex] = seenClustIndex;
        }
        primCoefficients[primIndex][seenClustIndex] += group.getStateCoefficient(siteNum);
      }

      // Reset the seen cluster indices
      for (int siteNum = 0; siteNum < clustSites.length; siteNum++) {
        seenClusterIndices[this.getSigmaIndex(clustSites[siteNum].getIndex())] = -1;
      }
    }

    m_Coefficients[groupIndex] = primCoefficients;
  }

  protected void initClusters(int groupIndex) {

    // Some variables we will use below
    ClusterGroup group = m_ActiveGroups[groupIndex];
    SuperStructure structure = this.getSuperStructure();
    DiscreteBasis integerBasis = structure.getParentStructure().getIntegerBasis();
    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    int numPrimCells = structure.numPrimCells();

    // Size the arrays
    short[] clusterStates = new short[group.numPrimClusters() * numPrimCells];
    int[][] clusterIndices = new int[this.numSigmaSites()][];
    short[][] coefficients = m_Coefficients[groupIndex];
    for (int siteNum = 0; siteNum < clusterIndices.length; siteNum++) {
      int primIndex = ((SuperStructure.Site) this.getSigmaSite(siteNum)).getParentSite().getIndex();
      clusterIndices[siteNum] = new int[coefficients[primIndex].length];
    }

    // Populate the arrays
    int[] siteClustIndices = new int[clusterExpansion.numPrimSites()];
    int siteClustIndex = 0;
    for (int clustNum = 0; clustNum < group.numPrimClusters(); clustNum++) {
      Coordinates[] clustCoords = group.getPrimClusterCoords(clustNum);

      boolean[] seenSites = new boolean[this.numSigmaSites()]; // resets this to all false
      for (int siteNum = 0; siteNum < clustCoords.length; siteNum++) {
        Coordinates intCoords = clustCoords[siteNum].getCoordinates(integerBasis);
        double[] intCoordArray = intCoords.getArrayCopy();
        int primIndex = (int) intCoordArray[intCoordArray.length - 1];
        int firstSigmaIndex = this.getSigmaIndex(structure.getSiteIndex(intCoords));
        boolean newSite = false;
        if (!seenSites[firstSigmaIndex]) {
          siteClustIndex = siteClustIndices[primIndex]++;
          newSite = true;
        }
        seenSites[firstSigmaIndex] = true;
        for (int imageNum = 0; imageNum < numPrimCells; imageNum++) {
          int[] cellCoords = this.getSuperStructure().getSuperLattice().getPrimCellOrigin(imageNum);
          //int[] cellCoords = structure.getSuperSiteLattice().getPrimCellOrigin(imageNum);
          for (int coordNum = 0; coordNum < cellCoords.length; coordNum++) {
            cellCoords[coordNum] = cellCoords[coordNum] + (int) intCoordArray[coordNum];
          }
          int sigmaIndex = this.getSigmaIndex(structure.getSiteIndex(cellCoords, primIndex));
          int clustIndex = (clustNum * numPrimCells) + imageNum;
          if (newSite) {clusterIndices[sigmaIndex][siteClustIndex] = clustIndex;}
          clusterStates[clustIndex] += m_SiteStates[sigmaIndex] * group.getStateCoefficient(siteNum);
        }
      }
    }

    m_ClusterStates[groupIndex] = clusterStates;
    m_ClusterIndices[groupIndex] = clusterIndices;
  }

  public double getValue() {
    double value = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      short[] clusterStates = m_ClusterStates[groupIndex];
      double[] lut = m_ClusterValueLUT[groupIndex];
      for (int clustNum = 0; clustNum < clusterStates.length; clustNum++) {
        value += lut[clusterStates[clustNum]];
      }
    }
    for (int sigmaSite = 0; sigmaSite < this.numSigmaSites(); sigmaSite++) {
      value += this.getChemPot(sigmaSite);
    }
    //return value / this.getSuperStructure().numPrimCells();
    return value; 
    
  }
  
  public double[][] setSigmaStateGetCorrelationDelta(int sigmaIndex, int newState) {

    super.setSigmaState(sigmaIndex, newState); // Actually decorates the lattice
    
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta == 0) {return null;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();

    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    double[][] returnArray = new double[clusterExpansion.getMaxGroupNumber() + 1][];
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      short[] coefficients = m_Coefficients[groupIndex][primIndex];
      double[] groupDeltas = new double[group.numFunctionGroups()];
      int[] clusterIndices = m_ClusterIndices[groupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        int clusterIndex = clusterIndices[clustNum];
        short oldClustState = clusterStates[clusterIndex];
        short newClustState = (short) (oldClustState + coefficients[clustNum] * siteStateDelta);
        for (int functionGroup = 0; functionGroup < groupDeltas.length; functionGroup++) {
          groupDeltas[functionGroup] -= group.getFunctionGroupValue(oldClustState, functionGroup);
          groupDeltas[functionGroup] += group.getFunctionGroupValue(newClustState, functionGroup);
        }
        clusterStates[clusterIndex] = newClustState;
      }
      returnArray[group.getGroupNumber()] = groupDeltas;
    }

    // Scale by the multiplicities
    int numPrimCells = this.getSuperStructure().numPrimCells();
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      double multiplicity = group.getMultiplicity() * numPrimCells;
      double[] groupCorrelations = returnArray[group.getGroupNumber()];
      for (int functionGroup = 0; functionGroup < groupCorrelations.length; functionGroup++) {
        groupCorrelations[functionGroup] /= multiplicity;
      }
    }

    m_SiteStates[sigmaIndex] = (short) newState;
    return returnArray;

  }
  
  public double[][] getCorrelationDelta(int sigmaIndex, int newState) {
    
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta == 0) {return null;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();

    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    double[][] returnArray = new double[clusterExpansion.getMaxGroupNumber() + 1][];
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      short[] coefficients = m_Coefficients[groupIndex][primIndex];
      double[] groupDeltas = new double[group.numFunctionGroups()];
      int[] clusterIndices = m_ClusterIndices[groupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        int clusterIndex = clusterIndices[clustNum];
        short oldClustState = clusterStates[clusterIndex];
        short newClustState = (short) (oldClustState + coefficients[clustNum] * siteStateDelta);
        for (int functionGroup = 0; functionGroup < groupDeltas.length; functionGroup++) {
          groupDeltas[functionGroup] -= group.getFunctionGroupValue(oldClustState, functionGroup);
          groupDeltas[functionGroup] += group.getFunctionGroupValue(newClustState, functionGroup);
        }
      }
      returnArray[group.getGroupNumber()] = groupDeltas;
    }

    // Scale by the multiplicities
    int numPrimCells = this.getSuperStructure().numPrimCells();
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      double multiplicity = group.getMultiplicity() * numPrimCells;
      double[] groupCorrelations = returnArray[group.getGroupNumber()];
      for (int functionGroup = 0; functionGroup < groupCorrelations.length; functionGroup++) {
        groupCorrelations[functionGroup] /= multiplicity;
      }
    }

    return returnArray;
  }

  public double[][] getCorrelations() {

    ClusterExpansion clusterExpansion = this.getClusterExpansion();
    double[][] returnArray = new double[clusterExpansion.getMaxGroupNumber() + 1][];
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      double[] groupCorrelations = new double[group.numFunctionGroups()];
      for (int clustNum = 0; clustNum < clusterStates.length; clustNum++) {
        int clustState = clusterStates[clustNum];
        for (int functionGroup = 0; functionGroup < groupCorrelations.length; functionGroup++) {
          groupCorrelations[functionGroup] += group.getFunctionGroupValue(clustState, functionGroup);
        }
      }
      returnArray[group.getGroupNumber()] = groupCorrelations;
    }

    // Scale by the multiplicities
    int numPrimCells = this.getSuperStructure().numPrimCells();
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      double multiplicity = group.getMultiplicity() * numPrimCells;
      double[] groupCorrelations = returnArray[group.getGroupNumber()];
      for (int functionGroup = 0; functionGroup < groupCorrelations.length; functionGroup++) {
        groupCorrelations[functionGroup] /= multiplicity;
      }
    }

    return returnArray;
  }

  public void setSigmaState(int sigmaIndex, int newState) {

    super.setSigmaState(sigmaIndex, newState);
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta ==0) {return;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      short[] coefficients = m_Coefficients[groupIndex][primIndex];
      int[] clusterIndices = m_ClusterIndices[groupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        clusterStates[clusterIndices[clustNum]] += coefficients[clustNum] * siteStateDelta;
      }
    }
    m_SiteStates[sigmaIndex] = (short) newState;

  }

  public double getDelta(int sigmaIndex, int newState) {
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta ==0) {return 0;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();
    //double delta = 0;
    double delta = this.getChemPotDelta(sigmaIndex, newState);
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      short[] coefficients = m_Coefficients[groupIndex][primIndex];
      int[] clusterIndices = m_ClusterIndices[groupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      double[] lut = m_ClusterValueLUT[groupIndex];
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        int clusterIndex = clusterIndices[clustNum];
        int oldClusterState = clusterStates[clusterIndex];
        int newClusterState = oldClusterState + (coefficients[clustNum] * siteStateDelta);
        delta += lut[newClusterState] - lut[oldClusterState];
      }
    }
    //return delta / this.getSuperStructure().numPrimCells();
    return delta;
  }

  public double setSigmaStateGetDelta(int sigmaIndex, int newState) {

    super.setSigmaState(sigmaIndex, newState);
    int siteStateDelta = newState - m_SiteStates[sigmaIndex];
    if (siteStateDelta ==0) {return 0;}
    int primIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();
    // double delta = 0;
    double delta = this.getChemPotDelta(sigmaIndex, newState);
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      short[] coefficients = m_Coefficients[groupIndex][primIndex];
      int[] clusterIndices = m_ClusterIndices[groupIndex][sigmaIndex];
      short[] clusterStates = m_ClusterStates[groupIndex];
      double[] lut = m_ClusterValueLUT[groupIndex];
      for (int clustNum = 0; clustNum < clusterIndices.length; clustNum++) {
        int clusterIndex = clusterIndices[clustNum];
        short oldClusterState = clusterStates[clusterIndex];
        short newClusterState = (short) (oldClusterState + (coefficients[clustNum] * siteStateDelta));
        clusterStates[clusterIndex] = newClusterState;
        delta += lut[newClusterState] - lut[oldClusterState];
      }
    }
    m_SiteStates[sigmaIndex] = (short) newState;
    // return delta / this.getSuperStructure().numPrimCells();
    return delta;
    
  }
  
  public double getDelta(int[] sigmaIndices, int[] newStates) {
    
    int[] oldStates = new int[newStates.length];
    double returnValue = 0;
    for (int siteNum = 0; siteNum < newStates.length; siteNum++) {
      oldStates[siteNum] = m_SiteStates[sigmaIndices[siteNum]];
      returnValue += this.setSigmaStateGetDelta(sigmaIndices[siteNum], newStates[siteNum]);
    }
    
    for (int siteNum = 0; siteNum < newStates.length; siteNum++) {
      this.setSigmaState(sigmaIndices[siteNum], oldStates[siteNum]);
    }
    
    return returnValue;
  }

  public int getQuickSigmaState(int sigmaIndex) {
    return m_SiteStates[sigmaIndex];
  }
  
  public int[] getQuickSigmaStates(int[] template) {
    if (template == null) {
      template = new int[m_SiteStates.length];
    }
    
    for (int siteNum = 0; siteNum < template.length; siteNum++) {
      template[siteNum] = m_SiteStates[siteNum];
    }
    
    return template;
  }
  
  public int[] countClusterStates(ClusterGroup group) {
    int groupIndex = ArrayUtils.findIndex(m_ActiveGroups, group);
    if (groupIndex < 0) {return null;}
    short[] clustStates = m_ClusterStates[groupIndex];
    int[] returnArray = new int[group.numClusterStates()];
    for (int clustNum = 0; clustNum < clustStates.length; clustNum++) {
      returnArray[clustStates[clustNum]]++;
    }
    return returnArray;
  }
  
  /*public int[] getClusterStates(ClusterGroup group) {
    int groupIndex = ArrayUtils.findIndex(m_ActiveGroups, group);
    if (groupIndex < 0) {return null;}
    
    short[] clustStates = m_ClusterStates[groupIndex];
    int[] returnArray = new int[clustStates.length];
    for (int clustNum = 0; clustNum < clustStates.length; clustNum++) {
      returnArray[clustNum] = clustStates[clustNum];
    }
    return returnArray;
  }*/
}
