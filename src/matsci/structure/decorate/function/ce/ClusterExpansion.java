package matsci.structure.decorate.function.ce;

import java.util.Arrays;

import matsci.Species;
import matsci.io.app.log.Status;
import matsci.io.clusterexpansion.*;
import matsci.location.Coordinates;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.BravaisLattice;
import matsci.structure.IStructureData;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.structure.decorate.DecorationTemplate;
import matsci.structure.decorate.exceptions.IllegalOccupationException;
import matsci.structure.decorate.function.ce.basis.CEBasis;
import matsci.structure.decorate.function.ce.basis.CECosineBasis;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.structures.*;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
import matsci.util.arrays.MatrixInverter;
import matsci.structure.function.ce.*;
import matsci.structure.function.ce.clusters.ClusterGenerator;
import matsci.structure.function.ce.clusters.ClusterGroupMapper;
import matsci.structure.function.ce.structures.ClustStructureData;

/**
 * <p>This class represents a general cluster expansion.  In addition to the information that
 * defines an DecorationTemplate, the pieces of data that define the cluster
 * expansion are the primitive cell, the cluster groups, functions, and the ECI.  
 * They are defined as follows:</p>
 * <br><b>The primitive cell</b> is a unit cell that should be the smallest periodic cell that can be 
 * used to describe the arrangement of sites in the structure on which the cluster expansion is defined.
 * You can use a unit cell other than the smallest one, but that will just make everything slower.
 * <br><b>Cluster groups</b> are collections of symmetrically equivalent clusters, where the symmetry
 * is defined by the structure of the unit cell and the allowed occupancies at each of the sites.
 * <br><b>Functions.</b> If there are N different species allowed at a given site, that site will be 
 * associated with N-1 functions, where each function is a mapping between the occpancy of the site
 * and a value.  The functions for a cluster are the set of all possible products of functions
 * for the individual sites.  The functions for a cluster group can be represented by the functions 
 * of any cluster in the group.
 * <br><b>ECI</b> are effective cluster interactions.  There is one ECI for each function in a cluster
 * group.  The value of the Hamiltonian is calculated by taking a linear combination of all cluster
 * functions where the coefficients are the ECI for the cluster functions.
 * <p>To calculate the ECI for a cluster expansion, it is useful to have information about the 
 * calculated energies and the correlations of a sample of structures based on supercells
 * of the primitive cell.  This class also provides mechanisms for storing that information.
 * <p>Copyright: Copyright (c) 2003</p>
 * @author Tim Mueller
 * @version 1.0
 */

public class ClusterExpansion extends DecorationTemplate {

  // We initialize this so it has the empty group
  private ClusterGroup[] m_ClusterGroups = new ClusterGroup[0];

  // Initialize this to a default orthogonal basis
  private CEBasis m_FunctionBasis = new CECosineBasis();
  
  // This is used to determine how clusters relate to each other
  private ClusterGroupMapper m_ClusterMapper;
  
  // This class manages all of the known structures
  private StructureList m_StructureList;
  
  private ClusterGroup[] m_GroupsBySublattice;
  
  // A penalty for excess positive charge.  A negative penalty is not necessary, as it is linearly 
  // dependent on this penalty and the individual site terms.
  private double m_PositiveChargePenalty;
  private double m_NegativeChargePenalty; // Include this anyway for regularization
  
  public ClusterExpansion(Structure structure, Species[][] allowedSpecies) {
    super(structure, allowedSpecies);
    
    m_ClusterMapper = new ClusterGroupMapper(this.getDefiningSpaceGroup());
    m_ClusterGroups = new ClusterGroup[1];
    m_ClusterGroups[0] = new ClusterGroup(0, new Coordinates[1][0], this);
    m_StructureList = new StructureList(this);
    m_GroupsBySublattice = new ClusterGroup[this.numSublattices()];
  }
  
  public ClusterExpansion(Structure structure, Species[][] allowedSpecies, CEBasis functionBasis) {
    this(structure, allowedSpecies);
    m_FunctionBasis =functionBasis;
  }
  
  public ClusterExpansion(PRIM pr) {
    this(new Structure(pr), pr.getAllowedSpecies());
  }
  
  public ClusterExpansion(PRIM pr, CEBasis functionBasis) {
    this(pr);
    m_FunctionBasis = functionBasis;
  }
  
  /*
  public ClusterExpansion(PRIM pr, ClusterGroupSet clusterData) {
    this(pr);
    this.mergeClusterGroups(clusterData);
  }
  */
  public CEBasis getFunctionBasis() {
    return m_FunctionBasis;
  }
  
  public ClusterGroupMapper getClusterMapper() {
    return m_ClusterMapper;
  }
  
  public void setPositiveChargePenalty(double penalty) {
    m_PositiveChargePenalty = penalty;
  }
  
  public double getPositiveChargePenalty() {
    return m_PositiveChargePenalty;
  }
  
  public void setNegativeChargePenalty(double penalty) {
    m_NegativeChargePenalty = penalty;
  }
  
  public double getNegativeChargePenalty() {
    return m_NegativeChargePenalty;
  }
  
  // TODO Clean up this whole cluster generation process
  public void appendClusters(int size, double radius) {
    ClusterGenerator generator = new ClusterGenerator(this);
    this.appendClusterGroups(generator.generateClusterCoords(size, radius));
  }
  
  public void appendClusters(int[] sizeByLatticeType, double radius) {
    ClusterGenerator generator = new ClusterGenerator(this);
    this.appendClusterGroups(generator.generateClusterCoords(sizeByLatticeType, radius));
  }
  
  /**
   * This method merges clusters with "size" sites per cluster, and a maximum distance between sites
   * of no more than "clustRadius".  The clusters are only created if all sites are within a 
   * distance of "distanceFromCenter" from "centerCoords".  To calculate the distance, the 
   * vector between the sites is divided into a component along the periodic dimensions, and a 
   * component along the non-periodic dimensions.  Only the length of the non-periodic component is
   * used to calculate the distance.  
   * 
   * A merge is used because if this method is called multiple times, it is anticipated that there
   * might be some duplicate groups created.  The "merge" prevents a group from being added if a 
   * symmetrically identical group has already been added.
   * 
   * @param size
   * @param clustRadius
   * @param centerCoords
   * @param distanceFromCenter
   */
  public void mergeClusters(int size, double clustRadius, Coordinates centerCoords, double distanceFromCenter, boolean includeCenter) {
    
    /**
     *  Here we remove all of the sites that are too far from the center 
     *  in the non-translational directions.
     */
    Structure structure = this.getBaseStructure();
    boolean[] allowedSites = new boolean[structure.numDefiningSites()];
    BravaisLattice lattice = structure.getDefiningLattice();
    Structure.Site[] sites = new Structure.Site[structure.numDefiningSites()];
    Coordinates shiftedCenter = lattice.removeLattice(centerCoords);
    for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
      Structure.Site site = structure.getDefiningSite(siteNum);
      Coordinates shiftedSiteCoords = lattice.removeLattice(site.getCoords());
      if (shiftedCenter.distanceFrom(shiftedSiteCoords) > distanceFromCenter) {continue;}
      allowedSites[siteNum] = true;
    }
    
    ClusterGenerator generator = new ClusterGenerator(this, allowedSites);
    Coordinates[][][] clusterCoords = generator.generateClusterCoords(size, clustRadius);
    for (int groupNum = clusterCoords.length -1; groupNum >= 0; groupNum--) {
      Coordinates[] sampleCoords = clusterCoords[groupNum][0];
      if (sampleCoords == null || sampleCoords.length == 0) {continue;}
      boolean includesCenter = false;
      for (int coordNum = 0; coordNum < sampleCoords.length; coordNum++) {
        Coordinates shiftedCoords = lattice.removeLattice(sampleCoords[coordNum]);
        includesCenter |= (shiftedCoords.isCloseEnoughTo(shiftedCenter));
      }
      if ((includesCenter && !includeCenter) || (!includesCenter && includeCenter)) {
        clusterCoords = (Coordinates[][][]) ArrayUtils.removeElement(clusterCoords, groupNum);
      }
    }
    this.mergeClusterGroups(clusterCoords);
    
  }
  
  public void appendClustersNoSym(int size, double radius) {
    ClusterGenerator generator = new ClusterGenerator(this);
    Coordinates[][][] coords = generator.generateClusterCoords(size, radius);
    
    int numClusters = 0;
    for (int orbitIndex = 0; orbitIndex < coords.length; orbitIndex++) {
      numClusters += coords[orbitIndex].length;
    }
    Coordinates[][][] flattenedArray = new Coordinates[numClusters][1][];
    
    int flattenedClustIndex = 0;
    for (int orbitIndex = 0; orbitIndex < coords.length; orbitIndex++) {
      for (int clustNum = 0; clustNum < coords[orbitIndex].length; clustNum++) {
        flattenedArray[flattenedClustIndex++][0] = coords[orbitIndex][clustNum];
      }
    }
    this.appendClusterGroups(flattenedArray);
  }
  
  public void appendClusters(CSPECS cspecs) {
    
    ClusterGenerator generator = new ClusterGenerator(this);
    for (int entryNum = 0; entryNum < cspecs.numEntries(); entryNum++) {
      int numSites = cspecs.getSitesPerCluster(entryNum);
      double distance = cspecs.getClusterRadius(entryNum);
      this.appendClusterGroups(generator.generateClusterCoords(numSites, distance));
    }
    
  }
  
  public void appendClusterGroup(Coordinates[] sampleCoords) {
    this.appendClusterGroups(new Coordinates[][] {sampleCoords});
  }
  
  public void appendClusterGroups(Coordinates[][] sampleCoords) {
    
    ClusterGenerator generator = new ClusterGenerator(this);
    Coordinates[][][] orbits = new Coordinates[sampleCoords.length][][];
    for (int entryNum = 0; entryNum < sampleCoords.length; entryNum++) {
      Coordinates[] clustCoords = sampleCoords[entryNum];
      Coordinates[][] clusterOrbit = generator.generateClusterOrbit(clustCoords);
      orbits[entryNum] = clusterOrbit;
    }
    this.appendClusterGroups(orbits);
    
  }
  
  public void appendClusterGroups(Coordinates[][][] newClusterCoords) {
    
    int oldSize =  m_ClusterGroups.length;
    m_ClusterGroups = (ClusterGroup[]) ArrayUtils.growArray(m_ClusterGroups, newClusterCoords.length);
    
    for (int groupNum = 0; groupNum < newClusterCoords.length; groupNum++) {
      int newGroupNumber = oldSize + groupNum;
      m_ClusterGroups[newGroupNumber] = new ClusterGroup(newGroupNumber, newClusterCoords[groupNum], this);
      this.updateGroupSublatticeMap(m_ClusterGroups[newGroupNumber]);
    }
    
  }
  
  public void mergeClusterGroups(Coordinates[][][] newClusterCoords) {
    
    int newGroupNumber =  m_ClusterGroups.length;
    m_ClusterGroups = (ClusterGroup[]) ArrayUtils.growArray(m_ClusterGroups, newClusterCoords.length);
    
    for (int groupNum = 0; groupNum < newClusterCoords.length; groupNum++) {
      
      if (newClusterCoords[groupNum].length > 0) {
        Coordinates[] sampleCoords = newClusterCoords[groupNum][0];
        SpaceGroup spaceGroup = this.getDefiningSpaceGroup();
        boolean match = false;
        for (int prevGroupNum = 0; prevGroupNum < newGroupNumber; prevGroupNum++) {
          Coordinates[] prevSampleCoords = m_ClusterGroups[prevGroupNum].getSampleCoords();
          if (spaceGroup.areSymmetricallyEquivalent(sampleCoords, prevSampleCoords)) {
            match = true;
            break;
          }
        }
        if (match) {continue;} // We already have this group.
      }
      
      m_ClusterGroups[newGroupNumber] = new ClusterGroup(newGroupNumber, newClusterCoords[groupNum], this);
      this.updateGroupSublatticeMap(m_ClusterGroups[newGroupNumber]);
      newGroupNumber++;
    }
    
    m_ClusterGroups = (ClusterGroup[]) ArrayUtils.truncateArray(m_ClusterGroups, newGroupNumber);
    
  }
  
  public void mergeClusterGroups(FCLUST clusterData) {
    
    int maxGroupNumber = clusterData.getMaxGroupNumber();
    int oldMaxGroupNumber = this.getMaxGroupNumber();
    if (maxGroupNumber > oldMaxGroupNumber) {
      m_ClusterGroups = (ClusterGroup[]) ArrayUtils.growArray(m_ClusterGroups, maxGroupNumber - oldMaxGroupNumber);
    }
    for (int groupNum = 0; groupNum <= maxGroupNumber; groupNum++) {
      if (clusterData.containsGroup(groupNum)) {
        m_ClusterGroups[groupNum] = new ClusterGroup(groupNum, clusterData.getClusterCoordsForGroup(groupNum), this);
        this.updateGroupSublatticeMap(m_ClusterGroups[groupNum]);
      }
    }
  }
  
  public void mergeClusterGroups(CLUST clusterData) {
    
    int maxGroupNumber = clusterData.getMaxGroupNumber();
    int oldMaxGroupNumber = this.getMaxGroupNumber();
    if (maxGroupNumber > oldMaxGroupNumber) {
      m_ClusterGroups = (ClusterGroup[]) ArrayUtils.growArray(m_ClusterGroups, maxGroupNumber - oldMaxGroupNumber);
    }
    ClusterGenerator clustGenerator = new ClusterGenerator(this);
    for (int groupNum = 1; groupNum <= maxGroupNumber; groupNum++) {
      if (clusterData.containsGroup(groupNum)) {
        Coordinates[][] allCoords = clustGenerator.generateClusterOrbit(clusterData.getClusterCoordsForGroup(groupNum));
        m_ClusterGroups[groupNum] = new ClusterGroup(groupNum, allCoords, this);
        this.updateGroupSublatticeMap(m_ClusterGroups[groupNum]);
      }
    }
  }
  
  public void appendClusterGroups(FCLUST clusterData) {
    
    int maxGroupNumber = clusterData.getMaxGroupNumber();
    int oldMaxGroupNumber = this.getMaxGroupNumber();
    m_ClusterGroups = (ClusterGroup[]) ArrayUtils.growArray(m_ClusterGroups, maxGroupNumber);
    
    // We start with 1 so that we never append the empty group, which is always group 0
    for (int groupNum = 1; groupNum <= maxGroupNumber; groupNum++) {
      if (clusterData.containsGroup(groupNum)) {
        int newGroupNumber = oldMaxGroupNumber + groupNum;
        m_ClusterGroups[newGroupNumber] = new ClusterGroup(newGroupNumber, clusterData.getClusterCoordsForGroup(groupNum), this);
        this.updateGroupSublatticeMap(m_ClusterGroups[newGroupNumber]);
      }
    }
  }
  
  public void appendClusterGroups(CLUST clusterData) {
    
    int maxGroupNumber = clusterData.getMaxGroupNumber();
    int oldMaxGroupNumber = this.getMaxGroupNumber();
    m_ClusterGroups = (ClusterGroup[]) ArrayUtils.growArray(m_ClusterGroups, maxGroupNumber);
    ClusterGenerator clustGenerator = new ClusterGenerator(this);
    // We start with 1 so that we never append the empty group, which is always group 0
    for (int groupNum = 1; groupNum <= maxGroupNumber; groupNum++) {
      if (clusterData.containsGroup(groupNum)) {
        int newGroupNumber = oldMaxGroupNumber + groupNum;
        Coordinates[][] allCoords = clustGenerator.generateClusterOrbit(clusterData.getClusterCoordsForGroup(groupNum));
        m_ClusterGroups[newGroupNumber] = new ClusterGroup(newGroupNumber, allCoords, this);
        this.updateGroupSublatticeMap(m_ClusterGroups[newGroupNumber]);
      }
    }
  }
  
  protected boolean updateGroupSublatticeMap(ClusterGroup group) {
    int sublatticeIndex = this.getSublatticeForGroup(group);
    if (sublatticeIndex < 0) {return false;}
    m_GroupsBySublattice[sublatticeIndex] = group;
    return true;
  }
  
  /**
   * Similar to calculateFunctionValue, with two differences:
   * <br> 1) Instead of generally specifying the number of allowed states, this function requires
   * the integer index of a site in the primitive cell.
   * <br> 2) This method looks up cached values instead of calculating them anew.  This is faster,
   * and it also means that if you change the basis this method does not need to be overridden.
   * 
   * @param primSiteNumber The integer index of a site in the primitive cell on which this 
   * cluster expansion is defined.
   * @param siteState The integer state that uniquely maps to the occupancy of a site.
   * @param functionNumber The integer representing what function we want to evaluate for the site.
   * @return The cached value of the function.
   */
  public double getFunctionValue(int primSiteNumber, int siteState, int functionNumber) {
    
    return m_FunctionBasis.getFunctionValue(siteState, functionNumber, this.numAllowedSpecies(primSiteNumber));
    
  }

  /**
   * 
   * @return The number of different cluster groups known to this cluster expansion.
   */
  public int numGroups() {
    int returnValue = 0; 
    for (int groupNum = 0; groupNum <= this.getMaxGroupNumber(); groupNum++) {
      if (this.containsGroup(groupNum)) returnValue++;
    }
    return returnValue;
  }
  
  /**
   * 
   * @return Each cluster group is assigned a unique, non-negative integer identifier known
   * as the "group number".  This returns the largest group number from the groups known to this
   * cluster expansion.
   */
  public int getMaxGroupNumber() {
    return m_ClusterGroups.length -1;
  }

  /**
   * 
   * @param groupNum A unique non-negative identifier for a cluster group.
   * @return The cluster group.
   */
  public ClusterGroup getClusterGroup(int groupNum) {
    return m_ClusterGroups[groupNum];
  }
  
  /**
   * 
   * @return The cluster group that depends on no sites, or null if it does not exist.
   */
  public ClusterGroup getEmptyGroup() {
    for (int groupNum = 0; groupNum < m_ClusterGroups.length; groupNum++) {
      ClusterGroup group = m_ClusterGroups[groupNum];
      if (group != null && group.numSitesPerCluster() == 0) {
        return group;
      }
    }
    return null;
  }

  /**
   * 
   * @param groupNum A unique non-negative identifier for a cluster group.
   * @return True if this cluster expansion is aware of this group.
   */
  public boolean containsGroup(int groupNum) {
    return (m_ClusterGroups[groupNum] != null);
  }
  
  public double[][] getECI() {
    double[][] returnArray = new double[this.getMaxGroupNumber() + 1][];
    for (int groupNum = 0; groupNum < m_ClusterGroups.length; groupNum++) {
      ClusterGroup group = m_ClusterGroups[groupNum];
      if (group == null) {continue;}
      returnArray[groupNum] = new double[group.numFunctionGroups()];
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        returnArray[groupNum][functGroupNum] = group.getECI(functGroupNum);
      }
    }
    return returnArray;
  }

  /**
   * 
   * @param includeEmpty True if the "empty group" (that with no sites) should be included 
   * in the return array.  False if otherwise.
   * @return An array of all cluster groups known to this cluster expansion.
   */
  public ClusterGroup[] getAllGroups(boolean includeEmpty) {

    if (m_ClusterGroups.length == 0) {return new ClusterGroup[0];}

    int numGroups = this.numGroups();
    numGroups = ((includeEmpty && (m_ClusterGroups[0] != null )) ? numGroups : numGroups - 1);
    int startNum = (includeEmpty ? 0 : 1);

    ClusterGroup[] returnArray = new ClusterGroup[numGroups];
    int groupIndex = 0;
    for (int groupNum = startNum; groupNum < m_ClusterGroups.length; groupNum++) {
      ClusterGroup group = m_ClusterGroups[groupNum];
      if (group == null) {continue;}
      returnArray[groupIndex] = group;
      groupIndex++;
    }
    return returnArray;
  }
  
  /**
   * Assumes an orthonormal basis
   * 
   * @param cutoff
   * @return
   */
  public ClusterGroup[] getSignificantGroups(double cutoff) {
    return this.getSignificantGroups(this.getAllGroups(true), cutoff);
  } 
  
  /**
   * Assumes an orthonormal basis
   * 
   * @param cutoff
   * @return
   */
  public ClusterGroup[] getSignificantGroups(ClusterGroup[] activeGroups, double cutoff) {
    
    double cutoffSq = cutoff*cutoff;
    GroupSumSqECIComparator[] comparators = new GroupSumSqECIComparator[activeGroups.length];
    int comparatorIndex = 0;
    for (int groupIndex = 0; groupIndex < comparators.length; groupIndex++) {
      ClusterGroup group = activeGroups[groupIndex];
      if (group == null) {continue;}
      comparators[comparatorIndex] = new GroupSumSqECIComparator(group);
      comparatorIndex++;
    }
    comparators = (GroupSumSqECIComparator[]) ArrayUtils.truncateArray(comparators, comparatorIndex);
    Arrays.sort(comparators);
    
    double runningTotal = 0;
    for (comparatorIndex =0; comparatorIndex < comparators.length; comparatorIndex++) {
      runningTotal += comparators[comparatorIndex].m_SumSqECI.doubleValue();
      if (runningTotal > cutoffSq) {break;}
    }
    
    ClusterGroup[] returnArray = new ClusterGroup[comparators.length - comparatorIndex];
    for (int returnIndex = 0; returnIndex < returnArray.length; returnIndex++) {
      returnArray[returnIndex] = comparators[comparatorIndex + returnIndex].m_Group;
    }
    return returnArray;
    
  }
  
  protected class GroupSumSqECIComparator implements Comparable {
    
    protected ClusterGroup m_Group;
    protected Double m_SumSqECI;
    
    public GroupSumSqECIComparator(ClusterGroup group) {
      m_Group = group;
      double sumSqECI = 0;
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        double eci = group.getECI(functGroupNum);
        sumSqECI += eci * eci * group.numFunctionsForFunctionGroup(functGroupNum);
      }
      sumSqECI *= group.getMultiplicity();
      m_SumSqECI = new Double(sumSqECI);
    }
    
    public int compareTo(Object o) {
      GroupSumSqECIComparator comparator = (GroupSumSqECIComparator) o;
      return m_SumSqECI.compareTo(comparator.m_SumSqECI);
    }
  }
  
  /** 
   * This is used for compatability with AntonEmulator's binary code.  I'm not sure how it would 
   * be extended for higher-order cluster expansions.
   * 
   * @param er An ECIOut representing the ECI for binary cluster functions.
   * TODO Look into the general case for this, or at least confirm that this is a binary cluster expansion.
   * */
  public void setECI(ECIOut er) {
    for (int entryNum = 0; entryNum < er.numEntries(); entryNum++) {
      int clustNumber = er.getClusterOrbit(entryNum);
      ClusterGroup group = m_ClusterGroups[clustNumber];
      if (group == null) {continue;}
      int functionNumber = er.getFunctionOrbit(entryNum);
      group.setECI(functionNumber, er.getECI(entryNum));
    }
  }

  /**
   * This method is most useful for setting the values of the ECI to values calculated by 
   * some sort of fitting method.
   * 
   * It is important in this method that the eci correspond to the given groups and the order
   * of the eci correspond to the group order and the order of the functions within the groups.
   * 
   * @param eci This is an array of eci for the given groups.  The first elements in this array
   * should be the ECI for the functions in the first group, listed in the order of the functions.
   * After that should be the functions for the second group, listed in the order of the functions.
   * @param activeGroups The list of groups for which we are assigning the ECI.  It is important that
   * the given ECI are listed in an order corresponding the order of these groups.
   * TODO Account for groups of symmetrically equivalent functions within cluster groups.
   */
  public void setECI(double[] eci, ClusterGroup[] activeGroups, boolean areCorrelationCoefficients) {

    // TODO verify the scaling by multiplicity
    int totalFunctNum = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      ClusterGroup group = activeGroups[groupIndex];
      for (int functGroup = 0; functGroup < group.numFunctionGroups(); functGroup++) {
        double currentECI = eci[totalFunctNum++];
        if (areCorrelationCoefficients) {currentECI /= group.getMultiplicity();}
        group.setECI(functGroup, currentECI);
      }
    }
    
    if (totalFunctNum < eci.length) {
      Status.warning("Didn't use all provided values when setting ECI");
    }
  }
  
  public StructureList getStructureList() {
    return m_StructureList;
  }
  
  public int getSublatticeForGroup(ClusterGroup group) {
    
    if (group.numSitesPerCluster() != 1) {return -1;}
    int primSiteIndex = group.getPrimSiteIndex(0);
    return this.getSublatticeForSite(primSiteIndex);
    
  }
  
  public ClusterGroup getGroupForSublattice(int sublatticeIndex) {
    return m_GroupsBySublattice[sublatticeIndex];
  }

}
