/*
 * Created on May 9, 2006
 *
 */
package matsci.structure.decorate.function.ce.reg;

import java.util.Random;

import matsci.util.arrays.ArrayUtils;
import matsci.model.reg.ILinearRegGenerator;
import matsci.structure.decorate.function.ce.clusters.*;

public class ExponentialRegGenerator implements ILinearCERegGenerator {

  protected static Random RANDOM = new Random();
  
  protected ClusterGroup[] m_ActiveGroups;
  
  protected int[] m_Multiplicities;
  protected int[] m_NumSites;
  protected double[] m_MaxRadii;
  
  protected double m_Factor = 2;
  protected double m_MultiplicityExponent = 0;
  protected boolean m_RegEmpty = false;
  
  public ExponentialRegGenerator(ClusterGroup[] activeGroups) {
    this.setActiveGroups(activeGroups);
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroups) {
    
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroups);
    
    int maxGroupNumber = 0;
    for (int groupIndex =0; groupIndex < activeGroups.length; groupIndex++) {
      maxGroupNumber = Math.max(activeGroups[groupIndex].getGroupNumber(), maxGroupNumber);
    }
    
    //ClusterGroup[] sortedGroups = new ClusterGroup[maxGroupNumber + 1];
    int numFunctionGroups =0;
    for (int groupIndex =0; groupIndex < activeGroups.length; groupIndex++) {
      ClusterGroup group = activeGroups[groupIndex];
      //sortedGroups[group.getGroupNumber()] = group;
      numFunctionGroups += group.numFunctionGroups();
    }
    ClusterGroup[] sortedGroups = activeGroups;
    
    m_NumSites =new int[numFunctionGroups];
    m_MaxRadii = new double[numFunctionGroups];
    m_Multiplicities =new int[numFunctionGroups];
    int varIndex =0;
    for (int groupNum = 0; groupNum < sortedGroups.length; groupNum++) {
      ClusterGroup group = sortedGroups[groupNum];
      if (group == null) {continue;}
      int numSites = group.numSitesPerCluster();
      double maxRadius = group.getMaxDistance();
      int multiplicity = group.getMultiplicity();
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        m_NumSites[varIndex] = numSites;
        m_MaxRadii[varIndex] = maxRadius;
        m_Multiplicities[varIndex] = multiplicity;
        varIndex++;
      }
    }
    
  }
  
  public void regularizeEmpty(boolean value) {
    m_RegEmpty = value;
  }
  
  public static double getRegularizerForGroup(ClusterGroup group, double multPower, double[] parameters, boolean regEmpty) {
    double multScale = Math.pow(group.getMultiplicity(), multPower);
    return getRegularizer(group.numSitesPerCluster(), group.getMaxDistance(), multScale, parameters, regEmpty);
  }

  public double[] getRegularizer(double[] state, double[] template) {
    double[] returnArray = template;
    if (template == null || template.length != m_NumSites.length) {
      returnArray = new double[m_NumSites.length];
    }
    
    for (int varNum = 0; varNum < returnArray.length; varNum++) {
      int numSites = m_NumSites[varNum];
      double maxDist = m_MaxRadii[varNum];
      double multScale = Math.pow(m_Multiplicities[varNum], m_MultiplicityExponent);
      returnArray[varNum] = getRegularizer(numSites, maxDist, multScale, state, m_RegEmpty);
    }
    return returnArray;
  }
  
  protected static double getRegularizer(int numSites, double maxDist, double multScale, double[] parameters, boolean regEmpty) {
    if (numSites < 1) {return regEmpty ? parameters[0] / multScale : 0;}
    if (numSites == 1) {return parameters[0] / multScale;}
    double lambda = parameters[1];
    double alpha = parameters[2];
    double beta = parameters[3];
    return lambda * Math.pow(alpha * maxDist + 1, beta * numSites + 1) / multScale; 
  }

  public double[] changeParametersRandomly(double[] oldState) {
    double[] returnArray = ArrayUtils.copyArray(oldState);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }
  
  public void setFactor(double factor) {
    m_Factor = factor;
  }
  
  public double getFactor() {
    return m_Factor;
  }
  
  public void setMultiplicityExponent(double value) {
    m_MultiplicityExponent = value;
  }

  public int numParameters() {
    return 4;
  }

  public int numVariables() {
    return m_Multiplicities.length;
  }

  public double getMultiplicityExponent() {
    return m_MultiplicityExponent;
  }

  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }
  

}
