/*
 * Created on May 8, 2006
 *
 */
package matsci.structure.decorate.function.ce.reg;

import java.util.*;

import matsci.structure.decorate.function.ce.clusters.*;
import matsci.model.reg.ILinearRegGenerator;
import matsci.util.arrays.*;

public class IndRegGenerator implements ILinearCERegGenerator {

  protected static Random RANDOM = new Random();
  protected double m_Factor = 2;
  protected int m_NumVariables = 0;

  protected ClusterGroup[] m_ActiveGroups;
  
  protected BitSet m_UnRegVars = new BitSet();
  
  protected int m_EmptyVarNum = -1;
  protected int[] m_Multiplicities;
  protected double m_MultiplicityExponent = 0;
  
  /*public IndRegGenerator(int numVariables) {
    m_NumVariables = numVariables;
  }*/
  
  public IndRegGenerator(ClusterGroup[] activeGroupsInOrder) {
    this.setActiveGroups(activeGroupsInOrder);
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {
    
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroupsInOrder);
    
    int numVariables = 0;
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      if (group == null) {continue;}
      numVariables += group.numFunctionGroups();
    }
    m_NumVariables = numVariables;
    
    m_Multiplicities =new int[m_NumVariables];
    int varIndex =0;

    for (int groupNum = 0; groupNum < activeGroupsInOrder.length; groupNum++) {
      ClusterGroup group = activeGroupsInOrder[groupNum];
      if (group == null) {continue;}
      int multiplicity = group.getMultiplicity();
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        m_Multiplicities[varIndex] = multiplicity;
        if (group.numSitesPerCluster() == 0) {
          m_EmptyVarNum = varIndex;
        }
        varIndex++;
      }
    }
  }

  public double[] getRegularizer(double[] state, double[] returnArray) {

    if (state.length != m_NumVariables) {
      throw new RuntimeException("State doesn't correspond to number of input variables");
    }
    if (returnArray == null || returnArray.length != state.length) {
      returnArray = new double[state.length];
    }
    System.arraycopy(state, 0, returnArray, 0, state.length);
    
    for(int i = m_UnRegVars.nextSetBit(0); i >= 0; i = m_UnRegVars.nextSetBit(i+1)) {
      returnArray[i] = 0;
    }
    
    for (int varIndex = 0; varIndex < returnArray.length; varIndex++) {
      returnArray[varIndex] /= Math.pow(m_Multiplicities[varIndex], m_MultiplicityExponent);
    }
    
    return returnArray;
  }
  
  public double[] changeParametersRandomly(double[] oldState) {
    double[] returnArray = ArrayUtils.copyArray(oldState);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }
  
  public double getMultiplicityExponent() {
    return m_MultiplicityExponent;
  }
  
  public void setMultiplicityExponent(double value) {
    m_MultiplicityExponent = value;
  }
  
  public void setFactor(double factor) {
    m_Factor = factor;
  }
  
  public double getFactor() {
    return m_Factor;
  }
  
  public void regularizeVariable(int varNum, boolean value) {
    m_UnRegVars.set(varNum, !value);
  }
  
  public int numParameters() {
    return m_NumVariables;
  }

  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }
  
  public int numVariables() {
    return m_NumVariables;
  }

}
