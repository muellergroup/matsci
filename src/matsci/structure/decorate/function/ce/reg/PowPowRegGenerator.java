/*
 * Created on Mar 29, 2010
 *
 */
package matsci.structure.decorate.function.ce.reg;

import java.util.Random;

import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;

public class PowPowRegGenerator implements ILinearCERegGenerator {
  
  protected static Random RANDOM = new Random();

  protected ClusterGroup[] m_ActiveGroups;
  
  protected int[] m_Multiplicities;
  protected int[] m_NumSites;
  protected double[] m_MaxRadii;
  
  protected double m_Factor = 2;
  protected double m_MultiplicityExponent = 0;
  
  public PowPowRegGenerator(ClusterGroup[] activeGroupsInOrder) {
    this.setActiveGroups(activeGroupsInOrder);
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {

    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroupsInOrder);

    int maxGroupNumber = 0;
    int numFunctionGroups = 0;
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      maxGroupNumber = Math.max(group.getGroupNumber(), maxGroupNumber);
      numFunctionGroups += group.numFunctionGroups();
    }
    
    m_NumSites =new int[numFunctionGroups];
    m_MaxRadii = new double[numFunctionGroups];
    m_Multiplicities =new int[numFunctionGroups];
    int varIndex =0;
    for (int groupNum = 0; groupNum < activeGroupsInOrder.length; groupNum++) {
      ClusterGroup group = activeGroupsInOrder[groupNum];
      if (group == null) {continue;}
      int numSites = group.numSitesPerCluster();
      double maxRadius = group.getMaxDistance();
      int multiplicity = group.getMultiplicity();
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        m_NumSites[varIndex] = numSites;
        m_MaxRadii[varIndex] = maxRadius;
        m_Multiplicities[varIndex] = multiplicity;
        varIndex++;
      }
    }
  }
  
  public static double getRegularizerForGroup(ClusterGroup group, double multPower, double[] parameters) {
    double multScale = Math.pow(group.getMultiplicity(), multPower);
    return getRegularizer(group.numSitesPerCluster(), group.getMaxDistance(), multScale, parameters);
  }

  public double[] getRegularizer(double[] state, double[] template) {
    double[] returnArray = template;
    if (template == null || template.length != m_NumSites.length) {
      returnArray = new double[m_NumSites.length];
    }
    
    for (int clustNum = 0; clustNum < returnArray.length; clustNum++) {
      int numSites = m_NumSites[clustNum];
      double maxDist = m_MaxRadii[clustNum];
      double multScale = Math.pow(m_Multiplicities[clustNum], m_MultiplicityExponent);
      returnArray[clustNum] = getRegularizer(numSites, maxDist, multScale, state);
    }
    return returnArray;
  }
  
  protected static double getRegularizer(int numSites, double maxDist, double multScale, double[] parameters) {
    if (numSites == 0) {return 0;}
    if (numSites == 1) {return parameters[0];}
    double lambda = parameters[1];
    double distScale = parameters[2];
    //double distOffset = parameters[3];
    double sitesScale =parameters[3];
    //double sitesOffset = parameters[5];
    return lambda * Math.pow(1+maxDist, distScale) * Math.pow(numSites, sitesScale) / multScale; 
  }

  public double[] changeParametersRandomly(double[] oldState) {
    double[] returnArray = ArrayUtils.copyArray(oldState);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }
  
  public void setFactor(double factor) {
    m_Factor = factor;
  }
  
  public double getFactor() {
    return m_Factor;
  }
  
  public void setMultiplicityExponent(double value) {
    m_MultiplicityExponent = value;
  }
  
  public int numParameters() {
    return 4;
  }
  
  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }
  
  public int numVariables() {
    return m_Multiplicities.length;
  }

  public double getMultiplicityExponent() {
    return m_MultiplicityExponent;
  }

}
