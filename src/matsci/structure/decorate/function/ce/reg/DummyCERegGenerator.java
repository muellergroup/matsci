/*
 * Created on Nov 18, 2010
 *
 */
package matsci.structure.decorate.function.ce.reg;

import java.util.Arrays;

import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;

public class DummyCERegGenerator implements ILinearCERegGenerator {

  private ClusterGroup[] m_ActiveGroups;
  private int m_NumVariables;
  private double m_MultiplicityExponent = 0;
  
  public DummyCERegGenerator(ClusterGroup[] activeGroups) {
    this.setActiveGroups(activeGroups);
  }
  
  public double[] getRegularizer(double[] parameters, double[] template) {
    int numVariables = this.numVariables();
    if (template == null || template.length != numVariables) {
      return new double[this.numVariables()];
    }
    Arrays.fill(template, 0);
    return template;
  }

  public int numVariables() {
    return m_NumVariables;
  }

  public double[] changeParametersRandomly(double[] oldParameters) {
    return oldParameters;
  }

  public int numParameters() {
    return 0;
  }

  public double getMultiplicityExponent() {
    return m_MultiplicityExponent;
  }

  public void setMultiplicityExponent(double value) {
    m_MultiplicityExponent = value;
  }

  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }

  public void setActiveGroups(ClusterGroup[] activeGroups) {
    
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroups);
    
    int numVariables = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      ClusterGroup group = activeGroups[groupIndex];
      if (group == null) {continue;}
      numVariables += group.numFunctionGroups();
    }
    m_NumVariables = numVariables;
    
  }

}
