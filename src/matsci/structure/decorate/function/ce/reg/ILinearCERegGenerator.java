/*
 * Created on Aug 10, 2007
 *
 */
package matsci.structure.decorate.function.ce.reg;

import matsci.model.reg.ILinearRegGenerator;

public interface ILinearCERegGenerator extends ILinearRegGenerator, ICERegGenerator {

}
