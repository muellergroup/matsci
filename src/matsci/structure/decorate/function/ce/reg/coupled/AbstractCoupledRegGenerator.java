/*
 * Created on Aug 16, 2007
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import java.util.Arrays;
import java.util.Random;

import matsci.Species;
import matsci.location.symmetry.CoordSetMapper;
import matsci.model.reg.ILinearCovRegGenerator;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ICERegGenerator;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;
import matsci.util.arrays.ArrayUtils;

public abstract class AbstractCoupledRegGenerator implements ILinearCovRegGenerator, ICERegGenerator {
  
  private ILinearCERegGenerator m_LinearRegGenerator;
  protected ClusterGroup[] m_ActiveGroups = new ClusterGroup[0];
  
  private double[] m_Multiplicities;
  private int[] m_VarGroupMap;
  private int m_NumClusterVars = 0;
  
  protected static Random RANDOM = new Random();
  double m_Factor = 2;
  boolean m_RegEmpty = false;
  int m_EmptyVarNum = -1;
  
  private double m_MultiplicityExponent = 2;
  
  public AbstractCoupledRegGenerator(ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    m_LinearRegGenerator = regGenerator;
    m_MultiplicityExponent = regGenerator.getMultiplicityExponent();
    regGenerator.setMultiplicityExponent(0);
    this.setActiveGroups(activeGroupsInOrder);
  }

  public double[][] getRegularizer(double[] parameters, double[][] returnArray) {
    
    int numVariables = this.numVariables();
    if (returnArray == null || returnArray.length != numVariables) {
      returnArray = new double[numVariables][numVariables];
    }
    
    double multScale = m_MultiplicityExponent / 2.0;
    
    double[] linearLambda = this.getLinearLambda(parameters);
    double[] selfSigmaSq = new double[linearLambda.length];
    for (int rowNum = 0; rowNum < numVariables; rowNum++) {
      selfSigmaSq[rowNum] = 1 / linearLambda[rowNum];
      if (m_RegEmpty || rowNum != m_EmptyVarNum) {
        double mult1 = m_Multiplicities[rowNum];
        returnArray[rowNum][rowNum] += linearLambda[rowNum] / (Math.pow(mult1 * mult1, multScale));
      }
    }
    
    for (int rowNum = 0; rowNum < m_NumClusterVars; rowNum++) {
      double mult1 = m_Multiplicities[rowNum];
      //double sigmaSqPair = selfSigmaSq[rowNum] * coupledScaleFactor;
      for (int colNum = 0; colNum < rowNum; colNum++) {
        double coupledSigmaSq = this.getCoupledSigmaSq(rowNum, colNum, parameters, selfSigmaSq);
        double mult2 = m_Multiplicities[colNum];
        returnArray[rowNum][rowNum] += (1.0 / (coupledSigmaSq * Math.pow(mult1 * mult1, multScale)));
        returnArray[colNum][colNum] += (1.0 / (coupledSigmaSq * Math.pow(mult2 * mult2, multScale)));
        returnArray[rowNum][colNum] -= (1.0 / (coupledSigmaSq * Math.pow(mult1 * mult2, multScale)));
        returnArray[colNum][rowNum] -= (1.0 / (coupledSigmaSq * Math.pow(mult2 * mult1, multScale)));
      }
    }
    
    /* double[] selfSigmaSq = this.getLinearSigmaSq(parameters);
     * for (int rowNum = 0; rowNum < numVariables; rowNum++) {
      double[] returnRow = returnArray[rowNum];
      double mult1 = m_Multiplicities[rowNum];
      if (m_RegEmpty || rowNum != m_EmptyVarNum) {
        returnRow[rowNum] += 1.0 / (selfSigmaSq[rowNum] * Math.pow(mult1 * mult1, multScale));
      }
      //double sigmaSqPair = selfSigmaSq[rowNum] * coupledScaleFactor;
      for (int colNum = 0; colNum < rowNum; colNum++) {
        double coupledSigmaSq = this.getCoupledSigmaSq(rowNum, colNum, parameters, selfSigmaSq);
        double mult2 = m_Multiplicities[colNum];
        returnArray[rowNum][rowNum] += (1.0 / (coupledSigmaSq * Math.pow(mult1 * mult1, multScale)));
        returnArray[colNum][colNum] += (1.0 / (coupledSigmaSq * Math.pow(mult2 * mult2, multScale)));
        returnArray[rowNum][colNum] -= (1.0 / (coupledSigmaSq * Math.pow(mult1 * mult2, multScale)));
        returnArray[colNum][rowNum] -= (1.0 / (coupledSigmaSq * Math.pow(mult2 * mult1, multScale)));
      }
    }*/
    
    /*for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
      for (int colNum = 0; colNum < returnArray[rowNum].length; colNum++) {
        if (Double.isNaN(returnArray[rowNum][colNum])) {
          System.currentTimeMillis();
        }
      }
    }*/
    
    return returnArray;
  }
  
  public double[] changeParametersRandomly(double[] oldParameters) {
    double[] returnArray = ArrayUtils.copyArray(oldParameters);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    if (indexToChange >= m_LinearRegGenerator.numParameters()) {
      double exponent = (RANDOM.nextDouble() * 2 - 1);
      returnArray[indexToChange] *= Math.pow(this.getScaleFactor(), exponent);
    } else {
      double[] baseParams = ArrayUtils.truncateArray(returnArray, m_LinearRegGenerator.numParameters());
      baseParams = m_LinearRegGenerator.changeParametersRandomly(baseParams);
      System.arraycopy(baseParams, 0, returnArray, 0, baseParams.length);
    }
    return returnArray;
  }
  
  public int numVariables() {
    return m_LinearRegGenerator.numVariables();
  }

  public void setMultiplicityExponent(double value) {
    m_MultiplicityExponent = value;
  }
  
  public double getMultiplicityExponent() {
    return m_MultiplicityExponent;
  }
  
  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }
  
  public int getGroupForVar(int varIndex) {
    return m_VarGroupMap[varIndex];
  }
  
  public int getEmptyGroupVarIndex() {
    return ArrayUtils.findIndex(m_VarGroupMap, 0);
  }
  
  public int getFunctGroupForVar(int varIndex) {
    int groupIndex = m_VarGroupMap[varIndex];
    ClusterGroup group = m_ActiveGroups[groupIndex];
    
    for (int functGroup = 0; functGroup < group.numFunctionGroups(); functGroup++) {
      varIndex--;
      if (varIndex < 0) {return functGroup;}
      if (m_VarGroupMap[varIndex] != groupIndex) {return functGroup;}
    }
    
    throw new RuntimeException("Error in variable-orbit index map.");
  }
  
  public int getVarIndex(int groupIndex, int functGroupNum) {
    
    for (int varIndex = 0; varIndex < m_VarGroupMap.length; varIndex++) {
      if (m_VarGroupMap[varIndex] == groupIndex) {
        return groupIndex + functGroupNum;
      }
    }
    
    return -1;
    
  }

  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {
    
    if (Arrays.equals(m_ActiveGroups, activeGroupsInOrder)) {return;}
    m_LinearRegGenerator.setActiveGroups(activeGroupsInOrder);
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroupsInOrder);

    int numVariables = m_LinearRegGenerator.numVariables();
    m_VarGroupMap = new int[numVariables];
    m_Multiplicities = new double[numVariables];
    
    int varIndex = 0;
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      if (group.numSitesPerCluster() == 0) {
        m_EmptyVarNum = varIndex;
      }
      for (int functGroup = 0; functGroup < group.numFunctionGroups(); functGroup++) {
        m_VarGroupMap[varIndex] = groupIndex;
        m_Multiplicities[varIndex++] = group.getMultiplicity();
      }
    }
    m_NumClusterVars = varIndex;
  
  }
  
  /*protected double[] getLinearSigmaSq(double[] parameters) {
    double[] selfParams = new double[m_LinearRegGenerator.numParameters()];
    System.arraycopy(parameters, 0, selfParams, 0, selfParams.length);
    double[] lambdaArray = m_LinearRegGenerator.getRegularizer(selfParams, null);
    for (int regNum = 0; regNum < lambdaArray.length; regNum++) {
      lambdaArray[regNum] = 1.0 / lambdaArray[regNum];  // Convert to sigmaSq
    }
    return lambdaArray;
  }*/
  
  protected double[] getLinearLambda(double[] parameters) {
    double[] selfParams = new double[m_LinearRegGenerator.numParameters()];
    System.arraycopy(parameters, 0, selfParams, 0, selfParams.length);
    return m_LinearRegGenerator.getRegularizer(selfParams, null);
  }
  
  public void regularizeEmpty(boolean value) {
    m_RegEmpty = value;
  }
  
  public void setScaleFactor(double value) {
    m_Factor = value;
  }
  
  public double getScaleFactor() {
    return m_Factor;
  }
  
  public ILinearCERegGenerator getLinearRegGenerator() {
    return m_LinearRegGenerator;
  }
  
  public abstract double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq);
  public abstract int numParameters();

}
