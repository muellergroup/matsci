/*
 * Created on Oct 20, 2008
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import java.util.Arrays;

import matsci.location.*;
import matsci.Species;
import matsci.structure.decorate.function.ce.*;
import matsci.structure.*;
import matsci.location.symmetry.CoordSetMapper;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;
import matsci.util.arrays.*;

public class LayeredBulkCoupledRegGenerator extends AbstractCoupledRegGenerator {

  private boolean[][] m_CongruentGroups;
  private double[] m_DistancesFromSurface;
  private ClusterExpansion m_TemplateCE;
  
  public LayeredBulkCoupledRegGenerator(ClusterExpansion templateCE, ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    super(regGenerator, activeGroupsInOrder);
    m_TemplateCE = templateCE;
    this.setSurfaceDistances(activeGroupsInOrder);
  }

  public double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq) {
    int group1index = this.getGroupForVar(var1);
    int group2index = this.getGroupForVar(var2);
    if (!m_CongruentGroups[group1index][group2index]) {return Double.POSITIVE_INFINITY;}
    double scaleFactor1 = this.getBulkScaleFactor(group1index, parameters);
    double scaleFactor2 = this.getBulkScaleFactor(group2index, parameters);
    
    double layeredFactor = this.getLayeredFactor(group1index, group2index, parameters);
    double totalFactor = 1 / (1 / (scaleFactor1 + scaleFactor2) + layeredFactor);
    
    return selfSigmaSq[var1] * totalFactor ;
  }

  public int numParameters() {
    return this.getLinearRegGenerator().numParameters() + 3;
  }
  
  protected double getLayeredFactor(int group1Index, int group2Index, double[] parameters) {
    double dist1 = m_DistancesFromSurface[group1Index];
    double dist2 = m_DistancesFromSurface[group2Index];
    double tolerance = 1E-2;
    if (Math.abs(dist1 - dist2) > tolerance) {
      return 0;
    }
    int numSelfParams = this.getLinearRegGenerator().numParameters();
    return parameters[numSelfParams + 2];
  }
  
  protected double getBulkScaleFactor(int groupIndex, double[] parameters) {
    double distance = m_DistancesFromSurface[groupIndex];
    int numSelfParams = this.getLinearRegGenerator().numParameters();
    double surfMag = parameters[numSelfParams];
    double surfDecay = parameters[numSelfParams + 1];
    return Math.exp(-surfDecay*(distance)) / surfMag;
  }

  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {

    if (Arrays.equals(m_ActiveGroups, activeGroupsInOrder)) {return;}
    super.setActiveGroups(activeGroupsInOrder);
    
    m_CongruentGroups = new boolean[activeGroupsInOrder.length][activeGroupsInOrder.length];
    
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];

      CoordSetMapper mapper = new CoordSetMapper(group.getSampleCoords());
      m_CongruentGroups[groupIndex][groupIndex] = true;
      for (int prevGroupIndex = 0; prevGroupIndex < groupIndex; prevGroupIndex++) {
        ClusterGroup prevGroup = activeGroupsInOrder[prevGroupIndex];
        if (prevGroup.numSitesPerCluster() != group.numSitesPerCluster()) {continue;}
        int[][] maps = mapper.mapCoordinates(prevGroup.getSampleCoords(), false);
        boolean match = (maps.length > 0);
        for (int mapNum = 0; mapNum < maps.length; mapNum++) {
          int[] map = maps[mapNum];
          for (int siteNum = 0; siteNum < map.length; siteNum++) {
            Species[] allowedSpecies = group.getAllowedSpecies(siteNum);
            Species[] prevAllowedSpecies = prevGroup.getAllowedSpecies(map[siteNum]);
            match = Arrays.equals(allowedSpecies, prevAllowedSpecies);
            if (!match) {break;}
          }
          if (!match) {break;}
        }
        if (!match) {continue;}
        m_CongruentGroups[prevGroupIndex][groupIndex] = true;
        m_CongruentGroups[groupIndex][prevGroupIndex] = true;
      }
    }
    
    this.setSurfaceDistances(activeGroupsInOrder);
  }
  
  protected void setSurfaceDistances(ClusterGroup[] activeGroupsInOrder) {
    
    if (m_TemplateCE == null) {return;}
    m_DistancesFromSurface = new double[activeGroupsInOrder.length];
    if (activeGroupsInOrder.length == 0) {return;}
    
    Structure bulkStructure = m_TemplateCE.getBaseStructure();
    ClusterExpansion ce = activeGroupsInOrder[0].getClusterExpansion();
    Structure primStructure = ce.getBaseStructure();
    
    BravaisLattice primLattice = primStructure.getDefiningLattice();
    if (primLattice.numNonPeriodicVectors() == 0) {
      Arrays.fill(m_DistancesFromSurface, Double.POSITIVE_INFINITY);
      return;
    }
    
    Coordinates[] projSurfaceCoords = new Coordinates[primStructure.numDefiningSites()];
    Coordinates[] projDefiningCoords =new Coordinates[primStructure.numDefiningSites()];
    int surfaceIndex = 0;
    for (int siteIndex = 0; siteIndex < primStructure.numDefiningSites(); siteIndex++) {
      Structure.Site definingSite = primStructure.getDefiningSite(siteIndex);
      Coordinates projCoords = primLattice.removeLattice(definingSite.getCoords());
      projDefiningCoords[siteIndex] = projCoords;
      Structure.Site[] nearestNeighbors = primStructure.getNearestNeighbors(definingSite);
      Structure.Site bulkSite = bulkStructure.getDefiningSite(definingSite.getCoords());
      if (bulkSite == null) {
        projSurfaceCoords[surfaceIndex++] = projCoords;
        continue;
      } 
      Structure.Site[] bulkNeighbors = bulkStructure.getNearestNeighbors(bulkSite);
      if (bulkNeighbors.length > nearestNeighbors.length) {
        projSurfaceCoords[surfaceIndex++] = projCoords;
      }
    }
    projSurfaceCoords = (Coordinates[]) ArrayUtils.truncateArray(projSurfaceCoords, surfaceIndex);
    double[] surfDistances = new double[projDefiningCoords.length];
    for (int siteNum = 0; siteNum < projDefiningCoords.length; siteNum++) {
      double surfDistance = Double.POSITIVE_INFINITY;
      Coordinates projCoords = projDefiningCoords[siteNum];
      for (int surfSiteNum = 0; surfSiteNum < projSurfaceCoords.length; surfSiteNum++) {
        surfDistance = Math.min(surfDistance, projCoords.distanceFrom(projSurfaceCoords[surfSiteNum]));
      }
      surfDistances[siteNum] = surfDistance;
    }
    
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      double groupDistance = Double.POSITIVE_INFINITY;
      for (int siteNum = 0; siteNum < group.numSitesPerCluster(); siteNum++) {
        int siteIndex = group.getPrimSiteIndex(siteNum);
        groupDistance = Math.min(groupDistance, surfDistances[siteIndex]);
      }
      m_DistancesFromSurface[groupIndex] = groupDistance;
    }
  }

}
