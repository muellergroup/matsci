/*
 * Created on Feb 4, 2008
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;

public class CoarseCoupledRegGenerator extends CongruenceCoupledRegGenerator {

  public CoarseCoupledRegGenerator(ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    super(regGenerator, activeGroupsInOrder);
  }

  public double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq) {
    int group1index = this.getGroupForVar(var1);
    int group2index = this.getGroupForVar(var2);
    
    int functGroupIndex1 = this.getFunctGroupForVar(var1);
    int functGroupIndex2 = this.getFunctGroupForVar(var2);
    
    if (!this.areGroupsCongruent(group1index, group2index, functGroupIndex1, functGroupIndex2)) {return Double.POSITIVE_INFINITY;}
    return 1E-7;
  }

  public int numParameters() {
    return this.getLinearRegGenerator().numParameters();
  }

}
