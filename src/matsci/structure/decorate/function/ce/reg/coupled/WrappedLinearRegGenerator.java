/*
 * Created on May 26, 2010
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;

public class WrappedLinearRegGenerator extends AbstractCoupledRegGenerator {

  public WrappedLinearRegGenerator(ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    super(regGenerator, activeGroupsInOrder);
  }

  @Override
  public double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq) {
    return Double.POSITIVE_INFINITY;
  }

  @Override
  public int numParameters() {
    return this.getLinearRegGenerator().numParameters();
  }

}
