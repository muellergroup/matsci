/*
 * Created on May 4, 2009
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import java.util.Arrays;
import java.util.Random;

import matsci.*;
import matsci.location.*;
import matsci.location.symmetry.*;
import matsci.model.reg.*;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;
import matsci.util.arrays.*;

public class ProximityCongruentCoupledRegGenerator extends AbstractCoupledRegGenerator {
  
  private boolean[][] m_CongruentGroups;
  private double[][] m_CongruentGroupDistances;
  
  public ProximityCongruentCoupledRegGenerator(ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    super(regGenerator, activeGroupsInOrder);
  }
  
  public double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq) {
    int group1index = this.getGroupForVar(var1);
    int group2index = this.getGroupForVar(var2);
    if (!m_CongruentGroups[group1index][group2index]) {return Double.POSITIVE_INFINITY;}
    double distance = m_CongruentGroupDistances[group1index][group2index];
    double distScale = parameters[parameters.length - 2];
    double scaleFactor = Math.pow(distance, distScale) / parameters[parameters.length - 1];
    //double scaleFactor = distance / parameters[parameters.length - 1];
    //double scaleFactor = 1 / distScale + distance / parameters[parameters.length - 1];
    return selfSigmaSq[var1] * scaleFactor;
  }

  public double[] changeParametersRandomly(double[] oldParameters) {
    double[] returnArray = ArrayUtils.copyArray(oldParameters);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }

  public int numParameters() {
    return this.getLinearRegGenerator().numParameters() + 2;
  }
  
  protected boolean areGroupsCongruent(int group1Index, int group2Index) {
    return m_CongruentGroups[group1Index][group2Index];
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {

    if (Arrays.equals(m_ActiveGroups, activeGroupsInOrder)) {return;}
    super.setActiveGroups(activeGroupsInOrder);
    
    m_CongruentGroups = new boolean[activeGroupsInOrder.length][activeGroupsInOrder.length];
    m_CongruentGroupDistances = new double[activeGroupsInOrder.length][activeGroupsInOrder.length];
    
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      Coordinates[] sampleCoords = group.getSampleCoords();
      CoordSetMapper mapper = new CoordSetMapper(sampleCoords);
      m_CongruentGroups[groupIndex][groupIndex] = true;
      m_CongruentGroupDistances[groupIndex][groupIndex] = 0;
      for (int prevGroupIndex = 0; prevGroupIndex < groupIndex; prevGroupIndex++) {
        ClusterGroup prevGroup = activeGroupsInOrder[prevGroupIndex];
        if (prevGroup.numSitesPerCluster() != group.numSitesPerCluster()) {continue;}
        int[][] maps = mapper.mapCoordinates(prevGroup.getSampleCoords(), false);
        boolean match = (maps.length > 0);
        for (int mapNum = 0; mapNum < maps.length; mapNum++) {
          int[] map = maps[mapNum];
          for (int siteNum = 0; siteNum < map.length; siteNum++) {
            Species[] allowedSpecies = group.getAllowedSpecies(siteNum);
            Species[] prevAllowedSpecies = prevGroup.getAllowedSpecies(map[siteNum]);
            match = Arrays.equals(allowedSpecies, prevAllowedSpecies);
            if (!match) {break;}
          }
          if (!match) {break;}
        }
        if (!match) {continue;}
        m_CongruentGroups[prevGroupIndex][groupIndex] = true;
        m_CongruentGroups[groupIndex][prevGroupIndex] = true;
        
        double minDistance = Double.POSITIVE_INFINITY;
        for(int clustNum = 0; clustNum < prevGroup.numPrimClusters(); clustNum++) {
          double avgDistance = 0;
          Coordinates[] prevCoords = prevGroup.getPrimClusterCoords(clustNum);
          for (int coordNum = 0; coordNum < sampleCoords.length; coordNum++) {
            for (int prevCoordNum = 0; prevCoordNum < prevCoords.length; prevCoordNum++) {
              double distance = sampleCoords[coordNum].distanceFrom(prevCoords[prevCoordNum]);
              avgDistance += distance;
            }
          }
          avgDistance /= (sampleCoords.length * sampleCoords.length);
          minDistance = Math.min(minDistance, avgDistance);
        }
        
        m_CongruentGroupDistances[prevGroupIndex][groupIndex] = minDistance;
        m_CongruentGroupDistances[groupIndex][prevGroupIndex] = minDistance;
        
      }
    }
  }

}
