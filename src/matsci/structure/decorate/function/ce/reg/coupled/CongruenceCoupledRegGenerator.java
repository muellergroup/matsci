/*
 * Created on Aug 10, 2007
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import java.util.Arrays;
import java.util.Random;

import matsci.*;
import matsci.location.symmetry.*;
import matsci.model.reg.*;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;
import matsci.util.arrays.*;

public class CongruenceCoupledRegGenerator extends AbstractCoupledRegGenerator {
  
  private boolean[][][][] m_CongruentGroups;
  
  public CongruenceCoupledRegGenerator(ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    super(regGenerator, activeGroupsInOrder);
  }
  
  public double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq) {
    int group1index = this.getGroupForVar(var1);
    int group2index = this.getGroupForVar(var2);
    int functGroup1Index = this.getFunctGroupForVar(var1);
    int functGroup2Index = this.getFunctGroupForVar(var2);
    if (!m_CongruentGroups[group1index][group2index][functGroup1Index][functGroup2Index]) {return Double.POSITIVE_INFINITY;}
    double scaleFactor = 1 / parameters[this.numParameters() - 1]; // Better to use numParameters than parameters.length in case wrong number of parameters are passed in -- it will now throw an exception.
    return selfSigmaSq[var1] * scaleFactor;
    //return Double.POSITIVE_INFINITY;
  }

  public double[] changeParametersRandomly(double[] oldParameters) {
    double[] returnArray = ArrayUtils.copyArray(oldParameters);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }

  public int numParameters() {
    return this.getLinearRegGenerator().numParameters() + 1;
  }
  
  protected boolean areGroupsCongruent(int group1Index, int group2Index, int functGroup1Index, int functGroup2Index) {
    return m_CongruentGroups[group1Index][group2Index][functGroup1Index][functGroup2Index];
  }
  
  protected boolean areGroupsCongruent(int group1Index, int group2Index) {
    boolean[][] functGroupMaps = m_CongruentGroups[group1Index][group2Index];
    for (int functGroupNum = 0; functGroupNum < functGroupMaps.length; functGroupNum++) {
      if (ArrayUtils.arrayContains(functGroupMaps[functGroupNum], true)) {
        return true;
      }
    }
    return false;
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {

    if (Arrays.equals(m_ActiveGroups, activeGroupsInOrder)) {return;}
    super.setActiveGroups(activeGroupsInOrder);
    
    m_CongruentGroups = new boolean[activeGroupsInOrder.length][activeGroupsInOrder.length][][];
    
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];

      CoordSetMapper mapper = new CoordSetMapper(group.getSampleCoords());
      m_CongruentGroups[groupIndex][groupIndex] = new boolean[group.numFunctionGroups()][group.numFunctionGroups()];
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        Arrays.fill(m_CongruentGroups[groupIndex][groupIndex][functGroupNum], true); 
      }
      for (int prevGroupIndex = 0; prevGroupIndex < groupIndex; prevGroupIndex++) {
        ClusterGroup prevGroup = activeGroupsInOrder[prevGroupIndex];
        m_CongruentGroups[groupIndex][prevGroupIndex] = new boolean[group.numFunctionGroups()][prevGroup.numFunctionGroups()];
        m_CongruentGroups[prevGroupIndex][groupIndex] = new boolean[prevGroup.numFunctionGroups()][group.numFunctionGroups()];
        
        if (prevGroup.numSitesPerCluster() != group.numSitesPerCluster()) {continue;}
        
        int[][] maps = mapper.mapCoordinates(prevGroup.getSampleCoords(), false);
        if (maps.length == 0) {continue;}
        
        for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
          int[][] siteFunctions = group.getSiteFunctionsForFunctionGroup(functGroupNum);
          for (int prevFunctGroupNum = 0; prevFunctGroupNum < prevGroup.numFunctionGroups(); prevFunctGroupNum++) {
 
            boolean match = false;
            int[][] prevSiteFunctions = prevGroup.getSiteFunctionsForFunctionGroup(prevFunctGroupNum);
            for (int mapNum = 0; mapNum < maps.length; mapNum++) {
              int[] map = maps[mapNum];
              if (!allowedSpeciesMatch(group, prevGroup, map)) {continue;}
              if (!siteFunctionsMatch(siteFunctions, prevSiteFunctions, map)) {continue;}
              match = true;
              break;
            }
            if (!match) {continue;}
            m_CongruentGroups[prevGroupIndex][groupIndex][prevFunctGroupNum][functGroupNum] = true;
            m_CongruentGroups[groupIndex][prevGroupIndex][functGroupNum][prevFunctGroupNum] = true;
          }
        }
      }
    }
  }
  
  protected static boolean allowedSpeciesMatch(ClusterGroup group1, ClusterGroup group2, int[] map) {
    for (int siteNum = 0; siteNum < map.length; siteNum++) {
      Species[] allowedSpecies = group1.getAllowedSpecies(siteNum);
      Species[] allowedSpecies2 = group2.getAllowedSpecies(map[siteNum]);
      if (!Arrays.equals(allowedSpecies, allowedSpecies2)) {
        return false;
      }
    }
    return true;
  }
  
  protected static boolean siteFunctionsMatch(int[][] siteFunctions1, int[][] siteFunctions2, int[] map) {

    for (int functSetNum = 0; functSetNum < siteFunctions1.length; functSetNum++) {
      int[] functSet = siteFunctions1[functSetNum];
      boolean match = false;
      for (int functSet2Num = 0; functSet2Num < siteFunctions2.length; functSet2Num++) {
        int[] functSet2 = siteFunctions2[functSet2Num];
        for (int siteNum = 0; siteNum < map.length; siteNum++) {
          match = (functSet[siteNum] == functSet2[map[siteNum]]);
          if (!match) {break;}
        }
        if (match) {return true;}
      }
    }
    return false;
  }

}
