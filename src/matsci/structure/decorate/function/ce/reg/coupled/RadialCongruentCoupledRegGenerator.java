/*
 * Created on May 22, 2009
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import java.util.Arrays;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.symmetry.CoordSetMapper;
import matsci.structure.BravaisLattice;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;
import matsci.util.arrays.ArrayUtils;

public class RadialCongruentCoupledRegGenerator extends AbstractCoupledRegGenerator {
  
  private boolean[][] m_CongruentGroups;
  private double[] m_GroupDistancesFromCenter;
  
  public RadialCongruentCoupledRegGenerator(ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    super(regGenerator, activeGroupsInOrder);
  }
  
  public double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq) {
    int group1index = this.getGroupForVar(var1);
    int group2index = this.getGroupForVar(var2);
    if (!m_CongruentGroups[group1index][group2index]) {return Double.POSITIVE_INFINITY;}
    double distance1 = m_GroupDistancesFromCenter[group1index];
    double distance2 = m_GroupDistancesFromCenter[group2index];
    
    double minPossibleDist = parameters[parameters.length - 3];
    double minCouple = 1 / parameters[parameters.length - 2];

    double maxDist = Math.max(distance1, distance2) + minPossibleDist;
    double minDist = Math.min(distance1, distance2) + minPossibleDist;
    
    double couple = minCouple + maxDist / minDist;
    
    double energyScale = parameters[parameters.length - 1];
    double scaleFactor = couple / energyScale;
    //double scaleFactor = distance / parameters[parameters.length - 1];
    //double scaleFactor = 1 / distScale + distance / parameters[parameters.length - 1];
    return selfSigmaSq[var1] * scaleFactor;
  }

  public double[] changeParametersRandomly(double[] oldParameters) {
    double[] returnArray = ArrayUtils.copyArray(oldParameters);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }

  public int numParameters() {
    return this.getLinearRegGenerator().numParameters() + 3;
  }
  
  protected boolean areGroupsCongruent(int group1Index, int group2Index) {
    return m_CongruentGroups[group1Index][group2Index];
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {

    if (Arrays.equals(m_ActiveGroups, activeGroupsInOrder)) {return;}
    super.setActiveGroups(activeGroupsInOrder);
    
    m_CongruentGroups = new boolean[activeGroupsInOrder.length][activeGroupsInOrder.length];
    m_GroupDistancesFromCenter = new double[activeGroupsInOrder.length];
    
    if (activeGroupsInOrder.length == 0) {
      return;
    }
    
    BravaisLattice lattice = activeGroupsInOrder[0].getClusterExpansion().getBaseStructure().getDefiningLattice();
    Coordinates centerCoords = new Coordinates(new double[] {0.5, 0.5, 0.5}, lattice.getLatticeBasis());
    Coordinates offLatticeCenter = lattice.removeLattice(centerCoords);

    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      Coordinates[] sampleCoords = group.getSampleCoords();
      CoordSetMapper mapper = new CoordSetMapper(sampleCoords);
      m_CongruentGroups[groupIndex][groupIndex] = true;
      for (int prevGroupIndex = 0; prevGroupIndex < groupIndex; prevGroupIndex++) {
        ClusterGroup prevGroup = activeGroupsInOrder[prevGroupIndex];
        if (prevGroup.numSitesPerCluster() != group.numSitesPerCluster()) {continue;}
        int[][] maps = mapper.mapCoordinates(prevGroup.getSampleCoords(), false);
        boolean match = (maps.length > 0);
        for (int mapNum = 0; mapNum < maps.length; mapNum++) {
          int[] map = maps[mapNum];
          for (int siteNum = 0; siteNum < map.length; siteNum++) {
            Species[] allowedSpecies = group.getAllowedSpecies(siteNum);
            Species[] prevAllowedSpecies = prevGroup.getAllowedSpecies(map[siteNum]);
            match = Arrays.equals(allowedSpecies, prevAllowedSpecies);
            if (!match) {break;}
          }
          if (!match) {break;}
        }
        if (!match) {continue;}
        m_CongruentGroups[prevGroupIndex][groupIndex] = true;
        m_CongruentGroups[groupIndex][prevGroupIndex] = true;
        
      }
      
      double avgDistance = 0;
      for (int coordNum = 0; coordNum < sampleCoords.length; coordNum++) {
        Coordinates offLatticeCoords = lattice.removeLattice(sampleCoords[coordNum]);
        double distance = offLatticeCoords.distanceFrom(offLatticeCenter);
        avgDistance += distance;
      }
      avgDistance /= sampleCoords.length;
      m_GroupDistancesFromCenter[groupIndex] = avgDistance;
      
    }
  }

}
