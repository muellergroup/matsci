/*
 * Created on August 18, 2009
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import java.util.Arrays;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.symmetry.CoordSetMapper;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;
import matsci.util.arrays.ArrayUtils;

public class RadialAngleSurfaceRegGenerator extends AbstractCoupledRegGenerator {
  
  private boolean[][] m_CongruentGroups;
  private double[] m_GroupDistancesFromCenter;
  private double[] m_GroupDistancesFromSurface;
  private double[][] m_MinGroupAngles;
  
  private Coordinates[] m_OffLatticeSurfaceCoordinates;
  
  public RadialAngleSurfaceRegGenerator(ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    super(regGenerator, activeGroupsInOrder);
  }
  
  protected Coordinates[] findSurfaceCoordinates(Structure primStructure) {
    
    double minNeighborDist = Double.POSITIVE_INFINITY;
    int maxNearestNeighbors = 0;
    
    BravaisLattice lattice =primStructure.getDefiningLattice();
    
    for (int siteNum = 0; siteNum < primStructure.numDefiningSites(); siteNum++) {
      Structure.Site site = primStructure.getDefiningSite(siteNum);
      Structure.Site[] neighbors = primStructure.getNearestNeighbors(site);
      maxNearestNeighbors = Math.max(maxNearestNeighbors, neighbors.length);
      if (neighbors.length == 0) {
        return new Coordinates[] {lattice.removeLattice(site.getCoords())};
      }
      double distance = site.distanceFrom(neighbors[0]);
      minNeighborDist = Math.min(minNeighborDist, distance);
    }
    
    Coordinates[] returnArray = new Coordinates[primStructure.numDefiningSites()];
    int returnIndex = 0;
    
    double searchDistance = minNeighborDist * 1.15; // Arbitrary -- can probably do better.
    for (int siteNum = 0; siteNum < primStructure.numDefiningSites(); siteNum++) {
      Structure.Site site = primStructure.getDefiningSite(siteNum);
      Structure.Site[] neighbors = primStructure.getNearbySites(site.getCoords(), searchDistance, false);
      if (neighbors.length < maxNearestNeighbors) {
        returnArray[returnIndex++] = lattice.removeLattice(site.getCoords());
      }
    }
    
    return (Coordinates[]) ArrayUtils.truncateArray(returnArray, returnIndex);
    
  }
  
  public double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq) {
    int group1index = this.getGroupForVar(var1);
    int group2index = this.getGroupForVar(var2);
    if (!m_CongruentGroups[group1index][group2index]) {return Double.POSITIVE_INFINITY;}
    double distance1 = m_GroupDistancesFromCenter[group1index];
    double distance2 = m_GroupDistancesFromCenter[group2index];
    double surfDistance1 = m_GroupDistancesFromCenter[group1index];
    double surfDistance2 = m_GroupDistancesFromCenter[group2index];
    double angle = m_MinGroupAngles[group1index][group2index];
    
    double minDist = parameters[parameters.length - 3];
    distance1 += minDist;
    distance2 += minDist;
    surfDistance1 += minDist;
    surfDistance2 += minDist;
    
    double distPower = parameters[parameters.length - 2];
    double distFactor1 = Math.pow(distance1, -distPower);
    double distFactor2 = Math.pow(distance2, -distPower);
    double surfDistFactor1 = Math.pow(surfDistance1, -distPower);
    double surfDistFactor2 = Math.pow(surfDistance2, -distPower);
    double distFactor = distFactor1 + distFactor2 + surfDistFactor1 + surfDistFactor2;
    
    // Added for paper
    /*double distScale = parameters[parameters.length - 4];
    distFactor /= distScale;*/
    
    double angleScale = parameters[parameters.length - 1];
    double angleFactor = angle / angleScale;
    
    double scaleFactor = angleFactor + distFactor;
    
    //double scaleFactor = distance / parameters[parameters.length - 1];
    //double scaleFactor = 1 / distScale + distance / parameters[parameters.length - 1];
    return selfSigmaSq[var1] * scaleFactor;
  }

  public double[] changeParametersRandomly(double[] oldParameters) {
    double[] returnArray = ArrayUtils.copyArray(oldParameters);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }

  public int numParameters() {
    return this.getLinearRegGenerator().numParameters() + 3;
  }
  
  protected boolean areGroupsCongruent(int group1Index, int group2Index) {
    return m_CongruentGroups[group1Index][group2Index];
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {

    if (Arrays.equals(m_ActiveGroups, activeGroupsInOrder)) {return;}
    super.setActiveGroups(activeGroupsInOrder);
    
    m_CongruentGroups = new boolean[activeGroupsInOrder.length][activeGroupsInOrder.length];
    m_MinGroupAngles = new double[activeGroupsInOrder.length][activeGroupsInOrder.length];
    m_GroupDistancesFromCenter = new double[activeGroupsInOrder.length];
    m_GroupDistancesFromSurface = new double[activeGroupsInOrder.length];
    
    if (activeGroupsInOrder.length == 0) {
      return;
    }
    
    Structure baseStructure = activeGroupsInOrder[0].getClusterExpansion().getBaseStructure();
    BravaisLattice lattice = baseStructure.getDefiningLattice();
    Coordinates centerCoords = new Coordinates(new double[] {0.5, 0.5, 0.5}, lattice.getLatticeBasis());
    Coordinates offLatticeCenter = lattice.removeLattice(centerCoords);
    Coordinates[] offLatticeSurfaceCoords = this.findSurfaceCoordinates(baseStructure);

    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      
      Coordinates avgCoords = group.getAveragePrimClusterCoords(0);
      if (avgCoords == null) {continue;}
      Coordinates offLatticeAvgCoords = lattice.removeLattice(avgCoords);
      Vector groupVector = new Vector(offLatticeCenter, offLatticeAvgCoords);
      m_GroupDistancesFromCenter[groupIndex] = groupVector.length();
      double distFromSurface = Double.POSITIVE_INFINITY;
      for (int surfCoordNum = 0; surfCoordNum < offLatticeSurfaceCoords.length; surfCoordNum++) {
        double distance = offLatticeSurfaceCoords[surfCoordNum].distanceFrom(offLatticeAvgCoords);
        distFromSurface = Math.min(distFromSurface, distance);
      }
      m_GroupDistancesFromSurface[groupIndex] = distFromSurface;
      
      Coordinates[] sampleCoords = group.getSampleCoords();
      CoordSetMapper mapper = new CoordSetMapper(sampleCoords);
      m_CongruentGroups[groupIndex][groupIndex] = true;
      for (int prevGroupIndex = 0; prevGroupIndex < groupIndex; prevGroupIndex++) {
        ClusterGroup prevGroup = activeGroupsInOrder[prevGroupIndex];
        if (prevGroup.numSitesPerCluster() != group.numSitesPerCluster()) {continue;}
        int[][] maps = mapper.mapCoordinates(prevGroup.getSampleCoords(), false);
        boolean match = (maps.length > 0);
        for (int mapNum = 0; mapNum < maps.length; mapNum++) {
          int[] map = maps[mapNum];
          for (int siteNum = 0; siteNum < map.length; siteNum++) {
            Species[] allowedSpecies = group.getAllowedSpecies(siteNum);
            Species[] prevAllowedSpecies = prevGroup.getAllowedSpecies(map[siteNum]);
            match = Arrays.equals(allowedSpecies, prevAllowedSpecies);
            if (!match) {break;}
          }
          if (!match) {break;}
        }
        if (!match) {continue;}
        
        double minAngle = Double.POSITIVE_INFINITY;
        for (int clustNum = 0; clustNum < prevGroup.numPrimClusters(); clustNum++) {
          Coordinates prevAvgCoords = prevGroup.getAveragePrimClusterCoords(0);
          Coordinates offLatticePrevAvgCoords = lattice.removeLattice(prevAvgCoords);
          Vector prevGroupVector = new Vector(offLatticeCenter, offLatticePrevAvgCoords);
          double angle = groupVector.angle(prevGroupVector);
          if (Double.isNaN(angle)) {angle = Math.PI;}
          minAngle = Math.min(angle, minAngle);
        }

        m_CongruentGroups[prevGroupIndex][groupIndex] = true;
        m_CongruentGroups[groupIndex][prevGroupIndex] = true;
        m_MinGroupAngles[prevGroupIndex][groupIndex] = minAngle;
        m_MinGroupAngles[groupIndex][prevGroupIndex] = minAngle;
        
      }
    }
  }

}
