/*
 * Created on Aug 20, 2007
 *
 */
package matsci.structure.decorate.function.ce.reg.coupled;

import java.util.Arrays;

import matsci.*;
import matsci.location.*;
import matsci.location.symmetry.operations.*;
import matsci.structure.*;
import matsci.structure.decorate.function.ce.*;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;

public class LatticeCoupledRegGenerator extends AbstractCoupledRegGenerator {

  protected ClusterExpansion m_TemplateCE;
  protected SymmetryOperation[][][] m_ClusterMaps;
  protected int[][][][] m_MatchingOrigSites;
  protected int[][][][] m_MatchingOppedSites;
  
  protected double[][] m_SiteDistances;
  
  public LatticeCoupledRegGenerator(ClusterExpansion templateCE, ILinearCERegGenerator regGenerator, ClusterGroup[] activeGroupsInOrder) {
    super(regGenerator, new ClusterGroup[0]);
    m_TemplateCE = templateCE;
    this.setActiveGroups(activeGroupsInOrder);
  }

  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {

    if (Arrays.equals(m_ActiveGroups, activeGroupsInOrder)) {return;}
    super.setActiveGroups(activeGroupsInOrder);
    
    m_ClusterMaps =new SymmetryOperation[activeGroupsInOrder.length][][];
    m_MatchingOrigSites = new int[activeGroupsInOrder.length][][][];
    m_MatchingOppedSites = new int[activeGroupsInOrder.length][][][];
    SpaceGroup templateSpaceGroup = m_TemplateCE.getSpaceGroup();
    
    m_SiteDistances = new double[activeGroupsInOrder.length][];
    
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      
      ClusterExpansion ce = group.getClusterExpansion();
      Structure primStructure = ce.getBaseStructure();
      
      Coordinates[] sampleCoords = group.getSampleCoords();
      m_ClusterMaps[groupIndex] = new SymmetryOperation[groupIndex + 1][0];
      m_MatchingOrigSites[groupIndex] = new int[groupIndex + 1][0][];
      m_MatchingOppedSites[groupIndex] = new int[groupIndex + 1][0][];

      for (int prevGroupIndex = 0; prevGroupIndex < m_ClusterMaps[groupIndex].length; prevGroupIndex++) {
        ClusterGroup prevGroup = activeGroupsInOrder[prevGroupIndex];
        if (prevGroup.numSitesPerCluster() != group.numSitesPerCluster()) {continue;}
        Coordinates[] prevSampleCoords = prevGroup.getSampleCoords();
        SymmetryOperation[] operations = templateSpaceGroup.findMappingOperations(prevSampleCoords, sampleCoords, false);
        m_MatchingOrigSites[groupIndex][prevGroupIndex] = new int[operations.length][primStructure.numDefiningSites()];
        m_MatchingOppedSites[groupIndex][prevGroupIndex] = new int[operations.length][primStructure.numDefiningSites()];
        m_ClusterMaps[groupIndex][prevGroupIndex] = operations;
        
        for (int opNum = 0; opNum < operations.length; opNum++) {
          SymmetryOperation op = operations[opNum];
          Structure mappedStructure = primStructure.operate(op);
          for (int siteNum = 0; siteNum < primStructure.numDefiningSites(); siteNum++) {
            Structure.Site mappedSite = mappedStructure.getDefiningSite(primStructure.getSiteCoords(siteNum));
            if (mappedSite == null) {continue;}
            Species[] allowedSpecies = ce.getAllowedSpecies(siteNum);
            Species[] mappedAllowedSpecies = ce.getAllowedSpecies(mappedSite.getIndex());
            if (!Arrays.equals(allowedSpecies, mappedAllowedSpecies)) {continue;}
            m_MatchingOrigSites[groupIndex][prevGroupIndex][opNum][siteNum] = 1;
            m_MatchingOppedSites[groupIndex][prevGroupIndex][opNum][mappedSite.getIndex()] = 1;
          }
        }
      }
      
      // Find the minimum distances between the sample sites in this cluster and the other primitive sites
      double[] minDistances = new double[primStructure.numDefiningSites()];
      for (int siteNum = 0; siteNum < minDistances.length; siteNum++) {
        Coordinates siteCoords = primStructure.getSiteCoords(siteNum);
        double minDistance = Double.POSITIVE_INFINITY;
        for (int clustSiteNum = 0; clustSiteNum < sampleCoords.length; clustSiteNum++) {
          Coordinates clustCoords = sampleCoords[clustSiteNum];
          double distance = siteCoords.distanceFrom(clustCoords);
          Structure.Site[] nearbySites = primStructure.getNearbySites(clustCoords, distance, true);
          for (int nearbySiteNum = 0; nearbySiteNum < nearbySites.length; nearbySiteNum++) {
            Structure.Site nearbySite = nearbySites[nearbySiteNum];
            if (nearbySite.getIndex() != siteNum) {continue;}
            double nearbyDistance = nearbySite.getCoords().distanceFrom(clustCoords);
            distance = Math.min(distance, nearbyDistance);
          }
          minDistance = Math.min(distance, minDistance);
        }
        minDistances[siteNum] = minDistance;
      }
      
      m_SiteDistances[groupIndex] = minDistances;
    }
    
  }

  public double getCoupledSigmaSq(int var1, int var2, double[] parameters, double[] selfSigmaSq) {
  
    double overlap = this.getMaxOverlap(var1, var2, parameters);
    double scaleFactor = 1 / parameters[parameters.length - 1];
    return (Math.pow(overlap, -scaleFactor) - 1) * selfSigmaSq[var1];
    //double scaleFactor = 1 / parameters[parameters.length - 1];
    //return ((scaleFactor / overlap) - scaleFactor) * selfSigmaSq[var1];
    
  }
  
  protected double getMaxOverlap(int var1, int var2, double[] parameters) {
    
    int minVar = (var1 < var2) ? var1: var2;
    int maxVar = (var1 < var2) ? var2: var1;
    
    int[][] matchingOrigSites = m_MatchingOrigSites[maxVar][minVar];
    if (matchingOrigSites.length == 0) {return 0;}
    int[][] matchingOppedSites = m_MatchingOppedSites[maxVar][minVar];
    
    int minFunctGroup = this.getFunctGroupForVar(minVar);
    int maxFunctGroup = this.getFunctGroupForVar(maxVar);
    
    if (minFunctGroup != maxFunctGroup) {return 0;}
    
    int minGroupIndex = this.getGroupForVar(minVar);
    int maxGroupIndex = this.getGroupForVar(maxVar);
    
    double[] origSiteDistances = m_SiteDistances[minGroupIndex];
    double[] oppedSiteDistances = m_SiteDistances[maxGroupIndex];
    
    double returnValue = 0;
    for (int opNum = 0; opNum < matchingOrigSites.length; opNum++) {
      int[] origMatches = matchingOrigSites[opNum];
      int[] oppedMatches = matchingOppedSites[opNum];
      double origOverlap = 0;
      double origTotal = 0;
      double oppedOverlap = 0;
      double oppedTotal = 0;
      for (int siteNum = 0; siteNum < origMatches.length; siteNum++) {
        double origSiteOverlap = this.getOverlap(origSiteDistances[siteNum], parameters);
        origTotal += origSiteOverlap;
        origOverlap += origSiteOverlap * origMatches[siteNum];
        
        double oppedSiteOverlap = this.getOverlap(oppedSiteDistances[siteNum], parameters);
        oppedTotal += oppedSiteOverlap;
        oppedOverlap += oppedSiteOverlap * oppedMatches[siteNum];
      }
      
      double overlapPercent = (origOverlap + oppedOverlap) / (origTotal + oppedTotal);
      returnValue = Math.max(overlapPercent, returnValue);
    }
    
    return returnValue;
    
  }
  
  protected double getOverlap(double distance, double[] parameters) {
    int paramStart = parameters.length - 3;
    double base = parameters[paramStart];
    double scaleFactor = parameters[paramStart+1];
    return Math.pow(1+base, -1*scaleFactor*distance);
  }

  public int numParameters() {
    return this.getLinearRegGenerator().numParameters() + 3;
  }

}
