/*
 * Created on Mar 31, 2007
 *
 */
package matsci.structure.decorate.function.ce.reg;

import matsci.model.reg.IRegGenerator;
import matsci.structure.decorate.function.ce.clusters.*;

public interface ICERegGenerator extends IRegGenerator {
  
  public double getMultiplicityExponent();
  public void setMultiplicityExponent(double value);
  
  public ClusterGroup[] getActiveGroups();
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder);

}
