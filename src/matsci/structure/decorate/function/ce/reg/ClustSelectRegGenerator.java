/*
 * Created on Nov 20, 2010
 *
 */
package matsci.structure.decorate.function.ce.reg;

import java.util.Arrays;
import java.util.BitSet;
import java.util.Random;

import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;

public class ClustSelectRegGenerator implements ILinearCERegGenerator {
  protected static Random RANDOM = new Random();
  protected double m_Factor = 2;
  protected int m_NumVariables = 0;
  
  protected double m_ForbiddenValue = 1E7;

  protected ClusterGroup[] m_ActiveGroups;
  
  protected BitSet m_UnRegVars = new BitSet();
  
  protected int m_EmptyVarNum = -1;
  protected int[] m_Multiplicities;
  protected double m_MultiplicityExponent = 0;
  
  protected int[] m_VarStartsByGroupIndex;
  protected int[] m_GroupIndicesByVarNum;
  protected int[] m_GroupIndicesByGroupNum;
  
  protected boolean m_SubgroupConstraint = true;
  protected int[][] m_Constraints;
  
  public ClustSelectRegGenerator(ClusterGroup[] activeGroupsInOrder) {
    this.setActiveGroups(activeGroupsInOrder);
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {
    
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroupsInOrder);
    
    int numVariables = 0;
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      if (group == null) {continue;}
      numVariables += group.numFunctionGroups();
    }
    m_NumVariables = numVariables;
    
    m_Multiplicities =new int[m_NumVariables];
    int varIndex =0;

    m_VarStartsByGroupIndex = new int[activeGroupsInOrder.length];
    int totalFunctNum = 0;
    int maxGroupNumber = 0;
    
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      m_VarStartsByGroupIndex[groupIndex] = totalFunctNum;
      totalFunctNum += activeGroupsInOrder[groupIndex].numFunctionGroups();
      maxGroupNumber = Math.max(maxGroupNumber, activeGroupsInOrder[groupIndex].getGroupNumber());
      if (group == null) {continue;}
      int multiplicity = group.getMultiplicity();
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        m_Multiplicities[varIndex] = multiplicity;
        if (group.numSitesPerCluster() == 0) {
          m_EmptyVarNum = varIndex;
        }
        varIndex++;
      }
    }
    
    m_GroupIndicesByVarNum = new int[totalFunctNum];
    m_GroupIndicesByGroupNum = new int[maxGroupNumber + 1];
    Arrays.fill(m_GroupIndicesByGroupNum, -1);
    totalFunctNum = 0;
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      for (int functNum = 0; functNum < group.numFunctionGroups(); functNum++) {
        m_GroupIndicesByVarNum[totalFunctNum++] = groupIndex;
        //System.out.println("Group number " + group.getGroupNumber() + ", Var index: " + m_VarStartsByGroupIndex[groupIndex]);
      }
      m_GroupIndicesByGroupNum[group.getGroupNumber()] = groupIndex;
    }
    
    m_Constraints = m_SubgroupConstraint ? getSubGroupConstraints() : getIndividualClusterConstraints();
  }

  public double[] getRegularizer(double[] state, double[] returnArray) {

    if (state.length != m_NumVariables) {
      throw new RuntimeException("State doesn't correspond to number of input variables");
    }
    if (returnArray == null || returnArray.length != state.length) {
      returnArray = new double[state.length];
    }
    
    boolean[] forcedVars = new boolean[returnArray.length];
    for (int varNum = 0; varNum < returnArray.length; varNum++) {
      if (state[varNum] < 0.5 || m_UnRegVars.get(varNum)) { // Include this and constrained vars
        returnArray[varNum] = 0;
        int[] varsToForce = m_Constraints[varNum];
        for (int subVarNum = 0; subVarNum < varsToForce.length; subVarNum++) {
          returnArray[varsToForce[subVarNum]] = 0;
          forcedVars[varsToForce[subVarNum]] = true;
        }
      } else { // Exclude if not constrained
        if (forcedVars[varNum]) {continue;}
        returnArray[varNum] = m_ForbiddenValue;
      }
    }
    
    for(int i = m_UnRegVars.nextSetBit(0); i >= 0; i = m_UnRegVars.nextSetBit(i+1)) {
      returnArray[i] = 0;
    }
    
    for (int varIndex = 0; varIndex < returnArray.length; varIndex++) {
      returnArray[varIndex] /= Math.pow(m_Multiplicities[varIndex], m_MultiplicityExponent);
    }
    
    return returnArray;
  }
  
  public double[] changeParametersRandomly(double[] oldState) {
    double[] returnArray = ArrayUtils.copyArray(oldState);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    /*double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);*/
    if (returnArray[indexToChange] < 0.5) { // Exclude this and all parents that might force it
      returnArray[indexToChange] = 1;
      for (int varIndex = 0; varIndex < m_Constraints.length; varIndex++) {
        if (ArrayUtils.arrayContains(m_Constraints[varIndex], indexToChange)) {
          returnArray[varIndex] = 1;
        }
      }
    } else {
      returnArray[indexToChange] = 0;
      int[] varsToForce = m_Constraints[indexToChange];
      for (int forcedVarNum = 0; forcedVarNum < varsToForce.length; forcedVarNum++) {
        returnArray[varsToForce[forcedVarNum]] = 0;
      }
    }
    
    return returnArray;
  }
  
  public double getMultiplicityExponent() {
    return m_MultiplicityExponent;
  }
  
  public void setMultiplicityExponent(double value) {
    m_MultiplicityExponent = value;
  }
  
  public void setFactor(double factor) {
    m_Factor = factor;
  }
  
  public double getFactor() {
    return m_Factor;
  }
  
  public void regularizeVariable(int varNum, boolean value) {
    m_UnRegVars.set(varNum, !value);
  }
  
  public int numParameters() {
    return m_NumVariables;
  }

  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }
  
  public int numVariables() {
    return m_NumVariables;
  }
  
  public int[][] getConstraints() {
    return ArrayUtils.copyArray(m_Constraints);
  }
  
  public void setConstraints(int[][] constraints) {
    m_Constraints = ArrayUtils.copyArray(constraints);
  }
  
  /**
   * This constrains that all function groups in a given cluster must be either 
   * activated or deactivated at the same time.
   * @return
   */
  private int[][] getIndividualClusterConstraints() {
    
    int numVariables = m_GroupIndicesByVarNum.length;
    int[][] returnArray = new int[numVariables][];
    int returnIndex = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        returnArray[returnIndex] = new int[group.numFunctionGroups() - 1];
        int subFunctIndex =0;
        for (int subFunctNum = 0; subFunctNum < group.numFunctionGroups(); subFunctNum++) {
          if (subFunctNum == functGroupNum) {continue;}
          returnArray[returnIndex][subFunctIndex++] = subFunctNum;
        }
        returnIndex++;
      }
    }
    return returnArray;
  }
  
  private int[][] getSubGroupConstraints() {
    
    int[][] returnArray = this.getIndividualClusterConstraints();
    int returnIndex = 0;
    for (int groupIndex = 0; groupIndex < m_ActiveGroups.length; groupIndex++) {
      ClusterGroup group = m_ActiveGroups[groupIndex];
      int[] subGroupIndices =new int[0];
      for (int subGroupCount = 0; subGroupCount < group.numSubGroups(); subGroupCount++) {
        ClusterGroup subGroup = group.getSubGroup(subGroupCount);
        if (!ArrayUtils.arrayContains(m_ActiveGroups, subGroup)) {continue;}
        int groupIndexForSubGroup = m_GroupIndicesByGroupNum[subGroup.getGroupNumber()];
        int[] indicesForSubGroup = new int[subGroup.numFunctionGroups()];
        for (int functNum = 0; functNum < indicesForSubGroup.length; functNum++) {
          indicesForSubGroup[functNum] = m_VarStartsByGroupIndex[groupIndexForSubGroup] + functNum;
        }
        subGroupIndices = ArrayUtils.appendArray(subGroupIndices, indicesForSubGroup);
      }
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        returnArray[returnIndex] = ArrayUtils.appendArray(returnArray[returnIndex], subGroupIndices);
        returnIndex++;
      }
    }
    return returnArray;
  }

}
