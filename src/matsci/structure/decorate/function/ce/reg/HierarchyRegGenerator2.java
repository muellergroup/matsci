/*
 * Created on Sep 25, 2006
 *
 */
package matsci.structure.decorate.function.ce.reg;

import java.util.Arrays;
import java.util.Random;

import matsci.location.Coordinates;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.model.reg.ILinearRegGenerator;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;

public class HierarchyRegGenerator2 implements ILinearCERegGenerator {

  private ClusterGroup[] m_ActiveClusters;
  private int[][] m_SubPoints;
  private double[][][] m_SiteDistances;

  protected ClusterGroup[] m_ActiveGroups;
  
  private double m_MultiplicityExponent = 0;
  private double m_Factor = 2;
  private int m_NumVars = 0;
  private int m_NumPointClusters;
  
  private static final Random RANDOM = new Random();
  
  public HierarchyRegGenerator2(ClusterGroup[] activeGroups) {
    this.setActiveGroups(activeGroups);
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroups) {

    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroups);

    m_ActiveClusters = (ClusterGroup[]) ArrayUtils.copyArray(activeGroups);
    m_SubPoints = new int[activeGroups.length][];
    m_SiteDistances = new double[activeGroups.length][][];
    m_NumVars = 0;
    
    if (activeGroups.length == 0) {return;}
    ClusterExpansion ce = activeGroups[0].getClusterExpansion();
    SpaceGroup spaceGroup = ce.getSpaceGroup();
    
    int maxGroupNumber = 0;
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      maxGroupNumber = Math.max(maxGroupNumber, activeGroups[groupIndex].getGroupNumber());
    }
    
    int[] pointClusterIndices = new int[0];
    int[] pointClusterMap = new int[maxGroupNumber + 1];
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      ClusterGroup group = activeGroups[groupIndex];
      m_NumVars += group.numFunctionGroups();
      int groupNumber = group.getGroupNumber();
      maxGroupNumber = Math.max(maxGroupNumber, groupNumber);
      if (group.numSitesPerCluster() == 1) {
        pointClusterMap[groupNumber] = pointClusterIndices.length;
        pointClusterIndices = ArrayUtils.appendElement(pointClusterIndices, groupIndex);
      }
    }
    m_NumPointClusters = pointClusterIndices.length;
    
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      ClusterGroup group = activeGroups[groupIndex];
      m_SubPoints[groupIndex] = new int[group.numSitesPerCluster()];
      Arrays.fill(m_SubPoints[groupIndex], -1);
      m_SiteDistances[groupIndex] = new double[group.numSitesPerCluster()][group.numSitesPerCluster()];
      Coordinates[] sampleCoords = group.getSampleCoords();
      for (int coordNum = 0; coordNum < sampleCoords.length; coordNum++) {
        Coordinates coord1 = sampleCoords[coordNum];
        for (int pointClusterNum = 0; pointClusterNum < pointClusterIndices.length; pointClusterNum++) {
          Coordinates samplePointCoords = activeGroups[pointClusterIndices[pointClusterNum]].getSampleCoords()[0];
          if (spaceGroup.areSymmetricallyEquivalent(samplePointCoords, coord1)) {
            m_SubPoints[groupIndex][coordNum] = pointClusterNum;
            break;
          }
        }
        
        for (int coordNum2 = 0; coordNum2 < coordNum; coordNum2++) {
          Coordinates coord2 = sampleCoords[coordNum2];
          double distance = coord1.distanceFrom(coord2);
          m_SiteDistances[groupIndex][coordNum][coordNum2] = distance;
          m_SiteDistances[groupIndex][coordNum2][coordNum] = distance;
        }
      }
    }
  }

  public double[] getRegularizer(double[] state, double[] template) {
    double[] returnArray = template;
    if (template == null || template.length != m_NumVars) {
      returnArray = new double[m_NumVars];
    }
    
    double pointLambda = state[0];
    double lambda2 = state[1];
    double lambda3 = state[2];
    int returnIndex = 0;
    for (int clustIndex = 0; clustIndex < m_SubPoints.length; clustIndex++) {
      ClusterGroup group = m_ActiveClusters[clustIndex];
      if (group.numSitesPerCluster() == 0) {
        returnIndex++;
        continue;
      }
      if (group.numSitesPerCluster() == 1) {
        returnArray[returnIndex++] = pointLambda;
        continue;
      }
      double multScale = Math.pow(group.getMultiplicity(), m_MultiplicityExponent);
      int[] subPoints = m_SubPoints[clustIndex];
      double[][] distances = m_SiteDistances[clustIndex];
      double lambda = 0;
      for (int coordNum = 0; coordNum < subPoints.length; coordNum++) {
        int pointIndex = subPoints[coordNum];
        if (pointIndex < 0) {
          lambda = Double.POSITIVE_INFINITY;
          break;
        }
        double pointTerm = state[3 + pointIndex];
        double product = pointTerm;
        for (int coord2Num = 0; coord2Num < distances.length; coord2Num++) {
          if (coord2Num == coordNum) {continue;}
          product *= (1 + lambda2 * distances[coordNum][coord2Num]);
        }
        product = Math.pow(product, 1.0/lambda3);
        lambda += product;
      }
      lambda = Math.pow(lambda, lambda3);
      for (int functNum = 0; functNum < m_ActiveClusters[clustIndex].numFunctionGroups(); functNum++) {
        returnArray[returnIndex++] = lambda / multScale;
      }
    }
    return returnArray;
  }

  public double[] changeParametersRandomly(double[] oldState) {
    double[] returnArray = ArrayUtils.copyArray(oldState);
    int indexToChange = RANDOM.nextInt(returnArray.length);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[indexToChange] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }
  
  public void setMultiplicityExponent(double value) {
    m_MultiplicityExponent = value;
  }
  
  public void setFactor(double factor) {
    m_Factor = factor;
  }
  
  public double getFactor() {
    return m_Factor;
  }
  
  public int numParameters() {
    return 3 + m_NumPointClusters;
  }

  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }
  
  public int numVariables() {
    return m_NumVars;
  }

  public double getMultiplicityExponent() {
    return m_MultiplicityExponent;
  }

}
