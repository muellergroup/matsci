/*
 * Created on May 13, 2006
 *
 */
package matsci.structure.decorate.function.ce.reg;

import java.util.Arrays;
import java.util.Random;

import matsci.model.reg.ILinearRegGenerator;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.ArrayUtils;

public class LambdaRegGenerator implements ILinearCERegGenerator {

  protected static Random RANDOM = new Random();

  protected int m_NumVariables;
  protected double m_Factor = 2;
  protected int[] m_UnregulatedVarNums = new int[0];
  protected boolean m_RegEmpty;
  
  protected int m_EmptyVarNum = -1;
  protected ClusterGroup[] m_ActiveGroups;
  
  protected int[] m_Multiplicities;
  protected double m_MultiplicityExponent = 0;
  
  public LambdaRegGenerator(ClusterGroup[] activeGroupsInOrder) {
    this.setActiveGroups(activeGroupsInOrder);
  }
  
  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {
    m_EmptyVarNum = -1;
    m_ActiveGroups = (ClusterGroup[]) ArrayUtils.copyArray(activeGroupsInOrder);
    m_NumVariables =0;
    for (int groupIndex =0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      m_NumVariables += group.numFunctionGroups();
    }
    
    m_Multiplicities =new int[m_NumVariables];
    int varIndex =0;
    for (int groupNum = 0; groupNum < activeGroupsInOrder.length; groupNum++) {
      ClusterGroup group = activeGroupsInOrder[groupNum];
      if (group == null) {continue;}
      int multiplicity = group.getMultiplicity();
      for (int functGroupNum = 0; functGroupNum < group.numFunctionGroups(); functGroupNum++) {
        m_Multiplicities[varIndex] = multiplicity;
        if (group.numSitesPerCluster() == 0) {
          m_EmptyVarNum = varIndex;
        }
        varIndex++;
      }
    }
  }
  
  public LambdaRegGenerator(int[] multiplicities, int emptyVarNum) {
    m_NumVariables = multiplicities.length;
    m_Multiplicities =ArrayUtils.copyArray(multiplicities);
    m_EmptyVarNum = emptyVarNum;
  }
  
  public void regularizeEmpty(boolean value) {
    m_RegEmpty = value;
  }

  public double[] getRegularizer(double[] state, double[] template) {
    double[] returnArray = (template == null) ? new double[m_NumVariables] : template;
    Arrays.fill(returnArray, state[0]);
    for (int varIndex = 0; varIndex < m_UnregulatedVarNums.length; varIndex++) {
      returnArray[m_UnregulatedVarNums[varIndex]] = 0;
    }
    if (!m_RegEmpty && m_EmptyVarNum >= 0) {
      returnArray[m_EmptyVarNum] = 0;
    }
    for (int varNum =0; varNum < returnArray.length; varNum++) {
      returnArray[varNum] /= Math.pow(m_Multiplicities[varNum], m_MultiplicityExponent);
    }
    return returnArray;
  }

  public double[] changeParametersRandomly(double[] oldState) {
    double[] returnArray = ArrayUtils.copyArray(oldState);
    double exponent = (RANDOM.nextDouble() * 2 - 1);
    returnArray[0] *= Math.pow(m_Factor, exponent);
    return returnArray;
  }
  
  public void setFactor(double factor) {
    m_Factor = factor;
  }
  
  public double getFactor() {
    return m_Factor;
  }
  
  public void setUnregulatedVars(int[] varNums) {
    int index = ArrayUtils.findIndex(varNums, m_EmptyVarNum);
    if (index >= 0) {
      this.regularizeEmpty(true);
      varNums = ArrayUtils.removeElement(varNums, index);
    }
    m_UnregulatedVarNums = ArrayUtils.copyArray(varNums);
    
  }
  
  public int[] getUnregulatedVars() {
    return ArrayUtils.copyArray(m_UnregulatedVarNums);
  }
  
  public void setMultiplicityExponent(double value) {
    m_MultiplicityExponent = value;
  }

  public int numParameters() {
    return 1;
  }

  public ClusterGroup[] getActiveGroups() {
    return (ClusterGroup[]) ArrayUtils.copyArray(m_ActiveGroups);
  }

  public int numVariables() {
    return m_NumVariables;
  }

  public double getMultiplicityExponent() {
    return m_MultiplicityExponent;
  }

}
