/*
 * Created on Oct 21, 2010
 *
 */
package matsci.structure.decorate.function.ce.reg.empty;

import matsci.model.reg.ILinearCovRegGenerator;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ICERegGenerator;
import matsci.structure.decorate.function.ce.reg.coupled.AbstractCoupledRegGenerator;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class ConcentrationBasisRegGenerator implements ICERegGenerator, ILinearCovRegGenerator {
  
  private AbstractCoupledRegGenerator m_BaseGenerator;
  private double[][] m_FullTransformationMatrix;
  private double[][] m_ActiveTransformationMatrix;
  
  private int[][] m_VarIndices; // By cluster orbit then by function orbit
  
  public ConcentrationBasisRegGenerator(AbstractCoupledRegGenerator baseGenerator, ClusterGroup[] allGroupsInOrder, double[][] transformationMatrix) {
    
    m_BaseGenerator = baseGenerator;
    m_FullTransformationMatrix = ArrayUtils.copyArray(transformationMatrix);
    
    int maxGroupNumber = 0;
    for (int groupIndex = 0; groupIndex < allGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = allGroupsInOrder[groupIndex];
      maxGroupNumber = Math.max(maxGroupNumber, group.getGroupNumber());
    }

    m_VarIndices = new int[maxGroupNumber + 1][];
    
    int varIndex = 0;
    for (int groupIndex = 0; groupIndex < allGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = allGroupsInOrder[groupIndex];
      int groupNumber = group.getGroupNumber();
      m_VarIndices[groupNumber] = new int[group.numFunctionGroups()];
      for (int functGroupNum = 0; functGroupNum < m_VarIndices[groupIndex].length; functGroupNum++) {
        m_VarIndices[groupNumber][functGroupNum] = varIndex++;
      }
    }
    
    if (varIndex != transformationMatrix.length) {
      throw new RuntimeException("Number of rows in transformation matrix must match total number of allowed function orbits");
    }
    
    this.setActiveGroups(baseGenerator.getActiveGroups());
    
  }
  
  public double[] changeParametersRandomly(double[] oldParameters) {
    return m_BaseGenerator.changeParametersRandomly(oldParameters);
  }

  public int numParameters() {
    return m_BaseGenerator.numParameters();
  }

  public double[][] getRegularizer(double[] parameters, double[][] template) {
    double[][] baseRegularizer = m_BaseGenerator.getRegularizer(parameters, template);
    
    double[][] baseA = MSMath.matrixMultiply(baseRegularizer, m_ActiveTransformationMatrix);
    double[][] aT = MSMath.transpose(m_ActiveTransformationMatrix);
    return MSMath.matrixMultiply(aT, baseA, template);
    
  }

  public double getMultiplicityExponent() {
    return m_BaseGenerator.getMultiplicityExponent();
  }

  public void setMultiplicityExponent(double value) {
    m_BaseGenerator.setMultiplicityExponent(value);
  }

  public ClusterGroup[] getActiveGroups() {
    return m_BaseGenerator.getActiveGroups();
  }

  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {
    m_BaseGenerator.setActiveGroups(activeGroupsInOrder);
    
    int numVariables = m_BaseGenerator.numVariables();
    m_ActiveTransformationMatrix = new double[numVariables][numVariables];
    
    int activeVarNum = 0;
    int[] activeMatrixVars = new int[numVariables];
    for (int groupIndex = 0; groupIndex < activeGroupsInOrder.length; groupIndex++) {
      ClusterGroup group = activeGroupsInOrder[groupIndex];
      int[] varIndices = m_VarIndices[group.getGroupNumber()];
      for (int functGroupNum = 0; functGroupNum < varIndices.length; functGroupNum++) {
        activeMatrixVars[activeVarNum++] = varIndices[functGroupNum];
      }
    }
    
    m_ActiveTransformationMatrix = ArrayUtils.selectRowsAndColumns(m_FullTransformationMatrix, activeMatrixVars);
    
  }

}
