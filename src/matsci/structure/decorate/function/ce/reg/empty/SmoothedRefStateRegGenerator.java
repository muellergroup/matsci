/*
 * Created on Oct 17, 2009
 *
 */
package matsci.structure.decorate.function.ce.reg.empty;

import java.util.Random;

import matsci.model.reg.ILinearCovRegGenerator;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ICERegGenerator;
import matsci.structure.decorate.function.ce.reg.coupled.AbstractCoupledRegGenerator;
import matsci.util.arrays.ArrayUtils;

public class SmoothedRefStateRegGenerator implements ILinearCovRegGenerator, ICERegGenerator {
  
  private static Random RANDOM = new Random();
  
  private AbstractCoupledRegGenerator m_BaseGenerator;
  private int m_NumRefStates;
  private int m_BaseEmptyIndex = -1;
  
  public SmoothedRefStateRegGenerator(AbstractCoupledRegGenerator coupledGenerator, int numRefStates) {
    m_BaseGenerator = coupledGenerator;
    m_BaseEmptyIndex = coupledGenerator.getEmptyGroupVarIndex();
    m_NumRefStates = numRefStates;
  }

  public double[][] getRegularizer(double[] parameters, double[][] template) {

    double[] baseParams = ArrayUtils.truncateArray(parameters, parameters.length - 1);
    
    if (m_BaseEmptyIndex < 0) {
      return m_BaseGenerator.getRegularizer(baseParams, template);
    }
    
    double[][] returnArray = template;
    if (returnArray == null) {
      returnArray = new double[this.numVariables()][this.numVariables()];
    }
    
    double[][] baseRegularizer = m_BaseGenerator.getRegularizer(baseParams, null);
    double secondDerivParam = parameters[parameters.length - 1];
    
    for (int varNum = 0; varNum < baseRegularizer.length; varNum++) {
      
      double[] baseRow = baseRegularizer[varNum];
      int returnRowNum = (varNum > m_BaseEmptyIndex) ? varNum + m_NumRefStates - 1 : varNum;
      double[] returnRow = returnArray[returnRowNum];
      System.arraycopy(baseRow, 0, returnRow, 0, m_BaseEmptyIndex);
      System.arraycopy(baseRow, m_BaseEmptyIndex + 1, returnRow, m_BaseEmptyIndex + m_NumRefStates, baseRow.length - m_BaseEmptyIndex - 1);
      
      if (varNum == m_BaseEmptyIndex) {
        for (int returnVarNum = m_BaseEmptyIndex; returnVarNum < m_BaseEmptyIndex + m_NumRefStates; returnVarNum++) {
          returnArray[returnVarNum] = ArrayUtils.copyArray(returnRow);
          returnArray[returnVarNum][returnVarNum] = returnRow[m_BaseEmptyIndex];
          /*if (returnVarNum > m_BaseEmptyIndex) {
            returnArray[returnVarNum-1][returnVarNum-1] += firstDerivParam;
            returnArray[returnVarNum][returnVarNum-1] -= firstDerivParam;
            returnArray[returnVarNum-1][returnVarNum] -= firstDerivParam;
            returnArray[returnVarNum][returnVarNum] += firstDerivParam;
          }*/
          if (returnVarNum > m_BaseEmptyIndex + 1) {
            returnArray[returnVarNum-2][returnVarNum-2] += secondDerivParam;
            returnArray[returnVarNum-1][returnVarNum-1] += 4 * secondDerivParam;
            returnArray[returnVarNum][returnVarNum] += secondDerivParam;
            
            returnArray[returnVarNum-2][returnVarNum-1] -= 2 * secondDerivParam;
            returnArray[returnVarNum-1][returnVarNum-0] -= 2 * secondDerivParam;

            returnArray[returnVarNum-1][returnVarNum-2] -= 2 * secondDerivParam;
            returnArray[returnVarNum-0][returnVarNum-1] -= 2 * secondDerivParam;
            
            returnArray[returnVarNum-2][returnVarNum] += secondDerivParam;
            returnArray[returnVarNum][returnVarNum-2] += secondDerivParam;
          }
          /*if (returnVarNum > m_BaseEmptyIndex + 2) {
            returnArray[returnVarNum-3][returnVarNum-3] += secondDerivParam;
            returnArray[returnVarNum-2][returnVarNum-2] += 9 * secondDerivParam;
            returnArray[returnVarNum-1][returnVarNum-1] += 9 * secondDerivParam;
            returnArray[returnVarNum-0][returnVarNum-0] += secondDerivParam;
            
            returnArray[returnVarNum-3][returnVarNum-2] -= 3 * secondDerivParam;
            returnArray[returnVarNum-2][returnVarNum-1] -= 9 * secondDerivParam;
            returnArray[returnVarNum-1][returnVarNum-0] -= 3 * secondDerivParam;
            
            returnArray[returnVarNum-2][returnVarNum-3] -= 3 * secondDerivParam;
            returnArray[returnVarNum-1][returnVarNum-2] -= 9 * secondDerivParam;
            returnArray[returnVarNum-0][returnVarNum-1] -= 3 * secondDerivParam;
            
            returnArray[returnVarNum-3][returnVarNum-1] += 3 * secondDerivParam;
            returnArray[returnVarNum-2][returnVarNum-0] += 3 * secondDerivParam;

            returnArray[returnVarNum-1][returnVarNum-3] += 3 * secondDerivParam;
            returnArray[returnVarNum-0][returnVarNum-2] += 3 * secondDerivParam;
            
            returnArray[returnVarNum-3][returnVarNum] -= secondDerivParam;
            returnArray[returnVarNum][returnVarNum-3] -= secondDerivParam;
          }*/
        }
      } else {
        for (int returnVarNum = m_BaseEmptyIndex; returnVarNum < m_BaseEmptyIndex + m_NumRefStates; returnVarNum++) {
          returnRow[returnVarNum] = baseRow[m_BaseEmptyIndex];
        }
      }
      
    }
    
    return returnArray;
    
  }

  public double[] changeParametersRandomly(double[] oldParameters) {
    
    double[] returnArray = (oldParameters == null) ? new double[this.numParameters()] : oldParameters;
    int indexToChange = RANDOM.nextInt(returnArray.length);
    if (indexToChange >= returnArray.length - 1) {
      double exponent = (RANDOM.nextDouble() * 2 - 1);
      returnArray[indexToChange] *= Math.pow(m_BaseGenerator.getScaleFactor(), exponent);
    } else {
      double[] baseParams = ArrayUtils.truncateArray(returnArray, returnArray.length - 1);
      m_BaseGenerator.changeParametersRandomly(baseParams);
      System.arraycopy(baseParams, 0, returnArray, 0, baseParams.length);
    }
    return returnArray;
  }

  public int numParameters() {
    return m_BaseGenerator.numParameters() + 1;
  }
  
  public int numVariables() {
    if (m_BaseEmptyIndex < 0) { 
      return m_BaseGenerator.numVariables();
    }
    return m_BaseGenerator.numVariables() + m_NumRefStates - 1; // Don't double-count the empty state
  }

  public ClusterGroup[] getActiveGroups() {
    return m_BaseGenerator.getActiveGroups();
  }

  public double getMultiplicityExponent() {
    return m_BaseGenerator.getMultiplicityExponent();
  }

  public void setActiveGroups(ClusterGroup[] activeGroupsInOrder) {
    m_BaseGenerator.setActiveGroups(activeGroupsInOrder);
    m_BaseEmptyIndex = m_BaseGenerator.getEmptyGroupVarIndex();
  }

  public void setMultiplicityExponent(double value) {
    m_BaseGenerator.setMultiplicityExponent(value);
  }

}
