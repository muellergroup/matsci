/*
 * Created on Sep 14, 2017
 *
 */
package matsci.structure.decorate.function.ce;

import java.util.Arrays;

import matsci.model.reg.ILinearCovRegGenerator;
import matsci.structure.decorate.function.AppliedDecorationFunction;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.decorate.function.ce.reg.ICERegGenerator;
import matsci.structure.decorate.function.ce.reg.ILinearCERegGenerator;
import matsci.structure.decorate.function.ce.reg.coupled.AbstractCoupledRegGenerator;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
import matsci.util.arrays.MatrixInverter;

public class SQSScoreCalculator extends AppliedDecorationFunction {
  
  private GeneralAppliedCE m_AppliedCE;
  private double[][] m_TargetCorrelations;
  private double[][] m_CorrelationDelta;
  
  private AbstractScoreCalculator m_ScoreCalculator;

  public SQSScoreCalculator(GeneralAppliedCE appliedCE, AbstractCoupledRegGenerator regGenerator, double[] regParams) {
    super(appliedCE.getClusterExpansion(), appliedCE.getSuperStructure());
    m_AppliedCE = appliedCE;
    m_TargetCorrelations = appliedCE.getCorrelations();
    for (int clustNum = 0; clustNum < m_TargetCorrelations.length; clustNum++) {
      Arrays.fill(m_TargetCorrelations[clustNum], 0);
    }
    m_ScoreCalculator = new FullScoreCalculator(regGenerator, regParams);
    this.refreshFromHamiltonian();
  }
  
  public SQSScoreCalculator(GeneralAppliedCE appliedCE, ILinearCERegGenerator regGenerator, double[] regParams) {
    super(appliedCE.getClusterExpansion(), appliedCE.getSuperStructure());
    m_AppliedCE = appliedCE;
    for (int clustNum = 0; clustNum < m_TargetCorrelations.length; clustNum++) {
      Arrays.fill(m_TargetCorrelations[clustNum], 0);
    }
    m_ScoreCalculator = new LinearScoreCalculator(regGenerator, regParams);
    this.refreshFromHamiltonian();
  }

  public void refreshFromStructure() {
    super.refreshFromStructure();
    m_AppliedCE.refreshFromStructure();
    m_CorrelationDelta = MSMath.arraySubtractTolerateNull(m_AppliedCE.getCorrelations(), m_TargetCorrelations);
  }

  public void refreshFromHamiltonian() {
    m_AppliedCE.refreshFromHamiltonian();
    m_CorrelationDelta = MSMath.arraySubtractTolerateNull(m_AppliedCE.getCorrelations(), m_TargetCorrelations);
    m_ScoreCalculator.refreshRegularizer();
  }
  
  // TODO come up with a more clever way of doing this, provided the composition of each sublattice.
  public void setTargetCorrelations(double[][] correlations) {
    m_TargetCorrelations = ArrayUtils.copyArray(correlations);
    m_CorrelationDelta = MSMath.arraySubtractTolerateNull(m_AppliedCE.getCorrelations(), m_TargetCorrelations);
  }

  public double getValue() {
    
    return m_ScoreCalculator.getScore();
    
  }

  @Override
  public int getQuickSigmaState(int sigmaIndex) {
    return m_AppliedCE.getQuickSigmaState(sigmaIndex);
  }

  @Override
  public int[] getQuickSigmaStates(int[] template) {
    return m_AppliedCE.getQuickSigmaStates(template);
  }

  @Override
  public double getDelta(int sigmaIndex, int newState) {
    double[][] correlationDelta = m_AppliedCE.getCorrelationDelta(sigmaIndex, newState);
    
    for (int clustNum = 0; clustNum < correlationDelta.length; clustNum++) {
      if (m_CorrelationDelta[clustNum] == null) {continue;}
      for (int functNum = 0; functNum < correlationDelta[clustNum].length; functNum++) {
        correlationDelta[clustNum][functNum] += m_CorrelationDelta[clustNum][functNum];
      }
    }
    return m_ScoreCalculator.getDelta(correlationDelta);
  }

  @Override
  public double getDelta(int[] sigmaIndices, int[] newStates) {
    double value = this.getValue();
    int[] oldStates = this.getQuickSigmaStates(null);
    this.setSigmaStates(sigmaIndices, newStates);
    double delta = this.getValue() - value;
    this.setSigmaStates(sigmaIndices, oldStates);
    return delta;
  }

  @Override
  public double setSigmaStateGetDelta(int sigmaIndex, int newState) {
    double[][] correlationDelta = m_AppliedCE.setSigmaStateGetCorrelationDelta(sigmaIndex, newState);
    for (int clustNum = 0; clustNum < correlationDelta.length; clustNum++) {
      if (m_CorrelationDelta[clustNum] == null) {continue;}
      for (int functNum = 0; functNum < correlationDelta[clustNum].length; functNum++) {
        correlationDelta[clustNum][functNum] += m_CorrelationDelta[clustNum][functNum];
      }
    }
    double returnValue = m_ScoreCalculator.getDelta(correlationDelta);
    m_CorrelationDelta = correlationDelta;
    
    return returnValue;
  }
  
  public void setSigmaState(int sigmaIndex, int siteState) {
    super.setSigmaState(sigmaIndex, siteState);
    double[][] correlationDelta = m_AppliedCE.setSigmaStateGetCorrelationDelta(sigmaIndex, siteState);
    for (int clustNum = 0; clustNum < correlationDelta.length; clustNum++) {
      if (m_CorrelationDelta[clustNum] == null) {continue;}
      for (int functNum = 0; functNum < correlationDelta[clustNum].length; functNum++) {
        m_CorrelationDelta[clustNum][functNum] += correlationDelta[clustNum][functNum];
      }
    }
  }
  
  private abstract class AbstractScoreCalculator {

    protected double[] m_RegParams;
    protected ClusterGroup[] m_ActiveGroups;
    
    public AbstractScoreCalculator(ICERegGenerator regGenerator, double[] regParams) {
      m_ActiveGroups =  regGenerator.getActiveGroups();
      m_RegParams = ArrayUtils.copyArray(regParams);
    }
    
    public abstract double getScore();
    public abstract double getDelta(double[][] newCorrelations);
    public abstract void refreshRegularizer();
  }
  
  private class FullScoreCalculator extends AbstractScoreCalculator {
    
    private AbstractCoupledRegGenerator m_RegGenerator;
    private double[][] m_CovarianceMatrix;

    private FullScoreCalculator(AbstractCoupledRegGenerator regGenerator, double[] regParams) {
      super(regGenerator, regParams);
      m_RegGenerator = regGenerator;
    }
    
    public void refreshRegularizer() {
      double[][] regularizer = m_RegGenerator.getRegularizer(m_RegParams, null);
      m_CovarianceMatrix = new MatrixInverter(regularizer).getInverseArray();
    }
    
    @Override
    public double getScore() {
      
      double returnValue = 0;
      double[] vec1 = new double[m_CovarianceMatrix.length];
      
      for (int varNum = 0; varNum < m_CovarianceMatrix.length; varNum++) {
        int currIndex = 0;
        for (int clustIndex = 0; clustIndex < m_ActiveGroups.length; clustIndex++) {
          int clustNum = m_ActiveGroups[clustIndex].getGroupNumber();
          for (int functNum = 0; functNum < m_CorrelationDelta[clustNum].length; functNum++) {
            vec1[varNum] += m_CovarianceMatrix[varNum][currIndex++] * m_CorrelationDelta[clustNum][functNum];
          }
        }
      }
      
      int currIndex = 0;
      for (int clustIndex = 0; clustIndex < m_ActiveGroups.length; clustIndex++) {
        int clustNum = m_ActiveGroups[clustIndex].getGroupNumber();
        for (int functNum = 0; functNum < m_CorrelationDelta[clustNum].length; functNum++) {
          returnValue += vec1[currIndex++] * m_CorrelationDelta[clustNum][functNum];
        }
      }
      return returnValue;
    }
    
    public double getDelta(double[][] newCorrelations) {
      
      double returnValue = 0;
      
      double[] vec1 = new double[m_CovarianceMatrix.length];
      double[] newVec1 = new double[m_CovarianceMatrix.length];
      
      for (int varNum = 0; varNum < m_CovarianceMatrix.length; varNum++) {
        int currIndex = 0;
        for (int clustIndex = 0; clustIndex < m_ActiveGroups.length; clustIndex++) {
          int clustNum = m_ActiveGroups[clustIndex].getGroupNumber();
          for (int functNum = 0; functNum < m_CorrelationDelta[clustNum].length; functNum++) {
            vec1[varNum] += m_CovarianceMatrix[varNum][currIndex++] * m_CorrelationDelta[clustNum][functNum];
            newVec1[varNum] += m_CovarianceMatrix[varNum][currIndex++] * newCorrelations[clustNum][functNum];
          }
        }
      }
      
      int currIndex = 0;
      for (int clustIndex = 0; clustIndex < m_ActiveGroups.length; clustIndex++) {
        int clustNum = m_ActiveGroups[clustIndex].getGroupNumber();
        for (int functNum = 0; functNum < m_CorrelationDelta[clustNum].length; functNum++) {
          returnValue += newVec1[currIndex++] * newCorrelations[clustNum][functNum];
          returnValue -= vec1[currIndex++] * m_CorrelationDelta[clustNum][functNum];
        }
      }
      return returnValue;
    }
    
  }
  
  private class LinearScoreCalculator extends AbstractScoreCalculator {
    
    private ILinearCERegGenerator m_RegGenerator;
    private double[] m_VarianceArray;

    private LinearScoreCalculator(ILinearCERegGenerator regGenerator, double[] regParams) {
      super(regGenerator, regParams);
      m_RegGenerator = regGenerator;
      this.refreshRegularizer();
    }
    
    public void refreshRegularizer() {
      double[] regularizer = m_RegGenerator.getRegularizer(m_RegParams, null);
      m_VarianceArray = new double[regularizer.length];
      for (int varNum = 0; varNum < regularizer.length; varNum++) {
        m_VarianceArray[varNum] = 1.0 / regularizer[varNum];
      }
    }
    
    @Override
    public double getScore() {
      double returnValue = 0;
      int currIndex = 0;
      for (int clustIndex = 0; clustIndex < m_ActiveGroups.length; clustIndex++) {
        int clustNum = m_ActiveGroups[clustIndex].getGroupNumber();
        for (int functNum = 0; functNum < m_CorrelationDelta[clustNum].length; functNum++) {
          returnValue += m_VarianceArray[currIndex++] * m_CorrelationDelta[clustNum][functNum] * m_CorrelationDelta[clustNum][functNum];
        }
      }
      return returnValue;
    }
    
    public double getDelta(double[][] newCorrelations) {
      double returnValue = 0;
      int currIndex = 0;
      for (int clustIndex = 0; clustIndex < m_ActiveGroups.length; clustIndex++) {
        int clustNum = m_ActiveGroups[clustIndex].getGroupNumber();
        for (int functNum = 0; functNum < m_CorrelationDelta[clustNum].length; functNum++) {
          double sqCorrelationDelta = newCorrelations[clustNum][functNum] * newCorrelations[clustNum][functNum];
          sqCorrelationDelta -= m_CorrelationDelta[clustNum][functNum] * m_CorrelationDelta[clustNum][functNum];
          returnValue += m_VarianceArray[currIndex++] * sqCorrelationDelta;
        }
      }
      return returnValue;
    }
    
  }
  
}
