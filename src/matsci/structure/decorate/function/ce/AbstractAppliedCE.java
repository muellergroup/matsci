package matsci.structure.decorate.function.ce;

import matsci.*;
import matsci.structure.decorate.function.AppliedDecorationFunction;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.structure.superstructure.SuperStructure;

/**
 * <p>This is an abstract class representing a cluster expansion applied to a structure.
 * The algorithms used to apply the cluster expansion to a structure should be implemented
 * in a class that extends this one.</p>
 * 
 * <p> The following definitions should help you understand the documentation for this class:
 * <br> <b>Allowed Specie</b>  This is the finite set of species that are allowed by the Hamiltonian to 
 * occupy a given site.
 * <br> <b>AbstractSite State</b>  This is an integer assigned uniquely to each species allowed at a given site.  The
 * site states range from 0 to one less than the number of allowed species.
 * <br> <b>Sigma AbstractSite</b>  This is a site on a structure for which the number of allosed species is greater than one.
 * <br> <b>Sigma Index</b>  This is an integer assigned uniquely to each sigma site.  The sigma inidices range from 
 * 0 to one less than the number of sigma sites.  This allows for rapid iteration over only the sigma sites.
 * <br> <b>Cluster Group </b> A set of all symmetrically equivalent clusters.
 * <br> <b>Correlations </b> In the context of a cluster expansion, correlations are the average values the cluster functions
 * across all clusters in a cluster group.
 * </p> 
 * 
 * <p>Copyright: Copyright (c) 2003</p>
 * @author Tim Mueller
 * @version 1.0
 */

public abstract class AbstractAppliedCE extends AppliedDecorationFunction   {

  protected double m_ChargePenalty;
  
/**
  *
  * @param clusterExpansion The cluster expansion we want to apply to the structure
  * @param structure The structure we will aply the cluster expansion to
  */
  public AbstractAppliedCE(ClusterExpansion clusterExpansion, SuperStructure structure) {
    super(clusterExpansion, structure);
  }

  /**
   * 
   * @param clusterExpansion The cluster expansion 
   * @param superToDirect Each row of this matrix represents a vector in a supercell.  The supercell
   * vectors are a linear combination of the vectors that make up the defining cell of the cluster expansion, 
   * where the coefficients of the linear combination are given by the elements of the row.
   */
  public AbstractAppliedCE(ClusterExpansion clusterExpansion, int[][] superToDirect) {
    super(clusterExpansion, superToDirect);
  }

  /**
   * 
   * @return The cluster expansion we applied to the Hamiltonian
   */
  public ClusterExpansion getClusterExpansion() {
    return (ClusterExpansion) this.getHamiltonian();
  }

  /**
   * 
   * @param sigmaIndex A sigma index for the site we want to operate on
   * @param newState The site state corresponding to the species we want to assign to the sigma site
   * @return An array that contains the deltas in correlations when we change the occupancy of the
   * sigma site.  The first index of the array corresponds to the cluster groups, and the second 
   * index corresponds to the function number within that group.  For groups that have not been activated 
   * null is returned.
   */
  public abstract double[][] setSigmaStateGetCorrelationDelta(int sigmaIndex, int newState);
  
  /**
   * This method doesn't actually change the state of the sigma site
   * 
   * @param sigmaIndex A sigma index for the site we want to operate on
   * @param newState The site state corresponding to the species we want to assign to the sigma site
   * @return An array that contains the deltas in correlations when we change the occupancy of the
   * sigma site.  The first index of the array corresponds to the cluster groups, and the second 
   * index corresponds to the function number within that group.  For groups that have not been activated
   * null is returned.
   */
  public abstract double[][] getCorrelationDelta(int sigmaIndex, int newState);
  
  
  /**
   * 
   * @return An array that contains the correlations given the current occupation.
   * The first index of the array corresponds to the cluster groups, and the second 
   * index corresponds to the function number within that group.
   */
  public abstract double[][] getCorrelations();
  
  /**
   * 
   * @return The correlations for the given groups all flattened into one array
   */
  public double[] getCorrelations(ClusterGroup[] activeGroups) {
    double[][] correlations = this.getCorrelations();
    int numFunctions = 0;
    
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      int groupNum = activeGroups[groupIndex].getGroupNumber();
      double[] groupCorrelations = correlations[groupNum];
      if (groupCorrelations == null) {
        throw new RuntimeException("Correlations requested for cluster orbit that is not active.");
      }
      numFunctions += groupCorrelations.length;
    }
    
    int currIndex = 0;
    double[] returnArray = new double[numFunctions];
    for (int groupIndex = 0; groupIndex < activeGroups.length; groupIndex++) {
      int groupNum = activeGroups[groupIndex].getGroupNumber();
      double[] groupCorrelations = correlations[groupNum];
      System.arraycopy(groupCorrelations, 0, returnArray, currIndex, groupCorrelations.length);
      currIndex += groupCorrelations.length;
    }
    return returnArray;
  }
  
  /**
   * 
   * @param group Consider the clusters in the given group when evaluating this cluster expansion
   * on this structure.
   * @return True if the group was not already activated, false otherwise.
   */
  public abstract boolean activateGroup(ClusterGroup group);
  
  /**
   * 
   * @param groups Consider the clusters in the given group when evaluating this cluster expansion
   * on this structure.
   */
  public abstract void activateGroups(ClusterGroup[] groups);
  
  
  /**
   * 
   * @param group Remove the clusters in this group from consideration when evaluating this cluster
   * expansion on this structure
   * @return True if the group was activated, false otherwise.
   */
  public abstract boolean deactivateGroup(ClusterGroup group);
  
  /**
   * This method activates all cluster groups known to the cluster expansion, meaning that they will
   * all be considered when evaluating this cluster expansion applied to this structure.
   *
   */
  public abstract void deactivateAllGroups();

  /**
   * This method deactivates all cluster groups known to the cluster expansion, meaning that they will not
   * be considered when evaluating this cluster expansion applied to this structure.  This is faster
   * than deactivating all the groups individually.  Of course some groups need to be activated before you 
   * evaluate the cluster expansion.
   *
   */
  public abstract void activateAllGroups();
  
  /**
   * 
   * @param group A group in the cluster expansion that is being applied to the structure.
   * @return True if the group has been activated, false otherwise.
   */
  public abstract boolean isGroupActive(ClusterGroup group);
  
  /**
   * 
   * @return An array with no null entries of the cluster groups that are currently active
   */
  public abstract ClusterGroup[] getActiveGroups();
  
  

}
