/*
 * Created on Mar 30, 2007
 *
 */
package matsci.structure.decorate.function.ce;

import matsci.Species;
import matsci.structure.decorate.function.ce.clusters.ClusterGroup;
import matsci.util.arrays.*;

public class AppliedCECorrelationWrapper extends AbstractAppliedCE {

  private double[][] m_Correlations;
  private AbstractAppliedCE m_AppliedCE;
  
  public AppliedCECorrelationWrapper(AbstractAppliedCE appliedCE) {
    super(appliedCE.getClusterExpansion(), appliedCE.getSuperStructure());
    m_AppliedCE = appliedCE;
    m_Correlations = m_AppliedCE.getCorrelations();
  }
  
  public void setSigmaState(int sigmaIndex, int newState) {
    double[][] correlationDelta = m_AppliedCE.setSigmaStateGetCorrelationDelta(sigmaIndex, newState);
    this.incrementInternalCorrelations(correlationDelta);
  }

  public double[][] setSigmaStateGetCorrelationDelta(int sigmaIndex, int newState) {
    double[][] correlationDelta = m_AppliedCE.setSigmaStateGetCorrelationDelta(sigmaIndex, newState);
    this.incrementInternalCorrelations(correlationDelta);
    return correlationDelta;
  }

  public double[][] getCorrelationDelta(int sigmaIndex, int newState) {
    return m_AppliedCE.getCorrelationDelta(sigmaIndex, newState);
  }

  public double[][] getCorrelations() {
    return ArrayUtils.copyArray(m_Correlations);
  }

  public boolean activateGroup(ClusterGroup group) {
    boolean returnValue = m_AppliedCE.activateGroup(group);
    if (returnValue) {m_Correlations = m_AppliedCE.getCorrelations();}
    return returnValue;
  }
  
  public void activateGroups(ClusterGroup[] groups) {
    m_AppliedCE.activateGroups(groups);
    m_Correlations = m_AppliedCE.getCorrelations();
  }

  public boolean deactivateGroup(ClusterGroup group) {
    boolean returnValue = m_AppliedCE.deactivateGroup(group);
    if (returnValue) {m_Correlations = m_AppliedCE.getCorrelations();}
    return returnValue;
  }

  public void deactivateAllGroups() {
    m_AppliedCE.deactivateAllGroups();
    m_Correlations = m_AppliedCE.getCorrelations();
  }

  public void activateAllGroups() {
    m_AppliedCE.activateAllGroups();
    m_Correlations = m_AppliedCE.getCorrelations();
  }

  public boolean isGroupActive(ClusterGroup group) {
    return m_AppliedCE.isGroupActive(group);
  }

  public ClusterGroup[] getActiveGroups() {
    return m_AppliedCE.getActiveGroups();
  }

  public void setChemPot(Species spec, double chemicalPotential) {
    m_AppliedCE.setChemPot(spec, chemicalPotential);
  }

  public double getChemPot(Species spec) {
    return m_AppliedCE.getChemPot(spec);
  }

  public int getQuickSigmaState(int sigmaIndex) {
    return m_AppliedCE.getQuickSigmaState(sigmaIndex);
  }

  public int[] getQuickSigmaStates(int[] template) {
    return m_AppliedCE.getQuickSigmaStates(template);
  }

  public double getDelta(int sigmaIndex, int newState) {
    return m_AppliedCE.getDelta(sigmaIndex, newState);
  }

  public double getDelta(int[] sigmaIndices, int[] newStates) {
    return m_AppliedCE.getDelta(sigmaIndices, newStates);
  }

  public double setSigmaStateGetDelta(int sigmaIndex, int newState) {
    double[][] correlationDelta = m_AppliedCE.getCorrelationDelta(sigmaIndex, newState);
    this.incrementInternalCorrelations(correlationDelta);
    return m_AppliedCE.setSigmaStateGetDelta(sigmaIndex, newState);
  }

  public void refreshFromHamiltonian() {
    m_AppliedCE.refreshFromHamiltonian();
    m_Correlations = m_AppliedCE.getCorrelations();
  }

  public double getValue() {
    return m_AppliedCE.getValue();
  }
  
  private void incrementInternalCorrelations(double[][] correlationDelta) {
    
    if (correlationDelta == null) {return;}
    
    for (int groupNum = 0; groupNum < m_Correlations.length; groupNum++) {
      double[] groupCorrelations = m_Correlations[groupNum];
      if (groupCorrelations == null || groupCorrelations.length == 0) {continue;}
      double[] groupDelta = correlationDelta[groupNum];
      if (correlationDelta == null || correlationDelta.length == 0) {
        throw new RuntimeException("Correlation delta does not update all known correlations");
      }
      for (int functNum = 0; functNum < groupCorrelations.length; functNum++) {
        groupCorrelations[functNum] += groupDelta[functNum];
      }
    }
    
  }
  
  /*
  protected void incrementInternalCorrelations(double[][] increment) {
    for (int groupNum = 0; groupNum < increment.length; groupNum++) {
      double[] incrementForGroup = increment[groupNum];
      if (incrementForGroup == null) {continue;}
      double[] correlationsForGroup = m_Correlations[groupNum];
      for (int functGroupNum = 0; functGroupNum < incrementForGroup.length; functGroupNum++) {
        correlationsForGroup[functGroupNum] += incrementForGroup[functGroupNum];
      }
    }
  }*/

}
