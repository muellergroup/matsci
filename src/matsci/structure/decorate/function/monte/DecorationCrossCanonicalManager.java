/*
 * Created on Sep 19, 2005
 *
 */
package matsci.structure.decorate.function.monte;

import java.util.BitSet;
import java.util.Random;

import matsci.Species;
import matsci.engine.monte.metropolis.IAllowsMetropolis;
import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.structure.decorate.function.AppliedDecorationFunction;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 * Allows swaps between different sublattices
 * 
 * Might generate a lot of failed (disallowed) swaps.
 * 
 */
public class DecorationCrossCanonicalManager implements IAllowsMetropolis {

  private AppliedDecorationFunction m_AppliedFunction;
  private Random m_Generator;
  private Event m_Event;
  
  private Species[] m_SigmaSpecies;
  private boolean[][] m_AllowedIndicesBySpecies;
  private int[] m_SpeciesIndexBySigmaIndex;
  
  public DecorationCrossCanonicalManager(AppliedDecorationFunction ah) {
    m_AppliedFunction = ah;
    m_Generator = new Random();
    m_SigmaSpecies =new Species[0];
    m_AllowedIndicesBySpecies = new boolean[0][];
    m_Event = new Event();
    
    this.refreshFromStructure();
  }
  
  public void refreshFromStructure() {
    
    int numSigmaSites = m_AppliedFunction.numSigmaSites();
    
    m_SpeciesIndexBySigmaIndex = new int[numSigmaSites];
    for (int sigmaIndex = 0; sigmaIndex < m_SpeciesIndexBySigmaIndex.length; sigmaIndex++) {
      Species[] allowedSpecies = m_AppliedFunction.getAllowedSpecies(sigmaIndex);
      Species siteSpecies = allowedSpecies[m_AppliedFunction.getQuickSigmaState(sigmaIndex)];
      int speciesIndex = ArrayUtils.findIndex(m_SigmaSpecies, siteSpecies);
      if (speciesIndex < 0) {
        speciesIndex = m_SigmaSpecies.length; 
        m_SigmaSpecies = (Species[]) ArrayUtils.appendElement(m_SigmaSpecies, siteSpecies);
        m_AllowedIndicesBySpecies = ArrayUtils.appendElement(m_AllowedIndicesBySpecies, new boolean[numSigmaSites]);
        for (int allowedSpecIndex = 0; allowedSpecIndex < numSigmaSites; allowedSpecIndex++) {
          if (m_AppliedFunction.allowsSpecies(allowedSpecIndex, siteSpecies)) {
            m_AllowedIndicesBySpecies[speciesIndex][allowedSpecIndex] = true;
          }
        }
      }
      m_SpeciesIndexBySigmaIndex[sigmaIndex] = speciesIndex;
    }

  }
  
  public AppliedDecorationFunction getAppliedHamiltonian() {
    return m_AppliedFunction;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getValue()
   */
  public double getValue() {
    return m_AppliedFunction.getValue();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getRandomEvent()
   */
  public IMetropolisEvent getRandomEvent() {
    
    boolean isSwapAllowed = false;
    int sigmaIndex1 = -1;
    int sigmaIndex2 = -1;

    BitSet triedSwaps = new BitSet();
    int numPossibleSwaps = m_SpeciesIndexBySigmaIndex.length * m_SpeciesIndexBySigmaIndex.length;
    
    while (!isSwapAllowed) {
      /*if (triedSwaps.cardinality() == numPossibleSwaps) { // Evaluating cardinality takes a very long time
        throw new RuntimeException("All swaps have been tried and none are allowed!");
      }*/
      sigmaIndex1 = m_Generator.nextInt(m_SpeciesIndexBySigmaIndex.length);
      sigmaIndex2 = m_Generator.nextInt(m_SpeciesIndexBySigmaIndex.length);
      int swapIndex = sigmaIndex2 * m_SpeciesIndexBySigmaIndex.length + sigmaIndex1;
      //triedSwaps.set(swapIndex);
      
      int newSpeciesIndex1 = m_SpeciesIndexBySigmaIndex[sigmaIndex2];
      int newSpeciesIndex2 = m_SpeciesIndexBySigmaIndex[sigmaIndex1];
      
      isSwapAllowed = (newSpeciesIndex1 != newSpeciesIndex2); // Don't swap the same species
      isSwapAllowed &= m_AllowedIndicesBySpecies[newSpeciesIndex1][sigmaIndex1] && m_AllowedIndicesBySpecies[newSpeciesIndex2][sigmaIndex2];
    }

    m_Event.setSwap(sigmaIndex1, sigmaIndex2);
    return m_Event;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#setState(java.lang.Object)
   */
  public void setState(Object snapshot) {
    m_AppliedFunction.setSigmaStates((int[]) snapshot);
    this.refreshFromStructure();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#getSnapshot(java.lang.Object)
   */
  public Object getSnapshot(Object template) {
    return m_AppliedFunction.getQuickSigmaStates((int[]) template);
  }
  
  public class Event implements IMetropolisEvent {

    private int[] m_SigmaIndicesToSwap = new int[2];
    private int[] m_NewSiteStates = new int[2];
    private int[] m_OldSiteStates = new int[2];
    
    private int m_NewSpeciesIndex1;
    private int m_NewSpeciesIndex2;
    
    public void setSwap(int sigmaIndex1, int sigmaIndex2) {
      
      m_NewSpeciesIndex1 = m_SpeciesIndexBySigmaIndex[sigmaIndex2];
      m_NewSpeciesIndex2 = m_SpeciesIndexBySigmaIndex[sigmaIndex1];
      
      m_SigmaIndicesToSwap[0] = sigmaIndex1;
      m_SigmaIndicesToSwap[1] = sigmaIndex2;
      m_NewSiteStates[0] = m_AppliedFunction.getStateForSpecies(sigmaIndex1, m_SigmaSpecies[m_NewSpeciesIndex1]);
      m_NewSiteStates[1] = m_AppliedFunction.getStateForSpecies(sigmaIndex2, m_SigmaSpecies[m_NewSpeciesIndex2]);
	    m_OldSiteStates[0] = m_AppliedFunction.getQuickSigmaState(sigmaIndex1);
	    m_OldSiteStates[1] = m_AppliedFunction.getQuickSigmaState(sigmaIndex2);
    }
    
	  /* (non-Javadoc)
	   * @see matsci.engine.monte.metropolis.IMetropolisEvent#trigger()
	   */
	  public void trigger() {
	    
	    m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[0]] = m_NewSpeciesIndex1;
	    m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[1]] = m_NewSpeciesIndex2;
	    
	    m_AppliedFunction.setSigmaStates(m_SigmaIndicesToSwap, m_NewSiteStates);
	  }
	
	  /* (non-Javadoc)
	   * @see matsci.engine.monte.metropolis.IMetropolisEvent#getDelta()
	   */
	  public double getDelta() {
	    
	    return m_AppliedFunction.getDelta(m_SigmaIndicesToSwap, m_NewSiteStates);

	  }
	
	  /* (non-Javadoc)
	   * @see matsci.engine.monte.metropolis.IMetropolisEvent#triggerGetDelta()
	   */
	  public double triggerGetDelta() {
	    
	    m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[0]] = m_NewSpeciesIndex1;
	    m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[1]] = m_NewSpeciesIndex2;
	    
	    return m_AppliedFunction.setSigmaStatesGetDelta(m_SigmaIndicesToSwap, m_NewSiteStates);
	  }
	
	  /* (non-Javadoc)
	   * @see matsci.engine.monte.metropolis.IMetropolisEvent#reverse()
	   */
	  public void reverse() {
	    
	    m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[0]] = m_NewSpeciesIndex2;
	    m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[1]] = m_NewSpeciesIndex1;
	    
	    m_AppliedFunction.setSigmaStates(m_SigmaIndicesToSwap, m_OldSiteStates);
	  }
  }


}
