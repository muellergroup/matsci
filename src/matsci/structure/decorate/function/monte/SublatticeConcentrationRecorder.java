/*
 * Created on Dec 20, 2007
 *
 */
package matsci.structure.decorate.function.monte;

import matsci.engine.monte.GroundStateRecorder;
import matsci.engine.monte.IAllowsSnapshot;
import matsci.structure.decorate.*;
import matsci.structure.decorate.function.*;

public class SublatticeConcentrationRecorder extends GroundStateRecorder {

  protected AppliedDecorationFunction m_AppliedFunction;
  protected double[][] m_AverageStateCounts;
  
  public SublatticeConcentrationRecorder(IAllowsSnapshot system, AppliedDecorationFunction appliedFunction) {
    super(system);
    m_AppliedFunction = appliedFunction;
    DecorationTemplate decorationTemplate = appliedFunction.getHamiltonian();
    m_AverageStateCounts = new double[decorationTemplate.numSublattices()][];
    for (int subLatticeIndex = 0; subLatticeIndex < decorationTemplate.numSublattices(); subLatticeIndex++) {
      int numStates = decorationTemplate.getAllowedSpeciesForSublattice(subLatticeIndex).length;
      m_AverageStateCounts[subLatticeIndex] = new double[numStates];
    }
    
  }
  
  public void recordEndState(double value, double weight) {
    double oldTotalWeight = m_TotalWeight;
    double newTotalWeight = oldTotalWeight + weight;
    super.recordEndState(value, weight);
    double correctionRatio = oldTotalWeight / newTotalWeight;
    double incrementRatio = (1 - correctionRatio);
    for (int sublatticeIndex = 0; sublatticeIndex < m_AverageStateCounts.length; sublatticeIndex++) {
      double[] stateCounts = m_AverageStateCounts[sublatticeIndex];
      for (int state = 0; state < stateCounts.length; state++) {
        stateCounts[state] *= correctionRatio;
      }
    }
    
    for (int sigmaIndex = 0; sigmaIndex < m_AppliedFunction.numSigmaSites(); sigmaIndex++) {
      int state = m_AppliedFunction.getQuickSigmaState(sigmaIndex);
      int sublattice = m_AppliedFunction.getSublattice(sigmaIndex);
      m_AverageStateCounts[sublattice][state] += incrementRatio;
    }
  }
  
  public double getAverageStateCount(int sublatticeIndex, int state) {
    return m_AverageStateCounts[sublatticeIndex][state];
  }
  
}
