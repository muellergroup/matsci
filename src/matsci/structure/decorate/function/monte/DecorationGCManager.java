/*
 * Created on Mar 30, 2005
 *
 */
package matsci.structure.decorate.function.monte;

import java.util.Random;

import matsci.engine.monte.metropolis.IAllowsMetropolis;
import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.structure.decorate.function.AppliedDecorationFunction;
import matsci.util.arrays.ArrayUtils;
/**
 * @author Tim Mueller
 *
 */
public class DecorationGCManager implements IAllowsMetropolis {

  private final AppliedDecorationFunction m_AppliedFunction;
  private final Event m_Event;
  private static final Random m_Generator = new Random();
  
  private int[] m_SigmaIndicesInSublattice;
  
  private int[][] m_SigmaIndices = new int[0][]; // The sigma indices grouped by the number of allowed changes at each site
  private int[] m_NumAllowedChanges = new int[0]; // The corresponding number of allowed changes at each site
  private int m_NumTotalChanges = 0;

  public DecorationGCManager(AppliedDecorationFunction appliedFunction, int[] sigmaIndicesToSwap) {
    m_AppliedFunction = appliedFunction;
    m_Event = new Event(this);

    m_SigmaIndicesInSublattice = ArrayUtils.copyArray(sigmaIndicesToSwap);
    
    for (int siteIndex =0; siteIndex < m_SigmaIndicesInSublattice.length; siteIndex++) {
      int sigmaIndex = m_SigmaIndicesInSublattice[siteIndex];
      int numAllowedChanges = appliedFunction.numAllowedSpecies(sigmaIndex) - 1;
      int siteType = 0;
      for (siteType = 0; siteType < m_NumAllowedChanges.length; siteType++) {
        if (m_NumAllowedChanges[siteType] == numAllowedChanges) {break;}
      }
      
      if (siteType == m_NumAllowedChanges.length) {
        m_NumAllowedChanges = ArrayUtils.appendElement(m_NumAllowedChanges, numAllowedChanges);
        m_SigmaIndices = ArrayUtils.appendElement(m_SigmaIndices, new int[0]);
      }
      
      m_SigmaIndices[siteType] = ArrayUtils.appendElement(m_SigmaIndices[siteType], sigmaIndex);
      m_NumTotalChanges += numAllowedChanges;
    }
  }
  
  public DecorationGCManager(AppliedDecorationFunction appliedFunction) {

    this(appliedFunction, getDefaultSublatticeIndices(appliedFunction));
  }
  
  protected static int[] getDefaultSublatticeIndices(AppliedDecorationFunction appliedFunction) {
    int[] sublatticeIndices = new int[appliedFunction.numSigmaSites()];
    for (int sigmaIndex = 0; sigmaIndex < sublatticeIndices.length; sigmaIndex++) {
      sublatticeIndices[sigmaIndex] = sigmaIndex;
    }
    return sublatticeIndices;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getValue()
   */
  public double getValue() {
    return m_AppliedFunction.getValue();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getRandomEvent()
   */
  public IMetropolisEvent getRandomEvent() {
    
    int randInt = m_Generator.nextInt(m_NumTotalChanges);
    int siteType;
    for (siteType = 0; siteType < m_SigmaIndices.length; siteType++) {
      int numChangesForType = m_SigmaIndices[siteType].length * m_NumAllowedChanges[siteType];
      if (randInt < numChangesForType) {break;}
      randInt -= numChangesForType;
    }
    
    int[] sigmaIndices = m_SigmaIndices[siteType];
    int numAllowedChanges = m_NumAllowedChanges[siteType];
    
    m_Event.m_SigmaIndex = sigmaIndices[randInt / numAllowedChanges];
    int changeID = randInt % numAllowedChanges;
    m_Event.m_LastState = m_AppliedFunction.getQuickSigmaState(m_Event.m_SigmaIndex);
    m_Event.m_NextState = (m_Event.m_LastState + changeID + 1) % (numAllowedChanges + 1);
    return m_Event;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#setState(java.lang.Object)
   */
  public void setState(Object snapshot) {
    m_AppliedFunction.setSigmaStates((int[]) snapshot);
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#getSnapshot(java.lang.Object)
   */
  public Object getSnapshot(Object template) {
    return m_AppliedFunction.getQuickSigmaStates((int[])template);
  }
  
  public AppliedDecorationFunction getAppliedHamiltonian() {
    return m_AppliedFunction;
  }
  
  protected class Event implements IMetropolisEvent {

    private DecorationGCManager m_Manager;
    private int m_SigmaIndex;
    private int m_LastState;
    private int m_NextState;
    
    public Event(DecorationGCManager manager) {
      m_Manager = manager;
    }
    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#trigger()
     */
    public void trigger() {
      m_Manager.m_AppliedFunction.setSigmaState(m_SigmaIndex, m_NextState);
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#getDelta()
     */
    public double getDelta() {
      return m_Manager.m_AppliedFunction.getDelta(m_SigmaIndex, m_NextState);
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#triggerGetDelta()
     */
    public double triggerGetDelta() {
      return m_Manager.m_AppliedFunction.setSigmaStateGetDelta(m_SigmaIndex, m_NextState);
    }

    /* (non-Javadoc)
     * @see matsci.engine.monte.metropolis.IMetropolisEvent#reverse()
     */
    public void reverse() {
      m_Manager.m_AppliedFunction.setSigmaState(m_SigmaIndex, m_LastState);
    }
    
  }

}
