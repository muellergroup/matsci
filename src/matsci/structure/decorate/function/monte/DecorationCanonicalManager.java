/*
 * Created on Feb 27, 2005
 *
 */
package matsci.structure.decorate.function.monte;

import java.util.Arrays;
import java.util.Random;

import matsci.Species;
import matsci.engine.monte.metropolis.IAllowsMetropolis;
import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.structure.decorate.function.AppliedDecorationFunction;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 * Forces swaps to stay within sublattices
 *
 */
public class DecorationCanonicalManager implements IAllowsMetropolis {
  
  private AppliedDecorationFunction m_AppliedFunction;
  private Random m_Generator;
  private Event m_Event;
  private NullEvent m_NullEvent = new NullEvent();
  
  private int[] m_SigmaIndicesInSublattice;
  
  private int[][][] m_SigmaIndicesByType; // We can only make swaps among sites of the same type
  private int[][] m_NumSitesByTypeAndState;
  private int[][] m_NumSwapsByTypeAndState;
  private int[] m_NumSwapsByType;
  private int m_NumTotalSwaps;
  
  public DecorationCanonicalManager(AppliedDecorationFunction ah) {
    m_AppliedFunction = ah;
    m_Generator = new Random();
    m_Event = new Event();
    
    m_SigmaIndicesInSublattice = new int[ah.numSigmaSites()];
    for (int sigmaIndex = 0; sigmaIndex < m_SigmaIndicesInSublattice.length; sigmaIndex++) {
      m_SigmaIndicesInSublattice[sigmaIndex] = sigmaIndex;
    }
    
    this.refreshFromStructure();
  }
  
  public DecorationCanonicalManager(AppliedDecorationFunction ah, int[] sublatticeIndices) {
    m_AppliedFunction = ah;
    m_Generator = new Random();
    m_Event = new Event();
    
    m_SigmaIndicesInSublattice = ArrayUtils.copyArray(sublatticeIndices);
    
    this.refreshFromStructure();
  }
  
  public AppliedDecorationFunction getAppliedHamiltonian() {
    return m_AppliedFunction;
  }
  
  public void refreshFromStructure() {
    
    Species[][] allowedSpeciesByType = new Species[0][];
    int[] numSitesByType = new int[0];
    m_NumSitesByTypeAndState = new int[0][];
    m_SigmaIndicesByType = new int[0][][];
    m_NumSwapsByTypeAndState = new int[0][];
    m_NumSwapsByType = new int[0];
    m_NumTotalSwaps = 0;
    
    for (int siteIndex = 0; siteIndex < m_SigmaIndicesInSublattice.length; siteIndex++) {
      
      int sigmaIndex = m_SigmaIndicesInSublattice[siteIndex];
      
      Species[] allowedSpecies = m_AppliedFunction.getAllowedSpecies(sigmaIndex);
      int siteState = m_AppliedFunction.getQuickSigmaState(sigmaIndex);
      
      int typeNum;
      for (typeNum = 0; typeNum < allowedSpeciesByType.length; typeNum++) {
        if (Arrays.equals(allowedSpeciesByType[typeNum], allowedSpecies)) {break;}
      }
      
      if (typeNum == allowedSpeciesByType.length) {
        allowedSpeciesByType = (Species[][]) ArrayUtils.appendElement(allowedSpeciesByType, allowedSpecies);
        m_SigmaIndicesByType = ArrayUtils.appendElement(m_SigmaIndicesByType, new int[allowedSpecies.length][0]);
        m_NumSwapsByTypeAndState = ArrayUtils.appendElement(m_NumSwapsByTypeAndState, new int[allowedSpecies.length]);
        m_NumSwapsByType = ArrayUtils.growArray(m_NumSwapsByType, 1);
        m_NumSitesByTypeAndState = ArrayUtils.appendElement(m_NumSitesByTypeAndState, new int[allowedSpecies.length]);
        numSitesByType = ArrayUtils.growArray(numSitesByType, 1);
        
      }
      
      numSitesByType[typeNum] = numSitesByType[typeNum] + 1;
      m_NumSitesByTypeAndState[typeNum][siteState] = m_NumSitesByTypeAndState[typeNum][siteState] + 1;
      
      m_SigmaIndicesByType[typeNum][siteState] = ArrayUtils.appendElement(m_SigmaIndicesByType[typeNum][siteState], sigmaIndex);
    }
    
    // Now calculate the number of allowed swaps
    for (int typeNum = 0; typeNum < m_NumSitesByTypeAndState.length; typeNum++) {
      int[] numSitesByState = m_NumSitesByTypeAndState[typeNum];
      for (int stateNum = 0; stateNum < numSitesByState.length; stateNum++) {
        m_NumSwapsByTypeAndState[typeNum][stateNum] = numSitesByState[stateNum] * (numSitesByType[typeNum] - numSitesByState[stateNum]);
        m_NumSwapsByType[typeNum] = m_NumSwapsByType[typeNum] + m_NumSwapsByTypeAndState[typeNum][stateNum];
        m_NumTotalSwaps = m_NumTotalSwaps + m_NumSwapsByTypeAndState[typeNum][stateNum];
      }
    }
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getValue()
   */
  public double getValue() {
    return m_AppliedFunction.getValue();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getRandomEvent()
   */
  public IMetropolisEvent getRandomEvent() {
    
    if (m_NumTotalSwaps == 0) {
      return m_NullEvent;
      //throw new RuntimeException("No canonical swaps possible on this structure");
    }
    
    int changeID = m_Generator.nextInt(m_NumTotalSwaps);
    int typeNum;
    for (typeNum = 0; typeNum < m_NumSwapsByType.length; typeNum++) {
      int numAllowedSwaps = m_NumSwapsByType[typeNum];
      if (changeID < numAllowedSwaps) {break;}
      changeID = changeID - numAllowedSwaps;
    }
    
    int[][] sitesByState = m_SigmaIndicesByType[typeNum];
    int stateNum;
    for (stateNum = 0; stateNum < m_NumSwapsByTypeAndState[typeNum].length; stateNum++) {
      int changesForState = m_NumSwapsByTypeAndState[typeNum][stateNum];
      if (changeID < changesForState) {break;}
      changeID = changeID - changesForState;
    }
    
    m_Event.m_InternalNewIndices[1] = changeID % m_NumSitesByTypeAndState[typeNum][stateNum];
    m_Event.m_SigmaIndices[0] = sitesByState[stateNum][m_Event.m_InternalNewIndices[1]];
    
    //m_Event.m_SigmaIndices[0] = sitesByState[stateNum][changeID % m_NumSitesByTypeAndState[typeNum][stateNum]];
    m_Event.m_NewStates[1] = stateNum;
    
    m_Event.m_SitesIndicesByState = sitesByState;
    
    // Find a site with another species to swap with
    changeID = changeID / m_NumSitesByTypeAndState[typeNum][stateNum];
    int stateNum2 = stateNum;
    for (int speciesOffset = 1; speciesOffset < sitesByState.length; speciesOffset++) {
      stateNum2 = (stateNum + speciesOffset) % sitesByState.length;
      int numSites = sitesByState[stateNum2].length;
      if (changeID < numSites) {break;}
      changeID =changeID - numSites;
    }
    
    m_Event.m_InternalNewIndices[0] = changeID;
    m_Event.m_SigmaIndices[1] = sitesByState[stateNum2][changeID];
    m_Event.m_NewStates[0] = stateNum2;
    
    return m_Event;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#setInitialState(java.lang.Object)
   */
  public void setState(Object snapshot) {
    m_AppliedFunction.setSigmaStates((int[]) snapshot);
    this.refreshFromStructure();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#getSnapshot(java.lang.Object)
   */
  public Object getSnapshot(Object template) {
    return m_AppliedFunction.getQuickSigmaStates((int[]) template);
  }
  
  public class NullEvent implements IMetropolisEvent {

    public void trigger() {
    }

    public double getDelta() {
      return 0;
    }

    public double triggerGetDelta() {
      return 0;
    }

    public void reverse() {
    }
    
  }
  
  public class Event implements IMetropolisEvent {

    private int[] m_InternalNewIndices = new int[2];
    private int[] m_NewStates = new int[2];
    private int[] m_SigmaIndices = new int[2];                                    
    private int[][] m_SitesIndicesByState;
    
	  /* (non-Javadoc)
	   * @see matsci.engine.monte.metropolis.IMetropolisEvent#trigger()
	   */
	  public void trigger() {
	    m_SitesIndicesByState[m_NewStates[0]][m_InternalNewIndices[0]] = m_SigmaIndices[0];
	    m_SitesIndicesByState[m_NewStates[1]][m_InternalNewIndices[1]] = m_SigmaIndices[1];
	    //m_AppliedFunction.setSigmaState(m_SigmaIndices[0], m_NewStates[0]);
	    //m_AppliedFunction.setSigmaState(m_SigmaIndices[1], m_NewStates[1]);
        
        m_AppliedFunction.setSigmaStates(m_SigmaIndices, m_NewStates);
	  }
	
	  /* (non-Javadoc)
	   * @see matsci.engine.monte.metropolis.IMetropolisEvent#getDelta()
	   */
	  public double getDelta() {
	    
	    return m_AppliedFunction.getDelta(m_SigmaIndices, m_NewStates);

	  }
	
	  /* (non-Javadoc)
	   * @see matsci.engine.monte.metropolis.IMetropolisEvent#triggerGetDelta()
	   */
	  public double triggerGetDelta() {
	    m_SitesIndicesByState[m_NewStates[0]][m_InternalNewIndices[0]] = m_SigmaIndices[0];
	    m_SitesIndicesByState[m_NewStates[1]][m_InternalNewIndices[1]] = m_SigmaIndices[1];
	    
        //int oldState1 = m_AppliedFunction.getQuickSigmaState(m_SigmaIndices[0]);
        //int oldState2 = m_AppliedFunction.getQuickSigmaState(m_SigmaIndices[1]);
        
	    return m_AppliedFunction.setSigmaStatesGetDelta(m_SigmaIndices, m_NewStates);
	  }
	
	  /* (non-Javadoc)
	   * @see matsci.engine.monte.metropolis.IMetropolisEvent#reverse()
	   */
	  public void reverse() {
	    m_SitesIndicesByState[m_NewStates[0]][m_InternalNewIndices[0]] = m_SigmaIndices[1];
	    m_SitesIndicesByState[m_NewStates[1]][m_InternalNewIndices[1]] = m_SigmaIndices[0];
	    m_AppliedFunction.setSigmaState(m_SigmaIndices[0], m_NewStates[1]);
	    m_AppliedFunction.setSigmaState(m_SigmaIndices[1], m_NewStates[0]);
	  }
      
      public int getSigmaIndex(int siteNum) {
        return m_SigmaIndices[siteNum];
      }
      
      public int getNewState(int siteNum) {
        return m_NewStates[siteNum];
      }
  
  }

}
