/*
 * Created on Nov 15, 2013
 *
 */
package matsci.structure.decorate.function.monte;

import java.util.Random;

import matsci.Species;
import matsci.engine.monte.metropolis.IAllowsMetropolis;
import matsci.engine.monte.metropolis.IMetropolisEvent;
import matsci.location.Coordinates;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.DiscreteBasis;
import matsci.location.basis.LinearBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.structure.decorate.function.AppliedDecorationFunction;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 * Allows swaps between different sublattices
 * 
 * Might generate a lot of failed (disallowed) swaps.
 * 
 */
public class NanoCanonicalManagerLowMem implements IAllowsMetropolis {

  private AppliedDecorationFunction m_AppliedFunction;
  private Random m_Generator;
  private Event m_Event;
  
  private Species[] m_SigmaSpecies;
  private boolean[][] m_AllowedIndicesBySpecies;
  private int[] m_SpeciesIndexBySigmaIndex;
  private boolean[] m_IsBoundarySite;
  
  //private int[][] m_NearestNeighborsBySigmaIndex;
  private int[][][] m_NearestNeighborOffsets;
  private int[][] m_NearestNeighborIndices;
  
  private int[][] m_NearestNeighborTempArray; // Where we store the calculated sigma indices of the neighbors
  
  private boolean m_AllowVacancies = true;
  private boolean m_FixShape = false;
  private boolean m_UseWideBuffer = true; // Default to true becasue in most cases max NN cluster size will be larger than two rows.
  
  public NanoCanonicalManagerLowMem(AppliedDecorationFunction ah) {
    m_AppliedFunction = ah;
    m_Generator = new Random();
    m_SigmaSpecies =new Species[0];
    m_AllowedIndicesBySpecies = new boolean[0][];
    m_Event = new Event();
    
    // TODO speed this up by using lattice translation math
    SuperStructure structure = ah.getSuperStructure();
    BravaisLattice lattice = structure.getDefiningLattice();
    double nnDist = Double.POSITIVE_INFINITY;
    SuperStructure.Site baseSite = (SuperStructure.Site) ah.getSigmaSite(0);
    int basePrimIndex = baseSite.getParentSite().getIndex();
    for (int sigmaIndex = 1; sigmaIndex < ah.numSigmaSites(); sigmaIndex++) {
      Structure.Site sigmaSite = ah.getSigmaSite(sigmaIndex);
      double distance = lattice.getNearestImageDistance(baseSite.getCoords(), sigmaSite.getCoords());
      nnDist = Math.min(nnDist, distance);
    }
    
    nnDist += 1E-2; // Tolerance
    m_NearestNeighborOffsets = new int[structure.getParentStructure().numDefiningSites()][0][];
    m_NearestNeighborIndices = new int[structure.getParentStructure().numDefiningSites()][0];
    DiscreteBasis intBasis = structure.getParentStructure().getIntegerBasis();
    double[] baseIntCoords = baseSite.getCoords().getCoordArray(intBasis);
   
    for (int sigmaIndex = 1; sigmaIndex < ah.numSigmaSites(); sigmaIndex++) {
      Structure.Site sigmaSite = ah.getSigmaSite(sigmaIndex);
      Coordinates nearestImage = lattice.getNearestImage(baseSite.getCoords(), sigmaSite.getCoords());
      double distance = baseSite.getCoords().distanceFrom(nearestImage);
      if (distance < nnDist) {
        double[] newIntCoords = nearestImage.getCoordArray(intBasis);
        
        int[] offsetArray = new int[newIntCoords.length - 1];
        for (int dimNum = 0; dimNum < offsetArray.length; dimNum++) {
          offsetArray[dimNum] = (int) Math.round(newIntCoords[dimNum] - baseIntCoords[dimNum]);
        }
        m_NearestNeighborOffsets[basePrimIndex] = ArrayUtils.appendElement(m_NearestNeighborOffsets[basePrimIndex], offsetArray);
        
        int siteIndex = (int) Math.round(newIntCoords[newIntCoords.length - 1]);
        m_NearestNeighborIndices[basePrimIndex] = ArrayUtils.appendElement(m_NearestNeighborIndices[basePrimIndex], siteIndex);        
      }
    }
    
    m_NearestNeighborTempArray = ArrayUtils.copyArray(m_NearestNeighborIndices);

    this.refreshFromStructure();
    /*
    int[] currIndices = new int[ah.numSigmaSites()];
    m_NearestNeighborsBySigmaIndex = new int[ah.numSigmaSites()][numNeighbors];
    for (int sigmaIndex = 0; sigmaIndex < m_NearestNeighborsBySigmaIndex.length; sigmaIndex++) {
      Structure.Site site1 = ah.getSigmaSite(sigmaIndex);
      for (int sigmaIndex2 = 0; sigmaIndex2 < sigmaIndex; sigmaIndex2++) {
        Structure.Site site2 = ah.getSigmaSite(sigmaIndex2);
        double distance = lattice.getNearestImageDistance(site1.getCoords(), site2.getCoords());
        if (distance < nnDist) {
          m_NearestNeighborsBySigmaIndex[sigmaIndex][currIndices[sigmaIndex]] = sigmaIndex2;
          m_NearestNeighborsBySigmaIndex[sigmaIndex2][currIndices[sigmaIndex2]] = sigmaIndex;
          currIndices[sigmaIndex]++;
          currIndices[sigmaIndex2]++;
        }
      }
    }*/
  }
  
  public void refreshFromStructure() {
    
    int numSigmaSites = m_AppliedFunction.numSigmaSites();
    
    m_SpeciesIndexBySigmaIndex = new int[numSigmaSites];
    m_IsBoundarySite = new boolean[numSigmaSites];
    
    Coordinates cartOrigin = new Coordinates(new double[] {0, 0, 0}, CartesianBasis.getInstance());
    for (int sigmaIndex = 0; sigmaIndex < m_SpeciesIndexBySigmaIndex.length; sigmaIndex++) {
      Coordinates siteCoords= m_AppliedFunction.getSigmaSite(sigmaIndex).getCoords();
      siteCoords = m_AppliedFunction.getSuperStructure().getDefiningLattice().getWignerSeitzNeighbor(cartOrigin, siteCoords);
      double[] cartCoordArray = siteCoords.getCartesianArray();
      for (int dimNum = 0; dimNum < cartCoordArray.length; dimNum++) {
        if (Math.abs(cartCoordArray[dimNum]) < CartesianBasis.getPrecision()) { // Assumes that the planes of boundary sites pass through the origin
          m_IsBoundarySite[sigmaIndex] = true;
          
          // Some extra buffer can help in some cases.  This wasn't used for first Mo-Ni-Pt paper.
          if (m_UseWideBuffer) {
            int[] neighborIndices = this.getNeighborIndices(sigmaIndex);
            for (int neighborNum = 0; neighborNum < neighborIndices.length; neighborNum++) {
              m_IsBoundarySite[neighborIndices[neighborNum]] = true;
            }
          }
          
        }
      }
      
      
      Species[] allowedSpecies = m_AppliedFunction.getAllowedSpecies(sigmaIndex);
      Species siteSpecies = allowedSpecies[m_AppliedFunction.getQuickSigmaState(sigmaIndex)];
      int speciesIndex = ArrayUtils.findIndex(m_SigmaSpecies, siteSpecies);
      if (speciesIndex < 0) {
        speciesIndex = m_SigmaSpecies.length; 
        m_SigmaSpecies = (Species[]) ArrayUtils.appendElement(m_SigmaSpecies, siteSpecies);
        m_AllowedIndicesBySpecies = ArrayUtils.appendElement(m_AllowedIndicesBySpecies, new boolean[numSigmaSites]);
        for (int allowedSpecIndex = 0; allowedSpecIndex < numSigmaSites; allowedSpecIndex++) {
          if (m_AppliedFunction.allowsSpecies(allowedSpecIndex, siteSpecies)) {
            m_AllowedIndicesBySpecies[speciesIndex][allowedSpecIndex] = true;
          }
        }
      }
      m_SpeciesIndexBySigmaIndex[sigmaIndex] = speciesIndex;
    }

  }
  
  public AppliedDecorationFunction getAppliedHamiltonian() {
    return m_AppliedFunction;
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getValue()
   */
  public double getValue() {
    return m_AppliedFunction.getValue();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.metropolis.IAllowsMetropolis#getRandomEvent()
   */
  public IMetropolisEvent getRandomEvent() {
    
    boolean isSwapAllowed = false;
    int sigmaIndex1 = -1;
    int sigmaIndex2 = -1;

    int specIndex1;
    int specIndex2;
    
    Species spec1;
    Species spec2; 
    
    while (!isSwapAllowed) {

      // The below allows vacancies in the particle
      /*while (true) {
        sigmaIndex1 = m_Generator.nextInt(m_SpeciesIndexBySigmaIndex.length);
        specIndex1 = m_SpeciesIndexBySigmaIndex[sigmaIndex1];
        spec1 = m_SigmaSpecies[specIndex1];
        if ((spec1 == Species.vacancy) && !isEdgeSite(sigmaIndex1)) {
          continue;
        }
        break;
      }
      
      while (true) {
        sigmaIndex2 = m_Generator.nextInt(m_SpeciesIndexBySigmaIndex.length);
        specIndex2 = m_SpeciesIndexBySigmaIndex[sigmaIndex2];
        spec2 = m_SigmaSpecies[specIndex2];
        if (spec1 == spec2) {continue;}
        if ((spec2 == Species.vacancy) && !isEdgeSite(sigmaIndex2)) {
          continue;
        }
        break;
      }*/
      
      boolean isBulk = false;
      while (true) {
        sigmaIndex1 = m_Generator.nextInt(m_SpeciesIndexBySigmaIndex.length);
        specIndex1 = m_SpeciesIndexBySigmaIndex[sigmaIndex1];
        spec1 = m_SigmaSpecies[specIndex1];
        if (spec1 == Species.vacancy) {continue;}
        if (!m_AllowVacancies) {isBulk = isBulkSite(sigmaIndex1);}  // Only relevant if m_AllowVacancies = false
        break;
      }
      
      while (true) {
        sigmaIndex2 = m_Generator.nextInt(m_SpeciesIndexBySigmaIndex.length);
        if (m_IsBoundarySite[sigmaIndex2]) {continue;}
        specIndex2 = m_SpeciesIndexBySigmaIndex[sigmaIndex2];
        spec2 = m_SigmaSpecies[specIndex2];
        if (spec1 == spec2) {continue;}
        if (spec2 == Species.vacancy) {
          if (m_FixShape) {continue;}
          if (isBulk) {continue;}
          if (!isEdgeSite(sigmaIndex2)) {continue;}
        }
        break;
      }
      
      int newSpeciesIndex1 = specIndex2;
      int newSpeciesIndex2 = specIndex1;
      
      //isSwapAllowed = (newSpeciesIndex1 != newSpeciesIndex2); // Don't swap the same species
      isSwapAllowed = m_AllowedIndicesBySpecies[newSpeciesIndex1][sigmaIndex1] && m_AllowedIndicesBySpecies[newSpeciesIndex2][sigmaIndex2];
    }

    m_Event.setSwap(sigmaIndex1, sigmaIndex2);
    return m_Event;
  }
  
  private boolean isBulkSite(int sigmaIndex) { // Assumes a site is occupied by an atom
    //int[] neighborIndices = m_NearestNeighborsBySigmaIndex[sigmaIndex];
    int[] neighborIndices = getNeighborIndices(sigmaIndex);
    for (int neighborNum = 0; neighborNum < neighborIndices.length; neighborNum++) {
      int neighborIndex = neighborIndices[neighborNum];
      Species neighborSpec = m_SigmaSpecies[m_SpeciesIndexBySigmaIndex[neighborIndex]];
      if (neighborSpec == Species.vacancy) {return false;}
    }
    return true;
  }
  
  private int[] getNeighborIndices(int sigmaIndex) {

    SuperStructure superStructure = m_AppliedFunction.getSuperStructure();
    DiscreteBasis intBasis = superStructure.getParentStructure().getIntegerBasis();
    double[] baseIntCoords = m_AppliedFunction.getSigmaSite(sigmaIndex).getCoords().getCoordArray(intBasis);
    int basePrimIndex = (int) Math.round(baseIntCoords[baseIntCoords.length - 1]);
    
    int[] returnArray = m_NearestNeighborTempArray[basePrimIndex];
    
    for (int neighborNum = 0; neighborNum < m_NearestNeighborOffsets[basePrimIndex].length; neighborNum++) {
      int[] offSet = m_NearestNeighborOffsets[basePrimIndex][neighborNum];
      int primSiteIndex = m_NearestNeighborIndices[basePrimIndex][neighborNum];
      for (int dimNum = 0; dimNum < offSet.length; dimNum++) {
        offSet[dimNum] += (int) Math.round(baseIntCoords[dimNum]);
      }
      int siteIndex = superStructure.getSiteIndex(offSet, primSiteIndex);
      returnArray[neighborNum] = m_AppliedFunction.getSigmaIndex(siteIndex);
      
      // Undo the temporary changes.  This is a bit messy, but prevents the creation of a new array.
      for (int dimNum = 0; dimNum < offSet.length; dimNum++) {
        offSet[dimNum] -= (int) Math.round(baseIntCoords[dimNum]);
      }
    }
    
    return returnArray;
  }
  
  private boolean isEdgeSite(int sigmaIndex) { // Assumes a site is occupied by vacancy
    //int[] neighborIndices = m_NearestNeighborsBySigmaIndex[sigmaIndex];
    int[] neighborIndices = getNeighborIndices(sigmaIndex);
    for (int neighborNum = 0; neighborNum < neighborIndices.length; neighborNum++) {
      int neighborIndex = neighborIndices[neighborNum];
      Species neighborSpec = m_SigmaSpecies[m_SpeciesIndexBySigmaIndex[neighborIndex]];
      if (neighborSpec != Species.vacancy) {return true;}
    }
    return false;
  }
  
  public void allowVacancies(boolean value) {
    m_AllowVacancies = value;
  }
  
  public boolean allowsVacancies() {
    return m_AllowVacancies;
  }
  
  public void fixShape(boolean value) {
    m_FixShape = value;
  }
  
  public boolean fixesShape() {
    return m_FixShape;
  }
  
  public void useWideBuffer(boolean value) {
    m_UseWideBuffer = value;
  }
  
  public boolean usesWideBuffer() {
    return m_UseWideBuffer;
  }


  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#setState(java.lang.Object)
   */
  public void setState(Object snapshot) {
    m_AppliedFunction.setSigmaStates((int[]) snapshot);
    this.refreshFromStructure();
  }

  /* (non-Javadoc)
   * @see matsci.engine.monte.IAllowsSnapshot#getSnapshot(java.lang.Object)
   */
  public Object getSnapshot(Object template) {
    return m_AppliedFunction.getQuickSigmaStates((int[]) template);
  }
  
  public class Event implements IMetropolisEvent {

    private int[] m_SigmaIndicesToSwap = new int[2];
    private int[] m_NewSiteStates = new int[2];
    private int[] m_OldSiteStates = new int[2];
    
    private int m_NewSpeciesIndex1;
    private int m_NewSpeciesIndex2;
    
    public void setSwap(int sigmaIndex1, int sigmaIndex2) {
      
      m_NewSpeciesIndex1 = m_SpeciesIndexBySigmaIndex[sigmaIndex2];
      m_NewSpeciesIndex2 = m_SpeciesIndexBySigmaIndex[sigmaIndex1];
      
      m_SigmaIndicesToSwap[0] = sigmaIndex1;
      m_SigmaIndicesToSwap[1] = sigmaIndex2;
      m_NewSiteStates[0] = m_AppliedFunction.getStateForSpecies(sigmaIndex1, m_SigmaSpecies[m_NewSpeciesIndex1]);
      m_NewSiteStates[1] = m_AppliedFunction.getStateForSpecies(sigmaIndex2, m_SigmaSpecies[m_NewSpeciesIndex2]);
        m_OldSiteStates[0] = m_AppliedFunction.getQuickSigmaState(sigmaIndex1);
        m_OldSiteStates[1] = m_AppliedFunction.getQuickSigmaState(sigmaIndex2);
    }
    
      /* (non-Javadoc)
       * @see matsci.engine.monte.metropolis.IMetropolisEvent#trigger()
       */
      public void trigger() {
        
        m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[0]] = m_NewSpeciesIndex1;
        m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[1]] = m_NewSpeciesIndex2;
        
        m_AppliedFunction.setSigmaStates(m_SigmaIndicesToSwap, m_NewSiteStates);
      }
    
      /* (non-Javadoc)
       * @see matsci.engine.monte.metropolis.IMetropolisEvent#getDelta()
       */
      public double getDelta() {
        
        return m_AppliedFunction.getDelta(m_SigmaIndicesToSwap, m_NewSiteStates);

      }
    
      /* (non-Javadoc)
       * @see matsci.engine.monte.metropolis.IMetropolisEvent#triggerGetDelta()
       */
      public double triggerGetDelta() {
        
        m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[0]] = m_NewSpeciesIndex1;
        m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[1]] = m_NewSpeciesIndex2;
        
        return m_AppliedFunction.setSigmaStatesGetDelta(m_SigmaIndicesToSwap, m_NewSiteStates);
      }
    
      /* (non-Javadoc)
       * @see matsci.engine.monte.metropolis.IMetropolisEvent#reverse()
       */
      public void reverse() {
        
        m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[0]] = m_NewSpeciesIndex2;
        m_SpeciesIndexBySigmaIndex[m_SigmaIndicesToSwap[1]] = m_NewSpeciesIndex1;
        
        m_AppliedFunction.setSigmaStates(m_SigmaIndicesToSwap, m_OldSiteStates);
      }
  }


}
