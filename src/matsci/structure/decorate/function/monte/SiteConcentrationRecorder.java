/*
 * Created on Jun 12, 2010
 *
 */
package matsci.structure.decorate.function.monte;

import matsci.engine.monte.GroundStateRecorder;
import matsci.engine.monte.IAllowsSnapshot;
import matsci.structure.decorate.*;
import matsci.structure.decorate.function.*;

public class SiteConcentrationRecorder extends GroundStateRecorder {

  protected AppliedDecorationFunction m_AppliedFunction;
  protected double[][] m_AverageStateCounts;
  
  public SiteConcentrationRecorder(IAllowsSnapshot system, AppliedDecorationFunction appliedFunction) {
    super(system);
    m_AppliedFunction = appliedFunction;
    m_AverageStateCounts = new double[appliedFunction.numSigmaSites()][];
    for (int sigmaIndex = 0; sigmaIndex < m_AverageStateCounts.length; sigmaIndex++) {
      int numStates = appliedFunction.getAllowedSpecies(sigmaIndex).length;
      m_AverageStateCounts[sigmaIndex] = new double[numStates];
    }
    
  }
  
  public void recordEndState(double value, double weight) {
    double oldTotalWeight = m_TotalWeight;
    double newTotalWeight = oldTotalWeight + weight;
    super.recordEndState(value, weight);
    double correctionRatio = oldTotalWeight / newTotalWeight;
    double incrementRatio = (1 - correctionRatio);
    for (int sigmaIndex = 0; sigmaIndex < m_AverageStateCounts.length; sigmaIndex++) {
      double[] stateCounts = m_AverageStateCounts[sigmaIndex];
      for (int state = 0; state < stateCounts.length; state++) {
        stateCounts[state] *= correctionRatio;
      }
    }
    
    for (int sigmaIndex = 0; sigmaIndex < m_AppliedFunction.numSigmaSites(); sigmaIndex++) {
      int state = m_AppliedFunction.getQuickSigmaState(sigmaIndex);
      m_AverageStateCounts[sigmaIndex][state] += incrementRatio;
    }
  }
  
  public double getAverageStateCount(int sigmaIndex, int state) {
    return m_AverageStateCounts[sigmaIndex][state];
  }
  
}
