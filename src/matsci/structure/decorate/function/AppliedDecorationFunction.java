package matsci.structure.decorate.function;

import java.util.Arrays;
import java.math.*;

import matsci.*;
import matsci.structure.Structure;
import matsci.structure.decorate.DecorationTemplate;
import matsci.structure.decorate.SiteDecorator;
import matsci.structure.decorate.exceptions.IllegalOccupationException;
import matsci.structure.decorate.function.ce.ClusterExpansion;
import matsci.structure.function.IAppliedStructureFunction;
import matsci.structure.superstructure.SuperStructure;
import matsci.structure.superstructure.SuperStructureDecorator;

/**
 * <p>Represents a configuration Hamiltonian applied to a structure.  
 * A "configuration Hamiltonian" is defined on a set of fixed sites where each
 * site can be populated from a finite set of species.  An example of a configuration
 * Hamiltonian is a Cluster Expansion. </p>
 * 
 * <p> The following definitions should help you understand the documentation for this class:
 * <br> <b>Allowed Specie</b>  This is the finite set of species that are allowed by the Hamiltonian to 
 * occupy a given site.
 * <br> <b>AbstractSite State</b>  This is an integer assigned uniquely to each species allowed at a given site.  The
 * site states range from 0 to one less than the number of allowed species.
 * <br> <b>Sigma AbstractSite</b>  This is a site on a structure for which the number of allosed species is greater than one.
 * <br> <b>Sigma Index</b>  This is an integer assigned uniquely to each sigma site.  The sigma inidices range from 
 * 0 to one less than the number of sigma sites.  This allows for rapid iteration over only the sigma sites.
 * <br> <b>Decoration Number</b>  This is a number uniquely assigned to a given decoration of species on the lattice.
 * Because each site can only be populated from a finite set of allowed species, the total number of unique decorations
 * is finite.  Every number between 0 and the maximum decoration number represents a unique decoration of the structure,
 * allowing for iteration through all decorations.
 * </p> 
 * 
 * <p>Copyright: Copyright (c) 2003</p>
 * 
 * @author Tim Mueller
 * @version 1.0
 */

public abstract class AppliedDecorationFunction extends SuperStructureDecorator implements IAppliedStructureFunction {

  private final DecorationTemplate m_ConfigFunction;
  private final double[] m_ChemicalPotentials;
  //private final SuperStructure m_Structure;

  /**
   *
   * @param ch The config Hamiltonian that will be applied to a structure
   * @param structure The structure to which we wish to apply the Hamiltonian.
   * The parent structure of the given structure must be the primitive structure
   * used to define the Hamiltonian ch.
   *
   */
  public AppliedDecorationFunction(DecorationTemplate ch, SuperStructure structure) {

    super(ch, structure);

    m_ConfigFunction = ch;
    m_ChemicalPotentials = new double[ch.numSigmaSpecies()];
  }
  
  /**
   *
   * @param ch The Hamiltionian that will be applied to the structure
   * @param superToDirect
   * @todo Define superToDirect
   */
  public AppliedDecorationFunction(DecorationTemplate ch, int[][] superToDirect) {
    this(ch, makeSuperStructure(ch, superToDirect));
  }
  
  /**
   * This creates a superStructure and sets any vacancies in the structure to site state 0.
   * 
   * @param function
   * @param superToDirect
   * @return
   */
  private static SuperStructure makeSuperStructure(DecorationTemplate function, int[][] superToDirect) {
    
    SuperStructure returnStructure =new SuperStructure(function.getBaseStructure(), superToDirect);
    SiteDecorator sd = new SiteDecorator(function, returnStructure);
    for (int sigmaIndex = 0; sigmaIndex < sd.numSigmaSites(); sigmaIndex++) {
      if (sd.getSigmaSite(sigmaIndex).getSpecies() == null) {
        sd.setSigmaState(sigmaIndex, 0);
      }
    }
    return returnStructure;
  }

  /**
   *
   * @return The structure to which the Hamiltonian is applied
   */
  public Structure getStructure() {
    return this.getSuperStructure();
  }

  /**
   *
   * @return The structure to which the Hamiltonian is applied.  Same as
   * getStructure(), except it returns an object of type SuperStructure without
   * casting.
   */
  /*public SuperStructure getSuperStructure() {
    return m_Structure;
  }*/

  /**
   *
   * @return The Hamiltonian we are applying to a structure.
   */
  public DecorationTemplate getHamiltonian() {
    return m_ConfigFunction;
  }
  
  /**
   * The same as getHamiltonian().  Just a more general way of writing it.
   * 
   * @return
   */
  public DecorationTemplate getDecorationTemplate() {
    return m_ConfigFunction;
  }
  
  /**
   *
   * @return The current site states of each of the sigma sites.  The returned
   * array is indexed by the sigma index of each sigma site.
   */
//  public int[] getSigmaStates() {
//    int[] returnArray = new int[this.numSigmaSites()];
//    for (int sigmaIndex = 0; sigmaIndex < returnArray.length; sigmaIndex++) {
//      returnArray[sigmaIndex] = this.getSigmaState(sigmaIndex);
//    }
//    return returnArray;
//  }

  /**
   * Sets the occupancies of the sigma sites in the underlying structure from an
   * array of site states for these sites.
   *
   * @param sigmaStates The site states of the sigma sites, indexed by their 
   * sigma indices.
   */
 // public void setSigmaStates(int[] sigmaStates) {
 //   for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
 //     this.setSigmaState(sigmaIndex, sigmaStates[sigmaIndex]);
 //   }
 // }

  /**
   *
   * @param template An array in which to store the site states of the sigma sites
   * @return Same as getSigmaStates(), but populates the given array.
   */
//  public int[] getSigmaStates(int[] template) {
//    
//    if (template == null) {
//      return this.getSigmaStates();
//    }
//    
//    for (int sigmaIndex = 0; sigmaIndex < template.length; sigmaIndex++) {
//      template[sigmaIndex] = this.getSigmaState(sigmaIndex);
//    }
//    return template;
//  }

  /**
   *
   * This method returns the site states for the sigma sites that correspond
   * to a given decoration number, but does not actually modify the underlying
   * structure.
   *
   * @param decorationNumber A decoration that must have been generated
   * by the method getDecorationNumber() for this Hamiltonian applied to this
   * superStructure.  Otherwise, the results are unpredictable.
   * @return The site states of the sigma sites that corresopnd to the given
   * decoration number.
   */
//  public int[] getSigmaStates(BigInteger decorationNumber) {
//    return this.getSigmaDecorator().getSigmaStates(decorationNumber);
//  }

  /**
   * This method is like getSigmaStates(), but it returns nothing and instead
   * populates the sigma sites of the superStructure in a manner that corresponds
   * to the given decoration Number.
   *
   * @param decorationNumber A decoration that must have been generated
   * by the method getDecorationNumber() for this Hamiltonian applied to this
   * superStructure.  Otherwise, the results are unpredictable.
   */
//  public void setSigmaStates(BigInteger decorationNumber) {
//    this.setSigmaStates(this.getSigmaStates(decorationNumber));
//  }

  /**
   *
   * @return The number of different ways in which the superstructure
   * can be decorated given the constraints of the Hamiltonian.  This 
   * is also one more than the maximum decoration number.
   */
//  public BigInteger numAllowedDecorations() {
//    return this.getSigmaDecorator().numAllowedDecorations();
//  }

  /**
   *
   * @return An integer that uniquely corresponds to the current decoration
   * of the superStructure.  The number can range from 0 to
   * this.numAllowedDecorations() -1.
   */
//  public BigInteger getDecorationNumber() {
//    return this.getDecorationNumber(this.getSigmaStates());
//  }

  /**
   *
   * @param sigmaStates An array of site states for the sigma sites in the
   * superStructure.
   * @return A unique integer that corresponds to the given decoration.
   */
  //public BigInteger getDecorationNumber(int[] sigmaStates) {
  //  return this.getSigmaDecorator().getDecorationID(sigmaStates);
 // }

  /**
   *
   * @return The object used to translate between decorationNumbers and
   * arrays of site states for the sigma sites.  For internal use only.
   */
  //protected SiteDecorator getSigmaDecorator() {
  //  return m_SigmaDecorator;
 // }

  /**
   * Decorates the superStructure randomly, within the constraints of the
   * Hamiltonian.
   */
  //public void decorateRandomly() {
  //  int[] siteStates = m_SigmaDecorator.getRandomStates();
  //  this.setSigmaStates(siteStates);
 // }

  /**
   *
   * @param siteState All sigma sites will be populated with the species that
   * corresponds to the given siteState.  Be careful here!  Some sigma sites may
   * allow a larger range of site states than other.  For example, a binary site
   * only allows the states 0 and 1, whereas a ternary site allows 0, 1, and 2.  If
   * you try to populate a structure containing both binary and ternary sites with
   * the site state "2", you will get an error.
   */
//  public void decorateWithSiteState(int siteState) {
//    int[] siteStates = new int[this.numSigmaSites()];
//    Arrays.fill(siteStates, siteState);
//    this.setSigmaStates(siteStates);
//  }
  


  /**
   *
   * @return The total number of sigma sites in the superStructure
   */
 // public int numSigmaSites() {
 //   return m_SigmaDecorator.numSigmaSites();
 // }

  /**
   *
   * @param sigmaIndex An index ranging from 0 to this.numSigmaSites -1 that
   * corresponds to a sigma AbstractSite in the superStructure.
   * @return The species allowed at the sigma site as defined by the Hamiltonian.
   */
 // public Specie[] getAllowedSpecies(int sigmaIndex) {
 //   return m_SigmaDecorator.getAllowedSpecies(sigmaIndex);
 // }

  /**
   *
   * @param sigmaIndex An index ranging from 0 to this.numSigmaSites -1 that
   * corresponds to a sigma AbstractSite in the superStructure.
   * @return The number of species allowed at the sigma site as defined by the
   * Hamiltonian.
   */
//  public int numAllowedSpecies(int sigmaIndex) {
//    return m_SigmaDecorator.numAllowedSpecies(sigmaIndex);
//  }

  /**
   *
   * @param sigmaIndex The sigma index is a unique integer assigned to each
   * sigma site on the superCell.  The index ranges from 0 to (this.numSigmaSites - 1).
   * The sigma index for a given site can be found using this.getSigmaIndex(...)
   *
   * @return The sigma site corresponding to the given sigma Index
   */
//  public SuperStructure.Site getSigmaSite(int sigmaIndex) {
//    return (SuperStructure.Site) m_SigmaDecorator.getSigmaSite(sigmaIndex);
//  }

  /**
   * If the structure has been modified directly (i.e., modified without
   * calling a method on the applied Hamiltonian), you should notify the
   * applied Hamiltonian to update its cached data accordingly.  This can
   * be expensive, so it is best to modify the structure through the methods
   * on the applied Hamiltonian if possible.
   */
  public void refreshFromStructure() {
    try {
      this.setSigmaStates(this.readSigmaStates());
    } catch (IllegalOccupationException e) {
      throw new RuntimeException("Cannot refresh from structure because structure has unallowed occupations.", e);
    }
  }

  /**
   * Initializes all sigma sites to the site state 0 (which is necessarily
   * allowed on all sites).
   */
//  public void setInitialStates() {
//    this.setSigmaStates(BigInteger.ZERO);
//  }

  /**
   * This method is used to iterate over all possible sets of site states.  It does
   * not actually assign any occupancies to the structure.
   *
   * @param sigmaStates An array of site states for the sigma sites in the structure.
   * The given array will be modified by replacing the values with the
   * next set of site states in a logical sequence (that corresponds to the
   * sequence of decoration numbers).  If this method reaches the last set of
   * site states allowed, it will loop back to setting all states equal to zero.
   * @return True if we did not loop, false otherwise.
   */
//  public boolean incrementSigmaStates(int[] sigmaStates) {
//    return m_SigmaDecorator.incrementSigmaStates(sigmaStates);
//  }

  /**
   *
   * @param sigmaIndex An index ranging from 0 to this.numSigmaSites -1 that
   * corresponds to a sigma AbstractSite in the superStructure.
   * @param newState The site state corresponding to the species you want to
   * assign to the sigma site with the given index
   */  
//   public abstract void setSigmaState(int sigmaIndex, int newState);

  /**
   *
   * @param sigmaIndex An index ranging from 0 to this.numSigmaSites -1 that
   * corresponds to a sigma AbstractSite in the superStructure.
   * @return The site state corresponding to the species currently occupying
   * the sigma site with the given index
   */
//  public int getSigmaState(int sigmaIndex) {
//    return m_ConfigFunction.getSiteState(this.getSigmaSite(sigmaIndex));
//  }
  
  public abstract int getQuickSigmaState(int sigmaIndex);
  public abstract int[] getQuickSigmaStates(int[] template);
  
  public BigInteger getQuickDecorationID() {
    return this.getDecorationID(getQuickSigmaStates(null));
  }

  /**
   * This method calculates how the value of the Hamiltonian would change
   * if the site state of the given sigma site were changed.
   *
   * @param sigmaIndex An index ranging from 0 to this.numSigmaSites -1 that
   * corresponds to a sigma AbstractSite in the superStructure.
   * @param newState The site state corresponding to the species you want to
   * hypothetically assign to the sigma site
   * @return The hypothetical change in the value of the Hamiltonian if the
   * change were made
   */
  public abstract double getDelta(int sigmaIndex, int newState);

  /**
   * This method calculates how the value of the Hamiltonian would change
   * if the site states of the given sigma sites were changed.
   *
   * @param sigmaIndices Indices ranging from 0 to this.numSigmaSites -1 that
   * correspond to a sigma AbstractSite in the superStructure.
   * @param newStates The site states corresponding to the species you want to
   * hypothetically assign to the sigma sites
   * @return The hypothetical change in the value of the Hamiltonian if the
   * change were made
   */
  public abstract double getDelta(int[] sigmaIndices, int[] newStates);
  
  /**
   * This method is like getDelta but it actually changes the occupancy of the
   * sigma site
   *
   * @param sigmaIndex An index ranging from 0 to this.numSigmaSites -1 that
   * corresponds to a sigma AbstractSite in the superStructure.
   * @param newState The site state corresponding to the species you want to
   * assign to the sigma site
   * @return The change in the value of the Hamiltonian due to the change.
   */
  public abstract double setSigmaStateGetDelta(int sigmaIndex, int newState);
  
  /**
   * This method is like getDelta but it actually changes the occupancy of the
   * sigma site
   *
   * @param sigmaIndices Indices ranging from 0 to this.numSigmaSites -1 that
   * corresponds to sigma sites in the superStructure.
   * @param newStates The site states corresponding to the species you want to
   * assign to the sigma sites
   * @return The change in the value of the Hamiltonian due to the change.
   */
  public double setSigmaStatesGetDelta(int[] sigmaIndices, int[] newStates) {

    double returnValue = 0;
    for (int siteNum = 0; siteNum < sigmaIndices.length; siteNum++) {
      returnValue += this.setSigmaStateGetDelta(sigmaIndices[siteNum], newStates[siteNum]);
    }
    return returnValue;
  }
  
  public int numSublattices() {
    return m_ConfigFunction.numSublattices();
  }
  
  public int getSublattice(int sigmaIndex) {
    int siteIndex = ((SuperStructure.Site) this.getSigmaSite(sigmaIndex)).getParentSite().getIndex();
    return m_ConfigFunction.getSublatticeForSite(siteIndex);
  }
  
  public int[] getSigmaIndicesForSublattice(int sublatticeIndex) {
    
    int numSites = m_ConfigFunction.numSitesForSublattice(sublatticeIndex) * this.numPrimCells();
    int[] returnArray = new int[numSites];
    int returnIndex =0;
    for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
      int siteSublatticeIndex = this.getSublattice(sigmaIndex);
      if (siteSublatticeIndex == sublatticeIndex) {
        returnArray[returnIndex++] = sigmaIndex;
      }
    }
    
    return returnArray;
  }
  
  public double getChemPot(int sigmaIndex) {
    return this.getChemPot(this.getSpeciesForState(sigmaIndex, this.getQuickSigmaState(sigmaIndex)));
  }
  
  public double getChemPot(int sigmaIndex, int siteState) {
    return this.getChemPot(this.getSpeciesForState(sigmaIndex, siteState));
  }
  
  public double getChemPotDelta(int sigmaIndex, int newState) {
    return this.getChemPot(sigmaIndex, newState) - this.getChemPot(sigmaIndex);
  }
  
  public double[] getChemPots() {
    double[] returnArray = new double[this.numSigmaSpecies()];
    for (int specNum = 0; specNum < returnArray.length; specNum++) {
      returnArray[specNum] = this.getChemPot(this.getSigmaSpecies(specNum));
    }
    return returnArray;
  }
  
  public void setChemPot(Species spec, double chemicalPotential) {
    int index = this.getIndexForSpecies(spec);
    m_ChemicalPotentials[index] = chemicalPotential;
  }

  public double getChemPot(Species spec) {
    int index = this.getIndexForSpecies(spec);
    return m_ChemicalPotentials[index];
  }
  
  public double getChemPotForSpecies(int specNum) {
    return m_ChemicalPotentials[specNum];
  }
  
  
}
