package matsci.structure.decorate;

import java.util.Arrays;

import matsci.Element;
import matsci.Species;
import matsci.io.clusterexpansion.PRIM;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.BravaisLattice;
import matsci.structure.IDisorderedStructureData;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.structure.Structure.Site;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>Title: </p>
 * <p>Description: A Hamiltonian that is a function of the configuration of the atoms in a cell.
 * This type of Hamitonian is characterized by a finite set of allowed species on each site in the cell</p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class DecorationTemplate {

  private final Structure m_PrimStructure;
  private final Species[][] m_AllowedSpecies;
  private final Species[] m_AllSigmaSpecies;
  private final Species[] m_AllSpecies;
  private final int[] m_NumSigmaSitesPerSpecies;
  private final int[] m_NumSitesPerSpecies;
  
  private SpaceGroup m_SpaceGroup;
  private SpaceGroup m_FactoredSpaceGroup;
  protected int[][] m_SitesBySublattice = new int[0][];
  protected int[] m_SublatticeBySite;

  /**
   * Creates a decoration template for a structure with fixed occupations
   * 
   * @param structure The structure with the only allowed species already assigned to each site.
   */
  public DecorationTemplate(Structure structure) {
    this(structure, getSimpleAllowedSpecies(structure));
  }
  
  protected static Species[][] getSimpleAllowedSpecies(Structure structure) {
    Species[][] species = new Species[structure.numDefiningSites()][1];
    for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
      species[siteNum] = new Species[] {structure.getSiteSpecies(siteNum)};
    }
    
    return species;
  }
  
  /**
   * Creates a new DecorationTemplate based on the given structure and allowed species
   * @param structure The primitive structure that this Hamiltonian is defined on
   * @param allowedSpecies Each element corresponds to at set of species allowed at the cooresponding 
   * defining site on the structure.
   */
  public DecorationTemplate(Structure structure, Species[][] allowedSpecies) {
    m_PrimStructure = structure;

    if (allowedSpecies.length != m_PrimStructure.numDefiningSites()) {
      throw new RuntimeException("Number of allowed species sets is different from the number of defining sites in the structure");
    }
    
    // Set the allowed species.
    m_AllowedSpecies = (Species[][]) ArrayUtils.copyArray(allowedSpecies);
    Species[] allSigmaSpecies = new Species[0];
    int[] numSigmaSitesPerSpecies = new int[0];
    Species[] allSpecies = new Species[0];
    int[] numSitesPerSpecies = new int[0];
    for (int siteNum = 0; siteNum < allowedSpecies.length; siteNum++) {
      Species[] allowedSpeciesForSite = allowedSpecies[siteNum];
      for (int specNum = 0; specNum < allowedSpeciesForSite.length; specNum++) {
        Species species = allowedSpeciesForSite[specNum];
        int specIndex = ArrayUtils.findIndex(allSpecies, species);
        if (specIndex < 0) {
          specIndex = allSpecies.length;
          allSpecies = (Species[]) ArrayUtils.appendElement(allSpecies, species);
          numSitesPerSpecies = ArrayUtils.appendElement(numSitesPerSpecies, 0);
        } 
        numSitesPerSpecies[specIndex]++;
        if (allowedSpeciesForSite.length > 1) {
          int sigmaSpecIndex = ArrayUtils.findIndex(allSigmaSpecies, species);
          if (sigmaSpecIndex < 0) {
            sigmaSpecIndex = allSigmaSpecies.length;
            allSigmaSpecies = (Species[]) ArrayUtils.appendElement(allSigmaSpecies, species);
            numSigmaSitesPerSpecies = ArrayUtils.appendElement(numSigmaSitesPerSpecies, 0);
          }       
          numSigmaSitesPerSpecies[sigmaSpecIndex]++;
        }

      }
    }
    m_AllSpecies = allSpecies;
    m_NumSitesPerSpecies = numSitesPerSpecies;
    m_AllSigmaSpecies = allSigmaSpecies;
    m_NumSigmaSitesPerSpecies = numSigmaSitesPerSpecies;
    
  }
  
  public DecorationTemplate(PRIM prim) {
    this(new Structure(prim), prim.getAllowedSpecies());
  }
  
  public SpaceGroup getDefiningSpaceGroup() {
    if (m_FactoredSpaceGroup == null) {      
      m_FactoredSpaceGroup = this.getSpaceGroup().getFactoredSpaceGroup(m_PrimStructure.getSuperSiteLattice());
    }
    return m_FactoredSpaceGroup;
  }
  
  public SpaceGroup getSpaceGroup() {
    if (m_SpaceGroup == null) {
      m_SpaceGroup = this.findSpaceGroup();
    }
    return m_SpaceGroup;
  }
  
  // Can't call this in constructor because subclass might not have finished initializing.  
  protected void findSublattices() {
    
    if (m_SublatticeBySite != null) {
      return;
    }
    
    SpaceGroup spaceGroup = this.getSpaceGroup();
    Coordinates[] sublatticeCoords = new Coordinates[0];
    m_SublatticeBySite = new int[m_PrimStructure.numDefiningSites()];
    for (int siteIndex = 0; siteIndex < m_PrimStructure.numDefiningSites(); siteIndex++) {
      Coordinates siteCoords = m_PrimStructure.getSiteCoords(siteIndex);
      int sublatticeIndex = 0;
      for (sublatticeIndex = 0; sublatticeIndex < sublatticeCoords.length; sublatticeIndex++) {
        Coordinates prevCoords = sublatticeCoords[sublatticeIndex];
        if (spaceGroup.areSymmetricallyEquivalent(siteCoords, prevCoords)) {break;}
      }
      if (sublatticeIndex == sublatticeCoords.length) {
        sublatticeCoords = (Coordinates[]) ArrayUtils.appendElement(sublatticeCoords, siteCoords);
        m_SitesBySublattice = ArrayUtils.appendElement(m_SitesBySublattice, new int[0]);
      }
      m_SitesBySublattice[sublatticeIndex] = ArrayUtils.appendElement( m_SitesBySublattice[sublatticeIndex], siteIndex);
      m_SublatticeBySite[siteIndex] = sublatticeIndex;
    }
  }
  
  public int numSublattices() {
    this.findSublattices();
    return m_SitesBySublattice.length;
  }
  
  public int getSublatticeForSite(int primIndex) {
    this.findSublattices();
    return m_SublatticeBySite[primIndex];
  }
  
  public int numSitesForSublattice(int sublatticeIndex) {
    this.findSublattices();
    return m_SitesBySublattice[sublatticeIndex].length;
  }
  
  public int[] getSitesForSublattice(int sublatticeIndex) {
    this.findSublattices();
    return ArrayUtils.copyArray(m_SitesBySublattice[sublatticeIndex]);
  }
  
  public Structure.Site[][] groupSitesBySublattice() {
	this.findSublattices();
    Structure.Site[][] returnArray = new Structure.Site[m_SitesBySublattice.length][];
    for (int sublatticeIndex = 0; sublatticeIndex < returnArray.length; sublatticeIndex++) {
      returnArray[sublatticeIndex] = new Structure.Site[m_SitesBySublattice[sublatticeIndex].length];
      for (int siteNum = 0; siteNum < m_SitesBySublattice[sublatticeIndex].length; siteNum++) {
        returnArray[sublatticeIndex][siteNum] = m_PrimStructure.getDefiningSite(m_SitesBySublattice[sublatticeIndex][siteNum]);
      }
    }
    return returnArray;
  }
  
  public Element[] getAllAllowedElements() {
    
    Element[] returnArray = new Element[m_AllSpecies.length];
    int returnIndex = 0;
    for (int specNum = 0; specNum < m_AllSpecies.length; specNum++) {
      Element element = m_AllSpecies[specNum].getElement();
      boolean seenBefore = false;
      for (int prevElementNum = 0; prevElementNum < returnIndex; prevElementNum++) {
        if (returnArray[prevElementNum] == element) {
          seenBefore = true;
          break;
        }
      }
      if (seenBefore) {continue;}
      returnArray[returnIndex++] = element;
    }
    
    return (Element[]) ArrayUtils.truncateArray(returnArray, returnIndex);
    
  }
  
  public Species[] getAllowedSpeciesForSublattice(int sublatticeIndex) {
    this.findSublattices();
    return this.getAllowedSpecies(m_SitesBySublattice[sublatticeIndex][0]);
  }
  
  public Species[] getAllAllowedSpecies() {
    //this.findSublattices();
    return (Species[]) ArrayUtils.copyArray(m_AllSpecies);
  }
  
  public int numAllowedSpecies() {
    return m_AllSpecies.length;
  }
  
  public Species getAllowedSpecie(int specNum) {
    return m_AllSpecies[specNum];
  }
  
  public int numSitesForAllowedSpecie(int specIndex) {
    return m_NumSitesPerSpecies[specIndex];
  }
  
  public int numSitesForAllowedSpecie(Species specie) {
    return m_NumSitesPerSpecies[this.getIndexForAllowedSpecie(specie)];
  }
  
  public int getIndexForAllowedSpecie(Species specie) {
    return ArrayUtils.findIndex(m_AllSpecies, specie);
  }
  
  public Species[] getAllSigmaSpecies() {
    return (Species[]) ArrayUtils.copyArray(m_AllSigmaSpecies);
  }
  
  public int numSigmaSpecies() {
    return m_AllSigmaSpecies.length;
  }
  
  public int numNonVacancySigmaSpecies() {
    return ArrayUtils.arrayContains(m_AllSigmaSpecies, Species.getVacancy()) ? m_AllSigmaSpecies.length - 1 : m_AllSigmaSpecies.length;
  }
  
  public Species getSigmaSpecie(int specNum) {
    return m_AllSigmaSpecies[specNum];
  }
  
  public int numSigmaSitesForSpecie(int specIndex) {
    return m_NumSigmaSitesPerSpecies[specIndex];
  }
  
  public int numSigmaSitesForSpecie(Species specie) {
    int index = this.getIndexForSigmaSpecie(specie);
    return index < 0 ? 0 : m_NumSigmaSitesPerSpecies[index];
  }
  
  public int getIndexForSigmaSpecie(Species specie) {
    return ArrayUtils.findIndex(m_AllSigmaSpecies, specie);
  }
  
  public int getIndexForAllowedSpecie(int siteIndex, Species species) {
    return ArrayUtils.findIndex(m_AllowedSpecies[siteIndex], species);
  }
  
  public Species[][] getAllowedSpecies() {
    return (Species[][]) ArrayUtils.copyArray(m_AllowedSpecies);
  }

  /**
   *
   * @param primIndex The index of a site in the prim cell on which this Hamiltonian is defined
   * @return The allowed species for that site (and symmetrically equivalent sites)
   */
  public Species[] getAllowedSpecies(int primIndex) {
    return m_AllowedSpecies[primIndex];
  }

  /**
   *
   * @param coords A set of coordinates corresponding to a site on a lattice
   * based on the primitive cell on which this template is defined
   * @return The allowed species for this site
   */
  public Species[] getAllowedSpecies(Coordinates coords) {
    return m_AllowedSpecies[this.getPrimIndex(coords)];
  }
  
  /**
   * Returns the index of the site associated with the given coordinates
   * 
   * @param coords The given coordinates
   * @return the index of the site associated with the given coordinates
   */
  public int getPrimIndex(Coordinates coords) {
	    Structure.Site primSite = m_PrimStructure.getDefiningSite(coords);
	    if (primSite == null) {
	      throw new RuntimeException("Site with following coordinates is not known: " + coords);
	    }
	    return primSite.getIndex();
  }

  /**
   *
   * @param site A site on a supercell based on the primitive cell on which this
   * Hamiltonian is defined
   * @return The allowed species for the site
   */
  public Species[] getAllowedSpecies(SuperStructure.Site site) {
    Structure.Site parentSite = site.getParentSite();
    if (parentSite.getStructure() != m_PrimStructure) {
      throw new RuntimeException("AbstractSite not known to this Hamiltonian");
    }
    return m_AllowedSpecies[parentSite.getIndex()];
  }
  
  public Species getAllowedSpecies(int primIndex, int siteState) {
    return m_AllowedSpecies[primIndex][siteState];
  }
  
  /**
   * 
   * @param primSiteIndex
   * @return The number of species allowed at the site corresponding to the given index
   */
  public int numAllowedSpecies(int primSiteIndex) {
    return m_AllowedSpecies[primSiteIndex].length;
  }

  /**
   *
   * @param site A site on a supercell based on the primitive cell on which this
   * Hamiltonian is defined
   * @return The integer state representing the current occupancy of the given site
   */
  public int getSiteState(SuperStructure.Site site) {
    Species[] allowedSpecies = this.getAllowedSpecies(site);
    for (int stateNum = 0; stateNum < allowedSpecies.length; stateNum++) {
      if ((site.getSpecies()) == allowedSpecies[stateNum]) {
        return stateNum;
      }
    }
    throw new RuntimeException("Species " + site.getSpecies() + " not allowed at this site.");
  }
  
  public int getSiteState(int primSiteIndex, Species species) {
    Species[] allowedSpecies = this.getAllowedSpecies(primSiteIndex);
    for (int stateNum = 0; stateNum < allowedSpecies.length; stateNum++) {
      if (species == allowedSpecies[stateNum]) {
        return stateNum;
      }
    }
    throw new RuntimeException("Species " + species + " not allowed at this site.");
  }

  /**
   *
   * @param site A site on a supercell based on the primitive cell on which this
   * Hamiltonian is defined
   * @param state The state corresponding to the species you want to populate this site with
   */
  public void setSpeciesForState(SuperStructure.Site site, int state) {
    site.setSpecies(this.getSpeciesForState(site, state));
  }

  /**
   *
   * @param site A site on a supercell based on the primitive cell on which this
   * Hamiltonian is defined
   * @param siteState A state corresponding to some unknown species given this site
   * @return The species the given state corresponds to
   */
  public Species getSpeciesForState(SuperStructure.Site site, int siteState) {
    Species[] allowedSpecies = this.getAllowedSpecies(site);
    try {
      return allowedSpecies[siteState];
    } catch (ArrayIndexOutOfBoundsException e) {
      throw new RuntimeException("State not allowed at this site", e);
    }
  }
 
  /**
   *
   * @param site A site on a supercell based on the primitive cell on which this
   * Hamiltonian is defined
   * @return True if this Hamiltonian allows more than one species to be
   * assigned to the site
   */
  public boolean isSigmaSite(SuperStructure.Site site) {
    return (this.getAllowedSpecies(site).length > 1);
  }
  
  /**
  *
  * @param coords Coordinates for a site on a supercell based on the primitive cell on which this
  * Hamiltonian is defined
  * @return True if this Hamiltonian allows more than one species to be
  * assigned to the site
  */
  public boolean isSigmaSite(Coordinates coords) {
    return (this.getAllowedSpecies(coords).length > 1);
  }

  /**
   *
   * @param primIndex The index for a site in the primitive cell on which this
   * Hamiltonian is defined
   * @return True if more than one species is allowed at that site (and
   * corresponding sites in a supercell based on the primitive cell)
   */
  public boolean isSigmaSite(int primIndex) {
    return (m_AllowedSpecies[primIndex].length > 1);
  }
  
  /**
  *
  * @return An array in which indices that are "true" corresponds to the site indices of sigma sites.
  */
 public boolean[] getSigmaSites(){
   boolean[] returnArray = new boolean[this.getBaseStructure().numDefiningSites()];
   for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
     returnArray[siteNum] = this.isSigmaSite(siteNum);
   }
   return returnArray;
 }

  /**
   *
   * @return The number of sites in the primitive cell on which this Hamiltonian
   * is defined that can be populated by more than one species
   */
  public int numSigmaSites() {
    int numSigmaSites = 0;
    for (int siteNum = 0; siteNum < m_AllowedSpecies.length; siteNum++) {
      if (m_AllowedSpecies[siteNum].length > 1) {
        numSigmaSites++;
      }
    }
    return numSigmaSites;
  }

  /**
   *
   * @return The number of sites in the primitive cell on which this Hamiltonian
   * is defined
   */
  public int numPrimSites() {
    return m_PrimStructure.numDefiningSites();
  }

  /**
   *
   * @return The structure object corresponding to the primitive cell on which
   * this Hamiltonian is defined
   */
  public Structure getBaseStructure() {
    return m_PrimStructure;
  }
  
  public SuperLattice getMinimalLattice() {
    
    if (m_SpaceGroup != null) {
      BravaisLattice minLattice = m_SpaceGroup.getPrimitiveSpaceGroup().getLattice();
      return new SuperLattice(m_PrimStructure.getSiteLattice(), minLattice);
    }
    
    Vector[] potentialVectors =new Vector[0];
    SuperLattice superLattice = m_PrimStructure.getSuperSiteLattice();
    if (superLattice.numPeriodicVectors() == 0) {return superLattice;}
    for (int primCellNum = 0; primCellNum < superLattice.numPrimCells(); primCellNum++) {
      Vector translation = superLattice.getPrimCellVector(primCellNum);
      boolean keeper = true;
      for (int siteNum = 0; siteNum < m_PrimStructure.numDefiningSites(); siteNum++) {
        Structure.Site site =m_PrimStructure.getDefiningSite(siteNum);
        Coordinates translatedCoords = site.getCoords().translateBy(translation);
        //Structure.Site transSite = m_PrimStructure.getNearbyDefiningSite(translatedCoords);
        Structure.Site transSite = m_PrimStructure.getFastDefiningSite(translatedCoords);
        if (!this.testEquivalentSites(site.getIndex(), transSite.getIndex())) {
          keeper = false;
        }
        if (!keeper) {break;}
      }
      if (!keeper) {continue;}
      potentialVectors = (Vector[]) ArrayUtils.appendElement(potentialVectors, translation);
    }
    BravaisLattice subLattice = m_PrimStructure.getDefiningLattice().getSubLattice(potentialVectors, potentialVectors.length);
    //System.out.println("Defining lattice size: " + this.getDefiningLattice().getCellSize() + ", SubLattice size: " + subLattice.getCellSize());
    return new SuperLattice(m_PrimStructure.getSiteLattice(), subLattice);
  }
  
  public DecorationTemplate getCompact() {
    
    BravaisLattice compactLattice = m_PrimStructure.getDefiningLattice().getCompactLattice();

    StructureBuilder structureBuilder = new StructureBuilder();
    structureBuilder.setCellVectors(compactLattice.getCellVectors());
    structureBuilder.setVectorPeriodicity(compactLattice.getDimensionPeriodicity());
    for (int siteNum = 0; siteNum < m_PrimStructure.numDefiningSites(); siteNum++) {
      structureBuilder.addSite(m_PrimStructure.getSiteCoords(siteNum), m_PrimStructure.getSiteSpecies(siteNum));
    }
    structureBuilder.setDescription(m_PrimStructure.getDescription());
    return new DecorationTemplate(new Structure(structureBuilder), m_AllowedSpecies);
  }
  
  public DecorationTemplate getSuperTemplate(int[][] superToDirect) {
    SuperStructure superStructure = new SuperStructure(m_PrimStructure, superToDirect);
    Species[][] superAllowedSpecies = new Species[superStructure.numPrimCells() * m_AllowedSpecies.length][];
    for (int siteNum = 0; siteNum < superStructure.numDefiningSites(); siteNum++) {
      SuperStructure.Site superSite = (SuperStructure.Site) superStructure.getDefiningSite(siteNum);
      superAllowedSpecies[siteNum] = m_AllowedSpecies[superSite.getParentSite().getIndex()];
    }
    return new DecorationTemplate(superStructure, superAllowedSpecies);
  }
  
  public DecorationTemplate findPrimitive() {
    
    if (m_PrimStructure.numPeriodicDimensions() == 0) {
      return this;
    }
    
    BravaisLattice newLattice = this.getMinimalLattice(); //.getCompactLattice();
    
    int numPrimCells = (int) Math.round(m_PrimStructure.getDefiningLattice().getCellSize() / newLattice.getCellSize());
    if (numPrimCells == 1) {return this;}
    if (numPrimCells == 0) {return this;}
    int numSites = m_PrimStructure.numDefiningSites() / numPrimCells;
    Species[][] allowedSpecies = new Species[numSites][];
    
    StructureBuilder structureBuilder = new StructureBuilder();
    structureBuilder.setCellVectors(newLattice.getCellVectors());
    structureBuilder.setVectorPeriodicity(newLattice.getDimensionPeriodicity());
    structureBuilder.setDescription(m_PrimStructure.getDescription());

    for (int siteNum = 0; siteNum < m_PrimStructure.numDefiningSites(); siteNum++) {
      Site site = m_PrimStructure.getDefiningSite(siteNum);
      Coordinates siteCoords =site.getCoords();
      boolean seenBefore = false;
      for (int prevCoordNum = 0; prevCoordNum < structureBuilder.numDefiningSites(); prevCoordNum++) {
        if (newLattice.areTranslationallyEquivalent(structureBuilder.getSiteCoords(prevCoordNum), siteCoords)) {
          seenBefore = true;
          break;
        }
      }
      if (seenBefore) {continue;}
      allowedSpecies[structureBuilder.numDefiningSites()] = m_AllowedSpecies[siteNum];
      structureBuilder.addSite(newLattice.hardRemainder(siteCoords), site.getSpecies());
    }
    
    Structure newStructure = new Structure(structureBuilder);
    return new DecorationTemplate(newStructure, allowedSpecies);
  }
  
  protected SpaceGroup findSpaceGroup() {
    
    SuperLattice minimalLattice = this.getMinimalLattice();
    SpaceGroup siteSpaceGroup = m_PrimStructure.getSiteSpaceGroup();
    
    /*int[] latticePreservingOps = minimalLattice.getLatticePreservingOpNums(siteSpaceGroup);
    SymmetryOperation[] preservingOperations = new SymmetryOperation[latticePreservingOps.length];
    for (int opNum = 0; opNum < latticePreservingOps.length; opNum++) {
      preservingOperations[opNum] = siteSpaceGroup.getOperation(latticePreservingOps[opNum]); 
    }
    SpaceGroup preservingGroup = new SpaceGroup(preservingOperations, m_PrimStructure.getSiteLattice());
    SymmetryOperation[] factorOperations =preservingGroup.getFactorOperations(minimalLattice);*/

    //SpaceGroup minLattPreservingSpaceGroup = siteSpaceGroup.getLatticePreservingSpaceGroup(minimalLattice);
    SpaceGroup minLattPreservingSpaceGroup = minimalLattice.getLatticePreservingSpaceGroup(siteSpaceGroup);
    
    SymmetryOperation[] keptOperations = new SymmetryOperation[0];
    
    /*for (int opNum = 0; opNum < factorOperations.length; opNum++) {
      SymmetryOperation operation = factorOperations[opNum];*/
    for (int opNum = 0; opNum < minLattPreservingSpaceGroup.numOperations(); opNum++) {
      SymmetryOperation operation = minLattPreservingSpaceGroup.getOperation(opNum);
      boolean allowed = true;
      for (int siteNum = 0; siteNum < m_PrimStructure.numDefiningSites(); siteNum++) {
        Coordinates siteCoords = m_PrimStructure.getSiteCoords(siteNum);
        Coordinates oppedCoords = operation.operate(siteCoords);
        //Structure.Site oppedSite = m_PrimStructure.getDefiningSite(oppedCoords); 
        //Structure.Site oppedSite = m_PrimStructure.getNearbyDefiningSite(oppedCoords); // This is more robust to numerical errors that may have occured when generating the space group
        //Structure.Site oppedSite = m_PrimStructure.getDefiningSite(oppedCoords, CartesianBasis.getPrecision() * 2); // This is faster and hopefully robust.
        Structure.Site oppedSite = m_PrimStructure.getFastDefiningSite(oppedCoords); // This is a good balance between speed and robustness.
        //double distance = m_PrimStructure.getDefiningLattice().getNearestImageDistance(oppedCoords, oppedSite.getCoords());
        //System.out.println(distance);
        if (!this.testEquivalentSites(siteNum, oppedSite.getIndex())) {
          allowed = false;
        }
        if (!allowed) {break;}
      }
      if (allowed) {
        keptOperations = (SymmetryOperation[]) ArrayUtils.appendElement(keptOperations, operation);
      }
    }
    
    //return new SpaceGroup(keptOperations, m_PrimStructure.getDefiningLattice());
    return new SpaceGroup(keptOperations, minimalLattice);
  }
  

  public double[][] getOxidationStatesForAllowedSpecies() {
    double[][] returnArray = new double[m_AllowedSpecies.length][];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = new double[m_AllowedSpecies[siteNum].length];
      for (int specNum = 0; specNum < returnArray[siteNum].length; specNum++) {
        returnArray[siteNum][specNum] = m_AllowedSpecies[siteNum][specNum].getOxidationState();
      }
    }
    return returnArray;
    
  }
  
  /**
   * Assumes the coordinates are symmetrically equivalent, but tests for other types of equivalency.
   * 
   * @return True if the sites are equivalent, false if otherwise.
   */
  protected boolean testEquivalentSites(int site1Index, int site2Index) {
    Species[] site1Species = m_AllowedSpecies[site1Index];
    Species[] site2Species = m_AllowedSpecies[site2Index];
    return Arrays.equals(site1Species, site2Species);
  }

  public Vector[] getCellVectors() {
    return m_PrimStructure.getCellVectors();
  }

  public String getDescription() {
    return m_PrimStructure.getDescription();
  }

  public Coordinates getSiteCoords(int siteIndex) {
    return m_PrimStructure.getSiteCoords(siteIndex);
  }

  public Species getSiteSpecies(int siteIndex, int allowedSpecNum) {
    return m_AllowedSpecies[siteIndex][allowedSpecNum];
  }

  public boolean[] getVectorPeriodicity() {
    return m_PrimStructure.getVectorPeriodicity();
  }

  public int numDefiningSites() {
    return m_PrimStructure.numDefiningSites();
  }

}