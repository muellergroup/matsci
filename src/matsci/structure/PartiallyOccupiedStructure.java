/*
 * Created on Dec 30, 2008
 *
 */
package matsci.structure;

import java.text.DecimalFormat;
import java.util.Arrays;

import matsci.Element;
import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.structure.Structure.Site;
import matsci.structure.decorate.DecorationTemplate;
import matsci.structure.decorate.SiteDecorator;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

/**
 * This class is basically a decoration template with a set of target occupancies.
 * 
 * @author Tim Mueller
 *
 */
public class PartiallyOccupiedStructure extends DecorationTemplate implements IDisorderedStructureData {
  
  private double[][] m_Occupancies;
  
  /**
   * Use this to construct a SigmaStructure instance in which each site is 100% 
   * occupied.
   * 
   * @param structure  The structure in which all sites are occupied by the only 
   * specie that is allowed to occupy that site.
   */
  public PartiallyOccupiedStructure(Structure structure) {
    super(structure);
    
    m_Occupancies = new double[structure.numDefiningSites()][];
    
    for (int siteNum = 0; siteNum < m_Occupancies.length; siteNum++) {
      m_Occupancies[siteNum] = new double[] {1};
    }
  }

  /**
   * 
   * @param structure  The underlying structure that defines the sites that may be occupied
   * @param allowedSpecies  The species allowed on each site, where the i<i>th</i> element is the 
   * specie allowed on the i<i>th</i> defining site of the given structure.  Vacancies should be 
   * included explicitly if they are allowed.
   * @param occupancies The target occupancies of the given species, where each entry corresponds
   * to a specie in the allowedSpecies array.  These will be automatically normalized so that the
   * sum of target occupancies for a given site is 1.
   */
  public PartiallyOccupiedStructure(Structure structure, Species[][] allowedSpecies, double[][] occupancies) {
    super(structure, allowedSpecies);
    
    // Normalize the occupancies
    m_Occupancies = new double[occupancies.length][];
    for (int siteNum = 0; siteNum < m_Occupancies.length; siteNum++) {
    	this.setOccupancies(siteNum, occupancies[siteNum]);
      /*double total = 0;
      for (int stateNum = 0; stateNum < m_Occupancies[siteNum].length; stateNum++) {
        total += m_Occupancies[siteNum][stateNum];
      }
      for (int stateNum = 0; stateNum < m_Occupancies[siteNum].length; stateNum++) {
        m_Occupancies[siteNum][stateNum] /= total;
      }*/
    }
  }
  
  public PartiallyOccupiedStructure(IDisorderedStructureData sigmaStructureData) {
    this(new Structure(sigmaStructureData), getAllowedSpecies(sigmaStructureData), getOccupancies(sigmaStructureData));
  }
  
  public void setOccupancies(int siteIndex, double[] occupancies) {
	  m_Occupancies[siteIndex] = MSMath.normalizeToUnitSum(occupancies);
  }
 
  protected static Species[][] getAllowedSpecies(IDisorderedStructureData sigmaStructureData) {
    Species[][] returnArray = new Species[sigmaStructureData.numDefiningSites()][0];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = new Species[sigmaStructureData.numAllowedSpecies(siteNum)];
      for (int specNum = 0; specNum < returnArray[siteNum].length; specNum++) {
        returnArray[siteNum][specNum] = sigmaStructureData.getSiteSpecies(siteNum, specNum);
      }
    }
    double[][] occupancies = getOccupancies(sigmaStructureData);
    for (int siteNum = 0; siteNum < occupancies.length; siteNum++) {
      if (occupancies[siteNum].length > returnArray[siteNum].length) { // It's because a vacancy was added
        returnArray[siteNum] = (Species[]) ArrayUtils.appendElement(returnArray[siteNum], Species.vacancy);
      }
    }
    return returnArray;
  }
  
  protected static double[][] getOccupancies(IDisorderedStructureData sigmaStructureData) {
    
    double tolerance = 0.011; // To ensure that, for example, 0.33 + 0.66 is treated as 1
    
    double[][] returnArray = new double[sigmaStructureData.numDefiningSites()][0];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = new double[sigmaStructureData.numAllowedSpecies(siteNum)];
      double remainder = 1;
      for (int specNum = 0; specNum < returnArray[siteNum].length; specNum++) {
        returnArray[siteNum][specNum] = sigmaStructureData.getSiteOccupancy(siteNum, specNum);
        remainder -= returnArray[siteNum][specNum];
      }
      if (remainder > tolerance) {
        returnArray[siteNum] = ArrayUtils.appendElement(returnArray[siteNum], remainder);
      } else {
        returnArray[siteNum] = MSMath.arrayMultiply(returnArray[siteNum], 1 / (1 - remainder));
      }
    }
    return returnArray;
  }
  
  /**
   * 
   * @return The target occupancies, where the i,j<i>th</i> entry corresponds to the target
   * occupancy for the j<i>th</i> allowed specie on the i<i>th</i> defining site.
   */
  public double[][] getOccupancies() {
    return ArrayUtils.copyArray(m_Occupancies);
  }
  
  public Element[] getDistinctElements() {
	return this.getAllAllowedElements();
  }
  
  public String getSpeciesCompositionString() {
	    
	  return this.getSpeciesCompositionString("");
    
  }

  public String getSpeciesCompositionString(String delimiter) {
	  return this.getSpeciesCompositionString(delimiter, true);
  }
  
  public String getSpeciesCompositionString(String delimiter, boolean normalize) {
  	    
    Species[] species = this.getAllAllowedSpecies();
    String[] symbols = new String[species.length];
    for (int elementNum = 0; elementNum < species.length; elementNum++) {
      symbols[elementNum] = species[elementNum].getSymbol();
    }
    Arrays.sort(symbols); // Alphabetize
    
    double precision = 1E-4;
    String formatString = "#.################";
    formatString = formatString.substring(0, 2-((int) Math.round(Math.log10(precision))));
    DecimalFormat formatter = new DecimalFormat(formatString);

    double[] counts = new double[symbols.length];
    int[] intCounts = new int[counts.length];
    boolean allInts = true;
    for (int symbolNum = 0; symbolNum < symbols.length; symbolNum++) {
      String symbol = symbols[symbolNum];
      counts[symbolNum] = this.getTotalOccupancy(Species.get(symbol));
      intCounts[symbolNum] = (int) Math.round(counts[symbolNum]);
      allInts &= (counts[symbolNum] == intCounts[symbolNum]); // No tolerance 
    }
    
    int normalizationFactor = (allInts && normalize) ? MSMath.GCF(intCounts) : 1;

    String returnString = "";
    for (int symbolNum = 0; symbolNum < symbols.length; symbolNum++) {
      String symbol = symbols[symbolNum];
      double count = counts[symbolNum] / normalizationFactor;
      if (count < 1E-6) {continue;}
      
      if (symbolNum > 0) {
    	  returnString = returnString + delimiter;
      }
      returnString = returnString + "(" + symbol + ")" + formatter.format(count);
    }
    
    return returnString;
    
  }
  

  public String getCompositionString() {
    
	  return this.getCompositionString("");
    
  }

  public String getCompositionString(String delimiter) {
	  return this.getCompositionString(delimiter, true);
  }
  
  public String getCompositionString(String delimiter, boolean normalize) {
  	    
    Element[] elements = this.getDistinctElements();
    String[] symbols = new String[elements.length];
    for (int elementNum = 0; elementNum < elements.length; elementNum++) {
      symbols[elementNum] = elements[elementNum].getSymbol();
    }
    Arrays.sort(symbols); // Alphabetize
    
    double precision = 1E-4;
    String formatString = "#.################";
    formatString = formatString.substring(0, 2-((int) Math.round(Math.log10(precision))));
    DecimalFormat formatter = new DecimalFormat(formatString);

    double[] counts = new double[symbols.length];
    int[] intCounts = new int[counts.length];
    boolean allInts = true;
    for (int elementNum = 0; elementNum < symbols.length; elementNum++) {
      String symbol = symbols[elementNum];
      counts[elementNum] = this.getTotalOccupancy(Element.getElement(symbol));
      intCounts[elementNum] = (int) Math.round(counts[elementNum]);
      allInts &= (counts[elementNum] == intCounts[elementNum]); // No tolerance 
    }
    
    int normalizationFactor = (allInts && normalize) ? MSMath.GCF(intCounts) : 1;

    String returnString = "";
    for (int elementNum = 0; elementNum < symbols.length; elementNum++) {
      String symbol = symbols[elementNum];
      double count = counts[elementNum] / normalizationFactor;
      
      if (elementNum > 0) {
    	  returnString = returnString + delimiter;
      }
      returnString = returnString + symbol + formatter.format(count);
    }
    
    return returnString;
    
  }
  
  public double getAtomicWeightPerUnitCell() {
	Element[] elements = this.getAllAllowedElements();
    double returnValue = 0;
    for (Element element : elements) {
      returnValue += this.getTotalOccupancy(element);
    }
    return returnValue;
  }
  
  public PartiallyOccupiedStructure removeElement(Element element) {
    
	if (element == Element.vacancy) {
		throw new RuntimeException("Can't remove vacancy from disordered structure.");
	}
    DisorderedStructureBuilder builder = new DisorderedStructureBuilder(this);
    for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
    	Species[] allowedSpecies = builder.getAllowedSpecies(siteNum);
    	double[] occupancies = builder.getOccupancies(siteNum);
    	for (int specNum = allowedSpecies.length - 1; specNum >= 0; specNum--) {
    		if (allowedSpecies[specNum].getElement() != element) {continue;}
    		allowedSpecies = (Species[]) ArrayUtils.removeElement(allowedSpecies, specNum);
    		occupancies = ArrayUtils.removeElement(occupancies, specNum);
    	}
    	builder.setAllowedSpecies(siteNum, allowedSpecies, occupancies);
    }
    return new PartiallyOccupiedStructure(builder);
  }
  
  /**
   * 
   * @param primIndex The index of the defining site for which we want target occupancies.
   * @return The target occupancies, where the j<i>th</i> entry corresponds to the target
   * occupancy for the j<i>th</i> allowed specie on the defining site.
   */
  public double[] getSiteOccupancies(int primIndex) {
    return ArrayUtils.copyArray(m_Occupancies[primIndex]);
  }
  
  /**
   * 
   * @param primIndex The index of the defining site for which we want a target occupancy.
   * @param specNum The index of the allowed specie on the given site for which we a target
   * occupancy.
   * @return The target occupancy for the given specie on the given site.
   */
  public double getSiteOccupancy(int primIndex, int specNum) {
    return m_Occupancies[primIndex][specNum];
  }
  
  protected boolean testEquivalentSites(int site1Index, int site2Index) {
    boolean returnValue = super.testEquivalentSites(site1Index, site2Index);
    if (!returnValue) {return false;}
    
    double[] site1Occupancies = m_Occupancies[site1Index];
    double[] site2Occupancies = m_Occupancies[site2Index];
    return Arrays.equals(site1Occupancies, site2Occupancies);
  }
  
  public DecorationTemplate getCompact() {
    
    DecorationTemplate superTemplate = super.getCompact();
    Structure thisStructure = this.getBaseStructure();
    Structure structure = superTemplate.getBaseStructure();
    double[][] occupancies = new double[superTemplate.getBaseStructure().numDefiningSites()][];
    for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
      int thisIndex =thisStructure.getDefiningSite(structure.getSiteCoords(siteNum)).getIndex();
      occupancies[siteNum] = this.getSiteOccupancies(thisIndex);
    }
    return new PartiallyOccupiedStructure(superTemplate.getBaseStructure(), superTemplate.getAllowedSpecies(), occupancies);
  }
  
  public DecorationTemplate findPrimitive() {
    
    DecorationTemplate superTemplate = super.findPrimitive();
    if (superTemplate == this) {
      return this;
    }
    Structure thisStructure = this.getBaseStructure();
    Structure templateStructure = superTemplate.getBaseStructure();
    double[][] occupancies = new double[superTemplate.getBaseStructure().numDefiningSites()][];
    for (int siteNum = 0; siteNum < templateStructure.numDefiningSites(); siteNum++) {
      int thisIndex =thisStructure.getDefiningSite(templateStructure.getSiteCoords(siteNum)).getIndex();
      occupancies[siteNum] = this.getSiteOccupancies(thisIndex);
    }
    return new PartiallyOccupiedStructure(superTemplate.getBaseStructure(), superTemplate.getAllowedSpecies(), occupancies);
  }
  
  /**
   * 
   * @param superToDirect An array that provides the coefficients for expressing the 
   * cell vectors of the supercell as a linear combination of the defining cell vectors.
   * The rows correspond to supercell vectors, and the columns correspond to defining cell
   * vectors.
   * @return A sigma structure based on a supercell of the defining cell for this structure.
   */
  public PartiallyOccupiedStructure getSuperSigmaStructure(int[][] superToDirect) {
    return (PartiallyOccupiedStructure) this.getSuperTemplate(superToDirect);
  }
  
  public DecorationTemplate getSuperTemplate(int[][] superToDirect) {
    SuperStructure superStructure = new SuperStructure(getBaseStructure(), superToDirect);
    Species[][] superAllowedSpecies = new Species[superStructure.numPrimCells() * numPrimSites()][];
    double[][] superOccupancies = new double[superAllowedSpecies.length][];
    for (int siteNum = 0; siteNum < superStructure.numDefiningSites(); siteNum++) {
      SuperStructure.Site superSite = (SuperStructure.Site) superStructure.getDefiningSite(siteNum);
      int primIndex = superSite.getParentSite().getIndex();
      superAllowedSpecies[siteNum] = getAllowedSpecies(primIndex);
      superOccupancies[siteNum] = m_Occupancies[primIndex];
    }
    return new PartiallyOccupiedStructure(superStructure, superAllowedSpecies, superOccupancies);
  }
  
  public PartiallyOccupiedStructure setOxidationStates(double[][] oxidationStates) {
    
    Species[][] allowedSpecies = this.getAllowedSpecies();
    for (int siteNum = 0; siteNum < allowedSpecies.length; siteNum++) {
      for (int specNum = 0; specNum < allowedSpecies[siteNum].length; specNum++) {
        Species oldSpecies = allowedSpecies[siteNum][specNum];
        double oxidationState = oxidationStates[siteNum][specNum];
        Species newSpecies = Species.get(oldSpecies.getElementSymbol(), oldSpecies.getIsotope(), oxidationState);
        allowedSpecies[siteNum][specNum] = newSpecies;
      }
    }
    
    return new PartiallyOccupiedStructure(this.getBaseStructure(), allowedSpecies, m_Occupancies);
    
  }
  
  /**
   * 
   * @return  The total target occupancy, per unit cell, listed in the same order as the 
   * sigma species are listed by getAllSigmaSpecies().
   */
  public double[] getTotalOccupancyBySigmaSpecie() {
    double[] returnArray = new double[this.numSigmaSpecies()];
    for (int siteNum = 0; siteNum < m_Occupancies.length; siteNum++) {
      for (int specNum = 0; specNum < m_Occupancies[siteNum].length; specNum++) {
        Species spec = this.getAllowedSpecies(siteNum, specNum);
        int sigmaSpecIndex = this.getIndexForSigmaSpecie(spec);
        if (sigmaSpecIndex < 0) {continue;}
        returnArray[sigmaSpecIndex] += m_Occupancies[siteNum][specNum];
      }
    }
    return returnArray;
  }
  

  /**
   * 
   * @param element An element for which we want the total target occupancy
   * @return The total target occupancy, per unit cell, for the given element.
   */
  public double getTotalOccupancy(Element element) {
    double returnValue = 0;
    for (int siteNum = 0; siteNum < m_Occupancies.length; siteNum++) {
      for (int specNum = 0; specNum < m_Occupancies[siteNum].length; specNum++) {
        Species allowedSpec = this.getAllowedSpecies(siteNum, specNum);
        if (allowedSpec.getElement() == element) {
          returnValue += m_Occupancies[siteNum][specNum];
        }
      }
    }
    return returnValue;
  }
  
  /**
   * 
   * @param spec A specie for which we want the total target occupancy
   * @return The total target occupancy, per unit cell, for the given specie.
   */
  public double getTotalOccupancy(Species spec) {
    double returnValue = 0;
    for (int siteNum = 0; siteNum < m_Occupancies.length; siteNum++) {
      for (int specNum = 0; specNum < m_Occupancies[siteNum].length; specNum++) {
        Species allowedSpec = this.getAllowedSpecies(siteNum, specNum);
        if (allowedSpec == spec) {
          returnValue += m_Occupancies[siteNum][specNum];
        }
      }
    }
    return returnValue;
  }
  
  /**
   * 
   * @param spec A specie for which we want the target occupancy on sigma sites (sites
   * which allow more than one specie.)
   * @return The target occupancy on sigma sites, per unit cell, for the given specie.
   */
  public double getSigmaOccupancy(Species spec) {
    double returnValue = 0;
    for (int siteNum = 0; siteNum < m_Occupancies.length; siteNum++) {
      if (m_Occupancies[siteNum].length <= 1) {continue;}
      for (int specNum = 0; specNum < m_Occupancies[siteNum].length; specNum++) {
        Species allowedSpec = this.getAllowedSpecies(siteNum, specNum);
        if (allowedSpec == spec) {
          returnValue += m_Occupancies[siteNum][specNum];
        }
      }
    }
    return returnValue;
  }
  
  /**
   * 
   * @param spec A specie for which we want the target occupancy on non-sigma sites (sites
   * which allow no more than one specie.)
   * @return The target occupancy on non-sigma sites, per unit cell, for the given specie.
   */
  public double getNonSigmaOccupancy(Species spec) {
    double returnValue = 0;
    for (int siteNum = 0; siteNum < m_Occupancies.length; siteNum++) {
      if (m_Occupancies[siteNum].length > 1) {continue;}
      for (int specNum = 0; specNum < m_Occupancies[siteNum].length; specNum++) {
        Species allowedSpec = this.getAllowedSpecies(siteNum, specNum);
        if (allowedSpec == spec) {
          returnValue += m_Occupancies[siteNum][specNum];
        }
      }
    }
    return returnValue;
  }
  
  /**
   * 
   * @return An array of the approximate target occupancies, rounded to integer values.
   * The total returned occupancy will be the number of sigma sites per unit cell.  The
   * i<i>th</i> element in the array corresponds to the i<i>th</i> sigma specie.
   */
  public int[] getApproximateSigmaIntOccupancies() {
    Species[] sigmaSpecies = this.getAllSigmaSpecies();
    int[] returnArray = new int[sigmaSpecies.length];
    
    boolean[] alreadyMapped = new boolean[sigmaSpecies.length];
    int remainingIntOccupancy = this.numSigmaSites();
    double remainingTargetOccupancy = this.numSigmaSites();
    for (int specNum = 0; specNum < sigmaSpecies.length; specNum++) {
      int minSpecNum = -1;
      double minDelta = Double.POSITIVE_INFINITY;
      for (int candidateSpecNum = 0; candidateSpecNum < alreadyMapped.length; candidateSpecNum++) {
        if (alreadyMapped[candidateSpecNum]) {continue;}
        Species spec = sigmaSpecies[candidateSpecNum];
        double occupancy = (this.getSigmaOccupancy(spec) / remainingTargetOccupancy) * remainingIntOccupancy;
        double delta = Math.abs(Math.round(occupancy) - occupancy);
        if (delta < minDelta) {
          minDelta = delta;
          minSpecNum = candidateSpecNum;
          returnArray[minSpecNum] = (int) Math.round(occupancy);
        }
      }
      alreadyMapped[minSpecNum] = true;
      remainingTargetOccupancy -= this.getSigmaOccupancy(sigmaSpecies[minSpecNum]);
      remainingIntOccupancy -= returnArray[minSpecNum];
      if ((remainingTargetOccupancy <= 0) || (remainingIntOccupancy == 0)) {
        break;
      }
    }
    
    return returnArray;
  }
  
  /**
   * 
   * @return A structure representing this sigma structure, but with the sites and species
   * re-arranged so that the species with highest occupancy correspond to the lowest states,
   * and the sites with the smallest occupancy gaps are the earliest defining sites.
   */
  public PartiallyOccupiedStructure sortSitesAndSpecies() {
    
    int numSites = numPrimSites();
    double[] firstDeltas = new double[numSites];
    int[][] specMaps = new int[numSites][];
    for (int siteNum = 0; siteNum < m_Occupancies.length; siteNum++) {
      double[] occupancies = m_Occupancies[siteNum];
      int[] map = ArrayUtils.getSortPermutation(occupancies);
      map = ArrayUtils.reverseArray(map); // We want the highest occupancies first
      specMaps[siteNum] = map;
      if (map.length > 1) {
        firstDeltas[siteNum] = occupancies[map[0]] - occupancies[map[1]];
      } else {
        firstDeltas[siteNum] = 1;
      }
    }
    int[] siteMap = ArrayUtils.getSortPermutation(firstDeltas);
    
    Structure structure = this.getBaseStructure();
    StructureBuilder builder = new StructureBuilder(structure);
    builder.clearSites();
    double[][] occupancies = new double[numSites][];
    Species[][] allowedSpecies = new Species[numSites][];
    for (int siteNum = 0; siteNum < siteMap.length; siteNum++) {
      int mappedSiteNum = siteMap[siteNum];
      int[] specMap = specMaps[mappedSiteNum];
      double[] oldOccupancies = m_Occupancies[mappedSiteNum];
      Species[] oldAllowedSpecies = this.getAllowedSpecies(mappedSiteNum);
      double[] sortedOccupancies = new double[oldOccupancies.length];
      Species[] sortedAllowedSpecies = new Species[oldAllowedSpecies.length];
      for (int specNum = 0; specNum < specMap.length; specNum++) {
        sortedOccupancies[specNum] = oldOccupancies[specMap[specNum]];
        sortedAllowedSpecies[specNum] = oldAllowedSpecies[specMap[specNum]];
      }
      occupancies[siteNum] = sortedOccupancies;
      allowedSpecies[siteNum] = sortedAllowedSpecies;
      
      Coordinates coords = structure.getSiteCoords(mappedSiteNum);
      Species spec = structure.getSiteSpecies(mappedSiteNum);
      builder.addSite(coords, spec);
    }
    
    Structure newStructure = new Structure(builder);
    return new PartiallyOccupiedStructure(newStructure, allowedSpecies, occupancies);
    
  }

}