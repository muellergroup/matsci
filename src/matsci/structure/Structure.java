package matsci.structure;

import java.util.*;

import matsci.Atom;
import matsci.Element;
import matsci.Species;
import matsci.io.app.log.Status;
import matsci.io.clusterexpansion.PRIM;
import matsci.io.vasp.POSCAR;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.DiscreteBasis;
import matsci.location.basis.LinearBasis;
import matsci.location.symmetry.operations.*;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.superstructure.SuperStructure;
import matsci.structure.symmetry.StructureSymmetryFinder;
import matsci.util.arrays.*;
import matsci.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class Structure implements IStructureData {

  // This is protected so subclasses can access it directly
  protected DefiningSite[] m_DefiningSites;
  
  private final BravaisLattice m_DefiningLattice;
  private SuperLattice m_SuperSiteLattice;
  private final DiscreteBasis m_IntegerBasis; // TODO:  Eliminate me
  
  // TODO:  Make it impossible to change species, cache spacegroup and site sets

  protected BravaisLattice m_SiteLattice;
  protected SpaceGroup m_SiteSpaceGroup;
  protected String m_Description = "Unknown Structure";

  protected Structure(Structure structure) {
    m_DefiningSites = new Structure.DefiningSite[structure.m_DefiningSites.length];
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      m_DefiningSites[siteNum] = (DefiningSite) structure.m_DefiningSites[siteNum].copy();
    }
    m_DefiningLattice = structure.m_DefiningLattice;
    m_IntegerBasis = structure.m_IntegerBasis;
    m_SiteSpaceGroup = structure.m_SiteSpaceGroup;
    m_Description = structure.m_Description;
  }
  
  public Structure(BravaisLattice lattice) {
    m_DefiningLattice = lattice;
    m_DefiningSites = new DefiningSite[0];
    m_IntegerBasis = new DiscreteBasis(this);
  }
  
  public Structure(Vector[] cellVectors, boolean[] areVectorsPeriodic) {
    this (new BravaisLattice(cellVectors, areVectorsPeriodic));
  }
  
  public Structure(IStructureData structureData) {
    this(structureData.getCellVectors(), structureData.getVectorPeriodicity());
    this.addSites(structureData);
    m_Description = structureData.getDescription();
  }
  
  public Structure(IDisorderedStructureData structureData) {
    this(new StructureBuilder(structureData));
  }
  
  public Structure copy() {
    return new Structure(this);
  }
  
  public Structure normalizeNonPeriodicVectors() {
	  
	  if (this.numPeriodicDimensions() == 3) {return new Structure(this);}
	  StructureBuilder builder = new StructureBuilder(this);
	  BravaisLattice normalLattice = this.getDefiningLattice().getOrthogonalizedLattice();
	  builder.setCellVectors(normalLattice.getCellVectors());
	  builder.setVectorPeriodicity(normalLattice.getDimensionPeriodicity());
	  return new Structure(builder);
  }
  
  protected void addSites(IStructureData cellReader) {
    m_DefiningSites = new DefiningSite[cellReader.numDefiningSites()]; // This is the maximum number of unique sites
    for (int currIndex = 0; currIndex < m_DefiningSites.length; currIndex++) {
      //Coordinates coords = m_DefiningLattice.softRemainder(cellReader.getSiteCoords(currIndex));
      //Coordinates coords = m_DefiningLattice.hardRemainder(cellReader.getSiteCoords(currIndex));
      Coordinates coords = cellReader.getSiteCoords(currIndex); // TODO consider making this consistent with superStructure (use hard remainder)
      m_DefiningSites[currIndex] = new DefiningSite(coords, currIndex);
      m_DefiningSites[currIndex].setSpecies(cellReader.getSiteSpecies(currIndex));
    }
  }
  
  public Site[] getSymmetricallyEquivalentDefiningSites(int siteIndex) {
    
    Site[] returnArray = new Site[this.numDefiningSites()];
    Structure.Site site = this.getDefiningSite(siteIndex);
    SpaceGroup spaceGroup = this.getDefiningSpaceGroup();
    int returnIndex = 0;
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      Structure.Site testSite = this.getDefiningSite(siteNum);
      if (spaceGroup.areSymmetricallyEquivalent(site.getCoords(), testSite.getCoords())) {
        returnArray[returnIndex++] = testSite;
      }
    }
    return (Site[]) ArrayUtils.truncateArray(returnArray, returnIndex);
  }
  
  public Site[] getDefiningSites() {
    return (Site[]) ArrayUtils.copyArray(m_DefiningSites);
  }
  
  public Site getDefiningSite(int index) {
    return m_DefiningSites[index];
  }
  
  public int numElectronsPerNeutralDefiningCell() {
    
    int returnValue = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Structure.Site site = m_DefiningSites[siteNum];
      if (site == null) {continue;}
      Species species = site.getSpecies();
      if (species == null || species == Species.vacancy) {continue;}
      returnValue += species.getElement().getAtomicNumber();
    }
    
    return returnValue;
    
  }
  
  public Structure centerNonPeriodicDimensions() {
	  
	  Vector totalNonPeriodicVector = Vector.getZero3DVector();
	  Coordinates cellCenter = new Coordinates(new double[] {0.5, 0.5, 0.5}, this.getDirectBasis());
	  for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
		  Vector siteVector = new Vector(this.getSiteCoords(siteNum), cellCenter);
		  totalNonPeriodicVector = totalNonPeriodicVector.add(siteVector);
	  }
	 
	  Vector shiftVector = this.getDefiningLattice().removeLattice(totalNonPeriodicVector);
	  shiftVector = shiftVector.divide(this.numDefiningSites());
	  
	  StructureBuilder builder = new StructureBuilder(this);
	  for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
		  Coordinates siteCoords = builder.getSiteCoords(siteNum);
		  siteCoords = siteCoords.translateBy(shiftVector);
		  builder.setSiteCoordinates(siteNum, siteCoords);
	  }
	  
	  return new Structure(builder);
	  
  }
  
  public Coordinates getDefiningCenterOfMass() {
    
    double[] comArray = new double[m_DefiningLattice.numTotalVectors()];
    double totalMass = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      
      Structure.Site site = m_DefiningSites[siteNum];
      if (site == null) {continue;}
      double siteMass = site.getSpecies().getElement().getAtomicWeight();
      totalMass += siteMass;
      for (int dimNum = 0; dimNum < comArray.length; dimNum++) {
        comArray[dimNum] += site.getCoords().cartCoord(dimNum) * siteMass;
      }
      
    }
    
    comArray = MSMath.arrayDivide(comArray, totalMass);
    return new Coordinates(comArray, CartesianBasis.getInstance());
    
  }
  
  public Coordinates getDefiningAverageCoordinates() {
    
    double[] comArray = new double[m_DefiningLattice.numTotalVectors()];
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      
      Structure.Site site = m_DefiningSites[siteNum];
      if (site == null) {continue;}
      for (int dimNum = 0; dimNum < comArray.length; dimNum++) {
        comArray[dimNum] += site.getCoords().cartCoord(dimNum);
      }
      
    }
    
    comArray = MSMath.arrayDivide(comArray, m_DefiningSites.length);
    return new Coordinates(comArray, CartesianBasis.getInstance());
    
  }
  
  public Site getDefiningSite(Coordinates coords) {
    
    return this.getDefiningSite(coords, CartesianBasis.getPrecision());
  }
  
  public Site getDefiningSite(Coordinates coords, double precision) {
    
    if (coords.getBasis() == this.getIntegerBasis()) {
      int coordNum = (int) coords.coord(coords.numCoords() - 1);
      return coordNum >= m_DefiningSites.length ? null : m_DefiningSites[coordNum];
    }
    
    //Coordinates definingCoords = m_DefiningLattice.softRemainder(coords);
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite returnSite = m_DefiningSites[siteNum];
      //if (returnSite.getCoords().isCloseEnoughTo(definingCoords)) {
      if (m_DefiningLattice.areTranslationallyEquivalent(coords, returnSite.getCoords(), precision)) {
        return returnSite;
      }
    }
    return null;
  }
  
  
  public int[][] getSuperToDirect(IStructureData superStructure) {
    return this.getSuperToDirect(superStructure, CartesianBasis.getPrecision());
  }
  
  public int[][] getSuperToDirect(IStructureData superStructure, double precision) {
    Coordinates origin = m_DefiningLattice.getOrigin();
    BravaisLattice superLattice = new BravaisLattice(origin, superStructure.getCellVectors(), superStructure.getVectorPeriodicity());
    return m_DefiningLattice.getSuperToDirect(superLattice, precision);
  }
  
  public SuperStructure getSuperStructure(IStructureData structureData) {
    
    int[][] superToDirect = this.getSuperToDirect(structureData);
    //int[][] superToDirect = this.getPrimStructure().getSuperToDirect(structureData);
    if (superToDirect == null) {return null;}
    
    SuperStructure superStructure = new SuperStructure(this, superToDirect);
    for (int siteNum = 0; siteNum < superStructure.numDefiningSites(); siteNum++) {
      Structure.Site site = superStructure.getDefiningSite(siteNum);
      site.setSpecies(Species.vacancy);
    }
    for (int siteNum = 0; siteNum < structureData.numDefiningSites(); siteNum++) {
      Structure.Site site = superStructure.getSite(structureData.getSiteCoords(siteNum));
      if (site == null) {
        throw new RuntimeException("Failed to find matching site at coordinates " + structureData.getSiteCoords(siteNum) + " when building superstructure.");
      }
      site.setSpecies(structureData.getSiteSpecies(siteNum));
    }
    superStructure.setDescription(structureData.getDescription());
    return superStructure;
  }
  
  public Site getSite(Coordinates coords) {
    return this.getSite(coords, CartesianBasis.getPrecision());
  }
  
  public Site getSite(Coordinates coords, double precision) {
    Site definingSite = this.getDefiningSite(coords, precision);
    if (definingSite == null) {return null;}
    Coordinates thisCoords = m_DefiningLattice.getCompactLattice().getQuickCoordsInParallelCell(coords, definingSite.getCoords());
    //Coordinates thisCoords = m_DefiningLattice.getNearestImage(coords, definingSite.getCoords());
    return new SiteImage(thisCoords, definingSite);
  }

  public Site[] getSites(Coordinates[] coords) {
    Site[] returnArray = new Site[coords.length];
    for (int index = 0; index < coords.length; index++) {
      returnArray[index] = this.getSite(coords[index]);
    }
    return returnArray;
  }

  public int numPeriodicDimensions() {
    return m_DefiningLattice.numPeriodicVectors();
  }

  public int numDefiningSites() {
    return m_DefiningSites.length;
  }
  
  public int numDefiningSitesWithSpecies(Species specie) {
    int returnValue = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      //if (m_DefiningSites[siteNum].getSpecie() == Specie.vacancy) {continue;}
      if (m_DefiningSites[siteNum].getSpecies() == specie) {returnValue++;}
    }
    return returnValue;
  }
  
  public Species[] getDistinctSpecies() {
    
    Species[] seenSpecies = new Species[m_DefiningSites.length];
    int numDistinctSpecies = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Species spec = m_DefiningSites[siteNum].getSpecies();
      boolean seenBefore = false;
      for (int seenSpecNum = 0; seenSpecNum < numDistinctSpecies; seenSpecNum++) {
        Species seenSpec = seenSpecies[seenSpecNum];
        if (seenSpec == spec) {
          seenBefore = true;
          break;
        }
      }
      if (!seenBefore) {
        seenSpecies[numDistinctSpecies] = spec;
        numDistinctSpecies++;
      }
    }
    
    return (Species[]) ArrayUtils.truncateArray(seenSpecies, numDistinctSpecies);
    
  }
  

  public Element[] getDistinctElements() {
    
    Element[] seenElements = new Element[m_DefiningSites.length];
    int numDistinctElements = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Element element = m_DefiningSites[siteNum].getSpecies().getElement();
      boolean seenBefore = false;
      for (int seenSpecNum = 0; seenSpecNum < numDistinctElements; seenSpecNum++) {
        Element seenElement = seenElements[seenSpecNum];
        if (seenElement == element) {
          seenBefore = true;
          break;
        }
      }
      if (!seenBefore) {
        seenElements[numDistinctElements] = element;
        numDistinctElements++;
      }
    }
    
    return (Element[]) ArrayUtils.truncateArray(seenElements, numDistinctElements);
    
  }
  
  public Structure padNonPeriodicDimensions(double pad) {
	  
	  Structure structure = this.useSitesInCellAsDefining(false);
	  LinearBasis basis = new LinearBasis(LinearBasis.fill3DBasis(m_DefiningLattice.getPeriodicVectors()));
	  double[] minValues = new double[basis.numDimensions()];
	  Arrays.fill(minValues, Double.POSITIVE_INFINITY);
	  double[] maxValues = new double[basis.numDimensions()];
	  Arrays.fill(maxValues, Double.NEGATIVE_INFINITY);
	  for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
		  double[] coordArray = structure.getSiteCoords(siteNum).getCoordArray(basis);
		  for (int dimNum = 0; dimNum < minValues.length; dimNum++) {
			  minValues[dimNum] = Math.min(minValues[dimNum], coordArray[dimNum]);
			  maxValues[dimNum] = Math.max(maxValues[dimNum], coordArray[dimNum]);
		  }
	  }
	  
	  Vector[] cellVectors = this.getCellVectors();
	  for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
		  if (m_DefiningLattice.isDimensionPeriodic(vecNum)) {continue;}
		  Vector vector = cellVectors[vecNum];
		  cellVectors[vecNum] = vector.resize(maxValues[vecNum] - minValues[vecNum] + pad);
	  }
	  
	  StructureBuilder builder = new StructureBuilder(this);
	  builder.setCellVectors(cellVectors);
	  return new Structure(builder);
  }
  
  public double[] getPairDistributionFunction(Species species, double maxDistance, double increment, double sigma) {
    
    double[] returnArray = new double[(int) Math.ceil(maxDistance / increment)];
    double twoSigSq = sigma * sigma;
    double prefactor = 1 / Math.sqrt(twoSigSq * Math.PI);
    int numContributingSites = 0;
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      
      Structure.Site site = this.getDefiningSite(siteNum);
      if (site.getSpecies() != species) {continue;}
      numContributingSites++;
      
      Structure.Site[] neighbors = this.getNearbySites(site.getCoords(), maxDistance, false);
      for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
        Structure.Site neighbor = neighbors[neighborNum];
        if (neighbor.getSpecies() != species) {continue;}
        double distance = site.distanceFrom(neighbor);
        for (int binNum = 0; binNum < returnArray.length; binNum++) {
          double binValue = increment * (binNum + 0.5);
          double delta = (distance - binValue);
          double weight = prefactor * Math.exp(-delta * delta / twoSigSq);
          returnArray[binNum] += weight;
        }
      }
      
    }
    
    for (int binNum = 0; binNum < returnArray.length; binNum++) {
      double binValue = increment * (binNum + 0.5);
      returnArray[binNum] /= 4 * Math.PI * binValue * binValue * numContributingSites;
    }
    
    return returnArray;
    
  }
  
  public int numDefiningSitesWithElement(Element element) {
    int returnValue = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      if (m_DefiningSites[siteNum].getSpecies().getElement() == element) {returnValue++;}
    }
    return returnValue;
  }
  
  public double getAtomicWeightPerUnitCell() {
    double returnValue = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      returnValue += m_DefiningSites[siteNum].getSpecies().getElement().getAtomicWeight();
    }
    return returnValue;
  }
  
  public double getPricePerMolUnitCell() {
    double returnValue = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      returnValue += m_DefiningSites[siteNum].getSpecies().getElement().getPricePerMol();
    }
    return returnValue;
  }

  public BravaisLattice getDefiningLattice() {
    return m_DefiningLattice;
  }
  
  public SuperLattice getDecoratedLattice() {
    
	  if (this.numPeriodicDimensions() == 0) {
		  return this.getSuperSiteLattice();
	  }
    Vector[] potentialVectors =new Vector[0];
    SuperLattice superLattice = this.getSuperSiteLattice();
    for (int primCellNum = 0; primCellNum < superLattice.numPrimCells(); primCellNum++) {
      Vector translation = superLattice.getPrimCellVector(primCellNum);
      boolean keeper = true;
      for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
        Structure.Site site = this.getDefiningSite(siteNum);
        Coordinates translatedCoords = site.getCoords().translateBy(translation);
        
        // This approach is fastest but may run inot numerical issues.
        //Structure.Site translatedSite = this.getDefiningSite(translatedCoords, CartesianBasis.getPrecision() * 2);
        
        // This appears to be a good balance between speed and robustness.
        Structure.Site translatedSite = this.getFastDefiningSite(translatedCoords);
        
        // This is slow.
        //Structure.Site translatedSite = this.getNearbyDefiningSite(translatedCoords);
        
        if (site.getSpecies() != translatedSite.getSpecies()) {
          keeper = false;
          break;
        }
      }
      if (!keeper) {continue;}
      potentialVectors = (Vector[]) ArrayUtils.appendElement(potentialVectors, translation);
    }
    //BravaisLattice subLattice = this.getDefiningLattice().getSubLattice(potentialVectors, potentialVectors.length);
    BravaisLattice subLattice = superLattice.getSubLattice(potentialVectors, potentialVectors.length);
    //System.out.println("Defining lattice size: " + this.getDefiningLattice().getCellSize() + ", SubLattice size: " + subLattice.getCellSize());
    return new SuperLattice(this.getSiteLattice(), subLattice);
  }
  
  public Structure.Site[][] groupEquivalentSites() {
    
    SpaceGroup spaceGroup = this.getPrimSpaceGroup();
    Structure.Site[][] returnArray = new Structure.Site[0][];

    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      Structure.Site site = this.getDefiningSite(siteNum);
      Coordinates siteCoords = site.getCoords();
      boolean seenBefore = false;
      for (int groupNum = 0; groupNum < returnArray.length; groupNum++) {
        Coordinates knownCoords = returnArray[groupNum][0].getCoords();
        if (spaceGroup.areSymmetricallyEquivalent(siteCoords, knownCoords)) {
          returnArray[groupNum] = (Structure.Site[]) ArrayUtils.appendElement(returnArray[groupNum], site);
          seenBefore = true;
          break;
        }
      }
      if (!seenBefore) {
        returnArray = (Structure.Site[][]) ArrayUtils.appendElement(returnArray, new Structure.Site[] {site});
      }
    }
    
    return returnArray;
  }
  
  public Structure makeSurface(int[] millerIndices, double minSlab, double minGap, double layerOffsetDistance) {
    
    Status.flow("Making ( " + millerIndices[0] + " " + millerIndices[1] + " " + millerIndices[2] + " ) surface.");
    int lcm = MSMath.LCM(millerIndices);
    Structure structure = this;
    
    int[] dirs = new int[3];
    
    dirs[0] = (millerIndices[0] == 0) ? 1 : lcm / millerIndices[0];
    dirs[1] = (millerIndices[1] == 0) ? 1 : lcm / millerIndices[1];
    dirs[2] = (millerIndices[2] == 0) ? 1 : lcm / millerIndices[2];
    
    int[][] superToDirect = new int[3][];
    
    superToDirect[0] = new int[] {0, dirs[1], -dirs[2]};
    superToDirect[1] = new int[] {-dirs[0], 0, dirs[2]};
    superToDirect[2] = ArrayUtils.copyArray(millerIndices);
    
    for (int dimNum = 0; dimNum < 3; dimNum++) {
      if (millerIndices[dimNum] == 0) {
        Arrays.fill(superToDirect[0], 0);
        superToDirect[0][dimNum] = 1;
        
        Arrays.fill(superToDirect[1], 0);
        if (millerIndices[(dimNum + 1) % 3] == 0) {
          superToDirect[1][(dimNum + 1) % 3] = 1;
        } else if (millerIndices[(dimNum + 2) % 3] == 0) {
          superToDirect[1][(dimNum + 2) % 3] = 1;
        } else {
          superToDirect[1][(dimNum + 1) % 3] = dirs[(dimNum + 1) % 3];
          superToDirect[1][(dimNum + 2) % 3] = -dirs[(dimNum + 2) % 3];
        }
      }
    }
    
    // Appears to retain only the largest of the miller indices.  That's a sensible thing to do, but I'm not sure why I did it this way.
    // Basically the idea here is to shorten the distance between the plane and the end of the non-planar vector
    /*boolean smallCells = true;
    if (smallCells) {
      int maxIndex = ArrayUtils.maxElementIndex(superToDirect[2]);
      int lastMaxIndex = maxIndex;
      int lastValue = superToDirect[2][maxIndex];
      while (superToDirect[2][maxIndex] > 0) {
        lastValue = superToDirect[2][maxIndex];
        superToDirect[2][maxIndex] = 0;
        lastMaxIndex = maxIndex;
        maxIndex = ArrayUtils.maxElementIndex(superToDirect[2]);
      }
      superToDirect[2][lastMaxIndex] = lastValue;
    }*/
    
    // Find the shortest c-vector we can
    // This is untested!
    Vector[] cellVectors = structure.getCellVectors();
    Vector superVec0 = Vector.getZero3DVector();
    Vector superVec1 = Vector.getZero3DVector();
    for (int dimNum = 0; dimNum < superToDirect[0].length; dimNum++) {
      superVec0 = superVec0.add(cellVectors[dimNum].multiply(superToDirect[0][dimNum]));
      superVec1 = superVec1.add(cellVectors[dimNum].multiply(superToDirect[1][dimNum]));
    }
    
    Vector normalUnit = superVec0.crossProduct(superVec1, CartesianBasis.getInstance()).unitVectorCartesian();
    double minLength = Double.POSITIVE_INFINITY;
    int minDimNum = -1;
    for (int dimNum = 0; dimNum < millerIndices.length; dimNum++) {
      if (millerIndices[dimNum] == 0) {continue;}
      double length = Math.abs(cellVectors[dimNum].innerProductCartesian(normalUnit));
      if ((length > 0) && (length < minLength)) {
        minLength = length;
        minDimNum = dimNum;
      }
    }
    
    //double cellNormalLength = Math.abs(superLattice.getCellVector(2).innerProductCartesian(normalUnit));
    double cellNormalLength = minLength;
    
    int numSlabCells = (int) Math.ceil(minSlab / cellNormalLength);
    int numVacuumCells = (int) Math.ceil(minGap / cellNormalLength);
    
    Arrays.fill(superToDirect[2], 0);
    //superToDirect[2][minDimNum] = numSlabCells + numVacuumCells;
    superToDirect[2][minDimNum] = 1;

    Status.detail("Normal vector has length of " + cellNormalLength + " for one unit cell.");
    Status.detail("Using " + numSlabCells + " cells for slab.");
    Status.detail("Using " + numVacuumCells + " cells for vacuum.");
    
    Structure superStructure1 = new SuperStructure(structure, superToDirect);
    BravaisLattice superLattice = superStructure1.getDefiningLattice();
    Vector cVector = superLattice.getCellVector(2);
    superStructure1 = superStructure1.translate(cVector.resize(layerOffsetDistance));
    superStructure1 = superStructure1.useSitesInCellAsDefining(false);
    
    int[][] superToDirect2 = new int[][] {
        {1, 0, 0},
        {0, 1, 0},
        {0, 0, numSlabCells + numVacuumCells},
    };
    //superStructure1 = new Structure(superStructure1);

    SuperStructure superStructure = new SuperStructure(superStructure1, superToDirect2);
    for (int primCellNum = 0; primCellNum < numVacuumCells; primCellNum++) {
      int[] cellOrigin = superStructure.getSuperLattice().getPrimCellOrigin(primCellNum);
      for (int primIndex =0; primIndex < superStructure1.numDefiningSites(); primIndex++) {
        int superIndex = superStructure.getSiteIndex(cellOrigin, primIndex);
        Structure.Site site = superStructure.getDefiningSite(superIndex);
        site.setSpecies(Species.vacancy);
      }
    }
    
    Structure outStructure = superStructure.useSitesInCellAsDefining(false);//.getCompactStructure();
    outStructure = outStructure.translate(cVector.resize(layerOffsetDistance).multiply(-1));

    return outStructure;
  }
  
  public SpaceGroup getPrimSpaceGroup() {
    
    SuperLattice decoratedLattice = this.getDecoratedLattice();
    //BravaisLattice decoratedLattice = this.getDefiningLattice();
    SpaceGroup siteSpaceGroup = this.getSiteSpaceGroup();
    
    /*int[] latticePreservingOps = decoratedLattice.getLatticePreservingOpNums(siteSpaceGroup);
    SymmetryOperation[] preservingOperations = new SymmetryOperation[latticePreservingOps.length];
    for (int opNum = 0; opNum < latticePreservingOps.length; opNum++) {
      preservingOperations[opNum] = siteSpaceGroup.getOperation(latticePreservingOps[opNum]); 
    }
    SpaceGroup preservingGroup = new SpaceGroup(preservingOperations, this.getSiteLattice());
    //SymmetryOperation[] factorOperations =preservingGroup.getFactorOperations(this.getSuperDefiningLattice());
    SymmetryOperation[] factorOperations =preservingGroup.getFactorOperations(decoratedLattice);*/
    
    //SpaceGroup decLattPreservingSpaceGroup = siteSpaceGroup.getLatticePreservingSpaceGroup(decoratedLattice);
    SpaceGroup decLattPreservingSpaceGroup = decoratedLattice.getLatticePreservingSpaceGroup(siteSpaceGroup);
    
    SymmetryOperation[] returnOperations = new SymmetryOperation[0];
    //for (int opNum = 0; opNum < siteSpaceGroup.numOperations(); opNum++) {
    //for (int opNum = 0; opNum < latticePreservingOps.length; opNum++) {
    /*for (int opNum= 0; opNum < factorOperations.length; opNum++) {
      SymmetryOperation operation = factorOperations[opNum];*/
    for (int opNum = 0; opNum < decLattPreservingSpaceGroup.numOperations(); opNum++) {
      SymmetryOperation operation = decLattPreservingSpaceGroup.getOperation(opNum);
      //SymmetryOperation operation = siteSpaceGroup.getOperation(latticePreservingOps[opNum]);
      //SymmetryOperation operation = siteSpaceGroup.getOperation(opNum);
      boolean match = true;
      for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
        Site site = m_DefiningSites[siteNum];
        Coordinates oppedCoords = operation.operate(site.getCoords());
        
        // This gets a little complicated because we may not be a primitive cell
        //Point oppedSite = this.getDefiningSite(oppedCoords);
        //Site oppedSite = this.getNearbyDefiningSite(oppedCoords);
        
        // This is much faster than calling getNearbyDefiningSite.  
        // It should always find a site, but I double the precision just in case there are numerical problems.
        // It's slightly faster to use a defining site here, as that's an intermediate step to creating a regular site.
        //Site oppedSite = this.getDefiningSite(oppedCoords, CartesianBasis.getPrecision() * 2);
        
        // This is a good balance between speed and robustness.
        Site oppedSite = this.getFastDefiningSite(oppedCoords);
        
        if (site.getSpecies() != oppedSite.getSpecies()) {
          match = false;
          break;
        }
      }
      if (match) {
        returnOperations = (SymmetryOperation[]) ArrayUtils.appendElement(returnOperations, operation);
      }
    }
    
    //return new SpaceGroup(returnOperations, m_DefiningLattice);
    //SpaceGroup reducedGroup = new SpaceGroup(returnOperations, this.getSiteLattice());
    //SymmetryOperation[] factorOperations = reducedGroup.getFactorOperations(decoratedLattice);
    //return new SpaceGroup(factorOperations, decoratedLattice);
    //return new SpaceGroup(returnOperations, this.getSiteLattice());
    
    return new SpaceGroup(returnOperations, decoratedLattice);
  }
  
  public SpaceGroup getDefiningSpaceGroup() {
    SpaceGroup primSpaceGroup = this.getPrimSpaceGroup();
    BravaisLattice decoratedLattice = primSpaceGroup.getLattice();
    int[][] decoratedSuperToDirect = decoratedLattice.getSuperToDirect(m_DefiningLattice);
    SuperLattice superDecoratedLattice = new SuperLattice(decoratedLattice, decoratedSuperToDirect);
    return primSpaceGroup.getFactoredSpaceGroup(superDecoratedLattice);
  }
  
  /**
   * This is in case the structure is not a primitive structure.
   * @return
   */
  public SpaceGroup getLatticePreservingSpaceGroup() {
    SpaceGroup primSpaceGroup = this.getPrimSpaceGroup();
    BravaisLattice decoratedLattice = primSpaceGroup.getLattice();
    int[][] decoratedSuperToDirect = decoratedLattice.getSuperToDirect(m_DefiningLattice);
    SuperLattice superDecoratedLattice = new SuperLattice(decoratedLattice, decoratedSuperToDirect);
    return superDecoratedLattice.getLatticePreservingSpaceGroup(primSpaceGroup);
    //return primSpaceGroup.getLatticePreservingSpaceGroup(superDecoratedLattice);
  }
  
  public SuperLattice getSuperSiteLattice() {
    if (m_SuperSiteLattice == null) {
      m_SuperSiteLattice = new SuperLattice(this.getSiteLattice(), m_DefiningLattice);
    }
    return m_SuperSiteLattice;
  }
  
  public BravaisLattice getSiteLattice() {
    if (m_SiteLattice == null) {
      m_SiteLattice = this.findSiteLattice();
    }
    return m_SiteLattice;
  }
  
  public SpaceGroup getSiteSpaceGroup() {
    
    if (m_SiteSpaceGroup == null) {
      m_SiteSpaceGroup = this.findSiteSpaceGroup();
    }
    return m_SiteSpaceGroup;
    
  }
  
  /**
   * This method could probably be made more efficient, but it only needs to be done once as long as
   * none of the sites move.
   * 
   * @return The space group for the unpopulated sites in this structure.
   */
  private SpaceGroup findSiteSpaceGroup() {
    
    StructureSymmetryFinder symFinder = new StructureSymmetryFinder(this);
    //return new SpaceGroup(symFinder.getSiteFactorOperations(), m_DefiningLattice);
    return new SpaceGroup(symFinder.getSiteFactorOperations(), this.getSiteLattice());
  }
  
  private BravaisLattice findSiteLattice() {

    if (this.numDefiningSites() == 0) {return m_DefiningLattice;}
    if (this.numPeriodicDimensions() == 0) {return m_DefiningLattice;}
    
    // First find potential translation vectors
    Coordinates firstCoords = m_DefiningSites[0].getCoords();
    Vector[] potentialVectors = new Vector[0];
    int numPrimCells = 1;
    
    // Sort by lattice vector length
    double[] distances = new double[m_DefiningSites.length];
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      //distances[siteNum] = m_DefiningLattice.getDistanceInWignerSeitzCell(firstCoords, m_DefiningSites[siteNum].getCoords());
      distances[siteNum] = m_DefiningLattice.getNearestImageDistance(firstCoords, m_DefiningSites[siteNum].getCoords());
    }
    int[] map = ArrayUtils.getSortPermutation(distances);
    
    AbstractLinearBasis directBasis = this.getDirectBasis();
    boolean[] addedSites = new boolean[m_DefiningSites.length];
    double precision = 1.0 / m_DefiningSites.length;
    for (int orderedSiteNum = 1; orderedSiteNum < m_DefiningSites.length; orderedSiteNum++) {
      int siteNum = map[orderedSiteNum];
      if (addedSites[siteNum]) {continue;}
      Coordinates siteCoords = m_DefiningSites[siteNum].getCoords();
      
      // I used to get the Wigner Seitz neighbor here, but that's slower than this, and I think this works OK.
      siteCoords = m_DefiningLattice.getQuickCoordsInParallelCell(firstCoords, siteCoords);
      
      // Only get potential vectors that might round off correctly.
      Vector potentialVector = new Vector(firstCoords, siteCoords);
      double[] directArray = potentialVector.getDirectionArray(directBasis);
      for (int dimNum = 0; dimNum < directArray.length; dimNum++) {
        directArray[dimNum] = MSMath.roundWithPrecision(directArray[dimNum], precision);
        //double numLayers = (directArray[dimNum] == 0) ? Double.POSITIVE_INFINITY : Math.round(1.0 / directArray[dimNum]);
        //directArray[dimNum] = (numLayers > m_DefiningSites.length) ? 0 : 1.0 / numLayers;
      }
      Vector translation = new Vector(directArray, directBasis);
      if (potentialVector.subtract(translation).length() >= CartesianBasis.getPrecision()) {
        continue;
      }
      potentialVector = translation;
      
      boolean keeper = true;
      boolean[] okSites = new boolean[m_DefiningSites.length];
      for (int siteNum2 = 0; siteNum2 < m_DefiningSites.length; siteNum2++) {
        if (okSites[siteNum2]) {continue;}
        okSites[siteNum2] = true;
        int translatedIndex = -1;
        Structure.Site site2 = m_DefiningSites[siteNum2];
        Coordinates translatedCoords = site2.getCoords();
        while (translatedIndex != site2.getIndex()) {
          translatedCoords = translatedCoords.translateBy(potentialVector);
          Structure.Site translatedSite = this.getSite(translatedCoords);
          if (translatedSite == null) {
            keeper = false; 
            break;
          }
          //System.out.println(siteNum + ", " + siteNum2 + ", " + translatedSite.getCoords().distanceFrom(translatedCoords));
          translatedIndex = translatedSite.getIndex();
          okSites[translatedIndex] = true;
        }
        if (!keeper) {break;}
      }
      if (!keeper) {continue;}
      //System.out.println(potentialVector.length());

      Coordinates shiftedCoords = firstCoords.translateBy(translation);
      while (siteNum != 0) {
        if (!addedSites[siteNum]) {
          potentialVectors = (Vector[]) ArrayUtils.appendElement(potentialVectors, potentialVector);
          addedSites[siteNum] = true;
          numPrimCells++;
        }
        shiftedCoords = shiftedCoords.translateBy(translation);
        Structure.Site definingSite = this.getDefiningSite(shiftedCoords);
        siteNum = definingSite.getIndex();
        shiftedCoords = m_DefiningLattice.getNearestImage(firstCoords, shiftedCoords); // This should make it more compact, but hasn't been tested
        potentialVector = new Vector(firstCoords, shiftedCoords);
      }
    }
    
    // Clean up any steps that aren't fractions of the number of prim cells
    precision = 1.0 / numPrimCells;
    for (int vecNum = 0; vecNum < potentialVectors.length; vecNum++) {
      Vector potentialVector = potentialVectors[vecNum];
      double[] directArray = potentialVector.getDirectionArray(directBasis);
      for (int dimNum = 0; dimNum < directArray.length; dimNum++) {
        //directArray[dimNum] -= Math.round(directArray[dimNum]);
        directArray[dimNum] = MSMath.roundWithPrecision(directArray[dimNum], precision);
      }
      Vector roundedPotentialVector = new Vector(directArray, directBasis);
      if (potentialVector.subtract(roundedPotentialVector).length() > CartesianBasis.getPrecision()) { // Something went wrong
        return m_DefiningLattice;
      }
      // Shorten the vector
      for (int dimNum = 0; dimNum < directArray.length; dimNum++) {
        directArray[dimNum] -= Math.round(directArray[dimNum]);
      }
      potentialVectors[vecNum] = new Vector(directArray, directBasis);
    }
    
    // Some numerical problem caused us not to find all of the lattice vectors
    if (m_DefiningSites.length % numPrimCells != 0) {
      return m_DefiningLattice;
    }
    
    //int numPrimCells = potentialVectors.length + 1;
    BravaisLattice returnLattice = m_DefiningLattice.getSubLattice(potentialVectors, numPrimCells);
    if (returnLattice == null) {  // This is probably due to a precision problem
      return m_DefiningLattice;
    }
    return returnLattice;
  }

  /**
   * We provide an integer basis so that you can know which basis will be common to all
   * superstructures based on this structure without having to generate the superstructures.
   *
   * @return The integer basis that will be common to all superstructures based on this structure
   */
  public DiscreteBasis getIntegerBasis() {
    return m_IntegerBasis;
  }
  
  public LinearBasis getDirectBasis() {
    return m_DefiningLattice.getLatticeBasis();
  }
  
  /* (non-Javadoc)
   * @see matsci.io.IStructureData#getDescription()
   */
  public String getDescription() {
    return this.toString();
  }
  
  public void setDescription(String description) {
    m_Description = description;
  }

  /* (non-Javadoc)
   * @see matsci.io.IStructureData#getLatticeVectors()
   */
 
  public Vector[] getCellVectors() {
    return m_DefiningLattice.getCellVectors();
  }
  
  public double getDefiningVolume() {
    return m_DefiningLattice.getCellSize();
  }
  
  public boolean[] getVectorPeriodicity() {
    return m_DefiningLattice.getDimensionPeriodicity();
  }
  
  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getNonPeriodicVectors()
   */
  /*
  public Vector[] getNonPeriodicVectors() {
    return m_DefiningLattice.getNonPeriodicVectors();
  }*/

  /* (non-Javadoc)
   * @see matsci.io.IStructureData#getSiteCoords(int)
   */
  public Coordinates getSiteCoords(int index) {
    return this.getDefiningSite(index).getCoords();
  }

  /* (non-Javadoc)
   * @see matsci.io.IStructureData#getSiteSpecies(int)
   */
  public Species getSiteSpecies(int siteIndex) {
    return this.getDefiningSite(siteIndex).getSpecies();
  }
  
  public Coordinates getCoordsForNearbySite(Coordinates coords, double distance) {
    //Coordinates[] nearbySiteCoords = this.getNearbySiteCoords(coords, distance, true);
    Structure.Site[] nearbySites = this.getNearbySites(coords, distance, true);
    if (nearbySites.length != 1) {return null;}
    return nearbySites[0].getCoords();
  }

  // TODO consider making this more robust by actually finding neighbors
  // Warning:  This can be slow.  If you know that a site is located at (or near) coords, consider getFastDefiningSite instead.
  public Structure.Site getNearbyDefiningSite(Coordinates coords) {
    
    double minDistance = Double.POSITIVE_INFINITY;
    Structure.Site returnSite = null;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Structure.Site site = m_DefiningSites[siteNum];
      double distance = m_DefiningLattice.getDistanceInWignerSeitzCell(coords, site.getCoords());
      if (distance < minDistance) {
        returnSite = site;
        minDistance = distance;
      }
    }
    return returnSite;
    
  }
  
  /**
   * Use this when you know that the coordinates are actually the coordinates of a defining site (or very close)
   * 
   * @param coords
   * @return
   */
  public Structure.Site getFastDefiningSite(Coordinates coords) {
    
    double minDistance = Double.POSITIVE_INFINITY;
    Structure.Site returnSite = null;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Structure.Site site = m_DefiningSites[siteNum];
      Coordinates siteCoords = m_DefiningLattice.getQuickCoordsInParallelCell(coords, site.getCoords());
      double distance = coords.distanceFrom(siteCoords);
      if (distance < CartesianBasis.getPrecision()) {
        return site;
      }
      
      // This makes things a little more robust against numerical problems
      if (distance < minDistance) {
        returnSite = site;
        minDistance = distance;
      }
    }
    return returnSite;
    
  }


  public Structure.Site getNearbySite(Coordinates coords) {
    
    double minDistance = Double.POSITIVE_INFINITY;
    SiteImage returnSite = null;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      Coordinates wsCoords = m_DefiningLattice.getWignerSeitzNeighbor(coords, site.getCoords());
      //double distance = m_DefiningLattice.getDistanceInWignerSeitzCell(coords, site.getCoords());
      double distance = coords.distanceFrom(wsCoords);
      if (distance < minDistance) {
        returnSite = new SiteImage(wsCoords, site);
        minDistance = distance;
      }
    }
    // The below line isn't necessary now that we've fixed the Wigner Seitz code
    //Coordinates imageCoords = m_DefiningLattice.getNearestImage(coords, returnSite.getCoords());
    //return new SiteImage(imageCoords, returnSite);
    
    return returnSite;
    
  }
  
  public Structure.Site getNearbySiteWithElement(Coordinates coords, Element element) {
    
    double minDistance = Double.POSITIVE_INFINITY;
    SiteImage returnSite = null;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      if (site.getSpecies().getElement() != element) {continue;}
      Coordinates wsCoords = m_DefiningLattice.getWignerSeitzNeighbor(coords, site.getCoords());
      double distance = coords.distanceFrom(wsCoords);
      if (distance < minDistance) {
        returnSite = new SiteImage(wsCoords, site);
        minDistance = distance;
      }
    }
    return returnSite;
    
  }
  
  public Structure.Site getNearbySiteWithSpecies(Coordinates coords, Species species) {
    
    double minDistance = Double.POSITIVE_INFINITY;
    SiteImage returnSite = null;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      if (site.getSpecies() != species) {continue;}
      Coordinates wsCoords = m_DefiningLattice.getWignerSeitzNeighbor(coords, site.getCoords());
      double distance = coords.distanceFrom(wsCoords);
      if (distance < minDistance) {
        returnSite = new SiteImage(wsCoords, site);
        minDistance = distance;
      }
    }
    return returnSite;
    
  }
  
  public double distanceToNearestSite(Coordinates coords) {
    
    double minDistance = Double.POSITIVE_INFINITY;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      Coordinates wsCoords = m_DefiningLattice.getWignerSeitzNeighbor(coords, site.getCoords());
      //double distance = m_DefiningLattice.getDistanceInWignerSeitzCell(coords, site.getCoords());
      double distance = coords.distanceFrom(wsCoords);
      minDistance = Math.min(minDistance, distance);
    }
    // The below line isn't necessary now that we've fixed the Wigner Seitz code
    //Coordinates imageCoords = m_DefiningLattice.getNearestImage(coords, returnSite.getCoords());
    //return new SiteImage(imageCoords, returnSite);
    
    return minDistance;
    
  }
  
  public double distanceToNearestSiteWithElement(Coordinates coords, Element element) {
    
    double minDistance = Double.POSITIVE_INFINITY;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      if (site.getSpecies().getElement() != element) {continue;}
      Coordinates wsCoords = m_DefiningLattice.getWignerSeitzNeighbor(coords, site.getCoords());
      //double distance = m_DefiningLattice.getDistanceInWignerSeitzCell(coords, site.getCoords());
      double distance = coords.distanceFrom(wsCoords);
      minDistance = Math.min(minDistance, distance);
    }
    // The below line isn't necessary now that we've fixed the Wigner Seitz code
    //Coordinates imageCoords = m_DefiningLattice.getNearestImage(coords, returnSite.getCoords());
    //return new SiteImage(imageCoords, returnSite);
    
    return minDistance;
    
  }
  
  public Structure.Site getNearbySite(Coordinates coords, int siteIndex) {
    
    DefiningSite returnSite = m_DefiningSites[siteIndex];
    Coordinates imageCoords = m_DefiningLattice.getNearestImage(coords, returnSite.getCoords());
    return new SiteImage(imageCoords, returnSite);
    
  }
  
  public Coordinates[] getNearbySiteCoords(Coordinates coords, double distance, boolean includeGivenCoords) {
    
    //return this.old_getNearbySiteCoords(coords, distance, includeGivenCoords);
    
    /*Structure.Site[] nearbySites = this.getNearbySites(coords, distance, includeGivenCoords);
    Coordinates[] returnArray = new Coordinates[nearbySites.length];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = nearbySites[siteNum].getCoords();
    }
    return returnArray;*/
    
    BravaisLattice lattice = this.getDefiningLattice();
    
    Coordinates[] returnArray = new Coordinates[0];
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      Coordinates[] nearbyCoords = lattice.getNearbyImages(coords, site.getCoords(), distance, includeGivenCoords);
      returnArray = (Coordinates[]) ArrayUtils.appendArray(returnArray, nearbyCoords);
    }
    
    return returnArray;
  }
  
  /*public Point[] old_getNearbySites(Coordinates coords, double distance, boolean inlcludeGiven) {
    Coordinates[] nearbyCoords = this.getNearbySiteCoords(coords, distance, inlcludeGiven);
    Point[] returnArray = new Point[nearbyCoords.length];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = this.getSite(nearbyCoords[siteNum]);
    }
    return returnArray;
  }*/
  
  public Structure.Site[] getNearestNeighbors(Structure.Site site) {
    return this.getNearestNeighbors(site, CartesianBasis.getPrecision());
  }
  
  /**
   * 
   * @param site
   * @param tolerance If this is positive, it's added to the nn distance.  
   * If negative, it's made positive and multiplied by the nn distance. 
   * @return
   */
  public Structure.Site[] getNearestNeighbors(Structure.Site site, double tolerance) {
    
    Coordinates siteCoords = site.getCoords();
    //Coordinates primCoords = m_DefiningLattice.hardRemainder(siteCoords);

    BravaisLattice lattice = this.getDefiningLattice();
    Coordinates[] candidates = new Coordinates[this.numDefiningSites()];
    double minDistance = (this.numDefiningSites() == 1) ? m_DefiningLattice.getMinPeriodicDistance() : Double.POSITIVE_INFINITY;
    double[] distances = new double[candidates.length];
    for (int siteNum = 0; siteNum < candidates.length; siteNum++) {
      if (siteNum == site.getIndex()) {
        continue;
      }
      Structure.Site definingSite = this.getDefiningSite(siteNum);
      candidates[siteNum] = lattice.getNearestImage(siteCoords, definingSite.getCoords());
      distances[siteNum] = siteCoords.distanceFrom(candidates[siteNum]);
      minDistance = Math.min(minDistance, distances[siteNum]);
    }
    // This was used before I fixed the Wigner Seitz code to really return the nearest neighbor
    /*double searchDistance = Double.POSITIVE_INFINITY;
    BravaisLattice definingLattice = this.getDefiningLattice();
    Vector[] periodicVectors = definingLattice.getPeriodicVectors();
    for (int vecNum = 0; vecNum < periodicVectors.length; vecNum++) {
      searchDistance = Math.min(searchDistance, periodicVectors[vecNum].length());
    }
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Structure.Site definingSite = m_DefiningSites[siteNum];
      if (definingSite.getIndex() == site.getIndex()) {continue;}
      //searchDistance = Math.min(searchDistance, definingSite.distanceFrom(primCoords));
      searchDistance = Math.min(searchDistance, definingLattice.getDistanceInWignerSeitzCell(siteCoords, definingSite.getCoords()));
    }
    searchDistance += tolerance;
    Structure.Site[] candidateSites = this.getNearbySites(siteCoords, searchDistance, false);
    
    // Now find the neighbors
    double minDistance = searchDistance;
    double[] distances = new double[candidateSites.length];
    for (int siteNum = 0; siteNum < candidateSites.length; siteNum++) {
      double distance = candidateSites[siteNum].distanceFrom(site.getCoords());
      minDistance = Math.min(minDistance, distance);
      distances[siteNum] = distance;
    }*/
    if (tolerance >= 0) {
    	minDistance += tolerance;
    } else {
    	minDistance *= -tolerance;
    }
    return this.getNearbySites(siteCoords, minDistance, false);
    /*int numNeighbors = 0;
    for (int siteNum = 0; siteNum < distances.length; siteNum++) {
      if (distances[siteNum] < minDistance) {numNeighbors++;}
    }
    
    Structure.Site[] returnArray = new Structure.Site[numNeighbors];
    int returnIndex = 0;
    for (int siteNum = 0; siteNum < candidates.length; siteNum++) {
    //for (int siteNum = 0; siteNum < candidateSites.length; siteNum++) {
      if (distances[siteNum] < minDistance) {
        returnArray[returnIndex++] = new SiteImage(candidates[siteNum], this.getDefiningSite(siteNum));
        //returnArray[returnIndex++] = candidateSites[siteNum];
      }
    }
    return returnArray;*/
  }
  
  public Structure.Site[] getNearbySites(Coordinates coords, double minDistance, double maxDistance) {
    
    BravaisLattice lattice = this.getDefiningLattice();
    boolean includeGiven = (minDistance <= 0);
    
    Structure.Site[] returnArray = new Structure.Site[0];
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      Coordinates[] nearbyCoords = lattice.getNearbyImages(coords, site.getCoords(), maxDistance, includeGiven);
      Structure.Site[] nearbySites = new Structure.Site[nearbyCoords.length];
      int newSiteNum = 0;
      for (int neighborNum = 0; neighborNum < nearbyCoords.length; neighborNum++) {
        if (nearbyCoords[neighborNum].distanceFrom(coords) < minDistance) {continue;}
        nearbySites[newSiteNum++] = new SiteImage(nearbyCoords[neighborNum], site);
      }
      nearbySites = (Structure.Site[]) ArrayUtils.truncateArray(nearbySites, newSiteNum);
      returnArray = (Structure.Site[]) ArrayUtils.appendArray(returnArray, nearbySites);
    }
    
    return returnArray;
    
  }
  
  public Structure.Site[] getNearbySites(Coordinates coords, double distance, boolean includeGiven) {
    
    //return this.old_getNearbySites(coords, distance, includeGiven);
    
    /*BravaisLattice lattice = this.getDefiningLattice();
    
    ArrayList nearbyCoords = new ArrayList();
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      double[] coordArray = site.getCoords().getCoordArray(this.getDirectBasis());
      lattice.getSitesRecursive(coords, site, coordArray, 0, nearbyCoords, distance, includeGiven);
    }
    Structure.Site[] returnArray = new Structure.Site[returnSites.size()];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = (Structure.Site) returnSites.get(siteNum);
    }
    return returnArray;*/
    
    BravaisLattice lattice = this.getDefiningLattice();
    
    Structure.Site[] returnArray = new Structure.Site[0];
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      Coordinates[] nearbyCoords = lattice.getNearbyImages(coords, site.getCoords(), distance, includeGiven);
      Structure.Site[] nearbySites = new Structure.Site[nearbyCoords.length];
      for (int neighborNum = 0; neighborNum < nearbyCoords.length; neighborNum++) {
        nearbySites[neighborNum] = new SiteImage(nearbyCoords[neighborNum], site);
      }
      returnArray = (Structure.Site[]) ArrayUtils.appendArray(returnArray, nearbySites);
    }
    
    return returnArray;
    
  }
  
  public Structure.Site[] getNearbySitesWithElement(Coordinates coords, Element element, double distance, boolean includeGiven) {
    
    BravaisLattice lattice = this.getDefiningLattice();
    
    Structure.Site[] returnArray = new Structure.Site[0];
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      if (site.getSpecies().getElement() != element) {continue;}
      Coordinates[] nearbyCoords = lattice.getNearbyImages(coords, site.getCoords(), distance, includeGiven);
      Structure.Site[] nearbySites = new Structure.Site[nearbyCoords.length];
      for (int neighborNum = 0; neighborNum < nearbyCoords.length; neighborNum++) {
        nearbySites[neighborNum] = new SiteImage(nearbyCoords[neighborNum], site);
      }
      returnArray = (Structure.Site[]) ArrayUtils.appendArray(returnArray, nearbySites);
    }
    
    return returnArray;
    
  }
  
  public Structure.Site[] getNearbySitesWithSpecies(Coordinates coords, Species species, double distance, boolean includeGiven) {
    
    BravaisLattice lattice = this.getDefiningLattice();
    
    Structure.Site[] returnArray = new Structure.Site[0];
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      DefiningSite site = m_DefiningSites[siteNum];
      if (site.getSpecies() != species) {continue;}
      Coordinates[] nearbyCoords = lattice.getNearbyImages(coords, site.getCoords(), distance, includeGiven);
      Structure.Site[] nearbySites = new Structure.Site[nearbyCoords.length];
      for (int neighborNum = 0; neighborNum < nearbyCoords.length; neighborNum++) {
        nearbySites[neighborNum] = new SiteImage(nearbyCoords[neighborNum], site);
      }
      returnArray = (Structure.Site[]) ArrayUtils.appendArray(returnArray, nearbySites);
    }
    
    return returnArray;
    
  }
  
  public Structure removeOxidationStates() {
	StructureBuilder builder = new StructureBuilder(this);
	for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
		Species species = builder.getSiteSpecies(siteNum);
		Species elementSpecies = Species.get(species.getElement());
		builder.setSiteSpecies(siteNum, elementSpecies);
	}
	return new Structure(builder);
  }
  
  public double getOxidationPerUnitCell() {
    
    double returnValue = 0;
    
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      returnValue += m_DefiningSites[siteNum].getAtom().getSpecies().getOxidationState();
    }
    
    return returnValue;
    
  }

  /*protected double getSitesRecursive(Coordinates baseCoords, DefiningSite definingSite, double[] currCoordArray, int dimNum, ArrayList returnSites, double cutoff, boolean includeGiven) {
    
    AbstractBasis directBasis = this.getDirectBasis();
    
    // End of the road
    if (dimNum == currCoordArray.length) {
      double distance = baseCoords.distanceFrom(currCoordArray, directBasis);
      if (distance <= cutoff && (includeGiven || distance > CartesianBasis.getPrecision())) {
        Coordinates siteCoords = new Coordinates(currCoordArray, directBasis);
        Structure.Site returnSite = new SiteImage(siteCoords, definingSite);
        returnSites.add(returnSite);
      }
      return distance;
    }
    
    if (!m_DefiningLattice.isDimensionPeriodic(dimNum)) {
      return this.getSitesRecursive(baseCoords, definingSite, currCoordArray, dimNum + 1, returnSites, cutoff, includeGiven);
    }
    
    double minDistance = Double.POSITIVE_INFINITY;
    double distance = Double.POSITIVE_INFINITY;
    double lastDistance = Double.POSITIVE_INFINITY;
    
    double[] origCoordArray = ArrayUtils.copyArray(currCoordArray);
    double minCoord = origCoordArray[dimNum];
    do  {
      lastDistance = distance;
      distance = this.getSitesRecursive(baseCoords, definingSite, currCoordArray, dimNum + 1, returnSites, cutoff, includeGiven);
      if (distance < minDistance) {
        minDistance = distance;
        minCoord = currCoordArray[dimNum];
      }
      currCoordArray[dimNum]++;
    } while (distance < cutoff || distance < lastDistance);
    
    distance = Double.POSITIVE_INFINITY;
    //currCoordArray[dimNum] = origCoord - 1;
    System.arraycopy(origCoordArray, dimNum, currCoordArray, dimNum, origCoordArray.length - dimNum);
    currCoordArray[dimNum]--;
    do {
      lastDistance = distance;
      distance = this.getSitesRecursive(baseCoords, definingSite, currCoordArray, dimNum + 1, returnSites, cutoff, includeGiven);
      if (distance < minDistance) {
        minDistance = distance;
        minCoord = currCoordArray[dimNum];
      }
      currCoordArray[dimNum]--;
    } while (distance < cutoff || distance < lastDistance);
    
    currCoordArray[dimNum] = minCoord;
    return minDistance;
    
  }*/
  /*
  public Coordinates[] old_getNearbySiteCoords(Coordinates coords, double distance, boolean includeGivenCoords) {
    
    int[] intCenterArray = m_DefiningLattice.hardFloor(coords);
    int[] periodicIndices = m_DefiningLattice.getPeriodicIndices();
    double[] intCoordArray = new double[periodicIndices.length + 1];
    Coordinates[] returnArray = new Coordinates[0];
    int shellSize = 0;
    boolean foundCoords = true;
    while (foundCoords == true) {
      if (shellSize > 0) {foundCoords = false;}
      //if (shellSize > 1) {foundCoords = false;}
      int[] shellDimensions = new int[periodicIndices.length];
      for (int dimNum = 0; dimNum < shellDimensions.length; dimNum++) {
        shellDimensions[dimNum] = shellSize * 2 + 1;
      }
      ArrayIndexer cellIterator = new ArrayIndexer(shellDimensions);
      int[] offSets = cellIterator.getInitialState();
      do {
        boolean onShell = (offSets.length == 0);
        for (int dimNum = 0; dimNum < offSets.length; dimNum++) {
          //offSets[dimNum] -= shellSize;
          int shiftedOffset = offSets[dimNum] - shellSize;
          intCoordArray[dimNum] = intCenterArray[periodicIndices[dimNum]] + shiftedOffset;
          if (Math.abs(shiftedOffset) == shellSize) {onShell = true;}
        }
        if (!onShell) {continue;}
        for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
          intCoordArray[intCoordArray.length - 1] = siteNum;
          Coordinates siteCoords = new Coordinates(intCoordArray, this.getIntegerBasis());
          if (siteCoords.distanceFrom(coords) < distance) {
            if (includeGivenCoords || !siteCoords.isCloseEnoughTo(coords)) {
              returnArray = (Coordinates[]) ArrayUtils.appendElement(returnArray, siteCoords);
              foundCoords = true;
            }
          }
        }
      } while (cellIterator.increment(offSets));
      
      if (offSets.length == 0) {break;}
      shellSize += 1;
    }
    
    return returnArray;
    
  }*/
  
  public SuperStructure getConventionalStructure() {
    
    BravaisLattice lattice = this.getDefiningLattice();
    SuperLattice superLattice = lattice.getConventionalLattice();
    return new SuperStructure(this, superLattice.getSuperToDirect());
  }
  
  public SuperStructure getConventionalPrimStructure() {
    
    BravaisLattice lattice = this.getDefiningLattice();
    BravaisLattice conventionalPrimLattice = lattice.getConventionalPrimLattice();
    return new SuperStructure(this, lattice.getSuperToDirect(conventionalPrimLattice));
  }
  
  public SuperStructure getMinimalSuperStructure(double minVectorLength) {
    
    return new SuperStructure(this, minVectorLength);
  }
  
  public SuperStructure getCompactSuperStructure(int numPrimCells) {
    
    return new SuperStructure(this, numPrimCells);
  }
  
  public Structure getCompactStructure() {
    
	  if (this.numPeriodicDimensions() == 0) {
		  return new Structure(this);
	  }
    /*
    Coordinates[] latticeGrid = m_DefiningLattice.generateGrid(10);
    Coordinates origin = m_DefiningLattice.getOrigin();
    Vector[] cellVectors = this.getCellVectors();
    boolean [] isVectorPeriodic = this.areVectorsPeriodic();
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      if (!isVectorPeriodic[vecNum]) {continue;}
      
      Vector[] basisVectors = LinearBasis.fill3DBasis(latticeVectors);
      LinearBasis latticeBasis =new LinearBasis(basisVectors);
 
      double minDistance = Double.POSITIVE_INFINITY;
      Vector nextVector = null;
      for (int pointNum = 0; pointNum < latticeGrid.length; pointNum++) {
        Coordinates gridPoint = latticeGrid[pointNum];
        double distance= gridPoint.distanceFrom(origin);
        if (distance + CartesianBasis.getPrecision() >= minDistance) {continue;}
        double[] returnCoordArray = gridPoint.getCoordArray(latticeBasis);
        boolean isIndependent = false;
        for (int dimNum = vecNum; dimNum < 3; dimNum++) {
          if (Math.abs(returnCoordArray[dimNum]) > CartesianBasis.getPrecision()) {
            isIndependent = true;
            break;
          }
        }
        if (isIndependent) {
          nextVector = new Vector(origin, gridPoint);
          minDistance = distance;
        }
      }
      
      latticeVectors = (Vector[]) ArrayUtils.appendElement(latticeVectors, nextVector);
    }*/
    
    /*Vector[] cellVectors = this.getCellVectors();
    boolean[] isVectorPeriodic = this.getVectorPeriodicity();
    boolean minimizing = true;
    while (minimizing) {
      minimizing = false;
      for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
        Vector vec = cellVectors[vecNum];
        if (!isVectorPeriodic[vecNum]) {continue;}
        for (int deltaVecNum = 0; deltaVecNum < cellVectors.length; deltaVecNum++) {
          if (deltaVecNum == vecNum) {continue;}
          if (!isVectorPeriodic[deltaVecNum]) {continue;}
          Vector deltaVec = cellVectors[deltaVecNum];
          int direction = vec.innerProductCartesian(deltaVec) < 0 ? 1 : -1;
          deltaVec = deltaVec.multiply(direction);
          Vector newVec = vec.add(deltaVec);
          while (newVec.length() < vec.length()) {
            vec = newVec;
            newVec = vec.add(deltaVec);
            minimizing = true;
          }
          cellVectors[vecNum] = vec;
        }
      }
    }*/
    
    BravaisLattice compactLattice = m_DefiningLattice.getCompactLattice();

    StructureBuilder structureBuilder = new StructureBuilder();
    structureBuilder.setCellVectors(compactLattice.getCellVectors());
    structureBuilder.setVectorPeriodicity(compactLattice.getDimensionPeriodicity());
    structureBuilder.addSites(m_DefiningSites);
    structureBuilder.setDescription(getDescription());
    return new Structure(structureBuilder);
  }

  // The code to find conventional cells is limited, and only 
  // necessarily works with symmorphic space groups.
  /*
  public SuperStructure getConventionalStructure() {
    
    BravaisLattice conventionalLattice = this.getDefiningSpaceGroup().getConventionalLattice();
    int[][] superToDirect = m_DefiningLattice.getSuperToDirect(conventionalLattice);
    return new SuperStructure(this, superToDirect);

  }*/
  
  public Structure useSitesInCellAsDefining(boolean useSoftRemainder) {
    StructureBuilder builder = new StructureBuilder(this);
    BravaisLattice lattice = this.getDefiningLattice();
    for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
      if (useSoftRemainder) {
        builder.setSiteCoordinates(siteNum, lattice.softRemainder(builder.getSiteCoords(siteNum)));
      } else {
        builder.setSiteCoordinates(siteNum, lattice.hardRemainder(builder.getSiteCoords(siteNum)));
      }
    }
    return new Structure(builder);
  }
  
  public Structure perturbSlightly(double width) {
      
    StructureBuilder builder = new StructureBuilder(this);
    Random generator = new Random();
    
    for (int siteNum= 0; siteNum < builder.numDefiningSites(); siteNum++) {
      Coordinates siteCoords = builder.getSiteCoords(siteNum);
      double[] cartArray = siteCoords.getCartesianArray();
      for (int dimNum = 0; dimNum < cartArray.length; dimNum++) {
        cartArray[dimNum] += (generator.nextDouble() -0.5) * width;
      }
      builder.setSiteCoordinates(siteNum, new Coordinates(cartArray, CartesianBasis.getInstance()));
    }
    
    return new Structure(builder);
  }
  
  public Structure symmetrize() {
    
    SpaceGroup spaceGroup = this.getDefiningSpaceGroup();
    double[][] averageCoords = new double[this.numDefiningSites()][3];
    AbstractBasis directBasis = this.getDirectBasis();
    for (int opNum = 0; opNum < spaceGroup.numOperations(); opNum++) {
      SymmetryOperation op = spaceGroup.getOperation(opNum);
      
      for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
        Coordinates origCoords = this.getSiteCoords(siteNum);
        Coordinates oppedCoords = op.operate(origCoords);
        double[] oppedCoordArray = oppedCoords.getCoordArray(directBasis);
        Structure.Site definingSite = this.getDefiningSite(oppedCoords);
        double[] siteCoordArray = definingSite.getCoords().getCoordArray(directBasis);
        for (int dimNum =0; dimNum < 3; dimNum++) {
          oppedCoordArray[dimNum] += Math.round(siteCoordArray[dimNum] - oppedCoordArray[dimNum]);
        }
        double[] cartCoords = directBasis.transformToCartesian(oppedCoordArray);
        for (int dimNum =0; dimNum < 3; dimNum++) {
          averageCoords[definingSite.getIndex()][dimNum] += cartCoords[dimNum];
        }
      }
    }
    
    StructureBuilder builder = new StructureBuilder(this);
    for (int siteNum = 0; siteNum < averageCoords.length; siteNum++) {
      double[] coordArray = averageCoords[siteNum];
      for (int dimNum =0; dimNum < coordArray.length; dimNum++) {
        coordArray[dimNum] /= spaceGroup.numOperations();
      }
      builder.setSiteCoordinates(siteNum, new Coordinates(coordArray, CartesianBasis.getInstance()));
    }
    
    return new Structure(builder);
  }
  
  // TODO Test this
  public Structure findPrimStructure() {
	  
	  if (this.numPeriodicDimensions() == 0) {
		  return new Structure(this);
	  }
    
    /*SpaceGroup factorGroup = this.getSpaceGroup(); //.getFactoredSpaceGroup(getSuperDefiningLattice());

    // Find operations that are pure translations.
    //Vector[] potentialVectors = this.getPeriodicVectors();
    Vector[] potentialVectors = new Vector[0];
    for (int operationNum = 0; operationNum < factorGroup.numOperations(); operationNum++) {
      SymmetryOperation op = factorGroup.getOperation(operationNum);
      Vector pureTranslation = op.isPureTranslation();
      if (pureTranslation == null) {continue;}
      //double[][] pointOperator = op.getPointOperator();
      
      //boolean identity = true;
      //for (int row = 0; row < pointOperator.length; row++) {
      //  double[] rowOp = pointOperator[row];
      //  for (int col = 0; col < rowOp.length; col++) {
      //    if (Math.abs(pointOperator[row][col] - identityOperator[row][col]) > CartesianBasis.getPrecision()) {
      //      identity = false;
      //      break;
      //    }
      //  }
      //  if (!identity) {break;}
      //}
      //if (!identity) {continue;}
      //Vector pureTranslation = op.getTranslation();
      
      //if (pureTranslation.length() == 0) {continue;}
      Coordinates origin = m_DefiningLattice.getOrigin();
      Coordinates translatedCoords = m_DefiningLattice.softRemainder(origin.translateBy(pureTranslation));
      Vector potentialVector = new Vector(origin, translatedCoords);
      potentialVectors = (Vector[]) ArrayUtils.appendElement(potentialVectors, potentialVector);
    }
    
    int numPrimCells = potentialVectors.length;
    if (numPrimCells == 1) {return this;}
    double targetVolume = m_DefiningLattice.getCellSize() / numPrimCells;
    
    //Vector[] cellVectors = this.getCellVectors();
    Vector[] periodicVectors = m_DefiningLattice.getPeriodicVectors();
    //boolean[] isVectorPeriodic = this.getVectorPeriodicity();
    boolean foundPrimCell = false;
    for (int vecNum = 0; vecNum < periodicVectors.length; vecNum++) {

      //if (!isVectorPeriodic[vecNum]) {continue;}
      //Vector[] basisVectors = LinearBasis.fill3DBasis(latticeVectors);

      //double minDistance = Double.POSITIVE_INFINITY;
      //Vector nextVector = null;
      double minLength = Double.POSITIVE_INFINITY;
      Vector minVector = periodicVectors[vecNum];
      for (int potentialVecNum = 0; potentialVecNum < potentialVectors.length; potentialVecNum++) {
        Vector potentialVector = potentialVectors[potentialVecNum];
        //double distance= potentialVector.length();
        
        // Check to make sure this is smaller than all other similar vectors
        //if (distance + CartesianBasis.getPrecision() > minDistance) {continue;}
        
        periodicVectors[vecNum] = potentialVector;
                
        // Check to make sure this is independent of the previously found vectors
        //double[] returnCoordArray = potentialVector.getDirection().getCoordArray(latticeBasis);
        //boolean isIndependent = false;
        //for (int dimNum = vecNum; dimNum < 3; dimNum++) {
        //  if (Math.abs(returnCoordArray[dimNum]) > CartesianBasis.getPrecision()) {
        //    isIndependent = true;
        //    break;
        //  }
        //}
        //if (!isIndependent) {continue;}
        
        // Check to make sure the volume of the resulting cell is actually a minimum
        //if (vecNum == m_DefiningLattice.numDimensions() -1) {
          //Vector[] newLatticeVectors = (Vector[]) ArrayUtils.appendElement(latticeVectors, potentialVector);
          //ParallelCell cell = new ParallelCell(cellVectors, isVectorPeriodic);
          double cellSize= ParallelCell.getCellSize(periodicVectors);
          //System.out.println(cellSize + ", " + targetVolume);
          if (Math.round(cellSize / targetVolume) == 1) {
            foundPrimCell = true;
            break;
          }
          double length = potentialVector.length();
          if (length < minLength && cellSize > targetVolume) {
            minVector = potentialVector;
            minLength = length;
          }
          periodicVectors[vecNum] = minVector;
        //}
        
        //nextVector = potentialVector;
        //minDistance = distance;
      }
      if (foundPrimCell) {break;}
      //latticeVectors = (Vector[]) ArrayUtils.appendElement(latticeVectors, nextVector);
    }
    
    if (!foundPrimCell) {
      throw new RuntimeException("Error finding primitive cell!");
    }
    Vector[] cellVectors = m_DefiningLattice.getCellVectors();
    boolean[] isVectorPeriodic = m_DefiningLattice.getDimensionPeriodicity();
    int cellIndex = 0;
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      if (!isVectorPeriodic[vecNum]) {continue;}
      cellVectors[vecNum] = periodicVectors[cellIndex++];
    }*/
    
    //BravaisLattice newLattice = new BravaisLattice(cellVectors, isVectorPeriodic);
    BravaisLattice newLattice = this.getDecoratedLattice();
    StructureBuilder structureBuilder = new StructureBuilder();
    //structureBuilder.setCellVectors(cellVectors);
    structureBuilder.setCellVectors(newLattice.getCellVectors());
    //structureBuilder.setVectorPeriodicity(isVectorPeriodic);
    structureBuilder.setVectorPeriodicity(newLattice.getDimensionPeriodicity());
    structureBuilder.setDescription(getDescription());

    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Site site = m_DefiningSites[siteNum];
      //Coordinates remainderCoords = newLattice.softRemainder(site.getCoords().getCartesianCoords());
      Coordinates siteCoords =site.getCoords();
      boolean seenBefore = false;
      for (int prevCoordNum = 0; prevCoordNum < structureBuilder.numDefiningSites(); prevCoordNum++) {
        if (newLattice.areTranslationallyEquivalent(structureBuilder.getSiteCoords(prevCoordNum), siteCoords)) {
        //if (remainderCoords.isCloseEnoughTo(structureBuilder.getSiteCoords(prevCoordNum))) {
          seenBefore = true;
          break;
        }
      }
      if (seenBefore) {continue;}
      //structureBuilder.addSite(remainderCoords, site.getSpecies());
      structureBuilder.addSite(newLattice.hardRemainder(siteCoords), site.getSpecies());
    }
    
    return new Structure(structureBuilder);    
  }
  
  public boolean isPrimitive() {
    return this.getDefiningSpaceGroup().isPrimitive();
  }

  public boolean isChargeBalanced() {
    
    return this.isChargeBalanced(0);
    
  }
  
  public boolean isChargeBalanced(double tolerancePerAtom) {
    
    double totalCharge = 0;
    int numAtoms = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Species species = this.getSiteSpecies(siteNum);
      if (species == null) {continue;}
      totalCharge += species.getOxidationState();
      numAtoms++;
    }
    
    return (Math.abs(totalCharge / numAtoms) <= tolerancePerAtom);
  }
  
  public void copyOccupancies(IStructureData data) {
    
    Structure dataStructure = new Structure(data);
    
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      Coordinates myCoords = this.getSiteCoords(siteNum);
      Site dataSite = dataStructure.getSite(myCoords);
      if (dataSite == null) {
        throw new RuntimeException("Cannot copy occupancies because site coordinates are different");
      }
      this.getDefiningSite(siteNum).setSpecies(dataSite.getSpecies());
    }
  }
  
  public Structure operate(SymmetryOperation operator) {
    
    StructureBuilder builder = new StructureBuilder(this);
    Vector[] cellVectors = this.getCellVectors();
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      cellVectors[vecNum] = operator.operate(cellVectors[vecNum]);
    }
    builder.setCellVectors(cellVectors);
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Structure.Site site = m_DefiningSites[siteNum];
      builder.setSiteCoordinates(siteNum, operator.operate(site.getCoords()));
    }
    return new Structure(builder);
    
  }
  
  public Structure scaleLatticeConstant(double scaleFactor) {
    StructureBuilder builder = new StructureBuilder(this);
    Vector[] cellVectors = this.getCellVectors();
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      cellVectors[vecNum] = cellVectors[vecNum].multiply(scaleFactor);
    }
    builder.setCellVectors(cellVectors);
    LinearBasis newBasis = new LinearBasis(cellVectors);
    for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
      Coordinates coords = builder.getSiteCoords(siteNum);
      double[] thisCoordArray = coords.getCoordArray(this.getDirectBasis());
      Coordinates newCoords = new Coordinates(thisCoordArray, newBasis);
      builder.setSiteCoordinates(siteNum, newCoords);
    }
    return new Structure(builder);
  }
  
  
  /**
   * I'm not sure we'll ever need this so I got rid of it.
   */
  
  /*
  public Structure removePeriodicDimension(Vector vector) {
    
    Vector unitVector = vector.unitVectorCartesian();
    StructureBuilder builder = new StructureBuilder(this);
    
    Vector[] cellVectors = builder.getCellVectors();
    boolean[] isVectorPeriodic = builder.getVectorPeriodicity();
    
    int numRemainingPeriodicVectors = m_DefiningLattice.numPeriodicVectors() - 1;
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      if (!isVectorPeriodic[vecNum]) {continue;}
      Vector cellVector = cellVectors[vecNum];
      double projection = cellVector.innerProductCartesian(unitVector);
      cellVector = cellVector.subtract(unitVector.multiply(projection));
      if (cellVector.length() < CartesianBasis.getPrecision() || numRemainingPeriodicVectors == 0) {
        cellVectors[vecNum] = unitVector;
        isVectorPeriodic[vecNum] = false;
      } else {
        cellVectors[vecNum] = cellVector;
        numRemainingPeriodicVectors--;
      }
    }
    builder.setCellVectors(cellVectors);
    builder.setVectorPeriodicity(isVectorPeriodic);

    Vector[] basisVectors = LinearBasis.fill3DBasis(new Vector[] {vector});
    isVectorPeriodic = new boolean[basisVectors.length];
    isVectorPeriodic[0] = true;
    BravaisLattice lattice = new BravaisLattice(basisVectors, isVectorPeriodic);

    for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
      Coordinates siteCoords = builder.getSiteCoords(siteNum);
      siteCoords = lattice.removeLattice(siteCoords);
      builder.setSiteCoordinates(siteNum, siteCoords);
    }
    
    return new Structure(builder);
    
  }*/
  
  public Structure removeElement(Element element) {
    
    StructureBuilder builder = new StructureBuilder(this);
    for (int siteNum = m_DefiningSites.length - 1; siteNum >= 0; siteNum--) {
      if (builder.getSiteSpecies(siteNum).getElement() == element) {
        builder.removeSite(siteNum);
      }
    }
    return new Structure(builder);
  }
  
  public int containsCoincidentSites() {
    
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      Coordinates siteCoords1 = m_DefiningSites[siteNum].getCoords();
      for (int siteNum2 = 0; siteNum2 < siteNum; siteNum2++) {
        Coordinates siteCoords2 = m_DefiningSites[siteNum2].getCoords();
        if (m_DefiningLattice.areTranslationallyEquivalent(siteCoords1, siteCoords2)) {
          return siteNum;
        }
      }
    }
    
    return -1;
    
  }
  
  public Structure removeDefiningSite(int siteNum) {
    
    StructureBuilder builder = new StructureBuilder(this);
    builder.removeSite(siteNum);
    return new Structure(builder);
    
  }
  
  public Structure translate(Vector translation) {
    
    StructureBuilder builder = new StructureBuilder(this);
    for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
      Coordinates siteCoords = builder.getSiteCoords(siteNum);
      siteCoords = siteCoords.translateBy(translation);
      builder.setSiteCoordinates(siteNum, siteCoords);
    }
    
    return new Structure(builder);
    
  }
  
  public Site getShiftedSite(Site shiftedSite, Site siteToShift) {
	  
	  Vector shiftVector = new Vector(shiftedSite.getDefiningSite().getCoords(), shiftedSite.getCoords());
	  Coordinates shiftedCoords = siteToShift.getCoords().translateBy(shiftVector);
	  return new SiteImage(shiftedCoords, siteToShift.getDefiningSite());
	  
  }
  
  public double getMolFraction(Element element) {
    int numOccupiedSites = 0;
    int numMatchingSites = 0;
    for (int siteNum = 0; siteNum < m_DefiningSites.length; siteNum++) {
      if (m_DefiningSites[siteNum] == null) {continue;}
      if (m_DefiningSites[siteNum].getSpecies() == null) {continue;}
      numOccupiedSites++;
      if (m_DefiningSites[siteNum].getSpecies().getElement() == element) {
        numMatchingSites++;
      }
    }
    return ((double) numMatchingSites) / numOccupiedSites;
  }


  public String getCompositionString(String delimiter) {
	  return this.getCompositionString(delimiter, true);
  }
  
  public String getCompositionString(String delimiter, boolean normalize) {
	  	    
	    Element[] elements = this.getDistinctElements();
	    String[] symbols = new String[elements.length];
	    for (int elementNum = 0; elementNum < elements.length; elementNum++) {
	      symbols[elementNum] = elements[elementNum].getSymbol();
	    }
	    Arrays.sort(symbols); // Alphabetize
	    
	    int[] counts = new int[symbols.length];
	    for (int elementNum = 0; elementNum < symbols.length; elementNum++) {
	      String symbol = symbols[elementNum];
	      counts[elementNum] = this.numDefiningSitesWithElement(Element.getElement(symbol));
	    }
	    
	    int normalizationFactor = normalize ? MSMath.GCF(counts) : 1;
	    String returnString = "";
	    for (int elementNum = 0; elementNum < symbols.length; elementNum++) {
	      String symbol = symbols[elementNum];
	      int count = counts[elementNum] / normalizationFactor;
	      if (elementNum > 0) {
	    	  returnString = returnString + delimiter;
	      }
	      returnString = returnString + symbol + count;
	    }
	    
	    return returnString;
	    
	  }
  
  public String getCompositionString() {
    
	  return this.getCompositionString("");
    
  }
  
  public String toString() {
    return m_Description;
  }
  
  public interface Site {
    public void setSpecies(Species s);
    public Species getSpecies();
    public boolean isEmpty();
    public Structure getStructure();
    public abstract Coordinates getCoords();
    public abstract Atom getAtom();
    public abstract void setAtom(Atom atom);
    public abstract int getIndex();
    public abstract double distanceFrom(Coordinates coords);
    public abstract double distanceFrom(Structure.Site site);
    public int[] getPrimCellOrigin();
    public Site getDefiningSite();
  }
  
  protected abstract class AbstractSite implements Site {

    public void setSpecies(Species s) {
      this.setAtom(s == null ? null : new Atom(s));
    }

    public Species getSpecies() {
      Atom atom = this.getAtom();
      return (atom == null ? null : atom.getSpecies());
    }
    
    public boolean isEmpty() {
      return (this.getAtom() == null);
    }
    
    public Structure getStructure() {
      return Structure.this;
    }
    
    public double distanceFrom(Coordinates coords) {
      return this.getCoords().distanceFrom(coords);
    }
    
    public double distanceFrom(Structure.Site site) {
      return this.distanceFrom(site.getCoords());
    }
    
    public abstract Site getDefiningSite();
    
  }
  
  protected class DefiningSite extends AbstractSite {
    
    private final int m_Index;
    private final Coordinates m_Coords;
    private Atom m_Atom;
    
    protected DefiningSite(DefiningSite site) {
      m_Index = site.m_Index;
      m_Coords = site.m_Coords;
      m_Atom = new Atom(site.m_Atom.getSpecies());
    }
    
    public DefiningSite(Coordinates coords, int index) {
      //m_Coords = Structure.this.getDefiningLattice().softRemainder(coords);
      //m_Coords = Structure.this.getDefiningLattice().hardRemainder(coords);
      m_Coords = coords;
      m_Index = index;
    }
    
    public Site getDefiningSite() {
      return this;
    }
    
    public Coordinates getCoords() {
      return m_Coords;
    }
    
    public int getIndex() {
      return m_Index;
    }

    public Atom getAtom() {
      return m_Atom;
    }

    public void setAtom(Atom atom) {
      m_Atom = atom;
    }
    
    public Site copy() {
      return new DefiningSite(this);
    }
    
    public int[] getPrimCellOrigin() {
      return m_DefiningLattice.hardFloor(this.getCoords());
    }
  }
  
  protected class SiteImage extends AbstractSite {
    
    private final Coordinates m_Coords;
    private final Site m_DefiningSite;
    
    public SiteImage(Coordinates coords, Site site) {
      m_Coords = coords;
      m_DefiningSite = site;
    }
    
    public Coordinates getCoords() {
      return m_Coords;
    }
    
    public int getIndex() {
      return m_DefiningSite.getIndex();
    }

    public Atom getAtom() {
      return m_DefiningSite.getAtom();
    }

    public void setAtom(Atom atom) {
      m_DefiningSite.setAtom(atom);
    }
    
    public Site getDefiningSite() {
      return m_DefiningSite;
    }
    
    public int[] getPrimCellOrigin() {
      int[] returnArray = m_DefiningSite.getPrimCellOrigin();
      Coordinates directCoords = this.getCoords().getCoordinates(getDirectBasis());
      Coordinates baseDirectCoords = m_DefiningSite.getCoords().getCoordinates(getDirectBasis());
      for (int dimNum = 0; dimNum < returnArray.length; dimNum++) {
        returnArray[dimNum] += (int) Math.round(directCoords.coord(dimNum) - baseDirectCoords.coord(dimNum));
      }
      return returnArray;
    }
  }

}
