/*
 * Created on May 28, 2014
 *
 */
package matsci.structure.reciprocal;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeMap;

import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.BravaisLattice;
import matsci.structure.ParallelCell;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.symmetry.SymmetryException;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class KPointLattice {
  
  private SuperLattice m_ReciprocalLattice;
  private SpaceGroup m_SpaceGroup;
  private int m_NumTetPerCell;
  
  private SuperLattice m_RealSpaceSuperLattice;
  
  private int[] m_DistinctKPointMap;
  
  private int[] m_DistinctKPoints;
  private int[] m_DistinctKPointWeights;
  
  private Coordinates[][] m_DistinctTetrahedra;
  private int[] m_DistinctTetrahedraWeights;
  
  private int[][] m_ReducedTetrahedra; // Geometry is thrown out the window -- just the site indices of the corners count
  private int[] m_ReducedTetrahedraWeights;
  
  private int m_NumFailedMaps;

  public KPointLattice(SuperLattice reciprocalLattice, SymmetryOperation[] pointOperations) {
    
    m_ReciprocalLattice = reciprocalLattice;
    
    // Assume that these point operations are consistent with both the super and prim lattices.
    // TODO check to verify this.
    // Get rid of any translations, as they don't work when you go from real to reciprocal space!
    m_SpaceGroup = new SpaceGroup(pointOperations, reciprocalLattice.getPrimLattice());
    m_NumTetPerCell = reciprocalLattice.getOriginCell().getTriangulation().length;

  }
  
  public double[] getShiftArray(BravaisLattice realPrimLattice) {
    
    int[][] superToDirect = this.getSuperToDirect(realPrimLattice);
    SuperLattice superLattice = new SuperLattice(realPrimLattice, superToDirect);
    BravaisLattice reciprocalLattice = superLattice.getInverseLattice();
    return this.getKPointLattice().getOrigin().getCoordArray(reciprocalLattice.getLatticeBasis());

  }
  
  public int[][] getSuperToDirect(BravaisLattice realPrimLattice) {

    SuperLattice superLattice = this.getRealSuperLattice();
    return realPrimLattice.getSuperToDirect(superLattice);

  }
  
  public boolean includesGammaPoint() {
    return m_ReciprocalLattice.getPrimLattice().isLatticePoint(CartesianBasis.getInstance().getOrigin());
  }
  
  public int numTotalKPoints() {
    return m_ReciprocalLattice.numPrimCells();
  }
  
  public int numDistinctKPoints() {
    if (m_DistinctKPoints == null) {
      this.findDistinctKPoints();
    }
    return m_DistinctKPoints.length;
  }
  
  public Coordinates getDistinctKPoint(int index) {
    if (m_DistinctKPoints == null) {
      this.findDistinctKPoints();
    }
    return m_ReciprocalLattice.getPrimCellOriginCoords(m_DistinctKPoints[index]);
  }
  
  public Coordinates[] getDistinctKPointCoords() {
    if (m_DistinctKPoints == null) {
      this.findDistinctKPoints();
    }
    Coordinates[] returnArray = new Coordinates[this.numDistinctKPoints()];
    for (int returnIndex = 0; returnIndex < returnArray.length; returnIndex++) {
      returnArray[returnIndex] = this.getDistinctKPoint(returnIndex);
    }
    return returnArray;
  }
  
  public int getDistinctKPointWeight(int index) {
    if (m_DistinctKPoints == null) {
      this.findDistinctKPoints();
    }
    return m_DistinctKPointWeights[index];
  }
  
  public double[] getDistinctKPointWeights() {
    if (m_DistinctKPoints == null) {
      this.findDistinctKPoints();
    }
    double[] returnArray = new double[this.numDistinctKPoints()];
    for (int returnIndex = 0; returnIndex < returnArray.length; returnIndex++) {
      returnArray[returnIndex] = this.getDistinctKPointWeight(returnIndex);
    }
    return returnArray;
  }
  
  public int numDistinctTetrahedra() {
    if (m_DistinctTetrahedra == null) {
      this.findDistinctTetrahedra();
    }
    return m_DistinctTetrahedra.length;
  }
  
  public Coordinates[] getDistinctTetrahedron(int index) {
    if (m_DistinctTetrahedra == null) {
      this.findDistinctTetrahedra();
    }
    return (Coordinates[]) ArrayUtils.copyArray(m_DistinctTetrahedra[index]);
  }
  
  public int getDistinctTetrahedronWeight(int index) {
    if (m_DistinctTetrahedra == null) {
      this.findDistinctTetrahedra();
    }
    return m_DistinctTetrahedraWeights[index];
  }
  
  public int[] getDistinctTetrahedraWeights() {
    int[] returnArray = new int[this.numDistinctTetrahedra()];
    for (int returnIndex = 0; returnIndex < returnArray.length; returnIndex++) {
      returnArray[returnIndex] = this.getDistinctTetrahedronWeight(returnIndex);
    }
    return returnArray;
  }
  
  public double getTetrahedronVolume() {
    return m_ReciprocalLattice.getPrimLattice().getCellSize() / m_NumTetPerCell;
  }
  
  public int[] getDistinctTetrahedonIndices(int index) {
    if (m_DistinctTetrahedra == null) {
      this.findDistinctTetrahedra();
    }
    if (m_DistinctKPoints == null) {
      this.findDistinctKPoints();
    }
    
    int[] returnArray = new int[m_DistinctTetrahedra[index].length];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      Coordinates coords = m_DistinctTetrahedra[index][siteNum];
      int primCellNum = m_ReciprocalLattice.getNearbyPrimCellOriginNumber(coords);
      returnArray[siteNum] = m_DistinctKPointMap[primCellNum];
    }
    
    return returnArray;
  }
  
  public int[][] getDistinctTetrahedaIndices() {
    int[][] returnArray = new int[this.numDistinctTetrahedra()][];
    for (int tetNum = 0; tetNum < returnArray.length; tetNum++) {
      returnArray[tetNum] = this.getDistinctTetrahedonIndices(tetNum);
    }
    
    return returnArray;
  }
  
  
  public int numReducedTetrahedra() {
    if (m_ReducedTetrahedra == null) {
      this.findReducedTetrahedra();
    }
    return m_ReducedTetrahedra.length;
  }
  
  public int getReducedTetrahedronWeight(int index) {
    if (m_ReducedTetrahedra == null) {
      this.findReducedTetrahedra();
    }
    return m_ReducedTetrahedraWeights[index];
  }
  
  public int[] getReducedTetrahedraWeights() {
    int[] returnArray = new int[this.numReducedTetrahedra()];
    for (int returnIndex = 0; returnIndex < returnArray.length; returnIndex++) {
      returnArray[returnIndex] = this.getReducedTetrahedronWeight(returnIndex);
    }
    return returnArray;
  }
  
  public int[] getReducedTetrahedron(int index) {
    if (m_ReducedTetrahedra == null) {
      this.findReducedTetrahedra();
    }
    
    return ArrayUtils.copyArray(m_ReducedTetrahedra[index]);
  }
  
  public int[][] getReducedTetraheda() {
    int[][] returnArray = new int[this.numReducedTetrahedra()][];
    for (int tetNum = 0; tetNum < returnArray.length; tetNum++) {
      returnArray[tetNum] = this.getReducedTetrahedron(tetNum);
    }
    
    return returnArray;
  }
  
  public BravaisLattice getKPointLattice() {
    return m_ReciprocalLattice.getPrimLattice();
  }
  
  public SuperLattice getReciprocalSpaceLattice() {
    return m_ReciprocalLattice;
  }
  
  public SuperLattice getReciprocalSpaceLattice(BravaisLattice realPrimLattice) {
    
    BravaisLattice reciprocalLattice = realPrimLattice.getInverseLattice();
    reciprocalLattice = reciprocalLattice.translateTo(m_ReciprocalLattice.getOrigin());
    return m_ReciprocalLattice.getPrimLattice().getSuperLattice(reciprocalLattice);
    
  }
  
  public SuperLattice getRealSuperLattice() {
    
    if (m_RealSpaceSuperLattice == null) {
      int[][] superToDirect = m_ReciprocalLattice.getSuperToDirect();
      superToDirect = MSMath.transpose(superToDirect);
      m_RealSpaceSuperLattice = new SuperLattice(m_ReciprocalLattice.getInverseLattice(), superToDirect);
    }
    
    return m_RealSpaceSuperLattice;
      
  }
  
  public SuperLattice getRealSuperLattice(BravaisLattice realPrimLattice) {
    
    SuperLattice superLattice = this.getRealSuperLattice();
    return realPrimLattice.getSuperLattice(superLattice);
    
  }
  
  public SuperLattice getCompactRealSuperLattice() {
    
    SuperLattice superLattice = this.getRealSuperLattice();
    return superLattice.getPrimLattice().getSuperLattice(superLattice.getCompactLattice());
    
  }
  
  public SpaceGroup getSpaceGroup() {
    return m_SpaceGroup;
  }
  
  public double getRealSpaceMinPeriodicDistance() {
    return this.getRealSuperLattice().getMinPeriodicDistance();
  }
  
  public int numTetPerPrimCell() {
    return m_NumTetPerCell;
  }
  
  private void findDistinctKPoints() {

    int numKPoints = m_ReciprocalLattice.numPrimCells();
    int[] weights = new int[numKPoints];
    int[] minMappedKPoints = new int[numKPoints];
    
    SymmetryOperation[] ops = m_SpaceGroup.getOperations();
    // TODO cut back on memory usage by doing this with arrays instead of coordinates.
    int numDistinctKPoints = 0;
    for (int kPointNum = 0; kPointNum < numKPoints; kPointNum++) {
      Coordinates coords = m_ReciprocalLattice.getPrimCellOriginCoords(kPointNum);

      int minMappedKPointNum = Integer.MAX_VALUE;
      /*for (int opNum = 0; opNum < ops.length; opNum++) {
        SymmetryOperation op = ops[opNum];
        //if (!op.isIdentity()) {continue;}
        Coordinates oppedCoords = op.operate(coords);
        if (!m_ReciprocalLattice.getPrimLattice().isLatticePoint(oppedCoords)) {
          throw new RuntimeException("Error calculating weights for k-points.");
        }*/
       for (int opNum = 0; opNum < ops.length; opNum++) {
         SymmetryOperation op = ops[opNum];
         Coordinates oppedCoords = op.operate(coords);
         BravaisLattice primLattice = m_ReciprocalLattice.getPrimLattice();
         if (!primLattice.isLatticePoint(oppedCoords)) {
           m_NumFailedMaps++;
        } else {
          int oppedPointNum = m_ReciprocalLattice.getNearbyPrimCellOriginNumber(oppedCoords);
          minMappedKPointNum = Math.min(minMappedKPointNum, oppedPointNum);
        }
      }
      
      minMappedKPoints[kPointNum] = minMappedKPointNum;
      if (weights[minMappedKPointNum] == 0) {
        numDistinctKPoints++;
      }
      weights[minMappedKPointNum]++;
    }
    
    m_DistinctKPoints = new int[numDistinctKPoints];
    m_DistinctKPointWeights = new int[numDistinctKPoints];
    m_DistinctKPointMap = new int[weights.length];
    
    int returnIndex = 0;
    for (int kPointNum = 0; kPointNum < weights.length; kPointNum++) {
      if (weights[kPointNum] == 0) {continue;}
      m_DistinctKPoints[returnIndex] = kPointNum;
      m_DistinctKPointWeights[returnIndex] = weights[kPointNum];
      m_DistinctKPointMap[kPointNum] = returnIndex;
      returnIndex++;
    }
    
    for (int kPointNum = 0; kPointNum < weights.length; kPointNum++) {
      if (weights[kPointNum] != 0) {continue;}
      m_DistinctKPointMap[kPointNum] = m_DistinctKPointMap[minMappedKPoints[kPointNum]];
    }
    
  }
  
  public double getMapFailureRatio() {
    if (m_DistinctKPoints == null) {
      this.findDistinctKPoints();
    }
    return 1.0 * m_NumFailedMaps / (m_SpaceGroup.numOperations() * this.numTotalKPoints());
  }
  
  private void findDistinctTetrahedra() {
    
    ParallelCell parallelCell = m_ReciprocalLattice.getPrimLattice().getCompactLattice().getOriginCell();
    //ParallelCell parallelCell = m_ReciprocalLattice.getPrimLattice().getReciprocalLattice().getCompactLattice().getReciprocalLattice().getOriginCell();
    Coordinates[][] triangulation = parallelCell.getTriangulation();
    
    Coordinates[][][] subCellTetrahedra = new Coordinates[m_ReciprocalLattice.numPrimCells()][][];
    int[][] subCellWeights = new int[m_ReciprocalLattice.numPrimCells()][m_NumTetPerCell];
    for (int cellNum = 0; cellNum < subCellWeights.length; cellNum++) {
      Arrays.fill(subCellWeights[cellNum], 1);
    }
    SymmetryOperation[] ops = m_SpaceGroup.getOperations();
    
    for (int cellNum = 0; cellNum < subCellWeights.length; cellNum++) {
      Coordinates origin = m_ReciprocalLattice.getPrimCellOriginCoords(cellNum);
      ParallelCell subCell = parallelCell.translateTo(origin);
      Coordinates[] corners = subCell.getCorners();
      int minMappedCellNum = Integer.MAX_VALUE;
      // Uncomment the below code for symmetrized lattices
      /*for (int opNum = 0; opNum < ops.length; opNum++) {
        SymmetryOperation op = ops[opNum];
        Coordinates[] oppedCorners = op.operate(corners);
        for (int cornerNum = 0; cornerNum < oppedCorners.length; cornerNum++) {
          Coordinates oppedCorner = oppedCorners[cornerNum];
          int oppedCornerCellNum = m_ReciprocalLattice.getNearbyPrimCellOriginNumber(oppedCorner);
          if (oppedCornerCellNum > cellNum) {continue;} // Only map to things we've already seen
          
          ParallelCell testCell = parallelCell.translateTo(oppedCorners[cornerNum]);
          Coordinates[] mappedCorners = testCell.getCorners();
          if (!this.areCoordSetsEqual(oppedCorners, mappedCorners)) {continue;}
          
          minMappedCellNum = Math.min(minMappedCellNum, oppedCornerCellNum);
          //break;
        }
      }*/
      
      // No match was found
      if (minMappedCellNum == Integer.MAX_VALUE) {
        Vector vector = new Vector(parallelCell.getOrigin(), subCell.getOrigin());
        Coordinates[][] shiftedTet = (Coordinates[][]) ArrayUtils.copyArray(triangulation);
        for (int tetNum = 0; tetNum < triangulation.length; tetNum++) {
          for (int pointNum = 0; pointNum < triangulation[tetNum].length; pointNum++) {
            shiftedTet[tetNum][pointNum] = shiftedTet[tetNum][pointNum].translateBy(vector);
          }
        }
        //subCellTetrahedra[cellNum] = subCell.getTriangulation();
        subCellTetrahedra[cellNum] = shiftedTet;
        continue;
      }
      
      // The cell maps onto itself
      if (minMappedCellNum == cellNum) {
        int[] tetWeights = subCellWeights[cellNum];
        Coordinates[][] tetrahedra = subCell.getTriangulation();
        for (int tetNum1 = tetrahedra.length - 1; tetNum1 >= 0; tetNum1--) {
          Coordinates[] tet1 = tetrahedra[tetNum1];
          boolean match = false;
          for (int opNum = 0; opNum < m_SpaceGroup.numOperations(); opNum++) {
            SymmetryOperation op = m_SpaceGroup.getOperation(opNum);
            Coordinates[] oppedTet1 = op.operate(tet1);
            for (int tetNum2 = 0; tetNum2 < tetNum1; tetNum2++) {
              Coordinates[] tet2 = tetrahedra[tetNum2];
              if (m_ReciprocalLattice.areTranslationallyEquivalent(oppedTet1, tet2)) {
                tetWeights[tetNum2] += tetWeights[tetNum1];
                tetWeights = ArrayUtils.removeElement(tetWeights, tetNum1);
                tetrahedra = (Coordinates[][]) ArrayUtils.removeElement(tetrahedra, tetNum1);                
                match = true;
                break;
              }
            }
            if (match) {break;}
          }
        }
        subCellTetrahedra[cellNum] = tetrahedra;
        subCellWeights[cellNum] = tetWeights;
      }
      
      /**
       * This updates the cell weights.  We have to consider that some of the 
       * tets within the cell might be symmetrically equivalent
       */
      if (minMappedCellNum < cellNum) {
        subCellWeights[cellNum] = new int[0];
        subCellTetrahedra[cellNum] = new Coordinates[0][];
        int[] tetWeights = subCellWeights[minMappedCellNum];
        int totalWeight = MSMath.arraySum(tetWeights);
        int numCells = totalWeight / m_NumTetPerCell;
        for (int tetNum = 0; tetNum < tetWeights.length; tetNum++) {
          tetWeights[tetNum] += tetWeights[tetNum] / numCells;
        }
      }
      
    }
    
    int numDistinctTet = 0;
    for (int cellNum = 0; cellNum < subCellTetrahedra.length; cellNum++) {
      numDistinctTet += subCellTetrahedra[cellNum].length;
    }
    
    m_DistinctTetrahedra = new Coordinates[numDistinctTet][];
    m_DistinctTetrahedraWeights = new int[numDistinctTet];
    
    int returnIndex = 0;
    for (int cellNum = 0; cellNum < subCellTetrahedra.length; cellNum++) {
      Coordinates[][] tetrahedra = subCellTetrahedra[cellNum];
      int[] weights = subCellWeights[cellNum];
      for (int tetNum = 0; tetNum < tetrahedra.length; tetNum++) {
        m_DistinctTetrahedra[returnIndex] = tetrahedra[tetNum];
        m_DistinctTetrahedraWeights[returnIndex] = weights[tetNum];
        returnIndex++;
      }
    }
    
  }
  
  /**
   * This assumes that the same point is not included more than once in a set
   * 
   * @param set1
   * @param set2
   * @return
   */
  private boolean areCoordSetsEqual(Coordinates[] set1, Coordinates[] set2) {
    
    for (int coordNum1 = 0; coordNum1 < set1.length; coordNum1++) {
      Coordinates coord1 = set1[coordNum1];
      boolean match = false;
      for (int coordNum2 = 0; coordNum2 < set2.length; coordNum2++) {
        if (set2[coordNum2].isCloseEnoughTo(coord1)) {
          match = true;
          break;
        }
      }
      if (!match) {return false;}
    }
    
    return true;
    
  }
  
  private void findReducedTetrahedra() {
    
    int[][] distinctIndices = this.getDistinctTetrahedaIndices();
    int[] distinctWeights = this.getDistinctTetrahedraWeights();
    
    TreeMap<String, Integer> reducedSet = new TreeMap<String, Integer>();
    for (int tetNum = 0; tetNum < distinctIndices.length; tetNum++) {
      Arrays.sort(distinctIndices[tetNum]);
      String key = "";
      for (int vertexNum = 0; vertexNum < distinctIndices[tetNum].length; vertexNum++) {
        key += distinctIndices[tetNum][vertexNum] + "_";
      }
      Integer matchIndex = reducedSet.get(key);
      if (matchIndex == null) {
        reducedSet.put(key, tetNum);
      } else {
        distinctWeights[matchIndex] += distinctWeights[tetNum];
      }
    }
    
    int tetNum = 0;
    m_ReducedTetrahedra = new int[reducedSet.size()][];
    m_ReducedTetrahedraWeights = new int[m_ReducedTetrahedra.length];
    Iterator<Integer> indexIterator = reducedSet.values().iterator();
    while (indexIterator.hasNext()) {
      int index = indexIterator.next();
      m_ReducedTetrahedra[tetNum] = distinctIndices[index];
      m_ReducedTetrahedraWeights[tetNum] = distinctWeights[index];
      tetNum++;
    }
    
    /*
     * This doesn't scale well
     * 
    for (int tetNum1 = distinctIndices.length - 1; tetNum1 >= 0; tetNum1--) {
      int[] tet = distinctIndices[tetNum1];
      Arrays.sort(tet);
      for (int tetNum2 = 0; tetNum2 < tetNum1; tetNum2++) {
        int[] tet2 = distinctIndices[tetNum2];
        Arrays.sort(tet2);
        if (Arrays.equals(tet, tet2)) {
          distinctWeights[tetNum2] += distinctWeights[tetNum1];
          distinctWeights = ArrayUtils.removeElement(distinctWeights, tetNum1);
          distinctIndices = ArrayUtils.removeElement(distinctIndices, tetNum1);
          break;
        }          
      }
    }
    
    m_ReducedTetrahedra = distinctIndices;
    m_ReducedTetrahedraWeights = distinctWeights;*/
  }

}