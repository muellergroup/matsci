package matsci.structure;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;

public interface IStructureData {

  public String getDescription();
  public Vector[] getCellVectors();
  public boolean[] getVectorPeriodicity();
  public int numDefiningSites();
  public Coordinates getSiteCoords(int index);
  public Species getSiteSpecies(int index);

}
