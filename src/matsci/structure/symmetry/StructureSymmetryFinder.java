/*
 * Created on Feb 20, 2005
 *
 */
package matsci.structure.symmetry;

import java.util.Arrays;

import matsci.io.app.log.Status;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.location.symmetry.CoordListMapper;
import matsci.location.symmetry.operations.OperationGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.BravaisLattice;
import matsci.structure.Structure;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;
/**
 * @author Tim Mueller
 *
 */
public class StructureSymmetryFinder {
  
  private static boolean ALLOW_ARTIFICIAL_GROUP_COMPLETION = true;

  private Structure m_Structure;
  
  private Coordinates[] m_ProjectedCoords;
  private Coordinates[] m_DefiningCoords;
  private Coordinates[] m_OffLatticeCoords;
  
  private Vector[] m_OffLatticeVectors;
  private int[] m_OffLatticeBasisIndices;
  
  private LinearBasis m_OffLatticeBasis;
  private BravaisLattice m_LatticeOrthogonalized;
  
  public StructureSymmetryFinder(Structure structure) {
    
    try {
      m_Structure = structure;
      //BravaisLattice lattice = structure.getLattice();
      //BravaisLattice lattice = structure.getSiteLattice();
      m_LatticeOrthogonalized = structure.getSiteLattice().getOrthogonalizedLattice();
      //m_LatticeOrthogonalized = structure.getSiteLattice();
      
      AbstractBasis latticeBasis = m_LatticeOrthogonalized.getLatticeBasis();
      /*m_DefiningCoords = new Coordinates[structure.numDefiningSites()];
      m_ProjectedCoords = new Coordinates[structure.numDefiningSites()];
      m_OffLatticeCoords = new Coordinates[structure.numDefiningSites()];*/
      int numPrimCells = structure.getSuperSiteLattice().numPrimCells();
      //int numPrimCells = (int) Math.round(structure.getDefiningLattice().getCellSize() / lattice.getCellSize());
      m_DefiningCoords = new Coordinates[structure.numDefiningSites() / numPrimCells];
      m_ProjectedCoords = new Coordinates[m_DefiningCoords.length];
      m_OffLatticeCoords = new Coordinates[m_DefiningCoords.length];
      
      int primSiteNum = 0;
      for (int siteNum = 0; siteNum < structure.numDefiningSites(); siteNum++) {
        Coordinates primCoords = structure.getSiteCoords(siteNum).getCoordinates(latticeBasis);
        //if (!lattice.hardRemainder(primCoords).isCloseEnoughTo(primCoords)) {continue;}
        
        boolean match = false;
        for (int prevCoordNum = 0; prevCoordNum < primSiteNum; prevCoordNum++) {
          Coordinates prevCoords = m_DefiningCoords[prevCoordNum];
          match = m_LatticeOrthogonalized.areTranslationallyEquivalent(primCoords, prevCoords);
          if (match) {break;}
        }
        if (match) {continue;}
        
        Coordinates cellCoords = m_LatticeOrthogonalized.hardRemainder(primCoords);
        
        //m_DefiningCoords[primSiteNum] = primCoords;
        m_DefiningCoords[primSiteNum] = cellCoords;
        m_ProjectedCoords[primSiteNum] = m_LatticeOrthogonalized.projectToLattice(m_DefiningCoords[primSiteNum]);
        m_OffLatticeCoords[primSiteNum] = m_LatticeOrthogonalized.removeLattice(m_DefiningCoords[primSiteNum]);
        //m_DefiningCoords[siteNum] = structure.getSiteCoords(siteNum).getCoordinates(latticeBasis);
        //m_ProjectedCoords[siteNum] = lattice.projectToLattice(m_DefiningCoords[siteNum]);
        //m_OffLatticeCoords[siteNum] = lattice.removeLattice(m_DefiningCoords[siteNum]);
        primSiteNum++;
      }
      
      this.findOffLatticeVectors();
      this.findOffLatticeBasis();
    } catch (Exception e) {
      throw new SymmetryException(e);
    }
  }

  public SymmetryOperation[] getSiteFactorOperations() {
    
    //BravaisLattice lattice = m_Structure.getLattice();    
    //BravaisLattice lattice = m_Structure.getSiteLattice();
    
    SymmetryOperation[] returnArray = new SymmetryOperation[0];
    AbstractLinearBasis latticeBasis = m_LatticeOrthogonalized.getLatticeBasis();
    
    OperationGroup onLatticeOps = m_LatticeOrthogonalized.getPointGroup();
    int[][] possibleMaps = this.findPossibleMaps();
    SymmetryOperation[] offLatticeOps = this.findOffLatticeOps(possibleMaps);

    // The general algorithm is to cycle through all point operations and apply them to the sites.
    // If we can find a vector that translates the operated sites
    
    Coordinates[] latticeBasisCoords = new Coordinates[3];
    for (int coordNum = 0; coordNum < latticeBasisCoords.length; coordNum++) {
      double[] coordArray = new double[3];
      coordArray[coordNum] = 1;
      latticeBasisCoords[coordNum] = new Coordinates(coordArray, m_LatticeOrthogonalized.getLatticeBasis());
    }

    Coordinates[] latticeOperatedCoords = new Coordinates[latticeBasisCoords.length];
    for (int onLatticeOpNum = 0; onLatticeOpNum < onLatticeOps.numOperations(); onLatticeOpNum++) {
      SymmetryOperation onLatticeOp = onLatticeOps.getOperation(onLatticeOpNum);
      for (int coordNum = 0; coordNum < latticeOperatedCoords.length; coordNum++) {
        latticeOperatedCoords[coordNum] = onLatticeOp.operate(latticeBasisCoords[coordNum]);
      }
      
      for (int offLatticeOpNum = 0; offLatticeOpNum < offLatticeOps.length; offLatticeOpNum++) {
        SymmetryOperation offLatticeOp = offLatticeOps[offLatticeOpNum];
        double[][] linearOpArray = new double[latticeBasisCoords.length][];
        for (int coordNum = 0; coordNum < linearOpArray.length; coordNum++) {
          Coordinates offLatticeOperatedCoords = offLatticeOp.operate(latticeOperatedCoords[coordNum]);
          linearOpArray[coordNum] = offLatticeOperatedCoords.getCoordArray(latticeBasis);
        }
        
        SymmetryOperation combinedOp = new SymmetryOperation(linearOpArray, latticeBasis);
        Coordinates firstCoords = m_DefiningCoords[0];
        Coordinates pointOppedTail = combinedOp.operate(firstCoords);
        Vector[] offsets = new Vector[m_DefiningCoords.length];
       
        // Now we try to find translations that might help us
        for (int transNum = 0; transNum < m_DefiningCoords.length; transNum++) {
 
          Vector translation = new Vector(pointOppedTail, m_DefiningCoords[transNum]);
          // TODO consider whether we should ensure translation is on lattice.  E.g.:
          /*if (m_LatticeOrthogonalized.projectToLattice(translation).subtract(translation).length() > CartesianBasis.getPrecision() * 2) {
            continue;
          }*/
          
          Vector averageOffset = Vector.getZero3DVector();
          boolean match = true;
          for (int siteNum = 0; siteNum < m_DefiningCoords.length; siteNum++) {
            Coordinates origCoords = m_DefiningCoords[siteNum];
            Coordinates operatedCoords = combinedOp.operate(origCoords).getCoordinates(latticeBasis);
            operatedCoords = operatedCoords.translateBy(translation);
            Structure.Site nearbySite = m_Structure.getSite(operatedCoords, CartesianBasis.getPrecision());
            if (nearbySite == null) {
              match = false;
              break;
            }
            
            // This was slow, as it uses a Wigner-Seitz search instead of a parallel cell search
            /*Structure.Site nearbySite = m_Structure.getNearbySite(operatedCoords);
            double distance = nearbySite.distanceFrom(operatedCoords);
            if (distance > (2 * CartesianBasis.getPrecision())) {
            //if (distance > (CartesianBasis.getPrecision())) {
                   match = false;
                   break;
            }*/
            offsets[siteNum] = new Vector(operatedCoords, nearbySite.getCoords());
            averageOffset = averageOffset.add(offsets[siteNum]);
            //if (m_Structure.getDefiningSite(operatedCoords) == null) {match = false;}
            //if (!match) {break;}
          }
          if (!match) {continue;}
          
          // Using the average is not ideal, but it's easy and better than nothing
          averageOffset = averageOffset.multiply(1.0 / m_DefiningCoords.length);
          for (int offsetNum = 0; offsetNum < offsets.length; offsetNum++) {
            double length = offsets[offsetNum].subtract(averageOffset).length();
            if (length > CartesianBasis.getPrecision()) {
              match = false;
              break;
            }
          }
          translation = translation.add(averageOffset);
          if (!match) {continue;}
          SymmetryOperation newOp = new SymmetryOperation(linearOpArray, translation, latticeBasis);
          boolean isOpNew = true;
          for (int oldOpNum = 0; oldOpNum < returnArray.length; oldOpNum++) {
            isOpNew = !newOp.isEquivalentTo(returnArray[oldOpNum]);
            if (!isOpNew) {break;}
          }
          if (isOpNew) {
            returnArray = (SymmetryOperation[]) ArrayUtils.appendElement(returnArray, newOp);
          }
        }
      }
    }
    
    int numOperations = returnArray.length;
    returnArray = OperationGroup.completeGroup(returnArray, m_LatticeOrthogonalized);
    if (returnArray.length != numOperations) {
      if (ALLOW_ARTIFICIAL_GROUP_COMPLETION) {
        Status.warning("Did not find all site factor operations within given tolerance.  Symmetry group was artificially completed.");
      } else {
        throw new SymmetryException("Did not find all site factor operations within given tolerance.  Symmetry group was not complete.");
      }
    }
    
    // Code to test operations
    /*Vector[] cellVectors = m_LatticeOrthogonalized.getCellVectors();
    for (int opNum = 0; opNum < returnArray.length; opNum++) {
      SymmetryOperation operation = returnArray[opNum];
      Vector[] oppedVectors = new Vector[] {
          operation.operate(cellVectors[0]),
          operation.operate(cellVectors[1]),
          operation.operate(cellVectors[2]),
      };
      BravaisLattice newLattice = new BravaisLattice(oppedVectors);
      int[][] superToDirect = m_LatticeOrthogonalized.getSuperToDirect(newLattice);
      System.out.println(MSMath.determinant(superToDirect));
      System.out.println(superToDirect[0][0] + ", " + superToDirect[0][1] + ", " + superToDirect[0][2]);
      System.out.println(superToDirect[1][0] + ", " + superToDirect[1][1] + ", " + superToDirect[1][2]);
      System.out.println(superToDirect[2][0] + ", " + superToDirect[2][1] + ", " + superToDirect[2][2]);
      System.out.println();
    }*/
    
    return returnArray;
  }
  
  private int[][] findPossibleMaps() {
    
    // We remove the lattice dimensions from the coordinates and see how many ways we can 
    // map the remaining vectors onto each other
    //BravaisLattice lattice = m_Structure.getLattice();
    //BravaisLattice lattice = m_Structure.getSiteLattice();
    
    CoordListMapper offLatticeMapper = new CoordListMapper(m_LatticeOrthogonalized.getOrigin(), m_OffLatticeCoords);
    offLatticeMapper.fixOrigin(false);
    /*Coordinates[] nonLatticeCoords = new Coordinates[m_DefiningCoords.length];
    for (int siteNum = 0; siteNum < m_DefiningCoords.length; siteNum++) {
      nonLatticeCoords[siteNum] = lattice.removeLattice(m_DefiningCoords[siteNum]);
    }*/
    return offLatticeMapper.mapCoordinates(m_OffLatticeCoords, false);
    
  }
  
  private SymmetryOperation[] findOffLatticeOps(int[][] maps) {
    
    SymmetryOperation[] returnArray = new SymmetryOperation[maps.length];
    
    for (int mapNum = 0; mapNum < maps.length; mapNum++) {
      int[] map = maps[mapNum];
      double[][] transformationArray = new double[][] {{1,0,0},{0,1,0},{0,0,1}};
      for (int vecNum = 0; vecNum < m_OffLatticeBasisIndices.length; vecNum++) {
        int mappedIndex = map[m_OffLatticeBasisIndices[vecNum]];
        //Coordinates mappedCoords = m_OffLatticeCoords[mappedIndex];
        //Coordinates mappedCoords = m_OffLatticeVectors[mappedIndex].getDirection();
        //transformationArray[vecNum] = mappedCoords.getCoordArray(m_OffLatticeBasis);
        //transformationArray[vecNum] = mappedCoords.getCoordArray(m_OffLatticeBasis);
        transformationArray[vecNum] = m_OffLatticeVectors[mappedIndex].getDirectionArray(m_OffLatticeBasis);
      }
      //returnArray[mapNum] = new LinearPointOp(transformationArray, m_OffLatticeBasis);
      returnArray[mapNum] = new SymmetryOperation(transformationArray, m_OffLatticeBasis);
    }
    
    int numOperations = returnArray.length;
    returnArray = OperationGroup.completeGroup(returnArray); // TODO Test this.
    if (returnArray.length != numOperations) {
      Status.warning("Did not find all off-lattice operations within given tolerance.  Symmetry group was artificially completed.");
    }
    
    /*returnArray = OperationGroup.trimGroup(returnArray); // TODO Test this.
    if (returnArray.length != numOperations) {
      Status.warning("Did not find all off-lattice operations within given tolerance.  Symmetry group was artificially trimmed.");
    }*/

    return returnArray;
  }
  
  private void findOffLatticeVectors() {
    
    m_OffLatticeVectors = new Vector[m_OffLatticeCoords.length];
    
    //BravaisLattice lattice = m_Structure.getLattice();
    //BravaisLattice lattice = m_Structure.getSiteLattice();
    AbstractBasis latticeBasis = m_LatticeOrthogonalized.getLatticeBasis();
    
    // Find the center of mass of the sites
    double[] centerArray = new double[3];
    for (int siteNum = 0; siteNum < m_OffLatticeCoords.length; siteNum++) {
      Coordinates definingCoords = m_DefiningCoords[siteNum].getCoordinates(latticeBasis);
      for (int dimNum = 0; dimNum < definingCoords.numCoords(); dimNum++) {
        centerArray[dimNum] += definingCoords.coord(dimNum);
      }
    }
    
    for (int dimNum = 0; dimNum < centerArray.length; dimNum++) {
      centerArray[dimNum] /= m_OffLatticeCoords.length;
    }
    
    Coordinates center = new Coordinates(centerArray, latticeBasis);
    Coordinates projectedCenter = m_LatticeOrthogonalized.projectToLattice(center);
    Vector latticeShift = new Vector(projectedCenter, center);
    
    for (int siteNum = 0; siteNum < m_ProjectedCoords.length; siteNum++) {
      Coordinates newTail = m_ProjectedCoords[siteNum].translateBy(latticeShift).getCoordinates(latticeBasis);
      m_OffLatticeVectors[siteNum] = new Vector(newTail, m_DefiningCoords[siteNum]);
    }
    
  }
  
  // TODO try to get rid of this
  private void findOffLatticeBasis() {
    
    m_OffLatticeBasisIndices = new int[0];
    Vector[] basisVectors = new Vector[0];
    LinearBasis tempBasis = new LinearBasis(LinearBasis.fill3DBasis(basisVectors));
    
    //BravaisLattice lattice = m_Structure.getLattice();
    //BravaisLattice lattice = m_Structure.getSiteLattice();
    
    // Find a set of linearly independent off-lattice vectors
    for (int vecNum = 0; vecNum < m_OffLatticeVectors.length; vecNum++) {

      Vector currVector = m_OffLatticeVectors[vecNum];
      double[] vectorCoordArray = currVector.getDirectionArray(tempBasis);
      for (int coordNum = 0; coordNum < basisVectors.length; coordNum++) {
        vectorCoordArray[coordNum] = 0;
      }
      
      Coordinates collapsedCoords = new Coordinates(vectorCoordArray, tempBasis);
      if (!collapsedCoords.isCloseEnoughTo(CartesianBasis.getInstance().getOrigin())) {
        m_OffLatticeBasisIndices = ArrayUtils.appendElement(m_OffLatticeBasisIndices, vecNum);
        basisVectors = (Vector[]) ArrayUtils.appendElement(basisVectors, currVector);
        tempBasis = new LinearBasis(LinearBasis.fill3DBasis(basisVectors));
      }
      // Added this in 2016.  It should speed things up a bit.
      if (basisVectors.length == 3) {break;}
    }
    
    // Create the full 3-D basis
    // TODO Consider just using the temp basis
    Vector[] givenVectors = new Vector[basisVectors.length + m_LatticeOrthogonalized.numPeriodicVectors()];
    System.arraycopy(basisVectors, 0, givenVectors, 0, basisVectors.length);
    for (int latticeVecNum = 0; latticeVecNum < m_LatticeOrthogonalized.numPeriodicVectors(); latticeVecNum++) {
      givenVectors[basisVectors.length + latticeVecNum] = m_LatticeOrthogonalized.getPeriodicVector(latticeVecNum);
    }
    
    m_OffLatticeBasis = new LinearBasis(LinearBasis.fill3DBasis(givenVectors));
    
  }
  
  public static void allowArtificialGroupCompletion(boolean value) {
    ALLOW_ARTIFICIAL_GROUP_COMPLETION = value;
  }
  
  public static boolean allowsArtificialGroupCompletion() {
    return ALLOW_ARTIFICIAL_GROUP_COMPLETION;
  }
}
