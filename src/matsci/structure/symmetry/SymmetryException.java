/*
 * Created on Apr 12, 2017
 *
 */
package matsci.structure.symmetry;

public class SymmetryException extends RuntimeException {

  public SymmetryException() {
  }

  public SymmetryException(String arg0) {
    super(arg0);
  }

  public SymmetryException(Throwable cause) {
    super(cause);
  }

  public SymmetryException(String message, Throwable cause) {
    super(message, cause);
  }

  public SymmetryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

}
