/*
 * Created on Feb 19, 2005
 *
 */
package matsci.structure.symmetry;

import matsci.io.app.log.Status;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.CoordSetMapper;
import matsci.location.symmetry.operations.OperationGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.BravaisLattice;
import matsci.util.arrays.ArrayIndexer;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class LatticeSymmetryFinder {
  
  private static boolean ALLOW_ARTIFICIAL_GROUP_COMPLETION = true;

  private BravaisLattice m_Lattice;
  private CoordSetMapper m_LatticeMapper;
  private int[] m_HalfDimensions;

  public LatticeSymmetryFinder(BravaisLattice lattice) {
    
    lattice = lattice.getCompactLattice(); // Use this to get around problems caused by skewed lattices.  Alternatively, increase the half dimensions of the grid.
    Coordinates[] latticePoints = new Coordinates[lattice.numPeriodicVectors()];
    for (int pointNum = 0; pointNum < latticePoints.length; pointNum++) {
      latticePoints[pointNum] = lattice.getPeriodicVector(pointNum).getHead();
    }
    
    m_LatticeMapper = new CoordSetMapper(lattice.getOrigin(), latticePoints);
    m_LatticeMapper.fixOrigin(true);
    m_Lattice = lattice;
    //this.setHalfDimensions(5); // This is an arbitrary default setting.
    this.setHalfDimensions(3); // This is an arbitrary default setting.  It should be sufficient.
  }

  public void setHalfDimensions(int[] halfDimensions) {
    if (halfDimensions.length != m_Lattice.numPeriodicVectors()) {
      throw new RuntimeException("Grid template for finding Bravais Lattice symmetry operations must have same dimensionality as the lattice");
    }
    m_HalfDimensions = halfDimensions;
  }
  
  public void setHalfDimensions(int halfDimensions) {
    m_HalfDimensions = new int[m_Lattice.numPeriodicVectors()];
    for (int dimNum = 0; dimNum < m_HalfDimensions.length; dimNum++) {
      m_HalfDimensions[dimNum] = halfDimensions;
    }
  }
  
  public int[] getHalfDimensions() {
    return ArrayUtils.copyArray(m_HalfDimensions);
  }
  
  public SymmetryOperation[] getPointOperations() {
    
    // TODO Make this work for shifted lattices!  It can get stuck
    // in an endless loop when completing the group.  We might need to updated completeGroup
    Coordinates[] grid = m_Lattice.generateGrid(m_HalfDimensions);
    int[][] maps = m_LatticeMapper.mapCoordinates(grid, false);
    
    ArrayIndexer pointIndexer = new ArrayIndexer(this.getGridDimensions());
    SymmetryOperation[] returnArray = new SymmetryOperation[maps.length];
    int[] periodicIndices = m_Lattice.getPeriodicIndices();
    for (int mapNum = 0; mapNum < returnArray.length; mapNum++) {
      
      int[] currMap = maps[mapNum];
      double[][] operation = new double[][] {{1,0,0}, {0,1,0}, {0,0,1}};
      for (int pointNum = 0; pointNum < currMap.length; pointNum++) {
        int[] gridLocation = pointIndexer.splitValue(currMap[pointNum]);
        for (int dimNum = 0; dimNum < gridLocation.length; dimNum++) {
          operation[periodicIndices[pointNum]][periodicIndices[dimNum]] = gridLocation[dimNum] - m_HalfDimensions[dimNum];
        }
      }
      SymmetryOperation newOperation = new SymmetryOperation(operation, m_Lattice.getLatticeBasis());
      returnArray[mapNum] = newOperation;
    }
    
    int numOperations = returnArray.length;
    
    returnArray = OperationGroup.completeGroup(returnArray, m_Lattice);
    if (returnArray.length != numOperations) {
      if (ALLOW_ARTIFICIAL_GROUP_COMPLETION) {
        Status.warning("Did not find all lattice point operations within given tolerance.  Symmetry group was artificially completed.");
      } else {
        throw new SymmetryException("Did not find all lattice point operations within given tolerance.  Symmetry group was not complete.");
      }
    }
    
    /*returnArray = OperationGroup.trimGroup(returnArray, m_Lattice);
    if (returnArray.length != numOperations) {
      Status.warning("Did not find all lattice point operations within given tolerance.  Symmetry group was artificially trimmed.");
    }*/
    
    /*for (int opNum = returnArray.length -1 ; opNum >= 0; opNum--) {
      if (!isOperationValid(returnArray[opNum])) {
        System.out.println("invalid!");
        returnArray = (SymmetryOperation[]) ArrayUtils.removeElement(returnArray, opNum);
      }
    }*/
    return returnArray;
  }
  
  /*private boolean isOperationValid(SymmetryOperation operation) {
    Vector[] compactVectors = m_Lattice.getCompactLattice().getCellVectors();
    for (int vecNum = 0; vecNum < compactVectors.length; vecNum++) {
      Vector vector = compactVectors[vecNum];
      Vector oppedVector = vector;
      for (int op = 0; op < 6; op++) {
        oppedVector = operation.operate(oppedVector);
        double distance = vector.subtract(oppedVector).length();
        if (distance < CartesianBasis.getPrecision() * 12) { // We're back home
          if (distance > CartesianBasis.getPrecision()) {
            return false;
          }
          break;
        }
      }
    }
    return true;
  }*/

  public int[] getGridDimensions() {
    
    int[] returnArray = new int[m_HalfDimensions.length];
    for (int dimNum = 0; dimNum < returnArray.length; dimNum++) {
      returnArray[dimNum] = m_HalfDimensions[dimNum] * 2 + 1;
    }
    return returnArray;
    
  }

  public static void allowArtificialGroupCompletion(boolean value) {
    ALLOW_ARTIFICIAL_GROUP_COMPLETION = value;
  }
  
  public static boolean allowsArtificialGroupCompletion() {
    return ALLOW_ARTIFICIAL_GROUP_COMPLETION;
  }

}
