/*
 * Created on Oct 30, 2009
 *
 */
package matsci.structure.symmetry;

import java.util.Arrays;

import org.ejml.data.DenseMatrix64F;
import org.ejml.simple.SimpleEVD;
import org.ejml.simple.SimpleMatrix;

import matsci.*;
import matsci.exceptions.TimeOutException;
import matsci.io.app.log.Status;
import matsci.io.vasp.POSCAR;
import matsci.location.*;
import matsci.location.basis.*;
import matsci.location.symmetry.operations.*;
import matsci.structure.*;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.superstructure.SuperLatticeGenerator;
import matsci.structure.superstructure.SuperStructure;
import matsci.util.*;
import matsci.util.arrays.*;

public class StructureMapper {
  
  private Structure m_SourceStructure;
  private Structure m_TargetStructure;
  
  private SymmetryOperation m_TransformationOperation = null;
  private int[] m_SiteMap = null;
  private double m_MappedRMSError = Double.NaN;
  private double m_MappedMaxOffsetDiff = Double.NaN;
  private double m_MappedSkewFactor = Double.NaN;
  private double m_MappedVolumeScale = Double.NaN;
  private int[][] m_SourceSuperToDirect = null;
  private int[][] m_TargetSuperToDirect = null;
  
  private long m_TimeOut = 300000;
  private boolean m_FindBest = false;
  private boolean m_AllowSuperCells = true;
  private boolean m_MatchElementsOnly = true; // Match just elements or match species (including oxidation states)
  
  private boolean m_MinimizeRMSError = false; // Alternative is to minimize offset difference
  private double m_MaxAllowedRMSError = 0.3;
  private double m_MaxAllowedOffsetDifference = 0.5;
  
  private double m_MaxAllowedSkewRMSError = 0.25;
  private double m_MinAllowedVolumeFactor = 0.7;
  
  // To scale the cutoffs with the target size
  private double m_MinTargetNNDist = -1; // Lazy load
  
  private int m_MaxAllowedSuperCellSize = 48;
  private int m_MaxAllowedSuperCellAtoms = 256;
  
  private boolean m_TriedToFindMap = false;

  public StructureMapper(Structure sourceStructure, Structure targetStructure) {
    
    this(sourceStructure, targetStructure, false);
    
  }
  
  public StructureMapper(Structure sourceStructure, Structure targetStructure, boolean findBest) {
    
    m_SourceStructure = sourceStructure;
    m_TargetStructure = targetStructure;
    m_FindBest = findBest;
    
  }
  
  protected double getMinTargetNNDist() {
    if (m_MinTargetNNDist < 0) {
      m_MinTargetNNDist = Double.POSITIVE_INFINITY;
      for (int siteNum = 0; siteNum < m_TargetStructure.numDefiningSites(); siteNum++) {
        Structure.Site site = m_TargetStructure.getDefiningSite(siteNum);
        Structure.Site[] neighbors = m_TargetStructure.getNearestNeighbors(site);
        for (int neighborNum = 0; neighborNum < neighbors.length; neighborNum++) {
          m_MinTargetNNDist = Math.min(m_MinTargetNNDist, site.distanceFrom(neighbors[neighborNum]));
        }
      }
    }
    
    return m_MinTargetNNDist;
  }
  
  public double getMaxAllowedSkewRMSError() {
    return m_MaxAllowedSkewRMSError;
  }
  
  public void setMaxAllowedSkewRMSError(double value) {
    m_MaxAllowedSkewRMSError = value;
  }

  public double getMinAllowedVolumeFactor() {
    return m_MinAllowedVolumeFactor;
  }
  
  public void setMinAllowedVolumeFactor(double value) {
    m_MinAllowedVolumeFactor = value;
  }
  
  public void setMaxAllowedRMSError(double value) {
    m_MaxAllowedRMSError = value;
  }
  
  public void setMaxAllowedOffsetDifference(double value) {
    m_MaxAllowedOffsetDifference = value;
  }
  
  public void setMaxAllowedRMSErrorScale(double value) {
    m_MaxAllowedRMSError = getMinTargetNNDist() * value;
  }
  
  public void setMaxAllowedOffsetDifferenceScale(double value) {
    m_MaxAllowedOffsetDifference = getMinTargetNNDist() * value;
  }
 
  public boolean isRMSMinimized() {
    return m_MinimizeRMSError;
  }
  
  public void minimizeRMSError(boolean value) {
    m_MinimizeRMSError = value;
  }
  
  /**
   * 
   * @return A map in which the indices are the source super structure sites, and the elements are target sites.
   */
  public int[] getSiteMap() {
    this.findMap();
    if (m_SiteMap == null) {return null;}
    return ArrayUtils.copyArray(m_SiteMap);
  }
  
  public double getMappedRMSErrorScale() {
    this.findMap();
    return m_MappedRMSError / this.getMinTargetNNDist();
  }
  
  public double getMappedRMSError() {
    this.findMap();
    return m_MappedRMSError;
  }
  
  public double getMappedMaxOffsetDiffScale() {
    this.findMap();
    return m_MappedMaxOffsetDiff / this.getMinTargetNNDist();
  }
  
  public double getMappedMaxOffsetDiff() {
    this.findMap();
    return m_MappedMaxOffsetDiff;
  }
  
  public double getMappedSkewFactor() {
    this.findMap();
    return m_MappedSkewFactor;
  }
  
  public double getMappedVolumeScale() {
    this.findMap();
    return m_MappedVolumeScale;
  }
  
  public int[][] getSourceSuperToDirect() {
    this.findMap();
    return ArrayUtils.copyArray(m_SourceSuperToDirect);
  }
  
  public int[][] getTargetSuperToDirect() {
    this.findMap();
    return ArrayUtils.copyArray(m_TargetSuperToDirect);
  }
  
  public void setTimeOut(long value) {
    m_TimeOut = value;
  }
  
  public double getTimeOut() {
    return m_TimeOut;
  }
  
  public void setMaxAllowedSuperCellSize(int value) {
    m_MaxAllowedSuperCellSize = value;
  }
  
  public int getMaxAllowedSuperCellSize() {
    return m_MaxAllowedSuperCellSize;
  }
  

  public void setMaxAllowedSuperCellAtoms(int value) {
    m_MaxAllowedSuperCellAtoms = value;
  }
  
  public int getMaxAllowedSuperCellAtoms() {
    return m_MaxAllowedSuperCellAtoms;
  }
  
  
  public void allowSuperCells(boolean value) {
    m_AllowSuperCells = value;
  }
  
  public boolean allowsSuperCells() {
    return m_AllowSuperCells;
  }
  
  public void matchElementsOnly(boolean value) {
    m_MatchElementsOnly = value;
  }
  
  public boolean matchesElementsOnly() {
    return m_MatchElementsOnly;
  }
  
  public double getMaxAllowedOffsetDifference() {
    return m_MaxAllowedOffsetDifference;
  }
  
  public double getMaxAllowedRMSError() {
    return m_MaxAllowedRMSError;
  }
  
  public double getMaxAllowedOffsetDifferenceScale() {
    return m_MaxAllowedOffsetDifference / this.getMinTargetNNDist();
  }
  
  public double getMaxAllowedRMSErrorScale() {
    return m_MaxAllowedRMSError / this.getMinTargetNNDist();
  }
  
  protected boolean isMapPossible() {
    
    if (m_SourceStructure.numPeriodicDimensions() != m_TargetStructure.numPeriodicDimensions()) {
      Status.detail("Cannot map structure to a structure with a different number of periodic dimensions.");
      return false;
    }
    
    if (!m_AllowSuperCells && (m_SourceStructure.numDefiningSites() != m_TargetStructure.numDefiningSites())) {
      Status.detail("Cannot map structure because primitive cells have different number of defining sites and super cells are not allowed.");
      return false;
    }
    
    BravaisLattice sourceLattice = m_SourceStructure.getDefiningLattice();
    BravaisLattice targetLattice = m_TargetStructure.getDefiningLattice();

    if (sourceLattice.numNonPeriodicVectors() != targetLattice.numNonPeriodicVectors()) {
      Status.detail("Cannot map structure to a structure with a different number of non-periodic dimensions.");
      return false;
    }
    
    if ((sourceLattice.numNonPeriodicVectors() > 0) || (targetLattice.numNonPeriodicVectors() > 0)) {
      Status.warning("In non-periodic dimensions mapper can only find identity operation!");
      //return false;
    }
    
    int lcm = MSMath.LCM(new int[] {m_SourceStructure.numDefiningSites(), m_TargetStructure.numDefiningSites()});    
    int numSourcePrimCells = lcm / m_SourceStructure.numDefiningSites();
    int numTargetPrimCells = lcm / m_TargetStructure.numDefiningSites();
    
    if (m_MatchElementsOnly) {
      Element[] sourceElements = m_SourceStructure.getDistinctElements();
      Element[] targetElements = m_TargetStructure.getDistinctElements();
      if (sourceElements.length != targetElements.length) {
        Status.detail("No map possible between structures with different numbers of elements.");
        return false;
      }
      for (int specNum = 0; specNum < sourceElements.length; specNum++) {
        int numSourceOccurences = m_SourceStructure.numDefiningSitesWithElement(sourceElements[specNum]);
        int numTargetOccurences = m_TargetStructure.numDefiningSitesWithElement(sourceElements[specNum]);
        if (numSourceOccurences * numSourcePrimCells != numTargetOccurences * numTargetPrimCells) {
          Status.detail("No map possible between structures different compositions.");
          return false;
        }
      }
    } else {
      Species[] sourceSpecies = m_SourceStructure.getDistinctSpecies();
      Species[] targetSpecies = m_TargetStructure.getDistinctSpecies();
      if (sourceSpecies.length != targetSpecies.length) {
        Status.detail("No map possible between structures with different numbers of species.");
        return false;
      }
      for (int specNum = 0; specNum < sourceSpecies.length; specNum++) {
        int numSourceOccurences = m_SourceStructure.numDefiningSitesWithSpecies(sourceSpecies[specNum]);
        int numTargetOccurences = m_TargetStructure.numDefiningSitesWithSpecies(sourceSpecies[specNum]);
        if (numSourceOccurences * numSourcePrimCells != numTargetOccurences * numTargetPrimCells) {
          Status.detail("No map possible between structures different compositions.");
          return false;
        }
      }
    }
    
    double volumeScale = this.getVolumeScale();
    if (volumeScale < m_MinAllowedVolumeFactor) {
      Status.detail("Min allowed volume scale of " + m_MinAllowedVolumeFactor + " violated by volume scale of " + volumeScale + ".");
      return false;
    }
    
    return true;
  }
  
  public boolean mapExists() {
    this.findMap();
    return (m_TransformationOperation != null);
  }
  
  public SymmetryOperation getTransformation() {    
    this.findMap();
    return m_TransformationOperation;
  }
  
  public Structure getTransformedSource() {    
    this.findMap();
    if (m_TransformationOperation == null) {return null;}
    Structure superSource = new SuperStructure(m_SourceStructure, m_SourceSuperToDirect);
    return superSource.operate(m_TransformationOperation);
  }
  
  public Structure getSuperTarget() {    
    this.findMap();
    if (m_TransformationOperation == null) {return null;}
    return new SuperStructure(m_TargetStructure, m_TargetSuperToDirect);
  }
  
  protected int[][][] getShiftMatrices(Structure sourceSuper, Structure targetSuper) {
    
    int numPeriodicDimensions = sourceSuper.numPeriodicDimensions();
    
    int[] arraySizes = new int[numPeriodicDimensions * numPeriodicDimensions];
    Arrays.fill(arraySizes, 3);
    
    double[][] sourceC2D = sourceSuper.getDirectBasis().getCartesianToBasis();
    double[][] targetD2C = targetSuper.getDirectBasis().getBasisToCartesian();
    
    int[] periodicSourceIndices = sourceSuper.getDefiningLattice().getPeriodicIndices();
    int[] periodicTargetIndices = sourceSuper.getDefiningLattice().getPeriodicIndices();

    sourceC2D = ArrayUtils.selectColumns(sourceC2D, periodicSourceIndices);
    targetD2C = ArrayUtils.selectRows(targetD2C, periodicTargetIndices);

    // Volume effects are dealt with separately
    double volumeScale = sourceSuper.getDefiningLattice().getCellSize() / targetSuper.getDefiningLattice().getCellSize();
    if (numPeriodicDimensions > 0) {
      targetD2C = MSMath.matrixTimesScalar(targetD2C, Math.pow(volumeScale, 1.0 / numPeriodicDimensions));
    }
   
    ArrayIndexer indexer = new ArrayIndexer(arraySizes);
    int[] currentState = indexer.getInitialState();
    int[][] superToDirect = new int[numPeriodicDimensions][numPeriodicDimensions];

    //int[][] superToDirect = new int[sourceC2D.length][sourceC2D.length];
    int[][][] returnArray = new int[indexer.numAllowedStates()][][];
    int returnIndex = 0;
    double minNorm = Double.POSITIVE_INFINITY;
    do {

      //superToDirect = ArrayUtils.identityMatrix(superToDirect.length);
      
      /*int periodicRow = 0;
      int periodicCol = 0;
      for (int row = 0; row < superToDirect.length; row++) {
        if (!sourceSuper.getDefiningLattice().isDimensionPeriodic(row)) {continue;}
        for (int col = 0; col < superToDirect.length; col++) {
          if (!sourceSuper.getDefiningLattice().isDimensionPeriodic(col)) {continue;}
          int currentStateIndex = periodicRow * numPeriodicDimensions + periodicCol;
          superToDirect[row][col] = currentState[currentStateIndex];
          periodicCol++;
        }
        periodicRow++;
      }*/
      
      for (int i = 0; i < currentState.length; i++) {
        int row = i / numPeriodicDimensions;
        int col = i % numPeriodicDimensions;
        superToDirect[row][col] = currentState[i] - 1;
      }
      
      int determinant = MSMath.determinant(superToDirect);
      if (determinant * determinant != 1) {continue;}

      //double rotDet = MSMath.determinant(rotationCheck);
      /*double[][] invIsometryCheck = MSMath.simpleInverse(isometryCheck);
      double norm = 0;
      double invNorm = 0;*/

      double eigNorm = getEigNorm(sourceC2D, targetD2C, superToDirect);
      
      /*DoubleMatrix2D invIsometryCheckMatrix = DoubleFactory2D.dense.make(invIsometryCheck);
      EigenvalueDecomposition invEig = new EigenvalueDecomposition(invIsometryCheckMatrix);
      double[] invEigValues = invEig.getRealEigenvalues().toArray();
      double invEigNorm = 0;
      for (int i = 0; i < eigValues.length; i++) {
        invEigNorm += Math.log(invEigValues[i]) * Math.log(invEigValues[i]);
      }
      invEigNorm = Math.exp(Math.sqrt(invEigNorm / eigValues.length)) - 1;
      
      for (int rowNum = 0; rowNum < isometryCheck.length; rowNum++) {
        isometryCheck[rowNum][rowNum] -= 1;
        invIsometryCheck[rowNum][rowNum] -= 1;
        double[] row = isometryCheck[rowNum];
        double[] invRow = invIsometryCheck[rowNum];
        for (int colNum = 0; colNum < row.length; colNum++) {
          norm += row[colNum] * row[colNum];
          invNorm += invRow[colNum] * invRow[colNum];
        }
      }
      norm = Math.sqrt(norm / numElements);
      invNorm = Math.sqrt(invNorm / numElements);*/
      if (eigNorm < minNorm) {
        minNorm = eigNorm;
      }
      if (eigNorm > m_MaxAllowedSkewRMSError) {continue;}
      returnArray[returnIndex++] = ArrayUtils.copyArray(superToDirect);
      
    } while (indexer.increment(currentState));
    
    return ArrayUtils.truncateArray(returnArray, returnIndex);

  }
  
  protected double getEigNorm(double[][] sourceC2D, double[][] targetD2C, int[][] superToDirect) {
    
    if (superToDirect.length == 0) {
      return 0;
    }
    
    double[][] d2s = MSMath.simpleInverse(superToDirect);
    double[][] cartesianOperation = MSMath.matrixMultiply(sourceC2D, d2s);
    cartesianOperation = MSMath.matrixMultiply(cartesianOperation, targetD2C);
  
    // I think this gets rid of the rotational component
    double[][] isometryCheck = MSMath.matrixMultiply(cartesianOperation, MSMath.transpose(cartesianOperation));
    
    DenseMatrix64F isometryCheckMatrix = new SimpleMatrix(isometryCheck).getMatrix();
    SimpleEVD<SimpleMatrix> eig = new SimpleEVD<SimpleMatrix>(isometryCheckMatrix);
    //DoubleMatrix2D isometryCheckMatrix = DoubleFactory2D.dense.make(isometryCheck);
    //EigenvalueDecomposition eig = new EigenvalueDecomposition(isometryCheckMatrix);
    //double[] eigValues = eig.getRealEigenvalues().toArray();
    double eigNorm = 0;
    for (int i = 0; i < eig.getNumberOfEigenvalues(); i++) {
      double logValue = Math.log(eig.getEigenvalue(i).getReal());  
      eigNorm += logValue * logValue;
    }
    return Math.sqrt(eigNorm / eig.getNumberOfEigenvalues()); // Use the fact that exp(dx) ~ 1 + dx
  }
  
  protected boolean findMap() {
    
    if (m_TriedToFindMap) {return (m_TransformationOperation != null);}
    m_TriedToFindMap = true;
    if (!this.isMapPossible()) {return false;}

    Status.detail("Starting to search for map.");
    
    long startTime = System.currentTimeMillis();
    long latestAllowedTime = startTime + m_TimeOut;
    
    Status.detail("Determined map may be possible based on stoichiometry, dimensionality, and volume.");
    
    /*Species[] knownSpecies = m_SourceStructure.getDistinctSpecies();
    int[] specCounts = new int[knownSpecies.length];
    for (int specNum = 0; specNum < specCounts.length; specNum++) {
      specCounts[specNum] = m_SourceStructure.numDefiningSitesWithSpecie(knownSpecies[specNum]);
    }
    int maxSourcePrimCells = MSMath.GCF(specCounts);
    int maxTargetPrimCells = maxSourcePrimCells * m_TargetStructure.numDefiningSites() / m_SourceStructure.numDefiningSites();
    int numSourcePrimCells = maxTargetPrimCells;
    int numTargetPrimCells = maxSourcePrimCells;*/
    
    int lcm = MSMath.LCM(new int[] {m_SourceStructure.numDefiningSites(), m_TargetStructure.numDefiningSites()});
    if (lcm > m_MaxAllowedSuperCellAtoms) {
        Status.warning("Mapper requires at least " + lcm + " atoms in a supercell to map structures.  This exceeds limit of " + m_MaxAllowedSuperCellAtoms + " atoms.");
        return false;
    }
    
    int numSourcePrimCells = lcm / m_SourceStructure.numDefiningSites();
    int numTargetPrimCells = lcm / m_TargetStructure.numDefiningSites();
    int maxSuperCells = Math.max(numSourcePrimCells, numTargetPrimCells);
    
    if (maxSuperCells > m_MaxAllowedSuperCellSize) {
      
      // The cellfactors should always be 1 becasue of the lcm above.
      /*int cellFactor = MSMath.GCF(new int[] {numSourcePrimCells, numTargetPrimCells});
      numSourcePrimCells /= cellFactor;
      numTargetPrimCells /= cellFactor;
      double requiredCells = Math.max(numSourcePrimCells, numTargetPrimCells);
      if (requiredCells > m_MaxAllowedSuperCellSize) {*/
        Status.warning("Mapper requires at supercells of at least " + maxSuperCells + " primitive cells to map structures.  This exceeds limit of " + m_MaxAllowedSuperCellSize + " primitive cells.");
        return false;
      //}
    }
    
    Status.detail("Using " + numSourcePrimCells + " cells for source structure and " + numTargetPrimCells + " cells for target structure");
    
    //numSourcePrimCells = 4;
    //numTargetPrimCells = 2;
    
    SuperLatticeGenerator sourceGenerator = new SuperLatticeGenerator(numSourcePrimCells, m_SourceStructure.numPeriodicDimensions());
    SuperLatticeGenerator targetGenerator = new SuperLatticeGenerator(numTargetPrimCells, m_TargetStructure.numPeriodicDimensions());
    
    int[][][] sourceSuperToDirects = sourceGenerator.generateSuperLattices(m_SourceStructure.getDefiningSpaceGroup(), true);
    int[][][] targetSuperToDirects = targetGenerator.generateSuperLattices(m_TargetStructure.getDefiningSpaceGroup(), true);
    
    Status.detail("Found possible superlattices.");
    
    Structure[] targetCompactSupers = new Structure[targetSuperToDirects.length];
    
    double bestKnownTotalSE = m_MaxAllowedRMSError * m_MaxAllowedRMSError * m_SourceStructure.numDefiningSites() * numSourcePrimCells;
    double bestKnownMinOffsetDiff = m_MaxAllowedOffsetDifference;
    int[][] minShiftMatrix = null;
    Structure minShiftedSuperSource = null;
    Structure minSuperSource = null;
    Structure minSuperTarget = null;
    Structure minAffineSource = null;
    Structure.Site[] minMappedSites = null;
    
    for (int sourceSuperNum = 0; sourceSuperNum < sourceSuperToDirects.length; sourceSuperNum++) {
      int[][] sourceS2D = sourceSuperToDirects[sourceSuperNum];
      Structure sourceSuper = new SuperStructure(m_SourceStructure, sourceS2D);
      sourceSuper = sourceSuper.getCompactStructure();
      for (int targetSuperNum = 0; targetSuperNum < targetSuperToDirects.length; targetSuperNum++) {
        try {
          Status.detail("Testing source superlattice " + sourceSuperNum + " / " + sourceSuperToDirects.length + ", target superlattice " + targetSuperNum + " / " + targetSuperToDirects.length + ".");
          if (System.currentTimeMillis() > latestAllowedTime) {
            throw new TimeOutException("Time out while iterating through superlattices.");
          }
          Structure targetSuper = targetCompactSupers[targetSuperNum];
          int[][] targetS2D = targetSuperToDirects[targetSuperNum];
          if (targetSuper == null) {
            targetSuper = new SuperStructure(m_TargetStructure, targetS2D).getCompactStructure();
            targetCompactSupers[targetSuperNum] = targetSuper;
          }
          LinearBasis targetBasis = targetSuper.getDirectBasis();
          int[][][] shiftMatrices = this.getShiftMatrices(sourceSuper, targetSuper);
          Status.detail("  Found " + shiftMatrices.length + " possible lattice matches.");
          for (int latticeNum = 0; latticeNum < shiftMatrices.length; latticeNum++) {
            Status.detail("  Testing lattice match #" + latticeNum + " / " + shiftMatrices.length);
  
            int[][] shiftMatrix = shiftMatrices[latticeNum];
            Structure shiftedSourceSuper = new SuperStructure(sourceSuper, shiftMatrix);
            LinearBasis shiftedSourceBasis = shiftedSourceSuper.getDirectBasis();
      
            StructureBuilder builder = new StructureBuilder(targetSuper);
            builder.clearSites();
            for (int siteNum = 0; siteNum < shiftedSourceSuper.numDefiningSites(); siteNum++) {
              double[] shiftedDirectCoords = shiftedSourceSuper.getSiteCoords(siteNum).getCoordArray(shiftedSourceBasis);
              builder.addSite(new Coordinates(shiftedDirectCoords, targetBasis), shiftedSourceSuper.getSiteSpecies(siteNum));
            }
            Structure affineSource = new Structure(builder);
            Structure.Site sourceSite = affineSource.getDefiningSite(0);
            for (int siteNum = 0; siteNum < targetSuper.numDefiningSites(); siteNum++) {
              Structure.Site targetSite = targetSuper.getDefiningSite(siteNum);
              if (sourceSite.getSpecies().getElement() != targetSite.getSpecies().getElement()) {
                continue;
              }
              if (!m_MatchElementsOnly && (sourceSite.getSpecies() != targetSite.getSpecies())) {
                continue;
              }
              Vector shift = new Vector(sourceSite.getCoords(), targetSite.getCoords());
              Structure shiftedAffineSource = affineSource.translate(shift);
              Structure.Site[] mappedSites = this.getBestMap(shiftedAffineSource, targetSuper, bestKnownTotalSE, bestKnownMinOffsetDiff, latestAllowedTime);
  
              if (mappedSites == null) {
                continue;
              }
              Status.detail("  Found map!");
              double sqError = this.getSQError(shiftedAffineSource, mappedSites);
              double maxOffsetDiff = this.getMaxOffsetDifference(shiftedAffineSource, mappedSites);
              if (((m_MinimizeRMSError && (sqError < bestKnownTotalSE)) || (!m_MinimizeRMSError && (maxOffsetDiff < bestKnownMinOffsetDiff)))) {
                bestKnownTotalSE = sqError;
                bestKnownMinOffsetDiff = maxOffsetDiff;
                minShiftMatrix = ArrayUtils.copyArray(shiftMatrix);
                minAffineSource = affineSource;
                minMappedSites = mappedSites;
                minShiftedSuperSource = shiftedSourceSuper;
                minSuperSource = sourceSuper;
                minSuperTarget = targetSuper;
                if (!m_FindBest) {break;}
              }
            }
            if (minAffineSource != null && !m_FindBest) {
              break;
            }
          }
        }
        catch (matsci.exceptions.TimeOutException t) {
          int numSteps = sourceSuperNum * targetSuperToDirects.length + targetSuperNum;
          int totalSteps = sourceSuperToDirects.length * targetSuperToDirects.length;
          double progress = 1.0 * numSteps / totalSteps;
          long elapsedTime = (System.currentTimeMillis() - startTime);
          long newTimeOut = Math.round((elapsedTime / progress) * 1.1);
          throw new TimeOutException("Time out after " + elapsedTime + " milliseconds while approximately " + ((int) Math.round(progress * 100)) + "% of the way through superlattice comparisons.  Consider increasing timeout from " + m_TimeOut + " to " + newTimeOut,t );
        }
      }
    }
    
    Status.detail("Finished main map search.");
    
    if (minAffineSource == null) {
      Status.detail("Failed to find map.");
      Status.detail("Done.");
      return false; // Couldn't find a map!
    }
    
    Status.detail("Saving data for best map.");
    
    double[][] shiftedSourceC2B = minShiftedSuperSource.getDirectBasis().getCartesianToBasis();
    double[][] sourceC2B = minSuperSource.getDirectBasis().getCartesianToBasis();
    double[][] targetB2C = minSuperTarget.getDirectBasis().getBasisToCartesian();
    int numPeriodicDimensions = minShiftMatrix.length;
    
    int[] sourcePeriodicIndices = minSuperSource.getDefiningLattice().getPeriodicIndices();
    int[] targetPeriodicIndices = minSuperSource.getDefiningLattice().getPeriodicIndices();
    
    shiftedSourceC2B = ArrayUtils.selectColumns(shiftedSourceC2B, sourcePeriodicIndices);
    sourceC2B = ArrayUtils.selectColumns(sourceC2B, sourcePeriodicIndices);
    targetB2C = ArrayUtils.selectRows(targetB2C, targetPeriodicIndices);
    
    double volumeScale = minSuperSource.getDefiningLattice().getCellSize() / minSuperTarget.getDefiningLattice().getCellSize();
    double[][] scaledTargetB2C = MSMath.matrixTimesScalar(targetB2C, Math.pow(volumeScale, 1.0 / numPeriodicDimensions));
   
    double[][] transformation = MSMath.matrixMultiply(shiftedSourceC2B, targetB2C);
    
    Vector avgOffset = this.getAvgOffset(minAffineSource, minMappedSites);
   
    m_SiteMap = new int[minMappedSites.length];
    for (int mappedSiteNum = 0; mappedSiteNum < minMappedSites.length; mappedSiteNum++) {
      m_SiteMap[mappedSiteNum] = minMappedSites[mappedSiteNum].getIndex();
    }
    m_MappedMaxOffsetDiff = bestKnownMinOffsetDiff;
    m_MappedRMSError = Math.sqrt(bestKnownTotalSE / m_SiteMap.length);
    m_MappedSkewFactor = getEigNorm(sourceC2B, scaledTargetB2C, minShiftMatrix);
    m_MappedVolumeScale = volumeScale;
    m_SourceSuperToDirect = m_SourceStructure.getSuperToDirect(minShiftedSuperSource);
    m_TargetSuperToDirect = m_TargetStructure.getSuperToDirect(minSuperTarget);
    m_TransformationOperation = new SymmetryOperation(transformation, avgOffset, CartesianBasis.getInstance());
   
    Status.detail("Done.");
    return true;
    
  }
  
  protected double getVolumeScale() {
    double sourceVolPerSite = m_SourceStructure.getDefiningVolume() / m_SourceStructure.numDefiningSites();
    double targetVolPerSite = m_TargetStructure.getDefiningVolume() / m_TargetStructure.numDefiningSites();
    double ratio = (m_SourceStructure.getDefiningLattice().numPeriodicVectors() == 0) ? 1 : sourceVolPerSite / targetVolPerSite;
    return Math.min(ratio, 1 / ratio);
  }

  /*protected static double getSkewFactor(BravaisLattice sourceLattice, BravaisLattice targetLattice) {
    
    int numDimensions = sourceLattice.numPeriodicVectors();
    if (numDimensions < 2) {return 1;}
    
    LinearBasis sourceBasis = sourceLattice.getLatticeBasis();
    LinearBasis targetBasis = targetLattice.getLatticeBasis();
    
    double[][] transformationArray = MSMath.matrixMultiply(sourceBasis.getCartesianToBasis(), targetBasis.getBasisToCartesian());
    transformationArray = MSMath.matrixMultiply(transformationArray, MSMath.transpose(transformationArray)); // Check to see if it's a rotation or reflection matrix
    
    //double[][] transformationArray = new double[numDimensions][];
    double maxVolume = 1;
    for (int vecNum = 0; vecNum < targetLattice.numPeriodicVectors(); vecNum++) {
      //transformationArray[vecNum] = targetLattice.getPeriodicVector(vecNum).getDirection().getCoordArray(sourceBasis);
      maxVolume *= MSMath.magnitude(transformationArray[vecNum]);
    }
    double ratio = MSMath.determinant(transformationArray) / maxVolume;
    ratio = Math.pow(ratio, 1.0 / (numDimensions - 1));
    double returnValue = 1 - Math.acos(ratio) / Math.PI;
    return Math.sqrt(returnValue); // Account for the fact that the matrix represents the transformation applied twice

  }*/
  
  protected Vector getAvgOffset(Structure sourceStructure, Structure.Site[] mappedTargetSites) {
    
    double[] avgOffset = new double[sourceStructure.getDefiningLattice().numTotalVectors()];
    for (int siteNum = 0; siteNum < sourceStructure.numDefiningSites(); siteNum++) {
      double[] sourceCartArray = sourceStructure.getDefiningSite(siteNum).getCoords().getCartesianArray();
      double[] targetCartArray = mappedTargetSites[siteNum].getCoords().getCartesianArray();
      for (int dimNum = 0; dimNum < avgOffset.length; dimNum++) {
        avgOffset[dimNum] += targetCartArray[dimNum] - sourceCartArray[dimNum];
      }
    }
    
    for (int dimNum = 0; dimNum < avgOffset.length; dimNum++) {
      avgOffset[dimNum] /= mappedTargetSites.length;
    }
    
    return new Vector(avgOffset, CartesianBasis.getInstance());
  }

  protected double getSQError(Structure sourceStructure, Structure.Site[] mappedTargetSites) {
    
    double[] avgOffset = new double[sourceStructure.getDefiningLattice().numTotalVectors()];
    for (int siteNum = 0; siteNum < sourceStructure.numDefiningSites(); siteNum++) {
      double[] sourceCartArray = sourceStructure.getDefiningSite(siteNum).getCoords().getCartesianArray();
      double[] targetCartArray = mappedTargetSites[siteNum].getCoords().getCartesianArray();
      for (int dimNum = 0; dimNum < avgOffset.length; dimNum++) {
        avgOffset[dimNum] += targetCartArray[dimNum] - sourceCartArray[dimNum];
      }
    }
    
    for (int dimNum = 0; dimNum < avgOffset.length; dimNum++) {
      avgOffset[dimNum] /= mappedTargetSites.length;
    }
    
    double sqError = 0;
    for (int siteNum = 0; siteNum < sourceStructure.numDefiningSites(); siteNum++) {
      double[] sourceCartArray = sourceStructure.getDefiningSite(siteNum).getCoords().getCartesianArray();
      double[] targetCartArray = mappedTargetSites[siteNum].getCoords().getCartesianArray();
      for (int dimNum = 0; dimNum < avgOffset.length; dimNum++) {
        double delta = targetCartArray[dimNum] - sourceCartArray[dimNum] - avgOffset[dimNum];
        sqError += delta * delta;
      }
    }
    
    return sqError;
    
  }
  
  protected double getMaxOffsetDifference(Structure sourceStructure, Structure.Site[] mappedTargetSites) {
    
    double[][] offsets = new double[sourceStructure.numDefiningSites()][];
    
    for (int siteNum = 0; siteNum < sourceStructure.numDefiningSites(); siteNum++) {
      double[] sourceCartArray = sourceStructure.getDefiningSite(siteNum).getCoords().getCartesianArray();
      double[] targetCartArray = mappedTargetSites[siteNum].getCoords().getCartesianArray();
      offsets[siteNum] = MSMath.arrayDiff(sourceCartArray, targetCartArray);
    }
    
    double maxDiffSq = 0;
    for (int siteNum = 0; siteNum < offsets.length; siteNum++) {
      double[] offset = offsets[siteNum];
      for (int siteNum2 = 0; siteNum2 < siteNum; siteNum2++) {
        double[] offset2 = offsets[siteNum2];
        double[] diff = MSMath.arrayDiff(offset, offset2);
        double mag = MSMath.dotProduct(diff, diff);
        maxDiffSq = Math.max(maxDiffSq, mag);
      }
    }
    
    return Math.sqrt(maxDiffSq);
    
    
  }
  
  protected Structure fitToLattice(BravaisLattice lattice, Structure structure, LinearBasis directBasis) {
    
    StructureBuilder builder = new StructureBuilder(structure);
    builder.setCellVectors(lattice.getCellVectors());
    for (int siteNum = 0; siteNum < builder.numDefiningSites(); siteNum++) {
      double[] directCoordArray = builder.getSiteCoords(siteNum).getCoordArray(directBasis);
      builder.setSiteCoordinates(siteNum, new Coordinates(directCoordArray, lattice.getLatticeBasis()));
    }
    
    return new Structure(builder);
  }
  
  /**
   * TODO replace this with the Hungarian Algorithm
   * 
   * @param source
   * @param target
   * @param bestKnownSE
   * @param bestKnownOffsetDiff
   * @param latestAllowedTime
   * @return
   */
  protected Structure.Site[] getBestMap(Structure source, Structure target, double bestKnownSE, double bestKnownOffsetDiff, long latestAllowedTime) {

    Structure.Site[][] possibleSites = new Structure.Site[source.numDefiningSites()][];
    double[][][] targetOffsets = new double[possibleSites.length][][];
    int[] numStates = new int[possibleSites.length];
    
    // Speed this up by trying to match the rarest elements first
    Species[] species = source.getDistinctSpecies();
    int[] counts = new int[species.length];
    for (int specNum = 0; specNum < species.length; specNum++) {
    	counts[specNum] = source.numDefiningSitesWithSpecies(species[specNum]);
    }
    int[] specMap = ArrayUtils.getSortPermutation(counts);
    
    for (int specNum = 0; specNum < specMap.length; specNum++) {
    	Species spec = species[specMap[specNum]];
	    for (int siteNum = 0; siteNum < source.numDefiningSites(); siteNum++) {
	      if (source.getSiteSpecies(siteNum) != spec) {continue;}
	      Structure.Site sourceSite = source.getDefiningSite(siteNum);
	      double[] sourceCoordArray = sourceSite.getCoords().getCartesianArray();
	      if (m_MatchElementsOnly) {
	        numStates[siteNum] = target.numDefiningSitesWithElement(sourceSite.getSpecies().getElement());
	      } else {
	        numStates[siteNum] = target.numDefiningSitesWithSpecies(sourceSite.getSpecies());
	      }
	      possibleSites[siteNum] = new Structure.Site[numStates[siteNum]];
	      targetOffsets[siteNum] = new double[possibleSites[siteNum].length][];
	      double[] distances = new double[possibleSites[siteNum].length];
	      int targetSiteIndex = 0;
	      for (int siteNum2 = 0; siteNum2 < target.numDefiningSites(); siteNum2++) {
	        if (System.currentTimeMillis() > latestAllowedTime) {
	          throw new TimeOutException("Time out while building nearest-neighbor list.");
	        }
	        Structure.Site targetSite = target.getDefiningSite(siteNum2);
	        if (targetSite.getSpecies().getElement() != sourceSite.getSpecies().getElement()) {continue;}
	        if (!m_MatchElementsOnly && (targetSite.getSpecies() != sourceSite.getSpecies())) {continue;}
	        Structure.Site nearbySite = target.getNearbySite(sourceSite.getCoords(), siteNum2);
	        double distance = sourceSite.distanceFrom(nearbySite);
	        if (distance > m_MaxAllowedOffsetDifference) {continue;} // TODO verify this is OK
	        if (!m_MinimizeRMSError && (distance > bestKnownOffsetDiff)) {continue;} // TODO verify this is OK
	        distances[targetSiteIndex] = distance;
	        possibleSites[siteNum][targetSiteIndex] = nearbySite;
	        targetSiteIndex++;
	      }
	      if (targetSiteIndex == 0) { // This can't be mapped to any sites 
	        /*if (siteNum > 3) {
	          Status.detail("Unable to find map for source site " + siteNum);
	        }*/
	        return null;
	      } 
	      distances = ArrayUtils.truncateArray(distances, targetSiteIndex);
	      possibleSites[siteNum] = (Structure.Site[]) ArrayUtils.truncateArray(possibleSites[siteNum], targetSiteIndex);
	      targetOffsets[siteNum] = (double[][]) ArrayUtils.truncateArray(targetOffsets[siteNum], targetSiteIndex);
	      numStates[siteNum] = targetSiteIndex;
	      int[] map = ArrayUtils.getSortPermutation(distances);
	      Structure.Site[] sortedSites = new Structure.Site[map.length];
	      for (int siteIndex = 0; siteIndex < distances.length; siteIndex++) {
	        sortedSites[siteIndex] = possibleSites[siteNum][map[siteIndex]];
	        double[] targetCoordArray = sortedSites[siteIndex].getCoords().getCartesianArray();
	        targetOffsets[siteNum][siteIndex] = MSMath.arrayDiff(targetCoordArray, sourceCoordArray);
	      }
	      possibleSites[siteNum] = sortedSites;
	    }
    }
    
    ArrayIndexer indexer = new ArrayIndexer(numStates);
    PermutationFilter permutationFilter = new PermutationFilter(possibleSites);
    MaxSEFilter seFilter = new MaxSEFilter(targetOffsets);
    seFilter.setMaxAllowedSE(m_MinimizeRMSError ? bestKnownSE :  m_MaxAllowedRMSError * m_MaxAllowedRMSError * source.numDefiningSites());
    MaxOffsetDifferenceFilter diffFilter = new MaxOffsetDifferenceFilter(targetOffsets);
    diffFilter.setMaxAllowedOffsetDifference(m_MinimizeRMSError ? m_MaxAllowedOffsetDifference : bestKnownOffsetDiff);
    
    ArrayIndexer.Filter[] filters = new ArrayIndexer.Filter[] {permutationFilter, seFilter, diffFilter};
    
    int[] selectedSites = indexer.getInitialState(filters, latestAllowedTime - System.currentTimeMillis());
    if (selectedSites == null) {
      return null;
    }
    int[] bestSites = ArrayUtils.copyArray(selectedSites);
    if (m_FindBest) {
      do {
        bestSites = ArrayUtils.copyArray(selectedSites);
        if (m_MinimizeRMSError) {
          seFilter.setMaxAllowedSE(seFilter.getLastGoodSE());
        } else {
          diffFilter.setMaxAllowedOffsetDifference(diffFilter.getLastMaxOffsetDifference());
        }
      } while (indexer.increment(selectedSites, filters, latestAllowedTime - System.currentTimeMillis()));
    }
    
    Structure.Site[] returnMap = new Structure.Site[bestSites.length];
    for (int siteNum = 0; siteNum < bestSites.length; siteNum++) {
      returnMap[siteNum] = possibleSites[siteNum][bestSites[siteNum]];
    }
    return returnMap;
  }
  
  // Assumes both structures have the same lattice, and translations have been taken
  // into account
  /*protected boolean mapSites(Structure sourceStructure, Structure targetStructure) {
    
    Specie[] allSpecies = new Specie[0];
    int[] sourceSpecieCount = new int[0];
    int[] targetSpecieCount = new int[0];
    for (int siteNum = 0; siteNum < sourceStructure.numDefiningSites(); siteNum++) {
      Specie spec = sourceStructure.getSiteSpecie(siteNum);
      int index = ArrayUtils.findIndex(allSpecies, spec);
      if (index < 0) {
        index = allSpecies.length;
        allSpecies = (Specie[]) ArrayUtils.appendElement(allSpecies, spec);
        sourceSpecieCount = ArrayUtils.appendElement(sourceSpecieCount, 0);
        targetSpecieCount = ArrayUtils.appendElement(targetSpecieCount, 0);
      }
      sourceSpecieCount[index]++;
    }
    
    for (int siteNum = 0; siteNum < targetStructure.numDefiningSites(); siteNum++) {
      Specie spec = targetStructure.getSiteSpecie(siteNum);
      int index = ArrayUtils.findIndex(allSpecies, spec);
      if (index < 0) {
        index = allSpecies.length;
        allSpecies = (Specie[]) ArrayUtils.appendElement(allSpecies, spec);
        sourceSpecieCount = ArrayUtils.appendElement(sourceSpecieCount, 0);
        targetSpecieCount = ArrayUtils.appendElement(targetSpecieCount, 0);
      }
      targetSpecieCount[index]++;
    }
    
    double volume = sourceStructure.getDefiningLattice().getCellSize();
    int numSmallSites = 0;
    for (int specNum = 0; specNum < allSpecies.length; specNum++) {
      numSmallSites += Math.min(sourceSpecieCount[specNum], targetSpecieCount[specNum]);
    }
    int numBigSites = sourceStructure.numDefiningSites() + targetStructure.numDefiningSites() - numSmallSites;
    double[][] probabilities = new double[numSmallSites][];
    int[][] bigSitesBySmallSite = new int[numSmallSites][];
    int[] numBigSitesBySmallSite = new int[numSmallSites];
    int[] smallSiteIndices = new int[numSmallSites];
    int[] specIndices = new int[numSmallSites];
    boolean[][] seenBigSites = new boolean[allSpecies.length][numBigSites]; // TODO think about this
    boolean[] isSmallSiteSource = new boolean[allSpecies.length];
    
    int smallSiteNum = 0;
    double probabilityScale = 1;
    for (int specNum = 0; specNum < allSpecies.length; specNum++) {
      Structure smallStructure = sourceStructure;
      Structure bigStructure = targetStructure;
      int numBigSitesForSpec = targetSpecieCount[specNum];
      isSmallSiteSource[specNum] = true;
      if (sourceSpecieCount[specNum] > targetSpecieCount[specNum]) {
        smallStructure = targetStructure;
        bigStructure = sourceStructure;
        numBigSitesForSpec = sourceSpecieCount[specNum];
        isSmallSiteSource[specNum] = false;
      }
      //double gaussianWidth = Math.pow(volume, 1.0 / sourceStructure.numPeriodicDimensions()) / 2; // TODO add a scale factor?
      double gaussianWidth = 1; // TODO add a scale factor?
      double denominator = 1 / (2 * gaussianWidth * gaussianWidth);
      Specie spec = allSpecies[specNum];
      for (int siteNum = 0; siteNum < smallStructure.numDefiningSites(); siteNum++) {
        Structure.Site site = smallStructure.getDefiningSite(siteNum);
        if (site.getSpecie() != spec) {continue;}
        double[] probsForSite = new double[numBigSitesForSpec];
        int[] bigSitesForSite = new int[numBigSitesForSpec];
        Arrays.fill(bigSitesForSite, -1);
        int numNearbyBigSites = 0;
        Structure.Site[] nearbySites = bigStructure.getNearbySites(site.getCoords(), 3 * gaussianWidth, true);
        for (int neighborNum = 0; neighborNum < nearbySites.length; neighborNum++) {
          Structure.Site neighbor = nearbySites[neighborNum];
          if (neighbor.getSpecie() != spec) {continue;}
          double distance = site.distanceFrom(neighbor.getCoords());
          double probability = Math.exp(-distance * distance * denominator);
          for (int bigSiteNum = 0; bigSiteNum < bigSitesForSite.length; bigSiteNum++) {
            int knownIndex = bigSitesForSite[bigSiteNum];
            if (knownIndex >= 0) {
              if (knownIndex != site.getIndex()) {continue;}
              if (probability < probsForSite[bigSiteNum]) {continue;}
              numNearbyBigSites--;
            }
            probsForSite[bigSiteNum] = probability;
            bigSitesForSite[bigSiteNum] = neighbor.getIndex();
            numNearbyBigSites++;
            break;
          }
        }

        if (numNearbyBigSites == 0) {return false;}  // No possible map for this site
        
        probsForSite = ArrayUtils.truncateArray(probsForSite, numNearbyBigSites);
        bigSitesForSite = ArrayUtils.truncateArray(bigSitesForSite, numNearbyBigSites);
        
        int[] map = ArrayUtils.getSortPermutation(probsForSite);
        probabilities[smallSiteNum] = new double[probsForSite.length];
        bigSitesBySmallSite[smallSiteNum] = new int[bigSitesForSite.length];
        numBigSitesBySmallSite[smallSiteNum] = probsForSite.length;
        specIndices[smallSiteNum] = specNum;
        smallSiteIndices[smallSiteNum] = site.getIndex();
        double maxProbForSite = probsForSite[map[map.length - 1]];
        if (maxProbForSite == 0) {return false;}
        probabilityScale *= maxProbForSite;
        for (int bigSiteNum = 0; bigSiteNum < map.length; bigSiteNum++) {
          int reverseIndex = map.length - bigSiteNum - 1;
          probabilities[smallSiteNum][reverseIndex] = probsForSite[map[bigSiteNum]] / maxProbForSite;
          bigSitesBySmallSite[smallSiteNum][reverseIndex] = bigSitesForSite[map[bigSiteNum]];
        }
        smallSiteNum++;
      }
    }
    
    ArrayIndexer bigSiteIndexer = new ArrayIndexer(numBigSitesBySmallSite);
    int[] stateIndices = bigSiteIndexer.getInitialState();
    int[] minIndices = new int[stateIndices.length];
    Arrays.fill(minIndices, -1);
    
    double maxProbability = Math.pow(m_SiteMapScore, numBigSites);
    do {
      double probability = 1;
      boolean legal = true;
      for (int specNum= 0; specNum < seenBigSites.length; specNum++) {
        Arrays.fill(seenBigSites[specNum], false);
      }
      for (smallSiteNum = stateIndices.length - 1; smallSiteNum >= 0; smallSiteNum--) {
        int bigSiteNum = stateIndices[smallSiteNum];
        int bigSiteIndex = bigSitesBySmallSite[smallSiteNum][bigSiteNum];
        int specNum = specIndices[smallSiteNum];
        if ((smallSiteNum != stateIndices.length - 1) && (specNum == specIndices[smallSiteNum+1]) && seenBigSites[specNum][bigSiteIndex]) {
          legal = false;
          bigSiteIndexer.branchEnd(smallSiteNum, stateIndices);
          break;
        }
        seenBigSites[specNum][bigSiteIndex] = true;
        probability *= probabilities[smallSiteNum][bigSiteNum];
        if ((probability < maxProbability) || probability == 0) {
          bigSiteIndexer.branchEnd(smallSiteNum, stateIndices);
          break;
        }
      }
      if (legal && (probability > maxProbability)) {
        maxProbability = probability;
        System.arraycopy(stateIndices, 0, minIndices, 0, stateIndices.length);
      }
    } while (bigSiteIndexer.increment(stateIndices));
    
    if (minIndices[0] < 0) {return false;}
    m_SiteMapScore = Math.pow(maxProbability * probabilityScale, 1.0 / numBigSites);
    System.out.println(m_SiteMapScore); // TODO for debugging only
    m_SiteMap = new int[2][numSmallSites];
    for (smallSiteNum = 0; smallSiteNum < minIndices.length; smallSiteNum++) {
      int bigSiteIndex = bigSitesBySmallSite[smallSiteNum][minIndices[smallSiteNum]];
      int specNum = specIndices[smallSiteNum];
      int smallSiteSourceOrTarget = (isSmallSiteSource[specNum]) ? 0 : 1;
      int bigSiteSourceOrTarget = 1 - smallSiteSourceOrTarget;
      m_SiteMap[smallSiteSourceOrTarget][smallSiteNum] = smallSiteIndices[smallSiteNum];
      m_SiteMap[bigSiteSourceOrTarget][smallSiteNum] = bigSiteIndex;
    }
    return true;
  }*/
  
  protected class PermutationFilter implements ArrayIndexer.Filter {

    protected boolean[] m_SeenSites;
    protected Structure.Site[][] m_AllowedSites;
    
    public PermutationFilter(Structure.Site[][] allowedSites) {
      m_AllowedSites = (Structure.Site[][]) ArrayUtils.copyArray(allowedSites);
      m_SeenSites = new boolean[allowedSites.length];
    }
    
    public int getBranchIndex(int[] currentState) {
      Arrays.fill(m_SeenSites, false);
      for (int siteNum = currentState.length - 1; siteNum >= 0; siteNum--) {
        int siteIndex = m_AllowedSites[siteNum][currentState[siteNum]].getIndex();
        if (m_SeenSites[siteIndex]) {
          return siteNum;
        }
        m_SeenSites[siteIndex] = true;
      }
      return -1;
    }
  }
  
  protected class MaxOffsetDifferenceFilter implements ArrayIndexer.Filter {

    protected double[][][] m_TargetOffsets;
    protected double m_MaxAllowedSqOffsetDifference;
    protected double m_LastMaxSqOffsetDifference = -1;
    
    protected MaxOffsetDifferenceFilter(double[][][] targetOffsets) {
      m_TargetOffsets = ArrayUtils.copyArray(targetOffsets);
    }
    
    protected void setMaxAllowedOffsetDifference(double value) {
      m_MaxAllowedSqOffsetDifference = value * value;
    }
    
    protected double getLastMaxOffsetDifference() {
      return Math.sqrt(m_LastMaxSqOffsetDifference);
    }
    
    public int getBranchIndex(int[] currentState) {
      
      double maxSqDifference = 0;
      
      for (int siteNum = currentState.length -1; siteNum >= 0; siteNum--) {
        
        double[] offset = m_TargetOffsets[siteNum][currentState[siteNum]];
        
        for (int siteNum2 = siteNum; siteNum2 < currentState.length; siteNum2++) {
          int siteState = currentState[siteNum2];
          double[] offset2 = m_TargetOffsets[siteNum2][siteState];
          double sqDifference = 0;
          for (int dimNum = 0; dimNum < offset2.length; dimNum++) {
            double delta = offset[dimNum] - offset2[dimNum];
            sqDifference += delta * delta;
          }
          if (sqDifference > m_MaxAllowedSqOffsetDifference) {
            return siteNum;
          }
          maxSqDifference = Math.max(maxSqDifference, sqDifference);
        }
        
      }
      
      m_LastMaxSqOffsetDifference = maxSqDifference;      
      return -1;
      
    }
    
  }
  
  protected class MaxSEFilter implements ArrayIndexer.Filter {

    protected double[][][] m_TargetOffsets;
    protected double m_MaxAllowedSE;
    protected double m_LastGoodSE = -1;
    
    protected MaxSEFilter(double[][][] targetOffsets) {
      m_TargetOffsets = ArrayUtils.copyArray(targetOffsets);
    }
    
    protected void setMaxAllowedSE(double value) {
      m_MaxAllowedSE = value;
    }
    
    protected double getLastGoodSE() {
      return m_LastGoodSE;
    }
    
    public int getBranchIndex(int[] currentState) {
      
      int numDimensions = m_TargetOffsets[0][0].length;
      double sqError = 0;
      
      for (int siteNum = currentState.length -1; siteNum >= 0; siteNum--) {
        double[] avgOffset = new double[numDimensions];
        
        for (int siteNum2 = siteNum; siteNum2 < currentState.length; siteNum2++) {
          int siteState = currentState[siteNum2];
          double[] offset2 = m_TargetOffsets[siteNum2][siteState];
          for (int dimNum = 0; dimNum < offset2.length; dimNum++) {
            avgOffset[dimNum] += offset2[dimNum];
          }
        }
        
        int numAvgSites = currentState.length - siteNum;
        for (int dimNum = 0; dimNum < avgOffset.length; dimNum++) {
          avgOffset[dimNum] /= numAvgSites;
        }
        
        sqError = 0;
        for (int siteNum2 = siteNum; siteNum2 < currentState.length; siteNum2++) {
          int siteState = currentState[siteNum2];
          double[] offset2 = m_TargetOffsets[siteNum2][siteState];
          for (int dimNum = 0; dimNum < offset2.length; dimNum++) {
            double delta = offset2[dimNum] - avgOffset[dimNum];
            sqError += delta * delta;
            if (sqError > m_MaxAllowedSE) {
              return siteNum;
            }
          }
        }
      }
      
      m_LastGoodSE = sqError;      
      return -1;
    }
   
  }

}