/*
 * Created on Mar 30, 2005
 *
 */
package matsci.structure;

import matsci.Species;
import matsci.location.basis.*;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class StructureBuilder implements IStructureData {

  private String m_Description = "Unknown Structure";
  private Coordinates[] m_SiteCoords = new Coordinates[0];
  private Species[] m_SiteSpecies = new Species[0];
  //private Vector[] m_LatticeVectors;
  //private Vector[] m_NonPeriodicVectors;
  private Vector[] m_CellVectors;
  private boolean m_IsVectorPeriodic[] = new boolean[] {true, true, true};
  
  /**
   * 
   */
  public StructureBuilder() {
  }
  
  public StructureBuilder(IStructureData structure) {
    this.setDescription(structure.getDescription());
    this.setCellVectors(structure.getCellVectors());
    this.setVectorPeriodicity(structure.getVectorPeriodicity());
    m_SiteCoords = new Coordinates[structure.numDefiningSites()];
    m_SiteSpecies = new Species[m_SiteCoords.length];
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      m_SiteCoords[siteNum] = structure.getSiteCoords(siteNum);
      m_SiteSpecies[siteNum] = structure.getSiteSpecies(siteNum);
    }
  }
  
  public StructureBuilder(IDisorderedStructureData structure) {
    this.setDescription(structure.getDescription());
    this.setCellVectors(structure.getCellVectors());
    this.setVectorPeriodicity(structure.getVectorPeriodicity());
    m_SiteCoords = new Coordinates[structure.numDefiningSites()];
    m_SiteSpecies = new Species[m_SiteCoords.length];
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      m_SiteCoords[siteNum] = structure.getSiteCoords(siteNum);
      if (structure.numAllowedSpecies(siteNum) == 1) {
        m_SiteSpecies[siteNum] = structure.getSiteSpecies(siteNum, 0);
      } else {
    	for (int specNum = 0; specNum < structure.numAllowedSpecies(siteNum); specNum++) {
        	double occupancy = structure.getSiteOccupancy(siteNum, specNum);
        	if (occupancy == 1) {
                m_SiteSpecies[siteNum] = structure.getSiteSpecies(siteNum, specNum);
                break;
        	}
    	}
      }
    }
  }
  
  public StructureBuilder(BravaisLattice lattice) {
    this.setCellVectors(lattice.getCellVectors());
    this.setVectorPeriodicity(lattice.getDimensionPeriodicity());
  }
  
  public void setDescription(String description) {
    m_Description = description;
  }
  
  /*
  public void setLatticeVectors(Vector[] latticeVectors) {
    m_LatticeVectors = (Vector[]) ArrayUtils.copyArray(latticeVectors);
  }
  
  public void setNonPeriodicVectors(Vector[] nonPeriodicVectors) {
    m_NonPeriodicVectors = (Vector[]) ArrayUtils.copyArray(nonPeriodicVectors);
  }*/
  
  public void setCellVectors(Vector[] cellVectors) {
    m_CellVectors = (Vector[]) ArrayUtils.copyArray(cellVectors);
  }
  
  public void setVectorPeriodicity(boolean[] isVectorPeriodic) {
    m_IsVectorPeriodic = ArrayUtils.copyArray(isVectorPeriodic);
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getDescription()
   */
  public String getDescription() {
    return m_Description;
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getLatticeVectors()
   */
  public Vector[] getCellVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_CellVectors);
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getNonPeriodicVectors()
   */
  /*
  public Vector[] getNonPeriodicVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_NonPeriodicVectors);
  }*/
  
  public boolean[] getVectorPeriodicity() {
    return ArrayUtils.copyArray(m_IsVectorPeriodic);
  }
  
  public boolean isVectorPeriodic(int vecNum) {
    return m_IsVectorPeriodic[vecNum];
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#numDefiningSites()
   */
  public int numDefiningSites() {
    return m_SiteCoords.length;
  }
  
  public void addSite(Coordinates coords, Species species) {
    m_SiteCoords = (Coordinates[]) ArrayUtils.appendElement(m_SiteCoords, coords);
    m_SiteSpecies = (Species[]) ArrayUtils.appendElement(m_SiteSpecies, species);
  }
  
  public void addSites(Coordinates[] coords, Species[] species) {
    
    if (species.length != coords.length) {
      throw new RuntimeException("Specie do not correspond to given coordinates.");
    }
    
    m_SiteCoords = (Coordinates[]) ArrayUtils.appendArray(m_SiteCoords, coords);
    m_SiteSpecies = (Species[]) ArrayUtils.appendArray(m_SiteSpecies, species);
  }
  
  public void addSites(Structure.Site[] sites) {
    
    int oldNumSites =m_SiteCoords.length;
    m_SiteCoords = (Coordinates[]) ArrayUtils.growArray(m_SiteCoords, sites.length);
    m_SiteSpecies = (Species[]) ArrayUtils.growArray(m_SiteSpecies, sites.length);
    for (int siteNum = 0; siteNum < sites.length; siteNum++) {
      int siteIndex = oldNumSites + siteNum;
      m_SiteCoords[siteIndex] = sites[siteNum].getCoords();
      m_SiteSpecies[siteIndex] = sites[siteNum].getSpecies();
    }
  }
  
  public void removeSite(int index) {
    m_SiteCoords = (Coordinates[]) ArrayUtils.removeElement(m_SiteCoords, index);
    m_SiteSpecies = (Species[]) ArrayUtils.removeElement(m_SiteSpecies, index);
  }
  
  public void clearSites() {
    this.removeAllSites();
  }
  
  public void removeAllSites() {
    m_SiteCoords = new Coordinates[0];
    m_SiteSpecies = new Species[0];
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getSiteCoords(int)
   */
  public Coordinates getSiteCoords(int index) {
    return m_SiteCoords[index];
  }
  
  public void setSiteCoordinates(Coordinates[] siteCoords) {
    m_SiteCoords = (Coordinates[]) ArrayUtils.copyArray(siteCoords);
  }
  
  public void setSiteCoordinates(int index, Coordinates coords) {
    m_SiteCoords[index] = coords;
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getSiteSpecies(int)
   */
  public Species getSiteSpecies(int index) {
    return m_SiteSpecies[index];
  }
  
  public void setSiteSpecies(int index, Species species) {
    if (m_SiteSpecies.length < m_SiteCoords.length) {
      m_SiteSpecies = (Species[]) ArrayUtils.growArray(m_SiteSpecies, m_SiteCoords.length - m_SiteSpecies.length);
    }
    m_SiteSpecies[index] = species;
  }
  
  public void setSiteSpecies(Species[] siteSpecies) {
    m_SiteSpecies = (Species[]) ArrayUtils.copyArray(siteSpecies);
  }
  
  public Coordinates getAverageDefiningPosition() {
    
    double[] cartCoordArray = new double[m_CellVectors.length];
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      Coordinates siteCoords = m_SiteCoords[siteNum];
      for (int dimNum = 0; dimNum < cartCoordArray.length; dimNum++) {
        cartCoordArray[dimNum] += siteCoords.cartesianCoord(dimNum);
      }
    }
    
    for (int dimNum = 0; dimNum < cartCoordArray.length; dimNum++) {
      cartCoordArray[dimNum] /= m_SiteCoords.length;
    }
    
    return new Coordinates(cartCoordArray, CartesianBasis.getInstance());
    
  }
  
  public void translateToAverageDefiningPosition(Coordinates newAverage) {
    
    Coordinates thisAverage = this.getAverageDefiningPosition();
    Vector translation = new Vector(thisAverage, newAverage);
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      m_SiteCoords[siteNum] = m_SiteCoords[siteNum].translateBy(translation);
    }
    
  }
  
  public void translateBy(Vector translation) {
    
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      m_SiteCoords[siteNum] = m_SiteCoords[siteNum].translateBy(translation);
    }
    
  }

}
