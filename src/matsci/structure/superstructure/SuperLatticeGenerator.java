package matsci.structure.superstructure;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

import matsci.structure.Structure;
import matsci.util.MSMath;
import matsci.util.arrays.*;
import matsci.structure.*;
import matsci.structure.superstructure.filters.ISuperLatticeFilter;
import matsci.structure.superstructure.filters.InverseCompactFilter;
import matsci.structure.superstructure.filters.MostCompactFilter;
import matsci.structure.superstructure.filters.RemoveDuplicateFilter;
import matsci.structure.superstructure.filters.SymmetryPreservingFilter;
import matsci.io.app.log.Status;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.symmetry.operations.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class SuperLatticeGenerator {

  private int m_Size;
  private int m_NumPeriodicDimensions;
  private int[][] m_MatrixDiagonals;
  private DiagonalHead m_OffDiagGeneratorTree;
  private ArrayIndexer[] m_OffDiagGenerators;
  private long m_NumTotalLattices = 0;

  public SuperLatticeGenerator(SuperData superData) {
    this(superData.getSize(), superData.getPrimStructure().numPeriodicDimensions());
  }

  public SuperLatticeGenerator(int size, int numPeriodicDimensions) {
    m_Size = size;
    m_NumPeriodicDimensions = numPeriodicDimensions;
    m_MatrixDiagonals = MSMath.getFactorSets(size, numPeriodicDimensions);
    m_OffDiagGenerators = new ArrayIndexer[m_MatrixDiagonals.length];
    m_OffDiagGeneratorTree = new DiagonalHead();

    for (int diagNum = 0; diagNum < m_MatrixDiagonals.length; diagNum++) {
      int[] diagonals = m_MatrixDiagonals[diagNum];
      int[] numAllowedValues = new int[(numPeriodicDimensions * (numPeriodicDimensions - 1))/2];
      int cellNum = 0;
      for (int row = 0; row < numPeriodicDimensions; row++) {
        for (int col = 0; col < row; col++) {
          numAllowedValues[cellNum++] = diagonals[col];
        }
      }
      ArrayIndexer offDiagGenerator = new ArrayIndexer(numAllowedValues);
      m_NumTotalLattices += offDiagGenerator.numAllowedStates();
      m_OffDiagGenerators[diagNum] = offDiagGenerator;
      m_OffDiagGeneratorTree.put(diagonals, 0, offDiagGenerator);
    }
    m_OffDiagGeneratorTree.setBase();
    /*if (m_NumTotalLattices > Integer.MAX_VALUE) {
      Status.warning("Calculated total number of superlattices is greater than the maximum int value.  This can occur when 3-dimensional superlattices of size 19800 or greater are requested.  Current size is " + size + ".");
    }*/
  }
  
  public long numTotalLatticesLongValue() {
    return m_NumTotalLattices;
  }

  public int numTotalLattices() {
    if (m_NumTotalLattices > Integer.MAX_VALUE) {
      Status.error("The number of total lattices (" + m_NumTotalLattices + ") is greater than the maximum allowed value (" + Integer.MAX_VALUE +").");
    }
    return (int) m_NumTotalLattices;
  }
  
  public int getSize() {
  	return m_Size;
  }
  
  public int numPeriodicDimensions() {
    return m_NumPeriodicDimensions;
  }
  
  public int numDiagonalSuperToDirects() {
    return m_MatrixDiagonals.length;
  }
  
  /**
   * The latticeIndex and superToDirect should match.
   * 
   * @param latticeIndex
   * @param superToDirect
   * @return
   */
  private int getNextDiagonalLatticeIndex(int latticeIndex, int[][] superToDirect) {
    
    int[] diagonal = new int[superToDirect.length];
    int numSuperToDirects = 1;
    for (int dimNum = 0; dimNum < diagonal.length; dimNum++) {
      diagonal[dimNum] = superToDirect[dimNum][dimNum];
      for (int prevDimNum = diagonal.length - 1; prevDimNum > dimNum; prevDimNum--) {
        numSuperToDirects *= diagonal[dimNum];
      }
    }
    int base = m_OffDiagGeneratorTree.get(diagonal).m_Base;
    return base + numSuperToDirects;
    
  }
  
  public int[][][] generateDiagonalSuperLattices(ISuperLatticeFilter[] filters) {

    if (m_NumPeriodicDimensions == 0) {
      return new int[1][0][0];
    }
    
    ArrayList superToDirects = new ArrayList();

    int latticeIndex = 0;
    do {
      int oldLatticeIndex = latticeIndex;
      
      int[][] superToDirect = this.getSuperToDirect(latticeIndex);
      for (int filterNum = 0; filterNum < filters.length; filterNum++) {
        ISuperLatticeFilter filter = filters[filterNum];
        latticeIndex = filter.screenLatticeIndex(latticeIndex, superToDirect);
        if (latticeIndex != oldLatticeIndex) {break;}
      }
      
      if (latticeIndex == oldLatticeIndex) {
        superToDirects.add(superToDirect); 
      }
      
      int nextDiagonalLatticeIndex = this.getNextDiagonalLatticeIndex(oldLatticeIndex, superToDirect);
      while ((nextDiagonalLatticeIndex < this.numTotalLattices()) && (nextDiagonalLatticeIndex < latticeIndex)) {
        int[][] nextSuperToDirect = this.getSuperToDirect(nextDiagonalLatticeIndex);
        nextDiagonalLatticeIndex = this.getNextDiagonalLatticeIndex(nextDiagonalLatticeIndex, nextSuperToDirect);
      }
      
      latticeIndex = nextDiagonalLatticeIndex;
    } while (latticeIndex < this.numTotalLattices());
    
    //return ArrayUtils.truncateArray(superToDirects, totalCellNum);
    return (int[][][]) superToDirects.toArray(new int[0][][]);

  }
  
  public int[][][] generateSuperLattices(ISuperLatticeFilter[] filters) {

    if (m_NumPeriodicDimensions == 0) {
      return new int[1][0][0];
    }
    
    ArrayList superToDirects = new ArrayList();
    int[][] template = new int[m_NumPeriodicDimensions][m_NumPeriodicDimensions];
    
    int latticeIndex = 0;
    do {
      int oldLatticeIndex = latticeIndex;
      
      int[][] superToDirect = this.getSuperToDirect(latticeIndex, template);
      for (int filterNum = 0; filterNum < filters.length; filterNum++) {
        ISuperLatticeFilter filter = filters[filterNum];
        latticeIndex = filter.screenLatticeIndex(latticeIndex, superToDirect);
        if (latticeIndex != oldLatticeIndex) {break;}
      }
      
      if (latticeIndex != oldLatticeIndex) {continue;}

      superToDirects.add(ArrayUtils.copyArray(superToDirect)); 
      latticeIndex++;
    } while (latticeIndex < this.numTotalLattices());
    
    //return ArrayUtils.truncateArray(superToDirects, totalCellNum);
    return (int[][][]) superToDirects.toArray(new int[0][][]);

  }
  
  public int[][][] generateSymmetryPreservingSuperLattices(SpaceGroup spaceGroup) {
    
    ISuperLatticeFilter[] filters = new ISuperLatticeFilter[] {
        new SymmetryPreservingFilter(spaceGroup)
    };
    return this.generateSuperLattices(filters);
    
  }
  
  public int[][] getDiagonalSuperToDirect(int diagonalNumber) {
    int[][] returnArray = new int[m_NumPeriodicDimensions][m_NumPeriodicDimensions];
    return this.getDiagonalSuperToDirect(diagonalNumber, returnArray);
  }
  
  public int[][] getDiagonalSuperToDirect(int diagonalNumber, int[][] returnArray) {
    int numPeriodicDimensions = m_NumPeriodicDimensions;
    if (returnArray == null) {
      returnArray = new int[numPeriodicDimensions][numPeriodicDimensions];
    }
    if (numPeriodicDimensions == 0) {return returnArray;}
    
    if (diagonalNumber >= m_MatrixDiagonals.length || diagonalNumber < 0) {
      throw new RuntimeException("No structure corresponds to the given diagonal number: " + diagonalNumber);
    }
    int[] diagonal = m_MatrixDiagonals[diagonalNumber];
    for (int dimNum = 0; dimNum < numPeriodicDimensions; dimNum++) {
      returnArray[dimNum][dimNum] = diagonal[dimNum];
    }

    return returnArray;
  }
  
  public int getLatticeIndexForNextDiagonalMatrix(int[][] superToDirect) {
    
    superToDirect = ArrayUtils.copyArray(superToDirect);
    for (int rowNum = 1; rowNum < superToDirect.length; rowNum++) {
      for (int colNum = 0; colNum < rowNum; colNum++) {
        superToDirect[rowNum][colNum] = superToDirect[colNum][colNum] - 1;
      }
    }
    return this.getFastLatticeIndex(superToDirect) + 1;
    
  }
  
  public int[][] getSuperToDirect(int latticeIndex) {
    int[][] returnArray = new int[m_NumPeriodicDimensions][m_NumPeriodicDimensions];
    return this.getSuperToDirect(latticeIndex, returnArray);
  }
  
  public int[][] getSuperToDirect(int latticeIndex, int[][] returnArray) {
    
    int numPeriodicDimensions = m_NumPeriodicDimensions;
    if (returnArray == null) {
      returnArray = new int[numPeriodicDimensions][numPeriodicDimensions];
    }
    if (numPeriodicDimensions == 0) {return returnArray;}
    
    if (latticeIndex >= m_NumTotalLattices || latticeIndex < 0) {
      throw new RuntimeException("No structure corresponds to the given lattice index: " + latticeIndex);
    }

    DiagonalLeaf leafNode = m_OffDiagGeneratorTree.get(latticeIndex);
    int[] diagElements = leafNode.m_Diagonal;
    int[] offDiagElements = leafNode.m_ArrayIndexer.splitValue(latticeIndex - leafNode.m_Base);
    int cellNum = 0;
    for (int row = 0; row < numPeriodicDimensions; row++) {
      returnArray[row][row] = diagElements[row];
      for (int col = 0; col < row; col++) {
        returnArray[row][col] = offDiagElements[cellNum++];
      }
    }

    return returnArray;
  }
  
  public int[][] getMostCompactSuperToDirect(BravaisLattice lattice) {
    
    return this.getMostCompactSuperToDirect(new SpaceGroup(new SymmetryOperation[0], lattice));

  }
  
  public int[][] getInvCompactSuperToDirect(BravaisLattice lattice) {
    
    return this.getInvCompactSuperToDirect(new SpaceGroup(new SymmetryOperation[0], lattice));

  }
  
  public int[][] getInvCompactSuperToDirect(SpaceGroup spaceGroup) {

    InverseCompactFilter filter = new InverseCompactFilter(spaceGroup.getLattice(), true); 
    ISuperLatticeFilter[] filters = new ISuperLatticeFilter[] {
        new RemoveDuplicateFilter(this, spaceGroup),
        filter
    };
    this.generateSuperLattices(filters);
    return filter.getBestKnownLattice().getSuperToDirect();
    
  }
  
  public int[][] getMostCompactSuperToDirect(SpaceGroup spaceGroup) {
    MostCompactFilter filter = new MostCompactFilter(spaceGroup.getLattice(), true); 
    ISuperLatticeFilter[] filters = new ISuperLatticeFilter[] {
        new RemoveDuplicateFilter(this, spaceGroup),
        filter
    };
    this.generateSuperLattices(filters);
    return filter.getBestKnownLattice().getSuperToDirect();
  }
  
  /*public int[][] getMostCompactSuperToDirect(BravaisLattice lattice, int[][][] superLattices) { 
    //System.out.println("Size: " + this.getSize());
    
    if (superLattices.length == 1) {
      return lattice.getSuperToDirect(new SuperLattice(lattice, superLattices[0]).getCompactLattice());
    }
    
    double maxMinDistance = 0;
    int[][] returnArray = null;
    for (int latticeNum = 0; latticeNum < superLattices.length; latticeNum++) {
      int[][] superToDirect = superLattices[latticeNum];
      BravaisLattice superLattice = new SuperLattice(lattice, superToDirect).getCompactLattice();
      double minPointDistance = superLattice.getMinPeriodicDistance();
      if (minPointDistance > maxMinDistance) {
        maxMinDistance = minPointDistance;
        returnArray = lattice.getSuperToDirect(superLattice);
      } 
    }
    
    return returnArray;
    
  }*/
  
  public int getLatticeIndex(int[][] superToDirect) {
    return (this.getFastLatticeIndex(ArrayUtils.copyArray(superToDirect)));
  }
 
  /**
   * Warning -- this method changes the given superToDirect matrix to canonical form!
   * @param superToDirect
   * @return
   */
  public int getFastLatticeIndex(int[][] superToDirect) {
    if (superToDirect.length == 0) {return 0;}
    int[][] canonicalMatrix = superToDirect;
    toCanonicalFast(canonicalMatrix);
    int[] diagonal = new int[canonicalMatrix.length];
    for (int dimNum = 0; dimNum < diagonal.length; dimNum++) {
      diagonal[dimNum] = canonicalMatrix[dimNum][dimNum];
    }
    
    DiagonalLeaf node = m_OffDiagGeneratorTree.get(diagonal);
    ArrayIndexer indexer = node.m_ArrayIndexer;
    
    int cellNum = 0;
    int[] offDiagElements = new int[indexer.numDimensions()];
    for (int row = 0; row < m_NumPeriodicDimensions; row++) {
      for (int col = 0; col < row; col++) {
        offDiagElements[cellNum++] = canonicalMatrix[row][col];
      }
    }

    return indexer.joinValuesBounded(offDiagElements) + node.m_Base;
  }
  
  public static int getSize(int[][] superToDirect) {
    return Math.abs(MSMath.determinant(superToDirect));
  }
  
  public static int[][] toCanonical(int[][] superToDirect) {
    int[][] returnArray = ArrayUtils.copyArray(superToDirect);
    toCanonicalFast(returnArray);
    return returnArray;
  }
 
  /**
   * This reduces the matrix to lower-triangular Hermite Normal form.  The algorithm
   * is simple -- start from the last column and perform elementary row operations to 
   * get the necessary zeroes.  Then do this for the rest of the columns in reverse
   * order.  Once you have a lower triangular matrix, use modulo for the off-diagonal elements.
   */
  public static void toCanonicalFast(int[][] superToDirect) {
    
    //int[][] returnArray = ArrayUtils.copyArray(superToDirect);
    int[][] returnArray = superToDirect;
    if (returnArray.length == 0) {return;}
    
    // First we make it lower triangular using elementary row operations
    for (int colNum = returnArray[0].length - 1; colNum > 0; colNum--) {
      
      // Find the row with the minimum value in this column
      int minValue = Integer.MAX_VALUE;
      int minRowNum = -1;
      for (int rowNum = 0; rowNum <= colNum; rowNum++) {
        int rowValue = Math.abs(returnArray[rowNum][colNum]);
        if (rowValue < minValue && rowValue != 0) {
          minRowNum = rowNum;
          minValue = rowValue;
        }
      }
      int[] minRow = returnArray[minRowNum];
      
      // Swap the minRow so it is the new "last" row
      returnArray[minRowNum] = returnArray[colNum];
      returnArray[colNum] = minRow;
      
      // Make sure the diagonal element on the found row is positive
      if (minRow[colNum] < 0) {
        for (int prevColNum = 0; prevColNum <= colNum; prevColNum++) {
          minRow[prevColNum] *= -1;
        }
      }
      
      // Now keep getting the remainder between the minimum row and other rows 
      // and calling that new remainder the new minimum.  Do this until all remainders
      // are zero
      boolean allZeroes = true;
      do {
        allZeroes = true;
        for (int rowNum = 0; rowNum < colNum; rowNum++) {
          int[] row = returnArray[rowNum];
          int quotient = MSMath.divFloor(row[colNum], minValue); 
          int remainder = row[colNum] - (minValue * quotient);
          if (remainder != 0) {
            for (int prevColNum = 0; prevColNum <= colNum; prevColNum++) {
              row[prevColNum] -= minRow[prevColNum] * quotient;
            }
            returnArray[rowNum] = minRow;
            returnArray[colNum] = row;
            minRow = row;
            minValue = minRow[colNum];
            allZeroes = false;
            break;
          }
        }
      } while (!allZeroes);
      
      // Now we subtract multiples of the minimum row from all of the previous rows
      for (int rowNum = 0; rowNum < colNum; rowNum++) {
        int[] row = returnArray[rowNum];
        int quotient = MSMath.divFloor(row[colNum], minValue);
        for (int prevColNum = 0; prevColNum <= colNum; prevColNum++) {
          row[prevColNum] -= minRow[prevColNum] * quotient;
        }
      }
    }
    
    // We've made all of the diagonal elements positive except one...
    if (returnArray[0][0] < 0) {
      returnArray[0][0] *= -1;
    }
    
    // Now we take the modulo of the off-diagonal elements
    try {
      for (int rowNum = returnArray.length - 2; rowNum >= 0; rowNum--) {
        int[] row = returnArray[rowNum];
        int diagonal = row[rowNum];
        for (int nextRowNum = rowNum+1; nextRowNum < returnArray.length; nextRowNum++) {
          int[] nextRow = returnArray[nextRowNum];
          int quotient = MSMath.divFloor(nextRow[rowNum], diagonal);
          for (int prevCol = 0; prevCol <= rowNum; prevCol++) {
            nextRow[prevCol] -= row[prevCol] * quotient;
          }
        }
      }
    } catch (ArithmeticException e) {
      throw new RuntimeException("Error finding Hermite Normal Form:  Matrix is singular.", e);
    }
  }
  
  public static SymmetryOperation getOperationToMinIndex(int[][] superToDirect, SpaceGroup spaceGroup) {
    SymmetryOperation minOp = null;
    int minIndex= Integer.MAX_VALUE;
    int dim = superToDirect.length;
    SuperLatticeGenerator generator = new SuperLatticeGenerator(getSize(superToDirect), dim);
    
    int[][] oppedArray = new int[dim][dim];
    for (int opNum= 0; opNum < spaceGroup.numOperations(); opNum++) {
      int[][] pointOperator = spaceGroup.getLatticePointOperator(opNum);
      if (pointOperator == null) {continue;}
      SuperLatticeGenerator.operateSuperToDirect(superToDirect, pointOperator, oppedArray);
      int oppedIndex = generator.getFastLatticeIndex(oppedArray);
      if (oppedIndex < minIndex) {
        minIndex =oppedIndex;
        minOp = spaceGroup.getOperation(opNum);
      }
    }
    return minOp;
  }
  
  public int[][][] generateSuperLattices() {
    return this.generateSuperLattices(new ISuperLatticeFilter[0]);
  }
  
  public int[][][] getVectorPermutations() {
    
    // TODO create a version that uses the space group to eliminate permutations that are equivalent to isometric transformations
    
    int[] arraySizes = new int[m_NumPeriodicDimensions];
    Arrays.fill(arraySizes, 2);
    
    ArrayIndexer indexer = new ArrayIndexer(arraySizes);
    int[][] permutations = MSMath.getPermutations(m_NumPeriodicDimensions);
    
    int[][][] returnArray = new int[indexer.numAllowedStates() * permutations.length][][];
    
    int[] currentState = indexer.getInitialState();
    int returnIndex = 0;
    do {
      for (int permIndex = 0; permIndex < permutations.length; permIndex++) {
        int[][] array = new int[m_NumPeriodicDimensions][m_NumPeriodicDimensions];
        int[] permutation = permutations[permIndex];
        for (int dimNum = 0; dimNum < array.length; dimNum++) {
          array[permutation[dimNum]][dimNum] = 1 - currentState[dimNum] * 2; // Ensure 1 1 1 is first
        }
        returnArray[returnIndex++] = array;
      }
    } while (indexer.increment(currentState));
    
    return returnArray;
  }
  
  public int countSuperLattices(int[][][] pointOperators) {

    if (m_NumPeriodicDimensions == 0) {
      return 1;
    }
    
    if (pointOperators == null) {
      pointOperators = new int[0][][];
    }

    int[][] oppedSuperToDirect = new int[m_NumPeriodicDimensions][m_NumPeriodicDimensions];
    int numLattices = 0;
    for (int latticeIndex = 0; latticeIndex < this.numTotalLattices(); latticeIndex++) {
      int[][] superToDirect = this.getSuperToDirect(latticeIndex);
      boolean match = false;
      for (int opNum = 0; opNum < pointOperators.length; opNum++) {
        int[][] pointOperator = pointOperators[opNum];
        operateSuperToDirect(superToDirect, pointOperator, oppedSuperToDirect);
        int oppedIndex = this.getFastLatticeIndex(oppedSuperToDirect);
        if (oppedIndex < latticeIndex) {
          match = true;
          break;
        }
      }
      if (match) continue;
      numLattices++;
    }
    return numLattices;
  }
  
  public int[][][] generateSuperLattices(int[][][] pointOperators) {

    if (m_NumPeriodicDimensions == 0) {
      return new int[1][0][0];
    }
    
    if (pointOperators == null) {
      pointOperators = new int[0][][];
    }

    int numOperators = pointOperators.length == 0 ? 1 : pointOperators.length; // There is always an identity operator
    ArrayList superToDirects = new ArrayList(this.numTotalLattices() / numOperators);
    //int[][][] superToDirects = new int[numPotentialCells][][];
    int[][] oppedSuperToDirect = new int[m_NumPeriodicDimensions][m_NumPeriodicDimensions];
    int totalCellNum = 0;
    for (int latticeIndex = 0; latticeIndex < this.numTotalLattices(); latticeIndex++) {
      int[][] superToDirect = this.getSuperToDirect(latticeIndex);
      boolean match = false;
      for (int opNum = 0; opNum < pointOperators.length; opNum++) {
        int[][] pointOperator = pointOperators[opNum];
        operateSuperToDirect(superToDirect, pointOperator, oppedSuperToDirect);
        int oppedIndex = this.getFastLatticeIndex(oppedSuperToDirect);
        if (oppedIndex < latticeIndex) {
          match = true;
          break;
        }
      }
      if (match) continue;
      //superToDirects[totalCellNum] = superToDirect;
      superToDirects.add(superToDirect);
      totalCellNum++;
    }
    //return ArrayUtils.truncateArray(superToDirects, totalCellNum);
    return (int[][][]) superToDirects.toArray(new int[0][][]);
  }
  
  public int[][][] generateSymmetryPreservingDiagonalLattices(SpaceGroup spaceGroup) {

    int[][][] pointOperators = spaceGroup.getLatticePointOperators(true);
    return this.generateSymmetryPreservingDiagonalLattices(pointOperators);
    
  }
  
  public int[][][] generateSymmetryPreservingDiagonalLattices(int[][][] pointOperators) {
    
    int[][][] returnArray = new int[m_MatrixDiagonals.length][][];
    int[][] oppedSuperToDirect = new int[m_NumPeriodicDimensions][m_NumPeriodicDimensions];
    int totalCellNum = 0;
    
    for (int diagNum = 0; diagNum < m_MatrixDiagonals.length; diagNum++) {
      int[][] superToDirect = this.getDiagonalSuperToDirect(diagNum);
      int latticeIndex = this.getFastLatticeIndex(superToDirect);
      boolean match = true;
      for (int opNum = 0; opNum < pointOperators.length; opNum++) {
        int[][] pointOperator = pointOperators[opNum];
        operateSuperToDirect(superToDirect, pointOperator, oppedSuperToDirect);
        int oppedIndex = this.getFastLatticeIndex(oppedSuperToDirect);
        if (oppedIndex != latticeIndex) {
          match = false;
          break;
        }
      }
      if (!match) continue;
      returnArray[totalCellNum] = superToDirect;
      totalCellNum++;
    }
    
    return ArrayUtils.truncateArray(returnArray, totalCellNum);
    
  }
  
  public int countSuperLattices(SpaceGroup spaceGroup, boolean removeDuplicates) {

    int[][][] pointOperators = new int[0][][];
    if (removeDuplicates) {
      pointOperators = spaceGroup.getLatticePointOperators(true);
    }
    
    return this.countSuperLattices(pointOperators);
  }
  
  public int[][][] generateSuperLattices(SpaceGroup spaceGroup, boolean removeDuplicates) {
    
    ISuperLatticeFilter[] filters = new ISuperLatticeFilter[0];
    if (removeDuplicates) {
      filters = new ISuperLatticeFilter[] {
         new RemoveDuplicateFilter(this, spaceGroup),
      };
    } 
    return this.generateSuperLattices(filters);
    
  }
  
  public boolean equivalentToPriorIndex(int[][] superToDirect, int[][][] pointOperators, int[][] returnArray) {
    int dim = superToDirect.length;
    returnArray = (returnArray == null) ? new int[dim][dim] : returnArray;
    int latticeIndex = this.getLatticeIndex(superToDirect);
    
    for (int opNum = 0; opNum < pointOperators.length; opNum++) {
      int[][] pointOperator = pointOperators[opNum];
      operateSuperToDirect(superToDirect, pointOperator, returnArray);
      int oppedIndex = this.getFastLatticeIndex(returnArray);
      if (oppedIndex < latticeIndex) {return true;}
    }
    return false;
  }
  
  public boolean equivalentLattices(int[][] superToDirect1, int[][] superToDirect2, int[][][] pointOperators) {
    
    int latticeIndex = this.getLatticeIndex(superToDirect1);
    
    for (int opNum = 0; opNum < pointOperators.length; opNum++) {
      int[][] pointOperator = pointOperators[opNum];
      int[][] oppedArray = operateSuperToDirect(superToDirect2, pointOperator, null);
      int oppedIndex = this.getFastLatticeIndex(oppedArray);
      if (oppedIndex == latticeIndex) {return true;}
    }
    
    return false;
  }
  
  public int getMultiplicity(int[][] superToDirect, int[][][] pointOperators, int[][] returnArray) {
    int dim = superToDirect.length;
    returnArray = (returnArray == null) ? new int[dim][dim] : returnArray;
    int latticeIndex = this.getLatticeIndex(superToDirect);
    
    int numSelfMaps = 0;
    for (int opNum = 0; opNum < pointOperators.length; opNum++) {
      int[][] pointOperator = pointOperators[opNum];
      operateSuperToDirect(superToDirect, pointOperator, returnArray);
      int oppedIndex = this.getFastLatticeIndex(returnArray);
      if (oppedIndex == latticeIndex) {numSelfMaps++;}
    }
    return pointOperators.length / numSelfMaps;
  }
  
  public static int[][] operateSuperToDirect(int[][] superToDirect, int[][] pointOperator, int[][] returnArray) {
    int dim = superToDirect.length;
    returnArray = (returnArray == null) ? new int[dim][dim] : returnArray;
    for (int rowNum = 0; rowNum < returnArray.length; rowNum++) {
      int[] row = superToDirect[rowNum];
      int[] oppedRow = returnArray[rowNum];
      for (int colNum = 0; colNum < row.length; colNum++) {
        int value = 0;
        for (int dimNum = 0; dimNum < row.length; dimNum++) {
          value += pointOperator[dimNum][colNum] * row[dimNum];
        }
        oppedRow[colNum] = value;
      }
    }
    return returnArray;
  }
  
  protected class DiagonalNode {
    
    private int[] m_BranchKeys = new int[0];
    private int[] m_StatesPerBranch = new int[0];
    private DiagonalNode[] m_Children = new DiagonalNode[0];
    
    protected void put(int[] diagonal, int currLevel, ArrayIndexer indexer) {
      
      int nextValue = diagonal[currLevel];
      int childIndex = Arrays.binarySearch(m_BranchKeys, nextValue);
      if (childIndex < 0) {
        m_BranchKeys = ArrayUtils.appendElement(m_BranchKeys, nextValue);
        Arrays.sort(m_BranchKeys);
        childIndex = Arrays.binarySearch(m_BranchKeys, nextValue);
        DiagonalNode newNode = (currLevel == diagonal.length -1) ? new DiagonalLeaf() : new DiagonalNode();
        m_Children = (DiagonalNode[]) ArrayUtils.insertElement(m_Children, childIndex, newNode);
        m_StatesPerBranch = ArrayUtils.insertElement(m_StatesPerBranch, childIndex, 0);
      }
      m_StatesPerBranch[childIndex] += indexer.numAllowedStates();
      m_Children[childIndex].put(diagonal, currLevel + 1, indexer);
    }
    
    protected DiagonalLeaf get(int[] diagonal, int currLevel) { 
      int index = Arrays.binarySearch(m_BranchKeys, diagonal[currLevel]);
      try {
        return m_Children[index].get(diagonal, currLevel + 1);
      } catch (ArrayIndexOutOfBoundsException e) {
        return null;
      }
    }
    
    public DiagonalLeaf get(int index) {
      for (int branchNum = 0; branchNum < m_StatesPerBranch.length; branchNum++) {
        int numStates = m_StatesPerBranch[branchNum];
        if (index < numStates) {
          return m_Children[branchNum].get(index);
        }
        index -= numStates;
      }
      return null;
    }
    
    protected void setBase(int base) {
      for (int branchNum = 0; branchNum < m_Children.length; branchNum++) {
        m_Children[branchNum].setBase(base);
        base += m_StatesPerBranch[branchNum];
      }
    }
    
  }
  
  protected class DiagonalHead extends DiagonalNode {
    public void put(int[] diagonal, ArrayIndexer item) {
      this.put(diagonal, 0, item);
    }
    
    public DiagonalLeaf get(int[] diagonal) {
      return this.get(diagonal, 0);
    }
    
    public void setBase() {
      this.setBase(0);
    }
  }
  
  protected class DiagonalLeaf extends DiagonalNode {
    
    private int[] m_Diagonal;
    private ArrayIndexer m_ArrayIndexer;
    private int m_Base;
    
    protected DiagonalLeaf get(int[] diagonal, int currLevel) { 
      return this;
    }
    
    public DiagonalLeaf get(int index) {
      return this;
    }
    
    protected void put(int[] diagonal, int currLevel, ArrayIndexer indexer) {
      m_Diagonal = diagonal;
      m_ArrayIndexer = indexer;
    }
    
    protected void setBase(int base) {
      m_Base = base;
    }
  }
}
