package matsci.structure.superstructure;

import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.LinearBasis;
import matsci.location.symmetry.operations.*;
import matsci.structure.BravaisLattice;
import matsci.structure.IStructureData;
import matsci.structure.Structure;
import matsci.structure.Structure.Site;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayIndexer;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class SuperStructure extends Structure {

  private final Structure m_ParentStructure;
  private final SuperLattice m_SuperLattice;
  //private final int m_NumPrimCells;

  //private final int[][] m_SuperToDirect;
  //private int[][] m_CanonicalToDirect;
  //private int m_LatticeIndex;

  private final ArrayIndexer m_IntCoordConverter;
  //private ArrayIndexer m_PrimCellIterator;

  protected SuperStructure(SuperStructure sourceStructure) {
    super(sourceStructure);
    m_ParentStructure = sourceStructure.m_ParentStructure;
    m_SuperLattice = sourceStructure.m_SuperLattice;
    //m_NumPrimCells = sourceStructure.m_NumPrimCells;
    //m_SuperToDirect = sourceStructure.m_SuperToDirect;
    //m_CanonicalToDirect= sourceStructure.m_CanonicalToDirect;
    //m_LatticeIndex = sourceStructure.m_LatticeIndex;
    m_IntCoordConverter = sourceStructure.m_IntCoordConverter;
    //m_PrimCellIterator = sourceStructure.m_PrimCellIterator;
  }
  
  public SuperStructure(IStructureData structureData, int[][] superToDirect) {
    
    this(new Structure(structureData), superToDirect);
    
  }
  
  public SuperStructure(SuperData superData) {
    this(superData.getPrimStructure(),
    new SuperLatticeGenerator(superData).getSuperToDirect(superData.getLatticeNumber()));
  }
  
  public SuperStructure(IStructureData baseStructure, double minVectorLength) {
    this(new Structure(baseStructure), minVectorLength);
  }
  
  public SuperStructure(Structure baseStructure, double minVectorLength) {
    this(baseStructure, baseStructure.getDefiningLattice().getMinimalSuperToDirect(minVectorLength));
    //this(baseStructure, getMostCompactSuperToDirect(baseStructure, minVectorLength));
  }
 
  public SuperStructure(Structure baseStructure, int numPrimCells) {
    this(baseStructure, baseStructure.getDefiningLattice().getCompactSuperToDirect(numPrimCells));
  }

  public SuperStructure(IStructureData baseStructure, int numPrimCells) {
    this(new Structure(baseStructure), numPrimCells);
  }

  /**
   *
   * @param parentStructure The parent structure for this structure
   * @param superToDirect A n-dimensional matrix that expresses a representative supercell in terms of the
   * representative cell of the parent structure.  n is the periodic dimensionality of the parent structure.
   */
  public SuperStructure(Structure parentStructure, int[][] superToDirect) {

    super(new SuperLattice(parentStructure.getDefiningLattice(), superToDirect));
    //super(parentStructure.getDefiningLattice().getSuperLattice(superToDirect));

    m_ParentStructure = parentStructure;
    //m_SuperLattice = new SuperLattice(parentStructure.getDefiningLattice(), superToDirect); // Doesn't take into account the fact that parent structure might be a superstructure
    m_SuperLattice = (SuperLattice) this.getDefiningLattice(); // Doesn't take into account the fact that parent structure might be a superstructure
    m_Description = parentStructure.getDescription();

    /**
     * These are the vectors, in direct coordinates, that define this lattice.
     */
    //m_SuperToDirect = superToDirect;

    /**
     * The volume of this supercell in units of the volume of the primitive cell
     */
    //m_NumPrimCells = Math.abs(MSMath.determinant(superToDirect));

    /**
     * The canonical basis is equivalent to the given basis.  It is comprised of three vectors expressed in terms
     * of the primitive basis.  The vectors form a lower triangular matrix such that [0][0] > [1][0], [2][0];  [1][1] > [2][1];
     */
    //int numPeriodicDimensions = parentStructure.numPeriodicDimensions();
    //m_CanonicalToDirect = new int[numPeriodicDimensions][numPeriodicDimensions];
    //this.createCanonicalLattice();
    int[] intCoordDimensions = ArrayUtils.appendElement(m_SuperLattice.getDimensionSizes(), m_ParentStructure.numDefiningSites());
    m_IntCoordConverter = new ArrayIndexer(intCoordDimensions);
    
    /**
     * All of the sites are stored in one array.  It is important to create the canoncial basis before filling
     * this array, as the canonical basis is used to calculate the array index for each site.
     */
    m_DefiningSites = new DefiningSite[m_SuperLattice.numPrimCells() * parentStructure.numDefiningSites()];
    this.addSites();
  }
  
  public Structure copy() {
    return new SuperStructure(this);
  }
/*
  private void createCanonicalLattice() {
    LinearBasis directBasis = m_ParentStructure.getLattice().getLatticeBasis();
    BravaisLattice superLattice = this.getLattice();
    Coordinates superOrigin = superLattice.getOrigin();

    int xwidth = 0;
    int ywidth = 0;

    Vector[] canonicalVectors = new Vector[superLattice.numDimensions()];
    int[] primCellLocations = new int[canonicalVectors.length];
    int[] intCoordDimensions = new int[canonicalVectors.length + 1];
    intCoordDimensions[intCoordDimensions.length - 1] = m_ParentStructure.numDefiningSites();

    //Get the canonical basis vector parallel to the x axis
    if (canonicalVectors.length > 0) {
      for (int x = 1; x <= m_NumPrimCells; x++) {
        Coordinates directCoords = new Coordinates(new double[] {x, 0, 0}, directBasis);
        Coordinates superRemainder = superLattice.softRemainder(directCoords);
        if (superOrigin.isCloseEnoughTo(superRemainder)) {
          m_CanonicalToDirect[0][0] = x;
          intCoordDimensions[0] = x;
          primCellLocations[0] = x;
          canonicalVectors[0] = new Vector(directCoords.getArrayCopy(), directBasis);
          xwidth = x;
          x = m_NumPrimCells;
        }
      }
    }

    //Get the canonical basis vector in the x-y plane
    if (canonicalVectors.length > 1) {
      for (int y = 1; y <= (m_NumPrimCells / xwidth); y++) {
        for (int x = 0; x < xwidth; x++) {
          Coordinates directCoords = new Coordinates(new double[] {x, y, 0}, directBasis);
          Coordinates superRemainder = superLattice.softRemainder(directCoords);
          if (superOrigin.isCloseEnoughTo(superRemainder)) {
            m_CanonicalToDirect[1][0] = x;
            m_CanonicalToDirect[1][1] = y;
            intCoordDimensions[1] = y;
            primCellLocations[1] = y;
            canonicalVectors[1] = new Vector(directCoords.getArrayCopy(), directBasis);
            ywidth = y;
            y = (m_NumPrimCells / xwidth);
            x = xwidth;
          }
        }
      }
    }

    //Get the canonical basis vector with a z component.
    if (canonicalVectors.length > 2) {
      int z = m_NumPrimCells / (xwidth * ywidth); // This should always be an integer
      for (int x = 0; x < xwidth; x++) {
        for (int y = 0; y < ywidth; y++) {
          Coordinates directCoords = new Coordinates(new double[] {x, y, z}, directBasis);
          Coordinates superRemainder = superLattice.softRemainder(directCoords);
          if (superOrigin.isCloseEnoughTo(superRemainder)) {
            m_CanonicalToDirect[2][0] = x;
            m_CanonicalToDirect[2][1] = y;
            m_CanonicalToDirect[2][2] = z;
            intCoordDimensions[2] = z;
            primCellLocations[2] = z;
            canonicalVectors[2] = new Vector(directCoords.getArrayCopy(), directBasis);
            x = xwidth;
            y = ywidth;
          }
        }
      }
    }
    
    //Create a basis object representing the canonical basis
    m_CanonicalToDirect = SuperLatticeGenerator.toCanonical(m_SuperToDirect);
    int size = SuperLatticeGenerator.getSize(m_CanonicalToDirect);
    SuperLatticeGenerator generator = new SuperLatticeGenerator(size, m_CanonicalToDirect.length);
    m_LatticeIndex = generator.getLatticeIndex(m_CanonicalToDirect);
    int[] intCoordDimensions = new int[m_CanonicalToDirect.length + 1];
    int[] primCellLocations = new int[m_CanonicalToDirect.length];
    
    intCoordDimensions[intCoordDimensions.length - 1] = m_ParentStructure.numDefiningSites();
    for (int dimNum = 0; dimNum < primCellLocations.length; dimNum++) {
      int diagValue = m_CanonicalToDirect[dimNum][dimNum];
      intCoordDimensions[dimNum] = diagValue;
      primCellLocations[dimNum] = diagValue;
    }
    m_IntCoordConverter = new ArrayIndexer(intCoordDimensions);
    m_PrimCellIterator = new ArrayIndexer(primCellLocations);
  }*/

  /**
   * Called during construction, uses the canonical basis an internal array of sites in this cell.
   */
  private void addSites() {

    int numPrimSites = m_ParentStructure.numDefiningSites();

    AbstractBasis primBasis = m_ParentStructure.getDefiningLattice().getLatticeBasis();

    /**
     * We cycle through all sites in the prism that is the canonical cell
     * We then take each site and map it to the corresponding site in the cell
     * defined by the vectors used to create this supercell.
     */
    int[] periodicIndices = this.getDefiningLattice().getPeriodicIndices();
    for (int primIndex = 0; primIndex < numPrimSites; primIndex++) {
      Structure.Site primSite = m_ParentStructure.getDefiningSite(primIndex);
      double[] coordArray =  primSite.getCoords().getCoordinates(primBasis).getArrayCopy();
      for (int imageNum = 0; imageNum < this.numPrimCells(); imageNum++) {
        int[] primCellLocation = m_SuperLattice.getPrimCellOrigin(imageNum);
        //int[] primCellLocation = m_PrimCellIterator.splitValue(imageNum);
        double[] newCoordArray = ArrayUtils.copyArray(coordArray);
        for (int vecNum = 0; vecNum < periodicIndices.length; vecNum++) {
          newCoordArray[periodicIndices[vecNum]] += primCellLocation[vecNum];
        }
        Coordinates newDirectCoords = new Coordinates(newCoordArray, primBasis);
        //Coordinates definingCoords = this.getDefiningLattice().softRemainder(newDirectCoords);
        //Coordinates definingCoords = this.getDefiningLattice().hardRemainder(newDirectCoords); // This results in more intuitive behavior down the line.
        Coordinates definingCoords = newDirectCoords;
        int siteIndex = this.getSiteIndex(primCellLocation, primIndex);
        m_DefiningSites[siteIndex] = new DefiningSite(definingCoords, siteIndex, primSite);
        m_DefiningSites[siteIndex].setSpecies(primSite.getSpecies());
      }
    }
  }

 /**
  * Returns the internal array index of the site corresponding to the given coordinates
  * @param coords The coordinates of a lattice site
  * @return The array index of the corresponding site in this supercell
  */
  public int getSiteIndex(Coordinates coords) {
    Coordinates intCoords = coords.getCoordinates(m_ParentStructure.getIntegerBasis());
    int[] intCoordArray = new int[intCoords.numCoords() - 1];
    for (int coordNum = 0; coordNum < intCoordArray.length; coordNum++) {
      intCoordArray[coordNum] = (int) intCoords.coord(coordNum);
    }
    return this.getSiteIndex(intCoordArray, (int) intCoords.coord(intCoordArray.length));
  }

  /**
   * Takes in the coordinates of a primitive cell and the index of a site within that cell and return the
   * index of the site in the supercell.
   *
   * @param intCoordArray A 3-element integer array defining a lattice site.  The first elements are the x,y, and z
   * offsets of the primitive cell containing the lattice site.
   * @param primIndex The index of the site in the primitive cell.
   * @return The index of a lattice site corresponding to the given coordinates.
   */
  public int getSiteIndex(int[] intCoordArray, int primIndex) {

    int[] primCellLocation = m_SuperLattice.getInnerPrimCell(intCoordArray);
    int[] innerIntCoords = ArrayUtils.appendElement(primCellLocation, primIndex);

    // Now we simply return the integer corresponding to the coordinates in the rectangular canonical cell
    return m_IntCoordConverter.joinValuesBounded(innerIntCoords);
  }
  
  public ArrayIndexer getIntCoordConverter() {
    return m_IntCoordConverter;
  }
  
  /*
  private int[] getInnerPrimCell(int[] primCellLocation) {
    
    int[] innerIntCoords = new int[primCellLocation.length];
    for (int coordNum = primCellLocation.length - 1; coordNum >= 0; coordNum--) {
      int innerCoord = primCellLocation[coordNum];
      for (int row = coordNum + 1; row < m_CanonicalToDirect.length; row++) {
        
        int rowDimensionSize = m_IntCoordConverter.getDimensionSize(row);
        int shiftedCoord = innerIntCoords[row];
        if (shiftedCoord < 0) {  // Account for how Java handles negatives in integer division
          innerCoord -= ((shiftedCoord - rowDimensionSize + 1) / rowDimensionSize) * m_CanonicalToDirect[row][coordNum];
        } else {
          innerCoord -= (shiftedCoord / rowDimensionSize) * m_CanonicalToDirect[row][coordNum];
        }
      }
      innerIntCoords[coordNum] = innerCoord;
    }*/
    
    /*
    for (int coordNum = 0; coordNum < primCellLocation.length; coordNum++) {
      int innerCoord = primCellLocation[coordNum];
      for (int row = coordNum + 1; row < m_CanonicalToDirect.length; row++) {
        
        int rowDimensionSize = m_IntCoordConverter.getDimensionSize(row);
        int givenCoord = primCellLocation[row];
        if (givenCoord < 0) {  // Account for how Java handles negatives in integer division
          innerCoord -= ((givenCoord - rowDimensionSize + 1) / rowDimensionSize) * m_CanonicalToDirect[row][coordNum];
        } else {
          innerCoord -= (givenCoord / rowDimensionSize) * m_CanonicalToDirect[row][coordNum];
        }
      }
      innerCoord = innerCoord % m_IntCoordConverter.getDimensionSize(coordNum);
      innerIntCoords[coordNum] = innerCoord < 0 ? innerCoord + m_IntCoordConverter.getDimensionSize(coordNum) : innerCoord;
    }*/
    
    // Now take the modulo of the coordinates
    /*for (int coordNum = 0; coordNum < innerIntCoords.length; coordNum++) {
      int innerCoord = innerIntCoords[coordNum] % m_IntCoordConverter.getDimensionSize(coordNum);
      innerIntCoords[coordNum] = innerCoord < 0 ? innerCoord + m_IntCoordConverter.getDimensionSize(coordNum) : innerCoord;
    }*/
    
    /* OLD
    // We do some fancy math to move these coordinates inside the rectangular canonical cell
    int[] innerIntCoords = new int[primCellLocation.length];
    for (int coordNum = 0; coordNum < primCellLocation.length; coordNum++) {
      int innerCoord = primCellLocation[coordNum];
      for (int row = coordNum + 1; row < m_CanonicalToDirect.length; row++) {
        innerCoord += (primCellLocation[row] / m_IntCoordConverter.getDimensionSize(row)) * m_CanonicalToDirect[row][coordNum];
      }
      innerCoord = innerCoord % m_IntCoordConverter.getDimensionSize(coordNum);
      innerIntCoords[coordNum] = innerCoord < 0 ? innerCoord + m_IntCoordConverter.getDimensionSize(coordNum) : innerCoord;
    }
    */
    /*
    return innerIntCoords;
    
  }*/

  /**
   *
   * @return A square int array that contains the lattice vectors that translate coordinates in a system defined by the
   * lattice vectors of the supercell into direct (primitive cell) coordinates.  Note you need to right-multiply a
   * vector times this matrix to make the translation.  Currently returns the interior copy of the matrix, so do not modify
   * the returned matrix!
   */
  public int[][] getSuperToDirect() {
    return m_SuperLattice.getSuperToDirect();
  }

  /**
   *
   * @return A 3x3 int array that contains the lattice vectors that translate coordinates in a system defined by the
   * canonical lattice vectors of this supercell into direct (primitive cell) coordinates.  Note you need to right-multiply a
   * vector times this matrix to make the translation.  Currently returns the interior copy of the matrix, so do not modify
   * the returned matrix!
   */
  public int[][] getCanonicalToDirect() {
    return m_SuperLattice.getCanonicalToDirect(); 
  }
  
  public int getLatticeIndex() {
    return m_SuperLattice.getLatticeIndex();
    //return m_LatticeIndex;
  }

  /**
   *
   * @return The volume of this supercell in units of the volume of the primitive cell.
   */
  public int numPrimCells() {
    //return m_NumPrimCells;
    return m_SuperLattice.numPrimCells();
  }

  /**
   *
   * @return The primitive cell that defines the lattice on which this supercell resides.
   */
  public Structure getParentStructure() {
    return m_ParentStructure;
  }
  
  public Structure.Site getDefiningSite(Coordinates coords) {
    return this.getDefiningSite(this.getSiteIndex(coords));
  }
  
  /**
   * Returns sites in the Wigner-Seitz cell surrounding the origin of the given primitive cell
   * @param primCellNumber
   * @return
   */
  public Structure.Site[] getSitesNearPrimCellOrigin(int primCellNumber) {

    return this.getSitesNearPrimCellOrigin(m_SuperLattice, primCellNumber);
  }
  
  /**
   * Returns sites in the Wigner-Seitz cell surrounding the origin of the given primitive cell
   * @param primCellNumber
   * @return
   */
  public Structure.Site[] getSitesNearPrimCellOrigin(SuperLattice superLattice, int primCellNumber) {

    // TODO make sure superlattice matches with this lattice
    
    // These are the coordinates of the corner of the prim cell within the supercell
    Coordinates primCellOrigin = superLattice.getPrimCellOriginCoords(primCellNumber);
    
    return this.getSitesInPrimWSCell(superLattice, primCellOrigin);
  }
  
  /**
   * This returns all sites in the Wigner Seitz cell around the given coords, where the 
   * Wigner Seitz cell is based on the primitive lattice
   * @param centerCoords
   * @return
   */
  public Structure.Site[] getSitesInPrimWSCell(Coordinates centerCoords) {
    
    return this.getSitesInPrimWSCell(m_SuperLattice, centerCoords);
  }
  
  /**
   * This returns all sites in the Wigner Seitz cell around the given coords, where the 
   * Wigner Seitz cell is based on the primitive lattice
   * @param centerCoords
   * @return
   */
  public Structure.Site[] getSitesInPrimWSCell(SuperLattice superLattice, Coordinates centerCoords) {
    
    // TODO verify that the superlattice matches this lattice
    
    // This is the primitive lattice that we used to construct the superlattice.
    BravaisLattice primLattice = superLattice.getPrimLattice();
    
    // This is where we will store the sites we find.
    Structure.Site[] returnArray = new Structure.Site[this.numDefiningSites()];
    int returnIndex = 0;
    
    for (int siteNum = 0; siteNum < this.numDefiningSites(); siteNum++) {
      Structure.Site site = this.getDefiningSite(siteNum);
      Coordinates siteCoords = site.getCoords();
      
      // We move the coordinates into the Wigner-Seitz cell surrounding the primCellOrigin
      Coordinates wignerSeitzCellCoords = primLattice.getWignerSeitzNeighbor(centerCoords, siteCoords);
      
      // Now we see if the new coordinates still point to the original site.  If so, then
      // it's in the Wigner-Seitz cell.  If not, we move on to the next site.
      Structure.Site wignerSeitzSite = this.getSite(wignerSeitzCellCoords);
      
      if (wignerSeitzSite == null) {continue;}
      
      // This checks to see if these two sites are represented by the same defining site in the supercell
      if (wignerSeitzSite.getIndex() != site.getIndex()) {continue;} 
      
      returnArray[returnIndex] = wignerSeitzSite;
      returnIndex++;
      
    }
    
    // We probably didn't fill up the entire return array, so we get rid of the empty elements
    returnArray = (Structure.Site[]) ArrayUtils.truncateArray(returnArray, returnIndex);
    return returnArray;
  }


  public Coordinates[] getNearbySiteCoords(Coordinates coords, double distance, boolean includeGivenCoords) {
    return m_ParentStructure.getNearbySiteCoords(coords, distance, includeGivenCoords);
  }
  
  public Structure.Site[] getNearbySites(Coordinates coords, double distance, boolean includeGiven) {
    Coordinates[] neighbors = this.getNearbySiteCoords(coords, distance, includeGiven);
    Structure.Site[] returnArray = new Structure.Site[neighbors.length];
    for (int siteNum = 0; siteNum < neighbors.length; siteNum++) {
      returnArray[siteNum] = this.getSite(neighbors[siteNum]);
    }
    return returnArray;
  }

  /**
   *
   * @param coords The coordinates of a site on this lattice
   * @return The site in this supercell corresponding to the given coordinates
   */
  public Structure.Site getSite(Coordinates coords) {
    try {
      Structure.Site site = this.getDefiningSite(coords);
      return new SiteImage(coords, (DefiningSite) site);
    } catch (Exception e) { // TODO make sure this breaks nothing
      return null; 
    }
  }
  
  /*
  public Vector getPrimCellVector(int primCellNumber) {
    int[] primCellOrigin = m_PrimCellIterator.splitValue(primCellNumber);
    Vector[] parentVectors = m_ParentStructure.getLattice().getPeriodicVectors();
    Vector returnVector = Vector.getZero3DVector();
    for (int vecNum = 0; vecNum < parentVectors.length; vecNum++) {
      returnVector = returnVector.add(parentVectors[vecNum].multiply(primCellOrigin[vecNum]));
    }
    return returnVector;
  }

  public int[] getPrimCellOrigin(int primCellNumber) {
    return m_PrimCellIterator.splitValue(primCellNumber);
  }
  
  public int getPrimCellNumber(int[] primCellOrigin) {
    return m_PrimCellIterator.joinValues(this.getInnerPrimCell(primCellOrigin));
  }
  */
  /*
  public int getPrimCellNumber(Coordinates coords) {
    Coordinates intCoords = coords.getCoordinates(m_ParentStructure.getIntegerBasis());
    int[] primCellOriginArray = new int[intCoords.numCoords() -1];
    for (int dimNum= 0; dimNum < primCellOriginArray.length; dimNum++) {
      primCellOriginArray[dimNum] = (int) intCoords.coord(dimNum);
    }
    return m_SuperLattice.getPrimCellNumber(primCellOriginArray);
  }*/
  
  /*public SuperLattice getSuperLattice() {
    return m_SuperLattice;
  }*/
 
  public BravaisLattice getSiteLattice() {
    return m_ParentStructure.getSiteLattice();
  }
  
  public SuperLattice getSuperLattice() {
    return m_SuperLattice;
  }
  
  public SpaceGroup getSiteSpaceGroup() {
    return m_ParentStructure.getSiteSpaceGroup();
  }
  
  /*
  public SpaceGroup getSiteSpaceGroup() {
    //return m_ParentStructure.getSiteSpaceGroup();
    if (m_SiteSpaceGroup == null) {
      SuperLattice superLattice = this.getSuperDefiningLattice();
      SymmetryOperation[] operations = m_ParentStructure.getSiteSpaceGroup().getFactorOperations(superLattice);
      m_SiteSpaceGroup = new SpaceGroup(operations, superLattice);
    }
    return m_SiteSpaceGroup;
  }*/

  public interface Site extends Structure.Site {
    public Structure.Site getParentSite();
    public int[] getPrimCellLocation();
  }
  
  protected class DefiningSite extends Structure.DefiningSite implements Site {
    
    Structure.Site m_ParentSite;
    
    protected DefiningSite(DefiningSite site) {
      super(site);
      m_ParentSite = site.getParentSite();
    }
    
    public DefiningSite(Coordinates coords, int index, Structure.Site parentSite) {
      super(coords, index);
      m_ParentSite = parentSite;
    }
    
    public Structure.Site getParentSite() {
      return m_ParentSite;
    }
    
    public Structure.Site copy() {
      return new DefiningSite(this);
    }
    
    public int[] getPrimCellLocation() {
      LinearBasis basis = m_ParentStructure.getDirectBasis();
      double[] coordArray = this.getCoords().getCoordArray(basis);
      double[] primCoordArray = this.getParentSite().getCoords().getCoordArray(basis);
      int[] returnArray = new int[primCoordArray.length];
      for (int dimNum = 0; dimNum < returnArray.length; dimNum++) {
        returnArray[dimNum] = (int) Math.round(coordArray[dimNum] - primCoordArray[dimNum]);
      }
      return returnArray;
    }
    
  }
  
  protected class SiteImage extends Structure.SiteImage implements Site {
    
    public SiteImage(Coordinates coords, SuperStructure.Site definingSite) {
      super(coords, definingSite);
    }
    
    public Structure.Site getParentSite() {
      SuperStructure.DefiningSite site = (SuperStructure.DefiningSite) this.getDefiningSite();
      return site.getParentSite();
    }
    
    // This is consistent with the discrete basis
    public int[] getPrimCellLocation() {
      LinearBasis basis = m_ParentStructure.getDirectBasis();
      double[] coordArray = this.getCoords().getCoordArray(basis);
      double[] primCoordArray = this.getParentSite().getCoords().getCoordArray(basis);
      int[] returnArray = new int[primCoordArray.length];
      for (int dimNum = 0; dimNum < returnArray.length; dimNum++) {
        returnArray[dimNum] = (int) Math.round(coordArray[dimNum] - primCoordArray[dimNum]);
      }
      return returnArray;
    }
  }
}
