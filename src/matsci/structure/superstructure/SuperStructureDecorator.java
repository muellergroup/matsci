/*
 * Created on Apr 20, 2006
 *
 */
package matsci.structure.superstructure;

import java.math.*;
import java.util.Arrays;

import matsci.location.*;
import matsci.location.symmetry.operations.*;
import matsci.Species;
import matsci.structure.*;
import matsci.structure.decorate.*;
import matsci.structure.decorate.exceptions.IllegalOccupationException;
//import matsci.structure.decorate.function.*;
import matsci.util.arrays.*;

public class SuperStructureDecorator extends SiteDecorator {

  //private final Specie[][] m_AllowedSpecies;
  //private final int[][] m_SuperToDirect;
  private final SuperStructure m_SuperStructure;
  private final SuperStructure m_SuperStructureFromPrim;
  //private final SiteDecorator m_SiteDecorator;
  //private final SuperLatticeGenerator m_LatticeGenerator;
  //private final int m_LatticeIndex;
  //private final Vector[] m_TranslationVectors;
  
  private DecorationTemplate m_OrigDecorationTemplate;
  private DecorationTemplate m_PrimDecorationTemplate;
  
  private int[][] m_Permutations = null; // Lazy load
  private boolean[] m_IsPermutationTranslation = null; // Lazy load
  private int m_IdentityPermutationIndex = -1;
  //private int[][] m_TranslationPermutations = null; // Lazy load
  
  private ArrayIndexer.Filter[] m_SeenStatesFilters = null; // Lazy load
  private ArrayIndexer.Filter[] m_IrreducibleStatesFilters = null; // Lazy load
  
  protected SuperStructureDecorator[] m_FactorDecorators = null; // Lazy load
  
  private int[] m_SigmaIndexMap; // How the site indices map to the indices of the sigma-site-only array

  /**
   * The i<i>th</i> element is the sub structure site index to which the i<i>th</i> site maps.
   */
  protected int[][] m_FactorMaps = null; // Lazy load.
  /**
   * These are just places to temporarily map a given set of states onto a substructure.
   */
  protected int[][] m_FactorStates = null; // Lazy load
  
  /*
  protected SuperStructureDecorator(SuperStructureDecorator oldDecorator, boolean compactStructure) {
    m_AllowedSpecies = oldDecorator.m_AllowedSpecies;
    if (compactStructure) {
      Structure newStructure = oldDecorator.m_SuperStructure.getCompactStructure();
      Structure oldParent = oldDecorator.m_SuperStructure.getParentStructure();
      m_SuperToDirect = oldParent.getSuperToDirect(newStructure);
      m_SuperStructure = new SuperStructure(oldParent, m_SuperToDirect);
    } else {
      m_SuperToDirect = oldDecorator.m_SuperToDirect;
      m_SuperStructure = oldDecorator.m_SuperStructure;
    }
    m_SiteDecorator = new SiteDecorator(m_AllowedSpecies, m_SuperStructure);
    //m_LatticeGenerator = oldDecorator.m_LatticeGenerator;
    //m_LatticeIndex = oldDecorator.m_LatticeIndex;
    m_TranslationVectors = new Vector[m_SuperStructure.numPrimCells()];
    for (int vecNum = 0; vecNum < m_TranslationVectors.length; vecNum++) {
      m_TranslationVectors[vecNum] = m_SuperStructure.getSuperDefiningLattice().getPrimCellVector(vecNum);
    }
  }*/
  
  public SuperStructureDecorator(DecorationTemplate cf, SuperLatticeGenerator generator, int latticeIndex) {
    
    this(cf, generator.getSuperToDirect(latticeIndex));
    //m_AllowedSpecies = cf.getAllowedSpecies();
    /*int[][] superToDirect = generator.getSuperToDirect(latticeIndex);
    //m_SuperToDirect = superToDirect;
    m_SuperStructure = new SuperStructure(cf.getPrimStructure(), superToDirect);
    m_SiteDecorator = new SiteDecorator(cf, m_SuperStructure);
    //m_LatticeGenerator = generator;
    //m_LatticeIndex = generator.getLatticeIndex(superToDirect);
    m_TranslationVectors = new Vector[m_SuperStructure.numPrimCells()];
    for (int vecNum = 0; vecNum < m_TranslationVectors.length; vecNum++) {
      m_TranslationVectors[vecNum] = m_SuperStructure.getSuperDefiningLattice().getPrimCellVector(vecNum);
    }
    m_SpaceGroup = cf.getSpaceGroup();
    this.getPermutations();*/
    
  }
  
  public SuperStructureDecorator(DecorationTemplate template) {
    this(template, getIdentityMatrix(template));
  }
  
  private static int[][] getIdentityMatrix(DecorationTemplate template) {
    
    int numPeriodicDimensions = template.getBaseStructure().numPeriodicDimensions();
    int[][] returnArray = new int[numPeriodicDimensions][numPeriodicDimensions];
    for (int dimNum = 0; dimNum < returnArray.length; dimNum++) {
      returnArray[dimNum][dimNum] = 1;
    }
    return returnArray;
    
  }
  
  public SuperStructureDecorator(DecorationTemplate cf, int[][] superToDirect) {

    this(cf, new SuperStructure(cf.getBaseStructure(), superToDirect));
    //superToDirect = SuperLatticeGenerator.toCanonical(superToDirect);
    //m_AllowedSpecies = cf.getAllowedSpecies();
    //m_SuperToDirect = superToDirect;
    /*m_SuperStructure = new SuperStructure(cf.getPrimStructure(), superToDirect);
    m_SiteDecorator = new SiteDecorator(cf, m_SuperStructure);
    //int size = SuperLatticeGenerator.getSize(superToDirect);
    //m_LatticeGenerator = new SuperLatticeGenerator(size, m_SuperStructure.numPeriodicDimensions());
    //m_LatticeIndex = m_LatticeGenerator.getLatticeIndex(superToDirect);
    m_TranslationVectors = new Vector[m_SuperStructure.numPrimCells()];
    for (int vecNum = 0; vecNum < m_TranslationVectors.length; vecNum++) {
      m_TranslationVectors[vecNum] = m_SuperStructure.getSuperDefiningLattice().getPrimCellVector(vecNum);
    }
    m_SpaceGroup = cf.getSpaceGroup();
    this.getPermutations();*/
  }
  
  public SuperStructureDecorator(DecorationTemplate cf, IStructureData structure) {
    this(cf, cf.getBaseStructure().getSuperStructure(structure));
  }
  
  public SuperStructureDecorator(DecorationTemplate cf, SuperStructure superStructure) {
    super(cf, superStructure);
    m_SuperStructure = superStructure;
    //m_SiteDecorator = new SiteDecorator(cf, m_SuperStructure);
    //int size = SuperLatticeGenerator.getSize(superToDirect);
    //m_LatticeGenerator = new SuperLatticeGenerator(size, m_SuperStructure.numPeriodicDimensions());
    //m_LatticeIndex = m_LatticeGenerator.getLatticeIndex(superToDirect);

    //this.getPermutations();
    m_OrigDecorationTemplate = cf;
    m_PrimDecorationTemplate = cf.findPrimitive();
    if (m_OrigDecorationTemplate.numSigmaSites() != m_PrimDecorationTemplate.numSigmaSites()) {
      Structure baseStructure = m_PrimDecorationTemplate.getBaseStructure();
      int[][] superToDirect = baseStructure.getDefiningLattice().getSuperToDirect(m_SuperStructure.getDefiningLattice());
      m_SuperStructureFromPrim = new SuperStructure(baseStructure, superToDirect);
    } else {
      m_SuperStructureFromPrim = m_SuperStructure;
    }
    
    m_SigmaIndexMap = new int[m_SuperStructure.numDefiningSites()];
    Arrays.fill(m_SigmaIndexMap, -1);
    for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
      int definingSiteIndex = this.getSigmaSite(sigmaIndex).getIndex();
      m_SigmaIndexMap[definingSiteIndex] = sigmaIndex;
    }
    
  }
  
  protected SuperStructureDecorator newDecoratorFromPrimTemplate(int[][] superToDirect) {
    //return new SuperStructureDecorator(this.getDecorationTemplate().findPrimitive(), superToDirect);
    return new SuperStructureDecorator(m_PrimDecorationTemplate, superToDirect);
  }
  
  public int[][] getSuperToDirectForPrimBasedDecorator() {
    DecorationTemplate primTemplate = m_PrimDecorationTemplate.findPrimitive();
    if (primTemplate.numSigmaSites() == m_PrimDecorationTemplate.numSigmaSites()) {
      int numDimensions = m_PrimDecorationTemplate.getSpaceGroup().getLattice().numPeriodicVectors();
      int[][] returnArray = new int[numDimensions][numDimensions];
      for (int dimNum = 0; dimNum < returnArray.length; dimNum++) {
        returnArray[dimNum][dimNum] = 1;
      }
      return returnArray;
    }
    BravaisLattice primLattice = primTemplate.getDefiningSpaceGroup().getLattice();
    BravaisLattice superLattice = this.getSuperStructure().getDefiningLattice();
    return primLattice.getSuperToDirect(superLattice);
  }
  
  /*
  public SuperStructureDecorator(Structure primStructure, Specie[][] allowedSpecies, SuperLatticeGenerator generator, int latticeIndex) {
    m_AllowedSpecies = (Specie[][]) ArrayUtils.copyArray(allowedSpecies);
    m_SuperToDirect = generator.getSuperToDirect(latticeIndex);
    m_SuperStructure = new SuperStructure(primStructure, m_SuperToDirect);
    m_SiteDecorator = new SiteDecorator(m_AllowedSpecies, m_SuperStructure);
    //m_LatticeGenerator = generator;
    //m_LatticeIndex = generator.getLatticeIndex(m_SuperToDirect);
    m_TranslationVectors = new Vector[m_SuperStructure.numPrimCells()];
    for (int vecNum = 0; vecNum < m_TranslationVectors.length; vecNum++) {
      m_TranslationVectors[vecNum] = m_SuperStructure.getSuperDefiningLattice().getPrimCellVector(vecNum);
    }
  }*/
  
  /*
  public SuperStructureDecorator(Structure primStructure, Specie[][] allowedSpecies, int[][] superToDirect) {
    
    superToDirect = SuperLatticeGenerator.toCanonical(superToDirect);
    m_AllowedSpecies = (Specie[][]) ArrayUtils.copyArray(allowedSpecies);
    m_SuperToDirect = superToDirect;
    m_SuperStructure = new SuperStructure(primStructure, superToDirect);
    m_SiteDecorator = new SiteDecorator(m_AllowedSpecies, m_SuperStructure);
    int size = SuperLatticeGenerator.getSize(superToDirect);
    //m_LatticeGenerator = new SuperLatticeGenerator(size, primStructure.numPeriodicDimensions());
    //m_LatticeIndex = m_LatticeGenerator.getLatticeIndex(superToDirect);
    m_TranslationVectors = new Vector[m_SuperStructure.numPrimCells()];
    for (int vecNum = 0; vecNum < m_TranslationVectors.length; vecNum++) {
      m_TranslationVectors[vecNum] = m_SuperStructure.getSuperDefiningLattice().getPrimCellVector(vecNum);
    }
  }*/
  
  /*
  public SuperStructureDecorator getCompactDecorator() {
    return new SuperStructureDecorator(this, true);
  }*/
  
  /*
  public SuperStructure getSuperStructure(BigInteger decorationID) {
    this.decorate(decorationID);
    return (SuperStructure) m_SuperStructure.copy();
  }*/
  
  public boolean isBasedOnPrim() {
    DecorationTemplate primTemplate = m_PrimDecorationTemplate.findPrimitive();
    return (primTemplate.numSigmaSites() == m_PrimDecorationTemplate.numSigmaSites());
  }
  
  /**
   * 
   * @return The prim template
   */
  public DecorationTemplate getPrimDecorationTemplate() {
    return m_PrimDecorationTemplate;
  }
  
  public DecorationTemplate getGivenDecorationTemplate() {
    return m_OrigDecorationTemplate;
  }
  
  public SuperStructure getSuperStructure() {
    return m_SuperStructure;
  }
  
  public SuperStructure getSuperStructureFromPrimTemplate() {
    return m_SuperStructureFromPrim;
  }
  
  public int numPrimCells() {
    return m_SuperStructure.numPrimCells();
  }
  
  public int numPrimCellsFromPrimTemplate() {
    return m_SuperStructureFromPrim.numPrimCells();
  }
  
  public int getPrimIndex(int sigmaIndex) {
    SuperStructure.Site site = (SuperStructure.Site) this.getSigmaSite(sigmaIndex);
    return site.getParentSite().getIndex();
  }
  

  /**
   *
   * @param siteIndex The index of a site in the superstructure to which
   * the Hamiltonian was applied.
   * @return If the corresponding site is a sigma site, return the sigma index of the site.  
   * If the site is not a sigma site, returns -1.  If there is no site corresponding to the
   * given index, throws an ArrayIndexOutOfBounds exception.
   */
  public int getSigmaIndex(int siteIndex) {
    return m_SigmaIndexMap[siteIndex];
  }
  
  /**
  *
  * @param siteIndex The index of a site in the superstructure to which
  * the Hamiltonian was applied.
  * @return True if and only if the corresponding site is a sigma site.  If there is no site 
  * corresponding to the given index, throws an ArrayIndexOutOfBounds exception.
  */
 public boolean isSigmaSite(int siteIndex) {
   return (m_SigmaIndexMap[siteIndex] != -1);
 }
 
  /*
  public boolean equivalentToLowerID(SpaceGroup spaceGroup, BigInteger decorationID) {
    
    try {
      int[] latticePreservingOpNums = m_SuperStructure.getSuperDefiningLattice().getLatticePreservingOpNums(spaceGroup);
      //int[] latticePreservingOpNums = this.getLatticePreservingOpNums(spaceGroup);
      for (int opIndex = 0; opIndex < latticePreservingOpNums.length; opIndex++) {
        int opNum = latticePreservingOpNums[opIndex];
        SymmetryOperation op = spaceGroup.getOperation(opNum);
        this.decorate(decorationID);
        this.operate(op);
        BigInteger oppedDecorationID = this.readDecorationID();
        for (int vecNum = 0; vecNum < m_TranslationVectors.length; vecNum++) {
          this.translate(m_TranslationVectors[vecNum]);
          BigInteger translatedID = this.readDecorationID();
          this.decorate(oppedDecorationID);
          if (translatedID.compareTo(decorationID) < 0) {return true;}
        }
      }
      return false;
    } catch (IllegalOccupationException e) {
      return false;
    }
  }*/
  
  /*
  public boolean incrementToUnseenSigmaStates(int[] currentStates, int[] specCount, Specie[] species) {
    
    // Go through each permutation, and find out of the permutation is 
    
    int[][] permutations = this.getPermutations();
    
    // We try to figure out which sigmaIndex should next be incremented
    boolean allClear = false;
    int firstGoodPermutation = 0;
    while (!allClear) {
      
      if (!m_ConfigIndexer.increment(currentStates)) {return false;}
      allClear = true;
      
      for (int permIncrement = 0; permIncrement < permutations.length; permIncrement++) {
        int permutationIndex = (firstGoodPermutation + permIncrement) % m_Permutations.length;
        int[] permutation = m_Permutations[permutationIndex];
        boolean okPerm = true;
        int minSigmaToIncrement = permutation.length - 1;
        for (int sigmaIndex = permutation.length - 1; sigmaIndex >= 0; sigmaIndex--) {
          int permState = currentStates[permutation[sigmaIndex]];
          int origState = currentStates[sigmaIndex];
          if (origState < permState) {break;}
          if (permutation[sigmaIndex] < permutation[minSigmaToIncrement]) {
            minSigmaToIncrement = sigmaIndex;
          }
          if (permState < origState) {
            okPerm = false;
            if (minSigmaToIncrement == sigmaIndex) {
              currentStates[permutation[sigmaIndex]] = origState - 1;
            }
            break;
          }
        }
        if (okPerm) {continue;}
        allClear = false;
        firstGoodPermutation = permutationIndex + 1;
        m_ConfigIndexer.branchEnd(permutation[minSigmaToIncrement], currentStates);
        break;
      }
    }
    
    return true;
  }*/

  /**
   * This method cycles over all substructures first.
   */
  public boolean incrementToUnseenSigmaStates(int[] currentStates) {
    
    this.getFactorDecorators();
    for (int decNum = 0; decNum < m_FactorDecorators.length - 1; decNum++) {
      
      // Check to see if we're in a substructure state.
      SuperStructureDecorator reducedDecorator = m_FactorDecorators[decNum];
      int[] reducedStates = m_FactorStates[decNum];
      Arrays.fill(reducedStates, -1);
      int[] map = m_FactorMaps[decNum];
      boolean match = true;
      for (int sigmaIndex = 0; sigmaIndex < currentStates.length; sigmaIndex++) {
        int newValue = currentStates[sigmaIndex];
        int reducedIndex = map[sigmaIndex];
        int oldValue = reducedStates[reducedIndex];
        if (oldValue == -1) {
          reducedStates[reducedIndex] = newValue;
          continue;
        }
        if (oldValue != newValue) {
          match = false;
          break;
        }
      }
      if (!match) {continue;}
      
      if (reducedDecorator.incrementToUnseenIrreducibleSigmaStates(reducedStates)) {
        for (int sigmaIndex = 0; sigmaIndex < currentStates.length; sigmaIndex++) {
          currentStates[sigmaIndex] = reducedStates[map[sigmaIndex]];
        }
        return true;
      }
      
      int[] nextReducedStates = null;
      while (nextReducedStates == null) {
        decNum++;
        if (decNum == m_FactorDecorators.length) {
          return false;
        }
        SuperStructureDecorator nextDecorator = m_FactorDecorators[decNum];
        ArrayIndexer.Filter[] irreducibleFilters = nextDecorator.getIrreducibleStatesFilters();
        nextReducedStates = nextDecorator.getInitialSigmaStates(irreducibleFilters);
      }
      int[] nextMap = m_FactorMaps[decNum];
      for (int sigmaIndex = 0; sigmaIndex < currentStates.length; sigmaIndex++) {
        currentStates[sigmaIndex] = nextReducedStates[nextMap[sigmaIndex]];
      }
      return true;
    }

    return this.incrementToUnseenIrreducibleSigmaStates(currentStates);
  }
  
  public boolean incrementToUnseenIrreducibleSigmaStates(int[] currentStates) {
    
    // Go through each permutation, and find out of the permutation is 
    
    int[][] permutations = this.getPermutations();
    
    // We try to figure out which sigmaIndex should next be incremented
    boolean allClear = false;
    int firstGoodPermutation = 0;
    while (!allClear) {
      
      if (!m_ConfigIndexer.increment(currentStates)) {return false;}
      
      allClear = true;
      
      for (int permIncrement = 0; permIncrement < permutations.length; permIncrement++) {
        int permutationIndex = (firstGoodPermutation + permIncrement) % m_Permutations.length;
        if (permutationIndex == m_IdentityPermutationIndex) {
          continue;
        }
        int[] permutation = m_Permutations[permutationIndex];
        boolean okPerm = true;
        int minSigmaToIncrement = permutation.length - 1;
        for (int sigmaIndex = permutation.length - 1; sigmaIndex >= 0; sigmaIndex--) {
          int permState = currentStates[permutation[sigmaIndex]];
          int origState = currentStates[sigmaIndex];
          if (origState < permState) {break;}
          if (permutation[sigmaIndex] < permutation[minSigmaToIncrement]) {
            minSigmaToIncrement = sigmaIndex;
          }
          if (permState < origState) {
            okPerm = false;
            if (minSigmaToIncrement == sigmaIndex) {
              currentStates[permutation[sigmaIndex]] = origState - 1;
            }
            break;
          }
          // This should eliminate translations
          if (sigmaIndex == 0) {
            okPerm = !m_IsPermutationTranslation[permutationIndex];
            break;
          }
        }
        if (okPerm) {continue;}
        allClear = false;
        firstGoodPermutation = permutationIndex + 1;
        m_ConfigIndexer.branchEnd(permutation[minSigmaToIncrement], currentStates);
        break;
      }
    }
    
    return true;
  }
  
  protected int[][] getPermutations() {
    if (m_Permutations == null) {
      this.makePermutations();
    }
    return m_Permutations;
  }
  
  public int[][] getSymmetryPermutations() {
	  return ArrayUtils.copyArray(this.getPermutations());
  }
  
  protected void makePermutations() {

    SpaceGroup spaceGroup = this.getPrimDecorationTemplate().getSpaceGroup();
    //spaceGroup = spaceGroup.getPrimitiveSpaceGroup();  // Just to be safe, we don't assume the space group was primitive.
    //BravaisLattice primLattice = spaceGroup.getLattice();
    //BravaisLattice primLattice = this.getDecorationTemplate().getBaseStructure().getDefiningLattice();
    //SuperLattice superLattice = primLattice.getSuperLattice(m_SuperStructure.getSuperLattice());
    SuperLattice superLattice = m_SuperStructureFromPrim.getSuperLattice();
    
    /*Vector[] translationVectors = new Vector[this.numPrimCells()];
    for (int vecNum = 0; vecNum < translationVectors.length; vecNum++) {
      translationVectors[vecNum] = m_SuperStructure.getSuperLattice().getPrimCellVector(vecNum);
    }*/
    
    Vector[] translationVectors = new Vector[superLattice.numPrimCells()];
    for (int vecNum = 0; vecNum < translationVectors.length; vecNum++) {
      translationVectors[vecNum] = superLattice.getPrimCellVector(vecNum);
    }
    
    m_Permutations = new int[0][];
    //m_TranslationPermutations = new int[0][];
    m_IsPermutationTranslation = new boolean[0];
    // TODO Figure out if this is too restrictive.  IT IS!  Need to consider subcells!
    int[] latticePreservingOpNums = superLattice.getLatticePreservingOpNums(spaceGroup);
    //int[] latticePreservingOpNums = m_SuperStructure.getSuperLattice().getLatticePreservingOpNums(spaceGroup);
    //int[] latticePreservingOpNums = this.getLatticePreservingOpNums(spaceGroup);
    //for (int opNum = 0; opNum < m_SpaceGroup.numOperations(); opNum++) {
    for (int opIndex = 0; opIndex < latticePreservingOpNums.length; opIndex++) {
      int opNum = latticePreservingOpNums[opIndex];
      SymmetryOperation latticePreservingOp = spaceGroup.getOperation(opNum);
      //boolean isIdentityOp = latticePreservingOp.isIdentity();
      Coordinates[] allOppedCoords = new Coordinates[this.numSigmaSites()];
      for (int sigmaIndex = 0; sigmaIndex < allOppedCoords.length; sigmaIndex++) {
        Structure.Site sourceSite = this.getSigmaSite(sigmaIndex);
        allOppedCoords[sigmaIndex] = latticePreservingOp.operate(sourceSite.getCoords());
      }
      for (int vecNum = 0; vecNum < translationVectors.length; vecNum++) {

        boolean isIdentityOp = true;
        Vector transVector = translationVectors[vecNum];
        int[] permutation = new int[this.numSigmaSites()];
        for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
          Coordinates oppedCoords = allOppedCoords[sigmaIndex].translateBy(transVector);
          //Structure.Site oppedSite = m_SuperStructure.getDefiningSite(oppedCoords);
          //Structure.Site oppedSite = m_SuperStructure.getNearbyDefiningSite(oppedCoords);
          Structure.Site oppedSite = m_SuperStructure.getFastDefiningSite(oppedCoords);
          for (int sigmaIndex2 = 0; sigmaIndex2 < this.numSigmaSites(); sigmaIndex2++) {
            Structure.Site sigmaSite2 = this.getSigmaSite(sigmaIndex2);
            if (sigmaSite2.getIndex() == oppedSite.getIndex()) {
              permutation[sigmaIndex2] = sigmaIndex;
              isIdentityOp &= (sigmaIndex2 == sigmaIndex);
              break;
            }
          }
        }
        
        /*if (isIdentityOp) {
          boolean newTransPermutation = true;
          for (int permutationIndex = 0; permutationIndex < m_TranslationPermutations.length; permutationIndex++) {
            if (java.util.Arrays.equals(permutation, m_TranslationPermutations[permutationIndex])) {
              newTransPermutation = false;
              break;
            }
          }
          if (newTransPermutation) {
            m_TranslationPermutations = ArrayUtils.appendElement(m_TranslationPermutations, permutation);
            //m_Permutations = ArrayUtils.appendElement(m_Permutations, permutation);
          }
        }*/
        boolean newPermutation = true;
        boolean isPureTranslation = (latticePreservingOp.isPureTranslation() != null);
        
        for (int permutationIndex = 0; permutationIndex < m_Permutations.length; permutationIndex++) {
          if (java.util.Arrays.equals(permutation, m_Permutations[permutationIndex])) {
            newPermutation = false;
            m_IsPermutationTranslation[permutationIndex] |= isPureTranslation;
            break;
          }
        }
        if (newPermutation) {
          if (isIdentityOp) {
            m_IdentityPermutationIndex = m_Permutations.length;
          }
          m_Permutations = ArrayUtils.appendElement(m_Permutations, permutation);
          m_IsPermutationTranslation = ArrayUtils.appendElement(m_IsPermutationTranslation, isPureTranslation);
        }
      }
    }
    
    // Verify that we have a closed group!
    int[] product = new int[this.numSigmaSites()];
    for (int permNum = 0; permNum < m_Permutations.length; permNum++) {
      int[] permutation = m_Permutations[permNum];
      for (int prevPermNum = 0; prevPermNum < m_Permutations.length; prevPermNum++) {
        int[] prevPermutation = m_Permutations[prevPermNum];
        for (int siteNum = 0; siteNum < permutation.length; siteNum++) {
          product[siteNum] = permutation[prevPermutation[siteNum]];
        }
        boolean match = false;
        for (int prodNum = 0; prodNum < m_Permutations.length; prodNum++) {
          match |= Arrays.equals(product, m_Permutations[prodNum]);
          if (match) {break;}
        }
        if (!match) {
          throw new RuntimeException("Symmetrically equivalent superstrucure decoration permutations did not form a group!");
        }
      }
      
    }
  }
  
  public int getMultiplicity(BigInteger decID) {
    return this.getMultiplicity(this.getSigmaStates(decID));
  }
  
  public int getMultiplicity(int[] sigmaStates) {
    
    int[][] permutations =this.getPermutations();
    int numSameMaps = 0;
    for (int permutationIndex = 0; permutationIndex < permutations.length; permutationIndex++) {
      int[] permutation = permutations[permutationIndex];
      boolean match = true;
      for (int sigmaIndex = 0; sigmaIndex < sigmaStates.length; sigmaIndex++) {
        if (sigmaStates[sigmaIndex] != sigmaStates[permutation[sigmaIndex]]) {
          match = false;
          break;
        }
      }
      if (match) {numSameMaps++;}
    }
    return permutations.length / numSameMaps;
    
  }
  
  public BigInteger getMinEquivalentDecorationID(BigInteger decorationID) {
    
    int[] sigmaStates = this.getSigmaStates(decorationID);
    int[] oppedSigmaStates = new int[sigmaStates.length];
    int[][] permutations = this.getPermutations();
    BigInteger minID = decorationID;
    for (int permutationIndex = 0; permutationIndex < permutations.length; permutationIndex++) {
      
      int[] permutation = permutations[permutationIndex];
      for (int siteNum = 0; siteNum < sigmaStates.length; siteNum++) {
        oppedSigmaStates[siteNum] = sigmaStates[permutation[siteNum]];
      }
      BigInteger oppedID = this.getDecorationID(oppedSigmaStates);
      if (oppedID.compareTo(minID) < 0) {
        minID = oppedID;
      }
    }
    return minID;
    
  }
  
  public boolean equivalentToLowerID(BigInteger decorationID) {
    
    int[] sigmaStates = this.getSigmaStates(decorationID);
    int[] oppedSigmaStates = new int[sigmaStates.length];
    int[][] permutations = this.getPermutations();
    for (int permutationIndex = 0; permutationIndex < permutations.length; permutationIndex++) {
      
      int[] permutation = permutations[permutationIndex];
      for (int siteNum = 0; siteNum < sigmaStates.length; siteNum++) {
        oppedSigmaStates[siteNum] = sigmaStates[permutation[siteNum]];
      }
      BigInteger oppedID = this.getDecorationID(oppedSigmaStates);
      if (oppedID.compareTo(decorationID) < 0) {return true;}
    }
    return false;
    
  }
  
  public boolean isPrimStructure(int[] sigmaStates) {
    int[][] permutations = this.getPermutations();
    for (int permutationIndex = 0; permutationIndex < permutations.length; permutationIndex++) {
      if (!m_IsPermutationTranslation[permutationIndex]) {
        continue;
      }
      if (permutationIndex == m_IdentityPermutationIndex) {
        continue;
      }
      int[] permutation = permutations[permutationIndex];
      boolean match = true;
      for (int siteNum = 0; siteNum < sigmaStates.length; siteNum++) {
        if (sigmaStates[siteNum] != sigmaStates[permutation[siteNum]]) {
          match = false;
          break;
        }
      }
      if (match) {return false;}
    }
    return true;
  }
  
  public int numFactors() {
    return this.getFactorDecorators().length;
  }
  
  public int numFactoredPrimCells(int factorIndex) {
    this.getFactorDecorators();
    return this.numSigmaSites() / m_FactorStates[factorIndex].length;
  }
  
  public int numPrimCells(int[] sigmaStates) {
    int decNum = this.getFactorIndex(sigmaStates);
    return sigmaStates.length / m_FactorStates[decNum].length;
  }
  
  public int getFactorIndex(int[] sigmaStates) {
    SuperStructureDecorator[] factorDecorators = this.getFactorDecorators();
    for (int decNum = 0; decNum < factorDecorators.length - 1; decNum++) {
      int[] reducedStates = m_FactorStates[decNum];
      Arrays.fill(reducedStates, -1);
      int[] map = m_FactorMaps[decNum];
      boolean match = true;
      for (int sigmaIndex = 0; sigmaIndex < sigmaStates.length; sigmaIndex++) {
        int newValue = sigmaStates[sigmaIndex];
        int reducedIndex = map[sigmaIndex];
        int oldValue = reducedStates[reducedIndex];
        if (oldValue == -1) {
          reducedStates[reducedIndex] = newValue;
          continue;
        }
        if (oldValue != newValue) {
          match = false;
          break;
        }
      }
      if (match) {return decNum;}
    }
    return factorDecorators.length - 1;
  }
  
  /*
  public BigInteger[] getEquivalentDecorationIDs(SpaceGroup spaceGroup, BigInteger decorationID) throws IllegalOccupationException {
    //int[] latticePreservingOpNums = this.getLatticePreservingOpNums(spaceGroup);
    int[] latticePreservingOpNums = m_SuperStructure.getSuperDefiningLattice().getLatticePreservingOpNums(spaceGroup);
    BigInteger[] returnArray = new BigInteger[latticePreservingOpNums.length];
    int returnIndex = 0;
    for (int opIndex = 0; opIndex < latticePreservingOpNums.length; opIndex++) {
      int opNum = latticePreservingOpNums[opIndex];
      SymmetryOperation op = spaceGroup.getOperation(opNum);
      m_SiteDecorator.decorate(decorationID);
      this.operate(op);
      BigInteger oppedDecorationID = m_SiteDecorator.readDecorationID();
      boolean equivalent = false;
      for (int vecNum = 0; vecNum < m_TranslationVectors.length; vecNum++) {
        this.translate(m_TranslationVectors[vecNum]);
        BigInteger translatedID = m_SiteDecorator.readDecorationID();
        m_SiteDecorator.decorate(oppedDecorationID);
        if (translatedID.compareTo(decorationID) == 0) {
          equivalent = true;
          break;
        }
      }
      if (equivalent) {returnArray[returnIndex++] = oppedDecorationID;}
    }
    return (BigInteger[]) ArrayUtils.truncateArray(returnArray, returnIndex);
  }*/
  
  /*
  protected void translate(Vector vector) {
    Species[] speciesToMove = new Species[this.numSigmaSites()];
    for (int sigmaIndex= 0; sigmaIndex < speciesToMove.length; sigmaIndex++) {
      speciesToMove[sigmaIndex] = this.getSigmaSite(sigmaIndex).getSpecies();
    }
    
    for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
      Structure.Site sourceSite = this.getSigmaSite(sigmaIndex);
      Coordinates translatedCoords = sourceSite.getCoords().translateBy(vector);
      m_SuperStructure.getSite(translatedCoords).setSpecies(speciesToMove[sigmaIndex]);
    }
  }*/
  
  protected void operate(SymmetryOperation latticePreservingOp) {
    Species[] speciesToMove = new Species[this.numSigmaSites()];
    for (int sigmaIndex= 0; sigmaIndex < speciesToMove.length; sigmaIndex++) {
      speciesToMove[sigmaIndex] = this.getSigmaSite(sigmaIndex).getSpecies();
    }
    
    for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
      Structure.Site sourceSite = this.getSigmaSite(sigmaIndex);
      Coordinates oppedCoords = latticePreservingOp.operate(sourceSite.getCoords());
      m_SuperStructure.getSite(oppedCoords).setSpecies(speciesToMove[sigmaIndex]);
    }
  }
  
  public int[] getSigmaStatesFromFactorStates(int[] factorStates, int factorNum, int[] template) {
    this.getFactorDecorators();
    if (template == null) {
      template = new int[this.numSigmaSites()];
    }
    int[] map = m_FactorMaps[factorNum];
    for (int sigmaIndex = 0; sigmaIndex < template.length; sigmaIndex++) {
      template[sigmaIndex] = factorStates[map[sigmaIndex]];
    }
    return template;
  }
  
  public SuperStructureDecorator[] getFactorDecorators() {
    
    if (m_FactorDecorators == null) {
      SpaceGroup spaceGroup = this.getPrimDecorationTemplate().getSpaceGroup();
      int[][][] latticePointOperators = spaceGroup.getLatticePointOperators(true);
      //SuperLattice superLattice = spaceGroup.getLattice().getSuperLattice(m_SuperStructure.getSuperLattice());
      //SuperLattice[] subLattices = superLattice.getDistinctFactorLattices(latticePointOperators);
      //SuperLattice[] subLattices = m_SuperStructure.getSuperLattice().getDistinctFactorLattices(latticePointOperators);
      SuperLattice[] subLattices = m_SuperStructureFromPrim.getSuperLattice().getDistinctFactorLattices(latticePointOperators);
      //System.out.println(subLattices.length);
      m_FactorDecorators = new SuperStructureDecorator[subLattices.length + 1];
      m_FactorMaps = new int[subLattices.length + 1][];
      m_FactorStates = new int[subLattices.length + 1][];
      for (int decNum = 0; decNum < subLattices.length; decNum++) {
        m_FactorDecorators[decNum] = this.newDecoratorFromPrimTemplate(subLattices[decNum].getCanonicalToDirect());
        m_FactorMaps[decNum] = new int[this.numSigmaSites()];
        SuperStructure subStructure = m_FactorDecorators[decNum].getSuperStructure();
        for (int sigmaIndex = 0; sigmaIndex < this.numSigmaSites(); sigmaIndex++) {
          Coordinates siteCoords = this.getSigmaSite(sigmaIndex).getCoords();
          int subSiteNum = subStructure.getSiteIndex(siteCoords);
          int subSigmaIndex = m_FactorDecorators[decNum].getSigmaIndex(subSiteNum);
          m_FactorMaps[decNum][sigmaIndex] = subSigmaIndex;
        }
        m_FactorStates[decNum] = new int[m_FactorDecorators[decNum].numSigmaSites()];
      }
      m_FactorDecorators[subLattices.length]= this;
      m_FactorMaps[subLattices.length] = ArrayUtils.getIdentityPermutation(this.numSigmaSites());
      m_FactorStates[subLattices.length] = new int[this.numSigmaSites()];
    }
    
    return (SuperStructureDecorator[]) ArrayUtils.copyArray(m_FactorDecorators);
  }
  
  public ArrayIndexer.Filter[] getIrreducibleStatesFilters() {
    
    if (m_IrreducibleStatesFilters == null) {
      int[][] permutations = this.getPermutations();
      m_IrreducibleStatesFilters = new ArrayIndexer.Filter[0];
      for (int permNum = 0; permNum < permutations.length; permNum++) {
        if (!m_IsPermutationTranslation[permNum]) {continue;}
        if (permNum == m_IdentityPermutationIndex) {continue;}
        ArrayIndexer.Filter newFilter = new IrreducibleStatesFilter(permutations[permNum]);
        m_IrreducibleStatesFilters = (ArrayIndexer.Filter[]) ArrayUtils.appendElement(m_IrreducibleStatesFilters, newFilter);
      }
    }
    
    return (ArrayIndexer.Filter[]) ArrayUtils.copyArray(m_IrreducibleStatesFilters);
    
  }
  
  public ArrayIndexer.Filter[] getUnseenStatesFilters() {
    
    if (m_SeenStatesFilters == null) {
      int[][] permutations = this.getPermutations();
      m_SeenStatesFilters = new ArrayIndexer.Filter[permutations.length];
      for (int filterNum = 0; filterNum < m_SeenStatesFilters.length; filterNum++) {
        m_SeenStatesFilters[filterNum] = new PermutationFilter(permutations[filterNum]);
      }
    }
    
    return (ArrayIndexer.Filter[]) ArrayUtils.copyArray(m_SeenStatesFilters);
    
  }
  
  public ArrayIndexer.Filter getPermutationFilter(int[] permutation) {
    return new PermutationFilter(permutation);
  }
  
  public ArrayIndexer.Filter getChargeBalanceFilter(double tolerance) {
    
    double nonSigmaCharge = 0;
    for (int siteNum = 0; siteNum < m_SuperStructure.numDefiningSites(); siteNum++) {
      if (this.isSigmaSite(siteNum)) {continue;}
      nonSigmaCharge += m_SuperStructure.getSiteSpecies(siteNum).getOxidationState();
    }
    return new ChargeBalanceFilter(nonSigmaCharge, tolerance);
    
  }
  
  protected class IrreducibleStatesFilter implements ArrayIndexer.Filter {
    
    private int[] m_Permutation;
    
    public IrreducibleStatesFilter(int[] translationPermutation) {
      m_Permutation = ArrayUtils.copyArray(translationPermutation);
    }
    
    public int getBranchIndex(int[] currentStates) {
      
      for (int sigmaIndex = m_Permutation.length - 1; sigmaIndex >= 0; sigmaIndex--) {
        int permState = currentStates[m_Permutation[sigmaIndex]];
        int origState = currentStates[sigmaIndex];
        if (origState != permState) {
          return -1;
        }
      }
      
      return 0;
    }
    
  }
  
  protected class PermutationFilter implements ArrayIndexer.Filter {
    
    private int[] m_Permutation;
    
    public PermutationFilter(int[] permutation) {
      m_Permutation = ArrayUtils.copyArray(permutation);
    }

    public int getBranchIndex(int[] currentStates) {
        
      // We try to figure out which sigmaIndex should next be incremented
      int minSigmaToIncrement = m_Permutation.length - 1;
      for (int sigmaIndex = m_Permutation.length - 1; sigmaIndex >= 0; sigmaIndex--) {
        int permState = currentStates[m_Permutation[sigmaIndex]];
        int origState = currentStates[sigmaIndex];
        if (origState < permState) {
          return -1;
        }
        if (m_Permutation[sigmaIndex] < m_Permutation[minSigmaToIncrement]) {
          minSigmaToIncrement = sigmaIndex;
        }
        if (permState < origState) {
          // Commented this out because a filter should not change the states.
          /*if (minSigmaToIncrement == sigmaIndex) {
            // This is like a branchend, and sets permState = origState - 1.
            currentStates[permutation[sigmaIndex]] = origState - 1;
          }*/
          return m_Permutation[minSigmaToIncrement];
        }

      }
            
      return -1;
    }
    
  }
  
  /*
  public SuperStructure getSuperStructure() {
    return m_SuperStructure;
  }*/
  
  /*
  public BigInteger getDecorationID(IStructureData sourceStructure) {
    m_SiteDecorator.decorateFromStructure(new Structure(sourceStructure));
    try {
      return m_SiteDecorator.readDecorationID();
    } catch (IllegalOccupationException e) {
      return null;
    }
  }*/
  
  /*
  public BigInteger getMinEquivalentDecorationID(SpaceGroup spaceGroup, BigInteger decorationID) {
    
    BigInteger minID = decorationID;
    
    try {
      BigInteger[] decIDs = this.getEquivalentDecorationIDs(spaceGroup, decorationID);
      for (int idNum = 0; idNum < decIDs.length; idNum++) {
        if (decIDs[idNum].compareTo(minID) < 0) {minID = decIDs[idNum];}
      }
    } catch (IllegalOccupationException e) {
      return null;
    }
    return minID;
  }*/
  
  /*
  public SiteDecorator getSiteDecorator() {
    return m_SiteDecorator;
  }*/
  
  /*
  public int[][] getSuperToDirect() {
    //return ArrayUtils.copyArray(m_SuperToDirect);
    return m_SuperStructure.getSuperToDirect();
  }*/
  
  /*
  public int getSize() {
    return SuperLatticeGenerator.getSize(this.getSuperToDirect());
  }
  
  public int getLatticeIndex() {
    int[][] superToDirect= this.getSuperToDirect();
    SuperLatticeGenerator generator = new SuperLatticeGenerator(this.getSize(), superToDirect.length);
    return generator.getLatticeIndex(superToDirect);
  }*/

}
