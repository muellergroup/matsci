package matsci.structure.superstructure;

import java.math.BigInteger;

//import matsci.Species;
//import matsci.location.Coordinates;
//import matsci.location.Vector;
import matsci.structure.*;
import matsci.structure.decorate.DecorationTemplate;
//import matsci.structure.decorate.function.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class SuperData {

  private Structure m_PrimStructure;
  private int m_Size;
  private int m_LatticeNumber;
  private BigInteger m_DecorationID = BigInteger.ZERO;
  private String m_Description;

  public SuperData(Structure primStructure, int size, int latticeNumber) {
  	m_PrimStructure = primStructure;
  	m_Size = size;
  	m_LatticeNumber = latticeNumber;
  }
  
  public void setDescription(String description) {
    m_Description = description;
  }
  
  public String getDescription() {
    return m_Description;
  }

  public Structure getPrimStructure() {
    return m_PrimStructure;
  }

  public int getSize() {
    return m_Size;
  }

  public int getLatticeNumber() {
    return m_LatticeNumber;
  }

  public void setDecorationID(BigInteger decorationID) {
    m_DecorationID = decorationID;
  }

  public BigInteger getDecorationID() {
    return m_DecorationID;
  }

  /*
  public SuperStructure newSuperStructure() {
    return new SuperStructure(this);
  }*/
  
  public SuperStructure newSuperStructure(DecorationTemplate ce) {
    SuperLatticeGenerator latticeGenerator = new SuperLatticeGenerator(m_Size, m_PrimStructure.numPeriodicDimensions());
    SuperStructureDecorator superDecorator = new SuperStructureDecorator(ce, latticeGenerator.getSuperToDirect(m_LatticeNumber));
    superDecorator.getSuperStructure().setDescription(this.getDescription());
    superDecorator.decorate(this.getDecorationID());
    return superDecorator.getSuperStructure();
    
    /*superDecorator.getSiteDecorator().decorate(m_DecorationID);
    return superDecorator.getSuperStructure();*/
  }
  
  public String toString() {
    return m_Size + "."  + m_LatticeNumber + "." + m_DecorationID;
  }
}