/*
 * Created on Apr 24, 2006
 *
 */
package matsci.structure.superstructure;

import java.util.*;

import matsci.Species;
import matsci.io.app.log.Status;
import matsci.location.basis.*;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.structure.BravaisLattice;
import matsci.structure.ParallelCell;
import matsci.structure.Structure;
import matsci.structure.StructureBuilder;
import matsci.util.MSMath;
import matsci.util.arrays.*;

public class SuperLattice extends BravaisLattice {

  private final BravaisLattice m_ParentLattice;
  
  private final ArrayIndexer m_PrimCellIterator;
  private final int m_NumPrimCells;

  private final int[][] m_SuperToDirect;
  private final int[][] m_CanonicalToDirect;
  private final int m_LatticeIndex;
  
  private final HashMap m_LatticePreservingOpNums = new HashMap();
  private SuperLattice[] m_FactorLattices = null; // Lazy load
  
  public SuperLattice(Vector[] primVectors, boolean[] isVectorPeriodic, int[][] superToDirect) {
    this(new BravaisLattice(primVectors, isVectorPeriodic), superToDirect);
  }
  
  public SuperLattice(BravaisLattice parentLattice, BravaisLattice superLattice) {
    this(parentLattice, parentLattice.getSuperToDirect(superLattice));
  }
  
  public SuperLattice(BravaisLattice parentLattice, int numPrimCells) {
    this(parentLattice, parentLattice.getCompactSuperToDirect(numPrimCells));
  }
  
  public SuperLattice(BravaisLattice parentLattice) {
    this(parentLattice, ArrayUtils.identityMatrix(parentLattice.numPeriodicVectors()));
  }
  
  public SuperLattice(BravaisLattice parentLattice, int[][] superToDirect) {
    super(getOriginCell(parentLattice, superToDirect));
    
    if (parentLattice.numPeriodicVectors() != superToDirect.length) {
      throw new RuntimeException("The number of periodic dimensions is different from the number of rows in the supercell matrix");
    }
    
    m_ParentLattice = parentLattice;
    /**
     * These are the vectors, in direct coordinates, that define this lattice.
     */
    m_SuperToDirect = ArrayUtils.copyArray(superToDirect);

    /**
     * The volume of this supercell in units of the volume of the primitive cell
     */
    //m_NumPrimCells = Math.abs(MSMath.determinant(superToDirect));
    m_NumPrimCells = SuperLatticeGenerator.getSize(superToDirect);

    /**
     * The canonical basis is equivalent to the given basis.  It is comprised of three vectors expressed in terms
     * of the primitive basis.  The vectors form a lower triangular matrix such that [0][0] > [1][0], [2][0];  [1][1] > [2][1];
     */
    //int numPeriodicDimensions = parentStructure.numPeriodicDimensions();
    //m_CanonicalToDirect = new int[numPeriodicDimensions][numPeriodicDimensions];
    //this.createCanonicalLattice();
    m_CanonicalToDirect = SuperLatticeGenerator.toCanonical(m_SuperToDirect);
    SuperLatticeGenerator generator = new SuperLatticeGenerator(m_NumPrimCells, m_CanonicalToDirect.length);
    m_LatticeIndex = generator.getLatticeIndex(m_CanonicalToDirect);

    int[] primCellLocations = new int[m_CanonicalToDirect.length];
    for (int dimNum = 0; dimNum < primCellLocations.length; dimNum++) {
      primCellLocations[dimNum] = m_CanonicalToDirect[dimNum][dimNum];
    }
    
    m_PrimCellIterator = new ArrayIndexer(primCellLocations);
  }
  
  protected static ParallelCell getOriginCell(BravaisLattice lattice, int[][] superToDirect) {
    int[] finiteVectors = lattice.getPeriodicIndices();

    Vector[] returnVectors = lattice.getCellVectors();

    for (int returnVecNum = 0; returnVecNum < finiteVectors.length; returnVecNum++) {
      int cellVecNum = finiteVectors[returnVecNum];
      returnVectors[cellVecNum] = Vector.getZero3DVector(); //new Vector(new double[] {0,0,0}, CartesianBasis.getInstance());
      for (int directVecNum = 0; directVecNum < finiteVectors.length; directVecNum++) {
        Vector directVector = lattice.getCellVector(finiteVectors[directVecNum]);
        returnVectors[cellVecNum] = 
          directVector.multiply(superToDirect[returnVecNum][directVecNum]).add(returnVectors[cellVecNum]);
      }
    }
    return new ParallelCell(lattice.getOrigin(), returnVectors, lattice.getDimensionPeriodicity());
  }
  
  public SuperStructure getRepresentativeStructure(Species pointSpecies) {
    return new SuperStructure(this.getPrimLattice().getRepresentativeStructure(pointSpecies), this.getSuperToDirect());
  }
  
  /**
  *
  * @return A square int array that contains the lattice vectors that translate coordinates in a system defined by the
  * lattice vectors of the supercell into direct (primitive cell) coordinates.  Note you need to right-multiply a
  * vector times this matrix to make the translation.  Currently returns the interior copy of the matrix, so do not modify
  * the returned matrix!
  */
  public int[][] getSuperToDirect() {
    return ArrayUtils.copyArray(m_SuperToDirect);
  }

 /**
  *
  * @return A 3x3 int array that contains the lattice vectors that translate coordinates in a system defined by the
  * canonical lattice vectors of this supercell into direct (primitive cell) coordinates.  Note you need to right-multiply a
  * vector times this matrix to make the translation.  Currently returns the interior copy of the matrix, so do not modify
  * the returned matrix!
  */
  public int[][] getCanonicalToDirect() {
    return ArrayUtils.copyArray(m_CanonicalToDirect);
  }
 
  public int getLatticeIndex() {
    return m_LatticeIndex;
  }

 /**
  *
  * @return The volume of this supercell in units of the volume of the primitive cell.
  */
  public int numPrimCells() {
    return m_NumPrimCells;
  }

  /*
  private void createCanonicalLattice() {
    
    m_CanonicalToDirect = SuperLatticeGenerator.toCanonical(m_SuperToDirect);
    SuperLatticeGenerator generator = new SuperLatticeGenerator(m_NumPrimCells, m_CanonicalToDirect.length);
    m_LatticeIndex = generator.getLatticeIndex(m_CanonicalToDirect);

    int[] primCellLocations = new int[m_CanonicalToDirect.length];
    for (int dimNum = 0; dimNum < primCellLocations.length; dimNum++) {
      primCellLocations[dimNum] = m_CanonicalToDirect[dimNum][dimNum];
    }
    
    m_PrimCellIterator = new ArrayIndexer(primCellLocations);
  }*/

  public int[] getInnerPrimCell(int[] primCellLocation) {
    
    int[] innerIntCoords = new int[primCellLocation.length];
    for (int coordNum = primCellLocation.length - 1; coordNum >= 0; coordNum--) {
      int innerCoord = primCellLocation[coordNum];
      for (int row = coordNum + 1; row < m_CanonicalToDirect.length; row++) {
        
        int rowDimensionSize = m_PrimCellIterator.getDimensionSize(row);
        int shiftedCoord = innerIntCoords[row];
        if (shiftedCoord < 0) {  // Account for how Java handles negatives in integer division
          innerCoord -= ((shiftedCoord - rowDimensionSize + 1) / rowDimensionSize) * m_CanonicalToDirect[row][coordNum];
        } else {
          innerCoord -= (shiftedCoord / rowDimensionSize) * m_CanonicalToDirect[row][coordNum];
        }
      }
      innerIntCoords[coordNum] = innerCoord;
    }
    
    // Now take the modulo of the coordinates
    for (int coordNum = 0; coordNum < innerIntCoords.length; coordNum++) {
      int innerCoord = innerIntCoords[coordNum] % m_PrimCellIterator.getDimensionSize(coordNum);
      innerIntCoords[coordNum] = innerCoord < 0 ? innerCoord + m_PrimCellIterator.getDimensionSize(coordNum) : innerCoord;
    }
    
    return innerIntCoords;
  }
  
  public int[] getDimensionSizes() {
    return m_PrimCellIterator.getDimensionSizes();
  }
  
  public Vector getPrimCellVector(int primCellNumber) {
    return new Vector(this.getOrigin(), this.getPrimCellOriginCoords(primCellNumber));
  }
  
  public Coordinates getPrimCellOriginCoords(int primCellNumber) {
    int[] primCellOrigin = m_PrimCellIterator.splitValue(primCellNumber);
    Vector[] parentVectors = m_ParentLattice.getPeriodicVectors();
    Coordinates head = this.getOrigin();
    for (int vecNum = 0; vecNum < parentVectors.length; vecNum++) {
      head = head.translateBy(parentVectors[vecNum].multiply(primCellOrigin[vecNum]));
    }
    return head;
  }

  public int[] getPrimCellOrigin(int primCellNumber) {
    return m_PrimCellIterator.splitValue(primCellNumber);
  }
  
  public int getPrimCellNumber(int[] primCellOrigin) {
    return m_PrimCellIterator.joinValuesBounded(this.getInnerPrimCell(primCellOrigin));
  }
  
  public int getNearbyPrimCellOriginNumber(Coordinates coords) {
    //int[] primCellOriginArray = m_ParentLattice.softFloor(coords);
    // Adjust for differences in the origin
    //coords = m_ParentLattice.getRelativeCoords(coords);
    double[] coordArray = coords.getCoordArray(m_ParentLattice.getLatticeBasis());
    int[] primCellOriginArray = new int[this.numPeriodicVectors()];
    int returnIndex = 0;
    for (int vecNum = 0; vecNum < coordArray.length; vecNum++) {
      if (!this.isDimensionPeriodic(vecNum)) {continue;}
      primCellOriginArray[returnIndex++] = (int) Math.round(coordArray[vecNum]);
    }
    return this.getPrimCellNumber(primCellOriginArray);
    
  }
  
  // I got rid of this because the hard remainder could cause confusion. 
  // Hopefully its use can be avoided
  /*public int getContainingPrimCellNumber(Coordinates coords) {
    //int[] primCellOriginArray = m_ParentLattice.softFloor(coords);
    int[] primCellOriginArray = m_ParentLattice.hardFloor(coords);
    return this.getPrimCellNumber(primCellOriginArray);
  }*/
  
  public BravaisLattice getPrimLattice() {
    return m_ParentLattice;
  }
  
  public SuperLattice scaleToSize(double cellVolume) {
    
    double parentVolume = cellVolume / this.numPrimCells();
    BravaisLattice scaledParent = this.getPrimLattice().scaleToSize(parentVolume);
    return new SuperLattice(scaledParent, this.getSuperToDirect());
    
  }
  
  public SuperLattice[] getDistinctFactorLattices(int[][][] pointOperators) {
    
    if (pointOperators == null) {
      pointOperators = new int[0][][];
    }
    
    if (m_FactorLattices == null) {
      
      m_FactorLattices = new SuperLattice[0];
      int size = this.numPrimCells();

      int[] factors = MSMath.factor(size);
      for (int factorNum = 0; factorNum < factors.length; factorNum++) {
        int subSize = factors[factorNum];
        if (subSize == size) {continue;}
        SuperLatticeGenerator generator = new SuperLatticeGenerator(subSize, this.numPeriodicVectors());
        int[][][] subLatticeCandidates = generator.generateSuperLattices();
        for (int candidateNum = 0; candidateNum < subLatticeCandidates.length; candidateNum++) {
          int[][] candidate = subLatticeCandidates[candidateNum];
          //MatrixInverter inverter = new MatrixInverter(candidate);
          //double[][] inverse = inverter.getInverseArray();
          double[][] inverse = MSMath.simpleInverse(candidate);
          double[][] transformation = MSMath.matrixMultiply(m_CanonicalToDirect, inverse);
          int[][] intTransformation = new int[transformation.length][];
          for (int rowNum = 0; rowNum < transformation.length; rowNum++) {
            intTransformation[rowNum] = new int[transformation[rowNum].length];
            for (int colNum = 0; colNum < intTransformation[rowNum].length; colNum++) {
              intTransformation[rowNum][colNum] = (int) Math.round(transformation[rowNum][colNum]);
            }
          }
          int[][] newCanonialToDirect = MSMath.matrixMultiply(intTransformation, candidate);
          if (!ArrayUtils.equals(m_CanonicalToDirect, newCanonialToDirect)) {
            continue;
          }
          boolean seenBefore = false;
          for (int knownLatticeNum = 0; knownLatticeNum < m_FactorLattices.length; knownLatticeNum++) {
            SuperLattice knownLattice = m_FactorLattices[knownLatticeNum];
            if (knownLattice.numPrimCells() != subSize) {continue;}
            int[][] knownSuperToDirect = knownLattice.getCanonicalToDirect();
            if (generator.equivalentLattices(knownSuperToDirect, candidate, pointOperators)) {
              seenBefore = true;
              break;
            }
          }
          if (seenBefore) {continue;}
          SuperLattice subLattice = new SuperLattice(m_ParentLattice, candidate);
          m_FactorLattices = (SuperLattice[]) ArrayUtils.appendElement(m_FactorLattices, subLattice);
        }
      }
      
    }
    
    return (SuperLattice[]) ArrayUtils.copyArray(m_FactorLattices);
    
  }
  
  public int[] getLatticePreservingOpNums(SpaceGroup spaceGroup) {
    
    int[] returnArray = (int[]) m_LatticePreservingOpNums.get(spaceGroup);
    if (returnArray != null) {return returnArray;}
    
    int[][] superToDirect = m_SuperToDirect;
    if (spaceGroup.getLattice() != m_ParentLattice) {
      //throw new RuntimeException("Can only get lattice-preserving operations for a space group defined for the parent lattice of this lattice");
      superToDirect = spaceGroup.getLattice().getSuperToDirect(this);
      SuperLattice superLattice = new SuperLattice(spaceGroup.getLattice(), superToDirect); 
      returnArray = superLattice.getLatticePreservingOpNums(spaceGroup);
      m_LatticePreservingOpNums.put(spaceGroup, returnArray);
      return returnArray;
    }
    int size = SuperLatticeGenerator.getSize(m_CanonicalToDirect);
    //int size = SuperLatticeGenerator.getSize(superToDirect);
    SuperLatticeGenerator latticeGenerator = new SuperLatticeGenerator(size, superToDirect.length); // TODO consider caching this
    returnArray = new int[spaceGroup.numOperations()];
    int[][] oppedSuperToDirect = new int[superToDirect.length][superToDirect.length];
    int returnIndex =0;
    for (int opNum = 0; opNum < spaceGroup.numOperations(); opNum++) {
      int[][] pointOperator = spaceGroup.getLatticePointOperator(opNum);
      if (pointOperator == null) {continue;}
      SuperLatticeGenerator.operateSuperToDirect(superToDirect, pointOperator, oppedSuperToDirect);
      int oppedIndex = latticeGenerator.getFastLatticeIndex(oppedSuperToDirect);
      if (oppedIndex == m_LatticeIndex) {
        returnArray[returnIndex++] = opNum;
      }
    }
    
    returnArray = ArrayUtils.truncateArray(returnArray, returnIndex);
    m_LatticePreservingOpNums.put(spaceGroup, returnArray);
    return returnArray;
  }
  
  public SpaceGroup getLatticePreservingSpaceGroup(SpaceGroup spaceGroup) {
    int[] latticePreservingOps = this.getLatticePreservingOpNums(spaceGroup);
    SymmetryOperation[] preservingOperations = new SymmetryOperation[latticePreservingOps.length];
    for (int opNum = 0; opNum < latticePreservingOps.length; opNum++) {
      preservingOperations[opNum] = spaceGroup.getOperation(latticePreservingOps[opNum]); 
    }
    SpaceGroup preservingGroup = new SpaceGroup(preservingOperations, spaceGroup.getLattice());
    SymmetryOperation[] factorOperations = preservingGroup.getFactorOperations(this);
    return new SpaceGroup(factorOperations, this);
  }
  
}
