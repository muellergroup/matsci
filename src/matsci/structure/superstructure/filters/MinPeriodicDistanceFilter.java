/*
 * Created on Feb 18, 2016
 *
 */
package matsci.structure.superstructure.filters;

import java.util.HashMap;

import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.superstructure.SuperLattice;
import matsci.structure.superstructure.SuperLatticeGenerator;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayUtils;

public class MinPeriodicDistanceFilter implements ISuperLatticeFilter {
  
  private BravaisLattice m_Lattice;
  private double m_MinAllowedDistance;
  private double[][] m_CartesianVectors;
  private boolean m_UpdateMinAllowedDistance;
  
  public MinPeriodicDistanceFilter(BravaisLattice lattice, double minAllowedDistance, boolean updateMinAllowedDistance) {
    
    m_Lattice = lattice;
    m_MinAllowedDistance = minAllowedDistance;
    m_UpdateMinAllowedDistance = updateMinAllowedDistance;
    m_CartesianVectors = new double[m_Lattice.numPeriodicVectors()][];
    for (int vecNum = 0; vecNum < m_CartesianVectors.length; vecNum++) {
      m_CartesianVectors[vecNum] = m_Lattice.getPeriodicVector(vecNum).getCartesianDirection();
    }
  }
  
  private boolean fastScreen(int[][] superToDirect, double minAllowedDistance) {
    for (int vecNum = 0; vecNum < superToDirect.length; vecNum++) {
      int[] row = superToDirect[vecNum];
      double[] cartVector = MSMath.vectorTimesMatrix(row, m_CartesianVectors);
      double mag = MSMath.magnitude(cartVector);
      if (mag < minAllowedDistance) {
        return false;
      }
    }
    return true;
  }
  
  private int mediumScreen(int latticeIndex, int[][] superToDirect, double minAllowedDistance) {
    
    double[][] superVectors = MSMath.matrixMultiply(superToDirect, m_CartesianVectors);
    double minAllowedSq = minAllowedDistance * minAllowedDistance;
    
    double precision = 1E-7;
    boolean minimizing = true;
    while (minimizing) {
      minimizing = false;
      for (int vecNum = 0; vecNum < superVectors.length; vecNum++) {
        double[] vector = superVectors[vecNum];
        double magSq = MSMath.dotProduct(vector, vector);
        if (magSq < minAllowedSq) {
          return latticeIndex + 1;
        }
        for (int increment = 1; increment < superVectors.length; increment++) {
          int vecNum2 = (vecNum + increment) % superVectors.length;
          double[] vector2 = superVectors[vecNum2];
          double innerProduct = MSMath.dotProduct(vector, vector2) / magSq;
          innerProduct = MSMath.roundWithPrecision(innerProduct, precision);
          double shift = Math.round(innerProduct);
          minimizing |= (shift != 0);
          for (int dimNum = 0; dimNum < vector2.length; dimNum++) {
            vector2[dimNum] -= vector[dimNum] * shift;
          }
        }
      }
    }
    
    return latticeIndex;
  }

  public int screenLatticeIndex(int latticeIndex, int[][] superToDirect) {

    double minAllowedDistance = m_MinAllowedDistance - CartesianBasis.getPrecision();
    
    // This is just a quick screen based on vector lengths.
    /*if (!fastScreen(superToDirect, minAllowedDistance)) {
      return latticeIndex + 1;
    }*/
    
    // This is much more effective.
    int nextLatticeIndex = mediumScreen(latticeIndex, superToDirect, minAllowedDistance);
    if (nextLatticeIndex != latticeIndex) {
      return nextLatticeIndex;
    }
    
    SuperLattice superLattice = new SuperLattice(m_Lattice, superToDirect);
        
    double minDistance = superLattice.getMinPeriodicDistance();
    if (minDistance < minAllowedDistance) {
      return latticeIndex + 1;
    }
    if (m_UpdateMinAllowedDistance) {m_MinAllowedDistance = minDistance;}
    return latticeIndex;
  }

}
