/*
 * Created on Feb 18, 2016
 *
 */
package matsci.structure.superstructure.filters;

import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.superstructure.SuperLatticeGenerator;
import matsci.util.arrays.ArrayUtils;

public class RemoveDuplicateFilter implements ISuperLatticeFilter {
  
  private int[][][] m_PointOperators;
  private SuperLatticeGenerator m_SuperLatticeGenerator;
  private int[][] m_OppedSuperToDirect;

  public RemoveDuplicateFilter(SuperLatticeGenerator generator, SpaceGroup spaceGroup) {
    
    m_PointOperators = spaceGroup.getLatticePointOperators(true);
    m_SuperLatticeGenerator = generator;
    m_OppedSuperToDirect = new int[generator.numPeriodicDimensions()][generator.numPeriodicDimensions()];
    
  }
  
  public RemoveDuplicateFilter(SuperLatticeGenerator generator, int[][][] pointOperators) {
    
    m_PointOperators = ArrayUtils.copyArray(pointOperators);
    m_SuperLatticeGenerator = generator;
    m_OppedSuperToDirect = new int[generator.numPeriodicDimensions()][generator.numPeriodicDimensions()];
    
  }


  public int screenLatticeIndex(int latticeIndex, int[][] superToDirect) {
    
    for (int opNum = 0; opNum < m_PointOperators.length; opNum++) {
      int[][] pointOperator = m_PointOperators[opNum];
      SuperLatticeGenerator.operateSuperToDirect(superToDirect, pointOperator, m_OppedSuperToDirect);
      int oppedIndex = m_SuperLatticeGenerator.getFastLatticeIndex(m_OppedSuperToDirect);
      if (oppedIndex < latticeIndex) {
        return latticeIndex + 1;
      }
    }
    return latticeIndex;
    
  }

}
