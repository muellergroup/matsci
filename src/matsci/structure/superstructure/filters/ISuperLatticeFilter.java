/*
 * Created on Feb 18, 2016
 *
 */
package matsci.structure.superstructure.filters;

public interface ISuperLatticeFilter {
  
  public int screenLatticeIndex(int latticeIndex, int[][] superToDirect);

}
