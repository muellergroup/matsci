/*
 * Created on Feb 18, 2016
 *
 */
package matsci.structure.superstructure.filters;

import java.util.Arrays;

import matsci.location.basis.CartesianBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.superstructure.SuperLattice;

public class MostCompactFilter implements ISuperLatticeFilter {

  private BravaisLattice m_Lattice;
  private double m_MaxKnownDistance = 0;
  private SuperLattice m_BestKnownLattice; 
  private boolean m_BlockAll;
  private boolean m_UseMinOrthogonalDistance = false;
  
  public MostCompactFilter(BravaisLattice lattice, boolean blockAll, boolean useMinOrthogonalDistance) {
    m_Lattice = lattice;
    m_BlockAll = blockAll;
    m_UseMinOrthogonalDistance = m_UseMinOrthogonalDistance;
  }
  
  public MostCompactFilter(BravaisLattice lattice, boolean blockAll) {
    this(lattice, blockAll, false);
  }
  
  
  public int screenLatticeIndex(int latticeIndex, int[][] superToDirect) {
    
    SuperLattice superLattice = new SuperLattice(m_Lattice, superToDirect);
    double distance = superLattice.getMinPeriodicDistance();
    int returnValue = m_BlockAll ? latticeIndex + 1 : latticeIndex;
    if (distance < m_MaxKnownDistance - CartesianBasis.getPrecision()) {
      return returnValue;
    } else if (distance <= m_MaxKnownDistance + CartesianBasis.getPrecision()) {
      // Tiebreaker
      if (!superLattice.isMoreCompactThan(m_BestKnownLattice)) {
        return returnValue;
      }
    }
    
    m_BestKnownLattice = superLattice;
    m_MaxKnownDistance = distance;

    return returnValue;
  }
  
  public SuperLattice getBestKnownLattice() {
    BravaisLattice primLattice = m_BestKnownLattice.getPrimLattice();
    return primLattice.getSuperLattice(m_BestKnownLattice.getCompactLattice());
  }


}
