/*
 * Created on Feb 18, 2016
 *
 */
package matsci.structure.superstructure.filters;

import java.util.Arrays;

import matsci.location.basis.CartesianBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.superstructure.SuperLattice;

public class InverseCompactFilter implements ISuperLatticeFilter {

  private BravaisLattice m_Lattice;
  private double m_MaxKnownDistance = 0;
  private SuperLattice m_BestKnownLattice; 
  private boolean m_BlockAll;
  
  public InverseCompactFilter(BravaisLattice lattice, boolean blockAll) {
    m_Lattice = lattice;
    m_BlockAll = blockAll;
  }
  
  public int screenLatticeIndex(int latticeIndex, int[][] superToDirect) {
    
    SuperLattice superLattice = new SuperLattice(m_Lattice, superToDirect);
    double distance = superLattice.getInverseLattice().getMinPeriodicDistance();
    int returnValue = m_BlockAll ? latticeIndex + 1 : latticeIndex;
    if (distance < m_MaxKnownDistance - CartesianBasis.getPrecision()) {
      return returnValue;
    } else if (distance <= m_MaxKnownDistance + CartesianBasis.getPrecision()) {
      // Tiebreaker
      if (!superLattice.getInverseLattice().isMoreCompactThan(m_BestKnownLattice.getInverseLattice())) {
        return returnValue;
      }
    }
    
    m_BestKnownLattice = superLattice;
    m_MaxKnownDistance = distance;

    return returnValue;
  }
  
  public SuperLattice getBestKnownLattice() {
    BravaisLattice primLattice = m_BestKnownLattice.getPrimLattice();
    return primLattice.getSuperLattice(m_BestKnownLattice.getCompactLattice());
  }

}
