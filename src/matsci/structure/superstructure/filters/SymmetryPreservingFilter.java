/*
 * Created on Feb 18, 2016
 *
 */
package matsci.structure.superstructure.filters;

import java.util.Arrays;

import matsci.location.symmetry.operations.SpaceGroup;
import matsci.structure.superstructure.SuperLatticeGenerator;
import matsci.util.arrays.ArrayUtils;

public class SymmetryPreservingFilter implements ISuperLatticeFilter {

  private int[][][] m_PointOperators;
  private int[][] m_OppedSuperToDirect;
  
  public SymmetryPreservingFilter(SpaceGroup spaceGroup) {
    
    m_PointOperators = spaceGroup.getLatticePointOperators(true, true, true);
    //m_PointOperators = spaceGroup.getLatticePointOperators(true);
    
    m_OppedSuperToDirect = new int[spaceGroup.getLattice().numPeriodicVectors()][spaceGroup.getLattice().numPeriodicVectors()];
    
  }
  
  public SymmetryPreservingFilter(SuperLatticeGenerator generator, int[][][] pointOperators) {
    
    m_PointOperators = ArrayUtils.copyArray(pointOperators);
    m_OppedSuperToDirect = new int[generator.numPeriodicDimensions()][generator.numPeriodicDimensions()];
    
  }

  public int screenLatticeIndex(int latticeIndex, int[][] canonicalSuperToDirect) {
    
    if (isSymmetryPreserving(canonicalSuperToDirect)) {
      return latticeIndex;
    }
    return latticeIndex + 1;
    
  }
  

  public boolean isSymmetryPreserving(int[][] canonicalSuperToDirect) {
    
    for (int opNum = 0; opNum < m_PointOperators.length; opNum++) {
      int[][] pointOperator = m_PointOperators[opNum];
      SuperLatticeGenerator.operateSuperToDirect(canonicalSuperToDirect, pointOperator, m_OppedSuperToDirect);
      SuperLatticeGenerator.toCanonicalFast(m_OppedSuperToDirect);
      for (int row = 0; row < canonicalSuperToDirect.length; row++) {
        if (!Arrays.equals(canonicalSuperToDirect[row], m_OppedSuperToDirect[row])) {
          return false;
        }
      }
      /*int oppedIndex = m_SuperLatticeGenerator.getFastLatticeIndex(m_OppedSuperToDirect);
      if (oppedIndex != latticeIndex) {
        return latticeIndex + 1;
      }*/
    }
    return true;
    
  }
  
}
