/*
 * Created on Feb 18, 2016
 *
 */
package matsci.structure.superstructure.filters;

import matsci.location.basis.CartesianBasis;
import matsci.structure.BravaisLattice;
import matsci.structure.superstructure.SuperLattice;

public class MinOrthogonalDistanceFilter implements ISuperLatticeFilter {
    
    private BravaisLattice m_Lattice;
    private double m_MinAllowedDistance;

    public MinOrthogonalDistanceFilter(BravaisLattice lattice, double minAllowedDistance) {
      m_Lattice = lattice;
      m_MinAllowedDistance = minAllowedDistance;
    }

    public int screenLatticeIndex(int latticeIndex, int[][] superToDirect) {
      BravaisLattice superLattice = new SuperLattice(m_Lattice, superToDirect);
      double minPointDistance = superLattice.getMinOrthogonalDistance();
      if (minPointDistance < m_MinAllowedDistance - CartesianBasis.getPrecision()) {
        return latticeIndex + 1;
      }
      return latticeIndex;
    }

}
