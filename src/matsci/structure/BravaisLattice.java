/*
 * Created on Jan 31, 2005
 *
 */
package matsci.structure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import matsci.Species;
import matsci.io.app.log.Status;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.AbstractBasis;
import matsci.location.basis.AbstractLinearBasis;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.location.symmetry.operations.OperationGroup;
import matsci.location.symmetry.operations.SpaceGroup;
import matsci.location.symmetry.operations.SymmetryOperation;
import matsci.location.symmetry.operations.SpaceGroup.CrystalSystem;
import matsci.structure.superstructure.*;
import matsci.structure.superstructure.filters.ISuperLatticeFilter;
import matsci.structure.superstructure.filters.InverseCompactFilter;
import matsci.structure.superstructure.filters.MostCompactFilter;
import matsci.structure.symmetry.LatticeSymmetryFinder;
import matsci.util.arrays.*;
import matsci.util.*;

/**
 * @author Tim Mueller
 *
 */
public class BravaisLattice {

  private ParallelCell m_OriginCell;
  private Vector[] m_CartesianNormals;
  
  private BravaisLattice m_CompactLattice;
  private BravaisLattice m_QuickCompactLattice;
  
  private OperationGroup m_PointGroup;
  private SpaceGroup m_SpaceGroup;
  
  private double m_MinPeriodicDistance = Double.NaN;
  private Vector[] m_WignerSeitzProjectors = null;
  
  public static enum CrystalFamily {
    TRICLINIC_3D, MONOCLINIC_3D, ORTHORHOMBIC_3D, TETRAGONAL_3D, HEXAGONAL_3D, CUBIC_3D, OBLIQUE_2D, RECTANGULAR_2D, SQUARE_2D, HEXAGONAL_2D, LINEAR_1D, POINT_0D
  }

  protected CrystalFamily m_CrystalFamily = null;
  protected SuperLattice m_ConventionalLattice = null;
  
  public static enum LatticeSystem {
    TRICLINIC_3D, MONOCLINIC_3D, ORTHORHOMBIC_3D, TETRAGONAL_3D, HEXAGONAL_3D, RHOMBOHEDRAL_3D, CUBIC_3D, OBLIQUE_2D, RECTANGULAR_2D, SQUARE_2D, HEXAGONAL_2D, LINEAR_1D, POINT_0D
  }
  
  protected LatticeSystem m_LatticeSystem = null;
  
  public static enum BravaisType {
    CUBIC_3D, BCC_3D, FCC_3D, TETRAGONAL_3D, TETRAGONAL_I_3D, ORTHORHOMBIC_3D, ORTHORHOMBIC_I_3D, ORTHORHOMBIC_F_3D, ORTHORHOMBIC_C_3D, HEXAGONAL_3D, RHOMBOHEDRAL_3D, MONOCLINIC_3D, MONOCLINIC_C_3D, TRICLINIC_3D, OBLIQUE_2D, RECTANGULAR_2D, RHOMBIC_2D, SQUARE_2D, HEXAGONAL_2D, LINEAR_1D, POINT_0D
  }
  
  private BravaisType m_BravaisType = null;
  
  /**
   * Constructs a new Bravais Lattice from an origin cell object
   * 
   * @param originCell
   */
  public BravaisLattice(ParallelCell originCell) {
    m_OriginCell = originCell;
    //Vector[] latticeVectors = originCell.getFiniteVectors();
    //m_CartesianNormals = new Vector[latticeVectors.length];
    Vector[] cellVectors = originCell.getCellVectors();
    
    Vector[] orthogonalVectors = new Vector[cellVectors.length];
    m_CartesianNormals = new Vector[cellVectors.length];
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      for (int orthVecIncrement = 0; orthVecIncrement < cellVectors.length; orthVecIncrement++) {
        int orthVecNum = (vecNum + orthVecIncrement + 1) % cellVectors.length;
        Vector shiftedVector = cellVectors[orthVecNum];
        for (int prevIncrement = 0; prevIncrement < orthVecIncrement; prevIncrement++) {
          int prevOrthVecNum = (vecNum + prevIncrement + 1) % cellVectors.length;
          Vector basisVector = orthogonalVectors[prevOrthVecNum];
          double innerProduct = shiftedVector.innerProductCartesian(basisVector);
          shiftedVector = shiftedVector.subtract(basisVector.multiply(innerProduct)); 
        }
        orthogonalVectors[orthVecNum] = shiftedVector.unitVectorCartesian();
      }
      m_CartesianNormals[vecNum] = orthogonalVectors[vecNum];
    }

  }
  

  public BravaisLattice(Coordinates origin, Vector[] cellVectors, boolean[] areVectorsFinite) {
    this(new ParallelCell(origin, cellVectors, areVectorsFinite));
  }
  
  public BravaisLattice(Vector[] cellVectors, boolean[] areVectorsFinite) {
    this(new ParallelCell(cellVectors, areVectorsFinite));
  }
  
  public BravaisLattice(Vector[] cellVectors) {
    this(new ParallelCell(cellVectors, getTrueArray(cellVectors.length)));
  }
  
  public BravaisLattice(double[][] cellVectors, AbstractLinearBasis basis) {
    this(arrayToVectors(cellVectors, basis));
  }
  
  public static Vector[] arrayToVectors(double[][] vectorArrays, AbstractLinearBasis basis) {
    Vector[] returnArray = new Vector[vectorArrays.length];
    for (int vecNum = 0; vecNum < vectorArrays.length; vecNum++) {
      returnArray[vecNum] = new Vector(vectorArrays[vecNum], basis);
    }
    return returnArray;
  }
  
  private static boolean[] getTrueArray(int numDimensions) {
    boolean[] returnArray = new boolean[numDimensions];
    Arrays.fill(returnArray, true);
    return returnArray;
  }
  
  public Structure getRepresentativeStructure(Species pointSpecies) {
    StructureBuilder builder = new StructureBuilder();
    builder.setCellVectors(this.getCellVectors());
    builder.setVectorPeriodicity(this.getDimensionPeriodicity());
    builder.addSite(this.getOrigin(), pointSpecies);
    return new Structure(builder);
  }
  
  public BravaisLattice translateBy(Vector shift) {
    
    // TODO make this more intelligent
    Coordinates newOrigin = this.getOrigin().translateBy(shift);
    ParallelCell newOriginCell = new ParallelCell(newOrigin, m_OriginCell.getCellVectors(), m_OriginCell.areVectorsFinite());
    return new BravaisLattice(newOriginCell);
    
  }
  
  public BravaisLattice translateTo(Coordinates newOrigin) {
    
    // TODO make this more intelligent
    if (newOrigin.isCloseEnoughTo(this.getOrigin())) {return this;}
    ParallelCell newOriginCell = new ParallelCell(newOrigin, m_OriginCell.getCellVectors(), m_OriginCell.areVectorsFinite());
    return new BravaisLattice(newOriginCell);
    
  }
  
  public BravaisLattice scaleToSize(double cellVolume) {
    
    double scaleFactor = Math.cbrt(cellVolume / this.getCellSize());
    Vector[] newCellVectors = new Vector[this.numTotalVectors()];
    for (int vecNum = 0; vecNum < newCellVectors.length; vecNum++) {
      newCellVectors[vecNum] = this.getCellVector(vecNum).multiply(scaleFactor);
    }
    return new BravaisLattice(this.getOrigin(), newCellVectors, this.getDimensionPeriodicity());
    
  }
  
  public BravaisLattice multiplyVectorsBy(double scaleFactor) {
    
    Vector[] newCellVectors = new Vector[this.numTotalVectors()];
    for (int vecNum = 0; vecNum < newCellVectors.length; vecNum++) {
      newCellVectors[vecNum] = this.getCellVector(vecNum).multiply(scaleFactor);
    }
    return new BravaisLattice(this.getOrigin(), newCellVectors, this.getDimensionPeriodicity());
    
  }
  
  /**
   * Returns a set if integer arrays that map the coordinates of the large set to the coordinates of the 
   * small set using the given translation (in the integer coordinate system of the primitive structure).
   * 
   * @param translation
   * @param intSmallSet
   * @param intLargeSet
   * @return
   */
  private int[] findTranslationMap(int[] translation, Coordinates[] intSmallSet, Coordinates[] intLargeSet) {
    
    int[] map = new int[intSmallSet.length];
  
    for (int smallCoordNum = 0; smallCoordNum < intSmallSet.length; smallCoordNum++) {
      Coordinates smallCoord = intSmallSet[smallCoordNum];
      int largeSetIndex = this.findTranslationIndex(smallCoord, intLargeSet, translation);
      if (largeSetIndex < 0) {return null;}
      map[smallCoordNum] = largeSetIndex;
    } 
    
    return map;
  }
  
  public boolean isEquivalentTo(BravaisLattice lattice) {
    
    if (lattice == this) {
      return true;
    }
    
    if (lattice.numPeriodicVectors() != this.numPeriodicVectors()) {
      return false;
    }
    
    if (lattice.numNonPeriodicVectors() != this.numNonPeriodicVectors()) {
      return false;
    }
    
    if (lattice.numTotalVectors() != this.numTotalVectors()) {
      return false;
    }
    
    int[][] superToDirect = this.getSuperToDirect(lattice);
    if (superToDirect == null) {
      return false;
    }
    
    int cellSize = Math.abs(MSMath.determinant(superToDirect));
    return (cellSize == 1);
  }
  
  /**
   * 
   * @param singleCoords
   * @param candidates
   * @param translation
   * @return
   */
  private int findTranslationIndex(Coordinates singleCoords, Coordinates[] candidates, int[] translation) {
    
    for (int largeCoordNum = 0; largeCoordNum < candidates.length; largeCoordNum++) {
      Coordinates candidateCoords = candidates[largeCoordNum];
      if (this.translationMapsCoords(translation, singleCoords, candidateCoords)) {
        return largeCoordNum;
      }
    }
    
    return -1;
  }
  
  /**
   * 
   * @param translation
   * @param coords1
   * @param coords2
   * @return
   */
  private boolean translationMapsCoords(int[] translation, Coordinates coords1, Coordinates coords2) {    
    
    AbstractBasis latticeBasis = this.getLatticeBasis();
    coords1 = coords1.getCoordinates(latticeBasis);
  
    double[] translatedCoords = coords1.getArrayCopy();
    //for (int dimNum = 0; dimNum < translation.length; dimNum++) {
    //  if (coords1.coord(dimNum) + translation[dimNum] != coords2.coord(dimNum)) {return false;}
    //} 
    
    for (int dimNum = 0; dimNum < translation.length; dimNum++) {
      translatedCoords[dimNum] += translation[dimNum];
    }
    
    Coordinates newCoords = new Coordinates(translatedCoords, latticeBasis);
    if (newCoords.isCloseEnoughTo(coords2)) {return true;}

    return false;
  }

  /**
   * Finds ways in which one set can be translated to the other by the translations of the lattice.
   * 
   * @param coordSet1
   * @param coordSet2
   * @param returnOne
   * @return
   */
  public int[][] findTranslationMaps(Coordinates[] coordSet1, Coordinates[] coordSet2, boolean returnOne) {
    
    int[][] returnMaps = new int[0][];
    
    if (coordSet1 == null || coordSet2 == null) {return returnMaps;}
    if (coordSet1.length == 0 || coordSet2.length == 0) {return new int[1][0];} // The empty cluster maps to all clusters with no translation

    Coordinates[] smallSet = coordSet1;
    Coordinates[] largeSet = coordSet2;
    
    if (coordSet1.length > coordSet2.length) {
      largeSet = coordSet1;
      smallSet = coordSet2;
    }
    
    //AbstractBasis intBasis = this.getPrimStructure().getIntegerBasis();
    //int numDimensions = m_Lattice.numDimensions();
    
    //Coordinates[] intSmallSet = intBasis.getCoordinatesFrom(smallSet);
    //Coordinates[] intLargeSet = intBasis.getCoordinatesFrom(largeSet);
    
    AbstractBasis latticeBasis = this.getLatticeBasis();
    Coordinates[] latticeSmallSet = latticeBasis.getCoordinatesFrom(smallSet);
    Coordinates[] latticeLargeSet = latticeBasis.getCoordinatesFrom(largeSet);
    
    // Find possible translations
    int[][] translations = new int[0][];
    //Coordinates firstSmallCoords = intSmallSet[0];
    Coordinates firstSmallLatticeCoords = latticeSmallSet[0];
    for (int largeCoordNum = 0; largeCoordNum < latticeLargeSet.length; largeCoordNum++) {
      Coordinates latticeLargeCoords = latticeLargeSet[largeCoordNum];
      int[] newTranslation = this.findLatticeTranslation(firstSmallLatticeCoords, latticeLargeCoords);
      if (newTranslation == null) {continue;}
      /*
      if (intLargeCoords.coord(numDimensions) != firstSmallCoords.coord(numDimensions)) {continue;}
      int[] newTranslation = new int[numDimensions];
      for (int dimNum = 0; dimNum < numDimensions; dimNum++) {
        newTranslation[dimNum] = (int) (intLargeCoords.coord(dimNum) - firstSmallCoords.coord(dimNum));
      }*/
      translations = ArrayUtils.appendElement(translations, newTranslation);
    }
    
    // See which translations generate maps
    for (int translationNum = 0; translationNum < translations.length; translationNum++) {
      int[] map = this.findTranslationMap(translations[translationNum], latticeSmallSet, latticeLargeSet);
      if (map != null && (!ArrayUtils.arrayContains(returnMaps, map))) {
        returnMaps = ArrayUtils.appendElement(returnMaps, map);
        if (returnOne) {return returnMaps;}
      }
    }
    
    return returnMaps;
  }
  
  public boolean isLatticeVector(Vector vector) {
    Coordinates newHead = this.getOrigin().translateBy(vector);
    return this.isLatticePoint(newHead);
  }
  
  public boolean isLatticePoint(Coordinates coordinates) {
    
    return this.areTranslationallyEquivalent(coordinates, this.getOrigin());
    
    // Shift to compensate for possible non-Cartesian origin
    /*coordinates = this.getRelativeCoords(coordinates);
    
    AbstractBasis latticeBasis = this.getLatticeBasis();
    double[] latticeVecArray = coordinates.getCoordArray(latticeBasis);
    
    for (int dimNum = 0; dimNum < latticeVecArray.length; dimNum++) {
      if (!this.isDimensionPeriodic(dimNum)) {continue;}
      latticeVecArray[dimNum] = latticeVecArray[dimNum] - Math.round(latticeVecArray[dimNum]);
    }
    
    Coordinates translatedOrigin = new Coordinates(latticeVecArray, latticeBasis);
    translatedOrigin = this.projectToLattice(translatedOrigin);
    return translatedOrigin.isCloseEnoughTo(CartesianBasis.getOrigin());*/
  }
  

  public boolean areTranslationallyEquivalent(Coordinates[] coords1, Coordinates[] coords2) {
    
    if (coords1 == null || coords2 == null) {return false;}
    if (coords1.length != coords2.length) {return false;}
    int[][] translationMaps = this.findTranslationMaps(coords1, coords2, true);
    return (translationMaps.length != 0);
  }
  
  
  public int[] findLatticeTranslation(Coordinates directCoords1, Coordinates directCoords2) {
    
    //int numPeriodicDimensions = m_Lattice.numDimensions();
    AbstractBasis latticeBasis = this.getLatticeBasis();
    directCoords1 = directCoords1.getCoordinates(latticeBasis);
    directCoords2 = directCoords2.getCoordinates(latticeBasis);

    double[] translationArray = new double[directCoords1.numCoords()];
    for (int dimNum = 0; dimNum < translationArray.length; dimNum++) {
      translationArray[dimNum] = directCoords2.coord(dimNum) - directCoords1.coord(dimNum);
    }
    
    int[] returnArray = new int[translationArray.length];
    for (int dimNum = 0; dimNum < returnArray.length; dimNum++) {
      if (!this.isDimensionPeriodic(dimNum)) {continue;}
      returnArray[dimNum] = (int) Math.round(translationArray[dimNum]);
      translationArray[dimNum] = translationArray[dimNum] - returnArray[dimNum];
    }
    
    Coordinates translatedOrigin = new Coordinates(translationArray, latticeBasis);
    if (!translatedOrigin.isCloseEnoughTo(this.getOrigin())) {return null;}
    
    return returnArray;
  }
  
  
  public boolean areTranslationallyEquivalent(Coordinates coords1, Coordinates coords2) {
    return this.areTranslationallyEquivalent(coords1, coords2, CartesianBasis.getPrecision());
  }
  
  public boolean areTranslationallyEquivalent(Coordinates coords1, Coordinates coords2, double tolerance) {
    // This could be slow
    /*Vector translation = new Vector(coords1, coords2);
    AbstractLinearBasis latticeBasis = this.getLatticeBasis();
    double[] directArray = translation.getDirectionArray(latticeBasis);*/
    
    // A little faster
    double[] cartArray1 = new double[] {
        coords2.cartCoord(0) - coords1.cartCoord(0),
        coords2.cartCoord(1) - coords1.cartCoord(1),
        coords2.cartCoord(2) - coords1.cartCoord(2),
    };
    AbstractLinearBasis latticeBasis = this.getLatticeBasis();
    double[] directArray = latticeBasis.transformFromCartesianDirection(cartArray1);

    /*AbstractLinearBasis latticeBasis = this.getLatticeBasis();
    double[] coord1Array = coords1.getCoordArray(latticeBasis);
    double[] coord2Array = coords2.getCoordArray(latticeBasis);
    double[] directArray = MSMath.arraySubtract(coord1Array, coord2Array);*/
    
    for (int dimNum = 0; dimNum < directArray.length; dimNum++) {
      if (!this.isDimensionPeriodic(dimNum)) {continue;}
      directArray[dimNum] -= Math.round(directArray[dimNum]);
    }
    double[] cartArray = latticeBasis.transformToCartesianDirection(directArray);
    /*double mag = MSMath.magnitude(cartArray);
    if (mag > CartesianBasis.getPrecision() && mag < CartesianBasis.getPrecision() * 2) {
      System.out.println(MSMath.magnitude(cartArray) + ", " + tolerance);
    }*/
    return (MSMath.magnitude(cartArray) <= tolerance);
    
    /*Vector remainder = new Vector(directArray, latticeBasis);
    //System.out.println(distance);
    return (remainder.length() <= tolerance);*/
  }
  
  /**
   * The lengths of these vectors are the inverse of the lenght of the corresponding 
   * lattice vector.
   * 
   * @return
   */
  private Vector[] getWignerSeitzProjectors() {
    
    if (m_WignerSeitzProjectors == null) {
      
      Coordinates origin = this.getOrigin();
      /*double maxDistance = this.getCompactLattice().m_OriginCell.getLongestDiagonal();
      Coordinates[] points = this.getNearbyLatticePoints(origin, maxDistance, false);
      Vector[] vectors = new Vector[points.length];
      for (int pointNum = 0; pointNum < points.length; pointNum++) {
        vectors[pointNum] = new Vector(origin, points[pointNum]);
      }*/
      
      // Find potential vectors
      BravaisLattice compactLattice = this.getCompactLattice();
      int[] superCellSize = new int[compactLattice.numTotalVectors()];
      int[] offsets = new int[superCellSize.length];
      for (int dimNum = 0; dimNum < superCellSize.length; dimNum++) {
        superCellSize[dimNum] = compactLattice.isDimensionPeriodic(dimNum) ? 5 : 1;
        offsets[dimNum] = (superCellSize[dimNum] - 1) / 2;
      }
      
      ArrayIndexer indexer = new ArrayIndexer(superCellSize);
      Vector[] vectors = new Vector[indexer.numAllowedStates()];
      for (int vecNum = 0; vecNum < vectors.length; vecNum++) {
        int[] coordArray = indexer.splitValue(vecNum);
        for (int dimNum = 0; dimNum < coordArray.length; dimNum++) {
          coordArray[dimNum] -= offsets[dimNum];
        }
        Coordinates cellCoords = new Coordinates(coordArray, compactLattice.getLatticeBasis());
        vectors[vecNum] = new Vector(origin, cellCoords);
      } 
      
      // Remove anti-parallel vectors
      for (int vecNum = vectors.length - 1; vecNum >= 0; vecNum--) {
        Vector vector = vectors[vecNum];
        for (int vecNum2 = vecNum; vecNum2 < vectors.length; vecNum2++) {
          Vector vector2 = vectors[vecNum2];
          if (vector.add(vector2).length() < CartesianBasis.getPrecision()) {
            vectors = (Vector[]) ArrayUtils.removeElement(vectors, vecNum);
            break;
          }
        }
      }
      
      // Turn the vectors into projectors
      Vector[] projectors = new Vector[vectors.length];
      for (int vecNum = 0; vecNum < projectors.length; vecNum++) {
        Vector vector = vectors[vecNum];
        double length = vector.length();
        projectors[vecNum] = vector.divide(length * length);
      }
      
      // This relies on the fact that we have already set the WignerSeitzVectors
      /*for (int vecNum = vectors.length - 1; vecNum >= 0; vecNum--) {
        Vector vector = vectors[vecNum];
        Coordinates testCoords = origin.translateBy(vector.multiply(0.5)); 
        Coordinates wsCoords = this.getWignerSeitzNeighbor(origin, testCoords);
        if (!testCoords.isCloseEnoughTo(wsCoords)) {
          m_WignerSeitzProjectors = (Vector[]) ArrayUtils.removeElement(m_WignerSeitzProjectors, vecNum);
          vectors = (Vector[]) ArrayUtils.removeElement(vectors, vecNum);
        }
      }*/
      
      // This is cleaner and should be faster
      for (int vecNum = vectors.length - 1; vecNum >= 0; vecNum--) {
        Vector vector = vectors[vecNum];
        for (int vecNum2 = 0; vecNum2 < projectors.length; vecNum2++) {
          if (vecNum == vecNum2) {continue;} // This isn't really necessary because of the numerical adjustment below.
          Vector projector = projectors[vecNum2];
          double projection = projector.innerProductCartesian(vector);
          
          // To deal with numerical issues
          projection = projection - Math.signum(projection) * (CartesianBasis.getPrecision() * projector.length());

          if (projection < -1 || projection > 1) {
            projectors = (Vector[]) ArrayUtils.removeElement(projectors, vecNum);
            vectors = (Vector[]) ArrayUtils.removeElement(vectors, vecNum);
            break;
          }
        }
      }
      
      m_WignerSeitzProjectors = projectors;

    }
    
    return m_WignerSeitzProjectors;
    
  }
  
  public Coordinates getWignerSeitzNeighbor(Coordinates cellCenter, Coordinates point) {
    
    if (this.numPeriodicVectors() == 0) {return point;}

    // This should speed things up for large cells
    Coordinates fastPoint = this.getQuickCoordsInParallelCell(cellCenter, point);
    Coordinates projectedCenter = this.projectToLattice(cellCenter);
    Coordinates projectedPoint = this.projectToLattice(fastPoint);
    if (projectedCenter.distanceFrom(projectedPoint) < this.getMinPeriodicDistance() / 2 - CartesianBasis.getPrecision()) {
      return fastPoint;
    }
    point = fastPoint; // Comment this out if we go back to the old way of finding projectors.
    
    Vector[] wignerSeitzProjectors = this.getWignerSeitzProjectors();
    int vectorIndex = 0;
    int lastChangedIndex = 0;
    //Vector vector = new Vector(cellCenter, point);
    Vector vector = new Vector(projectedCenter, projectedPoint);
    
    // TODO watch out for possible infinite loop here due to numerical problems.
    do {
      Vector wignerSeitzProjector = wignerSeitzProjectors[vectorIndex];      
      
      double shift = wignerSeitzProjector.innerProductCartesian(vector);
      
      // The real length of the Wigner Seitz vector
      double length = 1 / wignerSeitzProjector.length();
      
      // To deal with numerical issues
      shift = Math.round(shift - Math.signum(shift) * (CartesianBasis.getPrecision() / length));

      if (shift != 0) {
        lastChangedIndex = vectorIndex;
        point = point.translateBy(wignerSeitzProjector.multiply(-shift * length * length));
        vector = new Vector(cellCenter, point);
      }
      
      vectorIndex = (vectorIndex + 1) % wignerSeitzProjectors.length;
      
    } while (vectorIndex != lastChangedIndex);
    
    return point;
    
  }
  

  /*public Coordinates getWignerSeitzNeighborOld(Coordinates cellCenter, Coordinates point) {
    
    if (this.numPeriodicVectors() == 0) {return point;}
    
    // This should speed things up for large cells
    Coordinates projectedCenter = this.projectToLattice(cellCenter);
    Coordinates projectedPoint = this.projectToLattice(point);
    if (projectedCenter.distanceFrom(projectedPoint) < this.getMinPeriodicDistance() / 2 - CartesianBasis.getPrecision()) {
      return point;
    }
    
    Vector[] wignerSeitzProjectors = this.getWignerSeitzProjectors();
    int vectorIndex = 0;
    int lastChangedIndex = 0;
    Vector vector = new Vector(cellCenter, point);
    
    int numIterations = 0;
    // TODO watch out for possible infinite loop here due to numerical problems.
    do {
      Vector wignerSeitzProjector = wignerSeitzProjectors[vectorIndex];      
      
      double shift = wignerSeitzProjector.innerProductCartesian(vector);
      double length = 1 / wignerSeitzProjector.length();
      
      // To deal with numerical issues
      shift = Math.round(shift - (Math.signum(shift) * CartesianBasis.getPrecision()) / length);

      if (shift != 0) {
        lastChangedIndex = vectorIndex;
        point = point.translateBy(wignerSeitzProjector.multiply(-shift * length * length));
        vector = new Vector(cellCenter, point);
      }
      
      vectorIndex = (vectorIndex + 1) % wignerSeitzProjectors.length;
      numIterations ++;
      
    } while (vectorIndex != lastChangedIndex);
    System.out.println(numIterations);
    
    return point;
    
  }*/
  
  /**
   * This method is not guaranteed to return the closest image (unlike the Wigner Seitz method),
   * but it will generall return a nearby image if the lattice is reasonably compact.
   * 
   * This was mistakenly used for the old Wigner Seitz method.
   * 
   * @param cellCenter
   * @param point
   * @return
   */
  public Coordinates getQuickCoordsInParallelCell(Coordinates cellCenter, Coordinates point) {
    Vector translation = new Vector(cellCenter, point);
    AbstractLinearBasis latticeBasis = this.getLatticeBasis();
    double[] directArray = translation.getDirectionArray(latticeBasis);
    for (int dimNum = 0; dimNum < directArray.length; dimNum++) {
      if (this.isDimensionPeriodic(dimNum)) {
        directArray[dimNum] = -Math.round(directArray[dimNum]);
      } else {
        directArray[dimNum] = 0;
      }
    }
    Vector revTranslation = new Vector(directArray, latticeBasis);
    Coordinates transCoords = point.translateBy(revTranslation);
    return transCoords;
  }
  
  public double getNearestImageDistance(Coordinates coords1, Coordinates coords2) {
    return coords1.distanceFrom(this.getNearestImage(coords1, coords2));
  }
  
  public Coordinates getNearestImage(Coordinates neighborHoodCoords, Coordinates imageCoords) {
    
    // This was with the old (wrong) Wigner Seitz implementation, now known as getQuickCoordsInPallelCell
    /*double buffer = 1E-4;
    double searchRadius = this.getDistanceInWignerSeitzCell(neighborHoodCoords, imageCoords) + buffer;
    Coordinates[] contenders = this.getNearbyImages(neighborHoodCoords, imageCoords, searchRadius, true);
    double minDistance = Double.POSITIVE_INFINITY;
    Coordinates returnCoords = null;
    for (int siteNum = 0; siteNum < contenders.length; siteNum++) {
      double distance = neighborHoodCoords.distanceFrom(contenders[siteNum]);
      if (distance < minDistance) {
        returnCoords = contenders[siteNum];
        minDistance = distance;
      }
    }
    return returnCoords;*/
    
    return this.getWignerSeitzNeighbor(neighborHoodCoords, imageCoords);
  }
  
  public double getDistanceInWignerSeitzCell(Coordinates coords1, Coordinates coords2) {
    return coords1.distanceFrom(this.getWignerSeitzNeighbor(coords1, coords2));
  }
  
  public int numPeriodicVectors() {
    return m_OriginCell.numFiniteVectors();
  }
  
  public int numNonPeriodicVectors() {
    return m_OriginCell.numInfiniteVectors();
  }
  
  public int numTotalVectors() {
    return m_OriginCell.numCellVectors();
  }
  
  public OperationGroup getPointGroup() {
    if (m_PointGroup == null) {
      LatticeSymmetryFinder symFinder = new LatticeSymmetryFinder(this);
      m_PointGroup = new OperationGroup(symFinder.getPointOperations());
    }
    return m_PointGroup;
  }
  
  public SpaceGroup getSpaceGroup() {
    if (m_SpaceGroup == null) {
      OperationGroup pointGroup = this.getPointGroup();
      m_SpaceGroup = new SpaceGroup(pointGroup, this);
    }
    return m_SpaceGroup;
  }
  
  public Coordinates softRemainder(Coordinates coords) {

    // Shift to compensate for differences in origins
    //coords = this.getRelativeCoords(coords);
    LinearBasis cellBasis = m_OriginCell.getCellBasis();
    Coordinates cellCoords = coords.getCoordinates(cellBasis);

    // Translate the point to the cell that is on the negative side of the origin
    double[] remainderArray = cellCoords.getArrayCopy();
    for (int coordNum = 0; coordNum < remainderArray.length; coordNum++) {
      if (!m_OriginCell.isVectorFinite(coordNum)) {continue;}
      double cellCoord = cellCoords.coord(coordNum);
      remainderArray[coordNum] = cellCoord - Math.ceil(cellCoord);
    }

    // Find the vector that identifies the location of this point relative to the origin
    Vector remainderVector = new Vector(remainderArray, cellBasis);

    // Find the distance between this
    for (int faceNum = 0; faceNum < m_CartesianNormals.length; faceNum++) {
      if (!m_OriginCell.isVectorFinite(faceNum)) {continue;}
      double dist = remainderVector.innerProductCartesian(m_CartesianNormals[faceNum]);
      if ((-1 * dist) > CartesianBasis.getPrecision()) {
        remainderArray[faceNum] = remainderArray[faceNum] + 1;
      }
    }

    // Undo previous shift
    Vector shift = new Vector(CartesianBasis.getInstance().getOrigin(), this.getOrigin());
    return new Coordinates(remainderArray, cellBasis).translateBy(shift);
  }
  
  public int[] softFloor(Coordinates coords) {
    Coordinates directCoords = coords.getCoordinates(this.getLatticeBasis());
    Coordinates remainderCoords = this.softRemainder(directCoords).getCoordinates(this.getLatticeBasis());
    int[] returnArray = new int[directCoords.numCoords()];
    for (int coordNum = 0; coordNum < returnArray.length; coordNum++) {
      if (!this.isDimensionPeriodic(coordNum)) {continue;}
      returnArray[coordNum] = (int) Math.round(directCoords.coord(coordNum) - remainderCoords.coord(coordNum));
    }
    return returnArray;
  }
  
  public Coordinates hardRemainder(Coordinates coords) {

    // Shift to a Cartesian origin
    //coords = this.getRelativeCoords(coords);
    
    LinearBasis cellBasis = m_OriginCell.getCellBasis();
    Coordinates cellCoords = coords.getCoordinates(cellBasis);

    // Translate the point to the cell that is on the negative side of the origin
    double[] remainderArray = cellCoords.getArrayCopy();
    for (int coordNum = 0; coordNum < remainderArray.length; coordNum++) {
      if (!m_OriginCell.isVectorFinite(coordNum)) {continue;}
      double cellCoord = cellCoords.coord(coordNum);
      remainderArray[coordNum] = cellCoord - Math.floor(cellCoord);
    }

    // Undo previous shift
    Vector shift = new Vector(CartesianBasis.getInstance().getOrigin(), this.getOrigin());
    return new Coordinates(remainderArray, cellBasis).translateBy(shift);
    
  }
  
  /**
   * 
   * @param coords
   * @return A new set of coordinates in which the origin of this lattice
   * is treated as the Cartesian origin
   */
  /*
  public Coordinates getRelativeCoords(Coordinates coords) {
    if (this.getOrigin() == CartesianBasis.getInstance().getOrigin()) {
      return coords;
    }
    Vector shift = new Vector(this.getOrigin(), CartesianBasis.getInstance().getOrigin());
    return coords.translateBy(shift);
  }*/
  
  public int[] hardFloor(Coordinates coords) {
    
    //coords = this.getRelativeCoords(coords); // Should not be necessary now that origin is included in basis
    LinearBasis cellBasis = m_OriginCell.getCellBasis();
    Coordinates cellCoords = coords.getCoordinates(cellBasis);

    //double[] remainderArray = cellCoords.getArrayCopy();
    //int[] floorArray = new int[this.numDimensions()];
    int[] floorArray = new int[cellCoords.numCoords()];
    for (int coordNum = 0; coordNum < floorArray.length; coordNum++) {
      if (!m_OriginCell.isVectorFinite(coordNum)) {continue;}
      double cellCoord = cellCoords.coord(coordNum);
      //remainderArray[coordNum] = cellCoord - Math.ceil(cellCoord);
      floorArray[coordNum] = (int) Math.floor(cellCoord);
    }

    // Find the vector that identifies the location of this point relative to the origin
    /*Vector remainderVector = new Vector(remainderArray, cellBasis);

    // Find the distance between the new point and the planes defined by the lattice vectors
    for (int faceNum = 0; faceNum < m_CartesianNormals.length; faceNum++) {
      double dist = remainderVector.innerProductCartesian(m_CartesianNormals[faceNum]);
      if (0 > dist && dist > (-1* CartesianBasis.getPrecision())) {
        //floorArray[faceNum] = floorArray[faceNum] + 1;
      }
    }*/
    
    return floorArray;
    
  }
  
  public int[] getPeriodicIndices() {
    int[] returnArray = new int[m_OriginCell.numFiniteVectors()];
    int returnIndex = 0; 
    for (int dimNum = 0; dimNum < m_OriginCell.numCellVectors(); dimNum++) {
      if (m_OriginCell.isVectorFinite(dimNum)) {
        returnArray[returnIndex++] = dimNum;
      }
    }
    return returnArray;
  }
  
  public ParallelCell getOriginCell() {
    return m_OriginCell;
  }
  
  public Coordinates getOrigin() {
    return m_OriginCell.getOrigin();
  }
  
  public Vector[] getCellVectors() {
    return m_OriginCell.getCellVectors();
  }
  
  public Vector[] getPeriodicVectors() {
    return m_OriginCell.getFiniteVectors();
  }
  
  public Vector[] getNonPeriodicVectors() {
    return m_OriginCell.getInfiniteVectors();
  }

  public Vector getPeriodicVector(int index) {
    return m_OriginCell.getFiniteVector(index);
  }
  
  public Vector getNonPeriodicVector(int index) {
    return m_OriginCell.getInfiniteVector(index);
  }
  
  public Vector getCellVector(int index) {
    return m_OriginCell.getCellVector(index);
  }
  
  public LinearBasis getLatticeBasis() {
    return m_OriginCell.getCellBasis();
  }
  
  public Coordinates removeLattice(Coordinates coords) {
    
    AbstractBasis latticeBasis = this.getLatticeBasis();
    
    double[] siteCoordArray = coords.getCoordArray(latticeBasis);
    double[] originArray = this.getOrigin().getCoordArray(latticeBasis);
    for (int coordNum = 0; coordNum < siteCoordArray.length; coordNum++ ) {
      if (m_OriginCell.isVectorFinite(coordNum)) {
        siteCoordArray[coordNum] = originArray[coordNum];
      }
    }
    return new Coordinates(siteCoordArray, latticeBasis);
    
  }
  
  public Vector removeLattice(Vector vector) {
    
    Coordinates newHead = this.removeLattice(vector.getHead());
    Coordinates newTail = this.removeLattice(vector.getTail());

    // This was a bug for a long time!  Fixed 09/02/2016
    //return new Vector(newHead, newTail);
    return new Vector(newTail, newHead);
  }
  
  public Coordinates projectToLattice(Coordinates coords) {
    
    AbstractBasis latticeBasis = this.getLatticeBasis();
    
    double[] siteCoordArray = coords.getCoordArray(latticeBasis);
    double[] originArray = this.getOrigin().getCoordArray(latticeBasis);
    for (int coordNum = 0; coordNum < siteCoordArray.length; coordNum++ ) {
      if (!m_OriginCell.isVectorFinite(coordNum)) {
        siteCoordArray[coordNum] = originArray[coordNum];
      }
    }
    return new Coordinates(siteCoordArray, latticeBasis);
    
  }
  
  public Vector projectToLattice(Vector vector) {
    
    Coordinates newHead = this.projectToLattice(vector.getHead());
    Coordinates newTail = this.projectToLattice(vector.getTail());
    
    // This was a bug for a long time!  Fixed 09/02/2016
    //return new Vector(newHead, newTail);
    
    return new Vector(newTail, newHead);
  }
  
  /**
   * Useful for lattices with non-periodic dimensions.  Makes the non-periodic 
   * vectors normal to the periodic vectors and to each other.
   * 
   * @return A Bravais lattice with the non-periodic vectors orthogonal to the periodic vectors
   */
  public BravaisLattice getOrthogonalizedLattice() {
    
    Vector[] cellVectors = this.getPeriodicVectors();
    boolean[] isVectorPeriodic = new boolean[this.numTotalVectors()];
    for (int dimNum = 0; dimNum < cellVectors.length; dimNum++) {
      isVectorPeriodic[dimNum] = true;
    }
    return new BravaisLattice(this.getOrigin(), LinearBasis.fill3DBasis(cellVectors), isVectorPeriodic);
    
  }
  
  public boolean isOnLatticePlane(Coordinates coords) {
    return this.projectToLattice(coords).isCloseEnoughTo(coords);
  }
  
  public boolean isOnLatticePlane(Vector vector) {
    return this.isOnLatticePlane(vector.getHead()) && this.isOnLatticePlane(vector.getTail());
  }
  
  public boolean isParallelToLatticePlane(Vector vector) {
    Coordinates coords = new Coordinates(vector.getCartesianDirection(), CartesianBasis.getInstance());
    return this.isOnLatticePlane(coords);
  }
  
  public int[][] getCompactSuperToDirect(int numPrimCells) {
    
    if (numPrimCells >= 0) {
      SuperLatticeGenerator generator = new SuperLatticeGenerator(numPrimCells, this.numPeriodicVectors());
      return generator.getMostCompactSuperToDirect(this);   
    } else {      
      SuperLatticeGenerator generator = new SuperLatticeGenerator(-numPrimCells, this.numPeriodicVectors());
      return generator.getInvCompactSuperToDirect(this);     
    }
    
  }
  
  /*
  public int[][] getMinimalSuperToDirect(double minPeriodicDistance) {
    
    // TODO incorporate symmetry
    double[] hermiteConstantPowers = new double[] { // http://mathworld.wolfram.com/HermiteConstants.html
        1,
        4.0 / 3.0,
        2,
        4,
        8,
        64.0 / 3.0,
        64,
        256
    };
    
    int numDimensions = this.numPeriodicVectors();
    double hermiteConstant = hermiteConstantPowers[numDimensions];
    
    double minCellVolume = Math.pow(minPeriodicDistance, numDimensions) / Math.sqrt(hermiteConstant);
    int minPrimCells = (int) Math.ceil(minCellVolume / this.getCellSize());
    
    for (int numPrimCells = minPrimCells; numPrimCells < Integer.MAX_VALUE; numPrimCells++) {
      
      int[][] superToDirect = this.getCompactSuperToDirect(numPrimCells);
      SuperLattice superLattice = new SuperLattice(this, superToDirect);
      double superPeriodicDistance = superLattice.getMinPeriodicDistance();
      if (superPeriodicDistance >= minPeriodicDistance) {
        return superToDirect;
      }
      
    }
    return null;
    
  }
  */
  
  // TODO create new version that uses SuperLatticeGenerator -- similar to LowSymmetryGenerator for kPoints
  // Or, if this is faster, update LowSymmetryGenerator to use this
  public int[][] getMinimalSuperToDirect(double minPeriodicDistance) {
    
    double padding = 1E-4;
    
    BravaisLattice compactLattice = this.getCompactLattice();
    
    double minVectorLength = Double.POSITIVE_INFINITY;
    double maxVectorLength = 0;
    for (int vecNum = 0; vecNum < compactLattice.numPeriodicVectors(); vecNum++) {
      Vector vector = compactLattice.getPeriodicVector(vecNum);
      double length = vector.length();
      minVectorLength = Math.min(minVectorLength, length);
      maxVectorLength = Math.max(maxVectorLength, length);
    }

    // Find a set of candidate vectors that is guaranteed to contain at least one acceptable basis
    Coordinates origin = this.getOrigin();
    double multiplier = Math.ceil(minPeriodicDistance / minVectorLength);
    double searchRadius = multiplier * maxVectorLength + padding;
 
    // To deal with Hg 64 63 problem
    double[] hermiteConstantPowers = new double[] { // http://mathworld.wolfram.com/HermiteConstants.html
        1,
        4.0 / 3.0,
        2,
        4,
        8,
        64.0 / 3.0,
        64,
        256
    };
    int numPeriodicDimensions = compactLattice.numPeriodicVectors();
    double hermiteConstant = -1;
    if (numPeriodicDimensions < hermiteConstantPowers.length) {
      hermiteConstant = Math.pow(hermiteConstantPowers[numPeriodicDimensions - 1], 1.0 / numPeriodicDimensions);
    } else {
      hermiteConstant = Math.pow(Math.sqrt(hermiteConstantPowers[1]), numPeriodicDimensions - 1); // Simple upper bound, can be made tighter with Mordell's inequality
    }
    double hermiteLength = multiplier * Math.pow(this.getCellSize(), 1.0 / numPeriodicDimensions) * Math.sqrt(hermiteConstant);
    //double hermiteLength2 = minPeriodicDistance * Math.sqrt(hermiteConstant);
    searchRadius = Math.max(searchRadius, hermiteLength + padding);
    
    Coordinates[] nearbyCoords = compactLattice.getNearbyLatticePoints(origin, searchRadius, false);
    Vector[] candidateVectors = new Vector[nearbyCoords.length];
    double[] distances = new double[nearbyCoords.length];
    int candidateNum = 0;
    for (int vecNum= 0; vecNum < candidateVectors.length; vecNum++) {
      double distance = origin.distanceFrom(nearbyCoords[vecNum]);
      if (distance < minPeriodicDistance) {continue;}
      distances[candidateNum] = distance;
      candidateVectors[candidateNum++] = new Vector(origin, nearbyCoords[vecNum]);
    }
    
    candidateVectors = (Vector[]) ArrayUtils.truncateArray(candidateVectors, candidateNum);
    distances = ArrayUtils.truncateArray(distances, candidateNum);
    int[] map = ArrayUtils.getSortPermutation(distances);
    Vector[] sortedVectors = new Vector[candidateVectors.length];
    for (int vecNum = 0; vecNum < map.length; vecNum++) {
      sortedVectors[vecNum] = candidateVectors[map[vecNum]];
    }
    
    // Search among all possible combinations of candidate vectors to find the acceptable
    // basis with the smallest number of unit cells.  Sort between bases with the same numbers
    // of unit cells by choosing the one with the longest shortest vector.
    Vector[] cellVectors = new Vector[this.numPeriodicVectors()];
    Vector[] orthogonalUnitVectors = new Vector[this.numPeriodicVectors()];
    double primCellSize = this.getCellSize();
    int[] numCandidatesByDim = new int[this.numPeriodicVectors()];
    Arrays.fill(numCandidatesByDim, candidateVectors.length);
    ArrayIndexer candidateIndexer = new ArrayIndexer(numCandidatesByDim);
    int[] candidateIndices = candidateIndexer.getInitialState();
    int[] periodicIndices = this.getPeriodicIndices();
    Vector[] keeperVectors = this.getCellVectors();
    boolean[] isDimPeriodic = this.getDimensionPeriodicity();
    BravaisLattice bestLattice = null;
    double minNumPrimCells = Double.POSITIVE_INFINITY;
    double minKeeperLength = 0;
    double maxAllowedLength = Double.POSITIVE_INFINITY;
    do {
      double cellSize = 1;
      boolean badCombination = false;
      for (int dimNum = candidateIndices.length - 1; dimNum >= 0; dimNum--) {
        Vector candidate = sortedVectors[candidateIndices[dimNum]];
        double length = candidate.length(); 
        if (length < minKeeperLength) {
          badCombination = true;
          candidateIndexer.branchEnd(dimNum, candidateIndices);
          break;
        }
        if (length > maxAllowedLength) {
          badCombination = true;
          candidateIndexer.branchEnd(dimNum, candidateIndices);
          break;
        }
        Vector shiftedVector = candidate;
        for (int prevDimNum = dimNum + 1; prevDimNum < orthogonalUnitVectors.length; prevDimNum++) {
          Vector prevUnitVector = orthogonalUnitVectors[prevDimNum];
          double innerProduct = shiftedVector.innerProductCartesian(prevUnitVector);
          shiftedVector = shiftedVector.subtract(prevUnitVector.multiply(innerProduct)); 
          if (shiftedVector.length() < CartesianBasis.getPrecision()) {
            badCombination = true;
            break;
          }
        }
        if (badCombination) {
          candidateIndexer.branchEnd(dimNum, candidateIndices);
          break;
        }
        orthogonalUnitVectors[dimNum] = shiftedVector.unitVectorCartesian();
        cellSize *= candidate.innerProductCartesian(orthogonalUnitVectors[dimNum]);
        cellVectors[dimNum] = candidate;
      }
      if (badCombination) {
        continue;
      }
      double numPrimCells = Math.abs(Math.round(cellSize / primCellSize));
      if (numPrimCells > minNumPrimCells) {continue;}
      for (int vecNum= 0; vecNum < cellVectors.length; vecNum++) {
        keeperVectors[periodicIndices[vecNum]] = cellVectors[vecNum];
      }
      
      BravaisLattice lattice = new BravaisLattice(this.getOrigin(), cellVectors, isDimPeriodic).getCompactLattice();
      double minLength = Double.POSITIVE_INFINITY;
      double maxLength = 0;
      for (int vecNum=0; vecNum < lattice.numPeriodicVectors(); vecNum++) {
        double length = lattice.getPeriodicVector(vecNum).length();
        minLength = Math.min(length, minLength);
        maxLength = Math.max(maxLength, length);
      }
      if (minLength < minPeriodicDistance) {continue;}
      if ((numPrimCells == minNumPrimCells) && (minLength <= minKeeperLength)) {continue;}
      minNumPrimCells = numPrimCells;
      minKeeperLength = minLength;
      boolean isOdd = (numPeriodicDimensions % 2 == 1);
      int k = isOdd ? (numPeriodicDimensions - 1) / 2 : numPeriodicDimensions / 2;
      double c = isOdd ? 
          Math.pow(2, numPeriodicDimensions) * MSMath.factorial(k) * Math.pow(Math.PI, k) / MSMath.factorial(numPeriodicDimensions) :
          Math.pow(Math.PI, k) / MSMath.factorial(k);
      maxAllowedLength = Math.pow(cellSize / c, 1.0 / numPeriodicDimensions) * 2; // In the limit of a perfect sphere
      bestLattice = lattice;
    } while (candidateIndexer.increment(candidateIndices));

    //int[][] superToDirect = new int[bestCellVectors.length][bestCellVectors.length];
    //AbstractBasis directBasis = this.getLatticeBasis();
    //int[] periodicIndices = this.getPeriodicIndices();
    //for (int vecNum = 0; vecNum < bestCellVectors.length; vecNum++) {
    //  double[] directArray = bestCellVectors[vecNum].getDirection().getCoordArray(directBasis);
    //  for (int dimNum = 0; dimNum < directArray.length; dimNum++) {
    //    if (!this.isDimensionPeriodic(dimNum)) {continue;}
    //    superToDirect[vecNum][dimNum] = (int) Math.round(directArray[periodicIndices[dimNum]]);
    //  }
   // }
    
    //return superToDirect;
    
    return this.getSuperToDirect(bestLattice);
    //return bestLattice.getSuperToDirect(bestLattice);
    
  }
  
  public double getPackingRatio() {
    
    double radius = this.getMinPeriodicDistance() / 2;
    double volume = this.getCellSize();
    double sphereVolume = Math.PI * radius * radius * radius * 4 / 3;
    return sphereVolume / volume;
    
  }
  
  public Coordinates[] getNearbyImages(Coordinates centerCoords, Coordinates imageCoords, double distance, boolean includeGiven) {
    
    if (m_CompactLattice != this) {
      return this.getCompactLattice().getNearbyImages(centerCoords, imageCoords, distance, includeGiven);
    }
    
    if (distance + CartesianBasis.getPrecision() < this.getMinPeriodicDistance() / 2) {
      Coordinates returnCoords = this.getNearestImage(centerCoords, imageCoords);
      double wignerDistance = returnCoords.distanceFrom(centerCoords);
      if ((!includeGiven && (wignerDistance < CartesianBasis.getPrecision())) || (wignerDistance > distance)) {
        return new Coordinates[0];
      }
      return new Coordinates[] {returnCoords};
    }
    
    // This doesn't seem to speed things up
    /* Vector shiftVector = new Vector(new double[] {-0.5, -0.5, -0.5}, this.getLatticeBasis());
    Coordinates newOrigin = centerCoords.translateBy(shiftVector);
    AbstractBasis latticeBasis = new LinearBasis(newOrigin, this.getCellVectors());*/
    
    AbstractBasis latticeBasis = this.getLatticeBasis();
    double[] coordArray = imageCoords.getCoordArray(latticeBasis);
    ArrayList nearbyCoords = new ArrayList();
    this.getNearbyCoordsRecursive(latticeBasis, centerCoords, coordArray, 0, nearbyCoords, distance, includeGiven);
    Coordinates[] returnArray = new Coordinates[nearbyCoords.size()];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = (Coordinates) nearbyCoords.get(siteNum);
    }
    return returnArray;
    
  }
  
  public double getLongestCompactVectorLength() {
    double distance = 0;
    BravaisLattice compactLattice = this.getCompactLattice();
    for (int vecNum = 0; vecNum < compactLattice.numPeriodicVectors(); vecNum++) {
      Vector vector = compactLattice.getPeriodicVector(vecNum);
      distance = Math.max(distance, vector.length());
    }
    return distance;
  }
  
  public Vector getMinPeriodicVector() {
    
    BravaisLattice compactLattice = this.getCompactLattice();
    Vector minPeriodicVector = null;
    for (int vecNum = 0; vecNum < compactLattice.numPeriodicVectors(); vecNum++) {
      Vector vector = compactLattice.getPeriodicVector(vecNum);
      if (minPeriodicVector == null || vector.length() < minPeriodicVector.length()) {
        minPeriodicVector = vector;
      }
    }
    return minPeriodicVector;
  }
  
  public double getMinPeriodicDistance() {
    
    if (Double.isNaN(m_MinPeriodicDistance)) {
      
      // Two options, depending on whether we have already found the compact lattice.
      // We can call getCompactLattice as long as it just returns the greedy lattice, which is Minkowski reduced.
      // Then it should contain the shortest lattice vector and be pretty fast (less memory intensive).
      m_MinPeriodicDistance = Double.POSITIVE_INFINITY;
      BravaisLattice compactLattice = this.getCompactLattice();
      for (int vecNum = 0; vecNum < compactLattice.numPeriodicVectors(); vecNum++) {
        Vector vector = compactLattice.getPeriodicVector(vecNum);
        m_MinPeriodicDistance = Math.min(m_MinPeriodicDistance, vector.length());
      }
      
      // NOT ANYMORE: getNearbyLatticePoints calls findCompactLattice anyway, so there's no reason to do this.
      /*double minVecLength = Double.POSITIVE_INFINITY;
      BravaisLattice semiCompactLattice = this.getQuickOrthogonalLattice();
      for (int vecNum= 0; vecNum < semiCompactLattice.numPeriodicVectors(); vecNum++) {
        Vector vector = semiCompactLattice.getPeriodicVector(vecNum);
        minVecLength = Math.min(minVecLength, vector.length());
      }*/
      
      /*Coordinates origin = this.getOrigin();
      Coordinates[] nearbyCoords = this.getNearbyLatticePoints(origin, minVecLength + 1E-2, false);
      for (int pointNum = 0; pointNum < nearbyCoords.length; pointNum++) {
        double distance = origin.distanceFrom(nearbyCoords[pointNum]);
        m_MinPeriodicDistance = Math.min(m_MinPeriodicDistance, distance);
      }*/
      
      // TODO:  Redo this using compact lattice?  I think the shortest vector in the compact
      // lattice is guaranteed to be the min periodic distance.  The problem is this might
      // change if we change our definition of "compact"
      /*double minVecLength = Double.POSITIVE_INFINITY;
      for (int vecNum= 0; vecNum < this.numPeriodicVectors(); vecNum++) {
        Vector vector = this.getPeriodicVector(vecNum);
        minVecLength = Math.min(minVecLength, vector.length());
      }
      
      
      Coordinates origin = this.getOrigin();
      Coordinates[] nearbyCoords = this.getNearbyLatticePoints(origin, minVecLength + 1E-2, false);
      m_MinPeriodicDistance = Double.POSITIVE_INFINITY;
      for (int pointNum = 0; pointNum < nearbyCoords.length; pointNum++) {
        double distance = origin.distanceFrom(nearbyCoords[pointNum]);
        m_MinPeriodicDistance = Math.min(m_MinPeriodicDistance, distance);
      }*/
      
    }
      
    return m_MinPeriodicDistance;
    
  }
  
  public double[] getCompactPeriodicDistances() {
    
    double[] returnArray = new double[this.numPeriodicVectors()];
    int returnIndex = 0;
    BravaisLattice compactLattice = this.getCompactLattice();
    for (int vecNum = 0; vecNum < compactLattice.numTotalVectors(); vecNum++) {
      if (!compactLattice.isDimensionPeriodic(vecNum)) {continue;}
      Vector vector = compactLattice.getCellVector(vecNum);
      returnArray[returnIndex++] = vector.length();
      
    }
    return returnArray;
    
  }
  
  public double[] getCompactOrthogonalDistances() {
    
    double[] returnArray = new double[this.numPeriodicVectors()];
    int returnIndex = 0;
    BravaisLattice compactLattice = this.getCompactLattice();
    for (int vecNum = 0; vecNum < compactLattice.numTotalVectors(); vecNum++) {
      if (!compactLattice.isDimensionPeriodic(vecNum)) {continue;}
      Vector vector = compactLattice.getCellVector(vecNum);
      Vector normal = compactLattice.m_CartesianNormals[vecNum];
      double projection = Math.abs(vector.innerProductCartesian(normal));
      returnArray[returnIndex++] = projection;
    }
    return returnArray;
    
  }
  
  public double getMinOrthogonalDistance() {
    
    double returnValue = Double.POSITIVE_INFINITY;
    BravaisLattice compactLattice = this.getCompactLattice();
    for (int vecNum = 0; vecNum < compactLattice.numTotalVectors(); vecNum++) {
      if (!compactLattice.isDimensionPeriodic(vecNum)) {continue;}
      Vector vector = compactLattice.getCellVector(vecNum);
      Vector normal = compactLattice.m_CartesianNormals[vecNum];
      double projection = Math.abs(vector.innerProductCartesian(normal));
      returnValue = Math.min(returnValue, projection);
    }
    return returnValue;
    
  }
  
  public Vector getCartesianNormal(int dimNum) {
    return m_CartesianNormals[dimNum];
  }
  
  public Vector getMinOrthogonalVector() {
    
    double minDistance = Double.POSITIVE_INFINITY;
    Vector returnVector = null;
    BravaisLattice compactLattice = this.getCompactLattice();
    for (int vecNum = 0; vecNum < compactLattice.numTotalVectors(); vecNum++) {
      if (!compactLattice.isDimensionPeriodic(vecNum)) {continue;}
      Vector vector = compactLattice.getCellVector(vecNum);
      Vector normal = compactLattice.m_CartesianNormals[vecNum];
      double projection = Math.abs(vector.innerProductCartesian(normal));
      if (projection < minDistance) {
        returnVector = normal.multiply(projection);
        minDistance = projection;
      }
    }
    return returnVector;
    
  }
  
  /*
  public double getMaxCompactPeriodicDistance() {
    double returnValue = 0;
    BravaisLattice compactLattice = this.getCompactLattice();
    for (int vecNum = 0; vecNum < compactLattice.numPeriodicVectors(); vecNum++) {
      Vector vector = compactLattice.getPeriodicVector(vecNum);
      returnValue = Math.max(returnValue, vector.length());
    }
    return returnValue;
  }*/

  protected double getNearbyCoordsRecursive(AbstractBasis directBasis, Coordinates baseCoords, double[] currCoordArray, int dimNum, ArrayList returnCoords, double cutoff, boolean includeGiven) {
    
    // End of the road
    if (dimNum == currCoordArray.length) {
      double distance = baseCoords.distanceFrom(currCoordArray, directBasis);
      if (distance <= cutoff && (includeGiven || distance > CartesianBasis.getPrecision())) {
        Coordinates siteCoords = new Coordinates(currCoordArray, directBasis);
        returnCoords.add(siteCoords);
      }
      return distance;
    }
    
    if (!this.isDimensionPeriodic(dimNum)) {
      return this.getNearbyCoordsRecursive(directBasis, baseCoords, currCoordArray, dimNum + 1, returnCoords, cutoff, includeGiven);
    }
    
    double minDistance = Double.POSITIVE_INFINITY;
    double distance = Double.POSITIVE_INFINITY;
    double lastDistance = Double.POSITIVE_INFINITY;
    
    double[] origCoordArray = ArrayUtils.copyArray(currCoordArray);
    double minCoord = origCoordArray[dimNum];
    do  {
      lastDistance = distance;
      distance = this.getNearbyCoordsRecursive(directBasis, baseCoords, currCoordArray, dimNum + 1, returnCoords, cutoff, includeGiven);
      if (distance < minDistance) {
        minDistance = distance;
        minCoord = currCoordArray[dimNum];
      }
      currCoordArray[dimNum]++;
    } while (distance < cutoff || distance < lastDistance);
    
    distance = Double.POSITIVE_INFINITY;
    //currCoordArray[dimNum] = origCoord - 1;
    System.arraycopy(origCoordArray, dimNum, currCoordArray, dimNum, origCoordArray.length - dimNum);
    currCoordArray[dimNum]--;
    do {
      lastDistance = distance;
      distance = this.getNearbyCoordsRecursive(directBasis, baseCoords, currCoordArray, dimNum + 1, returnCoords, cutoff, includeGiven);
      if (distance < minDistance) {
        minDistance = distance;
        minCoord = currCoordArray[dimNum];
      }
      currCoordArray[dimNum]--;
    } while (distance < cutoff || distance < lastDistance);
    
    currCoordArray[dimNum] = minCoord;
    return minDistance;
    
  }
  
  public SuperLattice getSuperLattice(BravaisLattice superLattice) {
    int[][] superToDirect = this.getSuperToDirect(superLattice);
    return new SuperLattice(this, superToDirect);
  }
  
  // Just use the superlattice constructor instead.
  /*
  public BravaisLattice getSuperLattice(int[][] superToDirect) {

    //Vector[] returnVectors = new Vector[superToDirect.length];
    //for (int returnVecNum = 0; returnVecNum < returnVectors.length; returnVecNum++) {
    //  for (int directVecNum = 0; directVecNum < returnVectors.length; directVecNum++) {
    //    Vector directVector = m_OriginCell.getFiniteVector(directVecNum);
    //    returnVectors[returnVecNum] = 
    //      directVector.multiply(superToDirect[returnVecNum][directVecNum]).add(returnVectors[returnVecNum]);
    //  }
    //}
    //return new BravaisLattice(returnVectors, m_OriginCell.getInfiniteVectors());
    //int[] finiteVectors = new int[superToDirect.length];
    //int finiteVecNum = 0;
    //for (int vecNum = 0; vecNum < m_OriginCell.numCellVectors(); vecNum++) {
    //  if (m_OriginCell.isVectorFinite(vecNum)) {
    //    finiteVectors[finiteVecNum++] = vecNum;
    //  }
    //}
    int[] finiteVectors = this.getPeriodicIndices();
    if (finiteVectors.length != superToDirect.length) {
      throw new RuntimeException("Periodic dimensions of superlattice (" + superToDirect.length + ") don't match periodic dimensions of lattice (" + finiteVectors.length + ").");
    }
    Vector[] returnVectors = m_OriginCell.getCellVectors();
    //for (int returnVecNum = 0; returnVecNum < returnVectors.length; returnVecNum++) {
    //  if (!m_OriginCell.isVectorFinite(returnVecNum)) {continue;}
    //  for (int directVecNum = 0; directVecNum < returnVectors.length; directVecNum++) {
    //    Vector directVector = m_OriginCell.getCellVector(directVecNum);
    //    returnVectors[returnVecNum] = 
    //      directVector.multiply(superToDirect[finiteVectors[returnVecNum]][finiteVectors[directVecNum]]).add(returnVectors[returnVecNum]);
    //  }
    //}
    for (int returnVecNum = 0; returnVecNum < finiteVectors.length; returnVecNum++) {
      int cellVecNum = finiteVectors[returnVecNum];
      returnVectors[cellVecNum] = Vector.getZero3DVector(); //new Vector(new double[] {0,0,0}, CartesianBasis.getInstance());
      for (int directVecNum = 0; directVecNum < finiteVectors.length; directVecNum++) {
        Vector directVector = m_OriginCell.getCellVector(finiteVectors[directVecNum]);
        returnVectors[cellVecNum] = 
          directVector.multiply(superToDirect[returnVecNum][directVecNum]).add(returnVectors[cellVecNum]);
      }
    }
    ParallelCell newCell = new ParallelCell(m_OriginCell.getOrigin(), returnVectors, m_OriginCell.areVectorsFinite());
    return new BravaisLattice(newCell);
  }*/
  
  public boolean[] getDimensionPeriodicity() {
    return m_OriginCell.areVectorsFinite();
  }
  
  public boolean isDimensionPeriodic(int dim) {
    return m_OriginCell.isVectorFinite(dim);
  }
  
  public int[][] getSuperToDirect(BravaisLattice superLattice) {
    return this.getSuperToDirect(superLattice, CartesianBasis.getPrecision());
  }
  
  public int[][] getSuperToDirect(BravaisLattice superLattice, double tolerance) {
    
    double[][] superToCartesian = superLattice.getLatticeBasis().getBasisToCartesian();
    double[][] cartesianToDirect = this.getLatticeBasis().getCartesianToBasis();
    
    double[][] superToDirectDouble = MSMath.matrixMultiply(superToCartesian, cartesianToDirect);
    
    // This should be more well tested.
    int[][] returnArray = new int[this.numPeriodicVectors()][this.numPeriodicVectors()];
    int returnRowNum = 0;
    for (int rowNum = 0; rowNum < superToDirectDouble.length; rowNum++) {
      if (!superLattice.isDimensionPeriodic(rowNum)) {continue;}
      int[] returnRow = returnArray[returnRowNum++];
      int returnColNum = 0;
      for (int colNum = 0; colNum < superToDirectDouble.length; colNum++) {
        if (!this.isDimensionPeriodic(colNum)) {continue;}
        returnRow[returnColNum++] = (int) Math.round(superToDirectDouble[rowNum][colNum]);
      }
    }
    
    // Check to make sure this really works
    double[][] basisToCartesian = this.getLatticeBasis().getBasisToCartesian();
    for (int rowNum = basisToCartesian.length - 1; rowNum >= 0; rowNum--) {
      if (!this.isDimensionPeriodic(rowNum)) {
        basisToCartesian = ArrayUtils.removeElement(basisToCartesian, rowNum);
      }
    }
    
    for (int rowNum = superToCartesian.length - 1; rowNum >= 0; rowNum--) {
      if (!superLattice.isDimensionPeriodic(rowNum)) {
        superToCartesian = ArrayUtils.removeElement(superToCartesian, rowNum);
      }
    }
    
    double[][] reconstructedSuperToCartesian = MSMath.matrixMultiply(returnArray, basisToCartesian);
    for (int rowNum = 0; rowNum < reconstructedSuperToCartesian.length; rowNum++) {
      double[] arrayDiff = MSMath.arrayDiff(superToCartesian[rowNum], reconstructedSuperToCartesian[rowNum]);
      if (MSMath.magnitude(arrayDiff) > tolerance) {return null;}
    }
    
    return returnArray;
    
    /*
    Vector[] superLatticeVectors = superLattice.getPeriodicVectors();
    Vector[] thisLattice = this.getPeriodicVectors();
    int[] thisPeriodicIndices = this.getPeriodicIndices();
    if (superLatticeVectors.length != thisLattice.length) {return null;}
    if (!this.getOrigin().isCloseEnoughTo(superLattice.getOrigin())) {return null;}
    
    int[][] returnArray = new int[superLatticeVectors.length][superLatticeVectors.length];
    AbstractLinearBasis directBasis = this.getLatticeBasis();

    for (int vecNum = 0; vecNum < superLatticeVectors.length; vecNum++) {
      Vector superVector = superLatticeVectors[vecNum];
      //Coordinates rebuiltDirection = directBasis.getOrigin(); // TODO check this
      Coordinates rebuiltHead = superVector.getTail();
      double[] directionArray = superVector.getDirectionArray(directBasis);
      for (int coordNum = 0; coordNum < thisLattice.length; coordNum++) {
        int intCoord = (int) Math.round(directionArray[thisPeriodicIndices[coordNum]]);
        Vector thisVector = thisLattice[coordNum];
        rebuiltHead = rebuiltHead.translateBy(thisVector.multiply(intCoord));
        returnArray[vecNum][coordNum] = intCoord;
      }
      //double distance = rebuiltDirection.distanceFrom(superVector.getDirection());
      double distance = rebuiltHead.distanceFrom(superVector.getHead());
      if (distance > tolerance) {return null;}
      //    .isCloseEnoughTo(superVector.getDirection())) {return null;}
    }
    return returnArray;*/
  }
  
  /**
   * This is just the inverse lattice times 2PI in every direction.  It's the form commonly used by physicists.  
   * 
   * @return
   */
  public BravaisLattice getReciprocalLattice() {
    
    return this.getInverseLattice().multiplyVectorsBy(2 * Math.PI);
    
  }
  
  /**
   * This just gets the inverse lattice; There is no factor of 2*PI, so the inverse of the 
   * inverse lattice is the original lattice;
   * 
   * @return
   */
  public BravaisLattice getInverseLattice() {
    
    /*int numDimensions = this.numDimensions();
    Vector[] finiteVectors = new Vector[numDimensions];
    Vector[] infiniteVectors = new Vector[3 - numDimensions];
    Vector[] periodicVectors = m_OriginCell.getFiniteVectors();
    for (int finiteVecNum = 0; finiteVecNum < finiteVectors.length; finiteVecNum++) {
      double dotProduct = m_CartesianNormals[finiteVecNum].innerProductCartesian(periodicVectors[finiteVecNum]);
      finiteVectors[finiteVecNum] = m_CartesianNormals[finiteVecNum].multiply(2 * Math.PI / dotProduct);
    }
    
    for (int infiniteVecNum = 0; infiniteVecNum < infiniteVectors.length; infiniteVecNum++) {
      infiniteVectors[infiniteVecNum] = m_CartesianNormals[numDimensions + infiniteVecNum];
    }
    
    return new BravaisLattice(finiteVectors, infiniteVectors);*/
    
    /*Vector[] cellVectors = m_OriginCell.getCellVectors();
    Vector[] reciprocalVectors = new Vector[cellVectors.length];
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      double dotProduct = cellVectors[vecNum].innerProductCartesian(m_CartesianNormals[vecNum]);
      reciprocalVectors[vecNum] = m_CartesianNormals[vecNum].multiply(2 * Math.PI / dotProduct);
    }
    ParallelCell newCell = new ParallelCell(m_OriginCell.getOrigin(), reciprocalVectors, m_OriginCell.areVectorsFinite());
    return new BravaisLattice(newCell);*/
    
    /**
     * The method of matrix inversion is more general
     * I eliminate the factor of 2 pi to make the reciprocal of the reciprocal real.
     */
    
    Vector[] nonPeriodicVectors = this.getNonPeriodicVectors();
    Vector[] allVectors = LinearBasis.fill3DBasis(nonPeriodicVectors);
    LinearBasis basis = new LinearBasis(allVectors);
    
    double[][] latticeArray = new double[this.numPeriodicVectors()][this.numPeriodicVectors()];
    for (int vecNum = 0; vecNum < latticeArray.length; vecNum++) {
      double[] coordArray = this.getPeriodicVector(vecNum).getDirectionArray(basis);
      for (int dimNum = 0; dimNum < this.numPeriodicVectors(); dimNum++) {
        latticeArray[vecNum][dimNum] = coordArray[this.numNonPeriodicVectors() + dimNum];
      }
    }
    
    double[][] reciprocalLattice = MSMath.simpleInverse(latticeArray);
    Vector[] returnArray = this.getCellVectors();
    for (int vecNum = 0; vecNum < returnArray.length; vecNum++) {
      if (!this.isDimensionPeriodic(vecNum)) {
        returnArray[vecNum] = this.getCellVector(vecNum);
        continue;
      }
      Vector returnVector = Vector.getZero3DVector();
      for (int dimNum = 0; dimNum < reciprocalLattice.length; dimNum++) {
        Vector periodicVector = allVectors[this.numNonPeriodicVectors() + dimNum];
        returnVector = returnVector.add(periodicVector.multiply(reciprocalLattice[dimNum][vecNum]));
      }
      returnArray[vecNum] = returnVector;//.multiply(2 * Math.PI);
    }
    return new BravaisLattice(returnArray, this.getDimensionPeriodicity());
    
  }
  
  public double getCellSize() {
    return m_OriginCell.getCellSize();
  }
  
  public Coordinates[] generateGrid(int[] halfDimensions) {
    return this.generateGrid(this.getOrigin(), halfDimensions);
  }
  
  public Coordinates[] generateGrid(int halfDimension) {
    int[] halfDimensions = new int[this.numPeriodicVectors()];
    for (int dimNum = 0; dimNum < halfDimensions.length; dimNum++) {
      halfDimensions[dimNum] = halfDimension;
    }
    return this.generateGrid(halfDimensions);
  }
  
  public Coordinates[] generateGrid(Coordinates center, int[] halfDimensions) {
    
    if (halfDimensions.length != this.numPeriodicVectors()) {
      throw new RuntimeException("Generated lattice grid must have same dimensions as lattice");
    }
    
    // Find the corner of this grid
    Coordinates corner = center;
    int[] gridDimensions = new int[halfDimensions.length];
    for (int dimNum = 0; dimNum < halfDimensions.length; dimNum++) {
      Vector translation = this.getPeriodicVector(dimNum).multiply(-1 * halfDimensions[dimNum]);
      corner = corner.translateBy(translation);
      gridDimensions[dimNum] = halfDimensions[dimNum] * 2 + 1;
    }
    
    // Generate the grid points
    ArrayIndexer siteIterator = new ArrayIndexer(gridDimensions);
    Coordinates[] grid = new Coordinates[siteIterator.numAllowedStates()];
    for (int siteNum = 0; siteNum < grid.length; siteNum++) {
      Coordinates site = corner;
      int[] siteCoordArray = siteIterator.splitValue(siteNum);
      for (int dimNum = 0; dimNum < this.numPeriodicVectors(); dimNum++) {
        int coefficient = siteCoordArray[dimNum];
        Vector translateVector = this.getPeriodicVector(dimNum).multiply(coefficient);
        site = site.translateBy(translateVector);
      }
      grid[siteNum] = site;
    }
    
    return grid;
  }
  
  /*public BravaisLattice getCompactLattice() {
    Vector[] cellVectors = this.getCellVectors();
    boolean[] isVectorPeriodic = this.getDimensionPeriodicity();
    boolean minimizing = true;
    while (minimizing) {
      minimizing = false;
      for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
        Vector vec = cellVectors[vecNum];
        if (!isVectorPeriodic[vecNum]) {continue;}
        for (int deltaVecNum = 0; deltaVecNum < cellVectors.length; deltaVecNum++) {
          if (deltaVecNum == vecNum) {continue;}
          if (!isVectorPeriodic[deltaVecNum]) {continue;}
          Vector deltaVec = cellVectors[deltaVecNum];
          int direction = vec.innerProductCartesian(deltaVec) < 0 ? 1 : -1;
          deltaVec = deltaVec.multiply(direction);
          Vector newVec = vec.add(deltaVec);
          while (newVec.length() < vec.length()) {
            vec = newVec;
            newVec = vec.add(deltaVec);
            minimizing = true;
          }
          cellVectors[vecNum] = vec;
        }
      }
    }
    return new BravaisLattice(cellVectors, isVectorPeriodic);
  }*/
  
  public BravaisLattice rotateToTriangularLattice() {
    
    Vector[] oldVectors = this.getCellVectors();
    Vector[] newVectors = new Vector[oldVectors.length];
    
    // Basically Gram-Schmidt
    double[][] newMatrix = new double[oldVectors.length][oldVectors.length];
    for (int vecNum = 0; vecNum < newVectors.length; vecNum++) {

      double[] cartArray = newMatrix[vecNum];
      for (int prevVecNum = 0; prevVecNum <= vecNum; prevVecNum++) {
        double innerProduct = oldVectors[vecNum].innerProductCartesian(oldVectors[prevVecNum]);
        for (int dimNum = 0; dimNum < prevVecNum; dimNum++) {
          innerProduct -= cartArray[dimNum] * newMatrix[prevVecNum][dimNum];
        }
        if (prevVecNum == vecNum) {
          cartArray[prevVecNum] = Math.sqrt(innerProduct);
        } else {
          cartArray[prevVecNum] = innerProduct / newMatrix[prevVecNum][prevVecNum];
        }
        
      }
      
      newVectors[vecNum] = new Vector(cartArray);
      
    }
    
    /* For debugging
    for (int vecNum = 0; vecNum < oldVectors.length; vecNum++) {
      for (int vecNum2 = 0; vecNum2 < oldVectors.length; vecNum2++) {
        double oldInnerProduct = oldVectors[vecNum].innerProductCartesian(oldVectors[vecNum2]);
        double newInnerProduct = newVectors[vecNum].innerProductCartesian(newVectors[vecNum2]);
        System.out.println(oldInnerProduct + ", " + newInnerProduct);
      }
    }
    */
    
    return new BravaisLattice(this.getOrigin(), newVectors, m_OriginCell.areVectorsFinite());
    
  }
  

  public boolean isMoreCompactThan(BravaisLattice otherLattice) {
    
    return this.isMoreCompactThan(otherLattice, false);
    
  }
  
  public boolean isMoreCompactThan(BravaisLattice otherLattice, boolean useOrthogonalDistance) {
    
    if (this.numPeriodicVectors() != otherLattice.numPeriodicVectors()) {
      throw new RuntimeException("Can't compare compactness of two lattices with different number of periodic dimensions.");
    }
    
    double[] thisDistances = useOrthogonalDistance ? this.getCompactOrthogonalDistances() : this.getCompactPeriodicDistances();
    double[] otherDistances = useOrthogonalDistance ? otherLattice.getCompactOrthogonalDistances() : otherLattice.getCompactPeriodicDistances();

    Arrays.sort(thisDistances);
    Arrays.sort(otherDistances);
    
    for (int vecNum = 0; vecNum < thisDistances.length; vecNum++) {
      if (thisDistances[vecNum] > otherDistances[vecNum] + CartesianBasis.getPrecision()) {
        return true;
      }
      if (thisDistances[vecNum] < otherDistances[vecNum] - CartesianBasis.getPrecision()) {
        return false;
      }
    }
    
    return false;
    
  }
  
  public BravaisLattice getCompactLattice() {
    if (m_CompactLattice == null) {
      this.findCompactLattice();
    }
    return m_CompactLattice;
  }
  
  /**
   * TODO eventually merge these into just "getCompactLattice",
   * assuming all works well with using the greedy-reduced lattice.
   * 
   * 10/22/15 -- I took a step in this direction by making this private.  TKM
   * 
   * @return
   */
  private BravaisLattice getQuickCompactLattice() {
    
    // I changed all of this from "orthogonal lattice" to "compact lattice" 
    // because the Minkowski-reduced lattice is not necessarily the most orthogonal one.
            
    if (m_QuickCompactLattice == null) {
      this.findQuickCompactLattice();
    }
    
    return m_QuickCompactLattice;
    
  }
  
  /**
   * This is a greedy reduction algorithm, which should result in a Minkowski-reduced
   * lattice up to three dimensions.
   */
  protected void findQuickCompactLattice() {
    Vector[] cellVectors = this.getCellVectors();
    int[] periodicIndices = this.getPeriodicIndices();
    
    // First pass minimization, just to reduce the search radius and better improve the search on the semi-compact lattice
    boolean minimizing = true;
    double precision = 1E-7; // For numerical problems with 60 degree angles TODO use angle tolerance?
    while (minimizing) {
      minimizing = false;
      for (int vecNum = 0; vecNum < periodicIndices.length; vecNum++) {
        Vector vector = cellVectors[periodicIndices[vecNum]];
        double magSq = vector.innerProductCartesian(vector);
        for (int increment = 1; increment < periodicIndices.length; increment++) {
          int vecNum2 = (vecNum + increment) % periodicIndices.length;
          Vector vector2 = cellVectors[periodicIndices[vecNum2]];
          double innerProduct = vector2.innerProductCartesian(vector) / magSq;
          innerProduct = MSMath.roundWithPrecision(innerProduct, precision);
          double shift = Math.round(innerProduct);
          minimizing |= (shift != 0);
          cellVectors[periodicIndices[vecNum2]] = vector2.subtract(vector.multiply(shift));
        }
      }
    }
    
    /**
     * The previous loop makes sure all vectors are pair-reduced.
     * In three dimensions, we need to go one step further. 
     * See "A 3-Dimensional Lattice Reduction Algorithm" Igor Semaev
     */
    if (this.numPeriodicVectors() == 3) {
      int eps01 = (int) Math.signum(cellVectors[0].innerProductCartesian(cellVectors[1]));
      int eps02 = (int) Math.signum(cellVectors[0].innerProductCartesian(cellVectors[2]));
      int eps12 = (int) Math.signum(cellVectors[1].innerProductCartesian(cellVectors[2]));

      if (eps01 * eps02 * eps12 == -1) {
        Vector newVec = cellVectors[0];
        newVec = newVec.subtract(cellVectors[1].multiply(eps01));
        newVec = newVec.subtract(cellVectors[2].multiply(eps02));
        int maxLengthIndex = (cellVectors[0].length() > cellVectors[1].length()) ? 0 : 1;
        maxLengthIndex = cellVectors[maxLengthIndex].length() > cellVectors[2].length() ? maxLengthIndex : 2;
        if (newVec.length() < cellVectors[maxLengthIndex].length()) {
          cellVectors[maxLengthIndex] = newVec;
        }
      }
    }
    
    m_QuickCompactLattice = new BravaisLattice(this.getOrigin(), cellVectors, this.getDimensionPeriodicity());
    m_QuickCompactLattice.m_QuickCompactLattice = m_QuickCompactLattice;
  }
  
  protected void findCompactLattice() {    
    
    BravaisLattice semiCompactLattice = this.getQuickCompactLattice();
    semiCompactLattice.m_CompactLattice = semiCompactLattice; // This is a bit of a hack to break an infinite loop.  Only useful because we are discarding this lattice
    
    // Up to three dimensions, the greedy reduced lattice should be Minkowski reduced
    // See Low-Dimensional Lattice Basis Reduction Revisited, Phong Q. Nguyen and Damien Stehle
    this.m_CompactLattice = semiCompactLattice;
    
    // Now we search for all vectors within the radius spanned by the existing vectors
    /**double maxDistance = 0;
    for (int vecNum = 0; vecNum < semiCompactLattice.numTotalVectors(); vecNum++) {
      if (!this.isDimensionPeriodic(vecNum)) {continue;}
      maxDistance = Math.max(maxDistance, semiCompactLattice.getCellVector(vecNum).length());
    }
    maxDistance += 1E-4;
    
    Coordinates origin = semiCompactLattice.getOrigin();
    Coordinates[] nearbyCoords = semiCompactLattice.getNearbyLatticePoints(origin, maxDistance, false);
    Vector[] candidateVectors = new Vector[nearbyCoords.length];
    for (int vecNum= 0; vecNum < candidateVectors.length; vecNum++) {
      candidateVectors[vecNum] = new Vector(origin, nearbyCoords[vecNum]);
    }
    
    Vector[] cellVectors = semiCompactLattice.getCompactCellVectors(candidateVectors);
    m_CompactLattice = new BravaisLattice(new ParallelCell(semiCompactLattice.getOrigin(), cellVectors, this.getDimensionPeriodicity()));
    m_CompactLattice.m_CompactLattice = m_CompactLattice;*/
  }
  
  protected Vector[] getCompactCellVectors(Vector[] candidateVectors) {
    
    Vector[] cellVectors = this.getCellVectors();
    Vector[] orthogonalBasis = new Vector[cellVectors.length];
    
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      if (!this.isDimensionPeriodic(vecNum)) {
        //Vector shiftedVector = m_CartesianNormals[vecNum];
        Vector shiftedVector = this.getCellVector(vecNum);
        for (int prevVecNum = 0; prevVecNum < vecNum; prevVecNum++) {
          if (this.isDimensionPeriodic(vecNum)) {continue;}
          Vector prevUnitVector = orthogonalBasis[prevVecNum];
          double innerProduct = shiftedVector.innerProductCartesian(prevUnitVector);
          shiftedVector = shiftedVector.subtract(prevUnitVector.multiply(innerProduct)); 
        }
        orthogonalBasis[vecNum] = shiftedVector.unitVectorCartesian();
        cellVectors[vecNum] = shiftedVector;
        continue;
      }
      double minDistance = Double.POSITIVE_INFINITY;
      for (int candidateNum = 0; candidateNum < candidateVectors.length; candidateNum++) {
        Vector candidateVector = candidateVectors[candidateNum];
        double distance = candidateVector.length();
        if (distance < minDistance) {
          Vector shiftedVector = candidateVector;
          for (int prevVecNum = 0; prevVecNum < vecNum; prevVecNum++) {
            Vector prevUnitVector = orthogonalBasis[prevVecNum];
            double innerProduct = shiftedVector.innerProductCartesian(prevUnitVector);
            shiftedVector = shiftedVector.subtract(prevUnitVector.multiply(innerProduct)); 
          }
          if (shiftedVector.length() < CartesianBasis.getPrecision()) {
            continue;
          }
          minDistance = distance;
          orthogonalBasis[vecNum] = shiftedVector.unitVectorCartesian();
          cellVectors[vecNum] = candidateVector;
        }
      }
    }
    
    return cellVectors;
  }
  
  public Coordinates[] getNearbyLatticePoints(Coordinates coords, double distance, boolean includeGiven) {


    /*if (m_CompactLattice != this) {
      return this.getCompactLattice().getNearbyLatticePoints(coords, distance, includeGiven);
    }*/
    
    // semiCompactLattice is much faster to calculate and requires less memory
    if (m_QuickCompactLattice != this) {
      return this.getQuickCompactLattice().getNearbyLatticePoints(coords, distance, includeGiven);
    }
    
    /**
     * First shift the origin
     * Not sure why we have to do this.  Perhaps it was because the 
     * lattice basis didn't use to be relative to the lattice origin?
     */
    /*coords = this.getRelativeCoords(coords);
    Vector shift = new Vector(CartesianBasis.getOrigin(), this.getOrigin());*/
    
    ArrayList returnPoints = new ArrayList();
    //LinearBasis latticeBasis = m_OriginCell.getCellBasis();
    LinearBasis latticeBasis = this.getLatticeBasis();
    double[] coordArray = coords.getCoordArray(latticeBasis);
    //this.getSitesRecursive(coords, coordArray, 0, returnPoints, distance, includeGiven);
    this.getNearbyCoordsRecursive(latticeBasis, coords, coordArray, 0, returnPoints, distance, includeGiven);
    Coordinates[] returnArray = new Coordinates[returnPoints.size()];
    for (int siteNum = 0; siteNum < returnArray.length; siteNum++) {
      returnArray[siteNum] = (Coordinates) returnPoints.get(siteNum);
      
      // Now undo the shift we did before
      //returnArray[siteNum] = returnArray[siteNum].translateBy(shift);
    }
    return returnArray;    
  }
  
  
/*
  protected double getSitesRecursive(Coordinates baseCoords, double[] currCoordArray, int dimNum, ArrayList returnPoints, double cutoff, boolean includeGiven) {
    
    AbstractBasis directBasis = m_OriginCell.getCellBasis();
    
    // End of the road
    if (dimNum == currCoordArray.length) {
      double distance = baseCoords.distanceFrom(currCoordArray, directBasis);
      if (distance <= cutoff && (includeGiven || distance > CartesianBasis.getPrecision())) {
        Coordinates siteCoords = new Coordinates(currCoordArray, directBasis);
        returnPoints.add(siteCoords);
      }
      return distance;
    }
    
    if (!this.isDimensionPeriodic(dimNum)) {
      return this.getSitesRecursive(baseCoords, currCoordArray, dimNum + 1, returnPoints, cutoff, includeGiven);
    }
    
    double minDistance = Double.POSITIVE_INFINITY;
    double distance = Double.POSITIVE_INFINITY;
    double lastDistance = Double.POSITIVE_INFINITY;
    
    double[] origCoordArray = ArrayUtils.copyArray(currCoordArray);
    double minCoord = origCoordArray[dimNum];
    do  {
      lastDistance = distance;
      distance = this.getSitesRecursive(baseCoords, currCoordArray, dimNum + 1, returnPoints, cutoff, includeGiven);
      if (distance < minDistance) {
        minDistance = distance;
        minCoord = currCoordArray[dimNum];
      }
      currCoordArray[dimNum]++;
    } while (distance < cutoff || distance < lastDistance);
    
    distance = Double.POSITIVE_INFINITY;
    //currCoordArray[dimNum] = origCoord - 1;
    System.arraycopy(origCoordArray, dimNum, currCoordArray, dimNum, origCoordArray.length - dimNum);
    currCoordArray[dimNum]--;
    do {
      lastDistance = distance;
      distance = this.getSitesRecursive(baseCoords, currCoordArray, dimNum + 1, returnPoints, cutoff, includeGiven);
      if (distance < minDistance) {
        minDistance = distance;
        minCoord = currCoordArray[dimNum];
      }
      currCoordArray[dimNum]--;
    } while (distance < cutoff || distance < lastDistance);
    
    currCoordArray[dimNum] = minCoord;
    return minDistance;
    
  }*/
  
  public BravaisLattice getSubLattice(Vector[] potentialVectors, int numPrimCells) {
    if (numPrimCells == 1) {return this;}
    double targetVolume = this.getCellSize() / numPrimCells;

    Vector[] periodicVectors = this.getPeriodicVectors();
    
    // We replace potential vectors with Wigner-Seitz equivalents
    // This, along with the minlength check, makes sure we always keep the shorter of vectors that are multiples of each other
    // TODO make sure this does not break anything
    potentialVectors = (Vector[]) ArrayUtils.copyArray(potentialVectors);
    for (int vecNum = 0; vecNum < potentialVectors.length; vecNum++) {
      Vector vector = potentialVectors[vecNum];
      Coordinates newHead = this.getWignerSeitzNeighbor(vector.getTail(), vector.getHead());
      potentialVectors[vecNum] = new Vector(vector.getTail(), newHead);
    }
    
    // We replace the zero-length vector with a unit-length vector
    if (periodicVectors.length > 0) {
      for (int vecNum = 0; vecNum < potentialVectors.length; vecNum++) {
        if (potentialVectors[vecNum].length() < CartesianBasis.getPrecision()) {
          //potentialVectors[vecNum]= potentialVectors[vecNum].add(periodicVectors[0]);
          potentialVectors[vecNum]= periodicVectors[0];
          for (int dim = 1; dim < periodicVectors.length; dim++) {
            potentialVectors = (Vector[]) ArrayUtils.appendElement(potentialVectors, periodicVectors[dim]);
          }
          break;
        }
      }
    }
    
    boolean foundPrimCell = false;
    for (int vecNum = 0; vecNum < periodicVectors.length; vecNum++) {

      Vector minVector = periodicVectors[vecNum];
      double minCellVolume = Double.POSITIVE_INFINITY;
      for (int potentialVecNum = 0; potentialVecNum < potentialVectors.length; potentialVecNum++) {
        Vector potentialVector = potentialVectors[potentialVecNum];
        periodicVectors[vecNum] = potentialVector;
                
          double cellVolume= ParallelCell.getCellSize(periodicVectors);
          if (Math.round(cellVolume / targetVolume) == 1) {
            foundPrimCell = true;
            break;
          }
          if (cellVolume > targetVolume) {
            double multiples = minVector.unitVectorCartesian().innerProductCartesian(potentialVector);
            // Check to see if they are collinear, and if so keep the shorter
            // Can't just do a volume check here because of numerical issues
            if (minVector.multiply(multiples).subtract(potentialVector).length() < CartesianBasis.getPrecision()) {
              if (potentialVector.length() < minVector.length()) {
                minVector = potentialVector;
                minCellVolume = cellVolume;
              }
            } else if (cellVolume < minCellVolume) { // Everyone else, just check volume
              minCellVolume = cellVolume;
              minVector = potentialVector;
            }
          }

          periodicVectors[vecNum] = minVector;
      }
      if (foundPrimCell) {break;}
    }
    
    /*for (int vecNum = 0; vecNum < periodicVectors.length; vecNum++) {

      double minLength = Double.POSITIVE_INFINITY;
      Vector minVector = periodicVectors[vecNum];
      for (int potentialVecNum = 0; potentialVecNum < potentialVectors.length; potentialVecNum++) {
        Vector potentialVector = potentialVectors[potentialVecNum];
        periodicVectors[vecNum] = potentialVector;
                
          double cellSize= ParallelCell.getCellSize(periodicVectors);
          if (Math.round(cellSize / targetVolume) == 1) {
            foundPrimCell = true;
            break;
          }
          double length = potentialVector.length();
          if (length < minLength && cellSize > targetVolume) {
            minVector = potentialVector;
            minLength = length;
          }
          periodicVectors[vecNum] = minVector;
      }
      if (foundPrimCell) {break;}
    }*/
    
    if (!foundPrimCell) {
      return null;
    }
    
    Vector[] cellVectors = this.getCellVectors();
    boolean[] isVectorPeriodic = this.getDimensionPeriodicity();
    int cellIndex = 0;
    for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
      if (!isVectorPeriodic[vecNum]) {continue;}
      cellVectors[vecNum] = periodicVectors[cellIndex++];
    }
    return new BravaisLattice(this.getOrigin(), cellVectors, this.getDimensionPeriodicity());
  }
  
  public BravaisType getBravaisType() {
    
    if (m_BravaisType == null) {
      m_BravaisType = this.findBravaisType();
    }
    
    return m_BravaisType;
  }
  
  private BravaisType findBravaisType() {

    return getBravaisType(this.getLatticeSystem(), this.getConventionalLattice());
    
  }
  
  public SuperLattice getConventionalLattice() {
    if (m_ConventionalLattice == null) {
      SymmetryOperation[] operations = this.getPointGroup().getOperations();
      m_ConventionalLattice = findConventionalLattice(this, operations);
      m_ConventionalLattice.m_ConventionalLattice = m_ConventionalLattice;
    }
    return m_ConventionalLattice;
  }

  public static BravaisType getBravaisType(LatticeSystem latticeSystem, SuperLattice conventionalLattice) {

    int numPrimCells = conventionalLattice.numPrimCells();
    
    if (latticeSystem == LatticeSystem.CUBIC_3D) {
      if (numPrimCells == 4) {return BravaisType.FCC_3D;}
      if (numPrimCells == 2) {return BravaisType.BCC_3D;}
      if (numPrimCells == 1) {return BravaisType.CUBIC_3D;}
    }
    
    if (latticeSystem == LatticeSystem.TETRAGONAL_3D) {
      if (numPrimCells == 2) {return BravaisType.TETRAGONAL_I_3D;}
      if (numPrimCells == 1) {return BravaisType.TETRAGONAL_3D;}
    }
    
    if (latticeSystem == LatticeSystem.ORTHORHOMBIC_3D) {
      if (numPrimCells == 4) {return BravaisType.ORTHORHOMBIC_F_3D;}
      if (numPrimCells == 1) {return BravaisType.ORTHORHOMBIC_3D;}
      if (numPrimCells == 2) {
        Coordinates bodyCenter = new Coordinates(new double[] {0.5, 0.5, 0.5}, conventionalLattice.getLatticeBasis());
        BravaisLattice primLattice = conventionalLattice.getPrimLattice();
        if (primLattice.isLatticePoint(bodyCenter)) {
          return BravaisType.ORTHORHOMBIC_I_3D;
        }
        return BravaisType.ORTHORHOMBIC_C_3D;
      }
    }
    
    if (latticeSystem == LatticeSystem.MONOCLINIC_3D) {
      if (numPrimCells == 2) {return BravaisType.MONOCLINIC_C_3D;}
      if (numPrimCells == 1) {return BravaisType.MONOCLINIC_3D;}
    }
    
    if (latticeSystem == LatticeSystem.RECTANGULAR_2D) {
      if (numPrimCells == 2) {return BravaisType.RHOMBIC_2D;}
      if (numPrimCells == 1) {return BravaisType.RECTANGULAR_2D;}
    }
    
    return BravaisType.valueOf(latticeSystem.name());
  }
  
  public LatticeSystem getLatticeSystem() {
    if (m_LatticeSystem == null) {
      m_LatticeSystem = this.findLatticeSystem();
    }
    // All others are the same as the crystal family
    return m_LatticeSystem;
  }
  
  private LatticeSystem findLatticeSystem() {
    CrystalFamily crystalFamily = this.getCrystalFamily();
    if (crystalFamily == CrystalFamily.HEXAGONAL_3D) {
      SuperLattice conventionalLattice = this.getConventionalLattice();
      LinearBasis conventionalBasis = conventionalLattice.getLatticeBasis();
      Coordinates testCoords = new Coordinates(new double[] {2.0/3, 1.0/3, 1.0/3}, conventionalBasis);
      if (conventionalLattice.getPrimLattice().isLatticePoint(testCoords)) {
        return LatticeSystem.RHOMBOHEDRAL_3D;
      } else {
        return LatticeSystem.HEXAGONAL_3D;
      }
    }
    return LatticeSystem.valueOf(crystalFamily.toString());
  }
  
  private CrystalFamily findCrystalFamily() {
    
    SymmetryOperation[] operations = this.getPointGroup().getOperations();
    int numDimensions = this.numPeriodicVectors();
    return getCrystalFamily(operations, numDimensions);
    
  }
  
  public static CrystalFamily getCrystalFamily(SymmetryOperation[] operations, int numDimensions) {
    
    if (numDimensions == 3) {
      Vector[] rotations120 = new Vector[0];
      Vector[] rotations180 = new Vector[0];
      int num90 = 0;
      int numMirror = 0;
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        if (op.isReflection() != null) {
          numMirror++; 
          continue;
        }
        Vector rotation = op.isRotationOrRotoInversion();
        if (rotation == null) {continue;}
        int angle = (int) Math.round(Math.toDegrees(rotation.length()));
        if (angle == 120) {
          boolean seenAxis = false;
          for (int vecNum = 0; vecNum < rotations120.length; vecNum++) {
            if (rotation.isParallelTo(rotations120[vecNum])) {
              seenAxis = true;
              break;
            }
          }
          if (seenAxis) {continue;}
          rotations120 = (Vector[]) ArrayUtils.appendElement(rotations120, rotation);
          continue;
        }
        if (angle == 180) {
          boolean seenAxis = false;
          for (int vecNum = 0; vecNum < rotations180.length; vecNum++) {
            if (rotation.isParallelTo(rotations180[vecNum])) {
              seenAxis = true;
              break;
            }
          }
          if (seenAxis) {continue;}
          rotations180 = (Vector[]) ArrayUtils.appendElement(rotations180, rotation);
          continue;
        }
        if (angle == 90) {
          num90++;
        }
      }
      if (rotations120.length == 4) {
        return CrystalFamily.CUBIC_3D;
      } else if (num90 > 0) {
        return CrystalFamily.TETRAGONAL_3D;
      } else if (rotations120.length > 0) {
        return CrystalFamily.HEXAGONAL_3D;
      } else if ((rotations180.length == 3) || (rotations180.length == 1 && numMirror == 2)) {
        return CrystalFamily.ORTHORHOMBIC_3D;
      } else if (rotations180.length == 1 || numMirror == 1) {
        return CrystalFamily.MONOCLINIC_3D;
      } else {
        return CrystalFamily.TRICLINIC_3D;
      }
    }
    
    if (numDimensions == 2) {
      int numMirror = 0;
      for (int opNum = 0; opNum < operations.length; opNum++) {
        SymmetryOperation op = operations[opNum];
        Vector rotation = op.isRotation();
        if (rotation != null) {
          int angle = (int) Math.round(Math.toDegrees(rotation.length()));
          if (angle == 90) {
            return CrystalFamily.SQUARE_2D;
          } else if (angle == 120) {
            return CrystalFamily.HEXAGONAL_2D;
          }
        }
        if (op.isReflection() != null) {numMirror++;}
      }
      if (numMirror > 0) {
        return CrystalFamily.RECTANGULAR_2D;
      } else {
        return CrystalFamily.OBLIQUE_2D;
      } 
    }
    
    if (numDimensions == 1) {
      return CrystalFamily.LINEAR_1D;
    }
    
    if (numDimensions == 0) {
      return CrystalFamily.POINT_0D;
    }
    
    return null;
    
  }
  
  public CrystalFamily getCrystalFamily() {
    if (m_CrystalFamily == null) {
      m_CrystalFamily = this.findCrystalFamily();
    }
    return m_CrystalFamily;
  }
  
  
  public BravaisLattice getConventionalPrimLattice() {
    BravaisLattice returnLattice = findConventionalPrimLattice(this.getConventionalLattice(), this.getBravaisType());
    returnLattice.m_ConventionalLattice = this.getConventionalLattice();
    return returnLattice;
  }
  
  /**
   * WARNING!  This currently only works for symmorphic space groups.
   * Orientations of glide planes, etc. won't necessarily be consistent.
   * 
   * @param conventionalLattice
   * @param type
   * @return
   */
  public static BravaisLattice findConventionalPrimLattice(SuperLattice conventionalLattice, BravaisType type) {
    
    if (conventionalLattice.numPrimCells() == 1) {
      return conventionalLattice;
    }
    
    Vector[] cellVectors = null;
    
    // Requires the vector that is not the face-centered plane to be the "b" vector
    if (type == BravaisType.MONOCLINIC_C_3D) {
      cellVectors = conventionalLattice.getCellVectors();
      LinearBasis latticeBasis = conventionalLattice.getLatticeBasis();
      Coordinates origin = conventionalLattice.getOrigin();
      Coordinates face = new Coordinates(new double[] {0.5, 0, 0.5}, latticeBasis);
      cellVectors[2] = new Vector(origin, face);
    }
    
    if (type == BravaisType.ORTHORHOMBIC_C_3D) {
      cellVectors = conventionalLattice.getCellVectors();
      LinearBasis latticeBasis = conventionalLattice.getLatticeBasis();
      Coordinates origin = conventionalLattice.getOrigin();
      Coordinates face = new Coordinates(new double[] {0.5, 0.5, 0}, latticeBasis);
      cellVectors[1] = new Vector(origin, face);
      return new BravaisLattice(cellVectors);
    }
    
  if (type == BravaisType.ORTHORHOMBIC_F_3D || type == BravaisType.FCC_3D) {
      cellVectors = conventionalLattice.getCellVectors();
      LinearBasis latticeBasis = conventionalLattice.getLatticeBasis();
      Coordinates origin = conventionalLattice.getOrigin();
      Coordinates face1 = new Coordinates(new double[] {0.5, 0.5, 0}, latticeBasis);
      Coordinates face2 = new Coordinates(new double[] {0.5, 0, 0.5}, latticeBasis);
      Coordinates face3 = new Coordinates(new double[] {0, 0.5, 0.5}, latticeBasis);
      cellVectors[0] = new Vector(origin, face1);
      cellVectors[1] = new Vector(origin, face2);
      cellVectors[2] = new Vector(origin, face3);
      return new BravaisLattice(cellVectors);
    }
    
    if (type == BravaisType.ORTHORHOMBIC_I_3D || type == BravaisType.BCC_3D || type == BravaisType.TETRAGONAL_I_3D) {
      cellVectors = conventionalLattice.getCellVectors();
      LinearBasis latticeBasis = conventionalLattice.getLatticeBasis();
      Coordinates origin = conventionalLattice.getOrigin();
      Coordinates center = new Coordinates(new double[] {0.5, 0.5, 0.5}, latticeBasis);
      cellVectors[2] = new Vector(origin, center);
      return new BravaisLattice(cellVectors);
    }
    
    // The angles between the first two conventional vectors should be 60 degrees
    if (type == BravaisType.RHOMBOHEDRAL_3D) {
      cellVectors = conventionalLattice.getCellVectors();
      LinearBasis latticeBasis = conventionalLattice.getLatticeBasis();
      Coordinates origin = conventionalLattice.getOrigin();
      Coordinates thirdPoint = new Coordinates(new double[] {2.0 / 3, 1.0 / 3, 1.0 / 3}, latticeBasis);
      // We shouldn't have to do the below because it is now taken care of in the
      // full conventional cell code.
      /*BravaisLattice primLattice = conventionalLattice.getPrimLattice();
      if (!primLattice.isLatticePoint(thirdPoint)) {
        thirdPoint = new Coordinates(new double[] {1.0 / 3, 2.0 / 3, 1.0 / 3}, latticeBasis);   
        Vector tempVector = cellVectors[0];
        cellVectors[0] = cellVectors[1];
        cellVectors[1] = tempVector; 
      } */
      cellVectors[2] = new Vector(origin, thirdPoint);
      return new BravaisLattice(cellVectors);
    }
    
    if (type == BravaisType.RHOMBIC_2D) {
      cellVectors = conventionalLattice.getCellVectors();
      Coordinates origin = conventionalLattice.getOrigin();
      Coordinates center = origin.translateBy(cellVectors[0].multiply(0.5)).translateBy(cellVectors[1].multiply(0.5));
      cellVectors[1] = new Vector(origin, center);
    }
    
    if (cellVectors != null) {
      BravaisLattice returnLattice = new BravaisLattice(conventionalLattice.getOrigin(), cellVectors, conventionalLattice.getDimensionPeriodicity());
      returnLattice.m_ConventionalLattice = conventionalLattice;
      return returnLattice;
    }
    return null;
    
  }
  
  public static CrystalFamily getFamilyForType(BravaisType type) {
    
    if (type == BravaisType.RHOMBOHEDRAL_3D || type == BravaisType.HEXAGONAL_3D) {
      return CrystalFamily.HEXAGONAL_3D;
    }
    
    if (type == BravaisType.RHOMBIC_2D) {
      return CrystalFamily.RECTANGULAR_2D;
    }
    
    return CrystalFamily.valueOf(type.name());
    
  }
  
  /**
   * WARNING!  This currently only works for symmorphic space groups.
   * Orientations of glide planes, etc. won't necessarily be consistent.
   * 
   * @param lattice
   * @param ops
   * @return
   */
  public static SuperLattice findConventionalLattice(BravaisLattice lattice, SymmetryOperation[] ops) {

    CrystalFamily crystalFamily = BravaisLattice.getCrystalFamily(ops, lattice.numPeriodicVectors());
    
    BravaisLattice orthogonalLattice = lattice.getQuickCompactLattice();
    double maxDistance = 0;
    for (int vecNum = 0; vecNum < orthogonalLattice.numTotalVectors(); vecNum++) {
      if (!orthogonalLattice.isDimensionPeriodic(vecNum)) {continue;}
      maxDistance = Math.max(maxDistance, orthogonalLattice.getCellVector(vecNum).length());
    }
    
    //Coordinates origin = orthogonalLattice.getOrigin();    
    Coordinates origin = lattice.getOrigin();  // Should be the same
    
    double factor = -1;
    
    switch (crystalFamily) {
      case CUBIC_3D:
        factor = 1.5; break;
      case HEXAGONAL_3D:
        factor = 3; break;
      case MONOCLINIC_3D:
        factor = 2; break;
      case ORTHORHOMBIC_3D:
        factor = 2; break;
      case TETRAGONAL_3D:
        factor = 2; break;
      case TRICLINIC_3D:
        factor = 1.1; break;
      default:
        factor = 2;
    }
    
   // double factor = (numDimensions == 3) ? 3 : 2; // The factor of 3 is for trigonal lattices    
    //double searchDistance = maxDistance * factor + 1E-6;
    double searchDistance = maxDistance * factor + 2 * CartesianBasis.getPrecision();
    Coordinates[] nearbyPoints = orthogonalLattice.getNearbyLatticePoints(origin, searchDistance, false);
    Vector[] potentialVectors = new Vector[nearbyPoints.length];
    for (int vecNum = 0; vecNum < nearbyPoints.length; vecNum++) {
      potentialVectors[vecNum] = new Vector(origin, nearbyPoints[vecNum]);
    }
        
    boolean[] periodicity = new boolean[] {true, true, true};
    Vector[] cellVectors = null;
    
    if (crystalFamily == CrystalFamily.CUBIC_3D) {
      Vector twoFoldRotation = null;
      for (int opNum = 0; opNum < ops.length; opNum++) {
        SymmetryOperation op = ops[opNum];
        Vector axis = op.isRotationOrRotoInversion();
        if (axis == null) {continue;}
        double angle = Math.toDegrees(axis.length());
        if (Math.abs(angle - 90) < CartesianBasis.getAngleToleranceDegrees())  {
          cellVectors = findConventionalVectors(axis, potentialVectors, crystalFamily, lattice);
          if (cellVectors != null) {break;}
        } else if (Math.abs(angle - 180) < CartesianBasis.getAngleToleranceDegrees()) {
          twoFoldRotation = axis;
        }  
      }
      if (cellVectors == null) { // For 23 and m-3 point groups (no 4-fold rotation).  Pretty sure for these the only 2-fold rotation axes are parallel conventional axes.
        cellVectors = findConventionalVectors(twoFoldRotation, potentialVectors, crystalFamily, lattice);
      }
    }
        
    if (crystalFamily == CrystalFamily.HEXAGONAL_3D) {
      for (int opNum = 0; opNum < ops.length; opNum++) {
        SymmetryOperation op = ops[opNum];
        Vector axis = op.isRotationOrRotoInversion();
        if (axis == null) {continue;}
        double angle = Math.toDegrees(axis.length());
        if (Math.abs(angle - 120) > CartesianBasis.getAngleToleranceDegrees()) {continue;}
        
        cellVectors = findConventionalVectors(axis, potentialVectors, crystalFamily, lattice);
        
        double volume = Math.abs(cellVectors[0].tripleProductCartesian(cellVectors[1], cellVectors[2]));
        
        if (Math.round(volume / lattice.getCellSize()) == 3) { // It's rhombohedral.  
          LinearBasis cellBasis = new LinearBasis(cellVectors);
          Coordinates testCoords = new Coordinates(new double[] {2.0/3, 1.0/3, 1.0/3}, cellBasis);
          if (!lattice.isLatticePoint(testCoords)) {
            Vector tempVector = cellVectors[0];
            cellVectors[0] = cellVectors[1];
            cellVectors[1] = tempVector;
          }
        }
        break;
      }
    }
    
    if (crystalFamily == CrystalFamily.TETRAGONAL_3D) {
      for (int opNum = 0; opNum < ops.length; opNum++) {
        SymmetryOperation op = ops[opNum];
        Vector axis = op.isRotationOrRotoInversion();
        if (axis == null) {continue;}
        double angle = Math.toDegrees(axis.length());
        if (Math.abs(angle - 90) < CartesianBasis.getAngleToleranceDegrees()) {
          cellVectors = findConventionalVectors(axis, potentialVectors, crystalFamily, lattice);
          if (cellVectors != null) {
            break;
          }
        }
      }
      if (cellVectors == null) {
        throw new RuntimeException("Couldn't find lattice vector parallel to axis of rotation");
      }
    }
      
    /**
     * For orthogonal lattices, we need to explicitly find all of the 
     * cell vectors, because otherwise C-type lattices can cause problems.
     */
    if (crystalFamily == CrystalFamily.ORTHORHOMBIC_3D) {
      
      cellVectors = new Vector[3];
      
      // First find reflection axes
      for (int opNum = 0; opNum < ops.length; opNum++) {
        SymmetryOperation op = ops[opNum];
        Vector axis = op.isReflection();
        if (axis == null) {continue;}
        Vector latticeVector = findMostParallelVector(axis, potentialVectors, 2E-7);
        if (!latticeVector.isParallelTo(axis)) {continue;}
        for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
          if (cellVectors[vecNum] == null) {
            cellVectors[vecNum] = axis.resize(searchDistance);
            break;
          } else if (latticeVector.isParallelTo(cellVectors[vecNum])) {
            break;
          }
        }
      }
      
      // And then find axes that are rotations
      for (int opNum = 0; opNum < ops.length; opNum++) {
        SymmetryOperation op = ops[opNum];
        Vector axis = op.isRotationOrRotoInversion();
        if (axis == null) {continue;}
        Vector latticeVector = findMostParallelVector(axis, potentialVectors, 2E-7);
        if (!latticeVector.isParallelTo(axis)) {continue;}
        for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
          if (cellVectors[vecNum] == null) {
            cellVectors[vecNum] = axis.resize(searchDistance);
            break;
          } else if (latticeVector.isParallelTo(cellVectors[vecNum])) {
            break;
          }
        }
      }
      
      if (cellVectors[2] == null) {
        throw new RuntimeException("Unable to find all conventional vectors for orthorhombic lattice.");
      }
      
      // Now find the corresponding cell vectors
      for (int vecNum = 0; vecNum < potentialVectors.length; vecNum++) {
        Vector potentialVector = potentialVectors[vecNum];
        double length = potentialVector.length();
        for (int cellVecNum = 0; cellVecNum < cellVectors.length; cellVecNum++) {
          Vector cellVector = cellVectors[cellVecNum];
          if ((length < cellVector.length()) && cellVector.isParallelTo(potentialVector)) {
            cellVectors[cellVecNum] = potentialVector;
            break;
          }
        }
      }
      
      // Sort the vectors by length
      // The orientation should be relative to the space group, not lattice params.
      //cellVectors = sortByLength(cellVectors);
      
      // Now treat the special case of "C" lattice types
      double volume = 1;
      for (int vecNum = 0; vecNum < cellVectors.length; vecNum++) {
        volume *= cellVectors[vecNum].length();
      }
      int numPrimCells = (int) Math.round(volume / lattice.getCellSize());
      if (numPrimCells == 2) {
        LinearBasis latticeBasis = new LinearBasis(origin, cellVectors);
        Coordinates face1 = new Coordinates(new double[] {0, 0.5, 0.5}, latticeBasis);
        Coordinates face2 = new Coordinates(new double[] {0.5, 0, 0.5}, latticeBasis);
        if (lattice.isLatticePoint(face1)) {
          Vector tempVector = cellVectors[0];
          cellVectors[0] = cellVectors[1];
          cellVectors[1] = cellVectors[2];
          cellVectors[2] = tempVector;
        } else if (lattice.isLatticePoint(face2)) {
          Vector tempVector = cellVectors[1];
          cellVectors[1] = cellVectors[2];
          cellVectors[2] = tempVector;
        } 
      }
    }
    
    if (crystalFamily == CrystalFamily.MONOCLINIC_3D) {
      for (int opNum = 0; opNum < ops.length; opNum++) {
        SymmetryOperation op = ops[opNum];
        Vector axis = op.isRotationOrRotoInversion();
        if (axis == null) {
          axis = op.isReflection();
        }
        if (axis == null) {continue;}
        cellVectors = findConventionalVectors(axis, potentialVectors, crystalFamily, lattice);
        
        // Make sure it's face centered
        LinearBasis latticeBasis = new LinearBasis(cellVectors);
        Coordinates center = origin.translateBy(new Vector(new double[] {0.5, 0.5, 0.5}, latticeBasis));
        if (lattice.isLatticePoint(center)) {
          cellVectors[1] = cellVectors[1].add(cellVectors[0]);
        } 
        
        // Sort the first two lattice Vectors
        if (cellVectors[0].length() > cellVectors[1].length()) {
          Vector tempVector = cellVectors[0];
          cellVectors[0] = cellVectors[1];
          cellVectors[1] = tempVector;
        }
        
        // If it's face-centered, the second lattice vector should not be planar with the centered face
        Coordinates face1 = origin.translateBy(new Vector(new double[] {0, 0.5, 0.5}, latticeBasis));
        if (lattice.isLatticePoint(face1)) {
          Vector tempVector = cellVectors[0];
          cellVectors[0] = cellVectors[1];
          cellVectors[1] = tempVector;
        }
        break; 
      }
    }
    
    if (lattice.numPeriodicVectors() == 2) {
      Vector periodicVector1 = lattice.getPeriodicVector(0);
      Vector periodicVector2 = lattice.getPeriodicVector(1);
      Vector nonPeriodicVector = periodicVector1.crossProductCartesian(periodicVector2);
      periodicity[2] = false;
      if (crystalFamily == CrystalFamily.SQUARE_2D || crystalFamily == CrystalFamily.RECTANGULAR_2D) {
        cellVectors = findConventionalVectors(nonPeriodicVector, potentialVectors, crystalFamily, lattice);
      }
      
      if (crystalFamily == CrystalFamily.HEXAGONAL_2D) {
        cellVectors = findConventionalVectors(nonPeriodicVector, potentialVectors, crystalFamily, lattice);
      }
    }
    
    if (cellVectors == null) {
      BravaisLattice compactLattice = orthogonalLattice; //lattice.getCompactLattice();
      cellVectors = compactLattice.getCellVectors();
      cellVectors = sortByLength(compactLattice.getPeriodicVectors());
      for (int vecNum = 0; vecNum < compactLattice.numNonPeriodicVectors(); vecNum++) {
        cellVectors = (Vector[]) ArrayUtils.appendElement(cellVectors, compactLattice.getNonPeriodicVector(vecNum));
        periodicity[compactLattice.numPeriodicVectors() + vecNum] = false;
      }
    }

    BravaisLattice conventionalLattice = new BravaisLattice(lattice.getOrigin(), cellVectors, periodicity);
    SuperLattice conventionalSuperLattice = lattice.getSuperLattice(conventionalLattice);
    return conventionalSuperLattice;
    
  }
  
  private static Vector[] sortByLength(Vector[] cellVectors) {
    double[] lengths = new double[cellVectors.length];
    for (int vecNum = 0; vecNum < lengths.length; vecNum++) {
      double length = cellVectors[vecNum].length();
      lengths[vecNum] = length;
    }
    int[] map = ArrayUtils.getSortPermutation(lengths);
    Vector[] sortedVectors = new Vector[lengths.length];
    for (int vecNum = 0; vecNum < map.length; vecNum++) {
      sortedVectors[vecNum] = cellVectors[map[vecNum]]; 
    }
    return sortedVectors;
  }
  

  private static Vector findMostParallelVector(Vector target, Vector[] testVectors) {
    return findMostParallelVector(target, testVectors, true);
  }
  
  private static Vector findMostParallelVector(Vector target, Vector[] testVectors, boolean allowAntiParallel) {
    return findMostParallelVector(target, testVectors, allowAntiParallel, 0);
  }
  
  private static Vector findMostParallelVector(Vector target, Vector[] testVectors, double tolerance) {
    return findMostParallelVector(target, testVectors, true, tolerance);
  }
  
  /**
   * 
   * @param target
   * @param testVectors
   * @param allowAntiParallel
   * @param tolerance A new vector has to be better than the old by at least this much (radians).  If this isn't used, sometimes vectors are effectively randomly selected.
   * @return
   */
  private static Vector findMostParallelVector(Vector target, Vector[] testVectors, boolean allowAntiParallel, double tolerance) {
    
    double minDelta = Double.POSITIVE_INFINITY;
    Vector returnVector = null;
    for (int vecNum = 0; vecNum < testVectors.length; vecNum++) {
      Vector vector = testVectors[vecNum];
      double angle = target.angle(vector);
      double delta0 = Math.abs(angle);
      double delta180 = Math.abs(angle - Math.PI);
      double delta = Math.min(delta0, delta180);
      if (delta < minDelta - tolerance) {
        minDelta = delta;
        returnVector = vector;
      }
    }
    return returnVector;
  }

  /**
   * TODO consider making this a non-static method on a BravaisLattice
   * @param cAxis
   * @param testVectors
   * @param crystalFamily
   * @param lattice
   * @return
   */
  private static Vector[] findConventionalVectors(Vector cAxis, Vector[] testVectors, CrystalFamily crystalFamily, BravaisLattice lattice) {
   
    Vector[] conventionalVectors = new Vector[3];
    
    boolean isCube = false; 
    int angle = -1;
    
    switch (crystalFamily) {
      case CUBIC_3D:
        isCube = true;
      case ORTHORHOMBIC_3D:
      case TETRAGONAL_3D:
      case RECTANGULAR_2D:
      case SQUARE_2D:
        angle = 90;
        break;
      case HEXAGONAL_3D:
      case HEXAGONAL_2D:
        angle = 120;
        break;
      default:
    }
    
    /*Vector[] testVectors = new Vector[nearbyPoints.length];
    for (int pointNum = 0; pointNum < nearbyPoints.length; pointNum++) {
      Vector testVector = new Vector(lattice.getOrigin(), nearbyPoints[pointNum]);
      testVectors[pointNum] = testVector;
    }*/
    
    // Find the shortest vector parallel to the c axis
    conventionalVectors[2] = findMostParallelVector(cAxis, testVectors, 2E-7);

    // This should never happen
    if (!conventionalVectors[2].isParallelTo(cAxis)) {
      //lattice.getCrystalFamily(lattice.getPointGroup().getOperations(), 3); // For debugging
      // TODO: throw a symmetry exception?
      Status.error("Unable to find lattice vector for c axis.");
      return null;
    }
    
    for (int pointNum = 0; pointNum < testVectors.length; pointNum++) {
      Vector testVector = testVectors[pointNum];
      Vector prevVector = conventionalVectors[2];
      if (!testVector.isParallelTo(prevVector)) {continue;}
      if (testVector.length() < prevVector.length()) {
        conventionalVectors[2] = testVector;
      }
    }
    

    boolean[] perpendicularVectors = new boolean[testVectors.length];
    for (int pointNum = 0; pointNum < testVectors.length; pointNum++) {
      Vector testVector = testVectors[pointNum];
      perpendicularVectors[pointNum] = testVector.isPerpendicularTo(cAxis);
      if (perpendicularVectors[pointNum] && isCube) {
        double deltaLength =  Math.abs(testVector.length() - conventionalVectors[2].length());
        perpendicularVectors[pointNum] &= (deltaLength < CartesianBasis.getPrecision());
      }
    }
    
    // Find the remaining two vectors
    for (int pointNum = 0; pointNum < testVectors.length; pointNum++) {
      if (!perpendicularVectors[pointNum]) {continue;}
      Vector testVector1 = testVectors[pointNum];
      Vector knownVector = conventionalVectors[0];
      if (knownVector == null || testVector1.length() < knownVector.length()) {
        Vector bestVector2 = null;
        for (int pointNum2 = 0; pointNum2 < pointNum; pointNum2++) {
          if (!perpendicularVectors[pointNum2]) {continue;}
          Vector testVector2 = testVectors[pointNum2];
          if (bestVector2 != null) {
            double oldArea = testVector1.crossProductCartesian(bestVector2).length();
            double newArea = testVector1.crossProductCartesian(testVector2).length();
            if (newArea > oldArea) {continue;}
          }
          double testAngle = Math.toDegrees(testVector1.angle(testVector2));
          if (Math.abs(testAngle) < CartesianBasis.getAngleToleranceDegrees() || Math.abs(testAngle - 180) < CartesianBasis.getAngleToleranceDegrees()) {continue;}
          if ((angle > 0) && Math.abs(testAngle - angle) > CartesianBasis.getAngleToleranceDegrees()) {continue;}
          bestVector2 = testVector2;
        }
        if (bestVector2 != null) {
          conventionalVectors[0] = testVector1;
          conventionalVectors[1] = bestVector2;
        }
      }
    }
    
    if ((conventionalVectors[0] == null) || (conventionalVectors[1] == null)) {
      //lattice.getCrystalFamily(lattice.getPointGroup().getOperations(), 3); // For debugging
      Status.error("Unable to find all conventional vectors.");
      return null;
    }
    
    boolean[] periodicity = new boolean[conventionalVectors.length];
    Arrays.fill(periodicity, true);
    if (lattice.numPeriodicVectors() == 2) {
      conventionalVectors[2] = lattice.getNonPeriodicVector(0);
    }
    return conventionalVectors;
  }
  
  /*
  private void findConventionalCell() {
    
    int numDimensions = this.numPeriodicVectors();
    
    if (numDimensions == 0) {
      m_CrystalFamily = CrystalFamily.POINT_0D;
      m_ConventionalLattice = new SuperLattice(this, new int[0][0]);
      return;
    }
    
    if (numDimensions == 1) {
      m_CrystalFamily = CrystalFamily.LINEAR_1D;
      m_ConventionalLattice = new SuperLattice(this, new int[][] {{1}});
      return;
    }
    
    // For 3D
    BravaisLattice lattice = this;
    
    // TODO make this more efficient
    if (numDimensions == 2) { // We create a dummy lattice and treat it as 3D
      Vector[] periodicVectors = this.getPeriodicVectors();
      // Ensures that the non-periodic vector is orthogonal, assumes length will be one Angstrom (otherwise it should be rescaled to something small)
      Vector[] cellVectors = LinearBasis.fill3DBasis(periodicVectors);
      lattice = new BravaisLattice(cellVectors, new boolean[] {true, true, false});
    }
    
    BravaisLattice orthogonalLattice = lattice.getQuickOrthogonalLattice();
    double maxDistance = 0;
    for (int vecNum = 0; vecNum < orthogonalLattice.numTotalVectors(); vecNum++) {
      maxDistance = Math.max(maxDistance, lattice.getCellVector(vecNum).length());
    }
    
    Coordinates origin = orthogonalLattice.getOrigin();
    
    // The factor of 3 is for trigonal lattices
    double factor = (numDimensions == 3) ? 3 : 2;
    
    Coordinates[] nearbyCoords = orthogonalLattice.getNearbyLatticePoints(origin, maxDistance * factor, false);
    Vector[] candidateVectors = new Vector[nearbyCoords.length];
    Vector[] unitVectors = new Vector[candidateVectors.length];
    double[] lengths = new double[candidateVectors.length];
    for (int vecNum= 0; vecNum < candidateVectors.length; vecNum++) {
      candidateVectors[vecNum] = new Vector(origin, nearbyCoords[vecNum]);
      lengths[vecNum] = candidateVectors[vecNum].length();
      // Normalize the candidate vectors to better evaluate angles
      unitVectors[vecNum] = candidateVectors[vecNum].divide(lengths[vecNum]);
    }
    
    double tolerance = 1E-3;
    double[][] dotProducts = new double[candidateVectors.length][candidateVectors.length];
    boolean[] ignoreVectors = new boolean[candidateVectors.length];
    for (int vecNum = 0; vecNum < candidateVectors.length; vecNum++) {
      Vector vector = unitVectors[vecNum];
      for (int vecNum2 = 0; vecNum2 < vecNum; vecNum2++) {
        if (ignoreVectors[vecNum2]) {continue;}
        Vector vector2 = unitVectors[vecNum2];
        double dotProduct = Math.abs(vector.innerProductCartesian(vector2));
        if (dotProduct > 1 - tolerance) {
          if (lengths[vecNum] < lengths[vecNum2]) {
            ignoreVectors[vecNum2] = true;
          } else {
            ignoreVectors[vecNum] = true;
          }
        }
        dotProducts[vecNum][vecNum2] = dotProduct;
        dotProducts[vecNum2][vecNum] = dotProduct;
      }
    }
    
    int[][] orthogonalVectors = null;
    
    if (numDimensions == 3) {      
      orthogonalVectors = new int[candidateVectors.length][0];
      for (int vecNum = 0; vecNum < candidateVectors.length; vecNum++) {
        if (ignoreVectors[vecNum]) {continue;}
        for (int vecNum2 = 0; vecNum2 < vecNum; vecNum2++) {
          if (ignoreVectors[vecNum2]) {continue;}
          double dotProduct = dotProducts[vecNum][vecNum2];
          if (dotProduct < tolerance) {
            orthogonalVectors[vecNum] = ArrayUtils.appendElement(orthogonalVectors[vecNum], vecNum2);
            orthogonalVectors[vecNum2] = ArrayUtils.appendElement(orthogonalVectors[vecNum2], vecNum);
          } 
        }
      }
    } else {
      orthogonalVectors = new int[1][0];
      for (int vecNum = 0; vecNum < candidateVectors.length; vecNum++) {
        if (ignoreVectors[vecNum]) {continue;}
        orthogonalVectors[0] = ArrayUtils.appendElement(orthogonalVectors[0], vecNum);
      }
    }

    int[] bestSet = null;
    double smallest = Double.POSITIVE_INFINITY;
    double smallestHexagonalArea = Double.POSITIVE_INFINITY;
    double smallestOrthoArea = Double.POSITIVE_INFINITY;
    boolean isHexagonal = false;
    boolean isOrthorhombic = false;
    int tetragonalZ = -1;
    for (int vecNum = 0; vecNum < orthogonalVectors.length; vecNum++) {
      if ((tetragonalZ >= 0) && (vecNum != tetragonalZ)) {continue;}
      if (orthogonalVectors[vecNum].length < 2) {continue;}
      for (int i = 0; i < orthogonalVectors[vecNum].length; i++) {
        if ((tetragonalZ >= 0) && (vecNum != tetragonalZ)) {continue;}
        int vecNum2 = orthogonalVectors[vecNum][i];
        Vector vector2 = candidateVectors[vecNum2];
        for (int j = 0; j < i; j++) {
          int vecNum3 = orthogonalVectors[vecNum][j];
          if (ignoreVectors[vecNum3]) {continue;}
          Vector vector3 = candidateVectors[vecNum3];
          double dotProduct = dotProducts[vecNum2][vecNum3];
          double area = vector2.crossProductCartesian(vector3).length();
          
          if (numDimensions == 3 && (dotProduct < tolerance)) { // Check to see if it's cubic
            double delta1 = Math.abs(lengths[vecNum] - lengths[vecNum2]);
            double delta2 = Math.abs(lengths[vecNum] - lengths[vecNum3]);
            if ((delta1 < CartesianBasis.getPrecision()) && delta2 < CartesianBasis.getPrecision()) {
              // It's cubic
              Vector[] conventionalVectors = new Vector[] {
                  candidateVectors[vecNum2],
                  candidateVectors[vecNum3],
                  candidateVectors[vecNum],
              };
              BravaisLattice newLattice = new BravaisLattice(conventionalVectors);
              m_ConventionalLattice = this.getSuperLattice(newLattice);
              m_CrystalFamily = CrystalFamily.CUBIC_3D;
              return;
            } else if (delta1 < CartesianBasis.getPrecision()) {
              tetragonalZ = vecNum3;
              break;
            } else if (delta2 < CartesianBasis.getPrecision()) {
              tetragonalZ = vecNum2;
              break;
            }
          }
          
          boolean hexagonalAngle = Math.abs(dotProduct - 0.5) < tolerance;
          boolean hexagonalLength = Math.abs(lengths[vecNum2] - lengths[vecNum3]) < CartesianBasis.getPrecision();
          if (hexagonalAngle && hexagonalLength) {  // It is trigonal / hexagonal
            if (area > smallestHexagonalArea) { continue; }
            smallestHexagonalArea = area;
            isHexagonal = true;
          } else if (isHexagonal) {
            continue;
          } else if (dotProduct < tolerance) {
            if (area > smallestOrthoArea) { continue; }
            smallestOrthoArea = area;
            isOrthorhombic = true;
          } else if (isOrthorhombic) {
            continue;
          } else {
            if (area > smallest) { continue; }
            smallest = area;
          }
          
          if (bestSet == null) {
            bestSet = new int[] {vecNum2, vecNum3, vecNum};
          } else {
            bestSet[0] = vecNum2;
            bestSet[1] = vecNum3;
            bestSet[2] = vecNum;
          }
        }
      }
    }
    
    // It's triclinic
    if (bestSet == null) {
      m_ConventionalLattice = new SuperLattice(this, ArrayUtils.diagonalMatrix(3, 1));
      m_CrystalFamily = CrystalFamily.TRICLINIC_3D;
      return;
    }
    
    Vector[] conventionalVectors = new Vector[3];
    conventionalVectors[0] = candidateVectors[bestSet[0]];
    conventionalVectors[1] = candidateVectors[bestSet[1]];
    conventionalVectors[2] = (numDimensions == 2) ? this.getNonPeriodicVectors()[0] : candidateVectors[bestSet[2]];
    BravaisLattice newLattice = new BravaisLattice(conventionalVectors, lattice.getDimensionPeriodicity());
    m_ConventionalLattice = this.getSuperLattice(newLattice);
    
    if (isHexagonal) {
      m_CrystalFamily = (numDimensions == 2) ? CrystalFamily.HEXAGONAL_2D : CrystalFamily.HEXAGONAL_3D;
    } else if (isOrthorhombic) {
      boolean isSquare = Math.abs(conventionalVectors[0].length() - conventionalVectors[1].length()) < CartesianBasis.getPrecision();
      if (isSquare) {
        m_CrystalFamily = (numDimensions == 2) ? CrystalFamily.SQUARE_2D : CrystalFamily.TETRAGONAL_3D;
      } else {
        m_CrystalFamily = (numDimensions == 2) ? CrystalFamily.RECTANGULAR_2D : CrystalFamily.ORTHORHOMBIC_3D;
      }
    } else {
      m_CrystalFamily = (numDimensions == 2) ? CrystalFamily.OBLIQUE_2D : CrystalFamily.MONOCLINIC_3D;
    }
    
  }
  */
}
