/*
 * Created on Oct 14, 2009
 *
 */
package matsci.structure;

import matsci.Species;
import matsci.location.Coordinates;
import matsci.location.Vector;

public interface IDisorderedStructureData {

  public String getDescription();
  public Vector[] getCellVectors();
  public boolean[] getVectorPeriodicity();
  public int numDefiningSites();
  public Coordinates getSiteCoords(int siteIndex);
  public int numAllowedSpecies(int siteIndex);
  public Species getSiteSpecies(int siteIndex, int allowedSpecNum);
  public double getSiteOccupancy(int siteIndex, int allowedSpecNum);
  
}
