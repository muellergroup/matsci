/*
 * Created on Mar 30, 2005
 *
 */
package matsci.structure;

import java.util.Arrays;

import matsci.Species;
import matsci.location.basis.*;
import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.util.arrays.ArrayUtils;

/**
 * @author Tim Mueller
 *
 */
public class DisorderedStructureBuilder implements IDisorderedStructureData {

  private String m_Description = "Unknown Structure";
  private Coordinates[] m_SiteCoords = new Coordinates[0];
  private Species[][] m_AllowedSpecies = new Species[0][];
  private double[][] m_Occupancies = new double[0][];
  //private Vector[] m_LatticeVectors;
  //private Vector[] m_NonPeriodicVectors;
  private Vector[] m_CellVectors;
  private boolean m_IsVectorPeriodic[] = new boolean[] {true, true, true};
  
  /**
   * 
   */
  public DisorderedStructureBuilder() {
  }
  
  public DisorderedStructureBuilder(IStructureData structure) {
    this.setDescription(structure.getDescription());
    this.setCellVectors(structure.getCellVectors());
    this.setVectorPeriodicity(structure.getVectorPeriodicity());
    m_SiteCoords = new Coordinates[structure.numDefiningSites()];
    m_AllowedSpecies = new Species[m_SiteCoords.length][1];
    m_Occupancies = new double[m_SiteCoords.length][1];
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      m_SiteCoords[siteNum] = structure.getSiteCoords(siteNum);
      m_AllowedSpecies[siteNum][0] = structure.getSiteSpecies(siteNum);
      m_Occupancies[siteNum][0] = 1;
    }
  }
  
  public DisorderedStructureBuilder(IDisorderedStructureData structure) {
    this.setDescription(structure.getDescription());
    this.setCellVectors(structure.getCellVectors());
    this.setVectorPeriodicity(structure.getVectorPeriodicity());
    m_SiteCoords = new Coordinates[structure.numDefiningSites()];
    m_AllowedSpecies = new Species[m_SiteCoords.length][];
    m_Occupancies = new double[m_SiteCoords.length][];
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      m_SiteCoords[siteNum] = structure.getSiteCoords(siteNum);
      m_AllowedSpecies[siteNum] = new Species[structure.numAllowedSpecies(siteNum)];
      m_Occupancies[siteNum] = new double[structure.numAllowedSpecies(siteNum)];
      for (int specNum = 0; specNum < m_AllowedSpecies[siteNum].length; specNum++) {
    	  m_AllowedSpecies[siteNum][specNum] = structure.getSiteSpecies(siteNum, specNum);
    	  m_Occupancies[siteNum][specNum] = structure.getSiteOccupancy(siteNum, specNum);
      }
    }
  }
  
  public DisorderedStructureBuilder(BravaisLattice lattice) {
    this.setCellVectors(lattice.getCellVectors());
    this.setVectorPeriodicity(lattice.getDimensionPeriodicity());
  }
  
  public void setDescription(String description) {
    m_Description = description;
  }
  
  /*
  public void setLatticeVectors(Vector[] latticeVectors) {
    m_LatticeVectors = (Vector[]) ArrayUtils.copyArray(latticeVectors);
  }
  
  public void setNonPeriodicVectors(Vector[] nonPeriodicVectors) {
    m_NonPeriodicVectors = (Vector[]) ArrayUtils.copyArray(nonPeriodicVectors);
  }*/
  
  public void setCellVectors(Vector[] cellVectors) {
    m_CellVectors = (Vector[]) ArrayUtils.copyArray(cellVectors);
  }
  
  public void setVectorPeriodicity(boolean[] isVectorPeriodic) {
    m_IsVectorPeriodic = ArrayUtils.copyArray(isVectorPeriodic);
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getDescription()
   */
  public String getDescription() {
    return m_Description;
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getLatticeVectors()
   */
  public Vector[] getCellVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_CellVectors);
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getNonPeriodicVectors()
   */
  /*
  public Vector[] getNonPeriodicVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_NonPeriodicVectors);
  }*/
  
  public boolean[] getVectorPeriodicity() {
    return ArrayUtils.copyArray(m_IsVectorPeriodic);
  }
  
  public boolean isVectorPeriodic(int vecNum) {
    return m_IsVectorPeriodic[vecNum];
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#numDefiningSites()
   */
  public int numDefiningSites() {
    return m_SiteCoords.length;
  }
  
  public void addSite(Coordinates coords, Species species) {
    m_SiteCoords = (Coordinates[]) ArrayUtils.appendElement(m_SiteCoords, coords);
    m_AllowedSpecies = (Species[][]) ArrayUtils.appendElement(m_AllowedSpecies, new Species[] {species});
    m_Occupancies = ArrayUtils.appendElement(m_Occupancies, new double[] {1});
  }
  
  public void addSites(Coordinates[] coords, Species[] species) {
    
    if (species.length != coords.length) {
      throw new RuntimeException("Specie do not correspond to given coordinates.");
    }
    
    for (int siteNum = 0; siteNum < coords.length; siteNum++) {
    	this.addSite(coords[siteNum], species[siteNum]);
    }
    
  }
  
  public void addSites(Structure.Site[] sites) {
    
    int oldNumSites =m_SiteCoords.length;
    m_SiteCoords = (Coordinates[]) ArrayUtils.growArray(m_SiteCoords, sites.length);
    m_AllowedSpecies = (Species[][]) ArrayUtils.growArray(m_AllowedSpecies, sites.length);
    m_Occupancies = ArrayUtils.growArray(m_Occupancies, sites.length);
    for (int siteNum = 0; siteNum < sites.length; siteNum++) {
      int siteIndex = oldNumSites + siteNum;
      m_SiteCoords[siteIndex] = sites[siteNum].getCoords();
      m_AllowedSpecies[siteIndex] = new Species[] {sites[siteNum].getSpecies()};
      m_Occupancies[siteIndex] = new double[] {1};
    }
  }
  
  public void removeSite(int index) {
    m_SiteCoords = (Coordinates[]) ArrayUtils.removeElement(m_SiteCoords, index);
    m_AllowedSpecies = (Species[][]) ArrayUtils.removeElement(m_AllowedSpecies, index);
    m_Occupancies = ArrayUtils.removeElement(m_Occupancies, index);
  }
  
  public void clearSites() {
    this.removeAllSites();
  }
  
  public void removeAllSites() {
    m_SiteCoords = new Coordinates[0];
    m_AllowedSpecies = new Species[0][];
    m_Occupancies = new double[0][];
  }

  /* (non-Javadoc)
   * @see matsci.structure.IStructureData#getSiteCoords(int)
   */
  public Coordinates getSiteCoords(int index) {
    return m_SiteCoords[index];
  }
  
  public void setSiteCoordinates(Coordinates[] siteCoords) {
    m_SiteCoords = (Coordinates[]) ArrayUtils.copyArray(siteCoords);
  }
  
  public void setSiteCoordinates(int index, Coordinates coords) {
    m_SiteCoords[index] = coords;
  }

  public void setSiteSpecies(int index, Species species) {
    if (m_AllowedSpecies.length < m_SiteCoords.length) {
      m_AllowedSpecies = (Species[][]) ArrayUtils.growArray(m_AllowedSpecies, m_SiteCoords.length - m_AllowedSpecies.length);
      m_AllowedSpecies[index] = new Species[] {species};

      m_Occupancies = ArrayUtils.growArray(m_Occupancies, m_SiteCoords.length - m_AllowedSpecies.length);
    }
    int specIndex = ArrayUtils.findIndex(m_AllowedSpecies[index], species);
    Arrays.fill(m_Occupancies[index], 0);
    m_Occupancies[index][specIndex] = 1;
  }
  
  public Coordinates getAverageDefiningPosition() {
    
    double[] cartCoordArray = new double[m_CellVectors.length];
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      Coordinates siteCoords = m_SiteCoords[siteNum];
      for (int dimNum = 0; dimNum < cartCoordArray.length; dimNum++) {
        cartCoordArray[dimNum] += siteCoords.cartesianCoord(dimNum);
      }
    }
    
    for (int dimNum = 0; dimNum < cartCoordArray.length; dimNum++) {
      cartCoordArray[dimNum] /= m_SiteCoords.length;
    }
    
    return new Coordinates(cartCoordArray, CartesianBasis.getInstance());
    
  }
  
  public void translateToAverageDefiningPosition(Coordinates newAverage) {
    
    Coordinates thisAverage = this.getAverageDefiningPosition();
    Vector translation = new Vector(thisAverage, newAverage);
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      m_SiteCoords[siteNum] = m_SiteCoords[siteNum].translateBy(translation);
    }
    
  }
  
  public void translateBy(Vector translation) {
    
    for (int siteNum = 0; siteNum < m_SiteCoords.length; siteNum++) {
      m_SiteCoords[siteNum] = m_SiteCoords[siteNum].translateBy(translation);
    }
    
  }

	@Override
	public int numAllowedSpecies(int siteIndex) {
		return m_AllowedSpecies[siteIndex].length;
	}
	
	@Override
	public Species getSiteSpecies(int siteIndex, int allowedSpecNum) {
		return m_AllowedSpecies[siteIndex][allowedSpecNum];
	}
	
	@Override
	public double getSiteOccupancy(int siteIndex, int allowedSpecNum) {
		return m_Occupancies[siteIndex][allowedSpecNum];
	}
	
	public void setAllowedSpecies(int siteNum, Species[] species, double[] occupancies) {
		if (species.length != occupancies.length) {
			throw new RuntimeException("Number of occupancies must match number of allowed species.");
		}
		m_AllowedSpecies[siteNum] = species.clone();
		m_Occupancies[siteNum] = occupancies.clone();
	}
	
	public Species[] getAllowedSpecies(int siteNum) {
		return (Species[]) m_AllowedSpecies[siteNum].clone();
	}
	
	public double[] getOccupancies(int siteNum) {
		return m_Occupancies[siteNum].clone();
	}

}
