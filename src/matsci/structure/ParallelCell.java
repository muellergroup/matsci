package matsci.structure;

import java.util.Arrays;

import matsci.location.Coordinates;
import matsci.location.Vector;
import matsci.location.basis.CartesianBasis;
import matsci.location.basis.LinearBasis;
import matsci.util.MSMath;
import matsci.util.arrays.ArrayIndexer;
import matsci.util.arrays.ArrayUtils;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: </p>
 * @author Tim Mueller
 * @version 1.0
 */

public class ParallelCell {

  private Coordinates m_Origin;
  private Vector[] m_CellVectors;
  private boolean[] m_IsVectorFinite;
  private Vector[] m_FiniteVectors;
  private Vector[] m_InfiniteVectors;

  private LinearBasis m_CellBasis; 
  
  public ParallelCell(Coordinates origin, Vector[] givenVectors, boolean[] isVectorFinite) {
    
    m_Origin = origin;
    
    // I force the non-periodic vectors to be orthogonal to the periodic vectors
    // because otherwise it can cause some problems with the symmetry code.  
    // No, you can't do this.  It messes up poscars.
    /*for (int vecNum = isVectorFinite.length - 1; vecNum >= 0; vecNum--) {
      if (!isVectorFinite[vecNum]) {
        givenVectors = (Vector[]) ArrayUtils.removeElement(givenVectors, vecNum);
      }
    }
    Vector[] fullBasis = LinearBasis.fill3DBasis(givenVectors);
    
    // At least try to keep the list of periodic and non-periodic vectors the same
    for (int vecNum = 0; vecNum < isVectorFinite.length; vecNum++) {
      if (!isVectorFinite[vecNum]) {
        Vector nonPeriodicVector = fullBasis[fullBasis.length - 1];
        System.arraycopy(fullBasis, vecNum, fullBasis, vecNum + 1, fullBasis.length - vecNum - 1);
        fullBasis[vecNum] = nonPeriodicVector;
      }
    }*/
    
    /*m_IsVectorFinite = ArrayUtils.copyArray(isVectorFinite);
    m_CellVectors = fullBasis;*/
    
    Vector[] shiftedVectors = (Vector[]) ArrayUtils.copyArray(givenVectors);
    for (int vecNum = 0; vecNum < shiftedVectors.length; vecNum++) {
      shiftedVectors[vecNum] = shiftedVectors[vecNum].translateTo(origin);
    }
    
    m_CellVectors = LinearBasis.fill3DBasis(givenVectors);
    m_IsVectorFinite = ArrayUtils.growArray(isVectorFinite, 3-isVectorFinite.length);
    
    //m_CellBasis = new LinearBasis(m_CellVectors);
    m_CellBasis = new LinearBasis(origin, m_CellVectors); // TODO make sure this doesn't break anything
    
    m_FiniteVectors = new Vector[0];
    m_InfiniteVectors = new Vector[0];
    for (int vecNum = 0; vecNum < m_CellVectors.length; vecNum++) {
      m_CellVectors[vecNum] = m_CellVectors[vecNum].translateTo(origin);
      if (m_IsVectorFinite[vecNum]) {
        m_FiniteVectors = (Vector[]) ArrayUtils.appendElement(m_FiniteVectors, m_CellVectors[vecNum]);
      } else {
        m_InfiniteVectors = (Vector[]) ArrayUtils.appendElement(m_InfiniteVectors, m_CellVectors[vecNum]);
      }
    }
    /*
    double[][] cellToCartesian = new double[3][];
    
    m_FiniteVectors = new Vector[finiteVectors.length];
    for (int vectNum = 0; vectNum < m_FiniteVectors.length; vectNum++) {
      m_FiniteVectors[vectNum] = finiteVectors[vectNum].translateTo(origin);      
      cellToCartesian[vectNum] = m_FiniteVectors[vectNum].getCartesianDirection();
    }

    this.setInfiniteVectors(infiniteVectors);
    for (int vectNum = 0; vectNum < m_InfiniteVectors.length; vectNum++) {
      cellToCartesian[vectNum + m_FiniteVectors.length] = m_InfiniteVectors[vectNum].getCartesianDirection();
    }
    
    m_CellBasis = new LinearBasis(m_Origin, cellToCartesian);*/
  }
  
  public ParallelCell(Vector[] cellVectors, boolean[] isVectorFinite) {
    this(CartesianBasis.getInstance().getOrigin(), cellVectors, isVectorFinite);
  }
  
  public Coordinates getOrigin() {
    return m_Origin;
  }

  public LinearBasis getCellBasis() {
    return m_CellBasis;
  }

  public int numFiniteVectors() {
    return m_FiniteVectors.length;
  }
  
  public int numInfiniteVectors() {
    return m_InfiniteVectors.length;
  }
  
  public Vector getFiniteVector(int vectNum) {
    return m_FiniteVectors[vectNum];
  }
  
  public Vector getInfiniteVector(int vectNum) {
    return m_InfiniteVectors[vectNum];
  }
  
  public Vector[] getCellVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_CellVectors);
  }
  
  public int numCellVectors() {
    return m_CellVectors.length;
  }
  
  public boolean[] areVectorsFinite() {
    return ArrayUtils.copyArray(m_IsVectorFinite);
  }
  
  public Vector getCellVector(int vectorNumber) {
    
    return m_CellVectors[vectorNumber];
    
    /*
    if (vectorNumber >= m_FiniteVectors.length) {
      return m_InfiniteVectors[vectorNumber - m_FiniteVectors.length];
    }
    return m_FiniteVectors[vectorNumber];*/
  }
  
  public boolean isVectorFinite(int vecNum) {
    return m_IsVectorFinite[vecNum];
  }
  
  public Vector[] getFiniteVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_FiniteVectors);
  }
  
  public Vector[] getInfiniteVectors() {
    return (Vector[]) ArrayUtils.copyArray(m_InfiniteVectors);
  }
  
  /*
  private void setInfiniteVectors(Vector[] givenVectors, boolean[] isVectorPeriodic) {

    
    Vector[] givenVectors = new Vector[infiniteVectors.length + m_FiniteVectors.length];
    
    for (int vectNum = 0; vectNum < m_FiniteVectors.length; vectNum++) {
      givenVectors[vectNum] = m_FiniteVectors[vectNum];
    }
    
    for (int vectNum = 0; vectNum < infiniteVectors.length; vectNum++) {
      Vector infiniteVector = infiniteVectors[vectNum].translateTo(m_Origin);
      givenVectors[m_FiniteVectors.length + vectNum] = infiniteVector;
    }
    
    Vector[] allVectors = LinearBasis.fill3DBasis(givenVectors);   
    m_InfiniteVectors = new Vector[3 - m_FiniteVectors.length];
    System.arraycopy(allVectors, m_FiniteVectors.length, m_InfiniteVectors, 0, m_InfiniteVectors.length);
    
  }*/
  
  public static double getCellSize(Vector[] finiteVectors) {
    if (finiteVectors.length == 0) {return Double.POSITIVE_INFINITY;}
    if (finiteVectors.length == 1) {return finiteVectors[0].length();}
    if (finiteVectors.length == 2) {return finiteVectors[0].crossProductCartesian(finiteVectors[1]).length();}
    return Math.abs(finiteVectors[0].tripleProductCartesian(finiteVectors[1], finiteVectors[2]));
  }
  
  public double getCellSize() {
    return getCellSize(m_FiniteVectors);
  }
  
  public double getLongestDiagonal() {
    
    /*LinearBasis basis = this.getCellBasis();
    int[] numOptions = new int[basis.numDimensions()];
    int[] allowedStates = new int[numOptions.length];
    
    for (int dimNum = 0; dimNum < numOptions.length; dimNum++) {
      numOptions[dimNum] = this.isVectorFinite(dimNum) ? 2 : 1;
      allowedStates[dimNum] = numOptions[dimNum];
    }
    
    for (int dimNum = 0; dimNum < numOptions.length; dimNum++) {
      if (numOptions[dimNum] == 2) {
        allowedStates[dimNum] = 1;
        break;  // We only need to look at half of the cell to get diagonals
      }
    }
    
    ArrayIndexer indexer = new ArrayIndexer(allowedStates);

    for (int diagNum = 0; diagNum < indexer.numAllowedStates(); diagNum++) {
      int[] state = indexer.splitValue(diagNum);
      int[] coordArray2 = new int[state.length];
      for (int dimNum = 0; dimNum < coordArray2.length; dimNum++) {
        coordArray2[dimNum] = (state[dimNum] + 1) %  numOptions[dimNum];
      }
      Coordinates coords1 = new Coordinates(state, basis);
      Coordinates coords2 = new Coordinates(coordArray2, basis);
      returnValue = Math.max(returnValue, coords1.distanceFrom(coords2));
    }*/
    
    Coordinates[] corners = this.getCorners();
    double returnValue = 0;
    for (int diagNum = 0; diagNum < corners.length / 2; diagNum++) {
      double distance = corners[diagNum].distanceFrom(corners[corners.length - diagNum - 1]);
      returnValue = Math.max(returnValue, distance);
    }
    return returnValue;
    
  }
  
  /**
   * For the periodic vectors, finds a set of "triangles" (or tetrahedra in 3 dimensions)
   * with vertices at the corners of the cell that completely tesselate the cell.
   * 
   * @return
   */
  public Coordinates[][] getTriangulation() {
    
    Coordinates[] corners = this.getCorners();
    
    int numDimensions = this.numFiniteVectors();
    
    if (numDimensions == 0) {
      return new Coordinates[][] {{this.getOrigin()}};
    }
    
    if (numDimensions == 1) {
      return new Coordinates[][] {{corners[0], corners[1]}};
    }
    
    if (numDimensions == 2) {

      if (corners[0].distanceFrom(corners[3]) < corners[1].distanceFrom(corners[2])) {
        return new Coordinates[][] {
            {corners[0], corners[1], corners[3]},
            {corners[0], corners[2], corners[3]}
        };
      } else {
        return new Coordinates[][] {
            {corners[1], corners[0], corners[2]},
            {corners[1], corners[3], corners[2]}
        };
      }
    }
    
    if (numDimensions != 3) {
      throw new RuntimeException("Cannot currently generate a tessellation for " + numDimensions + " finite dimensions.");
    }

    Coordinates[][] edges = this.findCompact3DEdges(corners);
    //Coordinates[][] edges = this.findCompact3DEdges2(corners);
    //Coordinates[][] edges = this.findCompact3DEdges3(corners);
    Coordinates[][] returnArray = new Coordinates[6][4];
    int returnIndex = 0;
    
    for (int corner1Num = 0; corner1Num < corners.length; corner1Num++) {
      Coordinates corner1 = corners[corner1Num];
      for (int corner2Num = corner1Num + 1; corner2Num < corners.length; corner2Num++) {
        Coordinates corner2 = corners[corner2Num];
        boolean match = false;
        for (int edgeNum = 0; edgeNum < edges.length; edgeNum++) {
          Coordinates[] edge = edges[edgeNum];
          match |= ((edge[0] == corner1) && (edge[1] == corner2));
          match |= ((edge[0] == corner2) && (edge[1] == corner1));
          if (match) {break;}
        }
        if (!match) {continue;}
        for (int corner3Num = corner2Num + 1; corner3Num < corners.length; corner3Num++) {
          Coordinates corner3 = corners[corner3Num];
          match = false;
          for (int edgeNum = 0; edgeNum < edges.length; edgeNum++) {
            Coordinates[] edge = edges[edgeNum];
            match |= ((edge[0] == corner3) && (edge[1] == corner1));
            match |= ((edge[0] == corner1) && (edge[1] == corner3));
            if (match) {break;}
          }
          if (!match) {continue;}
          match = false;
          for (int edgeNum = 0; edgeNum < edges.length; edgeNum++) {
            Coordinates[] edge = edges[edgeNum];
            match |= ((edge[0] == corner3) && (edge[1] == corner2));
            match |= ((edge[0] == corner2) && (edge[1] == corner3));
            if (match) {break;}
          }
          if (!match) {continue;}
          for (int corner4Num = corner3Num + 1; corner4Num < corners.length; corner4Num++) {
            Coordinates corner4 = corners[corner4Num];
            match = false;
            for (int edgeNum = 0; edgeNum < edges.length; edgeNum++) {
              Coordinates[] edge = edges[edgeNum];
              match |= ((edge[0] == corner4) && (edge[1] == corner1));
              match |= ((edge[0] == corner1) && (edge[1] == corner4));
              if (match) {break;}
            }
            if (!match) {continue;}
            match = false;
            for (int edgeNum = 0; edgeNum < edges.length; edgeNum++) {
              Coordinates[] edge = edges[edgeNum];
              match |= ((edge[0] == corner4) && (edge[1] == corner2));
              match |= ((edge[0] == corner2) && (edge[1] == corner4));
              if (match) {break;}
            }
            if (!match) {continue;}
            match = false;
            for (int edgeNum = 0; edgeNum < edges.length; edgeNum++) {
              Coordinates[] edge = edges[edgeNum];
              match |= ((edge[0] == corner4) && (edge[1] == corner3));
              match |= ((edge[0] == corner3) && (edge[1] == corner4));
              if (match) {break;}
            }
            if (!match) {continue;}
            returnArray[returnIndex][0] = corner1;
            returnArray[returnIndex][1] = corner2;
            returnArray[returnIndex][2] = corner3;
            returnArray[returnIndex][3] = corner4;
            returnIndex++;
          }
        }
      }
    }
   
    return returnArray;
  }
  
  public Coordinates[] getCorners() {
    
    LinearBasis basis = this.getCellBasis();
    
    int[] allowedStates = new int[this.numCellVectors()];
    for (int stateNum = 0; stateNum < allowedStates.length; stateNum++) {
       allowedStates[stateNum] = this.isVectorFinite(stateNum) ? 2 : 1;
    }
    
    ArrayIndexer indexer = new ArrayIndexer(allowedStates);
    
    Coordinates[] corners = new Coordinates[indexer.numAllowedStates()];
    for (int cornerNum = 0; cornerNum < corners.length; cornerNum++) {
      int[] coordArray = indexer.splitValue(cornerNum);
      corners[cornerNum] = new Coordinates(coordArray, basis);
    }
    
    return corners;
    
    /*corners[0] = new Coordinates(new double[] {0, 0, 0}, basis);
    corners[1] = new Coordinates(new double[] {0, 1, 0}, basis);
    corners[2] = new Coordinates(new double[] {1, 0, 0}, basis);
    corners[3] = new Coordinates(new double[] {1, 1, 0}, basis);
    corners[4] = new Coordinates(new double[] {0, 0, 1}, basis);
    corners[5] = new Coordinates(new double[] {0, 1, 1}, basis);
    corners[6] = new Coordinates(new double[] {1, 0, 1}, basis);
    corners[7] = new Coordinates(new double[] {1, 1, 1}, basis);*/
    
  }
  
  /**
   * Used for tesselation in 3d
   * 
   * @return
   */
  private Coordinates[][] findCompact3DEdges(Coordinates[] corners) {

    Coordinates[][] edges = new Coordinates[][] {
        {corners[0], corners[1]},
        {corners[0], corners[2]},
        {corners[0], corners[4]},
        {corners[1], corners[3]},
        {corners[1], corners[5]},
        {corners[2], corners[3]},
        {corners[2], corners[6]},
        {corners[3], corners[7]},
        {corners[4], corners[5]},
        {corners[4], corners[6]},
        {corners[5], corners[7]},
        {corners[6], corners[7]},
    };
    
    if (corners[0].distanceFrom(corners[3]) < corners[1].distanceFrom(corners[2])) {
      edges = (Coordinates[][]) ArrayUtils.appendArray(edges, new Coordinates[][] {
          {corners[0], corners[3]},
          {corners[4], corners[7]}
      });
    } else {
      edges = (Coordinates[][]) ArrayUtils.appendArray(edges, new Coordinates[][] {
          {corners[1], corners[2]},
          {corners[5], corners[6]}
      });
    }
    
    if (corners[0].distanceFrom(corners[5]) < corners[1].distanceFrom(corners[4])) {
      edges = (Coordinates[][]) ArrayUtils.appendArray(edges, new Coordinates[][] {
          {corners[0], corners[5]},
          {corners[2], corners[7]}
      });
    } else {
      edges = (Coordinates[][]) ArrayUtils.appendArray(edges, new Coordinates[][] {
          {corners[1], corners[4]},
          {corners[3], corners[6]}
      });
    }
    
    if (corners[0].distanceFrom(corners[6]) < corners[2].distanceFrom(corners[4])) {
      edges = (Coordinates[][]) ArrayUtils.appendArray(edges, new Coordinates[][] {
          {corners[0], corners[6]},
          {corners[1], corners[7]}
      });
    } else {
      edges = (Coordinates[][]) ArrayUtils.appendArray(edges, new Coordinates[][] {
          {corners[2], corners[4]},
          {corners[3], corners[5]}
      });
    }
    

    Coordinates[][] bodyDiagonals = new Coordinates[][] {
        {corners[0], corners[7]},
        {corners[1], corners[6]},
        {corners[2], corners[5]},
        {corners[3], corners[4]},
    };
    
    double minDistance = Double.POSITIVE_INFINITY;
    Coordinates[] bestDiag = null;
    for (int diagNum = 0; diagNum < bodyDiagonals.length; diagNum++) {
      Coordinates[] diagonal = bodyDiagonals[diagNum];
      Coordinates testPoint = diagonal[0];
      
      boolean match1 = (ArrayUtils.arrayContains(edges[12], testPoint) || ArrayUtils.arrayContains(edges[13], testPoint));
      boolean match2 = (ArrayUtils.arrayContains(edges[14], testPoint) || ArrayUtils.arrayContains(edges[15], testPoint));
      boolean match3 = (ArrayUtils.arrayContains(edges[16], testPoint) || ArrayUtils.arrayContains(edges[17], testPoint));

      if (!match1 && !match2 && !match3) { // We don't want this one
        continue;
      }
      
      if (match1 && match2 && match3) { // This must be it
        bestDiag = diagonal;
        break;
      }
      
      double distance = diagonal[0].distanceFrom(diagonal[1]);
      if (distance < minDistance) {
        minDistance = distance;
        bestDiag = diagonal;
      }
    }
    edges = (Coordinates[][]) ArrayUtils.appendElement(edges, bestDiag);    
    return edges;
  }
  
  /**
   * This method always returns edges designed so that three edge diagonals meet at a 
   * single point.  I think this is the version VASP uses.
   * @param corners
   * @return
   */
  private Coordinates[][] findCompact3DEdges2(Coordinates[] corners) {
    
    Coordinates[][] edges = new Coordinates[][] {
        {corners[0], corners[1]},
        {corners[0], corners[2]},
        {corners[0], corners[4]},
        {corners[1], corners[3]},
        {corners[1], corners[5]},
        {corners[2], corners[3]},
        {corners[2], corners[6]},
        {corners[3], corners[7]},
        {corners[4], corners[5]},
        {corners[4], corners[6]},
        {corners[5], corners[7]},
        {corners[6], corners[7]},
    };
    
    Coordinates[][] bodyDiagonals = new Coordinates[][] {
        {corners[0], corners[7]},
        {corners[1], corners[6]},
        {corners[2], corners[5]},
        {corners[3], corners[4]},
    };
    
    Coordinates[] minDiagonal = null;
    double minDiagonalLength = Double.POSITIVE_INFINITY;
    
    for (int diagonalNum = 0; diagonalNum < bodyDiagonals.length; diagonalNum++) {
      Coordinates[] diagonal = bodyDiagonals[diagonalNum];
      double distance = diagonal[0].distanceFrom(diagonal[1]);
      if (distance < minDiagonalLength) {
        minDiagonal = diagonal;
        minDiagonalLength = distance;
      }
    }
    
    Coordinates[][] faceDiagonals = new Coordinates[][] {
        {corners[0], corners[3]},
        {corners[4], corners[7]},
        {corners[1], corners[2]},
        {corners[5], corners[6]},
        {corners[0], corners[5]},
        {corners[2], corners[7]},
        {corners[1], corners[4]},
        {corners[3], corners[6]},
        {corners[0], corners[6]},
        {corners[1], corners[7]},
        {corners[2], corners[4]},
        {corners[3], corners[5]}
    };
    
    for (int diagNum = 0; diagNum < faceDiagonals.length; diagNum++) {
      Coordinates[] diagonal = faceDiagonals[diagNum];
      if (ArrayUtils.arrayContains(diagonal, minDiagonal[0]) || ArrayUtils.arrayContains(diagonal, minDiagonal[1])) {
        edges = (Coordinates[][]) ArrayUtils.appendElement(edges, diagonal);
      }
    }

    edges = (Coordinates[][]) ArrayUtils.appendElement(edges, minDiagonal);
    
    return edges;
  }
  
  
  /**
   * This method always returns edges designed so that three edge diagonals meet at a 
   * single point.  It doesn't even try to find the min diagonal. VASP might use something
   * like this.
   * @param corners
   * @return
   */
  private Coordinates[][] findCompact3DEdges3(Coordinates[] corners) {
    
    Coordinates[][] edges = new Coordinates[][] {
        {corners[0], corners[1]},
        {corners[0], corners[2]},
        {corners[0], corners[4]},
        {corners[1], corners[3]},
        {corners[1], corners[5]},
        {corners[2], corners[3]},
        {corners[2], corners[6]},
        {corners[3], corners[7]},
        {corners[4], corners[5]},
        {corners[4], corners[6]},
        {corners[5], corners[7]},
        {corners[6], corners[7]},
    };
    
    Coordinates[][] bodyDiagonals = new Coordinates[][] {
        //{corners[0], corners[7]},
        {corners[1], corners[6]},
        //{corners[2], corners[5]},
        //{corners[3], corners[4]},
    };
    
    Coordinates[] minDiagonal = null;
    double minDiagonalLength = Double.POSITIVE_INFINITY;
    
    for (int diagonalNum = 0; diagonalNum < bodyDiagonals.length; diagonalNum++) {
      Coordinates[] diagonal = bodyDiagonals[diagonalNum];
      double distance = diagonal[0].distanceFrom(diagonal[1]);
      if (distance < minDiagonalLength) {
        minDiagonal = diagonal;
        minDiagonalLength = distance;
      }
    }
    
    Coordinates[][] faceDiagonals = new Coordinates[][] {
        //{corners[0], corners[3]},
        //{corners[4], corners[7]},
        {corners[1], corners[2]},
        {corners[5], corners[6]},
        //{corners[0], corners[5]},
        //{corners[2], corners[7]},
        {corners[1], corners[4]},
        {corners[3], corners[6]},
        {corners[0], corners[6]},
        {corners[1], corners[7]},
        //{corners[2], corners[4]},
        //{corners[3], corners[5]}
    };
    
    for (int diagNum = 0; diagNum < faceDiagonals.length; diagNum++) {
      Coordinates[] diagonal = faceDiagonals[diagNum];
      if (ArrayUtils.arrayContains(diagonal, minDiagonal[0]) || ArrayUtils.arrayContains(diagonal, minDiagonal[1])) {
        edges = (Coordinates[][]) ArrayUtils.appendElement(edges, diagonal);
      }
    }

    edges = (Coordinates[][]) ArrayUtils.appendElement(edges, minDiagonal);
    
    return edges;
  }
  
  
  public ParallelCell translateTo(Coordinates newOrigin) {
    return new ParallelCell(newOrigin, m_CellVectors, m_IsVectorFinite);
  }
  
}
